﻿using System;
using System.Configuration;
using System.IO;
using System.Web.Hosting;

namespace CmsPublic.DataProviders.Logging
{
    ///<summary>
    ///</summary>
    public class FileLogger
    {
        private const string Format = "{0} {1}: {2}";
        private static readonly string Logpath;

        static FileLogger()
        {
            var path = HostingEnvironment.MapPath(@"~/App_Data/filelog.txt");
            var fullPath = ConfigurationManager.AppSettings["FileLoggerPath"];
            if (string.IsNullOrWhiteSpace(fullPath) && path != null)
            {
                fullPath = Path.GetFullPath(path);
            }
            Logpath = fullPath;
        }

        private static void DoSomething()
        {
        }

        ///<summary>
        ///</summary>
        ///<param name="message"></param>
        public static void Log(string message)
        {
            string formattedMessage = string.Format(Format, DateTime.Now.ToShortDateString(),
                                                    DateTime.Now.ToLongTimeString(), message);
            InternalLog(formattedMessage);
        }

        ///<summary>
        ///</summary>
        ///<param name="message"></param>
        public static void Log(object message)
        {
            string formattedMessage = string.Format(Format, DateTime.Now.ToShortDateString(),
                                                    DateTime.Now.ToLongTimeString(), message);
            InternalLog(formattedMessage);
        }

        private static void InternalLog(string formattedMessage)
        {
            try
            {
                var fs = new FileStream(Logpath, FileMode.Append, FileAccess.Write, FileShare.ReadWrite);
                var fw = new StreamWriter(fs);
                try
                {
                    fw.WriteLine(formattedMessage);
                    fw.Flush();
                }
                catch (IOException)
                {
                }
                finally
                {
                    fw.Close();
                    fs.Close();
                }
            }
            catch (Exception)
            {
                DoSomething();
            }
        }
    }
}