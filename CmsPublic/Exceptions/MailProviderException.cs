﻿using System;

namespace CmsPublic.Exceptions
{
    public class MailProviderException : Exception
    {
        public MailProviderException(string message)
            : base(message)
        {

        }
    }
}
