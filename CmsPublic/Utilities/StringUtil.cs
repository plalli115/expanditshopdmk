﻿namespace CmsPublic.Utilities {
    public class StringUtil {
        public static bool IsStringEqual(string a, string b){
            if (IsNullEqual(a, b)) {
                return true;
            }
            return !IsOneNull(a, b) && a.Equals(b);
        }

        private static bool IsNullEqual(string a, string b) {
            return a == null && b == null;
        }

        private static bool IsOneNull(string a, string b) {
            return a == null || b == null;
        }
    }
}
