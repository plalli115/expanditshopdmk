﻿using System;

namespace CmsPublic.Utilities
{
    ///<summary>
    ///</summary>
    public class StopWatch
    {
        private bool _running;
        private DateTime _startTime;
        private DateTime _stopTime;


        ///<summary>
        /// Start timer
        ///</summary>
        public void Start()
        {
            _startTime = DateTime.Now;
            _running = true;
        }


        ///<summary>
        /// Stop the timer
        ///</summary>
        public void Stop()
        {
            _stopTime = DateTime.Now;
            _running = false;
        }

        ///<summary>
        /// Get elaspsed time in milliseconds
        ///</summary>
        ///<returns></returns>
        public double GetElapsedTime()
        {
            TimeSpan interval;

            if (_running)
                interval = DateTime.Now - _startTime;
            else
                interval = _stopTime - _startTime;

            return interval.TotalMilliseconds;
        }

        ///<summary>
        /// Get elaspsed time in seconds
        ///</summary>
        ///<returns></returns>
        public double GetElapsedTimeSecs()
        {
            TimeSpan interval;

            if (_running)
                interval = DateTime.Now - _startTime;
            else
                interval = _stopTime - _startTime;

            return interval.TotalSeconds;
        }
    }
}