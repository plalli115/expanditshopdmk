﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using Dapper;

namespace CmsPublic.DataRepository
{
    public class BaseDataService
    {
        protected IExpanditDbFactory DbFactory;

        public BaseDataService(IExpanditDbFactory dbFactory)
        {
            DbFactory = dbFactory;
        }

        public IDbConnection GetConnection(IExpanditDbFactory connectionFactory = null)
        {
            return connectionFactory != null ? connectionFactory.GetConnection() : DbFactory.GetConnection();
        }

        public IDbDataAdapter GetDataAdapter(IDbConnection connection)
        {
            return DbFactory.GetDataAdapter(connection);
        }

        protected virtual T GetSingle<T>(string query, object param = null, IExpanditDbFactory connectionFactory = null, CommandType commandType = CommandType.Text) 
        {
            IDbConnection connection = GetConnection(connectionFactory);

            try
            {
                return connection.Query<T>(query, param, commandType: commandType).SingleOrDefault();
            }
            finally
            {
                DbFactory.Finalize(connection);
            }
        }

        protected virtual IEnumerable<T> GetResults<T>(string query, object param = null, IExpanditDbFactory connectionFactory = null, CommandType commandType = CommandType.Text)
        {
            IDbConnection connection = GetConnection(connectionFactory);

            try
            {
                return connection.Query<T>(query, param, commandType: commandType);
            }
            finally
            {
                DbFactory.Finalize(connection);
            }
        }

        public virtual IEnumerable<dynamic> GetResults(string query, object param = null, IExpanditDbFactory connectionFactory = null, CommandType commandType = CommandType.Text)
        {
            IDbConnection connection = GetConnection(connectionFactory);
            try
            {
                return connection.Query(query, param, commandType: commandType);
            }
            finally
            {
                DbFactory.Finalize(connection);
            }
        }

        public virtual bool Execute(string query, object param = null, IExpanditDbFactory connectionFactory = null)
        {
            IDbConnection connection = GetConnection(connectionFactory);
            try
            {
                int affectedRows = connection.Execute(query, param);
                return affectedRows > 0;
            }
            finally
            {
                DbFactory.Finalize(connection);
            }
        }

        public virtual IEnumerable<T> GetAllFrom<T>(string tableName, IExpanditDbFactory connectionFactory = null)
        {
            return GetResults<T>("select * from " + tableName, null, connectionFactory);
        }

        public string GenerateUpdateParameters(object obj)
        {
            PropertyInfo[] properties = obj.GetType().GetProperties();
            List<string> strings = (from propInfo in properties
                                    let infos = (TableColumnAttribute[])propInfo.GetCustomAttributes(typeof(TableColumnAttribute), false)
                                    where (infos.Length == 0 || infos.All(i => i.MapForUpdate))
                                    select "[" + GetColumnName(propInfo) + "] = @" + propInfo.Name).ToList();

            return string.Join(",", strings);
        }

        public virtual string GenerateInsertParameters(object obj, bool outputValue = true)
        {
            PropertyInfo[] properties = obj.GetType().GetProperties();
            var columnNames = new List<string>();
            var parameters = new List<string>();
            foreach (PropertyInfo propInfo in properties)
            {
                var infos = (TableColumnAttribute[])propInfo.GetCustomAttributes(typeof(TableColumnAttribute), false);
                if (infos.Length == 0 || infos.All(i => i.MapForInsert))
                {
                    columnNames.Add("[" + GetColumnName(propInfo) + "]");
                    parameters.Add("@" + propInfo.Name);
                }
            }

            string firstPart = "(" + string.Join(",", columnNames) + ")";

            if (outputValue)
            {
                firstPart += "output Inserted.* VALUES (" + string.Join(",", parameters) + ")";
            }
            else
            {
                firstPart += " VALUES (" + string.Join(",", parameters) + ")";
            }

            return firstPart;
        }

        private string GetColumnName(PropertyInfo propertyInfo)
        {
            var attributes = (TableColumnAttribute[])propertyInfo.GetCustomAttributes(typeof(TableColumnAttribute), false);
            TableColumnAttribute attribute = attributes.FirstOrDefault(a => !string.IsNullOrWhiteSpace(a.ColumnName));

            return (attribute != null) ? attribute.ColumnName : propertyInfo.Name;
        }
    }
}