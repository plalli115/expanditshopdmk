﻿using System;
using System.Linq;

namespace CmsPublic.DataRepository
{
    [AttributeUsage(AttributeTargets.Class)]
    public class TablePropertiesAttribute : Attribute
    {
        public string TableName { get; set; }
        public string PrimaryKeyName { get; set; }

        public static TablePropertiesAttribute GetTableProperties(object target)
        {
            Attribute[] attrs = GetCustomAttributes(target.GetType());
            var properties = attrs.FirstOrDefault(a => a is TablePropertiesAttribute) as TablePropertiesAttribute;

            return properties;
        }
    }
}