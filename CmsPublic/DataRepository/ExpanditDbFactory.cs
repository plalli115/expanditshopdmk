﻿using System;
using System.Data;
using System.Data.Common;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Transactions;
using StackExchange.Profiling.Data;

namespace CmsPublic.DataRepository
{
    public class ExpanditDbFactory : IExpanditDbFactory
    {
        private readonly DbProviderFactory _dbFactory;
        private readonly string _strConn;

        public ExpanditDbFactory(string connectionString, string providerName)
        {
            try
            {
                _strConn = connectionString;
                _dbFactory = new DbConnectionScope(DbProviderFactories.GetFactory(providerName));
            }
            catch (Exception ex)
            {
                throw new Exception("Could not Create DbFactory in ExpanditDbFactory" + ex.Message, ex);
            }
        }

        public IDbConnection GetConnection()
        {
            DbConnection conn = _dbFactory.CreateConnection();

            if (conn.State == ConnectionState.Closed)
            {
                conn.ConnectionString = _strConn;
                conn.Open();

                if (conn is SqlConnection)
                {
                    using (DbCommand cmd = conn.CreateCommand())
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandText =
                            @"
                    SET ANSI_NULLS ON
                    SET ANSI_PADDING ON
                    SET ANSI_WARNINGS ON
                    SET ARITHABORT ON
                    SET CONCAT_NULL_YIELDS_NULL ON
                    SET QUOTED_IDENTIFIER ON
                    SET NUMERIC_ROUNDABORT OFF";
                        cmd.ExecuteNonQuery();
                    }
                }
            }

            return conn;
        }

        public void Finalize(IDbConnection connection)
        {
            if (connection != null)
            {
                Transaction transaction = Transaction.Current;
                if (null == transaction || transaction.TransactionInformation.Status != TransactionStatus.Active)
                {
                    connection.Close();
                    connection.Dispose();
                }
            }
        }

        public IDbDataAdapter GetDataAdapter(IDbConnection connection)
        {
            if (connection is ProfiledDbConnection)
            {
                if (((ProfiledDbConnection)connection).WrappedConnection is SqlConnection)
                {
                    return new ProfiledDbDataAdapter(new SqlDataAdapter());
                }
            }
            if (connection is SqlConnection)
            {
                return new SqlDataAdapter();
            }
            if (connection is OleDbConnection)
            {
                return new OleDbDataAdapter();
            }
            return _dbFactory.CreateDataAdapter();
        }
    }
}