﻿using System.Data;

namespace CmsPublic.DataRepository
{
    public interface IExpanditDbFactory
    {
        IDbConnection GetConnection();
        void Finalize(IDbConnection connection);
        IDbDataAdapter GetDataAdapter(IDbConnection connection);
    }
}