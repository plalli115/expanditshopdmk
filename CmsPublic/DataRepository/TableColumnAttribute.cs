﻿using System;

namespace CmsPublic.DataRepository {
	[AttributeUsage(AttributeTargets.Property)]
	public class TableColumnAttribute : Attribute
	{
		public string ColumnName { get; set; }
		public TableColumnMapping Mapping { get; set; }

		public bool MapForInsert
		{
			get
			{
				bool dontMap = (Mapping == TableColumnMapping.NeverMap || Mapping == TableColumnMapping.NotMapForInserts);
				return !dontMap;
			}
		}

		public bool MapForUpdate
		{
			get
			{
				bool dontMap = (Mapping == TableColumnMapping.NeverMap || Mapping == TableColumnMapping.NotMapForUpdates);
				return !dontMap;
			}
		}
	}
}
