﻿using System;

namespace CmsPublic.DataRepository
{
    [Flags]
    public enum TableColumnMapping
	{
		AlwaysMap = 0,
		NotMapForInserts = 1,
		NotMapForUpdates = 2,
		NeverMap = 3,
	}
}