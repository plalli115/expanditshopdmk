﻿using System.Collections.Generic;
using System.Data.Common;
using System.Transactions;
using StackExchange.Profiling;
using StackExchange.Profiling.Data;

namespace CmsPublic.DataRepository
{
    ///<summary>
    ///</summary>
    public class DbConnectionScope : DbProviderFactory
    {
        private readonly DbProviderFactory _realFactory;

        private static readonly Dictionary<Transaction, DbConnection> _transactionConnections =
            new Dictionary<Transaction, DbConnection>();
        
        ///<summary>
        ///</summary>
        ///<param name="factoryToScope"></param>
        public DbConnectionScope(DbProviderFactory factoryToScope)
        {
            _realFactory = factoryToScope;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override DbCommand CreateCommand()
        {
            var dbCommand = _realFactory.CreateCommand();
            return dbCommand != null ? new ProfiledDbCommand(dbCommand, dbCommand.Connection, MiniProfiler.Current) : null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override DbConnection CreateConnection()
        {
            Transaction transaction = Transaction.Current;
            // we're not currently in a transaction, so we just let the real factory work normally
            if (transaction == null)
            {
                var cnn = _realFactory.CreateConnection();
                return new ProfiledDbConnection(cnn, MiniProfiler.Current);
            }
            DbConnection connection;
            if (!_transactionConnections.TryGetValue(transaction, out connection))
            {
                // this is the first time this transaction requests a connection. 
                // Lets create it, wrapping it in a ScopedConnectionProxy which will 
                // ensure that any call to Close does not result in actually closing the connection 
                // (because we want to reuse the open connection throughout the entire transaction scope)
                connection = _realFactory.CreateConnection();
                connection = new ProfiledDbConnection(connection, MiniProfiler.Current);
                _transactionConnections.Add(transaction, connection);
                // we need to know when the transaction ends, so we can close the connection.
                transaction.TransactionCompleted += TransactionTransactionCompleted;
            }
            return connection;
        }

        private void TransactionTransactionCompleted(object sender, TransactionEventArgs e)
        {
            DbConnection connection;
            // transaction has ended, remove it from the cache and ensure it is disposed
            if (_transactionConnections.TryGetValue(e.Transaction, out connection))
            {
                _transactionConnections.Remove(e.Transaction);
                connection.Dispose();
            }
        }
    }
}