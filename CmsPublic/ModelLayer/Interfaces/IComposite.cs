﻿using System.Collections.Generic;

namespace CmsPublic.ModelLayer.Interfaces
{
    ///<summary>
    /// Generic Tree Structure Interface
    ///</summary>
    ///<typeparam name="T"></typeparam>
    public interface IComposite<T>
    {
        ///<summary>
        /// Get all children
        ///</summary>
        List<T> Children { get; }

        ///<summary>
        /// Get nearest parent node
        ///</summary>
        T Parent { get; set; }

        ///<summary>
        /// Add child
        ///</summary>
        ///<param name="c"></param>
        void Add(T c);

        ///<summary>
        /// Remove child
        ///</summary>
        ///<param name="c"></param>
        void Remove(T c);

        ///<summary>
        /// Remove children
        ///</summary>
        void RemoveChildren();

        ///<summary>
        /// Get child by index
        ///</summary>
        ///<param name="index"></param>
        ///<returns></returns>
        T GetChild(int index);

        ///<summary>
        /// Finds if children exists
        ///</summary>
        ///<returns></returns>
        bool HasChildren();
    }
}