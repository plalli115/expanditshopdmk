﻿using System.Configuration;
using System.Data;

namespace CmsPublic.ModelLayer.Interfaces
{
    ///<summary>
    ///</summary>
    public interface IColumnTypeData
    {
        ///<summary>
        ///</summary>
        string COLUMN_NAME { get; set; }

        ///<summary>
        ///</summary>
        int ORDINAL_POSITION { get; set; }

        ///<summary>
        ///</summary>
        SettingsPropertyValue PropertyValue { get; set; }

        ///<summary>
        ///</summary>
        object FieldValue { get; set; }

        ///<summary>
        ///</summary>
        SqlDbType DataType { get; set; }

        ///<summary>
        ///</summary>
        int ColumnSize { get; set; }
    }
}