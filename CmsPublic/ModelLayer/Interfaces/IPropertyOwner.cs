﻿using System.Collections.Generic;
using CmsPublic.DataRepository;
using CmsPublic.ModelLayer.Models;

namespace CmsPublic.ModelLayer.Interfaces
{
    ///<summary>
    ///</summary>
    public interface IPropertyOwner
    {
        ///<summary>
        /// Get ID
        ///</summary>
        [TableColumn(Mapping = TableColumnMapping.NeverMap)]
        string propertyIdentifier { get; }

        ///<summary>
        /// Catalog Manager Properties
        ///</summary>
        [TableColumn(Mapping = TableColumnMapping.NeverMap)]
        IPropValSet IProperties { get; set; }

        ///<summary>
        /// Get Property Type Code
        ///</summary>
        [TableColumn(Mapping = TableColumnMapping.NeverMap)]
        string propType { get; }

        ///<summary>
        /// Internal Property Collection
        ///</summary>
        [TableColumn(Mapping = TableColumnMapping.NeverMap)]
        List<PropValPropTransQuerySet> PropCollection { get; set; }
    }
}