﻿namespace CmsPublic.ModelLayer.Interfaces
{
    ///<summary>
    ///</summary>
    public interface IPropetyValue
    {
        ///<summary>
        ///</summary>
        ///<param name="propertyName"></param>
        ///<returns></returns>
        string GetPropertyValue(string propertyName);

        void SetPropertyValue(string propertyName, string propertyValue);
    }
}