﻿using System.Collections.Generic;

namespace CmsPublic.ModelLayer.Interfaces
{
    ///<summary>
    /// Property Type Enumeration
    ///</summary>
    public enum PropTypes
    {
        ///<summary>
        ///</summary>
        ATTACHMENT,

        ///<summary>
        ///</summary>
        TEXT,

        ///<summary>
        ///</summary>
        MEMO,

        ///<summary>
        ///</summary>
        HTML,

        ///<summary>
        ///</summary>
        LINK,

        ///<summary>
        ///</summary>
        PICTURE,

        ///<summary>
        ///</summary>
        SELECT,

        ///<summary>
        ///</summary>
        TEMPLATE,

        ///<summary>
        ///</summary>
        DATE,

        ///<summary>
        ///</summary>
        MULTIPICTURE,
        CHECKBOX,
        DEFAULT
    }

    ///<summary>
    ///</summary>
    public interface IPropValSet
    {
        ///<summary>
        /// Property Owners ID
        ///</summary>
        string parentGuid { get; set; }

        ///<summary>
        /// Gets the Property Owners Type Code
        ///</summary>
        string propOwnerTypeGuid { get; }

        ///<summary>
        /// Keeping the actual properties
        ///</summary>
        Dictionary<string, string> PropDict { get; set; }

        ///<summary>
        /// Keeping information about the property types
        ///</summary>
        Dictionary<string, string> PropTypeDict { get; set; }
    }
}