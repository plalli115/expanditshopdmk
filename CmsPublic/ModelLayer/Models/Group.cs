﻿using System;
using System.Collections.Generic;
using System.Globalization;
using CmsPublic.DataRepository;
using CmsPublic.ModelLayer.Interfaces;

namespace CmsPublic.ModelLayer.Models
{
    ///<summary>
    /// Contains Row Data from the GroupTable. Forms a Tree structure with data from multiple rows.
    ///</summary>
    public class Group : Composite<Group>, IPropertyOwner, IPropetyValue
    {
        private List<Group> _Children;

        ///<summary>
        /// CTOR
        ///</summary>
        public Group()
        {
            _Children = new List<Group>();
        }

        ///<summary>
        /// Maps database table column
        ///</summary>
        public new int GroupGuid { get; set; }

        ///<summary>
        /// Maps database table column
        ///</summary>
        public new int ParentGuid { get; set; }

        ///<summary>
        /// Maps database table column
        ///</summary>
        public new int SortIndex { get; set; }

        [TableColumn(Mapping = TableColumnMapping.NeverMap)]
        public string RouteUrl { get; set; }
        [TableColumn(Mapping = TableColumnMapping.NeverMap)]
        public string TemplateFileName { get; set; }
        [TableColumn(Mapping = TableColumnMapping.NeverMap)]
        public string TemplateController { get; set; }

        ///<summary>
        /// Gets the number of children
        ///</summary>
        [TableColumn(Mapping = TableColumnMapping.NeverMap)]
        public int ChildrenCount { get; set; }

        ///<summary>
        ///</summary>
        ///<exception cref="NotImplementedException"></exception>
        [TableColumn(Mapping = TableColumnMapping.NeverMap)]
        public DateTime? StartDate
        {
            get { return GetPropertyDate("STARTDATE"); }
            set { throw new NotImplementedException(); }
        }

        ////TODO: Figure out what culture info should be used for parsing/writing dates
        //private void SetPropertyDate(string propertyName, DateTime? value) {
        //    Properties.PropDict[propertyName] = value.HasValue ? value.Value.ToString(CultureInfo.InvariantCulture) : null;
        //}

        //TODO: Figure out what culture info should be used for parsing/writing dates

        ///<summary>
        ///</summary>
        ///<exception cref="NotImplementedException"></exception>
        [TableColumn(Mapping = TableColumnMapping.NeverMap)]
        public DateTime? EndDate
        {
            get { return GetPropertyDate("ENDDATE"); }
            set { throw new NotImplementedException(); }
        }

        ///<summary>
        /// Properties
        ///</summary>
        [TableColumn(Mapping = TableColumnMapping.NeverMap)]
        public GrpPropValSet Properties { get; set; }

        ///<summary>
        /// Adds the possibility to group properties together for a better CMS UI-experience
        ///</summary>
        [TableColumn(Mapping = TableColumnMapping.NeverMap)]
        public List<PropValPropTransGroup> PropertyGroup { get; set; }

        ///<summary>
        /// Get ID
        ///</summary>
        [TableColumn(Mapping = TableColumnMapping.NeverMap)]
        public int ID
        {
            get { return GroupGuid; }
        }

        #region IPropertyValue

        ///<summary>
        ///</summary>
        ///<param name="propertyName"></param>
        ///<returns></returns>
        public string GetPropertyValue(string propertyName)
        {
            if (Properties == null || Properties.PropDict == null)
            {
                return null;
            }
            return Properties.PropDict.ContainsKey(propertyName) ? Properties.PropDict[propertyName] : null;
        }

        public void SetPropertyValue(string propertyName, string propertyValue)
        {
            if (Properties.PropDict.ContainsKey(propertyName))
            {
                Properties.PropDict[propertyName] = propertyValue;
            }
        }

        #endregion

        #region IPropertyOwner Members

        ///<summary>
        /// Internal Property Collection
        ///</summary>
        [TableColumn(Mapping = TableColumnMapping.NeverMap)]
        public List<PropValPropTransQuerySet> PropCollection { get; set; }

        ///<summary>
        /// Get ID
        ///</summary>
        [TableColumn(Mapping = TableColumnMapping.NeverMap)]
        public string propertyIdentifier
        {
            get { return GroupGuid.ToString(); }
        }

        ///<summary>
        /// Catalog Manager Properties
        ///</summary>
        [TableColumn(Mapping = TableColumnMapping.NeverMap)]
        public IPropValSet IProperties
        {
            get { return Properties; }
            set { Properties = (GrpPropValSet) value; }
        }

        ///<summary>
        /// Get Property Type Code
        ///</summary>
        [TableColumn(Mapping = TableColumnMapping.NeverMap)]
        public string propType
        {
            get { return "GRP"; }
        }

        #endregion

        private DateTime? GetPropertyDate(string propertyName)
        {
            string startdate = Properties.PropDict[propertyName];
            DateTime date;
            if (DateTime.TryParse(startdate, CultureInfo.InvariantCulture, DateTimeStyles.AssumeUniversal, out date))
                return date;

            return null;
        }

        ///<summary>
        /// Searches the tree by Group ID
        ///</summary>
        ///<param name="group"></param>
        ///<param name="id"></param>
        ///<returns></returns>
        public Group Find(Group group, int id)
        {
            Group match = null;
            Find(group, id, ref match);
            return match;
        }

        /// <summary>
        /// Searches the tree by Group ID
        /// </summary>
        /// <param name="group"></param>
        /// <param name="id"></param>
        /// <param name="match"></param>
        protected void Find(Group group, int id, ref Group match)
        {
            foreach (Group child in group.Children)
            {
                if (child.ID == id)
                {
                    match = child;
                }
                if (child.HasChildren() && match == null)
                {
                    Find(child, id, ref match);
                }
            }
        }
    }
}