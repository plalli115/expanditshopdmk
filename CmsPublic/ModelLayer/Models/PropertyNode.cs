﻿namespace CmsPublic.ModelLayer.Models
{
    ///<summary>
    ///</summary>
    public class PropertyNode
    {
        ///<summary>
        ///</summary>
        ///<param name="name"></param>
        ///<param name="value"></param>
        ///<param name="isMulti"></param>
        public PropertyNode(string name, string value, bool isMulti)
        {
            Name = name;
            Value = value;
            IsMulti = isMulti;
        }

        ///<summary>
        ///</summary>
        public string Name { get; private set; }

        ///<summary>
        ///</summary>
        public string Value { get; private set; }

        ///<summary>
        ///</summary>
        public bool IsMulti { get; private set; }
    }
}