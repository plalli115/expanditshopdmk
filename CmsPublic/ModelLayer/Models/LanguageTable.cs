﻿using System;

namespace EISCS.Shop.DO.Dto
{
    [Serializable]
    public class LanguageTable
    {
        public string LanguageGuid { get; set; }
        public string LanguageName { get; set; }
    }
}
