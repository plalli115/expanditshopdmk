﻿using System;
using System.Collections.Generic;
using CmsPublic.DataRepository;
using CmsPublic.ModelLayer.Interfaces;

namespace CmsPublic.ModelLayer.Models
{
    ///<summary>
    ///</summary>
    public class ExpanditMailMessage : IPropertyOwner, IPropetyValue
    {
        ///<summary>
        /// Maps database table column
        ///</summary>
        public int MailMessageGuid { get; set; }

        ///<summary>
        /// Maps database table column
        ///</summary>
        public DateTime DateCreated { get; set; }

        /// <summary>
        /// Maps database table column
        /// </summary>
        public bool Active { get; set; }

        /// <summary>
        /// Maps database table column
        /// </summary>
        public string MailMessageName { get; set; }

        [TableColumn(Mapping = TableColumnMapping.NotMapForInserts)]
        public string ErrorMessage { get; set; }
        
        [TableColumn(Mapping = TableColumnMapping.NotMapForInserts)]
        public MailMessagePropValSet Properties { get; set; }

        ///<summary>
        /// Adds the possibility to group properties together for a better CMS UI-experience
        ///</summary>
        [TableColumn(Mapping = TableColumnMapping.NotMapForInserts)]
        public List<PropValPropTransGroup> PropertyGroup { get; set; }

        #region IPropertyOwner Members

        [TableColumn(Mapping = TableColumnMapping.NotMapForInserts)]
        public string propertyIdentifier
        {
            get { return MailMessageGuid.ToString(); }
        }

        [TableColumn(Mapping = TableColumnMapping.NotMapForInserts)]
        public IPropValSet IProperties
        {
            get { return Properties; }
            set { Properties = (MailMessagePropValSet) value; }
        }

        ///<summary>
        /// Get Property Type Code
        ///</summary>
        [TableColumn(Mapping = TableColumnMapping.NotMapForInserts)]
        public string propType
        {
            get { return "MAILMESSAGE"; }
        }

        ///<summary>
        /// Property Collection
        ///</summary>
        [TableColumn(Mapping = TableColumnMapping.NotMapForInserts)]
        public List<PropValPropTransQuerySet> PropCollection { get; set; }

        #endregion

        #region IPropertyValue

        public string GetPropertyValue(string propertyName)
        {
            return Properties.PropDict.ContainsKey(propertyName) ? Properties.PropDict[propertyName] : null;
        }

        public void SetPropertyValue(string propertyName, string propertyValue)
        {
            if (Properties.PropDict.ContainsKey(propertyName))
            {
                Properties.PropDict[propertyName] = propertyValue;
            }
        }

        #endregion
    }
}