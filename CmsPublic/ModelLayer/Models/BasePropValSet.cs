﻿using System.Collections.Generic;

namespace CmsPublic.ModelLayer.Models
{
    // Base Result Set Mapping Class
    ///<summary>
    ///</summary>
    public class BasePropValSet
    {
        ///<summary>
        /// Keeping the actual properties
        ///</summary>
        public Dictionary<string, string> PropDict { get; set; }

        ///<summary>
        /// Keeping information about the property types
        ///</summary>
        public Dictionary<string, string> PropTypeDict { get; set; }

        public string parentGuid { get; set; }
    }
}