﻿using CmsPublic.ModelLayer.Interfaces;

namespace CmsPublic.ModelLayer.Models
{
    ///<summary>
    /// Result Set Mapping Class
    ///</summary>
    public class PrdPropValSet : BasePropValSet, IPropValSet
    {
        #region IPropValSet Members

        ///<summary>
        /// Gets the Property Owners Type Code
        ///</summary>
        public virtual string propOwnerTypeGuid
        {
            get { return "PRD"; }
        }

        #endregion
    }
}