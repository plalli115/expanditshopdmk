﻿using System.Collections.Generic;
using CmsPublic.DataRepository;
using CmsPublic.ModelLayer.Interfaces;
using DataFieldDefinition;

namespace CmsPublic.ModelLayer.Models
{
    ///<summary>
    /// Implements Tree structure
    ///</summary>
    ///<typeparam name="T"></typeparam>
    public class Composite<T> : GroupTableColumns, IComposite<T>
    {
        ///<summary>
        /// CTOR
        ///</summary>
        public Composite()
        {
            Children = new List<T>();
        }

        ///<summary>
        /// Keeps track of removed children
        ///</summary>
        [TableColumn(Mapping = TableColumnMapping.NeverMap)]
        public bool HadChildren { get; set; }

        #region IComposite<T> Members

        ///<summary>
        /// Add child
        ///</summary>
        ///<param name="c"></param>
        public void Add(T c)
        {
            Children.Add(c);
        }

        /// <summary>
        /// Remove child
        /// </summary>
        /// <param name="c"></param>
        public void Remove(T c)
        {
            Children.Remove(c);
        }

        ///<summary>
        /// Remove children
        ///</summary>
        public void RemoveChildren()
        {
            if (Children == null) return;
            Children = null;
            HadChildren = true;
        }

        /// <summary>
        /// Get child by index
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public T GetChild(int index)
        {
            return Children[index];
        }

        /// <summary>
        /// Get all children
        /// </summary>
        [TableColumn(Mapping = TableColumnMapping.NeverMap)]
        public List<T> Children { get; private set; }

        /// <summary>
        /// Get nearest parent node
        /// </summary>
        [TableColumn(Mapping = TableColumnMapping.NeverMap)]
        public T Parent { get; set; }

        /// <summary>
        /// Finds if children exists
        /// </summary>
        /// <returns></returns>
        public bool HasChildren()
        {
            return Children != null && Children.Count > 0;
        }

        #endregion
    }
}