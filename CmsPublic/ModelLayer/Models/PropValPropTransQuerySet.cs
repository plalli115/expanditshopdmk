﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace CmsPublic.ModelLayer.Models
{
    ///<summary>
    ///</summary>
    public class PropEquality : IEqualityComparer<PropValPropTransQuerySet>
    {
        #region IEqualityComparer<PropValPropTransQuerySet> Members

        /// <summary>
        /// Determines whether the specified objects are equal.
        /// </summary>
        /// <returns>
        /// true if the specified objects are equal; otherwise, false.
        /// </returns>
        /// <param name="x">The first object of type <paramref name="x"/> to compare.
        ///                 </param><param name="y">The second object of type <paramref name="y"/> to compare.
        ///                 </param>
        public bool Equals(PropValPropTransQuerySet x, PropValPropTransQuerySet y)
        {
            return (x.PropGuid.Equals(y.PropGuid));
        }

        /// <summary>
        /// Returns a hash code for the specified object.
        /// </summary>
        /// <returns>
        /// A hash code for the specified object.
        /// </returns>
        /// <param name="obj">The <see cref="T:System.Object"/> for which a hash code is to be returned.
        ///                 </param><exception cref="T:System.ArgumentNullException">The type of <paramref name="obj"/> is a reference type and <paramref name="obj"/> is null.
        ///                 </exception>
        public int GetHashCode(PropValPropTransQuerySet obj)
        {
            int hashValue = 0;
            PropertyInfo[] pInfs = obj.GetType().GetProperties();
            foreach (PropertyInfo pInf in pInfs)
            {
                try
                {
                    hashValue = hashValue ^ pInf.GetHashCode();
                }
                catch (Exception)
                {
                }
            }
            return hashValue;
        }

        #endregion
    }

    ///<summary>
    ///</summary>
    public class PropValPropTransQuerySet : IComparable<PropValPropTransQuerySet>
    {
        // From PropTable
        ///<summary>
        ///</summary>
        public string PropName { get; set; }

        ///<summary>
        ///</summary>
        public string PropTypeGuid { get; set; }

        // From PropVal and PropTrans
        ///<summary>
        ///</summary>
        public string PropOwnerRefGuid { get; set; }

        ///<summary>
        ///</summary>
        public string PropGuid { get; set; }

        ///<summary>
        ///</summary>
        public int PropMultilanguage { get; set; }

        ///<summary>
        ///</summary>
        public string PropLangGuid { get; set; }

        ///<summary>
        ///</summary>
        public string PropTransText { get; set; }

        ///<summary>
        ///</summary>
        public string PropValGuid { get; set; }

        ///<summary>
        ///</summary>
        public string PropTransGuid { get; set; }

        // from PropGrpRel
        ///<summary>
        ///</summary>
        public int SortIndex { get; set; }

        #region IComparable<PropValPropTransQuerySet> Members

        /// <summary>
        /// Compares the current object with another object of the same type.
        /// </summary>
        /// <returns>
        /// A 32-bit signed integer that indicates the relative order of the objects being compared. The return value has the following meanings: 
        ///                     Value 
        ///                     Meaning 
        ///                     Less than zero 
        ///                     This object is less than the <paramref name="other"/> parameter.
        ///                     Zero 
        ///                     This object is equal to <paramref name="other"/>. 
        ///                     Greater than zero 
        ///                     This object is greater than <paramref name="other"/>. 
        /// </returns>
        /// <param name="other">An object to compare with this object.
        ///                 </param>
        public int CompareTo(PropValPropTransQuerySet other)
        {
            if (SortIndex < other.SortIndex)
            {
                return -1;
            }
            return SortIndex == other.SortIndex ? 0 : 1;
        }

        #endregion
    }
}