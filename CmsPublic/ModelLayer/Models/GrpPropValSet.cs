﻿using CmsPublic.ModelLayer.Interfaces;

namespace CmsPublic.ModelLayer.Models
{
    ///<summary>
    /// Result Set Mapping Class
    ///</summary>
    public class GrpPropValSet : BasePropValSet, IPropValSet
    {
        #region IPropValSet Members

        ///<summary>
        /// Property Owners ID
        ///</summary>

        ///<summary>
        /// Gets the Property Owners Type Code
        ///</summary>
        public virtual string propOwnerTypeGuid
        {
            get { return "GRP"; }
        }

        #endregion
    }
}