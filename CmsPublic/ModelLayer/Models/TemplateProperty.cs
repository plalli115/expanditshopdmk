﻿namespace CmsPublic.ModelLayer.Models
{
    ///<summary>
    ///</summary>
    public class TemplateProperty
    {
        ///<summary>
        ///</summary>
        public string TemplateGuid { get; set; }

        ///<summary>
        ///</summary>
        public string PropGuid { get; set; }

        ///<summary>
        ///</summary>
        public string PropGroupGuid { get; set; }

        ///<summary>
        ///</summary>
        public string PropHeader { get; set; }
    }
}