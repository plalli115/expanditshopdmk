﻿using System.Collections.Generic;
using CmsPublic.DataRepository;
using CmsPublic.ModelLayer.Interfaces;
using DataFieldDefinition;

namespace CmsPublic.ModelLayer.Models
{
    /// <summary>
    /// Contains Row Data from the GroupProduct Table
    /// </summary>
    public class GroupProduct : GroupProductColumns, IPropertyOwner, IPropetyValue
    {
        ///<summary>
        /// Maps database table column
        ///</summary>
        
        public new int GroupProductGuid { get; set; }

        ///<summary>
        /// Maps database table column
        ///</summary>
        public new int GroupGuid { get; set; }

        ///<summary>
        /// Maps database table column
        ///</summary>
        public new string ProductGuid { get; set; }

        ///<summary>
        /// Maps database table column
        ///</summary>
        public new int SortIndex { get; set; }

        /// <summary>
        /// Holds the Product name
        /// </summary>
        [TableColumn(Mapping = TableColumnMapping.NeverMap)]
        public string ProductName { get; set; }

        ///<summary>
        /// Properties
        ///</summary>
        [TableColumn(Mapping = TableColumnMapping.NeverMap)]
        public PrdPropValSet Properties { get; set; }

        #region IPropertyOwner Members

        ///<summary>
        /// Internal Property Collection
        ///</summary>
        [TableColumn(Mapping = TableColumnMapping.NeverMap)]
        public List<PropValPropTransQuerySet> PropCollection { get; set; }

        ///<summary>
        /// Get ID
        ///</summary>
        [TableColumn(Mapping = TableColumnMapping.NeverMap)]
        public string propertyIdentifier
        {
            get { return ProductGuid; }
        }

        ///<summary>
        /// Catalog Manager Properties
        ///</summary>
        [TableColumn(Mapping = TableColumnMapping.NeverMap)]
        public IPropValSet IProperties
        {
            get { return Properties; }
            set { Properties = (PrdPropValSet) value; }
        }

        ///<summary>
        /// Get Property Type Code
        ///</summary>
        [TableColumn(Mapping = TableColumnMapping.NeverMap)]
        public string propType
        {
            get { return "PRD"; }
        }

        #endregion

        #region IPropertyValue

        ///<summary>
        ///</summary>
        ///<param name="propertyName"></param>
        ///<returns></returns>
        public string GetPropertyValue(string propertyName)
        {
            return Properties.PropDict.ContainsKey(propertyName) ? Properties.PropDict[propertyName] : null;
        }

        public void SetPropertyValue(string propertyName, string propertyValue)
        {
            if (Properties.PropDict.ContainsKey(propertyName))
            {
                Properties.PropDict[propertyName] = propertyValue;
            }
        }

        #endregion
    }
}