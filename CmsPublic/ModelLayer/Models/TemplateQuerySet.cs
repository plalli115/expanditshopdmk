﻿using System.Collections.Generic;

namespace CmsPublic.ModelLayer.Models
{
    ///<summary>
    ///</summary>
    public class TemplateQuerySet
    {
        public TemplateQuerySet()
        {
            TemplatePropertyEntries = new List<TemplatePropertyEntry>();
        }

        public TemplateQuerySet(List<TemplatePropertyEntry> templatePropertyEntries)
        {
            TemplatePropertyEntries = templatePropertyEntries;
        }

        ///<summary>
        ///</summary>
        public string TemplateGuid { get; set; }

        ///<summary>
        ///</summary>
        public string TemplateName { get; set; }

        ///<summary>
        ///</summary>
        public string TemplateFileName { get; set; }

        ///<summary>
        ///</summary>
        public string TemplateDescription { get; set; }

        ///<summary>
        ///</summary>
        public bool IsGroupTemplate { get; set; }

        ///<summary>
        ///</summary>
        public bool IsProductTemplate { get; set; }

        public string TemplateController { get; set; }
        public string Templatedescription { get; set; }
        public List<TemplatePropertyEntry> TemplatePropertyEntries { get; private set; }
    }

    public class TemplatePropertyEntry
    {
        public string TemplateGuid { get; set; }
        public string PropGuid { get; set; }
        public string PropGroupGuid { get; set; }
        public string Propheader { get; set; }
    }
}