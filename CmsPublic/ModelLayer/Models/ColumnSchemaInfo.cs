﻿using System;
using System.Configuration;
using System.Data;
using CmsPublic.ModelLayer.Interfaces;

namespace CmsPublic.ModelLayer.Models
{
    ///<summary>
    ///</summary>
    public class ColumnSchemaInfo : IColumnTypeData, IEquatable<ColumnSchemaInfo>
    {
        private const string SLegalChars = "_@#$";

        #region IColumnTypeData Members

        ///<summary>
        ///</summary>
        public string COLUMN_NAME { get; set; }

        ///<summary>
        ///</summary>
        public int ORDINAL_POSITION { get; set; }

        ///<summary>
        ///</summary>
        public SettingsPropertyValue PropertyValue { get; set; }

        ///<summary>
        ///</summary>
        public object FieldValue { get; set; }

        ///<summary>
        ///</summary>
        public SqlDbType DataType { get; set; }

        ///<summary>
        ///</summary>
        public int ColumnSize { get; set; }

        #endregion

        // Default Constructor MUST be defined

        #region IEquatable<ColumnSchemaInfo> Members

        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <returns>
        /// true if the current object is equal to the <paramref name="other"/> parameter; otherwise, false.
        /// </returns>
        /// <param name="other">An object to compare with this object.
        ///                 </param>
        public bool Equals(ColumnSchemaInfo other)
        {
            return COLUMN_NAME.Equals(other.COLUMN_NAME);
        }

        #endregion

        private void EnsureValidTableOrColumnName(string name)
        {
            for (int i = 0; i < name.Length; ++i)
            {
                if (!Char.IsLetterOrDigit(name[i]) && SLegalChars.IndexOf(name[i]) == -1)
                    throw new Exception("Table and column names cannot contain: " + name[i]);
            }
        }

        /// <summary>
        /// Serves as a hash function for a particular type. 
        /// </summary>
        /// <returns>
        /// A hash code for the current <see cref="T:System.Object"/>.
        /// </returns>
        /// <filterpriority>2</filterpriority>
        public override int GetHashCode()
        {
            return -1;
        }
    }
}