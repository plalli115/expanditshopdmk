﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CmsPublic.ModelLayer.Models
{
    public class MailMessageProduct
    {
        public string MailMessageGuid { get; set; }
        public string ProductGuid { get; set; }
    }
}
