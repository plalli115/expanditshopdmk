﻿using System.Collections.Generic;
using CmsPublic.ModelLayer.Interfaces;
using DataFieldDefinition;

namespace CmsPublic.ModelLayer.Models
{
    ///<summary>
    /// Contains Row Data from the ProductTable
    ///</summary>
    public class Product : ProductTableColumns, IPropertyOwner, IPropetyValue
    {
        private PrdPropValSet _properties;

        ///<summary>
        /// Maps database table column
        ///</summary>
        public new string ProductGuid { get; set; }

        ///<summary>
        /// Maps database table column
        ///</summary>
        public new string ProductGrp { get; set; }
        public int GroupProductGuid { get; set; }

        ///<summary>
        /// Maps database table column
        ///</summary>
        public new string ProductName { get; set; }
        public string ProductNameTranslate { get; set; }

        public new double ListPrice { get; set; }

        public bool IsUsedInAnyGroup { get; set; }
        public bool IsFavorite { get; set; }

        public string EditButtonHtml { get; set; }
        public string PictureHtml { get; set; }
        public string CheckboxHtml { get; set; }
        /// <summary>
        /// Properties
        /// </summary>
        public PrdPropValSet Properties
        {
            get { return _properties; }
            set { _properties = value; }
        }

        ///<summary>
        /// Adds the possibility to group properties together for a better CMS UI-experience
        ///</summary>
        public List<PropValPropTransGroup> PropertyGroup { get; set; }

        #region IPropertyOwner Members

        ///<summary>
        /// Internal Property Collection
        ///</summary>
        public List<PropValPropTransQuerySet> PropCollection { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string propertyIdentifier
        {
            get { return ProductGuid; }
        }

        ///<summary>
        /// Catalog Manager Properties
        ///</summary>
        public IPropValSet IProperties
        {
            get { return _properties; }
            set { _properties = (PrdPropValSet) value; }
        }


        ///<summary>
        /// Get Property Type Code
        ///</summary>
        public string propType
        {
            get { return "PRD"; }
        }

        #endregion

        #region IPropertyValue

        ///<summary>
        ///</summary>
        ///<param name="propertyName"></param>
        ///<returns></returns>
        public string GetPropertyValue(string propertyName)
        {
            return Properties.PropDict.ContainsKey(propertyName) ? Properties.PropDict[propertyName] : null;
        }

        public void SetPropertyValue(string propertyName, string propertyValue)
        {
            if (Properties.PropDict.ContainsKey(propertyName))
            {
                Properties.PropDict[propertyName] = propertyValue;
            }
        }

        #endregion
    }
}