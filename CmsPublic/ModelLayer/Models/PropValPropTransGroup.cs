﻿using System.Collections.Generic;

namespace CmsPublic.ModelLayer.Models
{
    ///<summary>
    ///</summary>
    public class PropValPropTransGroup
    {
        ///<summary>
        ///</summary>
        public PropValPropTransGroup()
        {
            PropertyGroups = new List<PropValPropTransQuerySet>();
            Propheader = "General";
        }

        ///<summary>
        ///</summary>
        public List<PropValPropTransQuerySet> PropertyGroups { get; set; }

        ///<summary>
        ///</summary>
        public string Propheader { get; set; }
    }
}