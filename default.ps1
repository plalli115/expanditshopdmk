properties {
$base_dir = Resolve-Path .
$build_dir = "$base_dir\build"
$sln_file = "Shop.sln"
$debug_dir = "$build_dir\Debug\\"
$release_dir = "$build_dir\Release\\";
$nunit_dir = "$base_dir\packages\NUnit.Runners.2.6.1\tools\"
$nunit_console = "$nunit_dir\nunit-console.exe"
$testAssemblies = (Get-ChildItem "$base_dir" -Recurse -Include *Test*.dll -Name | Select-String "bin")
$output_dir ="build\Debug\_PublishedWebsites\Shop"
$git_branch = if ("$env:BRANCHNAME".length -gt 0) { "$env:BRANCHNAME" } else { "unknown" }
$deploy_dir = "\\dev-web-1\c$\inetpub\wwwroot\StandardShop\$git_branch"
}

<# $git_branch = if ("$env:BRANCHNAME".length -gt 0) { "$env:BRANCHNAME" } else { "unknown" } #>
<# $deploy_dir = "\\dev-web-1\c$\inetpub\wwwroot\StandardShop\$git_branch" #>

Task default -Depends Build

Task Build{
msbuild $sln_file "/nologo" "/t:Build" "/p:Configuration=Debug" "/p:OutDir=""$debug_dir"""
}

Task Debug -depends Init {
msbuild $sln_file "/nologo" "/t:Rebuild" "/p:Configuration=Debug" "/p:OutDir=""$debug_dir"""
}

Task Release -depends Init {
msbuild $sln_file "/nologo" "/t:Rebuild" "/p:Configuration=Release" "/p:OutDir=""$release_dir"""
}

Task Clean {
remove-item -force -recurse $debug_dir -ErrorAction SilentlyContinue
remove-item -force -recurse $release_dir -ErrorAction SilentlyContinue
}

Task Init -depends Clean {
new-item $debug_dir -itemType directory
new-item $release_dir -itemType directory
}

Task DeployToIntegration {
	if($deploy_dir.Contains("<default>")) {
		$deploy_dir = "\\dev-web-1\c$\inetpub\wwwroot\StandardShop\master"		
	}	
	IF (test-path $deploy_dir){
    Write-Host "Deleting - $deploy_dir"
    remove-item $deploy_dir\* -force -recurse -erroraction SilentlyContinue
  } else {
    mkdir $deploy_dir
  }  
  cp -path shop\* -destination $deploy_dir\ -force -recurse
    
}

Task InstallNuGet {
    
	 tools\nuget\nuget.exe install .\ShopTests\packages.config -o .\packages
}
