﻿function markAndUnMarkFavorites( urlUnmark, urlMark, delelines) {
    $('.fa-star, .fa-star-o').on('click', function () {
        var element = $(this);
        var productGuid = element.data("productguid");
        var markAsFaverite = element.hasClass('fa-star-o');

        var ajaxUrl = urlUnmark + productGuid;
        if (markAsFaverite) {
            ajaxUrl = urlMark + productGuid; 
        }

        $.ajax({
            url: (ajaxUrl),
            type: 'get',
            cache: false,
            dataType: 'json',
            //beforeSend: function () {
            //    toggleElement("CustomerLoadingIcon", true);
            //},
            success: function (json) {
                if (markAsFaverite) {
                    element.removeClass('fa-star-o');
                    element.addClass('fa-star');
                } else {
                    if (!delelines) {
                        element.removeClass('fa-star');
                        element.addClass('fa-star-o');
                    } else {
                        var lineToRemove = $("li[data-productguid='" + productGuid + "'], tr[data-productguid='" + productGuid + "']");
                        lineToRemove.fadeOut("slow", function () {
                            lineToRemove.remove();
                        });
                    }

                }
            },
            error: function (xmlHttpRequest, textStatus, errorThrown) {
                console.log('Some error occoured', textStatus, errorThrown);
            }
        });
    });

}