﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ManageDependencyCache.aspx.cs"
    Inherits="admin_CacheMan_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table>
            <tr>
                <td>
                    <asp:Button runat="server" ID="DeleteDepCache" Text="Remove" 
                        onclick="DeleteDepCache_Click" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button runat="server" ID="CreateDepCache" Text="Create" 
                        onclick="CreateDepCache_Click" />
                </td>
            </tr>            
        </table>
    </div>
    <div>
        <br />
        <asp:Literal runat="server" ID="QueryResponse" />
    </div>
    </form>
</body>
</html>
