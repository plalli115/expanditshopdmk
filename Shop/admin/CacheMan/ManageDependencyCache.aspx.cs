﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EISCS.ExpandITFramework.Infrastructure.SqlDependencyCaching;

public partial class admin_CacheMan_Default : System.Web.UI.Page {

    private SqlCacheDependencyDatabaseInfrastructureManager _manager;

    protected void Page_Load(object sender, EventArgs e) {
        if (_manager == null) {
            _manager = new SqlCacheDependencyDatabaseInfrastructureManager();
        }
    }
    protected void DeleteDepCache_Click(object sender, EventArgs e) {
        string result = _manager.DeleteSqlCacheDependencyStructure();
        if (!string.IsNullOrEmpty(result)) {
            QueryResponse.Text = result;
        }
    }
    protected void CreateDepCache_Click(object sender, EventArgs e) {
        bool result = _manager.CreateSqlCacheDependencyStructure();
        QueryResponse.Text = result.ToString();
    }    
}