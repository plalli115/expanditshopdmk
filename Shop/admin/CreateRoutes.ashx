﻿<%@ WebHandler Language="VB" Class="CreateRoutes" %>

Imports System
Imports System.Web
Imports EISCS.Routing.Providers

Public Class CreateRoutes : Implements IHttpHandler
    
    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        
        context.Response.ContentType = "text/plain"
        context.Response.AppendHeader("Access-Control-Allow-Origin", "*")
        
        Dim path As String = HttpContext.Current.Server.MapPath("~/expanditremote.xml")
        If System.IO.File.Exists(path) Then
            context.Response.Write("Invalid action")
            Return
        End If
        
        Dim result As String = "OK " & vbCrLf
        Try
            Dim iResult As Integer = RouteManager.Route.PublicRouteDataProvider.RecreateAll()
            If iResult <= 0 Then
                result = "No records updated"
            Else
                result = result & "Updated " & iResult & " records"
            End If
        Catch ex As Exception
            result = "Update failed " & ex.Message
        End Try
        context.Response.Write(result)
    End Sub
 
    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class