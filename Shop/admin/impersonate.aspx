﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Impersonate.aspx.cs" Inherits="admin.Impersonate" %>

<div class="ImpersonatePage">

    <h2>
        <asp:literal id="headerText" runat="server" text="<%$ Resources: Language, LABEL_IMPERSONATION %>"></asp:literal>
    </h2>
    <p>
        <asp:literal id="litMessage" runat="server" text="<%$ Resources: Language, LABEL_MESSAGE %>"></asp:literal>
    </p>
</div>
