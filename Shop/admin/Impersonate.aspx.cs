﻿using System;
using FileIO;

namespace admin
{
    public partial class Impersonate : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Redirect("~/Impersonation/Index" + "?" + Request.QueryString);
        }
    }
}