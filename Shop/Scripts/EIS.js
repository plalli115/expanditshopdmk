﻿$(document).ready(function () {
    $('body').tooltip({
        selector: '.edit-btn, .delete-btn, .create-btn, .settings-btn',
        placement: 'bottom',
        title: function () {
            var elem = $(this);
            if (elem.hasClass('edit-btn'))
                return EIS.Translate.getTranslation("SO_TOOLTIP_EDIT");
            if (elem.hasClass('delete-btn'))
                return EIS.Translate.getTranslation("SO_TOOLTIP_DELETE");
            if (elem.hasClass('create-btn'))
                return EIS.Translate.getTranslation("SO_TOOLTIP_CREATE");
            if (elem.hasClass('settings-btn'))
                return EIS.Translate.getTranslation("SO_TOOLTIP_SETTINGS");
            return null;
        }
    });
});

// Common functions
/*** Handle jQuery plugin naming conflict between jQuery UI and Bootstrap ***/
$.widget.bridge('uibutton', $.ui.button);
$.widget.bridge('uitooltip', $.ui.tooltip);

function checkCookie() {
    var cookie = getCookie("browserNotSupported");
    if (cookie == "") {
        $("#browserAdvise").removeClass("hidden");
    }
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
    }
    return "";
}

var EIS = {
    Translate: {
        init: function (options) {
            this._translationsUrl = options.translationsUrl;
            this._fetchTranslation(options.forceReload);
        },
        getTranslation: function (key) {
            return this._translations[key];
        },
        _translations: {},
        _translationsUrl: '',
        _fetchTranslation: function (forceReload) {
            var me = this;
            if (!forceReload && $.sessionStorage != '') {
                if ($.sessionStorage.keys().length > 0) {
                    this._translations = $.sessionStorage.get($.sessionStorage.keys());
                    return;
                }
            }
            $.ajax({
                url: me._translationsUrl,
                type: 'post',
                cache: false,
                dataType: 'json',
                success: function (json) {
                    me._translations = json;
                    if ($.sessionStorage != '')
                        $.sessionStorage.set(json);
                },
                error: function (xmlHttpRequest, textStatus, errorThrown) {
                    console.log('Some error occoured', textStatus, errorThrown);
                }
            });
        }
    }
};

var dotNETDate = {
    toFullString: function (data) {
        if (data == null) return null;
        if (data instanceof Date) return data;
        var r = /\/Date\((-?[0-9]+)\)\//i;
        var matches = data.match(r);
        if (matches.length == 2) {
            if (matches[1] == -62135596800000) {
                return "";
            }
            return new Date(parseInt(matches[1])).toLocaleString();
        }
        else {
            return data;
        }
    },
    toDateString: function (data) {
        if (data == null) return null;
        if (data instanceof Date) return data;
        var r = /\/Date\((-?[0-9]+)\)\//i;
        var matches = data.match(r);
        if (matches.length == 2) {
            if (matches[1] == -62135596800000) {
                return "";
            }
            return new Date(parseInt(matches[1])).toLocaleDateString();
        }
        else {
            return data;
        }
    }
}

function convertNETDateToString(data) {
    if (data == null) return null;
    if (data instanceof Date) return data;
    var r = /\/Date\((-?[0-9]+)\)\//i;
    var matches = data.match(r);
    if (matches.length == 2) {
        if (matches[1] == -62135596800000) {
            return "";
        }
        return new Date(parseInt(matches[1])).toLocaleString();
    }
    else {
        return data;
    }
}

function convertNETDateToDateString(data) {
    if (data == null) return null;
    if (data instanceof Date) return data;
    var r = /\/Date\((-?[0-9]+)\)\//i;
    var matches = data.match(r);
    if (matches.length == 2) {
        if (matches[1] == -62135596800000) {
            return "";
        }
        return new Date(parseInt(matches[1])).toLocaleDateString();
    }
    else {
        return data;
    }
}