Imports System.Configuration.ConfigurationManager
Imports ExpanditLib2
Imports EISCS.Routing.Providers
Imports EISCS.ExpandITFramework.Util

Partial Class Link
    Inherits ExpandIT.Page

    Protected Sub RedirectToPath(ByVal path As String)
        Response.Redirect(path)
    End Sub

    Protected Overrides Sub OnPreInit(ByVal e As System.EventArgs)
        MyBase.OnPreInit(e)
        'RequireSsl = True
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Dim productGuid As String
        Dim linkurl As String
        Dim groupGuid As Integer

        productGuid = HttpContext.Current.Request("ProductGuid")
        groupGuid = CLngEx(HttpContext.Current.Request("GroupGuid"))

        Dim languageGuid As String = HttpContext.Current.Request("LanguageGuid")

        If (String.IsNullOrEmpty(languageGuid)) Then
            languageGuid = AppSettings("LANGUAGE_DEFAULT")
        End If

        SetCookieLanguage(languageGuid)

        If (Not String.IsNullOrEmpty(productGuid)) Then
            linkurl = RouteManager.Route.CreateTemplateProductLink(productGuid, groupGuid, languageGuid)
            RedirectToPath(linkurl)
            Return
        End If

        If groupGuid <> 0 Then
            linkurl = RouteManager.Route.CreateTemplateGroupLink(groupGuid, languageGuid)
            RedirectToPath(linkurl)
            Return
        End If
    End Sub

    Sub SetCookieLanguage(ByVal languageGuid As String)
        If Not String.IsNullOrEmpty(languageGuid) Then

            Dim cookieValue As String = Request("LanguageGuid")
            Dim unprotectedBytes As Byte() = Encoding.UTF8.GetBytes(cookieValue)
            Dim encodedCookieValue As String = MachineKey.Encode(unprotectedBytes, MachineKeyProtection.All)

            Response.Cookies("user")("LanguageGuid") = encodedCookieValue
            Response.Cookies("user").Path = HttpContext.Current.Server.UrlPathEncode(AppUtil.GetVirtualRoot()) & "/"
        End If
    End Sub

End Class
