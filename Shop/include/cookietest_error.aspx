<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Page Language="VB" AutoEventWireup="false" CodeFile="cookietest_error.aspx.vb" Inherits="include_cookietest_error" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title><% = LABELCOOKIETESTFAILED1 %></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <%  If Request.QueryString("RetUrl") <> "" Then RetURL = ExpandIT.ExpandITLib.SafeRetUrl(Request.QueryString("RetUrl"))%>
        <br /><h5><% = LABELCOOKIETESTFAILED1 %>.</h5><br />
        <% = LABELCOOKIETESTFAILED2 %>.<br />
        <% = LABELCOOKIETESTFAILED3 %>&nbsp;<a href="<% = RetUrl %>"><% = LABELPLEASELOGINIFACCOUNTEXISTS2 %></a>.

    </div>
    </form>
</body>
</html>
