Imports System.Configuration.ConfigurationManager

Partial Class include_cookietest_error
    Inherits Web.UI.Page

    Protected DefaultLanguageGuid As String
    Protected LABELCOOKIETESTFAILED1, LABELCOOKIETESTFAILED2, LABELCOOKIETESTFAILED3, LABELPLEASELOGINIFACCOUNTEXISTS2 As String
    Protected RetURL As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ' Get Default Language for the website.
        DefaultLanguageGuid = AppSettings("LANGUAGE_DEFAULT")

        ' Calculating the correct translations.
        ' All the variable names are from Language Tool and the original are:
        ' LABEL_COOKIE_TEST_FAILED_1, LABEL_COOKIE_TEST_FAILED_2, LABEL_COOKIE_TEST_FAILED_3 and LABEL_PLEASE_LOGIN_IF_ACCOUNT_EXISTS_2
        Select Case DefaultLanguageGuid
            Case "DA"
                LABELCOOKIETESTFAILED1 = "Din web browser tillader ikke dette web site at bruge cookies"
                LABELCOOKIETESTFAILED2 = "Web sitet vil ikke fungere korrekt f&#248;r browseren tillader cookies"
                LABELCOOKIETESTFAILED3 = "S&#230;t venligst din browser til at acceptere cookies, og klik"
                LABELPLEASELOGINIFACCOUNTEXISTS2 = "her"
            Case "DE"
                LABELCOOKIETESTFAILED1 = "Ihrem Web-Browser ist es nicht erlaubt diese Webseitemit cookies zu nutzen"
                LABELCOOKIETESTFAILED2 = "Die Web-Seite wird nicht funktionieren bevor cookies erlaubt sind"
                LABELCOOKIETESTFAILED3 = "Bitte rm&#246;glichen Sie cookies in Ihrem Browser und klick"
                LABELPLEASELOGINIFACCOUNTEXISTS2 = "hier"
            Case "PT"
                LABELCOOKIETESTFAILED1 = "O seu browser n&#227;o autoriza o uso de cookies para este web site"
                LABELCOOKIETESTFAILED2 = "O web site n&#227;o ir&#225; funcionar correctamente se n&#227;o autorizar o uso de cookies"
                LABELCOOKIETESTFAILED3 = "Por favor, active as cookies no seu seu browser e clique"
                LABELPLEASELOGINIFACCOUNTEXISTS2 = "aqui"
            Case "ES"
                LABELCOOKIETESTFAILED1 = "Su navegador web no permite usar cookies a este sitio web"
                LABELCOOKIETESTFAILED2 = "Este sitio web no funcionar&#225; correctamente mientras no permita el uso de cookies"
                LABELCOOKIETESTFAILED3 = "Por favor, active cookies en su navegador y pinche"
                LABELPLEASELOGINIFACCOUNTEXISTS2 = "aqu&#237;"
            Case Else
                LABELCOOKIETESTFAILED1 = "Your Web Browser does not allow this website to use cookies"
                LABELCOOKIETESTFAILED2 = "The website will not work properly before cookies are allowed"
                LABELCOOKIETESTFAILED3 = "Please enable cookies in your browser, and click"
                LABELPLEASELOGINIFACCOUNTEXISTS2 = "here"
        End Select

    End Sub
End Class
