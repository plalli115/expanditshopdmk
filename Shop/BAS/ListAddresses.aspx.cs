﻿using System;
using System.Configuration;
using EISCS.Shop.DO.BAS;
using EISCS.Shop.DO.BAS.Repository;
using ExpandIT;

namespace BAS
{
    public partial class ListAddresses : Page
    {
        private readonly CustomerAddressRepository _customerAddressRepository;

        public ListAddresses()
        {
            _customerAddressRepository =
                new CustomerAddressRepository(
                    ConfigurationManager.ConnectionStrings["ExpandITBASConnectionString"].ConnectionString);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                lvAddresses.DataSource = _customerAddressRepository.All();
                lvAddresses.DataBind();
            }
        }
    }
}