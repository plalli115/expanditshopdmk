﻿using System;
using System.Configuration;
using EISCS.Shop.DO.BAS;
using EISCS.Shop.DO.BAS.Repository;

namespace BAS
{
    public partial class ListItems : ExpandIT.Page
    {
        private readonly ServiceItemRepository _serviceItemRepository;

        public ListItems()
        {
            _serviceItemRepository =
               new ServiceItemRepository(
                   ConfigurationManager.ConnectionStrings["ExpandITBASConnectionString"].ConnectionString);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            string addressId = Request.QueryString["id"];
            if (!Page.IsPostBack)
            {
                lvItems.DataSource = _serviceItemRepository.GetByAddressId(addressId);
                lvItems.DataBind();
            }
        }
    }
}