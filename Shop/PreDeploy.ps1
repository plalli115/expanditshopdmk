$ErrorActionPreference='Stop'
$curpath = (Get-Variable MyInvocation).Value.MyCommand.Path

$myModulePath = join-path (Split-Path $curpath -Parent) "Deployment-PSModules"
$env:PSModulePath = $env:PSModulePath + ";$myModulePath"

Import-Module -force Backup
Import-Module -force ConfigurationManagement
Import-Module -force Db
Import-Module -force FileCleanup

$myScriptPath = join-path (Split-Path $curpath -Parent) "OctopusVariablesNaming.ps1"
. $myScriptPath

$ProjectName = GetOctopusProjectName
$PackageDirectoryPath = GetOctopusPackageDirectoryPath
$OriginalPackageDirectoryPath = GetOctopusOriginalPackageDirectoryPath

Try {
  Write-Host "Starting..."
  BackupExistingSolution $ProjectName $PackageDirectoryPath ""
  ResetWwwRoot $PackageDirectoryPath $EnvName

  Write-Host "SQL deployment to ShopLocal..."
  UpgradeDB $OctopusOriginalPackageDirectoryPath "" $ExpandITConnectionString $OctopusEnvironmentName
  
} Catch{
  Write-Host "ERROR Predeploy script failed $_.Exception.Message"
  Exit 1
}