﻿Imports ExpanditLib2
Imports EISCS.Routing.Providers
Imports EISCS.ExpandITFramework.Util

Partial Class _Default
    Inherits UI.Page

    Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Dim frontGroup As String = ConfigurationManager.AppSettings("FRONTPAGE_GROUP")
        If String.IsNullOrEmpty(frontGroup) Then
            frontGroup = 0
        End If
        Dim redirectUrl As String = GroupLink(CLngEx(frontGroup))

        If String.IsNullOrEmpty(redirectUrl) Then
            redirectUrl = AppUtil.GetVirtualRoot() & "/shop.aspx"
        End If

        Response.AddHeader("Location", redirectUrl)
        Response.StatusCode = 302
    End Sub

    Public Function GroupLink(ByVal groupguid As Integer) As String
        Dim languageGuid As String = CStrEx(HttpContext.Current.Session("LanguageGuid"))
        If String.IsNullOrEmpty(languageGuid) Then languageGuid = ConfigurationManager.AppSettings("LANGUAGE_DEFAULT")
        Return RouteManager.Route.CreateTemplateGroupLink(groupguid, languageGuid)
    End Function
End Class
