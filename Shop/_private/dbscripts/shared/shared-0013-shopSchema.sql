/* MailMessage */
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'MailMessage')
CREATE TABLE [MailMessage](
	[MailMessageGuid] [int] NOT NULL,
	[DateCreated] [datetime] NULL,
	[Active] [bit] NULL,
	[MailMessageName] [nvarchar](80) NULL
) ON [PRIMARY]
GO
/* MailMessageProduct */
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'MailMessageProduct')
CREATE TABLE [MailMessageProduct](
	[MailMessageGuid] [int] NOT NULL,
	[ProductGuid] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_MailMessageProduct] PRIMARY KEY CLUSTERED 
(
	[MailMessageGuid] ASC,
	[ProductGuid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/* MailMessageSettings */
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'MailMessageSettings')
CREATE TABLE [MailMessageSettings](
	[Id] [int] NOT NULL,
	[SendAsName] [nvarchar](50) NULL,
	[SendAsAddress] [nvarchar](50) NULL,
	[CC] [nvarchar](50) NULL,
	[BCC] [nvarchar](50) NULL,
	[SendReminderEmail] [bit] NULL,
	[SendAfterStalePeriod] [int] NULL,
	[RepeatPeriod] [int] NULL,
	[SendTime] [time](7) NULL,
	[ForceSsl] [bit] NULL,
	[Host] [nvarchar](50) NULL,
	[Port] [nvarchar](10) NULL,
	[UserName] [nvarchar](50) NULL,
	[Password] [nvarchar](50) NULL
) ON [PRIMARY]
GO
/* AccessTable */
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'AccessTable')
CREATE TABLE [AccessTable]
(
	[AccessClass] [nvarchar] (50) NOT NULL,
	[ClassDescription] [nvarchar] (100) NULL,
	[AccessType] [nvarchar] (50) NULL,
	CONSTRAINT [PK_AccessTable] PRIMARY KEY CLUSTERED
	(
		[AccessClass] ASC
	)
)

DECLARE @sql nvarchar(255)
WHILE EXISTS(SELECT OBJECT_NAME(constid) AS ContraintName FROM sysconstraints WHERE OBJECT_NAME(id) = 'AccessTable')
BEGIN
  SELECT    @sql = 'ALTER TABLE [AccessTable] DROP CONSTRAINT [' + OBJECT_NAME(constid) + ']'
  FROM     sysconstraints WHERE OBJECT_NAME(id) = 'AccessTable' ORDER BY colid
  EXEC    sp_executesql @sql
END
GO
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'AccessTable' AND COLUMN_NAME = 'AccessClass')
  ALTER TABLE [AccessTable] ADD [AccessClass] nvarchar(50) NOT NULL;
ELSE
  ALTER TABLE [AccessTable] ALTER COLUMN [AccessClass] nvarchar(50) NOT NULL;
GO
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'AccessTable' AND COLUMN_NAME = 'ClassDescription')
  ALTER TABLE [AccessTable] ADD [ClassDescription] nvarchar(100) NULL;
ELSE
  ALTER TABLE [AccessTable] ALTER COLUMN [ClassDescription] nvarchar(100) NULL;
GO
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'AccessTable' AND COLUMN_NAME = 'AccessType')
  ALTER TABLE [AccessTable] ADD [AccessType] nvarchar(50) NULL;
ELSE
  ALTER TABLE [AccessTable] ALTER COLUMN [AccessType] nvarchar(50) NULL;
GO
ALTER TABLE [AccessTable] ADD CONSTRAINT [PK_AccessTable] PRIMARY KEY ([AccessClass] ASC);
GO
/* AccessRoles */
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'AccessRoles')
CREATE TABLE [AccessRoles]
(
	[RoleId] [nvarchar] (50) NOT NULL,
	[AccessClass] [nvarchar] (50) NOT NULL,
	CONSTRAINT [PK_AccessRoles] PRIMARY KEY CLUSTERED
	(
		[RoleId] ASC,
		[AccessClass] ASC
	)
)
GO
/* CustomerGroup */
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'CustomerGroup')
CREATE TABLE [CustomerGroup]
(
	[Id] [int] IDENTITY (1,1) NOT NULL,
	[Name] [nvarchar] (max) NOT NULL,
	CONSTRAINT [PK_CustomerGroup] PRIMARY KEY CLUSTERED
	(
		[Id] ASC
	)
)
GO
/* CustomerProperties */
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'CustomerProperties')
CREATE TABLE [CustomerProperties]
(
	[Id] [int] IDENTITY (1,1) NOT NULL,
	[CustomerId] [nvarchar] (50) NOT NULL,
	[RelationTableId] [nvarchar] (50) NOT NULL,
	[RelationTableTypeId] [int] NOT NULL,
	CONSTRAINT [PK_CustomerProperties] PRIMARY KEY CLUSTERED
	(
		[Id] ASC
	)
)
GO
/* KeyPerformanceIndicators */
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'KeyPerformanceIndicators')
CREATE TABLE [KeyPerformanceIndicators]
(
	[EntryDate] [date] NOT NULL,
	[TotalLogins] [int] NULL,
	[TotalOrders] [int] NULL,
	[AvgOrderSize] [float] NULL,
	[TotalRevenueWithoutVAT] [float] NULL,
	[AvgItemPrize] [float] NULL,
	[TotalItems] [int] NULL,
	[NewUsers] [int] NULL,
	CONSTRAINT [PK_KeyPerformanceIndicators] PRIMARY KEY CLUSTERED
	(
		[EntryDate] ASC
	)
)
GO
/* MailMessage */
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'MailMessage')
CREATE TABLE [MailMessage]
(
	[MailMessageGuid] [int] NOT NULL,
	[DateCreated] [datetime] NULL,
	[Active] [bit] NULL,
	[MailMessageName] [nvarchar] (80) NULL
) ON [PRIMARY]
GO
/* MailMessageProduct */
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'MailMessageProduct')
CREATE TABLE [MailMessageProduct]
(
	[MailMessageGuid] [int] NOT NULL,
	[ProductGuid] [nvarchar] (50) NULL,
	CONSTRAINT [PK_MailMessageProduct] PRIMARY KEY CLUSTERED
	(
		[MailMessageGuid] ASC
	)
)
GO
/* MailMessageSettings */
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'MailMessageSettings')
CREATE TABLE [MailMessageSettings]
(
	[Id] [int] IDENTITY (1,1) NOT NULL,
	[SendAsName] [nvarchar] (50) NULL,
	[SendAsAddress] [nvarchar] (50) NULL,
	[CC] [nvarchar] (50) NULL,
	[BCC] [nvarchar] (50) NULL,
	[SendReminderEmail] [bit] NULL,
	[SendAfterStalePeriod] [int] NULL,
	[RepeatPeriod] [int] NULL,
	[SendTime] [time] NULL,
	[ForceSsl] [bit] NULL,
	[Host] [nvarchar] (50) NULL,
	[Port] [nvarchar] (10) NULL,
	[UserName] [nvarchar] (50) NULL,
	[Password] [nvarchar] (50) NULL
)
GO
/* UserRole */
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'UserRole')
CREATE TABLE [UserRole]
(
	[UserGuid] [nvarchar](50) NOT NULL,
	[RoleId] [nvarchar](50) NULL,
	CONSTRAINT [PK_UserRole] PRIMARY KEY CLUSTERED 
(
 [UserGuid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/* BusinessCenterUser */
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'BusinessCenterUser')
CREATE TABLE [BusinessCenterUser](
 [UserGuid] [nvarchar](50) NOT NULL,
 [UserLogin] [nvarchar](50) NOT NULL,
 [UserPassword] [nvarchar](50) NOT NULL,
 [RoleId] [nvarchar](50) NULL,
 [EncryptionType] [nvarchar](50) NULL,
 [EncryptionSalt] [nvarchar](200) NULL,
 [LastLoginDate] [datetime] NULL,
 [IsSuperUser] [bit] NULL,
 CONSTRAINT [PK_BusinessCenterUser] PRIMARY KEY CLUSTERED 
(
 [UserGuid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/* RoleTable */
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'RoleTable')
	CREATE TABLE [RoleTable]
	(
		[RoleId] [nvarchar] (50) NOT NULL,
		[RoleDescription] [nvarchar] (100) NULL,
		[ReadOnly] [bit] NULL DEFAULT (0),
		CONSTRAINT [PK_RoleTable] PRIMARY KEY CLUSTERED
		(
			[RoleId] ASC
		)
	)
ELSE
	IF COL_LENGTH('RoleTable','ReadOnly') IS NULL
 	BEGIN
 		ALTER TABLE [RoleTable] ADD [ReadOnly] bit NULL DEFAULT (0)
 	END
GO
/* ShopSalesHeader_InjectMark */
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'ShopSalesHeader_InjectMark')
CREATE TABLE [ShopSalesHeader_InjectMark]
(
	[KeyValue] [nvarchar] (50) NULL,
	[InjectTime] [datetime] NULL
) ON [PRIMARY]
GO
IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name = 'PK_ShopSalesHeader_InjectMark' AND object_id = OBJECT_ID('ShopSalesHeader_InjectMark'))
	CREATE UNIQUE NONCLUSTERED INDEX [PK_ShopSalesHeader_InjectMark] ON [ShopSalesHeader_InjectMark]
	(
		[KeyValue] ASC
	)
GO
/* ShopSalesLine_InjectMark */
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'ShopSalesLine_InjectMark')
CREATE TABLE [ShopSalesLine_InjectMark]
(
	[KeyValue] [nvarchar] (50) NULL,
	[InjectTime] [datetime] NULL
) ON [PRIMARY]
GO
IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name = 'PK_ShopSalesLine_InjectMark' AND object_id = OBJECT_ID('ShopSalesLine_InjectMark'))
	CREATE UNIQUE NONCLUSTERED INDEX [PK_ShopSalesLine_InjectMark] ON [ShopSalesLine_InjectMark]
	(
		[KeyValue] ASC
	)
GO
/* UsefulLinks */
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'UsefulLinks')
CREATE TABLE [UsefulLinks]
(
	[LinkText] [nvarchar] (50) NULL,
	[Url] [nvarchar] (256) NULL,
	[Description] [nvarchar] (140) NULL,
	[Position] [int] NOT NULL,
	[LinkId] [int] NOT NULL,
	CONSTRAINT [PK_UsefulLinks] PRIMARY KEY CLUSTERED
	(
		[LinkId] ASC
	)
)
GO
/* UserTable_InjectHistory */
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'UserTable_InjectHistory')
CREATE TABLE [UserTable_InjectHistory]
(
	[Address1] [nvarchar] (50) NULL,
	[Address2] [nvarchar] (50) NULL,
	[CityName] [nvarchar] (50) NULL,
	[CompanyName] [nvarchar] (50) NULL,
	[ContactName] [nvarchar] (50) NULL,
	[CountryGuid] [nvarchar] (50) NULL,
	[CountyName] [nvarchar] (50) NULL,
	[CurrencyGuid] [nvarchar] (38) NULL,
	[CustomerGuid] [nvarchar] (38) NULL,
	[EmailAddress] [nvarchar] (80) NULL,
	[IsB2B] [tinyint] NULL,
	[LanguageGuid] [nvarchar] (38) NULL,
	[PasswordVersion] [nvarchar] (38) NULL,
	[PhoneNo] [nvarchar] (30) NULL,
	[SecondaryCurrencyGuid] [nvarchar] (38) NULL,
	[StateName] [nvarchar] (50) NULL,
	[UserCreated] [datetime] NULL,
	[UserGuid] [nvarchar] (50) NOT NULL,
	[UserLogin] [nvarchar] (50) NULL,
	[UserModified] [datetime] NULL,
	[UserPassword] [nvarchar] (40) NULL,
	[ZipCode] [nvarchar] (50) NULL,
	[ShippingProvider] [nvarchar] (50) NULL,
	[Status] [tinyint] NULL
) ON [PRIMARY]
GO
/* UserTable */
DECLARE @sql nvarchar(255)
WHILE EXISTS(SELECT OBJECT_NAME(constid) AS ContraintName FROM sysconstraints WHERE OBJECT_NAME(id) = 'UserTable')
BEGIN
  SELECT    @sql = 'ALTER TABLE [UserTable] DROP CONSTRAINT [' + OBJECT_NAME(constid) + ']'
  FROM     sysconstraints WHERE OBJECT_NAME(id) = 'UserTable' ORDER BY colid
  EXEC    sp_executesql @sql
END
GO
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'UserTable' AND COLUMN_NAME = 'Address1')
  ALTER TABLE [UserTable] ADD [Address1] nvarchar(50) NULL;
ELSE
  ALTER TABLE [UserTable] ALTER COLUMN [Address1] nvarchar(50) NULL;
GO
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'UserTable' AND COLUMN_NAME = 'Address2')
  ALTER TABLE [UserTable] ADD [Address2] nvarchar(50) NULL;
ELSE
  ALTER TABLE [UserTable] ALTER COLUMN [Address2] nvarchar(50) NULL;
GO
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'UserTable' AND COLUMN_NAME = 'CityName')
  ALTER TABLE [UserTable] ADD [CityName] nvarchar(50) NULL;
ELSE
  ALTER TABLE [UserTable] ALTER COLUMN [CityName] nvarchar(50) NULL;
GO
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'UserTable' AND COLUMN_NAME = 'CompanyName')
  ALTER TABLE [UserTable] ADD [CompanyName] nvarchar(50) NULL;
ELSE
  ALTER TABLE [UserTable] ALTER COLUMN [CompanyName] nvarchar(50) NULL;
GO
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'UserTable' AND COLUMN_NAME = 'ContactName')
  ALTER TABLE [UserTable] ADD [ContactName] nvarchar(50) NULL;
ELSE
  ALTER TABLE [UserTable] ALTER COLUMN [ContactName] nvarchar(50) NULL;
GO
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'UserTable' AND COLUMN_NAME = 'CountryGuid')
  ALTER TABLE [UserTable] ADD [CountryGuid] nvarchar(50) NULL;
ELSE
  ALTER TABLE [UserTable] ALTER COLUMN [CountryGuid] nvarchar(50) NULL;
GO
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'UserTable' AND COLUMN_NAME = 'CurrencyGuid')
  ALTER TABLE [UserTable] ADD [CurrencyGuid] nvarchar(38) NULL;
ELSE
  ALTER TABLE [UserTable] ALTER COLUMN [CurrencyGuid] nvarchar(38) NULL;
GO
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'UserTable' AND COLUMN_NAME = 'CustomerGuid')
  ALTER TABLE [UserTable] ADD [CustomerGuid] nvarchar(38) NULL;
ELSE
  ALTER TABLE [UserTable] ALTER COLUMN [CustomerGuid] nvarchar(38) NULL;
GO
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'UserTable' AND COLUMN_NAME = 'EmailAddress')
  ALTER TABLE [UserTable] ADD [EmailAddress] nvarchar(80) NULL;
ELSE
  ALTER TABLE [UserTable] ALTER COLUMN [EmailAddress] nvarchar(80) NULL;
GO
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'UserTable' AND COLUMN_NAME = 'IsB2B')
  ALTER TABLE [UserTable] ADD [IsB2B] tinyint NULL;
ELSE
  ALTER TABLE [UserTable] ALTER COLUMN [IsB2B] tinyint NULL;
GO
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'UserTable' AND COLUMN_NAME = 'LanguageGuid')
  ALTER TABLE [UserTable] ADD [LanguageGuid] nvarchar(38) NULL;
ELSE
  ALTER TABLE [UserTable] ALTER COLUMN [LanguageGuid] nvarchar(38) NULL;
GO
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'UserTable' AND COLUMN_NAME = 'PasswordVersion')
  ALTER TABLE [UserTable] ADD [PasswordVersion] nvarchar(38) NULL;
ELSE
  ALTER TABLE [UserTable] ALTER COLUMN [PasswordVersion] nvarchar(38) NULL;
GO
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'UserTable' AND COLUMN_NAME = 'PhoneNo')
  ALTER TABLE [UserTable] ADD [PhoneNo] nvarchar(30) NULL;
ELSE
  ALTER TABLE [UserTable] ALTER COLUMN [PhoneNo] nvarchar(30) NULL;
GO
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'UserTable' AND COLUMN_NAME = 'SecondaryCurrencyGuid')
  ALTER TABLE [UserTable] ADD [SecondaryCurrencyGuid] nvarchar(38) NULL;
ELSE
  ALTER TABLE [UserTable] ALTER COLUMN [SecondaryCurrencyGuid] nvarchar(38) NULL;
GO
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'UserTable' AND COLUMN_NAME = 'StateName')
  ALTER TABLE [UserTable] ADD [StateName] nvarchar(50) NULL;
ELSE
  ALTER TABLE [UserTable] ALTER COLUMN [StateName] nvarchar(50) NULL;
GO
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'UserTable' AND COLUMN_NAME = 'UserCreated')
  ALTER TABLE [UserTable] ADD [UserCreated] datetime NULL;
ELSE
  ALTER TABLE [UserTable] ALTER COLUMN [UserCreated] datetime NULL;
GO
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'UserTable' AND COLUMN_NAME = 'UserGuid')
  ALTER TABLE [UserTable] ADD [UserGuid] nvarchar(50) NOT NULL;
ELSE
  ALTER TABLE [UserTable] ALTER COLUMN [UserGuid] nvarchar(50) NOT NULL;
GO
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'UserTable' AND COLUMN_NAME = 'UserLogin')
  ALTER TABLE [UserTable] ADD [UserLogin] nvarchar(50) NULL;
ELSE
  ALTER TABLE [UserTable] ALTER COLUMN [UserLogin] nvarchar(50) NULL;
GO
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'UserTable' AND COLUMN_NAME = 'UserModified')
  ALTER TABLE [UserTable] ADD [UserModified] datetime NULL;
ELSE
  ALTER TABLE [UserTable] ALTER COLUMN [UserModified] datetime NULL;
GO
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'UserTable' AND COLUMN_NAME = 'ZipCode')
  ALTER TABLE [UserTable] ADD [ZipCode] nvarchar(50) NULL;
ELSE
  ALTER TABLE [UserTable] ALTER COLUMN [ZipCode] nvarchar(50) NULL;
GO
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'UserTable' AND COLUMN_NAME = 'ShippingProvider')
  ALTER TABLE [UserTable] ADD [ShippingProvider] nvarchar(50) NULL;
ELSE
  ALTER TABLE [UserTable] ALTER COLUMN [ShippingProvider] nvarchar(50) NULL;
GO
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'UserTable' AND COLUMN_NAME = 'Status')
  ALTER TABLE [UserTable] ADD [Status] tinyint NULL;
ELSE
  ALTER TABLE [UserTable] ALTER COLUMN [Status] tinyint NULL;
GO
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'UserTable' AND COLUMN_NAME = 'RoleId')
  ALTER TABLE [UserTable] ADD [RoleId] nvarchar(max) NULL;
ELSE
  ALTER TABLE [UserTable] ALTER COLUMN [RoleId] nvarchar(max) NULL;
GO
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'UserTable' AND COLUMN_NAME = 'IsApproved')
  ALTER TABLE [UserTable] ADD [IsApproved] bit NULL;
ELSE
  ALTER TABLE [UserTable] ALTER COLUMN [IsApproved] bit NULL;
GO
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'UserTable' AND COLUMN_NAME = 'IsLockedOut')
  ALTER TABLE [UserTable] ADD [IsLockedOut] bit NULL;
ELSE
  ALTER TABLE [UserTable] ALTER COLUMN [IsLockedOut] bit NULL;
GO
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'UserTable' AND COLUMN_NAME = 'LastLoginDate')
  ALTER TABLE [UserTable] ADD [LastLoginDate] datetime NULL;
ELSE
  ALTER TABLE [UserTable] ALTER COLUMN [LastLoginDate] datetime NULL;
GO
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'UserTable' AND COLUMN_NAME = 'LastPasswordChangeDate')
  ALTER TABLE [UserTable] ADD [LastPasswordChangeDate] datetime NULL;
ELSE
  ALTER TABLE [UserTable] ALTER COLUMN [LastPasswordChangeDate] datetime NULL;
GO
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'UserTable' AND COLUMN_NAME = 'LastLockoutDate')
  ALTER TABLE [UserTable] ADD [LastLockoutDate] datetime NULL;
ELSE
  ALTER TABLE [UserTable] ALTER COLUMN [LastLockoutDate] datetime NULL;
GO
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'UserTable' AND COLUMN_NAME = 'FailedPasswordAttemptCount')
  ALTER TABLE [UserTable] ADD [FailedPasswordAttemptCount] int NULL;
ELSE
  ALTER TABLE [UserTable] ALTER COLUMN [FailedPasswordAttemptCount] int NULL;
GO
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'UserTable' AND COLUMN_NAME = 'FailedPasswordAttemptCountWindowStart')
  ALTER TABLE [UserTable] ADD [FailedPasswordAttemptCountWindowStart] datetime NULL;
ELSE
  ALTER TABLE [UserTable] ALTER COLUMN [FailedPasswordAttemptCountWindowStart] datetime NULL;
GO
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'UserTable' AND COLUMN_NAME = 'AccessClass')
  ALTER TABLE [UserTable] ADD [AccessClass] nvarchar(max) NULL;
ELSE
  ALTER TABLE [UserTable] ALTER COLUMN [AccessClass] nvarchar(max) NULL;
GO
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'UserTable' AND COLUMN_NAME = 'EncryptionType')
  ALTER TABLE [UserTable] ADD [EncryptionType] nvarchar(50) NULL;
ELSE
  ALTER TABLE [UserTable] ALTER COLUMN [EncryptionType] nvarchar(50) NULL;
GO
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'UserTable' AND COLUMN_NAME = 'EncryptionSalt')
  ALTER TABLE [UserTable] ADD [EncryptionSalt] nvarchar(max) NULL;
ELSE
  ALTER TABLE [UserTable] ALTER COLUMN [EncryptionSalt] nvarchar(max) NULL;
GO
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'UserTable' AND COLUMN_NAME = 'UserPassword')
  ALTER TABLE [UserTable] ADD [UserPassword] nvarchar(max) NULL;
ELSE
  ALTER TABLE [UserTable] ALTER COLUMN [UserPassword] nvarchar(max) NULL;
GO
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'UserTable' AND COLUMN_NAME = 'PasswordResetRequestKey')
  ALTER TABLE [UserTable] ADD [PasswordResetRequestKey] nvarchar(300) NULL;
ELSE
  ALTER TABLE [UserTable] ALTER COLUMN [PasswordResetRequestKey] nvarchar(300) NULL;
GO
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'UserTable' AND COLUMN_NAME = 'PasswordResetRequestTime')
  ALTER TABLE [UserTable] ADD [PasswordResetRequestTime] datetime NULL;
ELSE
  ALTER TABLE [UserTable] ALTER COLUMN [PasswordResetRequestTime] datetime NULL;
GO
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'UserTable' AND COLUMN_NAME = 'Secret')
  ALTER TABLE [UserTable] ADD [Secret] nvarchar(300) NULL;
ELSE
  ALTER TABLE [UserTable] ALTER COLUMN [Secret] nvarchar(300) NULL;
GO
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'UserTable' AND COLUMN_NAME = 'MultiFactorAuthEnabled')
  ALTER TABLE [UserTable] ADD [MultiFactorAuthEnabled] bit NULL DEFAULT 0;
ELSE
  ALTER TABLE [UserTable] ALTER COLUMN [MultiFactorAuthEnabled] bit NULL;
GO
ALTER TABLE [UserTable] ADD CONSTRAINT [PK_UserTable] PRIMARY KEY ([UserGuid]);
GO