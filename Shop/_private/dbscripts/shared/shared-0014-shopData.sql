/* PropGrpRel */
IF NOT EXISTS(SELECT 1 FROM PropGrpRel WHERE PropGrpGuid = 'MAILMESSAGE' AND PropGuid = 'ATTACHMENT') INSERT [PropGrpRel] ([PropGrpGuid], [PropGuid], [SortIndex]) VALUES (N'MAILMESSAGE', N'ATTACHMENT', 5)
GO
IF NOT EXISTS(SELECT 1 FROM PropGrpRel WHERE PropGrpGuid = 'MAILMESSAGE' AND PropGuid = 'HTMLDESC') INSERT [PropGrpRel] ([PropGrpGuid], [PropGuid], [SortIndex]) VALUES (N'MAILMESSAGE', N'HTMLDESC', 3)
GO
IF NOT EXISTS(SELECT 1 FROM PropGrpRel WHERE PropGrpGuid = 'MAILMESSAGE' AND PropGuid = 'MAILBCC') INSERT [PropGrpRel] ([PropGrpGuid], [PropGuid], [SortIndex]) VALUES (N'MAILMESSAGE', N'MAILBCC', 7)
GO
IF NOT EXISTS(SELECT 1 FROM PropGrpRel WHERE PropGrpGuid = 'MAILMESSAGE' AND PropGuid = 'MAILCC') INSERT [PropGrpRel] ([PropGrpGuid], [PropGuid], [SortIndex]) VALUES (N'MAILMESSAGE', N'MAILCC', 6)
GO
IF NOT EXISTS(SELECT 1 FROM PropGrpRel WHERE PropGrpGuid = 'MAILMESSAGE' AND PropGuid = 'MAILSUBJECT') INSERT [PropGrpRel] ([PropGrpGuid], [PropGuid], [SortIndex]) VALUES (N'MAILMESSAGE', N'MAILSUBJECT', 4)
GO
IF NOT EXISTS(SELECT 1 FROM PropGrpRel WHERE PropGrpGuid = 'MAILMESSAGE' AND PropGuid = 'NAME') INSERT [PropGrpRel] ([PropGrpGuid], [PropGuid], [SortIndex]) VALUES (N'MAILMESSAGE', N'NAME', 1)
GO
IF NOT EXISTS(SELECT 1 FROM PropGrpRel WHERE PropGrpGuid = 'MAILMESSAGE' AND PropGuid = 'PICTURE1') INSERT [PropGrpRel] ([PropGrpGuid], [PropGuid], [SortIndex]) VALUES (N'MAILMESSAGE', N'PICTURE1', 2)
GO
IF NOT EXISTS(SELECT 1 FROM PropGrpRel WHERE PropGrpGuid = 'MAILMESSAGE' AND PropGuid = 'PROMOTIONCTATEXT') INSERT [PropGrpRel] ([PropGrpGuid], [PropGuid], [SortIndex]) VALUES (N'MAILMESSAGE', N'PROMOTIONCTATEXT', 9)
GO
IF NOT EXISTS(SELECT 1 FROM PropGrpRel WHERE PropGrpGuid = 'MAILMESSAGE' AND PropGuid = 'PROMOTIONTEXT') INSERT [PropGrpRel] ([PropGrpGuid], [PropGuid], [SortIndex]) VALUES (N'MAILMESSAGE', N'PROMOTIONTEXT', 8)
GO
/* PropTable */
IF NOT EXISTS(SELECT 1 FROM PropTable WHERE PropGuid = 'MAILBCC') INSERT [PropTable] ([PropGuid], [PropName], [PropTypeGuid], [PropImageIndex], [PropMultilanguageDefault], [PropReadonly], [PropHelpHeader], [PropHelpBody]) VALUES (N'MAILBCC', N'Mail BCC', N'TEXT', NULL, 0, 0, N'Mail BCC', N'Mail BCC')
GO
IF NOT EXISTS(SELECT 1 FROM PropTable WHERE PropGuid = 'MAILCC') INSERT [PropTable] ([PropGuid], [PropName], [PropTypeGuid], [PropImageIndex], [PropMultilanguageDefault], [PropReadonly], [PropHelpHeader], [PropHelpBody]) VALUES (N'MAILCC', N'Mail CC', N'TEXT', NULL, 0, 0, N'Mail CC', N'Mail CC')
GO
IF NOT EXISTS(SELECT 1 FROM PropTable WHERE PropGuid = 'MAILSUBJECT') INSERT [PropTable] ([PropGuid], [PropName], [PropTypeGuid], [PropImageIndex], [PropMultilanguageDefault], [PropReadonly], [PropHelpHeader], [PropHelpBody]) VALUES (N'MAILSUBJECT', N'Mail Subject', N'TEXT', NULL, 1, 0, N'Mail Subject', N'Mail Subject')
GO
IF NOT EXISTS(SELECT 1 FROM PropTable WHERE PropGuid = 'PROMOTIONCTATEXT') INSERT [PropTable] ([PropGuid], [PropName], [PropTypeGuid], [PropImageIndex], [PropMultilanguageDefault], [PropReadonly], [PropHelpHeader], [PropHelpBody]) VALUES (N'PROMOTIONCTATEXT', N'Promotion CTA', N'TEXT', NULL, 1, 0, N'Promotion CTA Text', N'Promotion CTA text')
GO
IF NOT EXISTS(SELECT 1 FROM PropTable WHERE PropGuid = 'PROMOTIONTEXT') INSERT [PropTable] ([PropGuid], [PropName], [PropTypeGuid], [PropImageIndex], [PropMultilanguageDefault], [PropReadonly], [PropHelpHeader], [PropHelpBody]) VALUES (N'PROMOTIONTEXT', N'Promotion Header Text', N'TEXT', NULL, 1, 0, N'Promotion Text', N'Promotion Text')
GO
/* MailMessage */
IF NOT EXISTS(SELECT 1 FROM MailMessage WHERE MailMessageGuid = 111111) INSERT [MailMessage] ([MailMessageGuid], [DateCreated], [Active], [MailMessageName]) VALUES (111111, NULL, 1, N'OrderConfirmation')
GO
IF NOT EXISTS(SELECT 1 FROM MailMessage WHERE MailMessageGuid = 211112) INSERT [MailMessage] ([MailMessageGuid], [DateCreated], [Active], [MailMessageName]) VALUES (211112, NULL, 1, N'Welcome')
GO
IF NOT EXISTS(SELECT 1 FROM MailMessage WHERE MailMessageGuid = 311113) INSERT [MailMessage] ([MailMessageGuid], [DateCreated], [Active], [MailMessageName]) VALUES (311113, NULL, 1, N'PasswordChange')
GO
IF NOT EXISTS(SELECT 1 FROM MailMessage WHERE MailMessageGuid = 411114) INSERT [MailMessage] ([MailMessageGuid], [DateCreated], [Active], [MailMessageName]) VALUES (411114, NULL, 1, N'UserUpdate')
GO
IF NOT EXISTS(SELECT 1 FROM MailMessage WHERE MailMessageGuid = 611115) INSERT [MailMessage] ([MailMessageGuid], [DateCreated], [Active], [MailMessageName]) VALUES (611115, NULL, 1, N'TipFriend')
GO
IF NOT EXISTS(SELECT 1 FROM MailMessage WHERE MailMessageGuid = 711116) INSERT [MailMessage] ([MailMessageGuid], [DateCreated], [Active], [MailMessageName]) VALUES (711116, NULL, 1, N'LostPassword')
GO
IF NOT EXISTS(SELECT 1 FROM MailMessage WHERE MailMessageGuid = 811117) INSERT [MailMessage] ([MailMessageGuid], [DateCreated], [Active], [MailMessageName]) VALUES (811117, NULL, 1, N'WebContact')
GO
IF NOT EXISTS(SELECT 1 FROM MailMessage WHERE MailMessageGuid = 911119) INSERT [MailMessage] ([MailMessageGuid], [DateCreated], [Active], [MailMessageName]) VALUES (911119, NULL, 1, N'StaleUserCartReminder')
GO
IF NOT EXISTS(SELECT 1 FROM MailMessage WHERE MailMessageGuid = 921111) INSERT [MailMessage] ([MailMessageGuid], [DateCreated], [Active], [MailMessageName]) VALUES (921111, NULL, 1, N'ResetPassword')
GO
/* AccessClass */
IF NOT EXISTS(SELECT 1 FROM AccessTable WHERE AccessClass = 'AccountManagement') INSERT INTO AccessTable (AccessClass, ClassDescription, AccessType) VALUES ('AccountManagement','Access to Account Management', 'BC')
GO
IF NOT EXISTS(SELECT 1 FROM AccessTable WHERE AccessClass = 'BusinessCenter') INSERT INTO AccessTable (AccessClass, ClassDescription, AccessType) VALUES ('BusinessCenter','Access to the BC', 'BC')
GO
IF NOT EXISTS(SELECT 1 FROM AccessTable WHERE AccessClass = 'Cart') INSERT INTO AccessTable (AccessClass, ClassDescription, AccessType) VALUES ('Cart','Access to use the order pad', 'SHOP')
GO
IF NOT EXISTS(SELECT 1 FROM AccessTable WHERE AccessClass = 'Catalog') INSERT INTO AccessTable (AccessClass, ClassDescription, AccessType) VALUES ('Catalog','Access to the catalog', 'SHOP')
GO
IF NOT EXISTS(SELECT 1 FROM AccessTable WHERE AccessClass = 'CreateServiceItem') INSERT INTO AccessTable (AccessClass, ClassDescription, AccessType) VALUES ('CreateServiceItem','Allow create service item', 'PORTAL')
GO
IF NOT EXISTS(SELECT 1 FROM AccessTable WHERE AccessClass = 'CreateServiceOrder') INSERT INTO AccessTable (AccessClass, ClassDescription, AccessType) VALUES ('CreateServiceOrder','Access to creating service orders', 'PORTAL')
GO
IF NOT EXISTS(SELECT 1 FROM AccessTable WHERE AccessClass = 'CustomerSearch') INSERT INTO AccessTable (AccessClass, ClassDescription, AccessType) VALUES ('CustomerSearch','Allow searching for customer', 'PORTAL')
GO
IF NOT EXISTS(SELECT 1 FROM AccessTable WHERE AccessClass = 'EditCatalog') INSERT INTO AccessTable (AccessClass, ClassDescription, AccessType) VALUES ('EditCatalog','Allow to edit catalog content', 'BC')
GO
IF NOT EXISTS(SELECT 1 FROM AccessTable WHERE AccessClass = 'EditServiceItem') INSERT INTO AccessTable (AccessClass, ClassDescription, AccessType) VALUES ('EditServiceItem','Allow edit service item', 'PORTAL')
GO
IF NOT EXISTS(SELECT 1 FROM AccessTable WHERE AccessClass = 'EditServiceOrder') INSERT INTO AccessTable (AccessClass, ClassDescription, AccessType) VALUES ('EditServiceOrder','Allow edit service order', 'PORTAL')
GO
IF NOT EXISTS(SELECT 1 FROM AccessTable WHERE AccessClass = 'EditServiceOrderCustomer') INSERT INTO AccessTable (AccessClass, ClassDescription, AccessType) VALUES ('EditServiceOrderCustomer','Allow edit service order customer', 'PORTAL')
GO
IF NOT EXISTS(SELECT 1 FROM AccessTable WHERE AccessClass = 'EditServiceOrderDepartment') INSERT INTO AccessTable (AccessClass, ClassDescription, AccessType) VALUES ('EditServiceOrderDepartment','Allow edit service order department', 'PORTAL')
GO
IF NOT EXISTS(SELECT 1 FROM AccessTable WHERE AccessClass = 'EditServiceOrderJobStatus') INSERT INTO AccessTable (AccessClass, ClassDescription, AccessType) VALUES ('EditServiceOrderJobStatus','Allow edit service order job status', 'PORTAL')
GO
IF NOT EXISTS(SELECT 1 FROM AccessTable WHERE AccessClass = 'EditServiceOrderJobType') INSERT INTO AccessTable (AccessClass, ClassDescription, AccessType) VALUES ('EditServiceOrderJobType','Allow edit service order job type', 'PORTAL')
GO
IF NOT EXISTS(SELECT 1 FROM AccessTable WHERE AccessClass = 'EditServiceOrderProject') INSERT INTO AccessTable (AccessClass, ClassDescription, AccessType) VALUES ('EditServiceOrderProject','Allow edit service order project', 'PORTAL')
GO
IF NOT EXISTS(SELECT 1 FROM AccessTable WHERE AccessClass = 'EditServiceOrderServiceManager') INSERT INTO AccessTable (AccessClass, ClassDescription, AccessType) VALUES ('EditServiceOrderServiceManager','Allow edit service order service manager', 'PORTAL')
GO
IF NOT EXISTS(SELECT 1 FROM AccessTable WHERE AccessClass = 'EditServiceOrderTechnician') INSERT INTO AccessTable (AccessClass, ClassDescription, AccessType) VALUES ('EditServiceOrderTechnician','Allow edit service order technician', 'PORTAL')
GO
IF NOT EXISTS(SELECT 1 FROM AccessTable WHERE AccessClass = 'Favorites') INSERT INTO AccessTable (AccessClass, ClassDescription, AccessType) VALUES ('Favorites','Access to favorites', 'SHOP')
GO
IF NOT EXISTS(SELECT 1 FROM AccessTable WHERE AccessClass = 'HomePage') INSERT INTO AccessTable (AccessClass, ClassDescription, AccessType) VALUES ('HomePage','Access to the homepage', 'SHOP')
GO
IF NOT EXISTS(SELECT 1 FROM AccessTable WHERE AccessClass = 'ListAllCustomers') INSERT INTO AccessTable (AccessClass, ClassDescription, AccessType) VALUES ('ListAllCustomers','View all customers', 'PORTAL')
GO
IF NOT EXISTS(SELECT 1 FROM AccessTable WHERE AccessClass = 'ListCustomer') INSERT INTO AccessTable (AccessClass, ClassDescription, AccessType) VALUES ('ListCustomer','View users of same customer', 'PORTAL')
GO
IF NOT EXISTS(SELECT 1 FROM AccessTable WHERE AccessClass = 'ListCustomerGroup') INSERT INTO AccessTable (AccessClass, ClassDescription, AccessType) VALUES ('ListCustomerGroup','View customers in a specific group', 'PORTAL')
GO
IF NOT EXISTS(SELECT 1 FROM AccessTable WHERE AccessClass = 'OpenSite') INSERT INTO AccessTable (AccessClass, ClassDescription, AccessType) VALUES ('OpenSite','Access to create customer accounts', 'SHOP')
GO
IF NOT EXISTS(SELECT 1 FROM AccessTable WHERE AccessClass = 'Order') INSERT INTO AccessTable (AccessClass, ClassDescription, AccessType) VALUES ('Order','Access to order items', 'SHOP')
GO
IF NOT EXISTS(SELECT 1 FROM AccessTable WHERE AccessClass = 'Portal') INSERT INTO AccessTable (AccessClass, ClassDescription, AccessType) VALUES ('Portal','Access to the service portal', 'PORTAL')
GO
IF NOT EXISTS(SELECT 1 FROM AccessTable WHERE AccessClass = 'Profile') INSERT INTO AccessTable (AccessClass, ClassDescription, AccessType) VALUES ('Profile','Access to user profile', 'SHOP')
GO
IF NOT EXISTS(SELECT 1 FROM AccessTable WHERE AccessClass = 'ReadServiceOrderInternalComments') INSERT INTO AccessTable (AccessClass, ClassDescription, AccessType) VALUES ('ReadServiceOrderInternalComments','Allow read service order messages to office', 'PORTAL')
GO
IF NOT EXISTS(SELECT 1 FROM AccessTable WHERE AccessClass = 'ReadServiceOrderExternalComments') INSERT INTO AccessTable (AccessClass, ClassDescription, AccessType) VALUES ('ReadServiceOrderExternalComments','Allow read service order messages to invoice', 'PORTAL')
GO
/* RoleTable */
IF NOT EXISTS(SELECT 1 FROM RoleTable WHERE RoleId = 'Admin')
	INSERT INTO RoleTable (RoleId, RoleDescription, ReadOnly) VALUES ('Admin','Administrator', 1)
ELSE
	UPDATE RoleTable SET RoleId = 'Admin', RoleDescription = 'Administrator', ReadOnly = 1 WHERE RoleId = 'Admin'
GO
IF NOT EXISTS(SELECT 1 FROM RoleTable WHERE RoleId = 'Anonymous')
	INSERT INTO RoleTable (RoleId, RoleDescription, ReadOnly) VALUES ('Anonymous','Anonymous user', 1)
ELSE
	UPDATE RoleTable SET RoleId = 'Anonymous', RoleDescription = 'Anonymous user', ReadOnly = 1 WHERE RoleId = 'Anonymous'
GO
IF NOT EXISTS(SELECT 1 FROM RoleTable WHERE RoleId = 'B2B')
	INSERT INTO RoleTable (RoleId, RoleDescription, ReadOnly) VALUES ('B2B','B2B User', 1)
ELSE 
	UPDATE RoleTable SET RoleId = 'B2B', RoleDescription = 'B2B user', ReadOnly = 1 WHERE RoleId = 'B2B'
GO
IF NOT EXISTS(SELECT 1 FROM RoleTable WHERE RoleId = 'B2C')
	INSERT INTO RoleTable (RoleId, RoleDescription, ReadOnly) VALUES ('B2C','B2C User', 1)
ELSE 
	UPDATE RoleTable SET RoleId = 'B2C', RoleDescription = 'B2C user', ReadOnly = 1 WHERE RoleId = 'B2C'
GO
IF NOT EXISTS(SELECT 1 FROM RoleTable WHERE RoleId = 'Customer')
	INSERT INTO RoleTable (RoleId, RoleDescription, ReadOnly) VALUES ('Customer','Access to certain customer data', 1)
ELSE 
	UPDATE RoleTable SET RoleId = 'Customer', RoleDescription = 'Access to certain customer data', ReadOnly = 1 WHERE RoleId = 'Customer'
GO
IF NOT EXISTS(SELECT 1 FROM RoleTable WHERE RoleId = 'CustomerGroup')
	INSERT INTO RoleTable (RoleId, RoleDescription, ReadOnly) VALUES ('CustomerGroup','Access to a certain group of customer data', 1)
ELSE 
	UPDATE RoleTable SET RoleId = 'CustomerGroup', RoleDescription = 'Access to a certain group of customer data', ReadOnly = 1 WHERE RoleId = 'CustomerGroup'
GO
/* UserTable */
IF EXISTS(SELECT 1 FROM UserTable WHERE EncryptionType IS NULL)
	UPDATE UserTable SET EncryptionType = 'XOR' WHERE EncryptionType IS NULL
GO