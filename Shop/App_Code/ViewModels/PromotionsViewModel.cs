﻿using EISCS.Shop.DO.Dto.Cart;
using EISCS.Shop.DO.Dto.Promotions;
using EISCS.Shop.DO.Service;
using Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for PromotionViewModel
/// </summary>
/// 
namespace ViewModels
{


    public class PromotionsViewModel
    {
        private readonly PromotionsServices _promotionServices;
        [StringLength(50, ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "MESSAGE_THE_VALUE_IN_FIELD_IS_TOO_LONG")]
        [Required(ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "ERROR_VALIDATION_PROMOCODE")]
        [Display(ResourceType = typeof(Language), Name = "LABEL_PROMOTION_CODE")]
        public string NewPromoCode { get; set; }
        
        public List<Promotion> CurrentPromotions { get; set; }

        public PromotionsViewModel()
        {
            CurrentPromotions = new List<Promotion>();
            NewPromoCode = String.Empty;
        }

        public PromotionsViewModel(CartHeader cartHeader, PromotionsServices promotionServices)
        {
            _promotionServices = promotionServices;
            CurrentPromotions = new List<Promotion>();
            NewPromoCode = String.Empty;
            if (!String.IsNullOrEmpty(cartHeader.PromotionCode))
            {
                Promotion p = promotionServices.getPromoByCode(cartHeader.PromotionCode);
                if (p != null)
                {
                    CurrentPromotions.Add(p);
                }
            }

        }
    }

}