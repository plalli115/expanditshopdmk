﻿using System;
using EISCS.Shop.DO.BAS.Dto;

namespace ViewModels.ServiceOrderCommentLine
{
    public class ServiceOrderCommentLineViewModel
    {
        public string ServiceOrderGuid { get; set; }
        public string CommentLineGuid { get; set; }
        public ServiceOrderCommentLineType CommentType { get; set; }
        public string CommentText { get; set; }
        public DateTime CommentDate { get; set; }
        public int BASGuid { get; set; }
        public string UserGuid { get; set; }
        public string PortalUserName { get; set; }
        public string UserType { get; set; }
    }
}