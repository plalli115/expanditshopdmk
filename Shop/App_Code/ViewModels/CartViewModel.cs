﻿using System;
using System.Collections.Generic;
using System.Globalization;
using EISCS.ExpandIT;
using EISCS.ExpandITFramework.Util;
using EISCS.Routing.Providers;
using EISCS.Shop.BO.BusinessLogic;
using EISCS.Shop.DO.Dto.Cart;
using EISCS.Shop.DO.Interface;
using EISCS.Shop.DO.Service;
using EISCS.Wrappers.Configuration;
using ExpandIT;
using ViewServices;

namespace ViewModels
{
    public class CartViewModel
    {
        private readonly IConfigurationObject _configuration;
        private readonly ICurrencyConverter _currencyConverter;
        private readonly IExpanditUserService _expanditUserService;
        private readonly List<ProductInfoNode> _productInfo;
        private readonly string _userCurrency;
        private readonly string _userLanguage;
        private readonly IPaymentService _paymentService;

        public CartViewModel(CartHeader cartHeader, List<ProductInfoNode> productInfoNodes, IExpanditUserService expanditUserService, string shippingProviderName,
            IConfigurationObject configuration, ICurrencyConverter currencyConverter, IPaymentService paymentService)
        {
            _userCurrency = expanditUserService.CurrencyGuid;
            ErrorMessages = new Dictionary<string, string>();
            _userLanguage = expanditUserService.LanguageGuid;
            _configuration = configuration;
            _currencyConverter = currencyConverter;
            _paymentService = paymentService;
            _expanditUserService = expanditUserService;
            _productInfo = productInfoNodes;
            ProductItems = new List<ProductItem>();
            SetProductItems(cartHeader);
            SetBillingAddress(cartHeader);
            SetShippingAddress(cartHeader);
            SetTotals(cartHeader, shippingProviderName);
            SetHeaderInfo(cartHeader);
            ShippingProviderGuid = cartHeader.ShippingHandlingProviderGuid;
            ShippingProviderName = shippingProviderName;
            PaymentMethod = cartHeader.PaymentMethod;
            if (PaymentMethod != null)
                PaymentMethodName = GetPaymentMethodName(PaymentMethod);
            SetDibsInfo(cartHeader);
            SetAuthNetInfo(cartHeader);
            SetPayPalInfo(cartHeader);
            SetEEPGInfo(cartHeader);
            OrderNumber = cartHeader.CustomerReference;
        }

        private string GetPaymentMethodName(string paymentMethod)
        {
            return _paymentService.GetPaymentMethodName(paymentMethod);
        }

        public BillingAddress Billing { get; set; }
        public ShippingAddress Shipping { get; set; }
        public string ShippingProviderGuid { get; set; }
        public string ShippingProviderName { get; set; }
        public string PaymentMethod { get; set; }
        public string PaymentMethodName { get; set; }
        public bool HasError { get; set; }
        public Dictionary<string, string> ErrorMessages { get; set; }
        public AuthNet AuthNetInfo { get; private set; }
        public Dibs DibsInfo { get; private set; }
        public Paypal PayPalInfo { get; private set; }
        public EEPG EEPGInfo { get; private set; }
        public Totals OrderTotals { get; private set; }
        public List<ProductItem> ProductItems { get; private set; }
        public HeaderInfo Header { get; private set; }
        public string OrderNumber { get; set; }
        public bool PricesIncludeTax { get; set; }
        public CartCmsEditedFields CmsEditedFields { get; set; }

        private void SetAuthNetInfo(CartHeader cartHeader)
        {
            double amount = Math.Round(cartHeader.TotalInclTax, 2);
            string login = _configuration.Read("AUTHORIZENET_LOGIN");
            string txnKey = _configuration.Read("AUTHORIZENET_TRANSACTION_KEY");
            string strAmount = amount.ToString("F", CultureInfo.CreateSpecificCulture("en-US"));

            FingerprintFields fp = Authorizenet_Simlib.GetFingerprintFields(login, txnKey, strAmount, cartHeader.CustomerReference, _userCurrency);
            string lastName;
            AuthNetInfo = new AuthNet
                {
                    AuthNetUrl = "https://test.authorize.net/gateway/transact.dll", // TEST
                    // AuthNetUrl = "https://secure.authorize.net/gateway/transact.dll" // PRODUCTION
                    Amount = strAmount,
                    Timestamp = fp.Timestamp,
                    Sequence = fp.Sequence,
                    Fingerprint = fp.Fingerprint,
                    AdcUrl = AppUtil.ShopFullPath() + "/AuthorizeNetCallback/AuthNetCallback",
                    CurrencyCode = _userCurrency,
                    UserGuid = _expanditUserService.UserGuid,
                    CustomerReference = cartHeader.CustomerReference,
                    LoginId = login,
                    FirstName = GetFirstName(cartHeader.ContactName, out lastName),
                    LastName = lastName,
                    ShipToFirstName = GetFirstName(cartHeader.ShipToContactName, out lastName),
                    ShipToLastName = lastName
                };
        }

        private void SetDibsInfo(CartHeader cartHeader)
        {
            var cultureInfo = CurrencyFormatter.CurrencyIso2CultureInfo(_expanditUserService.CurrencyGuid);
            var multiplier = cultureInfo.NumberFormat.CurrencyDecimalDigits;
            var totalInclTax = Convert.ToInt64(cartHeader.TotalInclTax * Math.Pow(10, multiplier));
            var acceptedUrl = AppUtil.ShopFullPath() + "/Checkout/Accepted";
            var cancelUrl = AppUtil.ShopFullPath() + "/Checkout/Review";
            var callbackUrl = AppUtil.ShopFullPath() + "/DibsCallback/Callback";

            DibsInfo = new Dibs
                {
                    MerchantId = _configuration.Read("DIBS_MERCHANTID"),
                    AcceptReturnUrl = acceptedUrl,
                    PopupUrl = "https://sat1.dibspayment.com/dibspaymentwindow/entrypoint",
                    Currency = _userCurrency,
                    Amount = totalInclTax,
                    Language = _userLanguage,
                    OrderId = cartHeader.CustomerReference,
                    CancelReturnUrl = cancelUrl,
					CallbackUrl = callbackUrl,
                    UserGuid = cartHeader.UserGuid
                };
            if (_configuration.Read("DIBS_PAYMENT_TYPE") ==  "D2B")
            {
                DibsInfo.PopupUrl = "https://payment.architrade.com/paymentweb/start.action";
            }
        }

        private void SetPayPalInfo(CartHeader cartHeader)
        {
            string shopFullUrl = AppUtil.ShopFullPath().TrimEnd('/') + "/";

            double handling = cartHeader.ServiceChargeInclTax + cartHeader.ShippingAmountInclTax + cartHeader.HandlingAmountInclTax;

            string lastName;

            PayPalInfo = new Paypal
                {
                    ItemName = cartHeader.CustomerReference,
                    Cmd = _configuration.Read("cmd"),
                    Business = _configuration.Read("BusinessEmail"),
                    ReturnUrl = shopFullUrl + _configuration.Read("ReturnUrl"), // success url
                    NotifyUrl = shopFullUrl + _configuration.Read("NotifyUrl"), // callback url
                    ShopUrl = shopFullUrl + _configuration.Read("ShopUrl"), // continue shopping url
                    CancelUrl = shopFullUrl + _configuration.Read("CancelUrl"), // return to merchant url
                    CurrencyCode = cartHeader.CurrencyGuid,
                    PayPalUrl = _configuration.Read("PayPalUrl"),
                    SendToReturnUrl = _configuration.Read("SendToReturnURL"),
                    PayPalAmount = Math.Round(cartHeader.TotalInclTax, 2).ToString("F", CultureInfo.CreateSpecificCulture("en-US")),
                    HandlingCart = Math.Round(handling, 2).ToString("F", CultureInfo.CreateSpecificCulture("en-US")),
                    Custom = cartHeader.UserGuid,
                    CustomerReference = cartHeader.CustomerReference,
                    FirstName = GetFirstName(cartHeader.ContactName, out lastName),
                    LastName = lastName
                };
        }

        private void SetEEPGInfo(CartHeader cartHeader)
        {
            string shopFullUrl = AppUtil.ShopFullPath().TrimEnd('/') + "/EEPGProcessor/Process";

            string lastName;

            EEPGInfo = new EEPG
            {
                EEPGUrl = shopFullUrl,  
                Amount = Math.Round(cartHeader.TotalInclTax, 2).ToString("F", CultureInfo.CreateSpecificCulture("en-US")),
                CCN = "",
                CCV = "",
                ExpDate = "",
                FirstName = GetFirstName(cartHeader.ContactName, out lastName),
                LastName = lastName
            };

        }

        private string GetFirstName(string fullName, out string lastName)
        {
            if (string.IsNullOrEmpty(fullName))
            {
                lastName = null;
                return null;
            }
            string[] names = fullName.Split(new[] { ' ' });
            string firstName = null;
            lastName = names[names.Length - 1];
            for (int i = 0; i < names.Length - 1; i++)
            {
                var space = i > 0 ? " " : "";
                firstName += (space + names[i]);
            }
            return firstName;
        }

        private void SetShippingAddress(CartHeader cartHeader)
        {
            Shipping = new ShippingAddress
                {
                    Address1 = cartHeader.ShipToAddress1,
                    Address2 = cartHeader.ShipToAddress2,
                    CityName = cartHeader.ShipToCityName,
                    CompanyName = cartHeader.ShipToCompanyName,
                    ContactName = cartHeader.ShipToContactName,
                    CountryGuid = cartHeader.ShipToCountryGuid,
                    CountyName = cartHeader.ShipToCountyName,
                    CountryName = cartHeader.ShipToCountryName,
                    EmailAddress = cartHeader.ShipToEmailAddress,
                    StateName = cartHeader.ShipToStateName,
                    ZipCode = cartHeader.ShipToZipCode
                };
        }

        private void SetBillingAddress(CartHeader cartHeader)
        {
            Billing = new BillingAddress
                {
                    Address1 = cartHeader.Address1,
                    Address2 = cartHeader.Address2,
                    CityName = cartHeader.CityName,
                    CompanyName = cartHeader.CompanyName,
                    ContactName = cartHeader.ContactName,
                    CountryGuid = cartHeader.CountryGuid,
                    CountyName = cartHeader.CountyName,
                    CountryName = cartHeader.CountryName,
                    EmailAddress = cartHeader.EmailAddress,
                    StateName = cartHeader.StateName,
                    ZipCode = cartHeader.ZipCode
                };
        }

        private void SetHeaderInfo(CartHeader cartHeader)
        {
            Header = new HeaderInfo();
            Header.HeaderComment = cartHeader.HeaderComment;
            Header.CustomerPONumber = cartHeader.CustomerPONumber;
        }

        private void SetTotals(CartHeader cartHeader, string shippingProviderName)
        {
            ExpDictionary taxAmounts;
            var totalVatViewCalculator = new TotalVatViewCalculator();
            double shippingTaxPct = ExpanditLib2.ConvertToDbl(_configuration.Read("SHIPPING_TAX_PCT"));
            double total = totalVatViewCalculator.CalculateTotal(cartHeader, shippingTaxPct, out taxAmounts);
            OrderTotals = new Totals
                {
                    TotalInclTax = CurrencyFormatter.FormatCurrency(cartHeader.TotalInclTax, _userCurrency),
                    Total = CurrencyFormatter.FormatCurrency(total, _userCurrency),
                    ShippingHandlingProviderName = shippingProviderName,
                    PaymentFee = CurrencyFormatter.FormatAmount("0.0", _userCurrency)
                };

            string taxDisplayConfiguration = ExpanditLib2.CStrEx(_configuration.Read("SHOW_TAX_TYPE"));

            switch (taxDisplayConfiguration.ToUpper())
            {
                case "CUST":
                    OrderTotals.SubTotal = cartHeader.PricesIncludingVat
                        ? CurrencyFormatter.FormatCurrency(cartHeader.SubTotalInclTax, _userCurrency)
                        : CurrencyFormatter.FormatCurrency(cartHeader.SubTotal, _userCurrency);
                    OrderTotals.Discount = cartHeader.PricesIncludingVat
                        ? CurrencyFormatter.FormatCurrency(cartHeader.InvoiceDiscountInclTax, _userCurrency)
                        : CurrencyFormatter.FormatCurrency(cartHeader.InvoiceDiscount, _userCurrency);
                    OrderTotals.ServiceCharge = cartHeader.PricesIncludingVat ?
                        CurrencyFormatter.FormatCurrency(cartHeader.ServiceChargeInclTax, _userCurrency)
                        : CurrencyFormatter.FormatCurrency(cartHeader.ServiceCharge, _userCurrency);
                    OrderTotals.FreightCost = cartHeader.PricesIncludingVat
                        ? CurrencyFormatter.FormatCurrency(cartHeader.ShippingAmountInclTax.ToString("R"), _userCurrency)
                        : CurrencyFormatter.FormatAmount(cartHeader.ShippingAmount.ToString("R"), _userCurrency);
                    OrderTotals.HandlingFee = cartHeader.PricesIncludingVat
                        ? CurrencyFormatter.FormatCurrency(cartHeader.HandlingAmountInclTax.ToString("R"), _userCurrency)
                        : CurrencyFormatter.FormatAmount(cartHeader.HandlingAmount.ToString("R"), _userCurrency);
                    PricesIncludeTax = cartHeader.PricesIncludingVat;
                    break;
                case "INCL":
                    OrderTotals.SubTotal = CurrencyFormatter.FormatCurrency(cartHeader.SubTotalInclTax, _userCurrency);
                    OrderTotals.Discount = CurrencyFormatter.FormatCurrency(cartHeader.InvoiceDiscountInclTax, _userCurrency);
                    OrderTotals.ServiceCharge = CurrencyFormatter.FormatCurrency(cartHeader.ServiceChargeInclTax, _userCurrency);
                    OrderTotals.FreightCost = CurrencyFormatter.FormatCurrency(cartHeader.ShippingAmountInclTax.ToString("R"), _userCurrency);
                    OrderTotals.HandlingFee = CurrencyFormatter.FormatCurrency(cartHeader.HandlingAmountInclTax.ToString("R"), _userCurrency);
                    PricesIncludeTax = true;
                    break;
                default:
                    OrderTotals.SubTotal = CurrencyFormatter.FormatCurrency(cartHeader.SubTotal, _userCurrency);
                    OrderTotals.Discount = CurrencyFormatter.FormatCurrency(cartHeader.InvoiceDiscount, _userCurrency);
                    OrderTotals.ServiceCharge = CurrencyFormatter.FormatCurrency(cartHeader.ServiceCharge, _userCurrency);
                    OrderTotals.FreightCost = CurrencyFormatter.FormatAmount(cartHeader.ShippingAmount.ToString("R"), _userCurrency);
                    OrderTotals.HandlingFee = CurrencyFormatter.FormatAmount(cartHeader.HandlingAmount.ToString("R"), _userCurrency);
                    break;
            }

            var temp = new ExpDictionary();

            foreach (var keyValuePair in taxAmounts)
            {
                if (ExpanditLib2.ConvertToDbl(taxAmounts[keyValuePair.Key]) > 0)
                {
                    temp.Add(keyValuePair.Key, CurrencyFormatter.FormatCurrency(ExpanditLib2.ConvertToDbl(taxAmounts[keyValuePair.Key]), _userCurrency));
                }
            }

            OrderTotals.TaxAmounts = temp;
        }

        private void SetProductItems(CartHeader cartHeader)
        {
            foreach (ProductInfoNode productInfo in _productInfo)
            {
                if (productInfo.Prod == null)
                {
                    continue;
                }
                if (productInfo.Messages != null)
                {
                    HasError = true;
                    if (productInfo.Messages.Errors != null)
                    {
                        foreach (System.String e in productInfo.Messages.Errors)
                        {
                            if (!ErrorMessages.ContainsKey(e))
                            {
                                ErrorMessages.Add(e, e);
                            }
                        }
                    }
                }
                var lineTotalInclTax = productInfo.Line.TotalInclTax = productInfo.Line.LineTotal * (1 + ExpanditLib2.ConvertToDbl(productInfo.Line.TaxPct) / 100);
                var item = new ProductItem
                {
                    ShortDescription =
                            productInfo.Prod.GetPropertyValue("DESCRIPTION").
                                TruncateString(60,
                                    TruncateOptions.IncludeEllipsis |
                                    TruncateOptions.FinishWord),
                    LongDescription =
                            productInfo.Prod.GetPropertyValue("DESCRIPTION").
                                TruncateString(600,
                                    TruncateOptions.IncludeEllipsis |
                                    TruncateOptions.FinishWord),
                    ProductGuid = productInfo.Line.ProductGuid,
                    ProductName = productInfo.Prod.ProductName,
                    Picture1 = "~/" + productInfo.Prod.GetPropertyValue("PICTURE1"),
                    Picture2 = "~/" + productInfo.Prod.GetPropertyValue("PICTURE2"),
                    VariantCode = productInfo.Line.VariantCode,
                    VariantName = productInfo.Line.VariantName,
                    VersionGuid = productInfo.Line.VersionGuid,
                    LineGuid = productInfo.Line.LineGuid,
                    Quantity = productInfo.Line.Quantity,
                    LineDiscount = productInfo.Line.LineDiscount,
                    UserLanguage = _userLanguage,
                    PromotionalGift = productInfo.Line.PromotionalGift,
                };

                item.ProductLink = RouteManager.Route.CreateTemplateProductLink(productInfo.Prod.ProductGuid, 0, _userLanguage);

                string taxDisplayConfiguration = ExpanditLib2.CStrEx(_configuration.Read("SHOW_TAX_TYPE")).ToUpper();

                if (taxDisplayConfiguration == "CUST" && cartHeader.PricesIncludingVat)
                {
                    taxDisplayConfiguration = "INCL";
                }

                switch (taxDisplayConfiguration)
                {
                    case "INCL":
                        item.DisplayLineTotal = CurrencyFormatter.FormatCurrency(lineTotalInclTax, _userCurrency);
                        item.DisplayListPrice = CurrencyFormatter.FormatCurrency(
                            productInfo.Line.ListPriceInclTax, _userCurrency);
                        item.DisplayLineDiscountAmount = CurrencyFormatter.FormatCurrency(
                            productInfo.Line.LineDiscountAmountInclTax, _userCurrency);
                        break;
                    default:
                        item.DisplayLineTotal = CurrencyFormatter.FormatCurrency(
                            productInfo.Line.LineTotal, _userCurrency);
                        item.DisplayListPrice =
                            CurrencyFormatter.FormatCurrency(
                                productInfo.Line.ListPrice, _userCurrency);
                        item.DisplayLineDiscountAmount = CurrencyFormatter.FormatCurrency(
                            productInfo.Line.LineDiscountAmount, _userCurrency);
                        break;
                }

                ProductItems.Add(item);
            }
        }

        public class AuthNet
        {
            public string AuthNetUrl { get; set; }
            public string Amount { get; set; } // In USD
            public string AcceptReturnUrl { get; set; }
            public string CancelReturnUrl { get; set; }
            public string LoginId { get; set; }
            public string TransactionKey { get; set; }
            public string Sequence { get; set; }
            public string Timestamp { get; set; }
            public string Fingerprint { get; set; }
            public string AdcUrl { get; set; }
            public string CurrencyCode { get; set; }
            public string UserGuid { get; set; }
            public string CustomerReference { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string ShipToFirstName { get; set; }
            public string ShipToLastName { get; set; }
        }

        //

        public class BillingAddress
        {
            public string Address1 { get; set; }
            public string Address2 { get; set; }
            public string CityName { get; set; }
            public string CompanyName { get; set; }
            public string ContactName { get; set; }
            public string CountryGuid { get; set; }
            public string CountyName { get; set; }
            public string CountryName { get; set; }
            public string EmailAddress { get; set; }
            public string StateName { get; set; }
            public string ZipCode { get; set; }
        }

        public class Dibs
        {
            public string PopupUrl { get; set; }
            public string AcceptReturnUrl { get; set; }
            public string MerchantId { get; set; }
            public long Amount { get; set; }
            public string Currency { get; set; }
            public string Language { get; set; }
            public string OrderId { get; set; }
            public string CancelReturnUrl { get; set; }
	        public string CallbackUrl { get; set; }
            public string UserGuid { get; set; }
        }

        public class HeaderInfo
        {
            public string HeaderComment { get; set; }
            public string CustomerPONumber { get; set; }
        }

        public class Paypal
        {
            public string Cmd { get; set; }
            public string Business { get; set; }
            public string ItemName { get; set; }
            public string ReturnUrl { get; set; }
            public string NotifyUrl { get; set; }
            public string ShopUrl { get; set; }
            public string CancelUrl { get; set; }
            public string CurrencyCode { get; set; }
            public string OrderDict { get; set; }
            public string Shipping { get; set; }
            public string PayPalAmount { get; set; }
            public string PayPalHandlingAmount { get; set; }
            public string HandlingCart { get; set; }
            public string PayPalUrl { get; set; }
            public string SendToReturnUrl { get; set; }
            public string Custom { get; set; }
            public string CustomerReference { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string ShipToFirstName { get; set; }
            public string ShipToLastName { get; set; }
        }

        public class EEPG
        {
            public string CCN { get; set; }
            public string ExpDate { get; set; }
            public string CCV { get; set; }
            public string EEPGUrl { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string Amount { get; set; }
        }
        public class ProductItem
        {
            public string ProductGuid { get; set; }
            public string ProductName { get; set; }
            public string VariantCode { get; set; }
            public string VariantName { get; set; }
            public string DisplayListPrice { get; set; }
            public string ShortDescription { get; set; }
            public string LongDescription { get; set; }
            public string Picture1 { get; set; }
            public string Picture2 { get; set; }
            public string LineGuid { get; set; }
            public string VersionGuid { get; set; }
            public double Quantity { get; set; }
            public double LineDiscount { get; set; }
            public string UserLanguage { get; set; }
            public string DisplayLineTotal { get; set; }
            public string DisplayLineDiscountAmount { get; set; }
            public string ProductLink { get; set; }
            public bool PromotionalGift { get; set; }
        }

        public class ShippingAddress
        {
            public string Address1 { get; set; }
            public string Address2 { get; set; }
            public string CityName { get; set; }
            public string CompanyName { get; set; }
            public string ContactName { get; set; }
            public string CountryGuid { get; set; }
            public string CountyName { get; set; }
            public string CountryName { get; set; }
            public string EmailAddress { get; set; }
            public string StateName { get; set; }
            public string ZipCode { get; set; }
        }

        public class Totals
        {
            public string SubTotal { get; set; }
            public string Total { get; set; }
            public string TotalInclTax { get; set; }
            public string Discount { get; set; }
            public string ServiceCharge { get; set; }
            public string FreightCost { get; set; }
            public string HandlingFee { get; set; }
            public string PaymentFee { get; set; }
            public string ShippingHandlingProviderName { get; set; }
            public ExpDictionary TaxAmounts { get; set; }
        }

        public class CartCmsEditedFields
        {
            public string HtmlContent { get; set; }
        }
    }
}