﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using CmsPublic.ModelLayer.Models;
using EISCS.ExpandIT;
using EISCS.ExpandITFramework.Util;
using EISCS.Routing.Providers;
using EISCS.Shop.DO.Dto.Cart;
using EISCS.Shop.DO.Dto.Product;
using EISCS.Shop.DO.Interface;
using EISCS.Shop.DO.Service;
using DataFieldDefinition;
using AutoMapper;
using UIHelpers;

namespace ViewModels
{
    public class ProductsViewModel
    {
        public string UserLanguage { get; set; }
        public ListControl Control { get; set; }
        public bool HasError { get; set; }
        public GroupItem GroupItems { get; private set; }
        public List<ProductItem> ProductItems { get; private set; }
        public List<ProductItem> RelatedItems { get; private set; }
        private readonly string _customerCurrency;
        public bool IsAnonymous { get; set; }
        public Dictionary<string, string> ErrorMessages { get; set; }
        public bool HasCartAccess { get; set; }
        public string GroupTitle { get; set; }
        public CartHeader Header { get; private set; }
        public bool PricesIncludeTax { get; set; }

        public ProductsViewModel(IEnumerable<ProductInfoNode> productInfoNodes, IExpanditUserService expanditUserService, Group group, ListControl control, CartHeader header, IEnumerable<ProductInfoNode> relatedProductInfoNodes)
        {
            Header = header;
            _customerCurrency = expanditUserService.CurrencyGuid;
            ErrorMessages = new Dictionary<string, string>();
            UserLanguage = expanditUserService.LanguageGuid;
            SetGroupItems(group);
            ProductItems = new List<ProductItem>();
            SetProductItems(productInfoNodes, ProductItems);
            RelatedItems = new List<ProductItem>();
            SetProductItems(relatedProductInfoNodes, RelatedItems);

            Control = control;
            IsAnonymous = expanditUserService.UserType == UserClass.Anonymous;
            if (expanditUserService.UserAccess("Cart"))
            {
                HasCartAccess = true;
            }
        }

        private void SetGroupItems(Group group)
        {
            if (group != null)
            {
                GroupItems = new GroupItem
                    {
                        GroupGuid = @group.GroupGuid,
                        Description = @group.GetPropertyValue("DESCRIPTION"),
                        HtmlDescription = @group.GetPropertyValue("HTMLDESC").NormalizeHtmlContent()
                    };
                var title = group.GetPropertyValue("TITLE");
                var name = group.GetPropertyValue("NAME");
                GroupTitle = string.IsNullOrWhiteSpace(title) ? name : title;
            }
            else
            {
                GroupItems = new GroupItem();
            }
        }

        private void SetProductItems(IEnumerable<ProductInfoNode> productInfos, List<ProductItem> productItems)
        {
            if (productInfos == null)
            {
                return;
            }
            foreach (ProductInfoNode productInfo in productInfos)
            {
                if (productInfo.Prod != null)
                {
                    if (productInfo.Messages != null)
                    {
                        HasError = true;
                        if (productInfo.Messages.Errors != null)
                        {
                            foreach (string error in productInfo.Messages.Errors)
                            {
                                if (!ErrorMessages.ContainsKey(error))
                                {
                                    ErrorMessages.Add(error, error);
                                }
                            }
                        }
                    }

                    ProductItem item = Mapper.Map<Product, ProductItem>(productInfo.Prod);
                    item.ShortDescription =
                        productInfo.Prod.GetPropertyValue("DESCRIPTION").
                            TruncateString(60,
                                TruncateOptions.IncludeEllipsis |
                                TruncateOptions.FinishWord);
                    item.LongDescription =
                        productInfo.Prod.GetPropertyValue("DESCRIPTION").
                            TruncateString(600,
                                TruncateOptions.IncludeEllipsis |
                                TruncateOptions.FinishWord);
                    item.Picture1 = "~/" + (productInfo.Prod.GetPropertyValue("PICTURE1") ?? "images/big_camera.png");
                    item.Picture2 = "~/" + (productInfo.Prod.GetPropertyValue("PICTURE2") ?? "images/medium_camera.png");
                    item.VariantCode = productInfo.Line.VariantCode;
                    item.VariantName = productInfo.Line.VariantName;
                    item.ProductRelations = productInfo.ProductRelations;

                    item.ProductLink = RouteManager.Route.CreateTemplateProductLink(item.ProductGuid, GroupItems.GroupGuid, UserLanguage);

                    string taxDisplayConfiguration = ExpanditLib2.CStrEx(System.Configuration.ConfigurationManager.AppSettings["SHOW_TAX_TYPE"]);

                    switch (taxDisplayConfiguration.ToUpper())
                    {
                        case "CUST":
                            item.DisplayListPrice = Header.PricesIncludingVat
                            ? CurrencyFormatter.FormatCurrency(productInfo.Line.TotalInclTax, _customerCurrency)
                            : CurrencyFormatter.FormatCurrency(productInfo.Line.LineTotal, _customerCurrency);
                            PricesIncludeTax = Header.PricesIncludingVat;
                            break;
                        case "INCL":
                            item.DisplayListPrice = CurrencyFormatter.FormatCurrency(
                            productInfo.Line.TotalInclTax, _customerCurrency);
                            PricesIncludeTax = true;
                            break;
                        default:
                            item.DisplayListPrice =
                            CurrencyFormatter.FormatCurrency(
                                productInfo.Line.LineTotal, _customerCurrency);
                            break;
                    }

                    if (productInfo.VariantDict != null && productInfo.VariantDict.Any())
                    {
                        var list = new List<SelectListItem>();
                        foreach (var variant in productInfo.VariantDict)
                        {
                            var selectItem = new SelectListItem { Text = variant.Value.GetDisplayName(), Value = variant.Value.VariantCode };
                            list.Add(selectItem);
                        }
                        item.VariantSelect = new SelectList(list, "Value", "Text", productInfo.Line.VariantCode);
                    }

                    productItems.Add(item);
                }
            }
        }

        public class GroupItem
        {
            public int GroupGuid { get; set; }
            public string Description { get; set; }
            public string HtmlDescription { get; set; }
        }

        public class ProductItem : ProductTableColumns
        {
            public string DisplayListPrice { get; set; }
            public string ShortDescription { get; set; }
            public string LongDescription { get; set; }
            public string Picture1 { get; set; }
            public string Picture2 { get; set; }
            public string VariantCode { get; set; }
            public string VariantName { get; set; }
            public bool IsFavorite { get; set; }
            public SelectList VariantSelect { get; set; }
            public List<ProductRelation> ProductRelations { get; set; }
            public string ProductLink { get; set; }
        }
    }
}