﻿using System.Collections.Generic;
using System.Web.UI.WebControls;
using CmsPublic.ModelLayer.Models;
using EISCS.Catalog.Classes;
using EISCS.Catalog.Logic;
using EISCS.ExpandITFramework.Util;

namespace ViewModels.Templates
{
    /// <summary>
    /// Summary description for ShopFrontPageViewModel
    /// </summary>
    public class ShopFrontPageViewModel
    {
        private readonly Group _group;

        public string Name { get; set; }
        public string Title { get; set; }

        public Box Box1 { get; set; }
        public Box Box2 { get; set; }
        public Box Box3 { get; set; }
        public Box Box4 { get; set; }
        public Box Box5 { get; set; }
        public Box Box6 { get; set; }
        public Box Box7 { get; set; }

        public List<ImageObject> ImageObjects { get; private set; }

        public ShopFrontPageViewModel(Group group)
        {
            _group = group;
            Initialize();
            Name = _group.GetPropertyValue("NAME");
            Title = _group.GetPropertyValue("TITLE");
            if (string.IsNullOrWhiteSpace(Title))
            {
                Title = Name;
            }
            SetCarousel();
        }

        private void SetCarousel()
        {
            var multiImageHelper = new MultiImageHelper();
            multiImageHelper.PropToObjects(_group.GetPropertyValue("MULTIIMAGE"));
            ImageObjects = multiImageHelper.ImageObjects;

            foreach (var imageObject in ImageObjects)
            {
                imageObject.Img = AppUtil.PictureLink(imageObject.Img);
                imageObject.Link = AppUtil.PictureLink(imageObject.Link);
            }
        }

        public class Box
        {
            public string Header { get; set; }
            public string Link { get; set; }
            public string LinkText { get; set; }
            public string Image { get; set; }
            public string Description { get; set; }
            public string Target { get; set; }
        }

        protected void Initialize()
        {
            Box1 = GetBoxData("BOX1");
            Box2 = GetBoxData("BOX2");
            Box3 = GetBoxData("BOX3");
            Box4 = GetBoxData("BOX4");
            Box5 = GetBoxData("BOX5");
            Box6 = GetBoxData("BOX6");
            Box7 = GetBoxData("BOX7");
        }

        public void OnlyShowHyperlinkIfTextExists(HyperLink link, string propertyName)
        {
            string propertyValue = _group.GetPropertyValue(propertyName);
            if (string.IsNullOrEmpty(propertyValue))
            {
                link.Visible = false;
            }
        }
        private Box GetBoxData(string boxName)
        {
            return new Box
            {
                Header = _group.GetPropertyValue(boxName + "HEADER"),
                Link = _group.GetPropertyValue(boxName + "LINK").NormalizeLinkString(),
                LinkText = _group.GetPropertyValue(boxName + "LINKTEXT"),
                Image = AppUtil.PictureLink(_group.GetPropertyValue(boxName + "IMAGE")),
                Description = _group.GetPropertyValue(boxName + "DESC").NormalizeHtmlContent(),
                Target = GetTarget(_group.GetPropertyValue(boxName + "LINK").NormalizeLinkString())
            };
        }

        private string GetTarget(string urlValue)
        {
            if (string.IsNullOrEmpty(urlValue))
            {
                return "_blank";
            }

            if (urlValue.StartsWith("http:") || urlValue.StartsWith("https:"))
            {
                return "_blank";
            }
            return urlValue.StartsWith("www.") ? "_blank" : "_self";
        }

    }
}