﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ViewModels.Templates 
{
	public class AboutTemplateViewModel 
	{
		public string HtmlDesc { get; set; }
		public string Name { get; set; }
        public string Title { get; set; }
	}
}