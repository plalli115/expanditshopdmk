﻿namespace ViewModels.Templates
{
    public class ContactViewModel
    {
        public string Map { get; set; }
        public string Name { get; set; }
        public string Title { get; set; }
        public string BOX4DESC { get; set; }
        public string BOX5DESC { get; set; }
        public string BOX6DESC { get; set; }
    }
}