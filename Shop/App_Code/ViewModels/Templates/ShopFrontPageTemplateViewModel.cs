﻿using CmsPublic.ModelLayer.Models;
using EISCS.ExpandITFramework.Util;

namespace ViewModels.Templates
{
	public class ShopFrontPageTemplateViewModel
	{
		private readonly Group _group;

		public string Name { get; set; }
        public Box Box1 { get; set; }
        public Box Box2 { get; set; }
        public Box Box3 { get; set; }
        public Box Box4 { get; set; }
        public Box Box5 { get; set; }
        public Box Box6 { get; set; }
        public Box Box7 { get; set; }

		public ShopFrontPageTemplateViewModel(Group group)
		{
			_group = group;
			Name = _group.GetPropertyValue("NAME");
			Box1 = GetBoxData("BOX1");
			Box2 = GetBoxData("BOX1");
			Box3 = GetBoxData("BOX1");
			Box4 = GetBoxData("BOX1");
			Box5 = GetBoxData("BOX1");
			Box6 = GetBoxData("BOX1");
			Box7 = GetBoxData("BOX1");
		}

		private Box GetBoxData(string boxName)
		{
			return new Box
			{
				Header = _group.GetPropertyValue(boxName + "HEADER"),
				Link = _group.GetPropertyValue(boxName + "LINK").NormalizeLinkString(),
				LinkText = _group.GetPropertyValue(boxName + "LINKTEXT"),
				Image = AppUtil.PictureLink(_group.GetPropertyValue(boxName + "IMAGE")),
				Description = _group.GetPropertyValue(boxName + "DESC").NormalizeHtmlContent(),
				Target = GetTarget(_group.GetPropertyValue(boxName + "LINK").NormalizeLinkString())
			};
		}

		private string GetTarget(string urlValue)
		{
			if (string.IsNullOrEmpty(urlValue) || (urlValue.StartsWith("http:") || urlValue.StartsWith("https:")))
			{
				return "_blank";
			}
			return urlValue.StartsWith("www.") ? "_blank" : "_self";
		}

		public class Box {
			public string Header { get; set; }
			public string Link { get; set; }
			public string LinkText { get; set; }
			public string Image { get; set; }
			public string Description { get; set; }
			public string Target { get; set; }
		}
	}
}