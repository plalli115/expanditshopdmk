﻿using System.Configuration;
using EISCS.Shop.BO.BusinessLogic;
using EISCS.Shop.DO.Dto;
using Resources;

namespace ViewModels.Ledger
{
    /// <summary>
    ///     Summary description for CustomerLedgerEntryViewModel
    /// </summary>
    public class CustomerLedgerEntryViewModel
    {
        public CustomerLedgerEntryViewModel()
        {
            MailTemplateText = Language.MESSAGE_CUSTOMER_LEDGER_EMAIL_REQUEST_BODY;
            MailSubject = Language.MESSAGE_CUSTOMER_LEDGER_EMAIL_REQUEST_SUBJECT;
            MailAddress = ConfigurationManager.AppSettings["CUSTOMER_LEDGER_REQUEST_EMAIL_ADDRESS"];
        }

        public string MailAddress { get; set; }
        public string MailSubject { get; set; }
        public LedgerEntryContainer LedgerEntryContainer { get; set; }
        public string MailTemplateText { get; set; }
        public UserTable User { get; set; }
        public string GetMailText()
        {
            return MailTemplateText.Replace("%USER_INFO%",
                string.Format("{0}%NEWLINE%{1}%NEWLINE%{2}%NEWLINE%{3} {4}%NEWLINE%{5}: {6}%NEWLINE%", User.ContactName, User.CompanyName, User.Address1, User.ZipCode, User.CityName, Language.LABEL_CUSTOMER_NO, User.CustomerGuid));
        }
    }
}