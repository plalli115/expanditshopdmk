﻿namespace ViewModels.Ledger
{
    /// <summary>
    /// Summary description for DisplayLedgerEntryViewModel
    /// </summary>
    public class DisplayLedgerEntryViewModel
    {
        public CustomerLedgerEntryViewModel LedgerEntryViewModel { get; set; }
        public string CmsEditedHtml { get; set; }
    }
}