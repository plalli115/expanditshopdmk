﻿using System;

namespace ViewModels.Project
{
    public class ProjectViewModel
    {
        public string ProjectGuid { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime StartingDate { get; set; }
        public DateTime EndingDate { get; set; }
        public int ProjectStatusGuid { get; set; }
        public string ProjectManagerGujid { get; set; }
        public string DepartmentGuid { get; set; }
        public string ProjectDimentionGuid { get; set; }
        public string JobPostingGroup { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string ProjectName { get; set; }
        public string CustomerGuid { get; set; }
        public float BudgetedResGirQty { get; set; }

        public string ProjectLabel { get; set; }
    }
}