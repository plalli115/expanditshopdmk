﻿using System;
using EISCS.Shop.DO.BAS.Dto;

namespace ViewModels.ServiceOrderComment
{
    public class CommentLineCreateViewModel
    {
        public int BASGuid { get; set; }
        public string UserName { get; set; }
        public string Comment { get; set; }
        public DateTime? DateCreated { get; set; }
        public ServiceOrderCommentLineType CommentType { get; set; }
    }
}