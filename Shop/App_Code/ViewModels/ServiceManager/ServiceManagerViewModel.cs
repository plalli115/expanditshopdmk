﻿namespace ViewModels.ServiceManager
{
    public class ServiceManagerViewModel
    {
        public string UserGuid { get; set; }
        public string FullName { get; set; }
        public string DepartmentGuid { get; set; }
        public string ServiceManagerLabel { get; set; }
    }
}