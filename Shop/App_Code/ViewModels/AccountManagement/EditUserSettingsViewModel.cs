﻿using System.Collections.Generic;
using System.Linq;

namespace ViewModels.AccountManagement
{
    public class EditUserSettingsViewModel
    {
        public EditUserSettingsViewModel()
        {
            CustomerGroups = Enumerable.Empty<CustomerGroupViewModel>();
        }

        public string Name { get; set; }
        public string Email { get; set; }
        public string Id { get; set; }
        public IEnumerable<CustomerGroupViewModel> CustomerGroups { get; set; }
        public EditUserSettingsFormModel FormModel { get; set; }
    }
}