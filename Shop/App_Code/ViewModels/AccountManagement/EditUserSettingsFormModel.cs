using System.Collections.Generic;
using EISCS.Shop.DO.Dto;

namespace ViewModels.AccountManagement
{
    public class EditUserSettingsFormModel
    {
        public string UserId { get; set; }
        public int? CustomerGroupId { get; set; }
        public string CustomerId { get; set; }
        public bool ChooseProjects { get; set; }
        public IEnumerable<string> ProjectId { get; set; }
        public bool CanCreateServiceOrders { get; set; }
        public bool CanSelectProjects { get; set; }
    }
}