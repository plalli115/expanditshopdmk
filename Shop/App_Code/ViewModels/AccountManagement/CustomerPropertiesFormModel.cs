﻿using System.Collections.Generic;
using System.Linq;

namespace ViewModels.AccountManagement
{
    public class CustomerPropertiesFormModel
    {
        public CustomerPropertiesFormModel()
        {
            TaskId = Enumerable.Empty<string>();
            ServiceManagerId = Enumerable.Empty<string>();
            TechnicianId = Enumerable.Empty<string>();
        }

        public IEnumerable<string> TaskId { get; set; }
        public IEnumerable<string> ServiceManagerId { get; set; }
        public IEnumerable<string> TechnicianId { get; set; }

        public string CustomerId { get; set; }
    }
}