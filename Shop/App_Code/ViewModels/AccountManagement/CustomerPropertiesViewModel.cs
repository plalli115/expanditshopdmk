﻿using System.Collections.Generic;

namespace ViewModels.AccountManagement
{
    public class CustomerPropertiesViewModel
    {
        public string CustomerId { get; set; }
        public string CustomerName { get; set; }

        public IEnumerable<CustomerPropertyViewModel> Tasks { get; set; }
        public IEnumerable<CustomerPropertyViewModel> ServiceManagers { get; set; }
        public IEnumerable<CustomerPropertyViewModel> Technicians { get; set; }

        public class CustomerPropertyViewModel
        {
            public string Id { get; set; }
            public string Value { get; set; }
            public bool Selected { get; set; }
        }
    }
}