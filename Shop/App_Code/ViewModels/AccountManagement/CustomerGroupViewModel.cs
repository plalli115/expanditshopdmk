﻿namespace ViewModels.AccountManagement
{
    public class CustomerGroupViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool Selected { get; set; }
    }
}