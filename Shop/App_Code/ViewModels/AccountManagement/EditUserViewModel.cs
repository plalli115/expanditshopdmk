﻿using System.ComponentModel.DataAnnotations;

namespace ViewModels.AccountManagement
{
    public class EditUserViewModel
    {
        public EditUserViewModel()
        {
            NewUser = true;
        }

        [Required]
        [Display(ResourceType = typeof(Resources.Language), Name = "SO_LABEL_NAME")]
        public string Name { get; set; }

        [Display(ResourceType = typeof(Resources.Language), Name = "SO_LABEL_EMAIL")]
        public string Email { get; set; }
        
        [Display(ResourceType = typeof(Resources.Language), Name = "SO_LABEL_PASSWORD")]
        public string Password { get; set; }

        public string Id { get; set; }
        public bool NewUser { get; set; }
    }
}