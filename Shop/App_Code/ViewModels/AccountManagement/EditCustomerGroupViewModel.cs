﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace ViewModels.AccountManagement
{
    public class EditCustomerGroupViewModel
    {
        public EditCustomerGroupViewModel()
        {
            Customers = Enumerable.Empty<Customer>();
        }

        public int Id { get; set; }

        [Required(ErrorMessage = "Indtast navn")]
        [StringLength(50)]
        [Display(ResourceType = typeof(Resources.Language), Name = "SO_LABEL_NAME")]
        public string Name { get; set; }

        public bool NewCustomer { get { return Id == 0; } }

        public IEnumerable<Customer> Customers { get; set; }


        public class Customer
        {
            public string Id { get; set; }
            public string CompanyName { get; set; }
        }
    }

}