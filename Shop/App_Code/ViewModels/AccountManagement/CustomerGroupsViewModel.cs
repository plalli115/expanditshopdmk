﻿using System.Collections.Generic;
using System.Linq;
using ViewModels.User;

namespace ViewModels.AccountManagement
{
    public class CustomerGroupsViewModel
    {
        public CustomerGroupsViewModel()
        {
            CustomerGroups = Enumerable.Empty<CustomerGroupViewModel>();
            Users = Enumerable.Empty<UserViewModel>();
        }

        public IEnumerable<CustomerGroupViewModel> CustomerGroups { get; set; }
        public IEnumerable<UserViewModel> Users { get; set; }
    }

    //public class UserViewModel
    //{
    //    public string Id { get; set; }

    //    public string Name { get; set; }
    //}
}