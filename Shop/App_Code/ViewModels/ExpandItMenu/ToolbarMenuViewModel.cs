﻿namespace ViewModels.ExpandItMenu
{
    public class ToolbarMenuViewModel
    {
        public string CurrentUserName { get; set; }
        public bool ShowSearch { get; set; }
    }
}