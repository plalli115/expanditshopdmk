﻿using System.Collections.Generic;
using EISCS.Shop.BO.BAS;

namespace ViewModels.ExpandItMenu
{
    public class ExpandItMenuViewModel
    {
        public int TotalAmount { get; set; }
        public string TotalAmountText { get; set; }
        public string NumberOfItemsText { get; set; }

        public List<MenuItem> MenuItems { get; set; }

        //public PortalUser CurrentUser { get; set; }

        public class MenuItem
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public string Url { get; set; }
            public bool Selected { get; set; }
            public List<MenuItem> Children { get; set; }
        }
    }
}