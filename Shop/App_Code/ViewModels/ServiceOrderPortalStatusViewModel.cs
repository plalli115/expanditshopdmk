﻿namespace ViewModels
{
    public class ServiceOrderPortalStatusViewModel
    {
        public string ServiceOrderPortalStatusId { get; set; }
        public string ServiceOrderId { get; set; }
        public string PortalStatusId { get; set; }
        public string StatusCode { get; set; }
        public string StatusLabel { get; set; }
    }
}