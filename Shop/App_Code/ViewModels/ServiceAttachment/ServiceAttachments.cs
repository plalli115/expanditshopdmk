﻿using System.Collections.Generic;

namespace ViewModels.ServiceAttachment
{
    public class ServiceAttachments
    {
        public int ServiceOrderBasGuid { get; set; }
        public string ServiceOrderGuid { get; set; }
        public bool DeleteSuccess { get; set; }
        public IEnumerable<ServiceAttachmentListViewModel> AttachmentList { get; set; }
    }
}