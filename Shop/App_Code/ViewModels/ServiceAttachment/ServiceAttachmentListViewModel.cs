﻿using System;

namespace ViewModels.ServiceAttachment
{
    public class ServiceAttachmentListViewModel
    {
        public string ServiceOrderGuid { get; set; }
        public int BASGuid { get; set; }
        public string FileName { get; set; }
        public int FileSize { get; set; }
        public string Description { get; set; }
        public DateTime SavedTime { get; set; }
    }
}