﻿namespace ViewModels.ServiceOrderType
{
    public class ServiceOrderTypeViewModel
    {
        public string Code { get; set; }
        public string Description { get; set; }
    }
}