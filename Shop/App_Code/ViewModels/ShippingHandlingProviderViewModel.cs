﻿using System.Collections.Generic;
using EISCS.Shop.DO.Dto;

namespace ViewModels
{
	public class ShippingHandlingProviderViewModel
	{
		private readonly ShippingHandlingProvider _provider;

		public ShippingHandlingProviderViewModel(ShippingHandlingProvider provider)
		{
			_provider = provider;
			ShippingHandlingProviderGuid = _provider.ShippingHandlingProviderGuid;
			ProviderName = _provider.ProviderName;
			ProviderDescription = _provider.ProviderDescription;
			SortIndex = _provider.SortIndex;
			IsPickUp = _provider.IsPickUp;
		}

		public string ShippingHandlingProviderGuid { get; set; }
		public string ProviderName { get; set; }
		public string ProviderDescription { get; set; }
		public int SortIndex { get; set; }
		public bool IsPickUp { get; set; }
		public List<ShippingHandlingPricesViewModel> ShippingHandlingPrices { get; set; } 
	}

	public class ShippingHandlingPricesViewModel {
		public string ShippingHandlingProviderGuid { get; set; }
		public string PreviousCalculatedValue { get; set; }
		public string CalculatedValue { get; set; }
		public string ShippingAmount { get; set; }
		public string HandlingAmount { get; set; }
	}
}