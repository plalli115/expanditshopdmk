﻿using System;

namespace ViewModels
{
    public class KPIViewModel
    {
        public DateTime EntryDate { get; set; }
        public DateTime EndDate { get; set; }
        public int TotalLogins { get; set; }
        public int TotalOrders { get; set; }
        public double AvgOrderSize { get; set; }
        public double TotalRevenueWithoutVAT { get; set; }
        public double AvgItemPrize { get; set; }
        public int TotalItems { get; set; }
        public string TotalRevenueWithoutVATString { get; set; }
        public string AvgItemPrizeString { get; set; }
        public int NewUsers { get; set; }
        public string AvgOrderSizeString { get; set; }
    }
}