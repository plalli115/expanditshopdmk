﻿using System.Collections.Generic;
using Controllers;
using Controllers.Templates;
using EISCS.Shop.DO.Dto;
using EISCS.Shop.DO.Dto.Cart;
using FormModels.ShippingAddress;

namespace ViewModels
{
	public class CheckoutViewModel
	{
		public List<ShippingAddressItemFormModel> Addresses { get; set; }
		public List<ShippingHandlingProviderViewModel> Providers { get; set; }
		public List<PaymentTypeTable> PaymentTypeList { get; set; }
		public CartHeader CartHeader { get; set; }
		public double Total { get; set; }
		public string TotalAmountText { get; set; }
		public string UserType { get; set; }
		public Dictionary<string, string> CountryList { get; set; }
		public string ShippingHandlingAmount { get; set; }
		public B2BShippingAddressConfig B2BShippingAddressConfig { get; set; }
		public string DefaultPaymentProvider { get; set; }
		public int Step { get; set; }
		public CartViewModel CartViewModel { get; set; }
		public AnonymousBillingAndShippingAddressForm AnonymousBillingAndShippingAddressForm { get; set; }
        public string CmsEditedHtml { get; set; }
	}
}