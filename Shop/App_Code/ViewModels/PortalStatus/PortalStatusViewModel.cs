﻿namespace ViewModels.PortalStatus
{
    public class PortalStatusViewModel
    {
        public string StatusGuid { get; set; }
        public string StatusName { get; set; }
        public bool Display { get; set; }
        public int SortIndex { get; set; }
        public int Precedence { get; set; }
        public string ExternalStatusGuid { get; set; }
        public bool IsActive { get; set; }
        public bool SetStartTime { get; set; }
        public bool SyncToClient { get; set; }
        public bool AllowReschedule { get; set; }
        public bool AssignmentComplete { get; set; }
        public bool JobComplete { get; set; }
        public bool NeedReschedule { get; set; }
        public bool NeedReassign { get; set; }
        public bool ShowOnClient { get; set; }
        public bool AllowDataEntry { get; set; }
        public bool AllowDelete { get; set; }
        public bool RecordTime { get; set; }

        public bool AllowRoleBasic { get; set; }
        public bool AllowRoleExtended { get; set; }
        public bool AllowRoleSuper { get; set; }
    }
}