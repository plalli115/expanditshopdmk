﻿namespace ViewModels.Cms
{
    /// <summary>
    /// Summary description for ImpersonationModel
    /// </summary>
    public class ImpersonationViewModel
    {
        public string RemoteUrl { get; set; }
        public string ReturnUrl { get; set; }
    }
}