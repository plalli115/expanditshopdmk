﻿
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Resources;

namespace ViewModels.Customer
{
    public class CustomerViewModel
    {
        public string CustomerLabel { get; set; }
        public string CustomerGuid { get; set; }

        [Display(ResourceType = typeof(Language), Name = "LABEL_EMAIL")]
        public string EmailAddress { get; set; }
        public string CompanyName { get; set; }
        public string BillToCustomerGuid { get; set; }
        public string BillToCompanyName2 { get; set; }
        public string ShipToCompanyName { get; set; }
        public string ShipToCompanyName2 { get; set; }
        public string ContactName { get; set; }

        [Display(ResourceType = typeof(Language), Name = "LABEL_USERDATA_ADDRESS_1")]
        public string Address1 { get; set; }

        [Display(ResourceType = typeof(Language), Name = "LABEL_USERDATA_ADDRESS_2")]
        public string Address2 { get; set; }

        [Display(ResourceType = typeof(Language), Name = "LABEL_USERDATA_ZIPCODE")]
        public string ZipCode { get; set; }

        [Display(ResourceType = typeof(Language), Name = "LABEL_USERDATA_CITY")]
        public string CityName { get; set; }
        public bool EnableLogin { get; set; }

        [Display(ResourceType = typeof(Language), Name = "LABEL_USERDATA_COUNTRY")]
        public string CountryGuid { get; set; }

        public Dictionary<string, string> CountryList { get; set; }
    }
}