﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;
using EISCS.Shop.DO.BAS.Dto;
using EISCS.Shop.DO.Dto;
using FormModels.ServiceOrderComment;
using ViewModels.PortalStatus;
using ViewModels.ServiceAttachment;
using ViewModels.ServiceItem;
using ViewModels.ServiceOrderCommentLine;
using ViewModels.ServiceOrderType;

namespace ViewModels.ServiceOrder
{
    public class ServiceOrderDetailViewModel
    {
        public string ServiceOrderGuid { get; set; }
        public string ServiceHeaderGuid { get; set; }
        public string ServiceOrderType { get; set; }
        public string JobDescription { get; set; }
        public int ItemType { get; set; }
        public int BASGuid { get; set; }
        public string ShiptoContactName { get; set; }
        public string ShiptoAddress { get; set; }
        public string ShiptoAddress2 { get; set; }
        public string ShiptoCityName { get; set; }
        public string ShiptoEmailAddress { get; set; }
        public string ShiptoPhoneNo { get; set; }
        public string ShiptoPhoneNo2 { get; set; }
        public string ShiptoZipCode { get; set; }
        public DateTime SelectedStartDate { get; set; }
        public DateTime SelectedFinishDate { get; set; }
        public string BillableStatus { get; set; }
        public string StatusCode { get; set; }
        public string RepairStatuscode { get; set; }
        public string DepartmentGuid { get; set; }

        [Required(ErrorMessage = "Fil mangler. Vælg venligst en fil")]
        public HttpPostedFileBase File { get; set; }

        [StringLength(800, ErrorMessage = "Kommentar er for lang! Max. 800 tegn tilladt.")]
        public string AttachmentComment { get; set; }

        public bool ShowAddress2 { get; set; }
        public bool ShowServiceOrderType { get; set; }
        public bool ShowServiceOrderStatus { get; set; }
        public bool HasReport { get; set; }

        public UserTable User { get; set; }

        public ServiceOrderCommentLineViewModel CreatedComment { get; set; }
        public ServiceOrderCommentFormModel CommentForm { get; set; }

        public IEnumerable<ServiceOrderCommentLineViewModel> AddedComments { get; set; }
        public ServiceOrderTypeViewModel ServiceOrderTypeItem { get; set; }
        public DepartmentItem SelectedDepartment { get; set; }
        public PortalStatusViewModel SelectedStatus { get; set; }

        public IEnumerable<ServiceOrderCommentLineViewModel> Comments { get; set; }
        public IEnumerable<ServiceAttachmentListViewModel> Attachments { get; set; }
        public IEnumerable<PortalStatusViewModel> ServiceOrderPlanningStatus { get; set; }

        public IEnumerable<ReportHeader> Reports { get; set; }

        public ServiceItemViewModel ServiceItem { get; set; }
    }
}