﻿using System;
using EISCS.Shop.DO.BAS.Dto;

namespace ViewModels.ServiceOrder
{
    public class ServiceOrderListViewModel
    {
        public string BASGuid { get; set; }
        public string ServiceOrderGuid { get; set; }
        public string ServiceHeaderGuid { get; set; }
        public DateTime? StartingDate { get; set; }
        public DateTime? FinishingDate { get; set; }
        public DateTime? BASReceivedTime { get; set; }
        public string JobDescription { get; set; }
        public string StatusCode { get; set; }
        public string ResponsibleUserGuid { get; set; }
        public string DepartmentGuid { get; set; }
        public string ServiceOrderType { get; set; }

        public DepartmentItem SelectedDepartment { get; set; }
        public ServiceOrderTypeItem ServiceOrderTypeItem { get; set; }
        public PortalStatusItem SelectedStatus { get; set; }
    }
}