﻿using EISCS.Shop.DO.Dto;

namespace ViewModels.ServiceOrder
{
    public class ReportViewModel
    {
        public string Title { get; set; }
        public string HtmlContent { get; set; }

        public UserTable CurrentUser { get; set; }
    }
}