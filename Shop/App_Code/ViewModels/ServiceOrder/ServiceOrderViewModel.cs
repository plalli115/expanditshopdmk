﻿using System.Collections.Generic;
using EISCS.Shop.BO.BAS;
using EISCS.Shop.DO.Dto;

namespace ViewModels.ServiceOrder
{
    public class ServiceOrderViewModel
    {
        public string Title { get; set; }
        public bool ShowNoItemsInListPanel { get; set; }
        public bool ShowFinishedOrders { get; set; }
        public string HtmlContent { get; set; }

        public UserTable CurrentUser { get; set; }
        public List<ServiceOrderListViewModel> ServiceOrderItems { get; set; }
    }
}