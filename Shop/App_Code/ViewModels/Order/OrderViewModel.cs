﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using EISCS.Shop.DO.Dto;
using ExpandIT;
using ViewModels.CustomerAddress;

namespace ViewModels.Order
{
    /**
     * A view represenation of an order... 
     */

    public class OrderViewModel
    {
        public List<OrderLineViewModel> Lines { get; set; }
        public CustomerAddressViewModel ShippingAddress { get; set; }
        public CustomerAddressViewModel BillingAddress { get; set; }
        public ShippingHandlingProvider ShippingHandlingProvider { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", NullDisplayText = "-")]
        public DateTime CreatedDate { get; set; }
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", NullDisplayText = "-")]
        public DateTime HeaderDate { get; set; }
        public string CurrencyGuid { get; set; }
        public string CustomerPONumber { get; set; }
        public string HeaderComment { get; set; }
        public string HeaderGuid { get; set; }
        public double InvoiceDiscount { get; set; }
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", NullDisplayText = "-")]
        public DateTime ModifiedDate { get; set; }
        public string MultiCartDescription { get; set; }
        public string MultiCartStatus { get; set; }
        public double ServiceCharge { get; set; }
        public double SubTotal { get; set; }
        public double TaxAmount { get; set; }
        public string TaxCode { get; set; }
        public double TotalInclTax { get; set; }
        public string UserGuid { get; set; }
        public double Total { get; set; }
        public string CustomerReference { get; set; }
        public string ShippingHandlingProviderGuid { get; set; }        
        public bool IsCalculated { get; set; }
        public string VersionGuid { get; set; }
        public double SubTotalInclTax { get; set; }
        public double ServiceChargeInclTax { get; set; }
        public double InvoiceDiscountInclTax { get; set; }
        public double ShippingAmount { get; set; }
        public double ShippingAmountInclTax { get; set; }
        public double HandlingAmount { get; set; }
        public double HandlingAmountInclTax { get; set; }
        public string PaymentStatus { get; set; }
        public string PaymentError { get; set; }
        public string PaymentMethod { get; set; }
        public double PaymentFeeAmount { get; set; }
        public double PaymentFeeAmountInclTax { get; set; }
        public string InvoiceDiscountPct { get; set; }
        public string TaxPct { get; set; }
        public bool PricesIncludingVat { get; set; }
        public string VatTypes { get; set; }
        public ExpDictionary TaxAmounts { get; set; }
    }
}