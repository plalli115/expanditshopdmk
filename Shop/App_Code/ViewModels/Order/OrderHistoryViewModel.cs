﻿using System.Collections.Generic;

namespace ViewModels.Order
{
    /// <summary>
    /// Summary description for OrderHistoryViewModel
    /// </summary>
    public class OrderHistoryViewModel
    {
        public OrderViewModel OrderView { get; set; }
        public List<OrderViewModel> OrderViewList { get; set; }
        public string CmsEditedHtml { get; set; }
    }
}