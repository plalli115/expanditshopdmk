﻿namespace ViewModels.Order
{
    /// <summary>
    ///     Summary description for OrderLineViewModel
    /// </summary>
    public class OrderLineViewModel
    {
        public string HeaderGuid { get; set; }
        public string CurrencyGuid { get; set; }
        public string LineComment { get; set; }
        public double LineDiscountAmount { get; set; }
        public double LineDiscount { get; set; }
        public string LineGuid { get; set; }
        public string LineNumber { get; set; }
        public double LineTotal { get; set; }
        public double ListPrice { get; set; }
        public string ProductGuid { get; set; }
        public string ProductName { get; set; }
        public double Quantity { get; set; }
        public double TaxAmount { get; set; }
        public double TotalInclTax { get; set; }
        public string VariantCode { get; set; }
        public bool IsCalculated { get; set; }
        public string VersionGuid { get; set; }
        public double ListPriceInclTax { get; set; }
        public double LineDiscountAmountInclTax { get; set; }
        public string VariantName { get; set; }
        public string TaxPct { get; set; }
    }
}