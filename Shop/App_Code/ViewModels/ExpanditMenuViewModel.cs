﻿using System.Collections.Generic;
using System.Globalization;
using EISCS.ExpandIT;
using EISCS.Shop.DO.Dto.Cart;
using EISCS.Shop.DO.Interface;
using EISCS.Shop.DO.Service;
using Resources;

namespace ViewModels
{
    /// <summary>
    /// Summary description for ExpanditMenuViewModel
    /// </summary>
    public class ExpanditMenuViewModel
    {
        public IEnumerable<MenuItem> MenuItems { get; set; }
        public int TotalAmount { get; set; }
        public string TotalAmountText { get; set; }
        public string NumberOfItemsText { get; set; }

        public ExpanditMenuViewModel() { }

        public ExpanditMenuViewModel(MiniCartItem miniCart, IExpanditUserService userService, CartHeader header)
            : this(miniCart, userService, header, null) { }

        public ExpanditMenuViewModel(MiniCartItem miniCart, IExpanditUserService userService, CartHeader header, IEnumerable<MenuItem> menuItems)
        {
            int numberOfItems = 0;

            if (miniCart != null)
            {
                numberOfItems = (int)miniCart.NumberOfItems;
                var totalAmount = (decimal)miniCart.Total;
                var totalAmountInclTax = (decimal)miniCart.TotalInclTax;

                string taxDisplayConfiguration = ExpanditLib2.CStrEx(System.Configuration.ConfigurationManager.AppSettings["SHOW_TAX_TYPE"]);

                switch (taxDisplayConfiguration.ToUpper())
                {
                    case "CUST":
                        TotalAmountText = header.PricesIncludingVat
                        ? CurrencyFormatter.FormatCurrency(totalAmountInclTax, userService.CurrencyGuid)
                        : CurrencyFormatter.FormatCurrency(totalAmount, userService.CurrencyGuid);
                        break;
                    case "INCL":
                        TotalAmountText = CurrencyFormatter.FormatCurrency(
                            totalAmountInclTax, userService.CurrencyGuid);
                        break;
                    default:
                        TotalAmountText =
                        CurrencyFormatter.FormatCurrency(
                            totalAmount, userService.CurrencyGuid);
                        break;
                }

                switch (numberOfItems)
                {
                    case 0:
                        NumberOfItemsText = Language.LABEL_ORDER_PAD_IS_EMPTY;
                        TotalAmountText = null;
                        break;
                    case 1:
                        NumberOfItemsText = Language.LABEL_ONE_ITEM_IN_CART.Replace("%1", "1");
                        break;
                    default:
                        NumberOfItemsText = Language.LABEL_SEVERAL_ITEMS_IN_CART.Replace("%1", numberOfItems.ToString(CultureInfo.InvariantCulture));
                        break;
                }
            }
            else
            {
                NumberOfItemsText = Language.LABEL_ORDER_PAD_IS_EMPTY;
            }

            MenuItems = menuItems;

            TotalAmount = numberOfItems;

        }

        public class MenuItem
        {
            public string Url { get; set; }
            public string Text { get; set; }
            public bool Selected { get; set; }
            public IEnumerable<MenuItem> Children { get; set; }
        }

    }
}