﻿using System.ComponentModel.DataAnnotations;
using System.Web.UI.WebControls;
using EISCS.Shop.Attributes;
using Resources;

namespace ViewModels
{
    /// <summary>
    /// Summary description for LoginModel
    /// </summary>
    public class LoginModel
    {
        [Required(ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "ERROR_VALIDATION_LOGIN")]
        [Display(ResourceType = typeof(Language), Name = "LABEL_USERDATA_LOGIN")]
        public string UserName { get; set; }

        [Required(ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "ERROR_VALIDATION_PASSWORD")]
        [DataType(DataType.Password)]
        [Display(ResourceType = typeof(Language), Name = "LABEL_USERDATA_PASSWORD")]
        [PasswordStringLength(40, ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "MESSAGE_THE_VALUE_IN_FIELD_IS_NOT_CORRECT_1")]
        public string Password { get; set; }

        public bool UseTwoFactorLogin { get; set; }
        public string Pin { get; set; }

        [Display(ResourceType = typeof(Language), Name = "LABEL_REMEMBER_ME")]
        public bool RememberMe { get; set; }
        
        public string ReturnUrl { get; set; }
    }
}