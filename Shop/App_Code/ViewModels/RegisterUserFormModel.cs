﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using EISCS.Shop.Attributes;
using Resources;

namespace ViewModels
{
    public class RegisterUserFormModel
    {
		public string UserGuid { get; set; }

        [StringLength(50, ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "MESSAGE_THE_VALUE_IN_FIELD_IS_TOO_LONG")]
		[Required(ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "ERROR_VALIDATION_CONTACTPERSON")]
        [Display(ResourceType = typeof(Language), Name = "LABEL_NAME")]
        public string ContactName { get; set; }

        [StringLength(80, ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "MESSAGE_THE_VALUE_IN_FIELD_IS_TOO_LONG")]
		[Required(ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "ERROR_VALIDATION_EMAIL")]
        [RegularExpression(@"^[-!#$%&'*+/0-9=?A-Z^_a-z{|}~](\.?[-!#$%&'*+/0-9=?A-Z^_a-z{|}~])*@[a-zA-Z](-?[a-zA-Z0-9])*(\.[a-zA-Z](-?[a-zA-Z0-9])*)+$", ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "MESSAGE_EMAIL_ADDRESS_NOT_VALID")]
        [Display(ResourceType = typeof(Language), Name = "LABEL_EMAIL")]
        public string EmailAddress { get; set; }
        
		public string CustomerGuid { get; set; }

        [StringLength(50, ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "MESSAGE_THE_VALUE_IN_FIELD_IS_TOO_LONG")]
        [Display(ResourceType = typeof(Language), Name = "LABEL_USERDATA_COMPANY")]
        public string CompanyName { get; set; }
	
		[Required(ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "ERROR_VALIDATION_COUNTRY")]
		public string CountryGuid { get; set; }
	    
		public string CurrencyGuid { get; set; }
		
		public string SecondaryCurrencyGuid { get; set; }

        [StringLength(50, ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "MESSAGE_THE_VALUE_IN_FIELD_IS_TOO_LONG")]
        [Required(ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "ERROR_VALIDATION_ADDRESS")]
        [Display(ResourceType = typeof(Language), Name = "LABEL_USERDATA_ADDRESS_1")]
        public string Address1 { get; set; }

        [StringLength(50, ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "MESSAGE_THE_VALUE_IN_FIELD_IS_TOO_LONG")]
        [Display(ResourceType = typeof(Language), Name = "LABEL_USERDATA_ADDRESS_2")]
        public string Address2 { get; set; }

        [StringLength(50, ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "MESSAGE_THE_VALUE_IN_FIELD_IS_TOO_LONG")]
		[Required(ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "ERROR_VALIDATION_CITY")]
        [Display(ResourceType = typeof(Language), Name = "LABEL_USERDATA_CITY")]
        public string CityName { get; set; }

        [StringLength(30, ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "MESSAGE_THE_VALUE_IN_FIELD_IS_TOO_LONG")]
        [Display(ResourceType = typeof(Language), Name = "LABEL_USERDATA_PHONE")]
        public string PhoneNo { get; set; }

        [Required(ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "ERROR_VALIDATION_ZIP")]
        [Display(ResourceType = typeof(Language), Name = "LABEL_USERDATA_ZIPCODE")]
        public string ZipCode { get; set; }

        [StringLength(50, ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "MESSAGE_THE_VALUE_IN_FIELD_IS_TOO_LONG")]
        [Display(ResourceType = typeof(Language), Name = "LABEL_USERDATA_COUNTY")]
        public string CountyName { get; set; }

		public string LanguageGuid { get; set; }

        [StringLength(50, ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "MESSAGE_THE_VALUE_IN_FIELD_IS_TOO_LONG")]
		[Required(ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "ERROR_VALIDATION_LOGIN")]
        [Display(ResourceType = typeof(Language), Name = "LABEL_USERDATA_LOGIN")]
        public string UserName { get; set; }

        [PasswordStringLength(40, ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "MESSAGE_THE_VALUE_IN_FIELD_IS_NOT_CORRECT_1")]
        [DataType(DataType.Password)]
		[Required(ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "ERROR_VALIDATION_PASSWORD")]
        public string Password { get; set; }

        [PasswordStringLength(40, ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "MESSAGE_THE_VALUE_IN_FIELD_IS_NOT_CORRECT_1")]
        [Required(ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "ERROR_VALIDATION_CONFIRM_PASSWORD")]
        [System.Web.Mvc.Compare("Password", ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "MESSAGE_THE_PASSWORDS_MUST_MATCH")]
		[DataType(DataType.Password)]
		[Display(ResourceType = typeof(Language), Name = "LABEL_USERDATA_CONFIRM_PASSWORD")]
		public string ConfirmedPassword { get; set; }

		public Dictionary<string, string> CountryList { get; set; }
		public Dictionary<string, string> LanguageList { get; set; }
		public Dictionary<string, string> CurrencyList { get; set; }
    }
}