﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using EISCS.Shop.DO.Dto.Country;
using Resources;

namespace ViewModels
{
    /// <summary>
    /// Summary description for ContactUsViewModel
    /// </summary>
    public class ContactUsViewModel
    {

        [Display(ResourceType = typeof(Language), Name = "LABEL_USERDATA_COMPANY")]
        public string CompanyName { get; set; }

        [Display(ResourceType = typeof(Language), Name = "LABEL_USERDATA_CONTACT")]
        [Required(ErrorMessageResourceType =  typeof(Language), ErrorMessageResourceName = "MESSAGE_REQUIRED_FIELD")]
        public string ContactName { get; set; }

        [Display(ResourceType = typeof(Language), Name = "LABEL_USERDATA_ADDRESS_1")]
        public string Address1 { get; set; }

        [Display(ResourceType = typeof(Language), Name = "LABEL_USERDATA_ZIPCODE")]
        public string ZipCodeCity { get; set; }

        [Display(ResourceType = typeof(Language), Name = "LABEL_USERDATA_COUNTRY")]
        public IEnumerable<CountryItem> Countries { get; set; }
        [Display(ResourceType = typeof(Language), Name = "LABEL_USERDATA_COUNTRY")]
        public string SelectedCountry { get; set; }

        [Display(ResourceType = typeof(Language), Name = "LABEL_USERDATA_PHONE")]
        public string PhoneNo { get; set; }

        [Display(ResourceType = typeof(Language), Name = "LABEL_USERDATA_EMAIL")]
        [Required(ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "MESSAGE_REQUIRED_FIELD")]
        public string EmailAddress { get; set; }

        [Display(ResourceType = typeof(Language), Name = "LABEL_MAIL_CONTENT")]
        public string Content { get; set; }

        [Display(ResourceType = typeof(Language), Name = "LABEL_MAIL_SUBJ")]
        public string Subject { get; set; }

        [Display(ResourceType = typeof(Language), Name = "LABEL_CONTACT_SELECT_ADDRESS")]
        public List<SelectListItem> SendAddress { get; set; }
        [Display(ResourceType = typeof(Language), Name = "LABEL_CONTACT_SELECT_ADDRESS")]
        public string SelectedSendAddress { get; set; }

    }
}