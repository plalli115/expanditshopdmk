﻿using System.Collections.Generic;
using CmsPublic.ModelLayer.Models;
using EISCS.CMS.Services.PagingService;

namespace ViewModels
{
    public class CmsViewModel
    {
        /// <summary>
        /// </summary>
        /// <param name="compositeGroup"></param>
        /// <param name="group"></param>
        /// <param name="products"></param>
        /// <param name="selectedProducts"></param>
        /// <param name="prdPropValSet"></param>
        /// <param name="prod"></param>
        /// <param name="pager"></param>
        public CmsViewModel(Group compositeGroup, Group group, List<Product> products,
            List<GroupProduct> selectedProducts,
            PrdPropValSet prdPropValSet, Product prod, Pager pager)
        {
            CompositeGroup = compositeGroup;
            Group = group;
            Products = products;
            SelectedProducts = selectedProducts;
            PrdPropValSet = prdPropValSet;
            Prod = prod;
            Pager = pager;
        }

        public CmsViewModel()
        {
            CompositeGroup = null;
            Group = null;
            Products = null;
            SelectedProducts = null;
            PrdPropValSet = null;
            Prod = null;
            Pager = null;
        }

        /// <summary>
        /// </summary>
        public Group CompositeGroup { get; private set; }

        /// <summary>
        /// </summary>
        public Group Group { get; private set; }

        /// <summary>
        /// </summary>
        public List<Product> Products { get; private set; }

        /// <summary>
        /// </summary>
        public List<GroupProduct> SelectedProducts { get; private set; }

        /// <summary>
        /// </summary>
        public PrdPropValSet PrdPropValSet { get; private set; }

        /// <summary>
        /// </summary>
        public Product Prod { get; private set; }

        /// <summary>
        /// </summary>
        public Pager Pager { get; private set; }
    }
}