﻿using System.ComponentModel.DataAnnotations;
using Resources;

namespace ViewModels
{
    /// <summary>
    /// Summary description for RequestNewPasswordViewModel
    /// </summary>
    public class RequestNewPasswordViewModel
    {
        [StringLength(50)]
        [Required(ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "LABEL_STARMARKED_INPUT_IS_REQUIRED")]
        public string UserName { get; set; }

        [StringLength(80)]
        [Required(ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "LABEL_STARMARKED_INPUT_IS_REQUIRED")]
        public string EmailAddress { get; set; }

        public string Message { get; set; }
        public bool Success { get; set; }
    }
}