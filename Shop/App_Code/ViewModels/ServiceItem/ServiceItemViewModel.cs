﻿using System;

namespace ViewModels.ServiceItem
{
    public class ServiceItemViewModel
    {
        public string ServiceItemNo { get; set; }
        public string ItemNo { get; set; }
        public string SerialNo { get; set; }
        public string Description { get; set; }
        public int Priority { get; set; }
        public string CustomerGuid { get; set; }
        public string AddressGuid { get; set; }
        public string CustomerName { get; set; }
        public string CustomerAddress { get; set; }
    }
}