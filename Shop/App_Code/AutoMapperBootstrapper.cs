﻿using AutoMapper;
using CmsPublic.ModelLayer.Models;
using EISCS.Shop.DO.BAS.Dto;
using EISCS.Shop.DO.BAS.PostData;
using EISCS.Shop.DO.Dto;
using EISCS.Shop.DO.Dto.Cart;
using EISCS.Shop.DO.PostData;
using FormModels;
using FormModels.Cms2.FormModels;
using FormModels.ServiceOrder;
using FormModels.ServiceOrderAttachment;
using FormModels.ShippingAddress;
using ViewModels;
using ViewModels.AccountManagement;
using ViewModels.Cms;
using ViewModels.Customer;
using ViewModels.CustomerAddress;
using ViewModels.Order;
using ViewModels.PortalStatus;
using ViewModels.Project;
using ViewModels.ServiceAttachment;
using ViewModels.ServiceItem;
using ViewModels.ServiceManager;
using ViewModels.ServiceOrder;
using ViewModels.ServiceOrderCommentLine;
using ViewModels.ServiceOrderType;
using ViewModels.User;

public static class AutoMapperBootstrapper
{
    public static void RegisterTypes()
    {
        MapServiceOrderItems();
        MapServiceCommentLines();
        MapServiceOrderForms();
        MapUsers();
        MapServiceOrderAttachments();
        MapCustomers();
        MapServiceItem();
        MapShopSalesHeader();

        Mapper.CreateMap<Product, ProductsViewModel.ProductItem>();
        Mapper.CreateMap<ServiceOrderTypeItem, ServiceOrderTypeViewModel>();
        Mapper.CreateMap<PortalStatusItem, PortalStatusViewModel>();
        Mapper.CreateMap<ProjectItem, ProjectViewModel>()
              .ForMember(dest => dest.ProjectLabel, opt => opt.MapFrom(src => (src.ProjectGuid + " - " + src.ProjectName)));
        Mapper.CreateMap<ServiceManagerItem, ServiceManagerViewModel>()
              .ForMember(dest => dest.ServiceManagerLabel, opt => opt.MapFrom(src => (src.UserGuid + " - " + src.FullName)));
        Mapper.CreateMap<ServiceOrderPortalStatusItem, ServiceOrderPortalStatusViewModel>();
        Mapper.CreateMap<CartViewModel.ProductItem, CartUpdateInfo>();
        Mapper.CreateMap<CompanyInfoFormModel, Company>();
        Mapper.CreateMap<Company, CompanyInfoFormModel>();
        Mapper.CreateMap<KPI, KPIViewModel>();
        Mapper.CreateMap<MailMessageSettings, MailServerSettingsFormModel>();
        Mapper.CreateMap<MailMessageSettings, MailSettingsGeneralFormModel>();
        Mapper.CreateMap<MailMessageSettings, MailSettingsStaleCartFormModel>();
        Mapper.CreateMap<MailServerSettingsFormModel, MailServerSettings>();
        Mapper.CreateMap<MailSettingsGeneralFormModel, GeneralMailMessageSettings>();
        Mapper.CreateMap<MailSettingsStaleCartFormModel, StaleCartMailMessageSettings>();
        Mapper.CreateMap<MailMessageSettings, StaleCartMailMessageSettings>();
        Mapper.CreateMap<RoleFormModel, RoleTable>();
        Mapper.CreateMap<RoleTable, RoleFormModel>();
    }

    private static void MapServiceItem()
    {
        Mapper.CreateMap<ServiceItem, ServiceItemViewModel>();
        Mapper.CreateMap<ServiceItemViewModel, ServiceItem>();
    }

    private static void MapCustomers()
    {
        Mapper.CreateMap<CustomerItem, CustomerViewModel>()
              .ForMember(dest => dest.CustomerLabel, opt => opt.MapFrom(src => (src.CustomerGuid + " - " + src.CompanyName)))
              .ForMember(dest => dest.Address1, opt => opt.MapFrom(src => src.Address));
        Mapper.CreateMap<CustomerItem, EditCustomerGroupViewModel.Customer>()
              .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.CustomerGuid));
        Mapper.CreateMap<CustomerAddressItem, CustomerAddressViewModel>();
        Mapper.CreateMap<CustomerItem, ShippingAddress>();

        Mapper.CreateMap<CustomerTableBase, CustomerViewModel>()
              .ForMember(dest => dest.CustomerLabel, opt => opt.MapFrom(src => (src.CustomerGuid + " - " + src.CompanyName)));
    }

    private static void MapServiceOrderAttachments()
    {
        Mapper.CreateMap<ServiceAttachmentItem, ServiceAttachmentListViewModel>();
        Mapper.CreateMap<ServiceAttachmentFormModel, ServiceAttachmentItem>()
              .ForMember(dest => dest.Description, opt => opt.MapFrom(src => src.AttachmentComment));
    }

    private static void MapServiceOrderForms()
    {
        Mapper.CreateMap<ServiceOrderItemFormModel, ServiceOrderPostData>()
              .ForMember(dest => dest.ShortDescription, opt => opt.MapFrom(src => src.JobDescription))
              .ForMember(dest => dest.JobType, opt => opt.MapFrom(src => src.SelectedTask))
              .ForMember(dest => dest.Status, opt => opt.MapFrom(src => src.SelectedStatus))
              .ForMember(dest => dest.ProjectGuid, opt => opt.MapFrom(src => src.SelectedProject))
              .ForMember(dest => dest.CustomerGuid, opt => opt.MapFrom(src => src.SelectedCustomerId))
              .ForMember(dest => dest.ServiceItemDescription, opt => opt.MapFrom(src => src.Description));

        Mapper.CreateMap<ServiceOrderItemFormModel, ServiceOrderAdditionalPostData>()
              .ForMember(dest => dest.ResponsibleUserGuid, opt => opt.MapFrom(src => src.SelectedServiceManager))
              .ForMember(dest => dest.DepartmentGuid, opt => opt.MapFrom(src => src.SelectedDepartment));

        Mapper.CreateMap<ServiceOrderItemFormModel, ServiceOrderEditData>()
              .ForMember(dest => dest.Type, opt => opt.MapFrom(src => src.SelectedTask))
              .ForMember(dest => dest.ShopDescription, opt => opt.MapFrom(src => src.JobDescription))
              .ForMember(dest => dest.ShipToContactPerson, opt => opt.MapFrom(src => src.ShiptoContactName))
              .ForMember(dest => dest.ProjectGuid, opt => opt.MapFrom(src => src.SelectedProject))
              .ForMember(dest => dest.Status, opt => opt.MapFrom(src => src.SelectedStatus));

        Mapper.CreateMap<ServiceOrderEditData, ServiceOrder>()
              .ForMember(dest => dest.JobDescription, opt => opt.MapFrom(src => src.ShopDescription))
              .ForMember(dest => dest.BilltoContactName, opt => opt.MapFrom(src => src.ContactPerson))
              .ForMember(dest => dest.ShiptoContactName, opt => opt.MapFrom(src => src.ShipToContactPerson))
              .ForMember(dest => dest.ResponseDate, opt => opt.MapFrom(src => src.SelectedStartDate))
              .ForMember(dest => dest.ResponseTime, opt => opt.MapFrom(src => src.SelectedStartDate))
              .ForMember(dest => dest.RepairStatusCode, opt => opt.MapFrom(src => src.Status))
              .ForMember(dest => dest.StartingDate, opt => opt.MapFrom(src => src.SelectedStartDate))
              .ForMember(dest => dest.FinishingDate, opt => opt.MapFrom(src => src.SelectedFinishDate))
              .ForMember(dest => dest.Description, opt => opt.MapFrom(src => src.CreateServiceItemDescription));

        Mapper.CreateMap<ServiceOrder, ServiceOrderEditData>()
              .ForMember(dest => dest.ServiceItemDescription, opt => opt.MapFrom(src => src.Description));
        Mapper.CreateMap<ServiceOrderItemFormModel, ServiceOrderFormPostModel>();
        Mapper.CreateMap<ServiceOrderFormPostModel, ServiceOrderItemFormModel>()
              .ForMember(dest => dest.Description, opt => opt.MapFrom(src => src.CreateServiceItemDescription));
        Mapper.CreateMap<ServiceOrderFormPostModel, ServiceOrderAdditionalPostData>();
        Mapper.CreateMap<ServiceOrderFormPostModel, ServiceOrderPostData>()
              .ForMember(dest => dest.CustomerGuid, opt => opt.MapFrom(x => x.SelectedCustomerId))
              .ForMember(dest => dest.Status, opt => opt.MapFrom(src => src.SelectedStatus))
              .ForMember(dest => dest.ProjectGuid, opt => opt.MapFrom(src => src.SelectedProject))
              .ForMember(dest => dest.ShortDescription, opt => opt.MapFrom(src => src.JobDescription))
              .ForMember(dest => dest.ServiceItemDescription, opt => opt.MapFrom(src => src.Description));
              
        Mapper.CreateMap<ServiceOrderFormPostModel, ServiceOrderEditData>()
              .ForMember(dest => dest.Type, opt => opt.MapFrom(src => src.SelectedTask))
              .ForMember(dest => dest.ShopDescription, opt => opt.MapFrom(src => src.JobDescription))
              .ForMember(dest => dest.ShipToContactPerson, opt => opt.MapFrom(src => src.ShiptoContactName))
              .ForMember(dest => dest.Status, opt => opt.MapFrom(src => src.SelectedStatus))
              .ForMember(dest => dest.ProjectGuid, opt => opt.MapFrom(src => src.SelectedProject))
              .ForMember(dest => dest.ServiceOrderType, opt => opt.MapFrom(src => src.SelectedTask))
              .ForMember(dest => dest.DepartmentGuid, opt => opt.MapFrom(src => src.SelectedDepartment))
              .ForMember(dest => dest.ShipToPhoneNo, opt => opt.MapFrom(src => src.ShipToPhoneNo))
              .ForMember(dest => dest.ShipToPhoneNo2, opt => opt.MapFrom(src => src.ShipToPhoneNo2))
              .ForAllMembers(p => p.Condition(c => !c.IsSourceValueNull));

        Mapper.CreateMap<ServiceOrderEditData, ServiceItemPostData>()
            .ForMember(dest => dest.Description, opt => opt.MapFrom(src => src.CreateServiceItemDescription))
        ;
    }

    private static void MapServiceCommentLines()
    {
        Mapper.CreateMap<ServiceOrderItemFormModel, ServiceOrderCommentLineItem>()
              .ForMember(dest => dest.CommentText, opt => opt.MapFrom(src => src.LongDescription));
        Mapper.CreateMap<ServiceOrderPostData, ServiceOrderCommentLineItem>()
              .ForMember(dest => dest.CommentText, opt => opt.MapFrom(src => src.LongDescription));
        Mapper.CreateMap<ServiceOrderCommentLineItem, ServiceOrderCommentLineViewModel>()
              .ForMember(dest => dest.PortalUserName, opt => opt.MapFrom(src => src.PortalContactName));
    }

    private static void MapServiceOrderItems()
    {
        Mapper.CreateMap<ServiceOrder, ServiceOrderItemFormModel>()
              .ForMember(dest => dest.SelectedTask, opt => opt.MapFrom(src => src.ServiceOrderType))
              .ForMember(dest => dest.SelectedStatus, opt => opt.MapFrom(src => src.RepairStatusCode))
              .ForMember(dest => dest.SelectedStartDate, opt => opt.MapFrom(src => src.ResponseDate))
              .ForMember(dest => dest.SelectedProject, opt => opt.MapFrom(src => src.ProjectGuid))
              .ForMember(dest => dest.SelectedCustomerId, opt => opt.MapFrom(src => src.CustomerGuid))
              .ForMember(dest => dest.SelectedTask, opt => opt.MapFrom(src => src.ServiceOrderType))
              .ForMember(dest => dest.SelectedDepartment, opt => opt.MapFrom(src => src.DepartmentGuid))
              .ForMember(dest => dest.SelectedFinishDate, opt => opt.MapFrom(src => src.FinishingDate))
              .ForMember(dest => dest.ShipToPhoneNo, opt => opt.MapFrom(src => src.ShiptoPhoneNo));

        Mapper.CreateMap<ServiceOrderItemFormModel, ServiceOrder>()
              .ForMember(dest => dest.ServiceOrderType, opt => opt.MapFrom(src => src.SelectedTask))
              .ForMember(dest => dest.RepairStatusCode, opt => opt.MapFrom(src => src.SelectedStatus))
              .ForMember(dest => dest.ResponseDate, opt => opt.MapFrom(src => src.SelectedStartDate))
              .ForMember(dest => dest.ProjectGuid, opt => opt.MapFrom(src => src.SelectedProject));

        Mapper.CreateMap<ServiceOrderPostData, ServiceOrder>()
              .ForMember(dest => dest.ServiceOrderType, opt => opt.MapFrom(src => src.JobType))
              .ForMember(dest => dest.RepairStatusCode, opt => opt.MapFrom(src => src.Status))
              .ForMember(dest => dest.ResponseDate, opt => opt.MapFrom(src => src.SelectedStartDate))
              .ForMember(dest => dest.JobDescription, opt => opt.MapFrom(src => src.ShortDescription))
              .ForMember(dest => dest.BilltoContactName, opt => opt.MapFrom(src => src.BillToContactName));

        Mapper.CreateMap<ServiceOrder, ServiceOrderListViewModel>();

        Mapper.CreateMap<ServiceOrder, ServiceOrderDetailViewModel>()
              .ForMember(dest => dest.ShowAddress2, opt => opt.MapFrom(src => (!string.IsNullOrEmpty(src.Address2))))
              .ForMember(dest => dest.ShowServiceOrderStatus, opt => opt.MapFrom(src => (!string.IsNullOrEmpty(src.RepairStatusCode))))
              .ForMember(dest => dest.ShowServiceOrderType, opt => opt.MapFrom(src => (!string.IsNullOrEmpty(src.ServiceOrderType))))
              .ForMember(dest => dest.SelectedStartDate, opt => opt.MapFrom(src => src.ResponseDate))
              .ForMember(dest => dest.SelectedFinishDate, opt => opt.MapFrom(src => src.FinishingDate));

        Mapper.CreateMap<ReportHeader, ServiceOrderReport>()
            .ForMember(x => x.PdfFileData, opt => opt.Ignore());
    }

    private static void MapShopSalesHeader()
    {
        Mapper.CreateMap<ShopSalesHeader, OrderViewModel>()
              .ForMember(dest => dest.Lines, o => o.MapFrom(src => src.Lines))
              .AfterMap((shopSalesHeader, orderViewModel) =>
                  {
                      orderViewModel.ShippingAddress = new CustomerAddressViewModel
                          {
                              Address1 = shopSalesHeader.ShipToAddress1,
                              Address2 = shopSalesHeader.ShipToAddress2,
                              AddressGuid = shopSalesHeader.ShipToShippingAddressGuid,
                              CityName = shopSalesHeader.ShipToCityName,
                              CompanyName = shopSalesHeader.ShipToCompanyName,
                              ContactPerson = shopSalesHeader.ShipToContactName,
                              CountryGuid = shopSalesHeader.ShipToCountryGuid,
                              EMailAddress = shopSalesHeader.ShipToEmailAddress,
                              ZipCode = shopSalesHeader.ShipToZipCode,
                              CountryName = shopSalesHeader.ShipToCountryName
                          };
                      orderViewModel.BillingAddress = new CustomerAddressViewModel
                          {
                              Address1 = shopSalesHeader.Address1,
                              Address2 = shopSalesHeader.Address2,
                              CityName = shopSalesHeader.CityName,
                              CompanyName = shopSalesHeader.CompanyName,
                              ContactPerson = shopSalesHeader.ContactName,
                              CountryGuid = shopSalesHeader.CountryGuid,
                              EMailAddress = shopSalesHeader.EmailAddress,
                              ZipCode = shopSalesHeader.ZipCode,
                              CountryName = shopSalesHeader.CountryName
                          };
                  }
            )
            ;
        Mapper.CreateMap<ShopSalesLine, OrderLineViewModel>();
        Mapper.CreateMap<CartHeader, ShopSalesHeader>();
        Mapper.CreateMap<CartLine, ShopSalesLine>();
        Mapper.CreateMap<CartHeader, AnonymousBillingAndShippingAddressForm>();
    }

    private static void MapUsers()
    {
        Mapper.CreateMap<UserTable, UserViewModel>();
        Mapper.CreateMap<UserTable, UserItemFormModel>();
        Mapper.CreateMap<UserTable, ContactUsViewModel>();
        Mapper.CreateMap<UserItemFormModel, UserItemPostData>();
        Mapper.CreateMap<UserItemPostData, UserTable>();
        Mapper.CreateMap<UserPasswordFormModel, UserPasswordPostData>();

        Mapper.CreateMap<BusinessCenterRegisterUserFormModel, UserTable>()
            .ForMember(dest => dest.UserLogin, opt => opt.MapFrom(src => src.UserName))
            .ForMember(dest => dest.UserPassword, opt => opt.MapFrom(src => src.Password));
        Mapper.CreateMap<UserTable, BusinessCenterRegisterUserFormModel>()
            .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.UserLogin))
            .ForMember(dest => dest.Password, opt => opt.MapFrom(src => src.UserPassword))
            .ForMember(dest => dest.RoleId, opt => opt.MapFrom(src => src.RoleId))
            .ForMember(dest => dest.UserCreated, opt => opt.MapFrom(src => src.UserCreated));

        Mapper.CreateMap<BusinessCenterEditUserFormModel, UserTable>()
            .ForMember(dest => dest.UserLogin, opt => opt.MapFrom(src => src.UserName));
        Mapper.CreateMap<UserTable, BusinessCenterEditUserFormModel>()
            .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.UserLogin))
            .ForMember(dest => dest.RoleId, opt => opt.MapFrom(src => src.RoleId))
            .ForMember(dest => dest.UserCreated, opt => opt.MapFrom(src => src.UserCreated));

        Mapper.CreateMap<UserTable, UserViewModel>()
              .ForMember(dest => dest.UserGuid, opt => opt.MapFrom(src => src.UserGuid))
              .ForMember(dest => dest.ContactName, opt => opt.MapFrom(src => src.UserLogin));

        Mapper.CreateMap<UserTable, UserItemFormModel>()
            .ForMember(dest => dest.UserCreated, opt => opt.MapFrom(src => src.UserCreated));

        Mapper.CreateMap<EditUserViewModel, B2BUserData>();

        Mapper.CreateMap<ShippingAddress, ShippingAddressItemFormModel>();
        Mapper.CreateMap<ShippingAddressItemFormModel, ShippingAddress>();
        Mapper.CreateMap<UserTable, ShippingAddress>();

        //Mapper.CreateMap<MembershipUser, UserTable>()
        //    .ForMember(dest => dest.UserLogin, opt => opt.MapFrom(src => src.UserName))
        //    .ForMember(dest => dest.EmailAddress, opt => opt.MapFrom(src => src.Email));
        //Mapper.CreateMap<UserTable, MembershipUser>()
        //    .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.UserLogin))
        //    .ForMember(dest => dest.Email, opt => opt.MapFrom(src => src.EmailAddress))
        //    .ForMember(dest => dest.ProviderName, opt => opt.MapFrom(src => Membership.Provider.Name))
        //    .ForMember(dest => dest.ProviderUserKey, opt => opt.MapFrom(src => src.UserGuid));
    }
}
