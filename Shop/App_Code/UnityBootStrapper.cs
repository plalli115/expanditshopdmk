using System.Configuration;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using CmsPublic.DataRepository;
using EISCS.Catalog.Logic;
using EISCS.CMS.Account;
using EISCS.CMS.Services.Synchronization;
using EISCS.ExpandITFramework.Util;
using EISCS.Shop;
using EISCS.Shop.BO.BusinessLogic;
using EISCS.Shop.BO.BusinessLogic.Axapta;
using EISCS.Shop.BO.BusinessLogic.C5;
using EISCS.Shop.BO.BusinessLogic.Navision.Nav;
using EISCS.Shop.BO.BusinessLogic.Navision.Nf;
using EISCS.Shop.BO.BusinessLogic.None;
using EISCS.Shop.BO.BusinessLogic.Xal;
using EISCS.Shop.DO;
using EISCS.Shop.DO.BAS.Interface;
using EISCS.Shop.DO.BAS.Repository;
using EISCS.Shop.DO.BAS.Service;
using EISCS.Shop.DO.Interface;
using EISCS.Shop.DO.Repository;
using EISCS.Shop.DO.Service;
using EISCS.Shop.Users;
using EISCS.WebUserControlLogic.Country;
using EISCS.WebUserControlLogic.Language;
using EISCS.Wrappers.Configuration;
using Mailers;
using Microsoft.Practices.Unity;
using Unity.Mvc4;
using UnitySupport;
using ViewServiceInterfaces;
using ViewServices;
using WebServices;
using HttpContextWrapper = System.Web.HttpContextWrapper;

public static class UnityBootstrapper
{
    public static ConnectionStringSettings ExpanditBasConnection = ConfigurationManager.ConnectionStrings["ExpandITBASConnectionString"];
    public static ConnectionStringSettings ExpanditShopConnection = ConfigurationManager.ConnectionStrings["ExpandITConnectionString"];
    public static string BackendType = ConfigurationManager.AppSettings["BackendType"];

    public static IUnityContainer Initialise()
    {
        IUnityContainer container = BuildUnityContainer();

        var resolver = new UnityDependencyResolver(container);
        DependencyResolver.SetResolver(resolver);
        GlobalConfiguration.Configuration.DependencyResolver = new Unity.WebApi.UnityDependencyResolver(container);

        return container;
    }

    private static IUnityContainer BuildUnityContainer()
    {
        var container = new UnityContainer();

        RegisterTypes(container);

        return container;
    }

    public static void RegisterTypes(IUnityContainer container)
    {
        container.RegisterType<HttpContextBase>(new InjectionFactory(c => new HttpContextWrapper(HttpContext.Current)));

        container.RegisterType<IConfigurationObject, ConfigurationObject>();
        container.RegisterType<ICurrencyConverter, CurrencyConverter>();
        container.RegisterType<ISessionManager, HttpSessionManager>();
        container.RegisterType<IConfigurationObject, ConfigurationObject>();
        container.RegisterType<IMailSendClient, MailSendHttpClient>();
        container.RegisterType<IMailPromotionModule, MailPromotionModule>();

        RegisterFactories(container);
        RegisterLogics(container);
        RegisterRepositories(container);
        RegisterServices(container);

        RegisterBackend(container);
    }

    private static void RegisterFactories(IUnityContainer container)
    {
        container.RegisterType<IExpanditDbFactory, ExpanditDbFactory>(new InjectionConstructor(ExpanditShopConnection.ConnectionString, ExpanditShopConnection.ProviderName));
        container.RegisterType<IPortalDatabaseConnectionFactory, PortalDatabaseConnectionFactory>(new InjectionConstructor(ExpanditBasConnection));
        container.RegisterType<IShopDatabaseConnectionFactory, ShopDatabaseConnectionFactory>(new InjectionConstructor(ExpanditShopConnection));
    }

    private static void RegisterLogics(IUnityContainer container)
    {
        container.RegisterType<ICatalogLogic, CatalogLogic>(new InjectionConstructor());
        container.RegisterType<ICountryControlLogic, CountryControlLogic>();
        container.RegisterType<ILanguageControlLogic, LanguageControlLogic>();
    }

    private static void RegisterRepositories(IUnityContainer container)
    {
        container.RegisterType<IPortalStatusRespository, PortalStatusRepository>();
        container.RegisterType<IServiceOrderRepository, ServiceOrderRepository>();
        container.RegisterType<IServiceOrderCommentLineRepository, ServiceOrderCommentLineRepository>();
        container.RegisterType<IServiceOrderPortalStatusRepository, ServiceOrderPortalStatusRepository>();
        container.RegisterType<ICustomerPropertiesRepository, CustomerPropertiesRepository>();
        container.RegisterType<IServiceAttachmentRepository, ServiceAttachmentRepository>();
        container.RegisterType<ICustomerRepository, CustomerRepository>();
        container.RegisterType<IDepartmentRepository, DepartmentRepository>();
        container.RegisterType<IProjectRepository, ProjectRepository>();
        container.RegisterType<IUserItemRepository, UserItemRepository>();
        container.RegisterType<IProductRepository, ProductRepository>();
        container.RegisterType<ICountryRepository, CountryRepository>();
        container.RegisterType<ICurrencyRepository, CurrencyRepository>();
        container.RegisterType<IProductVariantService, ProductVariantService>();
        container.RegisterType<ICartHeaderRepository, CartHeaderRepository>();
        container.RegisterType<ICartLineRepository, CartLineRepository>();
        container.RegisterType<IShippingAddressRepository, ShippingAddressRepository>();
        container.RegisterType<IServiceItemRepository, ServiceItemRepository>();
        container.RegisterType<IShippingHandlingProviderRepository, ShippingHandlingProviderRepository>();
        container.RegisterType<IShippingHandlingPriceRepository, ShippingHandlingPriceRepository>();
        container.RegisterType<IShopSalesHeaderRepository, ShopSalesHeaderRepository>();
        container.RegisterType<IShopSalesLineRepository, ShopSalesLineRepository>();
        container.RegisterType<IPaymentTypeRepository, PaymentTypeRepository>();
        container.RegisterType<IFavoritesRepository, FavoritesRepository>();
        container.RegisterType<IConfigurationRepository, ConfigurationRepository>();
        container.RegisterType<ICompanyRepository, CompanyRepository>();
        container.RegisterType<IKeyPerformanceIndicatorsRepository, KeyPerformanceIndicatorsRepository>();
        container.RegisterType<IMailMessageSettingRepository, MailMessageSettingRepository>();
        container.RegisterType<ILanguageRepository, LanguageRepository>();
        container.RegisterType<IGroupRepository, GroupRepository>();
        container.RegisterType<IGroupProductRepository, GroupProductRepository>();
        container.RegisterType<IMailMessageRepository, MailMessageRepository>();
    }

    private static void RegisterServices(IUnityContainer container)
    {
        container.RegisterType<IUniqueGuidService, UniqueGuidService>();
        container.RegisterType<IUserStorageService, UserStorageService>(
            new InjectionConstructor(typeof(IExpanditDbFactory), typeof(IConfigurationObject)));
        container.RegisterType<IExpanditUserService, ExpanditUserService>(
            new UnityPerRequestLifetimeManager(),
            new InjectionConstructor(
                typeof(IUserStorageService),
                typeof(HttpContextBase),
                typeof(IConfigurationObject),
                typeof(ICurrencyRepository)));
        container.RegisterType<IUserManagerStorageService, UserManagerStorageService>(
            new InjectionConstructor(typeof(IExpanditDbFactory)));
        container.RegisterType<IUserManager, UserManager>();
        container.RegisterType<IServiceOrderService, ServiceOrderService>();
        container.RegisterType<IServiceAttachmentService, ServiceAttachmentService>();
        container.RegisterType<IServiceOrderReportService, ServiceOrderReportService>();

        container.RegisterType<IBusinessCenterAccountService, BusinessCenterAccountService>(
            new InjectionConstructor(typeof(IExpanditDbFactory), typeof(ILoginService)));
        container.RegisterType<IBusinessCenterAccountManager, AccountManager>(
            new UnityPerRequestLifetimeManager(),
            new InjectionConstructor(
                typeof(IBusinessCenterAccountService),
                typeof(HttpContextBase)
                ));

        container.RegisterType<IProductService, ProductService>();
        container.RegisterType<ICartDataService, CartDataService>();
        container.RegisterType<ICartService, CartService>();
        container.RegisterType<IOrderService, OrderService>();
        container.RegisterType<ILedgerService, LedgerService>();
        container.RegisterType<ICustomerService, CustomerService>();
        container.RegisterType<IServiceOrderViewService, ServiceOrderViewService>();
        container.RegisterType<ICustomerDataService, CustomerDataService>();
        container.RegisterType<ICustomerAddressDataService, CustomerAddressDataService>();
        container.RegisterType<IShippingHandlingService, ShippingHandlingService>();
        container.RegisterType<IHttpRuntimeWrapper, HttpRuntimeWrapper>();
        container.RegisterType<ILoginService, LoginService>();
        container.RegisterType<IPaymentService, PaymentService>();
        container.RegisterType<IProductService, ProductService>();
        container.RegisterType<IProductSearchService, ProductSearchService>();
        container.RegisterType<IShopSalesService, ShopSalesService>();
        container.RegisterType<IKeyPerformanceIndicatorsService, KeyPerformanceIndicatorsService>();
        container.RegisterType<IUserMailer, UserMailer>();
        container.RegisterType<IServiceItemService, ServiceItemService>();
        container.RegisterType<IPrincipalService, ExpanditPrincipalService>();
        container.RegisterType<IFormsAuthenticationService, FormsAuthenticationService>();
        container.RegisterType<IPropertiesDataService, PropertiesDataService>();
        container.RegisterType<ISynchronizationService, SynchronizationService>(new InjectionConstructor(
                typeof(HttpContextBase)
                ));
        container.RegisterType<ILegacyDataService, LegacyDataService>();
    }

    private static void RegisterBackend(IUnityContainer container)
    {
        switch (BackendType)
        {
            case "NAV":
                container.RegisterType<IBuslogic, BuslogicNav>();
                container.RegisterType<INavDataService, NavDataService>();
                break;
            case "AX":
            case "AX2012":
                container.RegisterType<IBuslogic, BuslogicAx>();
                container.RegisterType<IAxDataService, AxDataService>();
                break;
            case "XAL":
                container.RegisterType<IBuslogic, BuslogicXal>();
                container.RegisterType<IXalDataService, XalDataService>();
                break;
            case "C5":
                container.RegisterType<IBuslogic, BuslogicC5>();
                container.RegisterType<IC5DataService, C5DataService>();
                break;
            case "NF":
                container.RegisterType<IBuslogic, BuslogicNf>();
                container.RegisterType<INfDataService, NfDataService>();
                break;
            default:
                container.RegisterType<IBuslogic, BuslogicNone>();
                container.RegisterType<INoneDataService, NoneDataService>();
                break;
        }
    }
}
