﻿using FormModels.ServiceOrder;
using ViewModels.ServiceOrder;

namespace ViewServiceInterfaces
{
    public interface IServiceOrderViewService
    {
        ServiceOrderItemFormModel PrepareCreateFormModel();
        ServiceOrderItemFormModel PrepareEditFormModel(ServiceOrderItemFormModel formModel, string serviceOrderId, string serviceOrderStatusId = null);
        ServiceOrderItemFormModel PreparePostedFormModel(ServiceOrderFormPostModel formPostModel);
        ServiceOrderDetailViewModel PopulateDetails(string id);
    }
}