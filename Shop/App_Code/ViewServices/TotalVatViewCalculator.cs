﻿using System;
using System.Linq;
using CmsPublic.DataProviders.Logging;
using EISCS.ExpandIT.Serialization;
using EISCS.Shop.DO.Dto.Cart;
using ExpandIT;

namespace ViewServices
{
    /// <summary>
    /// Summary description for TotalVatViewCalculator
    /// </summary>
    public class TotalVatViewCalculator
    {
        // FROM MVP
        public double CalculateTotal<T>(BaseOrderHeader<T> cartHeader, double shippingTaxPct, out ExpDictionary taxAmounts) where T: BaseOrderLine
        {
            taxAmounts = new ExpDictionary();
            double invoiceDiscount2 = Convert.ToDouble(cartHeader.InvoiceDiscountPct);

            if (!string.IsNullOrEmpty(cartHeader.VatTypes))
            {
                try
                {
                    var x = Marshall.UnmarshallDictionary(cartHeader.VatTypes) as ExpDictionary;
                    if (x != null)
                    {
                        foreach (double vatType in x.Keys)
                        {
                            taxAmounts[vatType] = x[vatType];
                        }
                    }
                }
                catch (Exception ex)
                {
                    FileLogger.Log(ex.Message);
                }
            }
            else
            {
                foreach (var orderLine in cartHeader.Lines)
                {
                    double orderLineTaxPct = Convert.ToDouble(orderLine.TaxPct);
                    if (!taxAmounts.Exists(orderLineTaxPct))
                    {
                        taxAmounts.Add(orderLineTaxPct, orderLine.TaxAmount * (100 - invoiceDiscount2) / 100);
                    }
                    else
                    {
                        double calcLine = (orderLine.TaxAmount * (100 - invoiceDiscount2) / 100);
                        taxAmounts[orderLineTaxPct] = ExpanditLib2.ConvertToDbl(taxAmounts[orderLineTaxPct]) + calcLine;
                    }
                }
            }

            // Check also the Header for
            // 1. Handling tax amount
            // 2. Shipping tax amount
            // 3. Service charge tax amount

            double shippingTaxAmount = cartHeader.ShippingAmountInclTax - cartHeader.ShippingAmount;

            double handlingTaxAmount = cartHeader.HandlingAmountInclTax - cartHeader.HandlingAmount;
            double serviceChargeAmount = cartHeader.ServiceChargeInclTax - cartHeader.ServiceCharge;
            
            double handlingTaxPct = ExpanditLib2.ConvertToDbl(cartHeader.TaxPct);
            // This might be a different value - must be investigated
            double serviceChargeTacPct = ExpanditLib2.ConvertToDbl(cartHeader.TaxPct);

            // Add Shipping Tax amounts
            if (!taxAmounts.Exists(shippingTaxPct))
            {
                taxAmounts.Add(shippingTaxPct, shippingTaxAmount);
            }
            else
            {
                taxAmounts[shippingTaxPct] = ExpanditLib2.ConvertToDbl(taxAmounts[shippingTaxPct]) + shippingTaxAmount;
            }

            // Add Handling Tax Amounts
            if (!taxAmounts.Exists(handlingTaxPct))
            {
                taxAmounts.Add(handlingTaxPct, handlingTaxAmount);
            }
            else
            {
                taxAmounts[handlingTaxPct] = ExpanditLib2.ConvertToDbl(taxAmounts[handlingTaxPct]) + handlingTaxAmount;
            }

            // Add ServiceCharge Tax Amounts
            if (!taxAmounts.Exists(serviceChargeTacPct))
            {
                taxAmounts.Add(serviceChargeTacPct, serviceChargeAmount);
            }
            else
            {
                taxAmounts[serviceChargeTacPct] = ExpanditLib2.ConvertToDbl(taxAmounts[serviceChargeTacPct]) + serviceChargeAmount;
            }

            // Total

            decimal totalTaxAmount = (decimal) taxAmounts.Sum(kvPair => ExpanditLib2.ConvertToDbl(kvPair.Value));
            decimal totalamount = (decimal)(Math.Round(cartHeader.TotalInclTax - (double)totalTaxAmount, 2, MidpointRounding.AwayFromZero));

            if (taxAmounts.Exists(default(decimal)))
            {
                taxAmounts.Remove(default(decimal));
            }

            return (double) totalamount;
        }
    }
}