﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using AutoMapper;
using EISCS.ExpandITFramework.Util;
using EISCS.Shop.DO.BAS.Dto;
using EISCS.Shop.DO.BAS.Interface;
using EISCS.Shop.DO.BAS.Repository;
using EISCS.Shop.DO.Dto;
using EISCS.Shop.DO.Interface;
using FormModels.ServiceOrder;
using FormModels.ServiceOrderComment;
using ViewModels.PortalStatus;
using ViewModels.ServiceAttachment;
using ViewModels.ServiceItem;
using ViewModels.ServiceOrder;
using ViewModels.ServiceOrderCommentLine;
using ViewModels.ServiceOrderType;
using ViewServiceInterfaces;

namespace ViewServices
{
    public class ServiceOrderViewService : IServiceOrderViewService
    {
        private readonly ICustomerAddressDataService _customerAddressDataService;
        private readonly ICustomerDataService _customerDataService;
        private readonly ICustomerService _customerService;
        private readonly IDepartmentRepository _departmentRepository;
        private readonly IPortalStatusRespository _portalStatusRespository;
        private readonly IProjectRepository _projectRepository;
        private readonly IServiceAttachmentService _serviceAttachmentService;
        private readonly IServiceItemRepository _serviceItemRepository;
        private readonly IServiceOrderCommentLineRepository _serviceOrderCommentLineRepository;
        private readonly IServiceOrderService _serviceOrderService;
        private readonly IExpanditUserService _userService;
        private readonly bool _isAccountMangementEnabled;
        private readonly IServiceOrderReportService _serviceOrderReport;

        public ServiceOrderViewService(IPortalStatusRespository portalStatusRespository,
            IDepartmentRepository departmentRepository,
            ICustomerService customerService,
            IServiceOrderService serviceOrderService,
            IServiceOrderCommentLineRepository serviceOrderCommentLineRepository,
            IServiceAttachmentService serviceAttachmentService,
            IExpanditUserService userService,
            IProjectRepository projectRepository,
            ICustomerDataService customerDataService,
            ICustomerAddressDataService customerAddressDataService,
            IServiceItemRepository serviceItemRepository, 
            IServiceOrderReportService serviceOrderReport)
        {
            _portalStatusRespository = portalStatusRespository;
            _departmentRepository = departmentRepository;
            _customerService = customerService;
            _serviceOrderService = serviceOrderService;
            _serviceOrderCommentLineRepository = serviceOrderCommentLineRepository;
            _serviceAttachmentService = serviceAttachmentService;
            _userService = userService;
            _projectRepository = projectRepository;
            _customerDataService = customerDataService;
            _customerAddressDataService = customerAddressDataService;
            _serviceItemRepository = serviceItemRepository;
            _serviceOrderReport = serviceOrderReport;

            _isAccountMangementEnabled = IsAccountManagementEnabled();
        }

        public ServiceOrderItemFormModel PrepareCreateFormModel()
        {
            UserTable user = _userService.GetPortalUser();

            var formModel = new ServiceOrderItemFormModel
                {
                    User = user,
                    SelectedStartDate = DateTime.Now,
                    SelectedCustomerId = user.CustomerGuid,
                    CurrentShopCustomer = _customerService.GetCustomer(user.CustomerGuid),
                    CurrentPortalCustomer = _customerDataService.CustomerById(user.CustomerGuid)
                };

            PopulateDropdownsByUserPermissions(formModel);
            PopulateDeliveryDataFromCustomer(formModel);

            if (_isAccountMangementEnabled)
            {
                PopulateDropdownsFromAccountManagement(formModel);
            }

            if (user.RoleId.Equals("Customer"))
            {
                formModel.SelectedStatus = "APPOINTED";
            }

            return formModel;
        }

        public ServiceOrderItemFormModel PrepareEditFormModel(ServiceOrderItemFormModel formModel, string serviceOrderId, string serviceOrderStatusId = null)
        {
            formModel.IsInEditMode = true;
            formModel.User = _userService.GetPortalUser();

            if (formModel.SelectedStartDate == DateTime.MinValue)
                formModel.SelectedStartDate = null;

            PopulateDropdownsByUserPermissions(formModel);

            if (_isAccountMangementEnabled)
            {
                PopulateDropdownsFromAccountManagement(formModel);
            }

            formModel.ServiceOrderCommentLineItem = _serviceOrderCommentLineRepository.GetCommentLineByServiceOrderGuid(serviceOrderId);
            formModel.LongDescription = (formModel.ServiceOrderCommentLineItem == null) ? string.Empty : formModel.ServiceOrderCommentLineItem.CommentText;

            if (serviceOrderStatusId != null)
            {
                formModel.CurrentStatusItem = _serviceOrderService.GetServiceOrderPortalStatus(serviceOrderStatusId);
                formModel.SelectedStatus = (formModel.CurrentStatusItem == null) ? string.Empty : formModel.CurrentStatusItem.StatusGuid;
                formModel.ServiceOrderStatusList = Mapper.Map<IEnumerable<PortalStatusViewModel>>(_portalStatusRespository.All());
            }

            formModel.CurrentPortalCustomer = _customerDataService.CustomerById(formModel.SelectedCustomerId);
            formModel.CurrentShopCustomer = _customerService.GetCustomer(formModel.SelectedCustomerId);
            formModel.CurrentServiceItem = _serviceItemRepository.GetById(formModel.ServiceItemNo);

            if (formModel.CurrentStatusItem != null)
                formModel.SelectedStatus = formModel.CurrentStatusItem.StatusGuid;
            formModel.CurrentPortalCustomer = _customerDataService.CustomerById(formModel.SelectedCustomerId);

            return formModel;
        }

        public ServiceOrderItemFormModel PreparePostedFormModel(ServiceOrderFormPostModel formPostModel)
        {
            var formModel = Mapper.Map<ServiceOrderItemFormModel>(formPostModel);

            UserTable user = _userService.GetPortalUser();
            formModel.User = user;

            PopulateDropdownsByUserPermissions(formModel);

            if (_isAccountMangementEnabled)
                PopulateDropdownsFromAccountManagement(formModel);

            formModel.CurrentShopCustomer = _customerService.GetCustomer(user.CustomerGuid);
            formModel.CurrentServiceItem = _serviceItemRepository.GetById(formModel.ServiceItemNo);
            formModel.DepartmentItems = _departmentRepository.All().ToDictionary(key => key.DepartmentGuid, value => value.Description);
            formModel.ServiceItemNo = formPostModel.ServiceItemNo;
            formModel.ItemNo = formPostModel.ItemNo;
            formModel.Description = formPostModel.Description;
            formModel.IsInEditMode = _serviceOrderService.CanEditServiceOrder(formPostModel.ServiceOrderGuid);

            return formModel;
        }

        public ServiceOrderDetailViewModel PopulateDetails(string id)
        {
            var serviceOrder = _serviceOrderService.GetById(id);
            var orderId = serviceOrder.ServiceOrderGuid;
            var user = _userService.GetPortalUser();
            var serviceItem = _serviceItemRepository.GetById(serviceOrder.ServiceItemNo) ?? new ServiceItem {Description = serviceOrder.Description};

            var comments = Mapper.Map<IEnumerable<ServiceOrderCommentLineViewModel>>(_serviceOrderCommentLineRepository.GetAllCommentLinesByServiceOrderGuid(orderId));
            var attachments = Mapper.Map<IEnumerable<ServiceAttachmentListViewModel>>(_serviceAttachmentService.GetAllAttachmentsByServiceOrderGuid(orderId));

            var allStatusItems = Mapper.Map<IEnumerable<PortalStatusViewModel>>(_portalStatusRespository.All());
            var allServiceOrderTypes = Mapper.Map<IEnumerable<ServiceOrderTypeViewModel>>(_serviceOrderService.GetAllServiceOrderTypes());
            var allDepartments = _departmentRepository.All();

            var viewModel = Mapper.Map<ServiceOrderDetailViewModel>(serviceOrder);

            viewModel.ServiceItem = Mapper.Map<ServiceItemViewModel>(serviceItem);
            viewModel.User = user;
            viewModel.Attachments = attachments.OrderByDescending(x => x.BASGuid);
            viewModel.ServiceOrderPlanningStatus = allStatusItems;

            viewModel.Comments = comments.Where(
                x => x.CommentType == ServiceOrderCommentLineType.MessageFromPortal || 
                     x.CommentType == ServiceOrderCommentLineType.MessageToFromOffice ||
                     x.CommentType ==  ServiceOrderCommentLineType.MessageToInvoice).OrderByDescending(x => x.CommentDate);

            if (!_userService.UserAccess("ReadServiceOrderInternalComments"))
                if (!_userService.UserAccess("ReadServiceOrderExternalComments"))
                    viewModel.Comments = viewModel.Comments.Where(x => x.CommentType == ServiceOrderCommentLineType.MessageFromPortal);
                else
                    viewModel.Comments = viewModel.Comments.Where(x => x.CommentType == ServiceOrderCommentLineType.MessageFromPortal || x.CommentType == ServiceOrderCommentLineType.MessageToInvoice);
            else if (!_userService.UserAccess("ReadServiceOrderExternalComments"))
                viewModel.Comments = viewModel.Comments.Where(x => x.CommentType == ServiceOrderCommentLineType.MessageFromPortal || x.CommentType == ServiceOrderCommentLineType.MessageToFromOffice);

            foreach (var comment in viewModel.Comments)
                comment.PortalUserName = comment.UserType == "PORTAL" ? _userService.GetUser(comment.UserGuid).ContactName : comment.UserGuid;

            // This code seems to have no real meaning except showing a specific comment in the top. Therefore, it has been disabled
            //viewModel.CreatedComment = comments.SingleOrDefault(x => x.CommentType == ServiceOrderCommentLineType.MessageToFromOffice);
            viewModel.AddedComments = comments.Where(x => x.CommentType == ServiceOrderCommentLineType.MessageToFromOffice);
            viewModel.SelectedStatus = allStatusItems.SingleOrDefault(x => x.StatusGuid == viewModel.StatusCode);
            viewModel.ServiceOrderTypeItem = allServiceOrderTypes.SingleOrDefault(x => x.Code == viewModel.ServiceOrderType);
            viewModel.SelectedDepartment = allDepartments.SingleOrDefault(x => x.DepartmentGuid == viewModel.DepartmentGuid);
            viewModel.HasReport = _serviceOrderReport.ServiceOrderHasReport(id);
            viewModel.CommentForm = new ServiceOrderCommentFormModel
                {
                    UserGuid = user.UserGuid,
                    ServiceOrderGuid = orderId,
                    UserType = "PORTAL",
                    PortalUserName = user.UserLogin
                };
            viewModel.Reports = _serviceOrderReport.GetReportsForOrder(id);
            return viewModel;
        }

        private void PopulateDropdownsFromAccountManagement(ServiceOrderItemFormModel formModel)
        {
            Dictionary<string, string> allTechnicians = _customerService.GetAllTechnicians();
            Dictionary<string, string> allServiceManagers = _customerService.GetAllServiceManagers();
            IEnumerable<ServiceOrderTypeItem> allServiceOrderTypes = _serviceOrderService.GetAllServiceOrderTypes();
            IEnumerable<CustomerPropertyItem> properties = _customerService.GetPropertiesByCustomerId(formModel.User.CustomerGuid);

            if (!formModel.User.RoleId.Equals("Admin"))
            {
                IEnumerable<CustomerPropertyItem> tasks = properties.Where(p => p.RelationTableType == CustomerPropertyItem.TableType.Tasks);
                IEnumerable<CustomerPropertyItem> technicians = properties.Where(p => p.RelationTableType == CustomerPropertyItem.TableType.Technician);

                Dictionary<string, string> taskList = tasks.SelectMany(x => allServiceOrderTypes.Where(y => x.PropertyId == y.Code))
                    .ToDictionary(key => key.Code, value => value.Description);
                Dictionary<string, string> technicianList = technicians.SelectMany(x => allTechnicians.Where(y => x.PropertyId == y.Key))
                    .ToDictionary(key => key.Key, value => value.Value);
                Dictionary<string, string> serviceManagerList = allServiceManagers.SelectMany(x => allServiceManagers.Where(y => x.Key == y.Key))
                    .ToDictionary(key => key.Key, value => value.Value);

                switch (formModel.User.RoleId)
                {
                    case "CustomerGroup":
                        formModel.Projects = new Dictionary<string, string>();
                        break;
                    default:
                        formModel.Projects = _projectRepository.GetProjectsByCustomerId(formModel.User.CustomerGuid).ToDictionary(key => key.ProjectGuid, value => value.ProjectName);
                        break;
                }

                formModel.Technicians = technicianList;
                formModel.ServiceManagers = serviceManagerList;
                formModel.Tasks = taskList;
            }
            else
            {
                formModel.Technicians = allTechnicians;
                formModel.ServiceManagers = allServiceManagers;
                formModel.Tasks = allServiceOrderTypes.ToDictionary(key => key.Code, value => value.Description);
                formModel.Projects = _projectRepository.All().ToDictionary(key => key.ProjectGuid, value => value.ProjectName);
            }
        }

        private void PopulateDropdownsByUserPermissions(ServiceOrderItemFormModel formModel, string selectedCustomerAddressId = null)
        {
            if (_userService.UserAccess("EditServiceOrderDepartment"))
            {
                formModel.DepartmentItems = _departmentRepository.All().ToDictionary(key => key.DepartmentGuid, value => value.Description);
            }

            if (_userService.UserAccess("EditServiceOrderJobStatus"))
            {
                formModel.ServiceOrderStatusList = Mapper.Map<IEnumerable<PortalStatusViewModel>>(_portalStatusRespository.All());
            }

            if (_userService.UserAccess("EditServiceOrderJobType"))
            {
                formModel.Tasks = _serviceOrderService.GetAllServiceOrderTypes().ToDictionary(key => key.Code, value => value.Description);
            }

            if (_userService.UserAccess("EditServiceOrderProject"))
            {
                formModel.Projects = _projectRepository.All().ToDictionary(key => key.ProjectGuid, value => value.ProjectName);
            }

            if (_userService.UserAccess("EditServiceOrderServiceManager"))
            {
                formModel.ServiceManagers = _customerService.GetAllServiceManagers();
            }

            if (_userService.UserAccess("EditServiceOrderTechnician"))
            {
                formModel.Technicians = _customerService.GetAllTechnicians();
            }

            if (_userService.UserAccess("EditServiceOrderCustomer"))
            {
                formModel.CurrentPortalCustomer = _customerDataService.CustomerById(formModel.SelectedCustomerId);

                if (formModel.CurrentPortalCustomer != null)
                {
                    IEnumerable<CustomerAddressItem> customerAddress = _customerAddressDataService.AllByCustomerId(formModel.CurrentPortalCustomer.CustomerGuid);
                    List<CustomerAddressItem> customerAddressList = (customerAddress != null) ? customerAddress.ToList() : new List<CustomerAddressItem>();

                    customerAddressList.Insert(0, new CustomerAddressItem
                        {
                            Address1 = formModel.CurrentPortalCustomer.Address,
                            Address2 = formModel.CurrentPortalCustomer.Address2,
                            AddressGuid = string.Empty,
                            CityName = formModel.CurrentPortalCustomer.CityName,
                            CompanyName = formModel.CurrentPortalCustomer.CompanyName,
                            ContactPerson = formModel.CurrentPortalCustomer.ContactName,
                            CustomerGuid = formModel.CurrentPortalCustomer.CustomerGuid,
                            ZipCode = formModel.CurrentPortalCustomer.ZipCode
                        });

                    formModel.CustomerAddressList = customerAddressList.ToDictionary(key => key.AddressGuid, value => (string.Format("{0} - {1}", value.Address1, value.ZipCode)));
                    if (selectedCustomerAddressId != null)
                    {
                        formModel.SelectedCustomerDeliveryAddress = selectedCustomerAddressId;
                    }
                }
            }
        }

        private void PopulateDeliveryDataFromCustomer(ServiceOrderItemFormModel form)
        {
            if (form.CurrentShopCustomer != null)
            {
                form.ShiptoContactName = form.CurrentShopCustomer.ContactName;
                form.ShipToCompanyName = form.CurrentShopCustomer.CompanyName;
                form.ShipToAddress = form.CurrentShopCustomer.Address;
                form.ShipToZipCode = form.CurrentShopCustomer.ZipCode;
                form.ShipToCityName = form.CurrentShopCustomer.CityName;
                form.ShiptoEmailAddress = form.CurrentShopCustomer.EmailAddress;
            }
        }

        private bool IsAccountManagementEnabled()
        {
            string isAccountManagementEnabled = ConfigurationManager.AppSettings["ENABLE_ACCOUNT_MANAGEMENT"];
            bool result = Utilities.CBoolEx(isAccountManagementEnabled);

            return result;
        }
    }
}