﻿using System;
using System.Configuration;

namespace UIHelpers
{
    public static class ConfigurationHelper
    {
        public static bool ReadBoolean(string key, bool defaultMissingValue = false)
        {
            string value = ConfigurationManager.AppSettings[key];
            if (string.IsNullOrWhiteSpace(value))
                return defaultMissingValue;

            return ("1".Equals(value) || "true".Equals(value, StringComparison.InvariantCultureIgnoreCase));
        }
    }
}