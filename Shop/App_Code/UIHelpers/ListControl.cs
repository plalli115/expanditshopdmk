﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using EISCS.Shop.DO.Interface;

namespace UIHelpers
{
    public class ListControl
    {
        public static int PAGE_SIZE = 9;
        private int _totalPages = -1;

        public ListControl()
        {
            PageNumber = 1;
            Sort = "";
            Direction = "asc";
            SortLinksForGroupTemplate = new List<SelectListItem>();
        }

        public int PageNumber { get; set; }
        public int TotalItems { get; set; }
        public string Sort { get; set; }
        public string Direction { get; set; }
        public string SearchString { get; set; }
        public List<SelectListItem> SortLinksForGroupTemplate { get; set; }

        public int GetTotalPages()
        {
            if (_totalPages == -1)
            {
                double temp = TotalItems/(double) PAGE_SIZE;
                _totalPages = Convert.ToInt32(Math.Ceiling(temp));
            }

            return _totalPages;
        }

        public bool HasNextPage()
        {
            return GetTotalPages() >= PageNumber + 1;
        }

        public bool HasPreviousPage()
        {
            return 1 < PageNumber;
        }

        public int GetStartIndex1Based()
        {
            return (PageNumber*PAGE_SIZE) - PAGE_SIZE + 1;
        }

        public int GetEndIndex1Based()
        {
            return GetStartIndex1Based() + (PAGE_SIZE) - 1;
        }


        public string FindSortColumnProductslist()
        {
            string res = "";

            //This is done to make sure one direct sql-inject issue with url-hacking
            switch (Sort)
            {
                case "productname":
                    res = "ProductName";
                    break;
                case "price":
                    res = ProductServiceConstants.PRICE_SORT;
                    break;
            }

            return res;
        }

        public int GetMaxEndIndex(int startIndex)
        {
            if (PAGE_SIZE + startIndex > TotalItems)
            {
                return TotalItems - startIndex + 1;
            }
            return PAGE_SIZE;
        }

        public string CreateLink(string sortColumn)
        {
            string direction = "asc";
            if (sortColumn == Sort && Direction == "asc")
            {
                direction = "desc";
            }

            return CreateLink(sortColumn, direction);
        }

        public string CreateLink(string sortColumn, string direction)
        {
            return HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Path) + "?sort=" + sortColumn + "&direction=" + direction + "&searchstring=" + SearchString + "&pagenumber=1";
        }

        public string CreateLink(int pageNum)
        {
            return HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Path) + "?sort=" + Sort + "&direction=" + Direction + "&searchstring=" + SearchString + "&pagenumber=" +
                   pageNum;
        }

        public void CreateLinkForGroupSort(string text, string sortColumn, string direction)
        {
            string link = CreateLink(sortColumn, direction);
            var item = new SelectListItem {Text = text, Value = link};
            if (sortColumn.Equals(Sort) && direction.Equals(Direction))
            {
                item.Selected = true;
            }

            SortLinksForGroupTemplate.Add(item);
        }
    }
}