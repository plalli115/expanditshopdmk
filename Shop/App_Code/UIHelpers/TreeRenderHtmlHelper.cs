﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Handlers;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.UI;
using CmsPublic.DataProviders.Logging;
using CmsPublic.ModelLayer.Interfaces;
using CmsPublic.ModelLayer.Models;
using EISCS.CMS.Build;
using EISCS.CMS.Services.ResourceHandling;

namespace UIHelpers
{
    ///<summary>
    ///</summary>
    public static class UrlResourceHelper
    {
        private static Dictionary<string, string> _lookUp;

        ///<summary>
        ///</summary>
        ///<param name="htmlHelper"></param>
        ///<param name="type"></param>
        ///<param name="imagePath"></param>
        ///<returns></returns>
        public static string Content(this HtmlHelper htmlHelper, Type type, string imagePath)
        {
            if (_lookUp == null)
            {
                _lookUp = new Dictionary<string, string>();
            }
            try
            {
                if (_lookUp.ContainsKey(imagePath))
                {
                    return _lookUp[imagePath];
                }
            }
            catch
            {
            }
            string resourceUrl = GetWebResourceUrl(type, imagePath);
            // Old code for .NET 3.5 below
            //string resourceUrl = GetVirtualRoot() + "/" + GetWebResourceUrl(type, imagePath);
            _lookUp.Add(imagePath, resourceUrl);
            return resourceUrl;
        }

        ///<summary>
        ///</summary>
        ///<param name="type"></param>
        ///<param name="resourceId"></param>
        ///<returns></returns>
        public static string GetWebResourceUrl(Type type, string resourceId)
        {
            MethodInfo mi = typeof(AssemblyResourceLoader).GetMethod(
                "GetWebResourceUrlInternal",
                BindingFlags.NonPublic | BindingFlags.Static);
            return (string)mi.Invoke(null,
                                      new object[] { Assembly.GetAssembly(type), resourceId, false, false, null });
            // Below works for .NET Framework 3.5 but seems to have changed in 4.0
            //return (string)mi.Invoke(null,
            //                          new object[] { Assembly.GetAssembly(type), resourceId, false });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static string GetVirtualRoot()
        {
            if (HttpRuntime.AppDomainAppVirtualPath != null)
                return HttpRuntime.AppDomainAppVirtualPath.Equals("/") ? "" : HttpRuntime.AppDomainAppVirtualPath;
            return "";
        }
    }

    ///<summary>
    ///</summary>
    public static class TreeRenderHtmlHelper
    {
        ///<summary>
        ///</summary>
        ///<param name="htmlHelper"></param>
        ///<param name="rootLocations"></param>
        ///<param name="locationRenderer"></param>
        ///<param name="counter"></param>
        ///<typeparam name="T"></typeparam>
        ///<returns></returns>
        public static string RenderTree<T>(
            this HtmlHelper htmlHelper,
            IEnumerable<T> rootLocations,
            Func<T, Group> locationRenderer, int counter)
            where T : IComposite<T>
        {
            return new TreeRenderer<T>(rootLocations, locationRenderer, htmlHelper, counter).Render();
        }
    }

    ///<summary>
    ///</summary>
    ///<typeparam name="T"></typeparam>
    public class TreeRenderer<T> where T : IComposite<T>
    {
        private readonly HtmlHelper _helper;
        private readonly Func<T, Group> _locationRenderer;
        private readonly IEnumerable<T> _rootLocations;

        private string[] _contractedItems;
        private int _counter;
        private string _imageUrl;
        private HtmlTextWriter _writer;

        ///<summary>
        ///</summary>
        ///<param name="rootLocations"></param>
        ///<param name="locationRenderer"></param>
        ///<param name="htmlHelper"></param>
        ///<param name="counter"></param>
        public TreeRenderer(
            IEnumerable<T> rootLocations,
            Func<T, Group> locationRenderer, HtmlHelper htmlHelper, int counter)
        {
            _rootLocations = rootLocations;
            _locationRenderer = locationRenderer;
            _helper = htmlHelper;
            _counter = counter;
        }

        ///<summary>
        ///</summary>
        ///<returns></returns>
        public string Render()
        {
            HttpCookieCollection cookies = HttpContext.Current.Request.Cookies;
            if (cookies != null && cookies.Count > 0)
            {
                HttpCookie cookie = cookies["contractedItems"];
                if (cookie != null)
                {
                    string value = HttpContext.Current.Server.UrlDecode(cookie.Value);
                    _contractedItems = value.Split(new[] { ',' });
                }
            }
            _writer = new HtmlTextWriter(new StringWriter());
            // Cache resource url
            // Is not working in .net 4.0
            try
            {
                //_imageUrl = UrlResourceHelper.GetWebResourceUrl(typeof(WebResourceProxy), "EISCMS.Content.Images.minus1.png");
            }
            catch (Exception ex)
            {
                FileLogger.Log(ex.Message + " " + ex.StackTrace);
            }
            RenderLocations(_rootLocations);
            return _writer.InnerWriter.ToString();
        }

        /// <summary>
        /// Recursively walks the location tree outputting it as hierarchical UL/LI elements
        /// </summary>
        /// <param name="locations"></param>
        private void RenderLocations(IEnumerable<T> locations)
        {
            if (locations == null) return;
            if (!locations.Any()) return;

            InUl(() => locations.ForEach(location => InLi(location as Group, () =>
            {
                _writer.Write(
                    RenderTags(
                        _locationRenderer(location)));
                RenderLocations(location.Children);
            })));
        }

        private void InUl(Action action)
        {
            _writer.WriteLine();
            if (_counter++ == 0)
            {
                _writer.AddAttribute(HtmlTextWriterAttribute.Id, "sitemap");
            }
            _writer.AddAttribute(HtmlTextWriterAttribute.Class, "collapse in");
            _writer.RenderBeginTag(HtmlTextWriterTag.Ul);
            action();
            _writer.RenderEndTag();
            _writer.WriteLine();
        }

        private void InLi(Group locations, Action action)
        {
            _writer.AddAttribute(HtmlTextWriterAttribute.Id, "id_" + locations.GroupGuid);
            // TEST   
            if (_contractedItems != null)
            {
                if (_contractedItems.Contains("id_" + locations.GroupGuid))
                {
                    if (locations.HasChildren() || locations.HadChildren)
                    {
                        _writer.AddAttribute(HtmlTextWriterAttribute.Class, "contracted");
                    }
                }
            }

            // END TEST
            _writer.RenderBeginTag(HtmlTextWriterTag.Li);
            // Code for .NET 4.0
            _writer.Write("<div class=\"sortablezone\"><img src=\"" + _imageUrl + "\" alt=\"\" height=\"1\" width=\"1\"/></div>");
            // Original code written for .NET 3.5        
            //_writer.Write("<div class=\"sortablezone\"><img src=\"" + UrlResourceHelper.GetVirtualRoot() + "/" +
            //              _imageUrl + "\" alt=\"\" height=\"1\" width=\"1\"/></div>");
            if (locations.HasChildren())
            {
                _writer.Write("<span class=\"expand\"></span>");
            }
            if (locations.HadChildren)
            {
                _writer.Write("<span class=\"expand contract isparent\"></span>");
            }
            _writer.Write("<div class=\"dropzone\"></div>");
            // END TEST
            _writer.Write("<dl class=\"ui-droppable\">");
            _writer.Write("<dt>");
            action();
            _writer.RenderEndTag();
            _writer.WriteLine();
        }

        private string RenderTags(Group t)
        {
            var urlHelper = new UrlHelper(_helper.ViewContext.RequestContext, _helper.RouteCollection);
            var anchortagBuilder = new TagBuilder("a");
            anchortagBuilder.AddCssClass("grouptreelink");
            string nameString = null;
            try
            {
                if (t.Properties.PropDict.ContainsKey("NAME"))
                {
                    nameString = t.Properties.PropDict["NAME"];
                }
            }
            catch
            {
            }

            string name;
            if (string.IsNullOrEmpty(nameString))
            {
                string testVal = null;
                try
                {
                    var langGuid = (string)HttpContext.Current.Session["CurrentLanguage"];
                    testVal = ResourceManager.GetTranslatedValue("CATMAN_LABEL_UNKNOWN_PROPERTY", langGuid);
                }
                catch
                {
                }
                name = string.IsNullOrEmpty(testVal) ? CustomResourceHandler.CATMAN_LABEL_UNKNOWN_PROPERTY : testVal;
            }
            else
            {
                name = nameString;
            }

            anchortagBuilder.SetInnerText(name); // Sets the visible link text

            anchortagBuilder.MergeAttributes(new RouteValueDictionary());
            object routeValues = new { id = t.GroupGuid };

            anchortagBuilder.MergeAttribute("href", urlHelper.Action("GroupEdit", "Catalog", routeValues));

            var sb = new StringBuilder();
            sb.AppendLine(anchortagBuilder.ToString(TagRenderMode.Normal));
            sb.AppendLine("</dt>");
            sb.AppendLine("<dd>");
            sb.AppendLine("</dd>");
            sb.AppendLine("</dl>");
            return sb.ToString();
        }
    }

    ///<summary>
    ///</summary>
    ///<param name="value"></param>
    ///<typeparam name="T"></typeparam>
    public delegate void Func<T>(T value);

    ///<summary>
    ///</summary>
    public static class Extension
    {
        ///<summary>
        ///</summary>
        ///<param name="values"></param>
        ///<param name="function"></param>
        ///<typeparam name="T"></typeparam>
        public static void ForEach<T>(this IEnumerable<T> values, Func<T> function)
        {
            foreach (T value in values)
            {
                function(value);
            }
        }
    }
}