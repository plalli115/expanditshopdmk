﻿using System.Collections.Generic;
using System.Globalization;
using Resources;

namespace UIHelpers
{
    public static class ServiceOrderViewHelper
    {
        private const string FORM_LABEL_CLASS_NAME = "control-label col-md-4";
        private const string FIELD_CSS_CLASS = "col-md-6";
        private const string TRANSLATION_KEY = "SO_LABEL_SERVICEORDER_STATUS_";
        private const string DEFAULT_LABEL_KEY = "INITIAL";
        private const string STANDARD_SIZE_OF_TEXT = "12";

        private static readonly Dictionary<string, string> StatusCssClasses = new Dictionary<string, string>
            {
                {"doing", "fa-clock"},
                {"done", "fa-check-square-o"},
                {"warning", "fa-warning"},
                {"Igangsat", "fa-clock-o"}
            };

        private static readonly Dictionary<string, string> StatusIconMapper = new Dictionary<string, string>
            {
                {"INITIAL", "default"},
                {"INACTIVE", "info"},
                {"INCOMPLETE", "info"},
                {"OFFDUTY", "info"},
                {"ONHOLD", "info"},
                {"TRAVELSTARTED", "info"},
                {"ACTIVE", "info"},
                {"ACCEPTED", "info"},
                {"PARTLYSERV", "info"},
                {"APPOINTED", "info"},
                {"READY", "primary"},
                {"STARTED", "primary"},
                {"IN PROCESS", "primary"},
                {"MISSING INFO", "warning"},
                {"URGENT", "danger"},
                {"COMPLETE", "success"},
                {"COMPLETED", "success"},
                {"FINISHED", "success"},
                {"REJECTED", "danger"}
            };

        public static string GetStatusCssClass(string status)
        {
            return StatusCssClasses.ContainsKey(status) ? StatusCssClasses[status] : "icon-lock";
        }

        public static string FormFieldWidthCssClass()
        {
            return FIELD_CSS_CLASS;
        }

        public static string GetFormLabelClassName()
        {
            return FORM_LABEL_CLASS_NAME;
        }

        public static string GetServiceOrderStatusTranslation(string statusId, string textSize)
        {
            return BuildServiceOrderStatusLabel((statusId==null ? "INITIAL" : statusId.Trim()), textSize);
        }

        private static string BuildServiceOrderStatusLabel(string serviceOrderStatus, string textSize)
        {
            const string labelHtlm = "<span class=\"label label-{0} label-{1} \" style='font-size: {2}px;'>{3}</span>";
            string bootstrapClass = "default";

            if (serviceOrderStatus == "")
                serviceOrderStatus = "INITIAL";
            string translationKey = BuildTranslationKey(serviceOrderStatus);
            string translatedValue = ResourceHelper.GetString(typeof (Language), translationKey, CultureInfo.CurrentCulture);
            if (translatedValue == "")
                translatedValue = serviceOrderStatus;

            var serviceOrderTextSize = (string.IsNullOrEmpty(textSize) ? STANDARD_SIZE_OF_TEXT : textSize);
            var serviceOrderStatusDictKey = (string.IsNullOrEmpty(serviceOrderStatus)) ? DEFAULT_LABEL_KEY : serviceOrderStatus;
            var serviceOrderStatusClassValue = (string.IsNullOrEmpty(serviceOrderStatus)) ? string.Empty : (NonBreakSpaceToUnderScore(serviceOrderStatus));
            if (StatusIconMapper.ContainsKey(serviceOrderStatus))
                bootstrapClass = StatusIconMapper[serviceOrderStatus];
            return string.Format(labelHtlm, bootstrapClass, serviceOrderStatusClassValue, serviceOrderTextSize, translatedValue);
        }

        private static string BuildTranslationKey(string labelText)
        {
            string translateKey = string.IsNullOrEmpty(labelText) ? DEFAULT_LABEL_KEY : labelText;
            return string.Format("{0}{1}", TRANSLATION_KEY, NonBreakSpaceToUnderScore(translateKey));
        }

        private static string NonBreakSpaceToUnderScore(string stringValue)
        {
            return stringValue.Replace(" ", "_");
        }
    }
}