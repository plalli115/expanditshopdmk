﻿using System;
using System.Web;
using System.Web.Mvc;

namespace UIHelpers
{
    /// <summary>
    /// Summary description for MailHelpers
    /// </summary>
    public static class MailHelpers
    {
        public static MvcHtmlString EmbedCss(this HtmlHelper htmlHelper, string path)
        {
            var cssFilePath = HttpContext.Current.Server.MapPath(path);
            try
            {
                var cssText = System.IO.File.ReadAllText(cssFilePath);
                var styleElement = new TagBuilder("style");
                styleElement.SetInnerText(cssText);
                return MvcHtmlString.Create(styleElement.ToString());
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}