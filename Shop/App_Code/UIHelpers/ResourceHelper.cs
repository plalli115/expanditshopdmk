﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Resources;
using System.Web.Script.Serialization;
using Resources;

namespace UIHelpers
{
    public static class ResourceHelper
    {
        public static string GetString(Type resourcetype, string resourceName)
        {
            return new ResourceManager(resourcetype.FullName, resourcetype.Assembly).GetString(resourceName);
        }

        public static string GetString(Type resourcetype, string resourceName, CultureInfo culture)
        {
            return new ResourceManager(resourcetype.FullName, resourcetype.Assembly).GetString(resourceName, culture);
        }

        public static object GetObject(Type resourcetype, string resourceName)
        {
            return new ResourceManager(resourcetype.FullName, resourcetype.Assembly).GetObject(resourceName);
        }

        public static object GetObject(Type resourcetype, string resourceName, CultureInfo culture)
        {
            return new ResourceManager(resourcetype.FullName, resourcetype.Assembly).GetObject(resourceName, culture);
        }

        public static object GetLanguageTranslations()
        {
            ResourceManager resourceManager = Language.ResourceManager;
            ResourceSet resourceSet = resourceManager.GetResourceSet(CultureInfo.CurrentUICulture, true, true);

            Dictionary<object, object> dictionaryEntries =
                resourceSet.Cast<DictionaryEntry>().ToDictionary(key => key.Key, value => value.Value);

            return dictionaryEntries;
        }

    }
}