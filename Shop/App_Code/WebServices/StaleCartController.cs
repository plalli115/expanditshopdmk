﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using EISCS.Shop.BO.BusinessLogic;
using EISCS.Shop.DO.Interface;
using EISCS.Shop.DO.Repository;
using EISCS.Wrappers.Configuration;
using Filters;
using Mailers;
using Mvc.Mailer;
using Resources;
using ViewModels;

namespace WebServices
{
    [HmacApiAuthentication]
    public class StaleCartController : ApiController
    {
        private readonly IUserMailer _userMailer;
        private readonly ICartService _cartService;
        private readonly IExpanditUserService _userService;
        private readonly IConfigurationObject _configuration;
        private readonly ICurrencyConverter _currencyConverter;
        private readonly IPaymentService _paymentService;
        private readonly IMailMessageSettingRepository _mailMessageSettingRepository;

        public StaleCartController(IUserMailer userMailer, ICartService cartService, IExpanditUserService userService, IConfigurationObject configuration,
            ICurrencyConverter currencyConverter, IPaymentService paymentService, IMailMessageSettingRepository mailMessageSettingRepository)
        {
            _userMailer = userMailer;
            _cartService = cartService;
            _userService = userService;
            _configuration = configuration;
            _currencyConverter = currencyConverter;
            _paymentService = paymentService;
            _mailMessageSettingRepository = mailMessageSettingRepository;
        }

        private void SendStaleCartsReminder()
        {
            var settings = _mailMessageSettingRepository.All().FirstOrDefault();
            int maxAge = 0;
            if (settings != null)
            {
                maxAge = settings.SendAfterStalePeriod;
            }
            var carts = _cartService.GetStaleUserCarts(maxAge);
            foreach (var cartHeader in carts)
            {
                var user = _userService.GetUser(cartHeader.UserGuid);
                if (user != null)
                {
                    var infoNodes = _cartService.GetProducts(cartHeader);
                    var shippingProviderName = _cartService.ShippingHandlingName(cartHeader);

                    shippingProviderName = !string.IsNullOrEmpty(shippingProviderName)
                                                       ? Language.ResourceManager.GetString(shippingProviderName)
                                                       : Language.LABEL_NOT_SELECTED;
                    CartViewModel model = new CartViewModel(cartHeader, infoNodes, _userService, shippingProviderName, _configuration, _currencyConverter, _paymentService);
                    MvcMailMessage msg = _userMailer.StaleUserCartReminder(model, user.EmailAddress);
                    msg.Send();
                }
            }
        }

        [HttpGet]
        [ActionName("sendstalecartmail")]
        public IEnumerable<string> SendStaleCartMail()
        {
            SendStaleCartsReminder();
            return new[] { "value1", "value2" };
        }

        // GET api/<controller>
        public IEnumerable<string> Get()
        {
            return new[] { "value1", "value2" };
        }

        // GET api/<controller>/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}
