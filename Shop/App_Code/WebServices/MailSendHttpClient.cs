﻿using System;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using EISCS.ExpandITFramework.Infrastructure;
using EISCS.ExpandITFramework.Util;
using EISCS.Shop.DO.Dto;
using EISCS.Shop.DO.Repository;
using Hangfire;

namespace WebServices
{
    public interface IMailSendClient
    {
        void InitializeMailSendRecurringTask();
    }

    public class MailSendHttpClient : IMailSendClient
    {
        private readonly IMailMessageSettingRepository _mailMessageSettingRepository;

        public MailSendHttpClient(IMailMessageSettingRepository mailMessageSettingRepository)
        {
            _mailMessageSettingRepository = mailMessageSettingRepository;
        }

        public static bool SendReminderEmail(string shopFullPath)
        {
            if (DataAccess.IsLocalMode && !ExpanditLib2.CBoolEx(ConfigurationManager.AppSettings["EXPANDIT-MAIL-APP-SEND-IN-LOCALMODE"]))
            {
                return true;
            }

            string suffix = shopFullPath.EndsWith("/") ? "" : "/";
            
            var key = ConfigurationManager.AppSettings["EXPANDIT-MAIL-API-KEY"];
            var appId = ConfigurationManager.AppSettings["EXPANDIT-MAIL-APP-ID"];
            var scheme = ConfigurationManager.AppSettings["EXPANDIT-MAIL-APP-SCHEME"];

            var hmacDelegatingHandler = new HmacDelegatingHandler(appId, key, scheme);

            HttpClient client = HttpClientFactory.Create(hmacDelegatingHandler);

            client.BaseAddress = new Uri(shopFullPath + suffix);

            HttpResponseMessage responseMessage = client.GetAsync("api/stalecart/sendstalecartmail").Result;
            try
            {
                responseMessage.EnsureSuccessStatusCode();
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        public void InitializeMailSendRecurringTask()
        {
            var mailMessageSettings = _mailMessageSettingRepository.All().FirstOrDefault();
            //var settings = AutoMapper.Mapper.Map<StaleCartMailMessageSettings>(mailMessageSettings);
            //ActivateTask(settings);
        }

        public static void ActivateTask(StaleCartMailMessageSettings settings)
        {
            if (settings == null)
            {
                return;
            }

            int today = DateTime.Now.Day;
            string url = AppUtil.ShopFullPath();
            string cron = string.Format("{0} {1} {2}/{3} * *", settings.SendTime.Minutes, settings.SendTime.Hours, today, settings.RepeatPeriod);
            RecurringJob.AddOrUpdate("", () => SendReminderEmail(url), cron);
        }

        public static void DeactivateTask()
        {
            RecurringJob.RemoveIfExists("send-reminder-email");
        }
    }
}