﻿using System.Collections.Generic;
using ViewModels;

namespace Mailers
{
    /// <summary>
    /// Summary description for MailPromotionContainer
    /// </summary>
    public class MailPromotionContainer
    {
        public List<CartViewModel.ProductItem> PromotionProductItems { get; set; }
        public string PromotionHeaderText { get; set; }
        public string CtaText { get; set; }
    }
}