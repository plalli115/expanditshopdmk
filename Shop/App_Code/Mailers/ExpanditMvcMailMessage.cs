﻿using System.Net.Mail;
using Mvc.Mailer;

namespace Mailers
{
    /// <summary>
    /// Summary description for ExpanditMvcMailMessage
    /// </summary>
    public class ExpanditMvcMailMessage : MvcMailMessage
    {
        private readonly SmtpClient _client;

        public ExpanditMvcMailMessage(string from, string to, SmtpClient client)
            : base(from, to)
        {
            _client = client;
        }

        public override ISmtpClient GetSmtpClient()
        {
            if (MailerBase.IsTestModeEnabled)
            {
                return new TestSmtpClient();
            }
            return new SmtpClientWrapper(_client);
        }
    }
}