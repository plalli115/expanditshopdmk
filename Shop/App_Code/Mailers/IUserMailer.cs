using System.Collections.Generic;
using Mvc.Mailer;
using ViewModels;

namespace Mailers
{
    public interface IUserMailer
    {
        MvcMailMessage StaleUserCartReminder(CartViewModel viewModel, string toAddress);
        MvcMailMessage Welcome(string toAddress, string name, string login);
        MvcMailMessage WelcomeConfirmAccount(string toAddress, string name, string login, string uniqueToken);
        MvcMailMessage PasswordChange(string toAddress);
        MvcMailMessage UserUpdate(string toAddress, string name, string login);
        MvcMailMessage OrderConfirmation(CartViewModel viewModel, string toAddress);
        MvcMailMessage TipFriend(string sendersName, string toAddress, string fromAddress, ProductsViewModel model, string currentUrl);
        MvcMailMessage LostPassword(string toAddress, string contactName, string userName, string password);
        MvcMailMessage ResetPassword(string toAddress, string contactName, string userName, string uniqueToken);
        MvcMailMessage WebContact(ContactUsViewModel model, string toAddress);
    }
}