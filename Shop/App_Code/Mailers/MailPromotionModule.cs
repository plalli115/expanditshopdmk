﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using CmsPublic.ModelLayer.Models;
using EISCS.CMS.Services.ImageService;
using EISCS.ExpandIT;
using EISCS.ExpandITFramework.Util;
using EISCS.Routing.Providers;
using EISCS.Shop.DO.Dto.Cart;
using EISCS.Shop.DO.Interface;
using EISCS.Shop.DO.Service;
using EISCS.Wrappers.Configuration;
using Resources;
using ViewModels;

namespace Mailers
{
    public class MailPromotionModule : IMailPromotionModule
    {
        private readonly IProductService _productService;
        private readonly string _userCurrency;
        private readonly string _userLanguage;
        private readonly IExpanditUserService _userService;
        private readonly IConfigurationObject _configurationObject;
        private readonly IMailMessageRepository _mailMessageRepository;
        private List<ProductInfoNode> _productInfo;

        public bool HasError { get; set; }

        public MailPromotionModule(IProductService productService, IExpanditUserService userService, IConfigurationObject configurationObject, IMailMessageRepository mailMessageRepository)
        {
            _userCurrency = userService.CurrencyGuid;
            _userLanguage = userService.LanguageGuid;
            _productService = productService;
            _userService = userService;
            _configurationObject = configurationObject;
            _mailMessageRepository = mailMessageRepository;
        }

        public void SetImagesAsResources(List<CartViewModel.ProductItem> viewModel, Dictionary<string, string> resources)
        {
            foreach (var model in viewModel)
            {
                string pathToImage = (string.IsNullOrWhiteSpace(model.Picture2) || model.Picture2 == "~/") ? "~/images/medium_camera.png" : model.Picture2;

                string extension = Path.GetExtension(pathToImage);
                if (extension != null)
                {
                    string newPath = pathToImage.Substring(0, pathToImage.Length - extension.Length);
                    newPath += "_thumb" + extension;

                    var thumbNail = ImageHandler.CreateThumbnail(HttpContext.Current.Server.MapPath(pathToImage), 100, 100, extension);
                    var theThumbnailPath = HttpContext.Current.Server.MapPath(newPath);
                    File.WriteAllBytes(theThumbnailPath, thumbNail);

                    pathToImage = newPath;
                }

                if (model.ProductLink == null)
                {
                    model.ProductLink = "notfound/" + Guid.NewGuid();
                    resources.Add(model.ProductLink, HttpContext.Current.Server.MapPath(pathToImage));
                }
                if (!resources.ContainsKey(model.ProductLink))
                {
                    resources.Add(model.ProductLink, HttpContext.Current.Server.MapPath(pathToImage));
                }
            }
        }

        public MailPromotionContainer MailPromotedProducts(ExpanditMailMessage message, Dictionary<string, string> resources)
        {
            var viewModel = MailPromotedProducts(message.MailMessageGuid);

            SetImagesAsResources(viewModel, resources);

            var html = message.GetPropertyValue("PROMOTIONTEXT");
            var promotionCtaText = message.GetPropertyValue("PROMOTIONCTATEXT");
            promotionCtaText = string.IsNullOrWhiteSpace(promotionCtaText) ? Language.VIEW_PRODUCT_ONLINE_CTA : promotionCtaText;

            MailPromotionContainer container = new MailPromotionContainer
                {
                    PromotionProductItems = viewModel, 
                    PromotionHeaderText = html.NormalizeHtmlContent(), 
                    CtaText = promotionCtaText
                };

            return container;
        }

        private List<CartViewModel.ProductItem> MailPromotedProducts(int mailMessageGuid)
        {
            IEnumerable<string> guids = _mailMessageRepository.GetMailProducts(mailMessageGuid);
            var viewModel = GetPromotionProductViewModel(guids.ToList());
            return viewModel;
        }

        private List<CartViewModel.ProductItem> GetPromotionProductViewModel(ICollection<string> guids)
        {
            var productInfoNodes = GetProductInfo(guids);
            _productInfo = productInfoNodes.ProductInfoNodes;
            var productItems = SetProductItems(productInfoNodes.Header);
            return productItems;
        }

        private ProductInfoNodeContainer GetProductInfo(ICollection<string> guids)
        {
            CartHeader info = ProductServiceHelper.CreateCartHeader(_userService);
            var container = _productService.GetProducts(info, guids);
            container.Header = info;
            return container;
        }

        private List<CartViewModel.ProductItem> SetProductItems(CartHeader cartHeader)
        {
            var productItems = new List<CartViewModel.ProductItem>();

            foreach (ProductInfoNode productInfo in _productInfo)
            {
                if (productInfo.Prod == null)
                {
                    continue;
                }
                if (productInfo.Messages != null)
                {
                    HasError = true;
                }
                var lineTotalInclTax = productInfo.Line.TotalInclTax = productInfo.Line.LineTotal * (1 + ExpanditLib2.ConvertToDbl(productInfo.Line.TaxPct) / 100);
                var item = new CartViewModel.ProductItem
                {
                    ShortDescription =
                        productInfo.Prod.GetPropertyValue("DESCRIPTION").
                            TruncateString(60,
                                TruncateOptions.IncludeEllipsis |
                                TruncateOptions.FinishWord),
                    LongDescription =
                        productInfo.Prod.GetPropertyValue("DESCRIPTION").
                            TruncateString(600,
                                TruncateOptions.IncludeEllipsis |
                                TruncateOptions.FinishWord),
                    ProductGuid = productInfo.Line.ProductGuid,
                    ProductName = productInfo.Prod.ProductName,
                    Picture1 = "~/" + productInfo.Prod.GetPropertyValue("PICTURE1"),
                    Picture2 = "~/" + productInfo.Prod.GetPropertyValue("PICTURE2"),
                    VariantCode = productInfo.Line.VariantCode,
                    VariantName = productInfo.Line.VariantName,
                    VersionGuid = productInfo.Line.VersionGuid,
                    LineGuid = productInfo.Line.LineGuid,
                    Quantity = productInfo.Line.Quantity,
                    LineDiscount = productInfo.Line.LineDiscount,
                    UserLanguage = _userLanguage,
                };

                item.ProductLink = RouteManager.Route.CreateTemplateProductLink(productInfo.Prod.ProductGuid, 0, _userLanguage);

                string taxDisplayConfiguration = ExpanditLib2.CStrEx(_configurationObject.Read("SHOW_TAX_TYPE")).ToUpper();

                if (taxDisplayConfiguration == "CUST" && cartHeader.PricesIncludingVat)
                {
                    taxDisplayConfiguration = "INCL";
                }

                switch (taxDisplayConfiguration)
                {
                    case "INCL":
                        item.DisplayLineTotal = CurrencyFormatter.FormatCurrency(lineTotalInclTax, _userCurrency);
                        item.DisplayListPrice = CurrencyFormatter.FormatCurrency(
                            productInfo.Line.ListPriceInclTax, _userCurrency);
                        item.DisplayLineDiscountAmount = CurrencyFormatter.FormatCurrency(
                            productInfo.Line.LineDiscountAmountInclTax, _userCurrency);
                        break;
                    default:
                        item.DisplayLineTotal = CurrencyFormatter.FormatCurrency(
                            productInfo.Line.LineTotal, _userCurrency);
                        item.DisplayListPrice =
                            CurrencyFormatter.FormatCurrency(
                                productInfo.Line.ListPrice, _userCurrency);
                        item.DisplayLineDiscountAmount = CurrencyFormatter.FormatCurrency(
                            productInfo.Line.LineDiscountAmount, _userCurrency);
                        break;
                }

                productItems.Add(item);
            }
            return productItems;
        }
    }
}