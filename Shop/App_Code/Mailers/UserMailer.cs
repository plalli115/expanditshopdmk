using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Web;
using CmsPublic.ModelLayer.Models;
using EISCS.ExpandITFramework.Util;
using EISCS.Shop.DO.Dto;
using EISCS.Shop.DO.Interface;
using EISCS.Shop.DO.Repository;
using Mvc.Mailer;
using ViewModels;

namespace Mailers
{
    public sealed class UserMailer : MailerBase, IUserMailer
    {
        private readonly IExpanditUserService _expanditUserService;
        private readonly IMailMessageRepository _mailMessageRepository;
        private readonly IMailPromotionModule _mailPromotionModule;
        private readonly MailMessageSettings _settings;

        public UserMailer(IExpanditUserService expanditUserService, IMailMessageSettingRepository mailMessageSettingRepository, IMailMessageRepository mailMessageRepository, IMailPromotionModule mailPromotionModule)
        {
            _expanditUserService = expanditUserService;
            _settings = mailMessageSettingRepository.All().FirstOrDefault() ?? new MailMessageSettings
                {
                    SendAsName = "Please Setup the correct sender name in the standard email settings!",
                    SendAsAddress = "Please_Setup_the_correct_sender_address_in_the_standard_email@settings.com"
                };
            MasterName = "_MailLayout";
            _mailMessageRepository = mailMessageRepository;
            _mailPromotionModule = mailPromotionModule;
        }

        public MvcMailMessage StaleUserCartReminder(CartViewModel viewModel, string toAddress)
        {
            ViewBag.CurrentModel = viewModel;
            var message = GetMessage("StaleUserCartReminder");
            var resources = GetResources(message);
            // Add Images to show inline here:
            _mailPromotionModule.SetImagesAsResources(viewModel.ProductItems, resources);

            ViewBag.Promotion = _mailPromotionModule.MailPromotedProducts(message, resources);

            string fromAddress = _settings.SendAsAddress;
            string displayName = _settings.SendAsName;

            var mail = GetMailMessage(fromAddress, toAddress);

            PopulateBody(mail, "StaleUserCartReminder", resources);

            mail.Subject = GetSubject(message, Resources.Language.LABEL_YOU_LEFT_SOMETHING_IN_THE_CART);

            AdditionalRecipients(mail, message);

            MailAddress sender =
                    new MailAddress(fromAddress, displayName);

            mail.Sender = sender;

            SetAttachments(mail, message);

            return mail;
        }

        public MvcMailMessage Welcome(string toAddress, string name, string login)
        {
            ViewBag.Name = name;
            ViewBag.Login = login;
            var message = GetMessage("Welcome");
            var resources = GetResources(message);

            ViewBag.Promotion = _mailPromotionModule.MailPromotedProducts(message, resources);

            string fromAddress = _settings.SendAsAddress;
            string displayName = _settings.SendAsName;

            var mail = GetMailMessage(fromAddress, toAddress);

            PopulateBody(mail, "Welcome", resources);

            mail.Subject = GetSubject(message, Resources.Language.LABEL_MAIL_SUBJECT_NEW_CUSTOMER_ACCOUNT);

            AdditionalRecipients(mail, message);

            MailAddress sender =
                    new MailAddress(fromAddress, displayName);

            mail.Sender = sender;

            SetAttachments(mail, message);

            return mail;
        }

        public MvcMailMessage WelcomeConfirmAccount(string toAddress, string name, string login, string uniqueToken)
        {
            ViewBag.Name = name;
            ViewBag.Login = login;
            ViewBag.Token = uniqueToken;
            var message = GetMessage("WelcomeConfirmAccount");
            var resources = GetResources(message);

            ViewBag.Promotion = _mailPromotionModule.MailPromotedProducts(message, resources);

            string fromAddress = _settings.SendAsAddress;
            string displayName = _settings.SendAsName;

            var mail = GetMailMessage(fromAddress, toAddress);

            PopulateBody(mail, "WelcomeConfirmAccount", resources);

            mail.Subject = GetSubject(message, Resources.Language.LABEL_MAIL_SUBJECT_NEW_CUSTOMER_ACCOUNT);

            AdditionalRecipients(mail, message);

            MailAddress sender =
                    new MailAddress(fromAddress, displayName);

            mail.Sender = sender;

            SetAttachments(mail, message);

            return mail;
        }

        public MvcMailMessage PasswordChange(string toAddress)
        {
            var message = GetMessage("PasswordChange");
            var resources = GetResources(message);

            ViewBag.Promotion = _mailPromotionModule.MailPromotedProducts(message, resources);

            string fromAddress = _settings.SendAsAddress;
            string displayName = _settings.SendAsName;

            var mail = GetMailMessage(fromAddress, toAddress);

            PopulateBody(mail, "PasswordChange", resources);

            mail.Subject = GetSubject(message, Resources.Language.LABEL_MAIL_SUBJECT_PASSWORD_CHANGED);

            AdditionalRecipients(mail, message);

            MailAddress sender =
                    new MailAddress(fromAddress, displayName);

            mail.Sender = sender;

            SetAttachments(mail, message);

            return mail;
        }

        public MvcMailMessage UserUpdate(string toAddress, string name, string login)
        {
            ViewBag.Name = name;
            ViewBag.Login = login;
            var message = GetMessage("UserUpdate");

            var resources = GetResources(message);

            ViewBag.Promotion = _mailPromotionModule.MailPromotedProducts(message, resources);

            string fromAddress = _settings.SendAsAddress;
            string displayName = _settings.SendAsName;

            var mail = GetMailMessage(fromAddress, toAddress);

            PopulateBody(mail, "UserUpdate", resources);

            mail.Subject = GetSubject(message, Resources.Language.LABEL_MAIL_SUBJECT_UPDATED_CUSTOMER_ACCOUNT);

            AdditionalRecipients(mail, message);

            MailAddress sender =
                    new MailAddress(fromAddress, displayName);

            mail.Sender = sender;

            SetAttachments(mail, message);

            return mail;
        }

        public MvcMailMessage OrderConfirmation(CartViewModel viewModel, string toAddress)
        {
            ViewBag.CurrentModel = viewModel;
            var message = GetMessage("OrderConfirmation");
            var resources = GetResources(message);

            // Add Images to show inline here:
            _mailPromotionModule.SetImagesAsResources(viewModel.ProductItems, resources);

            ViewBag.Promotion = _mailPromotionModule.MailPromotedProducts(message, resources);

            string fromAddress = _settings.SendAsAddress;
            string displayName = _settings.SendAsName;

            var mail = GetMailMessage(fromAddress, toAddress);
            PopulateBody(mail, "OrderConfirmation", resources);

            mail.Subject = GetSubject(message, Resources.Language.LABEL_EMAIL_ORDER_CONFIRMATION);

            AdditionalRecipients(mail, message);

            if (viewModel.Shipping != null)
            {
                var shipping = viewModel.Shipping;
                if (!string.IsNullOrWhiteSpace(shipping.EmailAddress))
                {
                    if (shipping.EmailAddress != toAddress)
                    {
                        AddRecipients(mail, new List<string>(){shipping.EmailAddress});            
                    }
                }
            }

            MailAddress sender =
                    new MailAddress(fromAddress, displayName);

            mail.Sender = sender;

            SetAttachments(mail, message);

            return mail;
        }

        public MvcMailMessage TipFriend(string sendersName, string toAddress, string fromAddress, ProductsViewModel model, string currentUrl)
        {
            ViewBag.CurrentUrl = currentUrl;
            ViewBag.SendersName = sendersName;
            ViewBag.CurrentModel = model.ProductItems[0];
            var message = GetMessage("TipFriend");
            var resources = GetResources(message);
            var pathToImage = HttpContext.Current.Server.MapPath(model.ProductItems[0].Picture2);
            resources.Add("productImage", pathToImage);

            ViewBag.Promotion = _mailPromotionModule.MailPromotedProducts(message, resources);

            var mail = GetMailMessage(fromAddress, toAddress); // On behalf of

            PopulateBody(mail, "TipFriend", resources);

            mail.Subject = GetSubject(message, Resources.Language.LABEL_EMAIL_TIP_SUBJECT);

            AdditionalRecipients(mail, message);

            string sendAs = _settings.SendAsAddress;
            string sendAsName = _settings.SendAsName;

            MailAddress sender =
                new MailAddress(sendAs, sendAsName);

            mail.Sender = sender;

            SetAttachments(mail, message);

            return mail;
        }

        public MvcMailMessage LostPassword(string toAddress, string contactName, string userName, string password)
        {
            ViewBag.ContactName = contactName;
            ViewBag.Password = password;
            ViewBag.UserName = userName;
            var message = GetMessage("LostPassword");
            var resources = GetResources(message);

            ViewBag.Promotion = _mailPromotionModule.MailPromotedProducts(message, resources);

            string fromAddress = _settings.SendAsAddress;
            string displayName = _settings.SendAsName;

            var mail = GetMailMessage(fromAddress, toAddress);

            PopulateBody(mail, "LostPassword", resources);
            mail.Subject = GetSubject(message, Resources.Language.LABEL_LOST_PASSWORD_SUBJECT);

            AdditionalRecipients(mail, message);

            MailAddress sender =
                    new MailAddress(fromAddress, displayName);

            mail.Sender = sender;

            SetAttachments(mail, message);

            return mail;
        }

        public MvcMailMessage ResetPassword(string toAddress, string contactName, string userName, string uniqueToken)
        {
            ViewBag.ContactName = contactName;
            ViewBag.Password = uniqueToken;
            ViewBag.UserName = userName;
            var message = GetMessage("ResetPassword");
            var resources = GetResources(message);

            ViewBag.Promotion = _mailPromotionModule.MailPromotedProducts(message, resources);

            string fromAddress = _settings.SendAsAddress;
            string displayName = _settings.SendAsName;

            var mail = GetMailMessage(fromAddress, toAddress);

            PopulateBody(mail, "ResetPassword", resources);
            mail.Subject = GetSubject(message, Resources.Language.LABEL_LOST_PASSWORD_SUBJECT);

            AdditionalRecipients(mail, message);

            MailAddress sender =
                    new MailAddress(fromAddress, displayName);

            mail.Sender = sender;

            SetAttachments(mail, message);

            return mail;
        }

        public MvcMailMessage WebContact(ContactUsViewModel model, string toAddress)
        {
            ViewBag.CurrentModel = model;
            var message = GetMessage("WebContact");
            var resources = GetResources(message);

            ViewBag.Promotion = _mailPromotionModule.MailPromotedProducts(message, resources);

            string fromAddress = _settings.SendAsAddress;
            string displayName = _settings.SendAsName;

            var mail = GetMailMessage(model.EmailAddress, toAddress);

            PopulateBody(mail, "WebContact", resources);

            mail.Subject = model.Subject;

            AdditionalRecipients(mail, message);

            MailAddress sender =
                    new MailAddress(fromAddress, displayName);

            mail.Sender = sender;

            SetAttachments(mail, message);

            return mail;
        }

        private ExpanditMailMessage GetMessage(string messageName)
        {
            var message = _mailMessageRepository.GetMessageByName(messageName, _expanditUserService.LanguageGuid, true);
            return message == null ? 
                _mailMessageRepository.CreateMailMessage(_expanditUserService.LanguageGuid, messageName) 
                : _mailMessageRepository.GetMessageByName(messageName, _expanditUserService.LanguageGuid, true);
        }

        private Dictionary<string, string> GetResources(ExpanditMailMessage message = null)
        {
            var resources = new Dictionary<string, string>();
            // Default value
            resources["logo"] = HttpContext.Current.Server.MapPath("~/Content/images/Expandit_logo_pos_RGB_S.png");

            // Add image from property here
            if (message != null)
            {
                var image = message.GetPropertyValue("PICTURE1");
                if (!string.IsNullOrWhiteSpace(image))
                {
                    resources["logo"] = HttpContext.Current.Server.MapPath("~/" + image);
                }
                var html = message.GetPropertyValue("HTMLDESC");

                if (!string.IsNullOrWhiteSpace(html))
                {
                    html = AppUtil.ReplaceHref(html, AppUtil.GetVirtualRoot());
                    html = EmbedImages(html, resources);
                }

                ViewBag.CustomHtml = html;
            }


            return resources;
        }

        private ExpanditMvcMailMessage GetMailMessage(string fromAddress, string toAddress)
        {
            SmtpClient client = GetSmtpClient();

            var mail = new ExpanditMvcMailMessage(fromAddress, toAddress, client);

            return mail;
        }

        private SmtpClient GetSmtpClient()
        {
            string host = _settings.Host;
            string sPort = _settings.Port;

            SmtpClient client = null;

            if (string.IsNullOrWhiteSpace(host) && string.IsNullOrWhiteSpace(sPort))
            {
                client = new SmtpClient();
            }
            else if (!string.IsNullOrWhiteSpace(host) && string.IsNullOrWhiteSpace(sPort))
            {
                client = new SmtpClient(host);
            }
            else if (!string.IsNullOrWhiteSpace(host) && !string.IsNullOrWhiteSpace(sPort))
            {
                int port = int.Parse(sPort);
                client = new SmtpClient(host, port);
            }

            NetworkCredential credential = null;
            if (!string.IsNullOrWhiteSpace(_settings.UserName) && !string.IsNullOrWhiteSpace(_settings.Password))
            {
                credential = new NetworkCredential(_settings.UserName, _settings.Password);
            }

            if (client != null)
            {
                client.EnableSsl = _settings.ForceSsl;
                if (credential != null)
                {
                    client.Credentials = credential;
                }
            }

            return client;
        }

        private string GetSubject(ExpanditMailMessage message, string defaultLabel)
        {
            string mailSubject = message.GetPropertyValue("MAILSUBJECT");
            return string.IsNullOrWhiteSpace(mailSubject) ? defaultLabel : mailSubject;
        }

        private void AdditionalRecipients(ExpanditMvcMailMessage mail, ExpanditMailMessage message)
        {
            // Set/add additional recipients here
            string[] additionalRecipients = GetAdditionalRecipientsFromConfiguration(message);
            var cc = additionalRecipients[0];
            var bcc = additionalRecipients[1];

            List<string> ccAddresses = AddRecipientsFromConfiguration(cc, new List<string>());
            List<string> bccAddresses = AddRecipientsFromConfiguration(bcc, new List<string>());

            AddRecipients(mail, null, ccAddresses, bccAddresses);
        }

        private string[] GetAdditionalRecipientsFromConfiguration(ExpanditMailMessage message)
        {
            var cc = message.GetPropertyValue("MAILCC");
            if (string.IsNullOrWhiteSpace(cc))
            {
                if (_settings != null)
                {
                    cc = _settings.CC;
                }
            }
            var bcc = message.GetPropertyValue("MAILBCC");
            if (string.IsNullOrWhiteSpace(bcc))
            {
                if (_settings != null)
                {
                    bcc = _settings.BCC;
                }
            }
            return new[] { cc, bcc };
        }

        private List<string> AddRecipientsFromConfiguration(string commaSeparatedString, List<string> addressList)
        {
            if (addressList == null)
            {
                addressList = new List<string>();
            }

            if (string.IsNullOrWhiteSpace(commaSeparatedString))
            {
                return addressList;
            }

            var bccs = commaSeparatedString.Split(new[] { ',' });

            if (bccs.Length <= 0)
            {
                return addressList;
            }

            addressList.AddRange(bccs);
            return addressList;
        }

        private void AddRecipients(MvcMailMessage mail, List<string> toAddresses, List<string> ccAddresses = null, List<string> bccAddresses = null)
        {
            if (toAddresses != null)
            {
                toAddresses.ForEach(x => mail.To.Add(x));
            }
            if (ccAddresses != null)
            {
                ccAddresses.ForEach(x => mail.CC.Add(x));
            }
            if (bccAddresses != null)
            {
                bccAddresses.ForEach(x => mail.Bcc.Add(x));
            }
        }

        private void SetAttachments(ExpanditMvcMailMessage mail, ExpanditMailMessage message)
        {
            var attachmentText = message.GetPropertyValue("ATTACHMENT");
            if (!string.IsNullOrWhiteSpace(attachmentText))
            {
                Attachment attachment = new Attachment(HttpContext.Current.Server.MapPath("~/" + attachmentText));
                mail.Attachments.Add(attachment);
            }
        }

        private string EmbedImages(string text, Dictionary<string,string> resources){
            var images = GetImages(text);
            foreach (var imagePair in images)
            {
                var replacement = string.Format("cid:{0}", imagePair.Value);
                var newText = text.Replace(imagePair.Key, replacement);
                text = newText.Replace("/cid:", "cid:");

                resources[imagePair.Value] = HttpContext.Current.Server.MapPath("~/" + imagePair.Key.Replace("../", ""));
            }
            return text;
        }

        private Dictionary<string, string> GetImages(string text){
            Dictionary<string,string> resources = new Dictionary<string, string>();
            
            const string pattern = @"<img.*?src=[\""'](.+?)[\""'].*?>";
            MatchCollection matches = Regex.Matches(text, pattern);

            foreach (Match match in matches)
            {
                var key = match.Groups[1].Value;
                
                if (!resources.ContainsKey(key))
                {
                    var id = Guid.NewGuid().ToString();
                    resources.Add(key, id);
                }
            }
            
            return resources;
        } 
    }
}