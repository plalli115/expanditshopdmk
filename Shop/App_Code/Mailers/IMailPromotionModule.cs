﻿using System.Collections.Generic;
using CmsPublic.ModelLayer.Models;
using ViewModels;

namespace Mailers
{
    public interface IMailPromotionModule
    {
        MailPromotionContainer MailPromotedProducts(ExpanditMailMessage message, Dictionary<string, string> resources);
        void SetImagesAsResources(List<CartViewModel.ProductItem> viewModel, Dictionary<string, string> resources);
    }
}