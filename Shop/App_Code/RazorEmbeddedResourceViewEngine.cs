﻿using System.Web.Mvc;

public class RazorEmbeddedResourceViewEngine : RazorViewEngine
{
	public RazorEmbeddedResourceViewEngine()
	{
	    MasterLocationFormats = new[]
	        {
                "~/Views/Shared/{0}.cshtml",
	            "~/Views/{1}/{0}.cshtml",
	            "~/Views/Cms2/{1}/{0}.cshtml",
                "~/EISCMS/Views/{0}.cshtml",
	        };
        

	    ViewLocationFormats = new[]
	        {
	            "~/Views/Shared/{0}.cshtml",
	            "~/Views/{1}/{0}.cshtml",
	            "~/Views/Cms2/{1}/{0}.cshtml",
                "~/EISCMS/Views/{1}/{0}.cshtml",
	        };
	    PartialViewLocationFormats = ViewLocationFormats;
	}
}