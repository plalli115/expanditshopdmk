﻿using System.Web.Mvc;
using UIHelpers;

namespace Controllers
{
    /// <summary>
    ///     Summary description for TranslationController
    /// </summary>
    public class TranslationController : Controller
    {
        [HttpPost]
        public ActionResult GetTranslations()
        {
            object translations = ResourceHelper.GetLanguageTranslations();

            return Json(translations);
        }
    }
}