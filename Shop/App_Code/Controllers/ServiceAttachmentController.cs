﻿using System.Collections.Generic;
using System.Web.Mvc;
using AutoMapper;
using EISCS.Shop.DO.BAS.Dto;
using EISCS.Shop.DO.BAS.Interface;
using FormModels.ServiceOrderAttachment;
using ViewModels.ServiceAttachment;

namespace Controllers
{
    public class ServiceAttachmentController : Controller
    {
        private readonly IServiceAttachmentRepository _serviceAttachmentRepository;
        private readonly IServiceAttachmentService _serviceAttachmentService;

        public ServiceAttachmentController(IServiceAttachmentService serviceAttachmentService,
                                           IServiceAttachmentRepository serviceAttachmentRepository)
        {
            _serviceAttachmentService = serviceAttachmentService;
            _serviceAttachmentRepository = serviceAttachmentRepository;
        }

        [HttpPost]
        public ActionResult Upload(ServiceAttachmentFormModel attachmentForm)
        {
            if (ModelState.IsValid)
            {
                bool success = _serviceAttachmentService.SaveAttachment(attachmentForm.File,
                                                                        attachmentForm.ServiceOrderGuid,
                                                                        attachmentForm.AttachmentComment);
                if (success)
                    return RedirectToAction("Details", "ServiceOrder", new { serviceOrderGuid = attachmentForm.ServiceOrderGuid });
            }

            ModelState.AddModelError(string.Empty, Resources.Language.SO_ERROR_UNKNOWNERROR);
            return RedirectToAction("Details", "ServiceOrder",
                                    new {id = attachmentForm.BASGuid, validationErrors = ModelState.Values});
        }

        public ActionResult Delete(int uniqueGuid, int serviceOrderBasGuid, string serviceOrderGuid)
        {
            bool succes = _serviceAttachmentRepository.Remove(uniqueGuid);
            var serviceOrderAttachements = Mapper.Map<IEnumerable<ServiceAttachmentListViewModel>>(_serviceAttachmentRepository.GetAllByServiceOrderGuid(serviceOrderGuid));

            var serviceAttachmentAttachmentViewModel = new ServiceAttachments
                {
                    ServiceOrderGuid = serviceOrderGuid,
                    ServiceOrderBasGuid = serviceOrderBasGuid,
                    DeleteSuccess = succes,
                    AttachmentList = serviceOrderAttachements
                };

            return PartialView("_Attachments", serviceAttachmentAttachmentViewModel);
        }

        public FileContentResult GetFile(string attachmentGuid)
        {
            ServiceAttachmentItem item = _serviceAttachmentRepository.GetById(attachmentGuid);

            return File(item.FileData, item.FileExtension, item.FileName);
        }
    }
}