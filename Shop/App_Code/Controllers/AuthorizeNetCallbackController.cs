﻿using System.Web.Mvc;
using EISCS.ExpandITFramework.Util;
using EISCS.Wrappers.Configuration;
using ExpandIT;

namespace Controllers
{
    /// <summary>
    /// Summary description for AuthorizeNetCallbackController
    /// </summary>
    public class AuthorizeNetCallbackController : Controller
    {
        private readonly IConfigurationObject _configuration;

        public AuthorizeNetCallbackController(IConfigurationObject configuration)
        {
            _configuration = configuration;
        }

        public ActionResult AuthNetCallback()
        {
            if (!AuthorizeNetValidateHashValue())
            {
                AuthorizeNetRedirect(AppUtil.ShopFullPath() + "/Checkout/Error?PaymentType=AUTHNET");
                return RedirectToAction("Error", "Checkout");
            }
            AuthorizeNetRedirect(AppUtil.ShopFullPath() + "/Checkout/Accepted?PaymentType=AUTHNET");
            return RedirectToAction("Accepted", "Checkout");
        }

        public bool AuthorizeNetValidateHashValue()
        {
            string md5HashValue = _configuration.Read("AUTHORIZENET_MD5_HASH_VALUE");
            string loginId = _configuration.Read("AUTHORIZENET_LOGIN");
            string transId = Request["x_trans_id"];
            string amount = Request["x_amount"];
            string xMd5Hash = Request["x_MD5_Hash"];

            string xMd5HashCalc = Crypto.coreMD5(md5HashValue + loginId + transId + amount).ToUpper();

            return xMd5Hash == xMd5HashCalc;
        }

        public void AuthorizeNetRedirect(string aUrl)
        {
            Response.Clear();

            Response.Write("<HTML>");
            Response.Write("<HEAD>");
            Response.Write("<META http-equiv=REFRESH Content=\"0; url=" + aUrl + "\">");
            Response.Write("</HEAD>");
            Response.Write("<BODY>");
            Response.Write("</BODY>");
            Response.Write("</HTML>");

            Response.End();
        }
    }
}