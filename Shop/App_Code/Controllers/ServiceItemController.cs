﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using EISCS.Shop.DO.BAS.Dto;
using EISCS.Shop.DO.BAS.Repository;
using ViewModels.ServiceItem;

namespace Controllers
{
    public class ServiceItemController : Controller
    {
        private const int MAX_SEARCH_RESULTS = 10;
        private readonly IServiceItemRepository _serviceItemRepository;

        public ServiceItemController(IServiceItemRepository serviceItemRepository)
        {
            _serviceItemRepository = serviceItemRepository;
        }

        public JsonResult AjaxFindByCustomer(string searchQuery, string customerId, string customerAddressId = null)
        {
            IEnumerable<ServiceItem> serviceItems = _serviceItemRepository.AjaxFindServiceItemByCustomer(searchQuery, customerId, customerAddressId).Take(MAX_SEARCH_RESULTS);
            var serviceItemList = Mapper.Map<IEnumerable<ServiceItemViewModel>>(serviceItems);

            return Json(serviceItemList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult AjaxSearchAll(string searchQuery)
        {
            IEnumerable<ServiceItem> serviceItems = _serviceItemRepository.AjaxFindServiceItem(searchQuery).Take(MAX_SEARCH_RESULTS);
            var serviceItemList = Mapper.Map<IEnumerable<ServiceItemViewModel>>(serviceItems);

            return Json(serviceItemList, JsonRequestBehavior.AllowGet);
        }
    }
}