﻿using System;
using System.IO;
using System.Web.Mvc;
using CmsPublic.DataProviders.Logging;
using EISCS.CMS.Services.AppSettings;
using EISCS.ExpandITFramework.Util;
using Resources;

namespace Controllers.Cms2
{
    /// <summary>
    /// Summary description for FileUploadController
    /// </summary>
    public class FileUploadController : Controller
    {
        private readonly IHttpRuntimeWrapper _httpRuntimeWrapper;
        private readonly string _imagesRelativePath;

        public FileUploadController(IHttpRuntimeWrapper httpRuntimeWrapper)
        {
            _httpRuntimeWrapper = httpRuntimeWrapper;
            string uploadBaseFolder = string.Format("/{0}", AppSettingsReader.Read("UPLOAD_CONTENT_BASE_FOLDER"));
            _imagesRelativePath = string.Format("{0}/{1}/", uploadBaseFolder, "images"); // Standard: "/catalog/images/"
        }

        private string StorageRoot
        {
            get { return Path.Combine(Server.MapPath("~/Files/")); }
        }

        private string VirtualRoot()
        {
            return _httpRuntimeWrapper.AppDomainAppVirtualPath.Equals("/")
                ? "/cms"
                : _httpRuntimeWrapper.AppDomainAppVirtualPath + "/cms";
        }

        public string UploadFolderRelativePath()
        {
            return _imagesRelativePath;
        }

        //
        // GET: /FileUpload/

        public ActionResult Index()
        {
            return View();
        }

        public string UploadFile()
        {
            var file = Request.Form["value"];
            var name = Request.Form["name"];

            string uploadFolder = Request["upload_folder"];
            if (string.IsNullOrEmpty(uploadFolder))
            {
                uploadFolder = _imagesRelativePath;
            }

            var uploadPath = Path.Combine(Server.MapPath(string.Format("~{0}", uploadFolder)));

            var data = file.Split(',');
            var encodedData = data[1].Replace(' ', '+');

            var codedData = Convert.FromBase64String(encodedData);

            var responseText = "success";

            try
            {
                System.IO.File.WriteAllBytes(uploadPath + name, codedData);
            }
            catch (DirectoryNotFoundException dnfex)
            {
                string q = dnfex.Message;
                FileLogger.Log(q);
                responseText = "Directory does not exist. Failed"; // CmsLanguage.CATMAN_ERROR_DIRECTORY_DOES_NOT_EXIST;
            }
            catch (UnauthorizedAccessException ex)
            {
                string q = ex.Message;
                FileLogger.Log(q);
                responseText = CmsLanguage.CATMAN_LABEL_MISSING_WRITE_PERMISSION.Replace("\\", "");
            }
            catch (Exception ex)
            {
                string q = ex.Message;
                FileLogger.Log(q + "\r\n" + ex.StackTrace);
                responseText = "An error occurred while writing file to disk.";
            }

            //var serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            //var result = new { name = name, message = responseText };
            //return serializer.Serialize(result);

            return name + ":" + responseText;

        }

    }
}