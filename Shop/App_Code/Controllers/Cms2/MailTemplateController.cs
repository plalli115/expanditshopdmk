﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CmsPublic.DataProviders.Logging;
using CmsPublic.Exceptions;
using CmsPublic.ModelLayer.Models;
using EISCS.CMS.Models.ViewModels;
using EISCS.ExpandITFramework.Util;
using EISCS.Shop.DO.Dto;
using EISCS.Shop.DO.Interface;
using EISCS.Shop.DO.Repository;
using Filters;
using FormModels.Cms2.FormModels;
using Resources;
using WebServices;

namespace Controllers.Cms2
{
    /// <summary>
    /// Summary description for MailTemplateController
    /// </summary>
    public class MailTemplateController : CmsBaseController
    {
        private readonly HttpContextBase _httpContext;
        private readonly IMailMessageSettingRepository _mailMessageSettingRepository;
        private readonly IMailMessageRepository _mailMessageRepository;
        private readonly IPropertiesDataService _propertiesDataService;
        private readonly IProductRepository _productRepository;

        public MailTemplateController(HttpContextBase httpContext, IHttpRuntimeWrapper httpRuntimeWrapper, IMailMessageSettingRepository mailMessageSettingRepository, IMailMessageRepository mailMessageRepository, IPropertiesDataService propertiesDataService, IProductRepository productRepository)
            : base(httpRuntimeWrapper)
        {
            _httpContext = httpContext;
            _mailMessageSettingRepository = mailMessageSettingRepository;
            _mailMessageRepository = mailMessageRepository;
            _propertiesDataService = propertiesDataService;
            _productRepository = productRepository;
        }

        public ActionResult Index()
        {
            var language = (string)Session["CurrentLanguage"];
            var mailMessages = _mailMessageRepository.GetAll(language, true);
            var model = new MailViewModel(new ExpanditMailMessage(), mailMessages, null);
            return View(model);
        }

        public ActionResult EditMailMessage(int id = 0)
        {
            var language = (string)Session["CurrentLanguage"];
            ExpanditMailMessage mailMessage;
            try
            {
                mailMessage = _mailMessageRepository.GetMessageForEditor(id, language);
            }
            catch (MailProviderException ex)
            {
                mailMessage = new ExpanditMailMessage {PropCollection = new List<PropValPropTransQuerySet>(), ErrorMessage = ex.Message};
            }

            // Group together ...
            mailMessage.PropertyGroup = new List<PropValPropTransGroup>();
            var temp2 = mailMessage.PropCollection.ToList();
            var propValPropTransGroup = new PropValPropTransGroup { PropertyGroups = temp2, Propheader = "Mail" };
            mailMessage.PropertyGroup.Add(propValPropTransGroup);
            //

            var products = _mailMessageRepository.GetMailProducts(id, language);

            var model = new MailViewModel(mailMessage, null, products);
            return View(model);
        }

        [HttpPost, ValidateInput(false)]
        public string EditMailMessage(FormCollection form, int messageGuid = 0)
        {
            var languageGuid = (string)Session["CurrentLanguage"];

            if (messageGuid != 0)
            {
                try
                {
                    var product = _mailMessageRepository.GetMessage(messageGuid, languageGuid, true);
                    var propVal = product.Properties;

                    if (propVal.PropDict != null)
                    {
                        foreach (string s in form.Keys)
                        {
                            if (propVal.PropDict.ContainsKey(s))
                            {
                                var v = form[s];
                                propVal.PropDict[s] = v;
                            }
                        }
                    }

                    _propertiesDataService.SaveProperties(product.IProperties, languageGuid);

                    TempData["ModelState"] = true;
                }
                catch (Exception ex)
                {
                    FileLogger.Log(string.Format("Message {0},\nTrace {1}", ex.Message, ex.StackTrace));
                    return CmsLanguage.CATMAN_LABEL_AN_ERROR_OCCURRED;
                }
            }
            else
            {
                TempData["ModelState"] = false;
            }
            return CmsLanguage.CATMAN_LABEL_ALLSAVED;
        }

        public ActionResult MailTreeView()
        {
            var language = (string)Session["CurrentLanguage"];
            var mails = _mailMessageRepository.GetAll(language, true);
            var treeModels = new List<MailTemplateTreeModel>();
            foreach (var expanditMailMessage in mails)
            {
                var treeModel = new MailTemplateTreeModel
                    {
                        Name = string.IsNullOrWhiteSpace(expanditMailMessage.GetPropertyValue("NAME")) ? expanditMailMessage.MailMessageName : expanditMailMessage.GetPropertyValue("NAME"),
                        Id = expanditMailMessage.MailMessageGuid,
                        DateCreated = expanditMailMessage.DateCreated,
                        Active = expanditMailMessage.Active
                    };
                treeModels.Add(treeModel);
            }

            return PartialView("Partials/_MailTree", treeModels);
        }

        [ImportModelStateTempDataFromTempData]
        public ActionResult MailMessageSettings()
        {
            var mailMessageSettings = _mailMessageSettingRepository.All().FirstOrDefault();
            var serverSettings = AutoMapper.Mapper.Map<MailServerSettingsFormModel>(mailMessageSettings);
            var general = AutoMapper.Mapper.Map<MailSettingsGeneralFormModel>(mailMessageSettings);
            var stale = AutoMapper.Mapper.Map<MailSettingsStaleCartFormModel>(mailMessageSettings);
            var indexViewModel = new IndexViewModel {MailServerSettingsFormModel = serverSettings, GeneralMailSettingsFormModel = general, StaleCartMailSettingsFormModel = stale};
            return View(indexViewModel);
        }

        [ExportModelStateTempDataToTempData]
        public ActionResult SaveMailServerSettings(IndexViewModel model)
        {
            if (ModelState.IsValid)
            {
                var settings = AutoMapper.Mapper.Map<MailServerSettings>(model.MailServerSettingsFormModel);
                _mailMessageSettingRepository.UpdateOrInsert(settings);
            }

            return RedirectToAction("MailMessageSettings");
        }

        [ExportModelStateTempDataToTempData]
        public ActionResult SaveMailMessageSettings(IndexViewModel model)
        {
            if (ModelState.IsValid)
            {
                var settings = AutoMapper.Mapper.Map<GeneralMailMessageSettings>(model.GeneralMailSettingsFormModel);
                _mailMessageSettingRepository.UpdateOrInsert(settings);
            }

            return RedirectToAction("MailMessageSettings");
        }

        [ExportModelStateTempDataToTempData]
        public ActionResult SaveStaleMailMessageSettings(IndexViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return RedirectToAction("MailMessageSettings");
            }

            var settings = AutoMapper.Mapper.Map<StaleCartMailMessageSettings>(model.StaleCartMailSettingsFormModel);
            int updated = _mailMessageSettingRepository.UpdateOrInsert(settings);

            if (updated <= 0)
            {
                return RedirectToAction("MailMessageSettings");
            }

            if (settings.SendReminderEmail == model.StaleCartMailSettingsFormModel.SendReminderEmailCurrentSetting)
            {
                return RedirectToAction("MailMessageSettings");
            }

            if (settings.SendReminderEmail)
            {
                MailSendHttpClient.ActivateTask(settings);
            }
            else
            {
                MailSendHttpClient.DeactivateTask();
            }

            return RedirectToAction("MailMessageSettings");
        }

        public ActionResult AllProducts()
        {
            AllProductsViewModel model = new AllProductsViewModel { PreviousUrl = _httpContext.Request.UrlReferrer };
            return View(model);
        }

        [HttpPost]
        public ActionResult SetPromotionProducts(FormCollection collection, AllProductsViewModel model)
        {
            var additionalItems = collection.AllKeys
       .Where(k => collection[k].Contains("on"))
               .Select(k => k.ToString());

            var enumerable = additionalItems as string[] ?? additionalItems.ToArray();
            if (enumerable.Any())
            {
                FileLogger.Log(string.Join(",", enumerable));
                var idPart = model.PreviousUrl.Segments.Last();
                _mailMessageRepository.SetMailProducts(idPart, enumerable);
            }

            return Redirect(model.PreviousUrl.ToString());
        }

        public ActionResult MailPromotionProducts()
        {
            return View();
        }

        public JsonResult GetMailPromotionProducts(JQueryDataTableParamModel param)
        {
            var language = (string)Session["CurrentLanguage"];

            ICollection<string> productGuids = _mailMessageRepository.GetMailProducts(param.groupGuid).ToList();
            var products = _productRepository.GetProducts(productGuids, language);
            products = SetMissingPropertiesForDataTables(products);

            return Json(new
            {
                param.draw,
                data = products,
                recordsTotal = products.Count,
                recordsFiltered = products.Count
            });
        }

        public void RemoveProducts(int id, string collection)
        {
            string[] guids = collection.Split(',').Select(x => x.ToString()).ToArray();
            _mailMessageRepository.RemoveMailProducts(id, guids);
        }

    }

    public class MailTemplateTreeModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime DateCreated { get; set; }
        public bool Active { get; set; }
    }
}