﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Caching;
using System.Web.Hosting;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;
using AutoMapper;
using CmsPublic.DataProviders.Logging;
using CmsPublic.ModelLayer.Models;
using EISCS.Catalog;
using EISCS.Catalog.Logic;
using EISCS.CMS.ControllerCommon;
using EISCS.CMS.CentralManagement.Configuration.Util;
using EISCS.CMS.Models.Logic;
using EISCS.CMS.Services.AppSettings;
using EISCS.CMS.Services.ImageService;
using EISCS.CMS.Services.PagingService;
using EISCS.ExpandITFramework.Util;
using EISCS.Routing.Providers;
using EISCS.Shop.Attributes;
using EISCS.Shop.DO.Interface;
using FileIO;
using Resources;
using EISCS.CMS.Models.ViewModels;
using EISCS.Shop.DO.Dto;
using UIHelpers;

namespace Controllers.Cms2
{
    [BrowserCache(PreventBrowserCaching = true)]
    [BusinessCenterUserAccess(AccessClass = "EditCatalog")]
    public class CatalogController : CmsBaseController
    {
        private readonly IProductRepository _productRepository;
        private readonly GroupProductManager _groupProductManager = new GroupProductManager(null, null, null);
        private readonly IExpanditUserService _expanditUserService;
        private readonly HttpContextBase _httpContext;
        private readonly SelectListStore _selectListStore = new SelectListStore();
        private readonly string _defaultGroupTemplate = AppSettingsReader.Read("DEFAULT_GROUP_TEMPLATE");
        private readonly ILanguageRepository _languageRepository = DependencyResolver.Current.GetService<ILanguageRepository>();
        private readonly IGroupProductRepository _groupProductRepository = DependencyResolver.Current.GetService<IGroupProductRepository>();
        private readonly IFileStore _fileStore = new DiskFileStore();
        private readonly string _imagesRelativePath;
        private readonly string _uploadBaseFolder;

        // Determine if logging is used
        private bool _isLoggingEnabled;

        public CatalogController(IProductRepository productRepository, IExpanditUserService expanditUserService, HttpContextBase httpContext, IHttpRuntimeWrapper httpRuntimeWrapper)
            : base(httpRuntimeWrapper)
        {
            _productRepository = productRepository;
            _expanditUserService = expanditUserService;
            _httpContext = httpContext;
            _uploadBaseFolder = string.Format("/{0}", AppSettingsReader.Read("UPLOAD_CONTENT_BASE_FOLDER")); // Standard: "/catalog"
            _imagesRelativePath = string.Format("{0}/{1}", _uploadBaseFolder, "images"); // Standard: "/catalog/images"
        }

        // Properties language
        ///<summary>
        ///</summary>
        ///<param name="languages"></param>
        [AcceptVerbs(HttpVerbs.Post)]
        public void Lang(string languages)
        {
            Session["CurrentLanguage"] = languages;
        }

        ///<summary>
        /// Get name property for currently displayed groups when language changes
        ///</summary>
        ///<param name="jsArray"></param>
        ///<returns></returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult ChangeLanguage(string jsArray)
        {
            string[] commaSeparated = jsArray.Split(new[] { ',' });
            var groupGuids = new List<int>();
            for (int i = 0; i < commaSeparated.Length; i++)
            {
                int groupGuid;
                int.TryParse(commaSeparated[i], out groupGuid);
                groupGuids.Add(groupGuid);
            }

            if (Session["Languages"] == null)
            {
                LoadLanguageSessionData();
            }

            var languageGuid = (string)Session["CurrentLanguage"];

            List<Group> groupList = _groupProductManager.GetGroupsFromGroupGuids(groupGuids, languageGuid, true);

            var groupNames = new List<GroupName>();
            for (int i = 0; i < groupList.Count; i++)
            {
                string propertyName = CommonUtility.DictSafeString(groupList[i].Properties.PropDict, "NAME", languageGuid, _isLoggingEnabled);
                var gn = new GroupName { GroupGuid = "id_" + groupList[i].GroupGuid, NAME = propertyName };
                groupNames.Add(gn);
            }
            return Json(groupNames);
        }

        public ActionResult AllProducts()
        {
            ViewBag.CurrentLanguageGuid = Session["CurrentLanguage"];
            AllProductsViewModel model = new AllProductsViewModel { PreviousUrl = null };
            return View(model);
        }

        public JsonResult GetProducts(JQueryDataTableParamModel param)
        {
            var language = (string)Session["CurrentLanguage"];

            param = SetDataTableParamsFromRequest(param);

            var products = _productRepository.GetProductRange(param.start, param.start + param.length, true, param.searchValue, language, param.columnBeingSort, param.sortDir);
            products = SetMissingPropertiesForDataTables(products);

            return Json(new
            {
                param.draw,
                recordsTotal = _productRepository.GetTotalRecords(),
                recordsFiltered = _productRepository.GetTotalRecordsFiltered(param.searchValue),
                data = products
            });
        }

        public JsonResult GetGroupProducts(JQueryDataTableParamModel param)
        {
            var language = (string)Session["CurrentLanguage"];

            param = SetDataTableParamsFromRequest(param);

            var products = _productRepository.GetGroupProductsRange(param.start, param.start + param.length, true, language, param.searchValue, param.groupGuid, param.columnBeingSort, param.sortDir);
            products = SetMissingPropertiesForDataTables(products);

            return Json(new
            {
                param.draw,
                data = products,
                recordsTotal = _groupProductManager.GetGroupProductsCount(param.groupGuid),
                recordsFiltered = _groupProductManager.GetGroupProductsCount(param.groupGuid)
            });
        }

        //[HttpPost, ValidateInput(false)]
        public ActionResult _GroupProducts(int id)
        {
            var language = (string)Session["CurrentLanguage"];

            List<GroupProduct> gp = _groupProductManager.GroupProducts(id.ToString(), language, true);
            gp.Sort((gp1, gp2) => gp1.SortIndex.CompareTo(gp2.SortIndex));

            Group g = _groupProductManager.GetGroup(id, language);
            var viewData =
                new CmsViewModel(null,
                                 g,
                                 null,
                                 gp,
                                 null,
                                 null,
                                 null);

            return View("Partials/_GroupProducts", viewData);

        }

        public ActionResult _ProductEdit(string productGuid)
        {
            var languageGuid = (string)Session["CurrentLanguage"];
            var id = _groupProductManager.EnsureParentGroup(languageGuid);
            var prod = !string.IsNullOrEmpty(productGuid)
                               ? _groupProductManager.SingleProductAnonymousCollection(productGuid, languageGuid)
                               : new Product();

            var viewData =
                new CmsViewModel(null,
                                 _groupProductManager.GetGroup(id, languageGuid),
                                 null,
                                 null,
                                 null,
                                 prod,
                                 null);

            var currentTemplate = prod.Properties.PropDict["TEMPLATE"];
            ViewData["TemplateList"] = _selectListStore.CreateTemplateSelect("IsProductTemplate", currentTemplate, _groupProductManager);
            prod.PropertyGroup = new List<PropValPropTransGroup>();
            var someData = _groupProductManager.GetPropertyGuidsIncludedInTemplate(currentTemplate);
            if (someData.Count > 0)
            {
                var temp = new List<PropValPropTransQuerySet>();
                foreach (PropValPropTransQuerySet propValPropTransQuerySet in prod.PropCollection)
                {
                    if (propValPropTransQuerySet.PropGuid == "")
                    {
                    }
                    if (someData.Contains(propValPropTransQuerySet.PropGuid))
                    {
                        temp.Add(propValPropTransQuerySet);
                    }
                }

                if (temp.Count > 0)
                {
                    // Only if Properties are defined in the template
                    prod.PropCollection = temp;
                }

                List<TemplateProperty> templateProperties = _groupProductManager.GetTemplateProperties(currentTemplate);

                if (templateProperties.Count > 0)
                {
                    IEnumerable<string> ids = templateProperties.Select(x => x.PropGroupGuid).Distinct();

                    foreach (string pId in ids)
                    {
                        string localId = pId;
                        List<TemplateProperty> tProps =
                            templateProperties.Where(x => x.PropGroupGuid == localId).ToList();
                        var temp2 = new List<PropValPropTransQuerySet>();
                        string propHeader = "Default";
                        foreach (PropValPropTransQuerySet propValPropTransQuerySet in prod.PropCollection)
                        {
                            PropValPropTransQuerySet set = propValPropTransQuerySet;
                            if (tProps.Exists(x => x.PropGuid == set.PropGuid))
                            {
                                temp2.Add(propValPropTransQuerySet);
                                PropValPropTransQuerySet querySet = propValPropTransQuerySet;
                                propHeader = (tProps.Where(x => x.PropGuid == querySet.PropGuid).ToList())[0].PropHeader;
                            }
                        }
                        var propValPropTransGroup = new PropValPropTransGroup { PropertyGroups = temp2, Propheader = propHeader };
                        prod.PropertyGroup.Add(propValPropTransGroup);
                    }
                }
            }
            else
            {
                // No defined property groups in the Template
                try
                {
                    var temp2 = prod.PropCollection.ToList();
                    var propValPropTransGroup = new PropValPropTransGroup { PropertyGroups = temp2, Propheader = "" };
                    prod.PropertyGroup.Add(propValPropTransGroup);
                }
                catch (Exception ex)
                {
                    FileLogger.Log(ex.Message + " " + ex.StackTrace);
                }
            }

            if (TempData.ContainsKey("ModelState"))
            {
                var tempData = (bool)TempData["ModelState"];
                if (!tempData)
                {
                    ModelState.AddModelError("Error", CmsLanguage.CATMAN_LABEL_AN_ERROR_OCCURRED);
                }
            }

            return View("Partials/_ProductEdit", viewData);
        }

        public ActionResult Edit(string productGuid)
        {
            var languageGuid = (string)Session["CurrentLanguage"];
            var id = _groupProductManager.EnsureParentGroup(languageGuid);
            var prod = !string.IsNullOrEmpty(productGuid)
                               ? _groupProductManager.SingleProductAnonymousCollection(productGuid, languageGuid)
                               : new Product();

            var viewData =
                new CmsViewModel(new CatalogTreeUtility(null).Top2LevelGroups(languageGuid),
                                 _groupProductManager.GetGroup(id, languageGuid),
                                 null,
                                 null,
                                 null,
                                 prod,
                                 null);

            var currentTemplate = prod.Properties.PropDict["TEMPLATE"];
            var templateSelect = _selectListStore.CreateTemplateSelect("IsProductTemplate", currentTemplate, _groupProductManager);

            // Fix template if it is incorrect
            ViewData["TemplateList"] = templateSelect;
            currentTemplate = (string)templateSelect.SelectedValue;
            prod.Properties.PropDict["TEMPLATE"] = currentTemplate;
            foreach (var property in prod.PropCollection.Where(property => property.PropGuid == "TEMPLATE"))
            {
                property.PropTransText = currentTemplate;
            }
            _groupProductManager.SaveProperties(prod, languageGuid);

            prod.PropertyGroup = new List<PropValPropTransGroup>();
            var someData = _groupProductManager.GetPropertyGuidsIncludedInTemplate(currentTemplate);
            if (someData.Count > 0)
            {
                var temp = new List<PropValPropTransQuerySet>();
                foreach (PropValPropTransQuerySet propValPropTransQuerySet in prod.PropCollection)
                {
                    if (propValPropTransQuerySet.PropGuid == "")
                    {
                    }
                    if (someData.Contains(propValPropTransQuerySet.PropGuid))
                    {
                        temp.Add(propValPropTransQuerySet);
                    }
                }

                if (temp.Count > 0)
                {
                    // Only if Properties are defined in the template
                    prod.PropCollection = temp;
                }

                List<TemplateProperty> templateProperties = _groupProductManager.GetTemplateProperties(currentTemplate);

                if (templateProperties.Count > 0)
                {
                    IEnumerable<string> ids = templateProperties.Select(x => x.PropGroupGuid).Distinct();

                    foreach (string pId in ids)
                    {
                        string localId = pId;
                        List<TemplateProperty> tProps =
                            templateProperties.Where(x => x.PropGroupGuid == localId).ToList();
                        var temp2 = new List<PropValPropTransQuerySet>();
                        string propHeader = "Default";
                        foreach (PropValPropTransQuerySet propValPropTransQuerySet in prod.PropCollection)
                        {
                            PropValPropTransQuerySet set = propValPropTransQuerySet;
                            if (tProps.Exists(x => x.PropGuid == set.PropGuid))
                            {
                                temp2.Add(propValPropTransQuerySet);
                                PropValPropTransQuerySet querySet = propValPropTransQuerySet;
                                propHeader = (tProps.Where(x => x.PropGuid == querySet.PropGuid).ToList())[0].PropHeader;
                            }
                        }
                        var propValPropTransGroup = new PropValPropTransGroup { PropertyGroups = temp2, Propheader = propHeader };
                        prod.PropertyGroup.Add(propValPropTransGroup);
                    }
                }
            }
            else
            {
                // No defined property groups in the Template
                try
                {
                    var temp2 = prod.PropCollection.ToList();
                    var propValPropTransGroup = new PropValPropTransGroup { PropertyGroups = temp2, Propheader = "" };
                    prod.PropertyGroup.Add(propValPropTransGroup);
                }
                catch (Exception ex)
                {
                    FileLogger.Log(ex.Message + " " + ex.StackTrace);
                }
            }

            if (TempData.ContainsKey("ModelState"))
            {
                var tempData = (bool)TempData["ModelState"];
                if (!tempData)
                {
                    ModelState.AddModelError("Error", CmsLanguage.CATMAN_LABEL_AN_ERROR_OCCURRED);
                }
            }

            return View(viewData);
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult Edit(string productGuid, FormCollection form)
        {
            var languageGuid = (string)Session["CurrentLanguage"];

            if (!productGuid.IsNullOrEmpty())
            {
                try
                {
                    var product = _groupProductManager.SingleProduct(productGuid, languageGuid);
                    var propVal = product.Properties;

                    if (propVal.PropDict != null)
                    {
                        foreach (string s in form.Keys)
                        {
                            if (propVal.PropDict.ContainsKey(s))
                            {
                                var v = form[s];
                                propVal.PropDict[s] = v;
                            }
                        }
                    }

                    _groupProductManager.SaveProperties(product, languageGuid);
                    TempData["ModelState"] = true;
                }
                catch (Exception ex)
                {
                    FileLogger.Log(string.Format("Message {0},\nTrace {1}", ex.Message, ex.StackTrace));
                }
            }
            else
            {
                TempData["ModelState"] = false;
            }
            return RedirectToAction("Edit", new { productGuid });
        }

        public ActionResult _GroupEdit(int id)
        {
            var languageGuid = (string)Session["CurrentLanguage"];
            Group g = CommonGroupEditAction(id, languageGuid);

            string currentTemplate = g.Properties.PropDict["TEMPLATE"];

            if (string.IsNullOrEmpty(currentTemplate))
            {
                currentTemplate = _defaultGroupTemplate;
            }

            ViewData["TemplateList"] = _selectListStore.CreateTemplateSelect("IsGroupTemplate", currentTemplate, _groupProductManager);

            ViewData["UrlsList"] = _selectListStore.CreateUrlSelect(languageGuid, _groupProductManager);

            ViewData["TargetList"] = _selectListStore.CreateTargetSelect("_blank");

            if (TempData.ContainsKey("ModelState"))
            {
                var tempData = (bool)TempData["ModelState"];
                if (!tempData)
                {
                    ModelState.AddModelError("Error", CmsLanguage.CATMAN_LABEL_AN_ERROR_OCCURRED);
                }
            }

            var viewData =
                new CmsViewModel(null,
                                 g,
                                 null,
                                 null,
                                 null,
                                 null,
                                 null);

            return View("Partials/_GroupEdit", viewData);
        }

        [HttpGet]
        public JsonResult LoadUrls(string searchTerm, int page, int pageSize)
        {
            var languageGuid = (string)Session["CurrentLanguage"];
            var realUrls = _groupProductManager.GetInternalLinkUrls(languageGuid).Where(x => x.IndexOf(searchTerm, StringComparison.OrdinalIgnoreCase) != -1);

            var startIndex = page >= 1 ? (page - 1) * pageSize : 0;

            var urls = realUrls.Skip(startIndex).Take(pageSize);

            bool isMore = (page * pageSize) < realUrls.Count();

            var list = (from x in urls
                        select new { id = x, text = x });
            var urlResult = new UrlResult { results = list, pagination = new { more = isMore } };

            return Json(urlResult, JsonRequestBehavior.AllowGet);
        }

        public class UrlResult
        {
            public object results { get; set; }
            public object pagination { get; set; }
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult _TabbedGroupEdit(int id)
        {
            var languageGuid = (string)Session["CurrentLanguage"];
            Group g = CommonGroupEditAction(id, languageGuid);

            string currentTemplate = g.Properties.PropDict["TEMPLATE"];

            if (string.IsNullOrEmpty(currentTemplate))
            {
                currentTemplate = _defaultGroupTemplate;
            }

            ViewData["TemplateList"] = _selectListStore.CreateTemplateSelect("IsGroupTemplate", currentTemplate, _groupProductManager);

            ViewData["UrlsList"] = _selectListStore.CreateUrlSelect(languageGuid, _groupProductManager, 0, 1);

            ViewData["TargetList"] = _selectListStore.CreateTargetSelect("_blank");

            if (TempData.ContainsKey("ModelState"))
            {
                var tempData = (bool)TempData["ModelState"];
                if (!tempData)
                {
                    ModelState.AddModelError("Error", CmsLanguage.CATMAN_LABEL_AN_ERROR_OCCURRED);
                }
            }

            var viewData =
                new CmsViewModel(null,
                                 g,
                                 null,
                                 null,
                                 null,
                                 null,
                                 null);

            return View("Partials/_TabbedGroupEdit", viewData);
        }

        private Group CommonGroupEditAction(int id, string languageGuid)
        {
            Group g = _groupProductManager.GetGroupForEditor(id, languageGuid);

            string currentTemplate = g.Properties.PropDict["TEMPLATE"];

            if (string.IsNullOrEmpty(currentTemplate))
            {
                currentTemplate = _defaultGroupTemplate;
            }

            // GROUP PROPERTIES TOGETHER
            g.PropertyGroup = new List<PropValPropTransGroup>();

            // REMOVE UNUSED PROPERTIES FROM LIST IF NOT IN TEMPLATE
            List<string> someData = _groupProductManager.GetPropertyGuidsIncludedInTemplate(currentTemplate);

            if (someData.Count > 0)
            {
                var temp = new List<PropValPropTransQuerySet>();
                foreach (PropValPropTransQuerySet propValPropTransQuerySet in g.PropCollection)
                {
                    if (propValPropTransQuerySet.PropGuid == "")
                    {
                    }
                    if (someData.Contains(propValPropTransQuerySet.PropGuid))
                    {
                        temp.Add(propValPropTransQuerySet);
                    }
                }

                if (temp.Count > 0)
                {
                    // Only if Properties are defined in the template
                    g.PropCollection = temp;
                }

                List<TemplateProperty> templateProperties = _groupProductManager.GetTemplateProperties(currentTemplate);

                if (templateProperties.Count > 0)
                {
                    IEnumerable<string> ids = templateProperties.Select(x => x.PropGroupGuid).Distinct();

                    foreach (string pId in ids)
                    {
                        string localId = pId;
                        List<TemplateProperty> tProps =
                            templateProperties.Where(x => x.PropGroupGuid == localId).ToList();
                        var temp2 = new List<PropValPropTransQuerySet>();
                        string propHeader = "Default";
                        foreach (PropValPropTransQuerySet propValPropTransQuerySet in g.PropCollection)
                        {
                            PropValPropTransQuerySet set = propValPropTransQuerySet;
                            if (tProps.Exists(x => x.PropGuid == set.PropGuid))
                            {
                                temp2.Add(propValPropTransQuerySet);
                                PropValPropTransQuerySet querySet = propValPropTransQuerySet;
                                propHeader = (tProps.Where(x => x.PropGuid == querySet.PropGuid).ToList())[0].PropHeader;
                            }
                        }
                        var propValPropTransGroup = new PropValPropTransGroup { PropertyGroups = temp2, Propheader = propHeader };
                        g.PropertyGroup.Add(propValPropTransGroup);
                    }
                }
            }
            else
            {
                // No defined property groups in the Template
                try
                {
                    var temp2 = g.PropCollection.ToList();
                    var propValPropTransGroup = new PropValPropTransGroup { PropertyGroups = temp2, Propheader = "" };
                    g.PropertyGroup.Add(propValPropTransGroup);
                }
                catch (Exception ex)
                {
                    FileLogger.Log(ex.Message + " " + ex.StackTrace);
                }
            }

            return g;
        }

        public ActionResult GroupEdit(int id)
        {
            var languageGuid = (string)Session["CurrentLanguage"];
            Group g = CommonGroupEditAction(id, languageGuid);

            //List<GroupProduct> gp = _groupProductManager.GroupProducts(id.ToString(), languageGuid, false);
            //gp.Sort((gp1, gp2) => gp1.SortIndex.CompareTo(gp2.SortIndex));

            string currentTemplate = g.Properties.PropDict["TEMPLATE"];

            if (string.IsNullOrEmpty(currentTemplate))
            {
                currentTemplate = _defaultGroupTemplate;
            }

            ViewData["TemplateList"] = _selectListStore.CreateTemplateSelect("IsGroupTemplate", currentTemplate, _groupProductManager);

            ViewData["UrlsList"] = _selectListStore.CreateUrlSelect(languageGuid, _groupProductManager);

            //ViewData["AspxList"] = _selectListStore.CreateAspxSelect(_fileStore);

            ViewData["TargetList"] = _selectListStore.CreateTargetSelect("_blank");


            if (TempData.ContainsKey("ModelState"))
            {
                var tempData = (bool)TempData["ModelState"];
                if (!tempData)
                {
                    ModelState.AddModelError("Error", CmsLanguage.CATMAN_LABEL_AN_ERROR_OCCURRED);
                }
            }

            var viewData =
                new CmsViewModel(new CatalogTreeUtility(null).Top2LevelGroups(languageGuid),
                                 g,
                                 null,
                                 null,
                                 null,
                                 null,
                                 null);

            return View(viewData);
        }

        [HttpPost, ValidateInput(false)]
        public string TemplateEdit(int id, FormCollection collection)
        {
            var languageGuid = (string)Session["CurrentLanguage"];
            try
            {
                Group group = new Group();
                group.GroupGuid = id;
                group.Properties = new GrpPropValSet();
                group.Properties.parentGuid = group.ID.ToString();
                group.Properties.PropDict = new Dictionary<string, string> { { "TEMPLATE", collection["TEMPLATE"] } };
                group.Properties.PropDict.Add("_TEMPLATE", collection["_TEMPLATE"]);
                _groupProductManager.SaveProperties(group, languageGuid);
                TempData["ModelState"] = true;
            }
            catch
            {
                return CmsLanguage.CATMAN_LABEL_AN_ERROR_OCCURRED;
            }
            return CmsLanguage.CATMAN_LABEL_ALLSAVED;
        }

        [HttpPost, ValidateInput(false)]
        public string GroupEdit(int id, FormCollection collection)
        {
            var languageGuid = (string)Session["CurrentLanguage"];
            try
            {
                Group group = _groupProductManager.GetGroup(id, languageGuid);
                GrpPropValSet propVal = group.Properties;

                if (propVal.PropDict != null)
                {
                    foreach (string s in collection.Keys)
                    {
                        if (propVal.PropDict.ContainsKey(s))
                        {
                            string v = collection[s];
                            if (v == "true,false" || v == "false")
                            {
                                v = v == "true,false" ? "true" : "false";
                            }
                            propVal.PropDict[s] = v;
                        }
                    }
                }

                _groupProductManager.SaveProperties(group, languageGuid);
                TempData["ModelState"] = true;
            }
            catch
            {
                return CmsLanguage.CATMAN_LABEL_AN_ERROR_OCCURRED;
            }
            return CmsLanguage.CATMAN_LABEL_ALLSAVED;
        }

        public ActionResult CatalogOverview()
        {
            var languageGuid = (string)Session["CurrentLanguage"];
            var viewData =
                new CmsViewModel(new CatalogTreeUtility(null).Top2LevelGroups(languageGuid),
                                 null,
                                 null,
                                 null,
                                 null,
                                 null,
                                 null);
            return View(viewData);
        }

        [HttpPost]
        public string FileTree(string validExtensionArray, string filter)
        {
            return _fileStore.FileTree(validExtensionArray, filter);
        }

        // TODO:
        // Make sure virtual directory is correct also in fileStore
        ///<summary>
        ///</summary>
        [AcceptVerbs(HttpVerbs.Post)]
        public void AsyncUpload()
        {
            string uploadFolder = Request["upload_folder"];
            if (string.IsNullOrEmpty(uploadFolder))
            {
                uploadFolder = _uploadBaseFolder;
            }
            // Get the data
            HttpPostedFileBase jpegImageUpload = Request.Files["userfile"];
            string responseText = _fileStore.SaveUploadedFile(jpegImageUpload, uploadFolder);
            Response.ContentType = "text/plain";
            Response.Write(responseText);
        }

        ///<summary>
        ///</summary>
        ///<param name="parentGuid"></param>
        ///<param name="siblingGuid"></param>
        ///<returns></returns>
        [HttpPost]
        [ValidateInput(false)]
        public string CreateNewGroup(string parentGuid, string siblingGuid)
        {
            var languageGuid = (string)Session["CurrentLanguage"];
            string result;
            try
            {
                int iParentGuid = int.Parse(parentGuid);
                int iSiblingGuid = int.Parse(siblingGuid);
                int newGuid = _groupProductManager.CreateNewGroup(iParentGuid, iSiblingGuid);
                result = newGuid.ToString();
                // Check if master cms
                if (ConfigUtil.IsMasterCms())
                {
                    Group group = _groupProductManager.GetGroupForEditor(newGuid, languageGuid);
                    GrpPropValSet propVal = group.Properties;

                    if (propVal.PropDict != null)
                    {
                        if (propVal.PropDict.ContainsKey("MASTERGUID"))
                        {
                            string v = "Master_" + Guid.NewGuid();
                            propVal.PropDict["MASTERGUID"] = v;
                            propVal.PropDict["_MASTERGUID"] = v;
                        }
                    }
                    _groupProductManager.SaveProperties(group, languageGuid);
                }
            }
            catch (Exception)
            {
                result = "1";
            }
            return result;
        }

        ///<summary>
        ///</summary>
        ///<param name="groupGuid"></param>
        ///<returns></returns>
        [HttpPost]
        [ValidateInput(false)]
        public string DeleteGroup(string groupGuid)
        {
            var languageGuid = (string)Session["CurrentLanguage"];
            string result;
            try
            {
                int iGroupGuid = int.Parse(groupGuid);
                int parentGuid = _groupProductManager.DeleteGroup(iGroupGuid, languageGuid);
                result = parentGuid.ToString();
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }
            return result;
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [ValidateInput(false)]
        public string AddProducts(string productGuidString, string groupGuid)
        {
            string result = string.Empty;
            try
            {
                string[] guids = productGuidString.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                _groupProductManager.AddProductsToGroup(guids, groupGuid);
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }
            return result;
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [ValidateInput(false)]
        public string RemoveProducts(string groupProductGuidString, string groupGuid)
        {
            string result = string.Empty;
            try
            {
                string[] guids = groupProductGuidString.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                _groupProductManager.DeleteProductsFromGroup(guids, groupGuid);
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }
            return result;
        }

        ///<summary>
        ///</summary>
        ///<param name="groupGuid"></param>
        ///<returns></returns>
        [HttpPost]
        [ValidateInput(false)]
        public string LoadSubGroups(string groupGuid)
        {
            int iGroupGuid;

            string languageGuid = "";
            if (Session != null)
            {
                if (Session["CurrentLanguage"] != null)
                {
                    languageGuid = (string)Session["CurrentLanguage"];
                }
                else
                {
                    languageGuid = _expanditUserService.LanguageGuid;
                }
            }

            int.TryParse(groupGuid, out iGroupGuid);
            Group g = new CatalogTreeUtility(null).SubGroups(languageGuid, iGroupGuid);

            // Reuse the TreeRenderHelper from Ajax call
            var h = new HtmlHelper(
                new ViewContext(ControllerContext,
                                new WebFormView(ControllerContext, "bull"), new ViewDataDictionary(), new TempDataDictionary(),
                                new HtmlTextWriter(new StringWriter())), new ViewPage()
                );

            string html = h.RenderTree(g.Children, group1 => group1, 1);
            return html;
        }

        /*
         * Called from ONE js function - [ js code: function saveListRegrouping(parentGuid, groupGuid){ ... }  ]
         * Called when a group is moved from one (parent) group to another
         * 
         */

        ///<summary>
        ///</summary>
        ///<param name="parentGuid"></param>
        ///<param name="groupGuid"></param>
        ///<returns></returns>
        [HttpPost]
        [ValidateInput(false)]
        public string SaveGroupTableReordering(string parentGuid, string groupGuid)
        {
            try
            {
                int iGroupGuid = int.Parse(groupGuid);
                int iParentGuid = int.Parse(parentGuid);
                _groupProductManager.RegroupGroupTable(iGroupGuid, iParentGuid);
            }
            catch (Exception ex)
            {
                if (_isLoggingEnabled)
                {
                    FileLogger.Log(ex.Message);
                }
            }
            return "group: " + groupGuid + " parent: " + parentGuid;
        }

        /*
         * Called from ONE js function - [ js code: function saveListChanges(parentGuid, groupGuid, siblingGuid){ ... }  ]
         * 
         * js function called from - makeSitemapDroppable()  OBS! initialization of functions!
         * 
         * js droppable action - something is dropped - probably moved.
         * 
         */

        ///<summary>
        ///</summary>
        ///<param name="groupGuid"></param>
        ///<param name="parentGuid"></param>
        ///<param name="siblingGuid"></param>
        ///<returns></returns>
        [HttpPost]
        [ValidateInput(false)]
        public string SaveGroupTableChanges(string groupGuid, string parentGuid, string siblingGuid)
        {
            string result = string.Empty;
            try
            {
                int iGroupGuid = int.Parse(groupGuid.Trim());
                int iParentGuid = int.Parse(parentGuid.Trim());
                int iSiblingGuid = int.Parse(siblingGuid.Trim());
                _groupProductManager.RearrangeGroupOrder(iGroupGuid, iParentGuid, iSiblingGuid);
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }
            return result; // "group: " + groupGuid + " parent: " + parentGuid + " sibling: " + siblingGuid;
        }

        /// <summary>
        /// Remove ML properties and set one resulting languageGuid to NULL
        /// </summary>
        /// <param name="propOwnerRefGuid"></param>
        /// <param name="propGuid"></param>
        /// <param name="languageGuid"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateInput(false)]
        public string MultiLanguageToNull(string propOwnerRefGuid, string propGuid, string languageGuid)
        {
            _groupProductManager.ConvertFromMultiLanguage(propOwnerRefGuid, propGuid, languageGuid);
            return null;
        }

        /// <summary>
        /// Rmomove NULL value and convert property to ML
        /// </summary>
        /// <param name="propOwnerRefGuid"></param>
        /// <param name="propGuid"></param>
        /// <param name="languageGuid"></param>
        /// <param name="nullParam"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateInput(false)]
        public string NullToMultiLanguage(string propOwnerRefGuid, string propGuid, string languageGuid,
                                          string nullParam)
        {
            if (nullParam != "NULL")
            {
                List<LanguageTable> languages = _languageRepository.LoadLanguages();
                _groupProductManager.ConvertToMultiLanguage(propOwnerRefGuid, propGuid, languageGuid, languages);
            }
            else
            {
                _groupProductManager.ConvertToMultiLanguage(propOwnerRefGuid, propGuid, languageGuid, null);
            }
            return null;
        }

        ///<summary>
        ///</summary>
        ///<param name="key"></param>
        ///<returns></returns>
        [HttpPost]
        public string GetTranslation(string key)
        {
            var languageGuid = (string)Session["PageLanguageGuid"];

            string name = null;
            try
            {
                var ci = new CultureInfo(languageGuid);
                var globalResourceObject = _httpContext.GetGlobalResourceObject("CmsLanguage", key, ci);
                if (globalResourceObject != null)
                {
                    name = globalResourceObject.ToString();
                }
            }
            catch
            {
                return null;
            }
            return name;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        [HttpPost]
        public string GetTemplateFolder(string key)
        {
            return "templates";
            //return AppSettingsReader.Read("TEMPLATE_FOLDER");
        }

        ///<summary>
        ///</summary>
        ///<param name="actionCommand"></param>
        ///<returns></returns>
        [HttpPost]
        public bool CreateRoutes(string actionCommand)
        {
            bool isSuccess = true;
            try
            {
                RouteManager.Route.PublicRouteDataProvider.RecreateAll();
            }
            catch (Exception ex)
            {
                FileLogger.Log(ex.Message + ex.StackTrace);
                isSuccess = false;
            }
            return isSuccess;
        }

        ///<summary>
        ///</summary>
        ///<returns></returns>
        [AcceptVerbs(HttpVerbs.Get)]
        public string CreateRoutes()
        {
            string result = "OK\r\n";
            try
            {
                int iResult = RouteManager.Route.PublicRouteDataProvider.RecreateAll();
                if (iResult <= 0)
                {
                    result = "No records updated";
                }
                else
                {
                    result = result + "Updated " + iResult + " records";
                }
            }
            catch (Exception ex)
            {
                result = "Update failed " + ex.Message;
            }
            return result;
        }

        // Multi Image Folder Browser
        ///<summary>
        ///</summary>
        ///<param name="selectedValue"></param>
        ///<returns></returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public ViewResult MultiImageBrowser(string selectedValue)
        {
            string urlDecodedSelectedValue = Server.UrlDecode(selectedValue);

            // Get the full list of image paths
            List<string> imagePaths = GetFilePathList("ImageList", _imagesRelativePath);
            var thePaths = new List<string>();

            // Keep the list index of the selectedValue
            int selectedIndex = 0;

            if (imagePaths != null)
            {
                try
                {
                    if (!string.IsNullOrEmpty(urlDecodedSelectedValue))
                    {
                        if (!urlDecodedSelectedValue.StartsWith("/"))
                        {
                            urlDecodedSelectedValue = "/" + urlDecodedSelectedValue;
                        }
                        // Where are we in the whole list?
                        selectedIndex = imagePaths.FindIndex(s => s.Equals(urlDecodedSelectedValue));
                    }
                }
                catch (Exception pathException)
                {
                    if (_isLoggingEnabled)
                    {
                        FileLogger.Log(pathException.Message);
                    }
                }
            }

            int pageRecords = 30;

            int pagedIndexValue = (selectedIndex / pageRecords);

            int pageerSize = pageRecords * (pagedIndexValue + 1);

            int from = (pageerSize - pageRecords);

            if (imagePaths == null)
            {
                imagePaths = new List<string>();
            }

            if ((thePaths.Count == 0) || (thePaths.Count == imagePaths.Count))
            {
                thePaths.Clear();

                if (imagePaths.Count > 0)
                {
                    for (int i = from; i < pageerSize; i++)
                    {
                        thePaths.Add(imagePaths[i]);
                        if (i >= imagePaths.Count - 1)
                        {
                            break;
                        }
                    }
                }
            }

            //TEST
            var pager = new Pager(pagedIndexValue + 1, imagePaths.Count, pageRecords);

            // NEW IDEA
            string multiImage = string.IsNullOrEmpty(selectedValue) ? "" : Server.UrlDecode(selectedValue);
            int selectedSecondValue = 10;
            string selectedTargetValue = "_blank";
            if (!string.IsNullOrEmpty(multiImage))
            {
                try
                {
                    var multiImageHelper = new MultiImageHelper();
                    multiImageHelper.PropToObjects(multiImage);
                    if (multiImageHelper.ImageObjects.Count > 0)
                    {
                        int.TryParse(multiImageHelper.SliderTime, out selectedSecondValue);
                        selectedTargetValue = multiImageHelper.ImageObjects[0].Target;
                    }
                }
                catch (Exception ex)
                {
                    FileLogger.Log(ex.Message + " " + ex.StackTrace);
                }
            }
            //

            ViewData["ImagePaths"] = thePaths;

            var seconds = new List<int>();
            for (int i = 5; i <= 60; i += 5)
            {
                seconds.Add(i);
            }
            var selectList = new SelectList(seconds, selectedSecondValue);
            ViewData["SecondsList"] = selectList;

            ViewData["TargetList"] = _selectListStore.CreateTargetSelect(selectedTargetValue);

            var viewData =
                new CmsViewModel(null,
                                 null,
                                 null,
                                 null,
                                 null,
                                 null,
                                 pager);

            return View(viewData);
        }

        /// <summary>
        /// Ajax Load picture list view
        /// Used to load the initial list - no filters applied of course!
        /// </summary>
        /// <returns></returns>
        [AcceptVerbs(HttpVerbs.Get)]
        public ViewResult FolderBrowser(string selectedValue)
        {
            string urlDecodedSelectedValue = Server.UrlDecode(selectedValue);

            // Get the full list of image paths
            List<string> imagePaths = GetFilePathList("ImageList", _imagesRelativePath);
            var thePaths = new List<string>();

            // Keep the list index of the selectedValue
            int selectedIndex = 0;
            int imagePathCount = 0;

            if (imagePaths != null)
            {
                imagePathCount = imagePaths.Count;
                try
                {
                    if (urlDecodedSelectedValue != null)
                    {
                        if (!urlDecodedSelectedValue.StartsWith("/"))
                        {
                            urlDecodedSelectedValue = "/" + urlDecodedSelectedValue;
                        }
                        // Where are we in the whole list?
                        selectedIndex = imagePaths.FindIndex(s => s.Equals(urlDecodedSelectedValue));
                    }
                }
                catch (Exception pathException)
                {
                    if (_isLoggingEnabled)
                    {
                        FileLogger.Log(pathException.Message);
                    }
                }
            }

            int pageRecords = 30;

            int pagedIndexValue = (selectedIndex / pageRecords);

            int pageerSize = pageRecords * (pagedIndexValue + 1);

            int from = (pageerSize - pageRecords);

            if ((thePaths.Count == 0) || (thePaths.Count == imagePathCount))
            {
                thePaths.Clear();

                if (imagePathCount > 0)
                {
                    for (int i = from; i < pageerSize; i++)
                    {
                        if (imagePaths == null) continue;
                        thePaths.Add(imagePaths[i]);
                        if (i >= imagePaths.Count - 1)
                        {
                            break;
                        }
                    }
                }
            }

            //TEST
            var pager = new Pager(pagedIndexValue + 1, imagePathCount, pageRecords);

            ViewData["ImagePaths"] = thePaths;

            var viewData =
                new CmsViewModel(null,
                                 null,
                                 null,
                                 null,
                                 null,
                                 null,
                                 pager);

            return View("Partials/FolderBrowser", viewData);
        }

        /// <summary>
        /// Get The Cached ImageList
        /// </summary>
        /// <returns></returns>
        private List<string> GetFilePathList(string cacheKey, string relativePath)
        {
            if (HttpContext.Cache[cacheKey] == null)
            {
                // Load all the filenames first time
                var fh = new FileHandler();
                List<string> fileList = fh.ListFiles(relativePath, "*");

                string virtualDirectory = ConfigUtil.OptionalVirtualDirectory();
                string path = HostingEnvironment.MapPath("~" + relativePath);
                if (virtualDirectory != null)
                {
                    path = HostingEnvironment.MapPath("/" + virtualDirectory) + relativePath.Replace("/", @"\");
                }

                HttpContext.Cache.Insert(cacheKey, fileList,
                                         new CacheDependency(path));
            }
            return (List<string>)HttpContext.Cache[cacheKey];
        }

        /// <summary>
        /// The ThumbnailInfo Controller
        /// </summary>
        /// <param name="imagePath">imagePath is of type /catalog/images/image.jpg</param>
        /// <returns>string with information about the image</returns>
        [AcceptVerbs(HttpVerbs.Post)]
        [ValidateInput(false)]
        public string ThumbnailInfo(string imagePath)
        {
            var fh = new FileHandler();
            return fh.GetImageInformationString(imagePath);
        }

        //The new Thumbnail Controller        
        ///<summary>
        ///</summary>
        ///<param name="imageName"></param>
        ///<param name="width"></param>
        ///<param name="height"></param>
        ///<returns></returns>
        [ValidateInput(false)]
        public HttpResponseBase ShowThumbnail(string imageName, string width, string height)
        {
            int maxHeight = int.Parse(height); // 45;
            int maxWidth = int.Parse(width); // 60;

            string temp = imageName.Replace("/", @"\");
            string path = AppDomain.CurrentDomain.BaseDirectory + temp;

            string virtualDirectory = ConfigUtil.OptionalVirtualDirectory();
            if (virtualDirectory != null)
            {
                path = HostingEnvironment.MapPath("/" + virtualDirectory + "/" + imageName);
            }


            string extension = Path.GetExtension(path);

            byte[] pBuffer = ImageHandler.CreateThumbnail(path, maxHeight, maxWidth, extension);
            //set response content type
            Response.ContentType = "image/" + extension;
            //write the image to the output stream
            Response.OutputStream.Write(pBuffer, 0, pBuffer.Length);
            return Response;
        }
    }
}
