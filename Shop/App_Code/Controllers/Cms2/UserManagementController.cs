﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using AutoMapper;
using EISCS.ExpandITFramework.Util;
using EISCS.Shop.Attributes;
using EISCS.Shop.DO.BAS.Interface;
using EISCS.Shop.DO.Dto;
using EISCS.Shop.DO.Dto.Cart;
using EISCS.Shop.DO.Dto.Currency;
using EISCS.Shop.DO.Interface;
using EISCS.WebUserControlLogic.Country;
using EISCS.WebUserControlLogic.Language;
using FormModels.Cms2.FormModels;
using Resources;
using ViewModels.Cms;
using ViewModels.Customer;
using ViewModels.Order;

namespace Controllers.Cms2
{
    [BusinessCenterUserAccess(AccessClass = "AccountManagement")]
    public class UserManagementController : CmsBaseController
    {
        private readonly IUserManager _userManager;
        private readonly ICountryControlLogic _countryControlLogic;
        private readonly ILanguageControlLogic _languageControlLogic;
        private readonly ICurrencyRepository _currencyRepository;
        private readonly IShopSalesService _shopSalesService;
        private readonly ICustomerService _customerService;
        private readonly IConfigurationRepository _configurationRepository;

        public UserManagementController(IUserManager userManager,
            ICountryControlLogic countryControlLogic,
            ILanguageControlLogic languageControlLogic,
            ICurrencyRepository currencyRepository, IShopSalesService shopSalesService, IHttpRuntimeWrapper httpRuntimeWrapper, ICustomerService customerService, IConfigurationRepository configurationRepository)
            : base(httpRuntimeWrapper)
        {
            _userManager = userManager;
            _countryControlLogic = countryControlLogic;
            _languageControlLogic = languageControlLogic;
            _currencyRepository = currencyRepository;
            _shopSalesService = shopSalesService;
            _customerService = customerService;
            _configurationRepository = configurationRepository;
        }

        public ActionResult Roles()
        {
            ViewBag.CurrentLanguageGuid = Session["CurrentLanguage"];
            return View();
        }

        public ActionResult GetRolesList(JQueryDataTableParamModel param)
        {
            param = SetDataTableParamsFromRequest(param);

            if (param.length == -1)
            {
                param.length = _userManager.RoleCount();
            }

            var rolesList = _userManager.Roles(
                param.start,
                param.start + param.length,
                param.columnBeingSort,
                param.sortDir,
                param.searchValue
                );

            return Json(new
            {
                param.draw,
                recordsTotal = _userManager.RoleCount(),
                recordsFiltered = _userManager.RoleCount(param.searchValue),
                data = rolesList
            });
        }

        [HttpPost]
        public ActionResult AddRole(RoleFormModel form)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.CurrentLanguageGuid = Session["CurrentLanguage"];
                return View("Roles");
            }

            _userManager.AddRole(form.RoleId, form.RoleDescription, false);
            return RedirectToAction("Roles");
        }

        public ActionResult DeleteRole(string roleId, string substituteRoleId)
        {
            if ((roleId != "Admin") && (roleId != "Anonymous") && (roleId != "B2B") && (roleId != "B2C"))
            {
                _userManager.DeleteRole(roleId, substituteRoleId);
            }
            return RedirectToAction("Roles");
        }

        public ActionResult EditRole(string roleId)
        {
            var role = _userManager.GetRole(roleId);
            var roleForm = Mapper.Map<RoleFormModel>(role);
            ViewData["RoleId"] = role.RoleId;
            return View(roleForm);
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult EditRole(RoleFormModel form)
        {
            if (!ModelState.IsValid)
            {
                return View(form);
            }

            var role = new RoleTable
                {
                    RoleId = form.RoleId,
                    RoleDescription = form.RoleDescription,
                    ReadOnly = false
                };
            _userManager.UpdateRole(role);
            return RedirectToAction("Roles");
        }

        public ActionResult Customers()
        {
            ViewBag.CurrentLanguageGuid = Session["CurrentLanguage"];
            return View();
        }

        public ActionResult Customer(string customerGuid)
        {
            var customer = _userManager.GetCustomer(customerGuid);
            var viewModel = Mapper.Map<CustomerViewModel>(customer);
            var countryRows = _countryControlLogic.LoadCountries().Select();
            var countryDictionary = countryRows.ToDictionary(r => (string)r["CountryGuid"], r => (string)r["CountryName"]);
            viewModel.CountryList = countryDictionary;
            return View(viewModel);
        }

        public JsonResult GetCustomersList(JQueryDataTableParamModel param)
        {
            param = SetDataTableParamsFromRequest(param);

            if (param.length == -1)
            {
                param.length = _userManager.CustomerCount();
            }

            var customerList = _userManager.Customers(
                param.start,
                param.start + param.length,
                param.columnBeingSort,
                param.sortDir,
                param.searchValue
                );

            var customerTableBases = customerList as CustomerTableBase[] ?? customerList.ToArray();
            foreach (var customer in customerTableBases)
            {
                customer.CurrencyGuid = ExpanditLib2.CStrEx(customer.CurrencyGuid);
                customer.CountryGuid = ExpanditLib2.CStrEx(customer.CountryGuid);
                customer.Address1 = ExpanditLib2.CStrEx(customer.Address1);
                customer.CityName = ExpanditLib2.CStrEx(customer.CityName);
                customer.ZipCode = ExpanditLib2.CStrEx(customer.ZipCode);
            }

            return Json(new
            {
                param.draw,
                recordsTotal = _userManager.CustomerCount(),
                recordsFiltered = _userManager.CustomerCount(param.searchValue),
                data = customerTableBases
            });
        }

        public ActionResult Users()
        {
            string baseUrl = null;
            const string actionUrl = "/Impersonation";

            try
            {
                // Get the remote shop address from the configuration table
                baseUrl = _configurationRepository.GetRemoteUrl();
            }
            catch { }

            if (string.IsNullOrWhiteSpace(baseUrl))
            {
                // An empty result means we are going to use the local address
                baseUrl = VirtualShopRoot();
            }

            string remoteUrl = baseUrl + actionUrl;

            var model = new ImpersonationViewModel { RemoteUrl = remoteUrl, ReturnUrl = baseUrl };

            ViewBag.CurrentLanguageGuid = Session["CurrentLanguage"];
            return View(model);
        }

        [HttpPost]
        public JsonResult GetRoleList()
        {
            IEnumerable<RoleTable> roles = _userManager.GetAllRoles();
            var roleTables = roles as RoleTable[] ?? roles.ToArray();
            Array.Sort(roleTables, (x, y) => String.CompareOrdinal(x.RoleId, y.RoleId));
            return Json(roleTables.ToDictionary(r => r.RoleId, r => r.RoleDescription).ToList());
        }

        // START
        public JsonResult GetUsersList(JQueryDataTableParamModel param)
        {
            param = SetDataTableParamsFromRequest(param);

            if (param.length == -1)
            {
                param.length = _userManager.UserCount();
            }

            var usersList = _userManager.Users(
                param.start,
                param.start + param.length,
                param.columnBeingSort,
                param.sortDir,
                param.searchValue
                );

            usersList = NoNullValues(usersList);

            IEnumerable<RoleTable> roles = _userManager.GetAllRoles();
            var roleTables = roles as RoleTable[] ?? roles.ToArray();
            Array.Sort(roleTables, (x, y) => String.CompareOrdinal(x.RoleDescription, y.RoleDescription));

            // Get All Customers
            int customerCount = _userManager.CustomerCount();
            IEnumerable<CustomerTableBase> customers = _userManager.Customers(0, customerCount > 0 ? customerCount - 1 : customerCount, "CompanyName", "ASC", null);
            var customerTables = customers as IList<CustomerTableBase> ?? customers.ToList();

            var userTables = usersList as IList<UserTable> ?? usersList.ToList();
            foreach (UserTable userTable in userTables)
            {
                userTable.RoleList = roleTables.ToDictionary(r => r.RoleId, r => r.RoleDescription);
                //userTable.CustomerList = customerTables.ToDictionary(c => c.CustomerGuid, c => c.CompanyName);

                if (!string.IsNullOrWhiteSpace(userTable.CustomerGuid))
                {
                    var customer = customerTables.FirstOrDefault(c => c.CustomerGuid == userTable.CustomerGuid);
                    if (customer != null)
                    {
                        userTable.CustomerName = customer.CompanyName;
                    }
                }
                else
                {
                    userTable.CustomerName = "";
                }
            }

            return Json(new
            {
                param.draw,
                recordsTotal = _userManager.UserCount(),
                recordsFiltered = _userManager.UserCount(param.searchValue),
                data = userTables
            });
        }

        private IEnumerable<UserTable> NoNullValues(IEnumerable<UserTable> usersList)
        {
            var noNullValues = usersList as UserTable[] ?? usersList.ToArray();
            foreach (var user in noNullValues)
            {
                user.CountryGuid = ExpanditLib2.CStrEx(user.CountryGuid);
                user.CityName = ExpanditLib2.CStrEx(user.CityName);
                user.ContactName = ExpanditLib2.CStrEx(user.ContactName);
                user.Address1 = ExpanditLib2.CStrEx(user.Address1);
                user.Address2 = ExpanditLib2.CStrEx(user.Address2);
                user.EmailAddress = ExpanditLib2.CStrEx(user.EmailAddress);
                user.ZipCode = ExpanditLib2.CStrEx(user.ZipCode);
                user.RoleId = ExpanditLib2.CStrEx(user.RoleId);
            }
            return noNullValues;
        }
        // END

        public ActionResult UserRoleMatrix()
        {
            var roles = _userManager.GetAllRoles();
            var accesses = _userManager.GetAllAccesses();
            var accessRoles = _userManager.GetAllAccessRoles();

            var accessRoleMatrix = new AccessRoleMatrix(roles, accesses, accessRoles);

            return View(accessRoleMatrix);
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult Edit(FormCollection form)
        {
            // Form collection must only contain regular inputs of type checkbox where the value is on
            var keyValues = new Dictionary<string, List<string>>();
            foreach (string s in form.Keys)
            {
                string[] parts = s.Split(Convert.ToChar("_"));
                string part1 = parts[0];
                string part2 = parts[1];
                if (!keyValues.ContainsKey(part1))
                {
                    keyValues.Add(part1, new List<string> { part2 });
                }
                else
                {
                    keyValues[part1].Add(part2);
                }
            }

            _userManager.UpdateAccess(keyValues);

            return RedirectToAction("UserRoleMatrix");
        }

        public ActionResult DeleteUser(string userGuid)
        {
            var userTable = _userManager.GetUser(userGuid);
            _userManager.DeleteUser(userTable);
            return RedirectToAction("Users");
        }

        [HttpPost, ValidateInput(false)]
        public bool UpdateUserCustomer(string userGuid, string customerGuid)
        {
            return _userManager.UpdateUserCustomer(userGuid, customerGuid);
        }

        [HttpPost, ValidateInput(false)]
        public bool UpdateUserRole(string userGuid, string roleId)
        {
            return _userManager.UpdateUserRole(userGuid, roleId);
        }

        public ActionResult EditUser(string userGuid)
        {
            if (userGuid == null)
            {
                return RedirectToAction("Users");
            }
            var userTable = _userManager.GetUser(userGuid);
            var model = Mapper.Map<UserTable, BusinessCenterEditUserFormModel>(userTable);

            PopulateDropdowns(model);
            return View(model);
        }

        [HttpPost, ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult EditUser(BusinessCenterEditUserFormModel model)
        {
            UserTable userTable = Mapper.Map<BusinessCenterEditUserFormModel, UserTable>(model);
            userTable.UserModified = DateTime.Now;
            bool isUpdated = _userManager.ManagerUpdateUser(userTable);
            PopulateDropdowns(model);
            return View(model);
        }

        public ActionResult AddUser()
        {
            var model = new BusinessCenterRegisterUserFormModel();
            PopulateDropdowns(model);
            return View(model);
        }

        [HttpPost, ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult AddUser(BusinessCenterRegisterUserFormModel model)
        {
            var chosenEncryptionType = ConfigurationManager.AppSettings["ENCRYPTION_TYPE"];
            string encryptionSalt = Encryption.GenerateSaltValue();

            UserTable userTable = Mapper.Map<BusinessCenterRegisterUserFormModel, UserTable>(model);
            userTable.UserCreated = DateTime.Now;
            userTable.UserModified = DateTime.Now;
            userTable.UserPassword = Encryption.Encrypt(userTable.UserPassword, encryptionSalt, chosenEncryptionType);
            userTable.EncryptionSalt = encryptionSalt;
            userTable.EncryptionType = chosenEncryptionType;
            userTable.PasswordResetRequestTime = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;
            userTable.IsApproved = true;
            bool isUserCreated = _userManager.AddUser(userTable);
            if (isUserCreated)
            {
                model = new BusinessCenterRegisterUserFormModel(); // Reset to all blank fields
            }
            else
            {
                ModelState.AddModelError(string.Empty, "Could not create user.");
            }

            PopulateDropdowns(model);
            return View(model);
        }

        private void PopulateDropdowns(BusinessCenterEditUserFormModel model)
        {
            DataRow[] countryRows = _countryControlLogic.LoadCountries().Select();
            Dictionary<string, string> countryDictionary = countryRows.ToDictionary(r => (string)r["CountryGuid"], r => (string)r["CountryName"]);
            model.CountryList = countryDictionary;

            DataRow[] languageRows = _languageControlLogic.LoadLanguages(true).Select();
            Dictionary<string, string> languageDictionary = languageRows.ToDictionary(r => (string)r["LanguageGuid"], r => (string)r["LanguageName"]);
            model.LanguageList = languageDictionary;

            IEnumerable<CurrencyItem> currencyItems = GetValidCurrencyItems();
            model.CurrencyList = currencyItems.ToDictionary(c => c.Id, c => c.Name);

            IEnumerable<RoleTable> roles = _userManager.GetAllRoles();
            model.RoleList = roles.ToDictionary(r => r.RoleId, r => r.RoleDescription);

            int customerCount = _userManager.CustomerCount();
            IEnumerable<CustomerTableBase> customers = _userManager.Customers(0, customerCount > 0 ? customerCount - 1 : customerCount, "CompanyName", "ASC", null);
            model.CustomerList = customers.ToDictionary(c => c.CustomerGuid, c => c.CompanyName);
        }

        private IEnumerable<CurrencyItem> GetValidCurrencyItems()
        {
            return _currencyRepository.GetValid();
        }

        private JQueryDataTableParamModel SetDataTableParamsFromRequest(JQueryDataTableParamModel param)
        {
            param.searchValue = Request["search[value]"];
            if (param.start != 0)
            {
                param.start++;
                param.length--;
            }
            int columnNumber = Convert.ToInt32(Request["order[0][column]"]);

            string theRequestKey = @"columns[" + columnNumber + "][data]";
            string strColumnName = Request[theRequestKey];
            switch (strColumnName)
            {
                case "CheckboxHtml":
                    break;
                default:
                    param.columnBeingSort = strColumnName;
                    break;
            }
            param.sortDir = Request["order[0][dir]"];

            return param;
        }

        public ActionResult Details(string userGuid)
        {
            if (userGuid == null)
            {
                return RedirectToAction("Users");
            }

            ViewBag.CurrentLanguageGuid = Session["CurrentLanguage"];

            var userTable = _userManager.GetUser(userGuid);
            var model = Mapper.Map<UserTable, BusinessCenterRegisterUserFormModel>(userTable);

            PopulateDropdowns(model);
            return View(model);
        }

        [HttpPost]
        public ActionResult _GetLastOrders(string userGuid, JQueryDataTableParamModel param)
        {
            param = SetDataTableParamsFromRequest(param);

            var sales = _shopSalesService.GetShopSalesRange(userGuid, param.start, param.start + param.length, param.searchValue, param.columnBeingSort, param.sortDir);
            var salesViewModel = Mapper.Map<List<ShopSalesHeader>, List<OrderViewModel>>(sales);
            return Json(new
                {
                    param.draw,
                    recordsTotal = _shopSalesService.GetShopSalesHeaderByUser(userGuid).Count,
                    recordsFiltered = _shopSalesService.GetShopSalesHeaderByUser(userGuid).Count,
                    data = salesViewModel,
                });
        }

        public ActionResult ExportUsersToCSV()
        {
            var users = _userManager.Users(
                0,
                _userManager.UserCount(),
                "ContactName",
                "asc",
                null
                );

            users = NoNullValues(users);

            var roles = _userManager.GetAllRoles();
            var roleTables = roles as RoleTable[] ?? roles.ToArray();
            Array.Sort(roleTables, (x, y) => String.CompareOrdinal(x.RoleDescription, y.RoleDescription));

            var userTables = users as IList<UserTable> ?? users.ToList();
            foreach (var userTable in userTables)
            {
                userTable.RoleList = roleTables.ToDictionary(r => r.RoleId, r => r.RoleDescription);
            }
            var now = DateTime.Now;

            var attachment = "attachment; filename=Users-" + now.ToString("yyyy.MM.dd-HH.mm.ss") + ".csv";

            Response.Clear();
            Response.ClearHeaders();
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/vnd.ms-excel";
            Response.ContentEncoding = Encoding.UTF32;
            Response.AddHeader("Pragma", "public");

            WriteColumnName();
            foreach (var user in users)
            {
                WriteUserInfo(user);
            }

            Response.End();

            return null;
        }

        private void WriteUserInfo(UserTable user)
        {
            var stringBuilder = new StringBuilder();
            AddComma(user.ContactName, stringBuilder);
            AddComma(user.Address1, stringBuilder);
            AddComma(user.Address2, stringBuilder);
            AddComma(user.CityName, stringBuilder);
            AddComma(user.ZipCode, stringBuilder);
            AddComma(user.CountryGuid, stringBuilder);
            AddComma(user.EmailAddress, stringBuilder);
            AddComma(user.LastOrder.Year != 1 ? user.LastOrder.ToString("dd/MM/yyyy HH:mm:ss") : " ", stringBuilder);
            AddComma(user.RoleId, stringBuilder);
            Response.Write(stringBuilder.ToString());
            Response.Write(Environment.NewLine);
        }

        private void AddComma(string value, StringBuilder stringBuilder)
        {
            if (string.IsNullOrEmpty(value))
            {
                value = "";
            }
            stringBuilder.Append(value.Replace(',', ' '));
            stringBuilder.Append(", ");
        }

        private void WriteColumnName()
        {
            string columnNames = Language.LABEL_USERDATA_NAME + ", " +
                                 Language.LABEL_USERDATA_ADDRESS_1 + ", " +
                                 Language.LABEL_USERDATA_ADDRESS_2 + ", " +
                                 Language.LABEL_USERDATA_CITY + ", " +
                                 Language.LABEL_USERDATA_ZIPCODE + ", " +
                                 Language.LABEL_USERDATA_COUNTRY + ", " +
                                 Language.LABEL_EMAIL_ADDRESS + ", " +
                                 Language.LABEL_LAST_ORDER + ", " +
                                 "Role";

            Response.Write(columnNames);
            Response.Write(Environment.NewLine);
        }
    }

    public class AccessRoleMatrix
    {
        public IEnumerable<RoleTable> Roles { get; private set; }
        public IEnumerable<AccessTable> Accesses { get; private set; }
        public IEnumerable<AccessRoles> AccessRoles { get; private set; }
        public string[] AccessTypes { get; private set; }

        public AccessRoleMatrix(IEnumerable<RoleTable> roles, IEnumerable<AccessTable> accesses, IEnumerable<AccessRoles> accessRoles)
        {
            Roles = roles;
            Accesses = accesses;
            AccessRoles = accessRoles;
            AccessTypes = GetAccessTypes();
        }

        private string[] GetAccessTypes()
        {
            var distinct = Accesses.GroupBy(a => a.AccessType).Select(s => s.First());
            return distinct.Select(accessTable => accessTable.AccessType).ToArray();
        }
    }

    public class JQueryDataTableParamModel
    {
        public int draw { get; set; }
        public int length { get; set; }
        public int start { get; set; }
        public string searchValue { get; set; }
        public string columnBeingSort { get; set; }
        public string sortDir { get; set; }
        public int groupGuid { get; set; }
    }
}