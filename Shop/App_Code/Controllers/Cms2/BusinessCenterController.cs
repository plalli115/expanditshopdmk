﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using EISCS.Catalog;
using EISCS.ExpandIT;
using EISCS.ExpandITFramework.Util;
using EISCS.Shop.Attributes;
using EISCS.Shop.DO.Dto;
using EISCS.Shop.DO.Dto.Cart;
using EISCS.Shop.DO.Interface;
using EISCS.Shop.DO.Service;
using EISCS.Wrappers.Configuration;
using ExpandIT;
using FormModels;
using Resources;
using ViewModels;
using ViewModels.Order;
using ViewServices;

namespace Controllers.Cms2
{
    public class BusinessCenterController : CmsBaseController
    {
        private readonly ICompanyRepository _companyRepository;
        private readonly IConfigurationObject _configuration;
        private readonly IKeyPerformanceIndicatorsService _kpiService;
        private readonly IOrderService _orderService;
        private readonly IProductRepository _productRepository;
        private readonly IShippingHandlingProviderRepository _shippingHandlerRepository;
        private readonly IShopSalesService _shopSalesService;
        private readonly IUserItemRepository _userItemRepository;

        public BusinessCenterController(
            IHttpRuntimeWrapper httpRuntimeWrapper,
            IProductRepository productRepository,
            IShopSalesService shopSalesService,
            IShippingHandlingProviderRepository shippingHandlerRepository,
            IConfigurationObject configuration,
            IOrderService orderService,
            ICompanyRepository companyRepository,
            IUserItemRepository userItemRepository,
            IKeyPerformanceIndicatorsService kpiService)
            : base(httpRuntimeWrapper)
        {
            _productRepository = productRepository;
            _shopSalesService = shopSalesService;
            _shippingHandlerRepository = shippingHandlerRepository;
            _configuration = configuration;
            _orderService = orderService;
            _companyRepository = companyRepository;
            _userItemRepository = userItemRepository;
            _kpiService = kpiService;
        }

        public ActionResult CompanyName()
        {
            var companyInfo = _companyRepository.GetCompanyInfo();
            if (companyInfo == null || companyInfo.CompanyName.IsNullOrEmpty())
            {
                companyInfo = new Company {CompanyName = ConfigurationManager.AppSettings["COMPANY_NAME"]};
            }
            return View(companyInfo);
        }

        [BusinessCenterUserAccess(AccessClass = "BusinessCenter")]
        public ActionResult Index()
        {
            var languageGuid = (string) Session["CurrentLanguage"];
            var viewData =
                new CmsViewModel(new CatalogTreeUtility(null).Top2LevelGroups(languageGuid),
                    null,
                    null,
                    null,
                    null,
                    null,
                    null);

            ViewBag.UsersInCatalog = CountTotalUsers();
            ViewBag.ProductsInCatalog = ProductsInCatalog();
            ViewBag.RevenueLastYear = RevenueLastYear();
            ViewBag.RevenueCurrentYear = RevenueCurrentYear();
            ViewBag.B2CUsers = B2CUsersCount();
            return View(viewData);
        }

        private int B2CUsersCount()
        {
            return _userItemRepository.GetB2CUsersCount();
        }

        private string RevenueCurrentYear()
        {
            var initDate = new DateTime(DateTime.Now.Year, 1, 1);
            return _shopSalesService.GetRevenueBetweenDates(initDate, DateTime.Now);
        }

        private string RevenueLastYear()
        {
            var initDate = new DateTime(DateTime.Now.Year - 1, 1, 1);
            var endDate = new DateTime(initDate.Year, 12, 31);
            return _shopSalesService.GetRevenueBetweenDates(initDate, endDate);
        }

        private int CountTotalUsers()
        {
            return _userItemRepository.CountTotalUsers();
        }

        private int ProductsInCatalog()
        {
            return _productRepository.GetTotalRecords();
        }

        public ActionResult LastOrders()
        {
            List<ShopSalesHeader> lastSales = _shopSalesService.GetLastShopSales();
            List<OrderViewModel> lastSalesViewModel = Mapper.Map<List<ShopSalesHeader>, List<OrderViewModel>>(lastSales);
            return PartialView("Partials/_LastOrders", lastSalesViewModel);
        }

        public ActionResult OrderDetail(string id)
        {
            ShopSalesHeader salesHeader = _orderService.GetOrder(id);
            OrderViewModel viewOrder = Mapper.Map<ShopSalesHeader, OrderViewModel>(salesHeader);

            if (!string.IsNullOrEmpty(viewOrder.ShippingHandlingProviderGuid))
            {
                viewOrder.ShippingHandlingProvider = _shippingHandlerRepository.GetShippingHandlingProvider(viewOrder.ShippingHandlingProviderGuid);
            }

            // Make correct Vat/Total calculation for display in view
            ExpDictionary taxAmounts;
            var totalVatViewCalculator = new TotalVatViewCalculator();
            double shippingTaxPct = ExpanditLib2.ConvertToDbl(_configuration.Read("SHIPPING_TAX_PCT"));
            double total = totalVatViewCalculator.CalculateTotal(salesHeader, shippingTaxPct, out taxAmounts);

            viewOrder.Total = total;
            viewOrder.TaxAmounts = taxAmounts;

            string taxDisplayConfiguration = ExpanditLib2.CStrEx(_configuration.Read("SHOW_TAX_TYPE")).ToUpper();

            if (taxDisplayConfiguration == "CUST" && salesHeader.PricesIncludingVat)
            {
                taxDisplayConfiguration = "INCL";
            }

            if (taxDisplayConfiguration == "INCL")
            {
                viewOrder.InvoiceDiscount = viewOrder.InvoiceDiscountInclTax;
                viewOrder.HandlingAmount = viewOrder.HandlingAmountInclTax;
                viewOrder.PaymentFeeAmount = viewOrder.PaymentFeeAmountInclTax;
                viewOrder.ServiceCharge = viewOrder.ServiceChargeInclTax;
                viewOrder.ShippingAmount = viewOrder.ShippingAmountInclTax;
                viewOrder.SubTotal = viewOrder.SubTotalInclTax;
                viewOrder.PricesIncludingVat = true;

                foreach (var line in viewOrder.Lines)
                {
                    line.ListPrice = line.ListPriceInclTax;
                    line.LineDiscountAmount = line.LineDiscountAmountInclTax;
                    line.LineTotal = line.LineTotal*(1 + ExpanditLib2.ConvertToDbl(line.TaxPct)/100);
                }
            }
            else
            {
                viewOrder.PricesIncludingVat = false;
            }

            return View(viewOrder);
        }

        public JsonResult GetLatestSalesInXDays(int days)
        {
            List<ShopSalesHeader> salesHeader = _shopSalesService.GetLatestShopSalesInXDays(days);

            List<SaleInDay> salesInDays = GetTotalSalesInDay(salesHeader);

            return Json(salesInDays);
        }

        public List<SaleInDay> GetTotalSalesInDay(List<ShopSalesHeader> sales)
        {
            var salesInDays = new List<SaleInDay>();

            foreach (var item in sales)
            {
                if (!salesInDays.IsNullOrEmpty() && salesInDays.Last().DateTime.DayOfYear == item.HeaderDate.DayOfYear)
                {
                    salesInDays.Last().Total += item.TotalInclTax;
                }
                else
                {
                    var day = new SaleInDay
                        {
                            DateTime = item.HeaderDate,
                            DateTimeJs = GetJavascriptTimestamp(item.HeaderDate),
                            Total = item.TotalInclTax,
                            Currency = item.CurrencyGuid
                        };
                    salesInDays.Add(day);
                }
            }
            return salesInDays;
        }

        public static long GetJavascriptTimestamp(DateTime input)
        {
            var span = new TimeSpan(DateTime.Parse("1/1/1970").Ticks);
            DateTime time = input.Subtract(span);
            long ticks = time.Ticks/10000;
            return ticks;
        }

        [BusinessCenterUserAccess(AccessClass = "BusinessCenter")]
        public ActionResult Settings()
        {
            Company companyInfo = _companyRepository.GetCompanyInfo();
            CompanyInfoFormModel formModel = Mapper.Map<CompanyInfoFormModel>(companyInfo) ?? new CompanyInfoFormModel {IsSuccess = false};

            return View(formModel);
        }

        [BusinessCenterUserAccess(AccessClass = "BusinessCenter")]
        [HttpPost, ValidateInput(false)]
        public ActionResult Settings(CompanyInfoFormModel formModel)
        {
            if (ModelState.IsValid)
            {
                bool success = _companyRepository.Update(Mapper.Map<Company>(formModel));
                if (success)
                {
                    formModel.IsSuccess = true;

                    return View(formModel);
                }
                ModelState.AddModelError(string.Empty, Language.SO_ERROR_UNKNOWNERROR);
                return View(formModel);
            }
            ModelState.AddModelError(string.Empty, Language.SO_ERROR_VALIDATION);

            Company companyInfo = _companyRepository.GetCompanyInfo();
            formModel = Mapper.Map<CompanyInfoFormModel>(companyInfo);
            formModel.IsSuccess = false;

            return RedirectToAction("Settings");
        }

        [BusinessCenterUserAccess(AccessClass = "BusinessCenter")]
        public ActionResult KPI()
        {
            ViewData["DaysWithoutReport"] = _kpiService.DaysWithoutReport();
            _kpiService.GenerateReport();
            return View();
        }

        private List<KPIViewModel> FormatReport(IEnumerable<KPI> kpi)
        {
            var list = new List<KPIViewModel>();
            foreach (var report in kpi)
            {
                var reportViewModel = Mapper.Map<KPIViewModel>(report);
                if (Double.IsNaN(reportViewModel.AvgItemPrize))
                    reportViewModel.AvgItemPrize = 0.0;
                if (Double.IsNaN(reportViewModel.TotalRevenueWithoutVAT))
                    reportViewModel.TotalRevenueWithoutVAT = 0.0;

                reportViewModel.TotalRevenueWithoutVATString = CurrencyFormatter.FormatCurrency(reportViewModel.TotalRevenueWithoutVAT, _configuration.Read("MULTICURRENCY_SITE_CURRENCY"));
                reportViewModel.AvgItemPrizeString = CurrencyFormatter.FormatCurrency(reportViewModel.AvgItemPrize, _configuration.Read("MULTICURRENCY_SITE_CURRENCY"));
                reportViewModel.AvgOrderSizeString = report.AvgOrderSize.ToString("F");
                
                list.Add(reportViewModel);
            }

            return list;
        }

        [HttpPost]
        public ActionResult GenerateReport()
        {
            _kpiService.GenerateReport();
            return RedirectToAction("KPI", "BusinessCenter");
        }

        public class SaleInDay
        {
            public long DateTimeJs { get; set; }
            public DateTime DateTime { get; set; }
            public double Total { get; set; }
            public string Currency { get; set; }
        }

        [HttpPost]
        public ActionResult SelectPeriod()
        {
            var startDateRequest = Request["startDate"];
            var endDateRequest = Request["endDate"];
            var periodRequest = Request["period"];
            var startDate = DateTime.ParseExact(startDateRequest, "dd/MM/yyyy", CultureInfo.InvariantCulture).Date;
            var endDate = DateTime.ParseExact(endDateRequest, "dd/MM/yyyy", CultureInfo.InvariantCulture).Date;

            var periodStartDate = new DateTime();
            var periodEndDate = new DateTime();
            switch (periodRequest)
            {
                case "PP":
                    var days = (endDate - startDate).Days;
                    periodEndDate = startDate.AddDays(-1);
                    periodStartDate = periodEndDate.AddDays(-days);
                    break;
                case "LY":
                    periodStartDate = startDate.AddYears(-1);
                    periodEndDate = endDate.AddYears(-1);
                    break;
            }

            var report = _kpiService.GetReport(startDate, endDate, periodStartDate, periodEndDate);
            ViewData["DaysWithoutReport"] = _kpiService.DaysWithoutReport();

            if (report[0] == null || report[1] == null)
            {
                ModelState.AddModelError(string.Empty, Language.SO_ERROR_UNKNOWNERROR);
                return RedirectToAction("KPI");
            }
            
            var listViewModel = FormatReport(report);

            listViewModel[0].EntryDate = startDate;
            listViewModel[1].EntryDate = periodStartDate;
            listViewModel[0].EndDate = endDate;
            listViewModel[1].EndDate = periodEndDate;

            return Json(listViewModel);
        }

        public PartialViewResult GetCompanyInfo()
        {
            var companyInfo = _companyRepository.GetCompanyInfo();
            
            return PartialView(companyInfo);
        }
    }
}