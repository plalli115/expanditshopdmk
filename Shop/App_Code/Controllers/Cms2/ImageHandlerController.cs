using System.Linq;
using System.Web;
using System.Web.Mvc;
using EISCS.ExpandITFramework.Util;

namespace Controllers.Cms2
{
    public class ImageHandlerController : CmsBaseController
    {
        public ImageHandlerController(IHttpRuntimeWrapper httpRuntimeWrapper) : base(httpRuntimeWrapper)
        {

        }

        public ActionResult Index()
        {
            return View();
        }

    }

}