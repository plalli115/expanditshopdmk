﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using System.Web.UI;
using CmsPublic.DataProviders.Logging;
using CmsPublic.DataRepository;
using CmsPublic.ModelLayer.Models;
using EISCS.Catalog;
using EISCS.CMS.CentralManagement.Configuration;
using EISCS.CMS.CentralManagement.Data;
using EISCS.CMS.CentralManagement.Helpers;
using EISCS.CMS.CentralManagement.Logic;
using EISCS.CMS.CentralManagement.Models.ViewModels;
using EISCS.ExpandITFramework.Util;
using EISCS.Shop.DO;
using EISCS.Shop.DO.Interface;
using EISCS.Shop.DO.Repository;
using EISCS.Shop.DO.Service;

namespace Controllers.Cms2
{
    [HandleError]
    public class CentralManagementController : CmsBaseController
    {
        private readonly List<RepositoryCatalogPathNode> _clientNodes;
        private readonly DataCopyLogic _dataCopyLogic;
        private readonly IGroupRepository _masterGroupRepository;
        private readonly RepositoryCatalogPathNode _masterNode;

        public CentralManagementController(IHttpRuntimeWrapper httpRuntimeWrapper)
            : base(httpRuntimeWrapper)
        {
            ConnectionStrings connectionStrings = new ConnectionStrings("ExpandITConnectionString");
            IExpanditDbFactory db = new ExpanditDbFactory(connectionStrings.MainConnection.ConnectionString, connectionStrings.MainConnection.ProviderName);
            _masterGroupRepository =
                new GroupRepository(db, DependencyResolver.Current.GetService<IPropertiesDataService>());

            var configSettings = new ConfigSettings();
            Dictionary<string, string> pathConnection =
                configSettings.TestAlgoritm(connectionStrings.MainConnectionstringName,
                                            connectionStrings.ClientConnectionstringNames);
            if (pathConnection == null)
            {
                return;
            }

            CmsCatalogPathElement element = configSettings.GetSectionElements(connectionStrings.MainConnectionstringName);
            string masterDescription = element != null ? element.Description : null;

            var masterNode = new RepositoryCatalogPathNode
            {
                CatalogPath =
                    pathConnection[
                        connectionStrings.
                            MainConnection.Name],
                Repository = _masterGroupRepository,
                Description = masterDescription
            };


            try
            {
                //IPropertiesDataService propertiesDataService = DependencyResolver.Current.GetService<IPropertiesDataService>();
                
                _clientNodes = (from clientConnection in connectionStrings.ClientConnections
                                let dbExecutionCustom = new ExpanditDbFactory(clientConnection.ConnectionString, clientConnection.ProviderName)
                                let propertiesDataService = new PropertiesDataService(dbExecutionCustom)
                                let groupRepository = new GroupRepository(dbExecutionCustom, propertiesDataService)
                                let description = configSettings.GetSectionElements(clientConnection.Name).Description
                                let clientBaseUrl = configSettings.GetSectionElements(clientConnection.Name).BaseUrl
                                select new RepositoryCatalogPathNode
                                {
                                    CatalogPath = pathConnection.ContainsKey(clientConnection.Name) ? pathConnection[clientConnection.Name] : "",
                                    Repository = groupRepository,
                                    Description = description,
                                    BaseUrl = !string.IsNullOrWhiteSpace(clientBaseUrl) ? clientBaseUrl + "/admin/CreateRoutes.ashx" : null
                                }).ToList();
            }
            catch (System.Exception ex)
            {
                FileLogger.Log(ex.Message);
            }

            if (_clientNodes == null)
            {
                _clientNodes = new List<RepositoryCatalogPathNode>();
            }

            _dataCopyLogic = new DataCopyLogic(masterNode, _clientNodes);
            _masterNode = masterNode;
        }

        public ActionResult IndexM()
        {
            var cmsViewModels = new List<ManagementViewModel>();

            if (Session["Languages"] == null)
            {
                LoadLanguageSessionData();
            }

            if (_masterNode == null)
            {
                return View("IndexEmpty");
            }

            var viewData1 =
                new ManagementViewModel(new CatalogTreeUtility(_masterGroupRepository).Top2LevelGroups(_languageGuid), 0,
                                        _masterNode.Description);
            cmsViewModels.Add(viewData1);

            int i = 1;

            foreach (RepositoryCatalogPathNode repositoryCatalogPathNode in _clientNodes)
            {
                ManagementViewModel viewData;
                try
                {
                    viewData = new ManagementViewModel(
                        new CatalogTreeUtility(repositoryCatalogPathNode.Repository).Top2LevelGroups(_languageGuid), i,
                        repositoryCatalogPathNode.Description);
                }
                catch (Exception ex)
                {
                    viewData = new ManagementViewModel(new Group(), i, repositoryCatalogPathNode.Description);
                    viewData.ErrorMessage = ex.Message;
                }
                cmsViewModels.Add(viewData);
                i++;
            }

            return View(cmsViewModels);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public string CopyGroup(string groupGuid, string parentGuid, string siblingGuid, string from, string to)
        {
            return _dataCopyLogic.CopyGroup(groupGuid, parentGuid, siblingGuid, from, to, _languageGuid);
        }

        ///<summary>
        ///</summary>
        ///<param name="groupGuid"></param>
        ///<param name="dbIndex"></param>
        ///<returns></returns>
        [AcceptVerbs(HttpVerbs.Post)]
        [ValidateInput(false)]
        public string LoadSubGroups(string groupGuid, string dbIndex)
        {
            int index = _dataCopyLogic.ParseInt(dbIndex);
            int iGroupGuid = _dataCopyLogic.ParseInt(groupGuid);

            IGroupRepository repository = index > 0 ? _clientNodes[index - 1].Repository : _masterGroupRepository;

            Group g = new CatalogTreeUtility(repository).SubGroups(_languageGuid, iGroupGuid);

            // Reuse the TreeRenderHelper from Ajax call
            var h = new HtmlHelper(
                new ViewContext(ControllerContext,
                                new WebFormView(ControllerContext, "bull"), new ViewDataDictionary(), new TempDataDictionary(),
                                new HtmlTextWriter(new StringWriter())), new ViewPage()
                );

            string html = h.RenderTree(g.Children, group1 => group1, 1, dbIndex);
            return html;
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [ValidateInput(false)]
        public string GetUpdateRouteUrl(string to)
        {
            int id = int.Parse(to);
            return _clientNodes[id - 1].BaseUrl;
        }
    }
}