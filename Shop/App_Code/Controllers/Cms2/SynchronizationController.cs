﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Caching;
using System.Web.Hosting;
using System.Web.Mvc;
using System.Web.Routing;
using CmsPublic.DataProviders.Logging;
using EISCS.Catalog;
using EISCS.CMS.CentralManagement.Configuration.Util;
using EISCS.CMS.Configuration;
using EISCS.CMS.ControllerCommon;
using EISCS.CMS.Services.FileService;
using EISCS.CMS.Services.ResourceHandling;
using EISCS.CMS.Services.Synchronization;
using EISCS.ExpandITFramework.Util;
using EISCS.Routing.Providers;
using EISCS.Shop.Attributes;
using EISCS.Shop.DO.Interface;
using Resources;
using ViewModels;

namespace Controllers.Cms2
{
    [BusinessCenterUserAccess(AccessClass = "BusinessCenter")]
    public class SynchronizationController : CmsBaseController
    {
        private readonly ISynchronizationService _synchronizationService;
        private readonly SelectListStore _selectListStore = new SelectListStore();
        private readonly SyncTask _syncTask;
        private IConfigurationRepository _configurationRepository;
        private bool _isLoggingEnabled;
        private readonly string _plainSyncFolderPath;
        private readonly string _escapedSyncFolderPath;
        private readonly JobCollection _collection;

        public SynchronizationController(IHttpRuntimeWrapper httpRuntimeWrapper, ISynchronizationService synchronizationService)
            : base(httpRuntimeWrapper)
        {
            _synchronizationService = synchronizationService;
            _plainSyncFolderPath = CmsSyncTaskConfigSection.Section.SyncFolder;
            _escapedSyncFolderPath = string.Format("/{0}/", _plainSyncFolderPath);
            _collection = new JobConfiguration().ConfigurationToObjects();
            _syncTask = new SyncTask(HttpContext);
        }

        /// <summary>
        /// Initialize
        /// </summary>
        /// <param name="requestContext"></param>
        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);
            _isLoggingEnabled = false;

            // Find if cms is master
            ViewData["IsMaster"] = ConfigUtil.IsMasterCms();
            // Get sync list
            ViewData["SyncList"] = _selectListStore.GetJobSelectList("");
            // Get Route list
            ViewData["RouteList"] = _selectListStore.GetRouteSelectList("");
        }

        public ActionResult Index()
        {
            var languageGuid = (string)Session["CurrentLanguage"];
            var viewData =
                new CmsViewModel(new CatalogTreeUtility(null).Top2LevelGroups(languageGuid),
                                 null,
                                 null,
                                 null,
                                 null,
                                 null,
                                 null);
            return View(viewData);
        }

        public string SyncActionName()
        {
            string returnVal = "";
            try
            {
                var ssn = (SyncStatusNode)HttpContext.Cache["CurrentJob"];

                var parts = ssn.ActionFileName.Split('.');

                var actionFileName = "";
                if (parts.Length > 0)
                {
                    actionFileName = parts[0];
                }
                
                var jobItem = _collection.Job.FirstOrDefault(x => x.FileName == actionFileName);
                
                if (jobItem != null && !string.IsNullOrWhiteSpace(jobItem.MethodName))
                {
                    string jobSearchString = jobItem.MethodName;
                    var languageGuid = (string)Session["PageLanguageGuid"];
                    returnVal = ResourceManager.GetTranslatedValue(jobSearchString, languageGuid);
                }
                else
                {
                    returnVal = ssn.ActionFileName;
                }
            }
            catch (Exception ex)
            {
                if (_isLoggingEnabled)
                {
                    FileLogger.Log(ex.Message);
                }
            }
            return returnVal;
        }

        // Start sync tasks by writing files.
        ///<summary>
        ///</summary>
        ///<param name="actionCommand"></param>
        ///<param name="jobId"></param>
        ///<returns></returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public string SynchronizeShop(string actionCommand, string jobId)
        {
            string returnVal = "fail";
            string loweredCommand = actionCommand.ToLower();
            if (loweredCommand.Contains("upload") || loweredCommand.Contains("download") ||
                loweredCommand.Contains("full"))
            {
                _configurationRepository = DependencyResolver.Current.GetService<IConfigurationRepository>();
                string isFirstRun = _configurationRepository.ReadFirstRunValue();
                if (string.Equals(isFirstRun, "True"))
                {
                    var languageGuid = (string)Session["PageLanguageGuid"];
                    return ResourceManager.GetTranslatedValue("CATMAN_SYNC_FIRST_RUN_WARNING", languageGuid);
                }
            }
            SelectList sl = _selectListStore.GetJobSelectList("");
            string jobType;
            if (!_syncTask.FindFiles(sl, out jobType))
            {
                var jobItem = _collection.Job.FirstOrDefault(x => x.FileName == actionCommand);
                string executablePath = "";
                if (jobItem != null)
                {
                    executablePath = jobItem.ExecutablePath;
                }
                //
                var fh = new FileHandler();
                string actionFileName = actionCommand + ".job";
                string errorMessage;
                if (fh.CreateFile(_plainSyncFolderPath, actionFileName, executablePath, out errorMessage))
                {
                    returnVal = "success";
                    // A job is started, keep the name in the cache   
                    HttpContext.Cache.Insert(jobId, new SyncStatusNode(actionFileName, jobId, "started"),
                                             new CacheDependency(
                                                 HostingEnvironment.MapPath("~" + _escapedSyncFolderPath + actionFileName)));
                    HttpContext.Cache.Insert("CurrentJob", new SyncStatusNode(actionFileName, jobId, "started"),
                                             new CacheDependency(
                                                 HostingEnvironment.MapPath("~" + _escapedSyncFolderPath + actionFileName)));
                    // Assign a cookie to the starter
                    HttpContext.Response.Cookies.Add(new HttpCookie("jobId", jobId));
                }
                else if (!string.IsNullOrEmpty(errorMessage))
                {
                    returnVal = errorMessage;
                }
            }
            if (!string.IsNullOrWhiteSpace(jobType))
            {
                if (jobType == ".job.error")
                {
                    returnVal = "ERROR";
                }
            }
            return returnVal;
        }

        // Poll on all sync files
        ///<summary>
        ///</summary>
        ///<returns></returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public string SynchronizePoll()
        {
            string returnValue = "";
            var status = _synchronizationService.Poll();
            if (status == "3" || status.Length > 2)
            {
                returnValue = CmsLanguage.CATMAN_LABEL_SYNC_FAILED + "<br/> " + status;
            }
            else
            {
                returnValue = status;
            }
            return returnValue;
        }

        ///<summary>
        ///</summary>
        ///<param name="actionCommand"></param>
        ///<param name="guid"></param>
        ///<returns></returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public bool ResetSync(string actionCommand, string guid)
        {
            SelectList sl = _selectListStore.GetJobSelectList("");
            return _syncTask.DeleteSyncFiles(sl);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public bool CreateRoutes(string actionCommand)
        {
            bool isSuccess = true;
            try
            {
                RouteManager.Route.PublicRouteDataProvider.RecreateAll();
            }
            catch (Exception ex)
            {
                FileLogger.Log(ex.Message + ex.StackTrace);
                isSuccess = false;
            }
            return isSuccess;
        }

        ///<summary>
        ///</summary>
        ///<returns></returns>
        [AcceptVerbs(HttpVerbs.Get)]
        public string CreateRoutes()
        {
            string result = "OK\r\n";
            try
            {
                int iResult = RouteManager.Route.PublicRouteDataProvider.RecreateAll();
                if (iResult <= 0)
                {
                    result = "No records updated";
                }
                else
                {
                    result = result + "Updated " + iResult + " records";
                }
            }
            catch (Exception ex)
            {
                result = "Update failed " + ex.Message;
            }
            return result;
        }
    }
}