﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading;
using System.Web.Mvc;
using System.Web.Routing;
using CmsPublic.ModelLayer.Models;
using EISCS.CMS.ControllerCommon;
using EISCS.CMS.Services.AppSettings;
using EISCS.ExpandITFramework.Util;
using EISCS.Shop.DO.Interface;
using EISCS.Shop.DO.Repository;

namespace Controllers.Cms2
{
    public class CmsBaseController : Controller
    {
        protected readonly IHttpRuntimeWrapper HttpRuntimeWrapper;
        private readonly ILanguageRepository _languageRepository = DependencyResolver.Current.GetService<ILanguageRepository>();
        private readonly SelectListStore _selectListStore = new SelectListStore();
        protected string _languageGuid;

        public CmsBaseController(IHttpRuntimeWrapper httpRuntimeWrapper)
        {
            HttpRuntimeWrapper = httpRuntimeWrapper;
        }

        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);
            
            if (Session["Languages"] == null)
            {
                LoadLanguageSessionData();
            }
            if (Session["CurrentLanguage"] == null)
            {
                Session["CurrentLanguage"] = AppSettingsReader.Read("LANGUAGE_DEFAULT");
            }
            SetUiCulture();
            _languageGuid = (string)Session["CurrentLanguage"];
        }

        protected void LoadLanguageSessionData()
        {
            LanguageLoader.LoadLanguageSessionData(_languageRepository, Request, Session, _selectListStore, Server);
        }

        /// <summary>
        /// Set culture of current thread to session language
        /// </summary>
        private void SetUiCulture()
        {
            var languageGuid = (string)Session["PageLanguageGuid"];
            var ci = new CultureInfo(languageGuid);
            Thread.CurrentThread.CurrentUICulture = ci;
        }

        protected string VirtualRoot()
        {
            return HttpRuntimeWrapper.AppDomainAppVirtualPath.Equals("/")
                ? "/cms"
                : HttpRuntimeWrapper.AppDomainAppVirtualPath + "/cms";
        }

        protected string VirtualShopRoot()
        {
            return HttpRuntimeWrapper.AppDomainAppVirtualPath.Equals("/") ? "" : HttpRuntimeWrapper.AppDomainAppVirtualPath;
        }

        protected virtual List<Product> SetMissingPropertiesForDataTables(List<Product> products)
        {
            var checkbox = "<input type='checkbox' name='ProductGuid' class='chk' />";
            var button = "<a href=\"" + VirtualShopRoot() + "/cms/Catalog/Edit?productGuid=ProductGuid\" class='btn btn-primary'><i class='fa fa-edit'></i>&nbsp;Edit</a>";

            foreach (var product in products)
            {
                if (product.ProductName == null)
                    product.ProductName = "";
                if (product.ProductNameTranslate == null)
                    product.ProductNameTranslate = "";
                if (product.ProductGrp == null)
                    product.ProductGrp = "";

                product.CheckboxHtml = checkbox.Replace("ProductGuid", product.GroupProductGuid != 0 ? Convert.ToString(product.GroupProductGuid) : product.ProductGuid);

                var pictureString = "<div class='square-picture-centered'><img src='" + VirtualShopRoot() + "/Content/images/unknown.png' alt='ProductGuid' class='max-height-60' /></div>";

                if (product.Properties != null && product.Properties.PropDict.ContainsKey("PICTURE1") && product.Properties.PropDict["PICTURE1"] != null)
                {
                    var picture = product.Properties.PropDict["PICTURE1"];
                    if (picture.StartsWith("/"))
                    {
                        picture = picture.Substring(1);
                    }
                    pictureString = pictureString.Replace("Content/images/unknown.png", picture);
                }
                product.PictureHtml = pictureString.Replace("ProductGuid", product.ProductGuid);
                product.EditButtonHtml = button.Replace("ProductGuid", product.ProductGuid);
            }

            return products;
        }

        protected virtual JQueryDataTableParamModel SetDataTableParamsFromRequest(JQueryDataTableParamModel param)
        {
            param.searchValue = Request["search[value]"];
            if (param.start != 0)
            {
                param.start++;
                param.length--;
            }
            var columnNumber = Convert.ToInt32(Request["order[0][column]"]);

            var theRequestKey = @"columns[" + columnNumber + "][data]";
            var strColumnName = Request[theRequestKey];
            switch (strColumnName)
            {
                case "CheckboxHtml":
                case "PictureHtml":
                case "EditButtonHtml":
                    break;
                default:
                    param.columnBeingSort = strColumnName;
                    break;
            }
            param.sortDir = Request["order[0][dir]"];
            return param;
        }

        public class JQueryDataTableParamModel
        {
            public int draw { get; set; }
            public int length { get; set; }
            public int start { get; set; }
            public string searchValue { get; set; }
            public string columnBeingSort { get; set; }
            public string sortDir { get; set; }
            public int groupGuid { get; set; }
        }

    }
}