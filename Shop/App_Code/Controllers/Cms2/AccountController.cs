﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using EISCS.CMS.Account;
using EISCS.ExpandITFramework.Util;
using EISCS.Shop.Attributes;
using EISCS.Shop.DO.Dto;
using EISCS.Shop.DO.Interface;
using EISCS.Wrappers.Configuration;
using FormModels;
using Resources;
using ViewModels;

namespace Controllers.Cms2
{
    public class AccountController : CmsBaseController
    {
        private readonly IBusinessCenterAccountManager _accountManager;
        private readonly IUserManager _userManager;
        private readonly IConfigurationObject _configurationObject;

        public AccountController(IHttpRuntimeWrapper httpRuntimeWrapper, IBusinessCenterAccountManager accountAccountManager, IUserManager userManager, IConfigurationObject configurationObject)
            : base(httpRuntimeWrapper)
        {
            _accountManager = accountAccountManager;
            _userManager = userManager;
            _configurationObject = configurationObject;
        }

        public ActionResult Login(string retUrl)
        {
            ViewBag.ReturnUrl = retUrl;
            ViewData["MEMBERS"] = 1;
            LoginModel model = new LoginModel { ReturnUrl = retUrl };
            return View(model);
        }

        [HttpPost]
        public ActionResult Login(LoginModel user, string returnUrl)
        {
            ViewData["MEMBERS"] = 1;

            if (_accountManager.Login(user.UserName, user.Password))
            {
                if (string.IsNullOrWhiteSpace(returnUrl))
                {
                    return RedirectToAction("Index", "BusinessCenter");
                }
                return RedirectToLocal(returnUrl);
            }

            return View();
        }

        public ActionResult Logout()
        {
            _accountManager.LogOut();
            return RedirectToAction("Login");
        }

        public ActionResult ChangePassword()
        {
            var model = new UserPasswordFormModel { Success = false };
            return View(model);
        }

        [HttpPost]
        public ActionResult ChangePassword(UserPasswordFormModel formModel)
        {
            if (ModelState.IsValid)
            {
                var user = _accountManager.Get();

                if (user != null)
                {
                    var username = user.UserLogin;
                    if (_accountManager.ChangePassword(username, formModel.CurrentUserPassword, formModel.NewUserPassword))
                    {
                        formModel.Success = true;
                        formModel.Message = Language.LABEL_SUCCESS;
                        return View(formModel);
                    }
                    formModel.Success = false;
                    ModelState.AddModelError("", Language.MESSAGE_PASSWORD_INCORRECT);
                }
            }
            return View(formModel);
        }

        [BusinessCenterUserAccess(AccessClass = "Super")]
        public ActionResult UsersList()
        {
            var members = _accountManager.GetAllUsers();
            return View(members);
        }

        [BusinessCenterUserAccess(AccessClass = "Super")]
        public ActionResult AddUser()
        {
            var newUser = new RegisterCmsUserFormModel();
            IEnumerable<RoleTable> roles = _userManager.GetAllRoles();
            newUser.RoleList = roles.ToDictionary(r => r.RoleId, r => r.RoleDescription);
            return View(newUser);
        }

        // TODO: What about role?
        [HttpPost]
        public ActionResult AddUser(RegisterCmsUserFormModel form)
        {
            if (ModelState.IsValid)
            {
                var newUser = _accountManager.CreateUser(form.UserName, form.Password, form.RoleId);
                return RedirectToAction("UsersList");
            }
            return View(form);
        }

        public ActionResult ResetPassword(string userGuid)
        {
            int length = ExpanditLib2.CLngEx(_configurationObject.Read("SEND_FORGOTTEN_PASSWORD_LEN"));
            var newPassword = _accountManager.ResetPassword(userGuid, null, length);

            return Content(newPassword);
        }

        [HttpPost]
        public ActionResult DeleteUser(string userName)
        {
            var user = _accountManager.Get();

            _accountManager.DeleteUser(userName);

            if (userName == user.UserLogin)
            {
                return RedirectToAction("Logout");
            }
            return RedirectToAction("UsersList");
        }

        [AllowAnonymous]
        public JsonResult GetUsername()
        {
            var user = _accountManager.Get();

            string username = user != null ? user.UserLogin : null;
            return Json(new
            {
                username
            });
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                var redirectResult = Redirect(returnUrl);
                return redirectResult;
            }
            return RedirectToAction("Login", "Account");
        }
    }
}