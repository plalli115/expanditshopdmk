﻿using System.Web.Mvc;
using EISCS.Catalog;
using EISCS.CMS.Models.Logic;
using EISCS.CMS.Models.ViewModels;
using EISCS.ExpandITFramework.Util;

namespace Controllers.Cms2
{
    /// <summary>
    /// Summary description for SidebarController
    /// </summary>
    public class SidebarController : CmsBaseController
    {
        private readonly GroupProductManager _groupProductManager = new GroupProductManager(null, null, null);

        public SidebarController(IHttpRuntimeWrapper httpRuntimeWrapper)
            : base(httpRuntimeWrapper)
        {
            
        }

        public ActionResult SidebarTreeView()
        {
            var language = (string)Session["CurrentLanguage"];

            var viewData =
                new CmsViewModel(new CatalogTreeUtility(null).Top2LevelGroups(language),
                    null,
                    null,
                    null,
                    null,
                    null,
                    null);
            return PartialView("Partials/_GroupsTreeView", viewData);
        }
    }
}