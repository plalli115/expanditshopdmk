﻿using System;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Text;
using System.Web.Mvc;
using EISCS.Shop.DO.Dto.Cart;
using EISCS.Shop.DO.Interface;
using EISCS.Shop.DO.Service;
using EISCS.Wrappers.Configuration;

namespace Controllers
{
    /// <summary>
    ///1. Notification Validation 
    ///   In this step, your job is to post the data that was posted to you back to PayPal. 
    ///   This ensures that the original post indeed came from PayPal, and is not sort of fraud. 
    ///   In the data that you post back, you must append the "cmd=_notify-validate" value to the POST string. 
    ///   Once the data is posted back to PayPal, PayPal will respond with a string of either "VERIFIED" or "INVALID". 
    ///   "VERIFIED" means that PayPal sent the original post, and that you should continue your automation process as part of the transaction. 
    ///   "INVALID" means that PayPal did not send the original post, and it should probably be logged and investigated for possible fraud. 
    ///
    ///2. Payment Status Check 
    ///   In this step, you should check that the "payment_status" form field is "Completed". This ensures that 
    ///   the customer's payment has been processed by PayPal, and it has been added to the seller's account. 
    ///
    ///3. Transaction Duplication Check 
    ///   In this step, you should check that the "txn_id" form field, transaction ID, has not already been processed by your
    ///   automation system. A good thing to do is to store the transaction ID in a database or file for duplication checking. 
    ///   If the transaction ID posted by PayPal is a duplicate, you should not continue your automation process for this transaction. 
    ///   Otherwise, this could result in sending the same product to a customer twice. 
    ///
    ///4. Seller Email Validation 
    ///   In this step, you simply make sure that the transaction is for your account. Your account will have specific email addresses assigned to it. 
    ///   You should verify that the "receiver_email" field has a value that corresponds to an email associated with your account. 
    ///
    ///5. Payment Validation 
    ///   As of now, this step is not listed on other sites as a requirement, but it is very important. Because any customer who is 
    ///   familiar with query strings can modify the cost of a seller's product, you should verify that the "payment_gross" field 
    ///   corresponds with the actual price of the item that the customer is purchasing. It is up to you to determine the exact price 
    ///   of the item the customer is purchasing using the form fields. Some common fields you may use to lookup the item being purchased
    ///   include "item_name" and "item_number". To see for yourself, follow these steps: 
    ///   Click on a button used to purchase one of your products. 
    ///   Locate the "payment_gross" field in the query string and change its value. 
    ///   Use the changed URL and perform a re-request, typically by hitting [ENTER] in the browser Address Bar. 
    ///   Notice the changed price for your product on the PayPal order page.
    /// </summary>
    /// <remarks>
    ///   PLEASE NOTE THAT THIS CLASS REQUIRES REMOTE HTTP ACCESS PERMISSION TO WORK.
    ///   THIS IS NOT GRANTED BY DEFAULT IN TRUST LEVEL MEDIUM. 
    ///</remarks>
    public class PayPalCallbackController : Controller
    {
        private readonly ICartService _cartService;
        private readonly IConfigurationObject _configuration;
        private readonly IShopSalesHeaderRepository _shopSalesHeaderRepository;
        private readonly IShopSalesService _shopSalesService;
        private readonly ILegacyDataService _legacyDataService;

        public PayPalCallbackController(ICartService cartService, IConfigurationObject configuration, IShopSalesHeaderRepository shopSalesHeaderRepository, IShopSalesService shopSalesService, ILegacyDataService legacyDataService)
        {
            _cartService = cartService;
            _configuration = configuration;
            _shopSalesHeaderRepository = shopSalesHeaderRepository;
            _shopSalesService = shopSalesService;
            _legacyDataService = legacyDataService;
        }
        
        public ActionResult PaymentNotification()
        {
            // Load User with value from PayPal
            string userGuid = Request["custom"];

            // Load Order belonging to the current user
            var cartHeader = _cartService.GetCartById(userGuid);

            string paymentStatus = null;
            bool isValid = ProcessPayPalInstantPaymentNotification(cartHeader, ref paymentStatus);

            if (isValid)
            {
                _shopSalesService.CartHeader2ShopSalesHeader(cartHeader, userGuid);
            }

            return View();
        }
        
        private bool ProcessPayPalInstantPaymentNotification(CartHeader cartHeader, ref string paymentStatus)
        {
            // Get the current users currencycode
            string currencyCode = cartHeader.CurrencyGuid;

            // Get CartHeader
            string cartHeaderGuid = cartHeader.HeaderGuid;

            // Get amount from cart
            string amount = Math.Round(cartHeader.TotalInclTax, 2).ToString("F", CultureInfo.CreateSpecificCulture("en-US"));

            // Get CustomerReference from cart
            string cref = cartHeader.CustomerReference;

            // get the URL to work with
            string url = _configuration.Read("PayPalUrl");
            
            // Step 1a: Modify the POST string.
            string formPostData = Encoding.ASCII.GetString(Request.BinaryRead(Request.ContentLength));
            formPostData += "&cmd=_notify-validate";

            // Step 1b: POST the data back to PayPal.
            WebClient client = new WebClient();
            client.Headers.Add("Content-Type", "application/x-www-form-urlencoded");
            byte[] postByteArray = Encoding.ASCII.GetBytes(formPostData);
            Server.ClearError();
            byte[] responseArray;
            try
            {
                responseArray = client.UploadData(url, "POST", postByteArray);
            }
            catch (Exception)
            {
                responseArray = Encoding.ASCII.GetBytes("UNVERIFIED");
            }

            string response = Encoding.ASCII.GetString(responseArray);
            
            // Step 1c: Process the response.
            switch ((response))
            {

                case "VERIFIED":
                case "UNVERIFIED":
                    // If httpRequest was sent to PayPal we end up here if they verify that the original call came from them.
                    // If httpRequest was not sent this is due to security settings in the machine web.config.
                    // Either way, we will continue and process the transaction. 

                    // Check if transaction has been processed before
                    if (IsDuplicateId(Request["txn_id"]))
                    {
                        // transaction already processed, return
                        return false;
                    }


                    if ((Request["payment_status"] != "Completed" & Request["payment_status"] != "Pending"))
                    {
                        // Payment was denied
                        PaymentError("PAYMENT DENIED", cartHeaderGuid, "PayPal");
                        return false;
                    }

                    // check customer reference
                    if (Request["invoice"] != cref)
                    {
                        PaymentError("INVALID CUSTOMER REFERENCE", cartHeaderGuid, "PayPal");
                        return false;
                    }

                    // Check email and transaction type
                    if (Request["receiver_email"] != _configuration.Read("BusinessEmail") | Request["txn_type"] != _configuration.Read("txn_type"))
                    {
                        PaymentError("WRONG RECEIVER EMAIL ADDRESS OR TXN-TYPE", cartHeaderGuid, "PayPal");
                        return false;
                    }

                    if (Request["mc_gross"] != amount)
                    {
                        // amount is incorrect - Possible fraud
                        PaymentError("WRONG AMOUNT", cartHeaderGuid, "PayPal");
                        return false;
                    }

                    if (Request["mc_currency"] != currencyCode)
                    {
                        // currency is incorrect - Possible fraud
                        PaymentError("WRONG CURRENCY", cartHeaderGuid, "PayPal");
                        return false;
                    }

                    // Continue with automation processing if all prior steps succeeded.               
                    paymentStatus = response;

                    return true;
                default:
                    // Possible fraud. Log for investigation.
                    PaymentError("CALL NOT VALIDATED BY PAYPAL", cartHeaderGuid, "PayPal");
                    return false;
            }
        }
        
        /// <summary>
        /// checking whether the current request is duplicated for the unique number of the txn_id transaction
        /// </summary>
        /// <param name="txnId">the unique transaction number</param>
        /// <returns>true if the current request has already been processed</returns>
        public bool IsDuplicateId(string txnId)
        {
            var header = _shopSalesHeaderRepository.GetShopSalesHeadersByPaymentTransactionId(txnId);

            return header.Any();
        }
        
        protected void PaymentError(string errorString, string cartHeaderGuid, string paymentType)
        {
            
            if (!string.IsNullOrEmpty(errorString))
            {
                string sql = "UPDATE CartHeader " + " SET PaymentStatus = " + ExpanditLib2.SafeString(errorString) + "," + " PaymentError = " + ExpanditLib2.SafeString(errorString) + " WHERE HeaderGuid = " + ExpanditLib2.SafeString(cartHeaderGuid);

                _legacyDataService.ExcecuteNonQueryDb(sql);

                Response.Write("Error processing payment. [" + errorString + "]");

                Response.StatusCode = 200;
                Response.End();
            }
        }
    }
}