﻿using System.Collections.Generic;
using System.Web.Mvc;
using AutoMapper;
using EISCS.Shop.BO.BAS;
using EISCS.Shop.DO.BAS.Interface;
using EISCS.Shop.DO.Dto;
using EISCS.Shop.DO.Interface;
using ViewModels.ServiceOrder;

namespace Controllers
{
    public class ArchiveController : Controller
    {
        private readonly IServiceOrderService _serviceOrderService;
        private readonly IExpanditUserService _userService;

        public ArchiveController(IServiceOrderService serviceOrderService,
                                 IExpanditUserService userService)
        {
            _serviceOrderService = serviceOrderService;
            _userService = userService;
        }

        public ActionResult Index()
        {
            UserTable currentUser = _userService.GetPortalUser();
            var serviceOrders =
                Mapper.Map<IEnumerable<ServiceOrderListViewModel>>(
                    _serviceOrderService.GetAllArchivedItemsByUserRole(currentUser));

            return View(serviceOrders);
        }
    }
}