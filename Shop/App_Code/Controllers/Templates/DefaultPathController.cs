﻿using System.Linq;
using System.Web.Mvc;
using CmsPublic.ModelLayer.Models;
using EISCS.Shop.DO.Interface;

namespace Controllers.Templates
{
    /// <summary>
    /// Summary description for DefaultPathController
    /// </summary>
    public class DefaultPathController : ExpanditTemplateController
    {
        private readonly IPropertiesDataService _propertiesDataService;

        public DefaultPathController(IPropertiesDataService propertiesDataService, IExpanditUserService expanditUserService)
            : base(expanditUserService)
        {
            _propertiesDataService = propertiesDataService;
        }

        public ActionResult Index(string id)
        {
            Group group = GetGroup(id);
            string templateGuid = group.Properties.PropDict["TEMPLATE"];

            var templateInfos = _propertiesDataService.GetAllTemplateInfo();

            // Find specific action for the empty url
            TemplateQuerySet template = templateInfos.FirstOrDefault(x => x.TemplateGuid == templateGuid);

            if (template != null)
            {
                return RedirectToAction("Index", template.TemplateController);
            }

            // Get Default action if no specific action was found
            var defaultGroupTemplate = templateInfos.FirstOrDefault(t => t.TemplateName == "Group");

            if (defaultGroupTemplate != null)
            {
                return RedirectToAction("Index", defaultGroupTemplate.TemplateController, new { id });
            }

            return null;
        }
    }
}