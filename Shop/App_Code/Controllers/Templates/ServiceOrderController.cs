﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using System.Web.Routing;
using AutoMapper;
using CmsPublic.ModelLayer.Models;
using Controllers.Templates;
using EISCS.ExpandITFramework.Util;
using EISCS.Shop.Attributes;
using EISCS.Shop.DO.BAS.Dto;
using EISCS.Shop.DO.BAS.Interface;
using EISCS.Shop.DO.BAS.PostData;
using EISCS.Shop.DO.BAS.Repository;
using EISCS.Shop.DO.Dto;
using EISCS.Shop.DO.Interface;
using FormModels.ServiceOrder;
using Hangfire.Dashboard;
using Resources;
using UIHelpers;
using ViewModels.AccountManagement;
using ViewModels.Customer;
using ViewModels.ServiceItem;
using ViewModels.ServiceOrder;
using ViewServiceInterfaces;

namespace Controllers
{
    [UserAccess(AccessClass = "Portal")]
    public class ServiceOrderController : ExpanditTemplateController
    {
        private const string RECORD_ACTION = "NEW";
        private const int BAS_CLIENT = 0;
        private const int SERVICE_ITEM_LINE_NUMBER = 10000;

        private readonly IServiceAttachmentService _serviceAttachmentService;
        private readonly IServiceOrderCommentLineRepository _serviceOrderCommentLineRepository;
        private readonly IServiceOrderPortalStatusRepository _serviceOrderPortalStatusRepository;
        private readonly IServiceOrderService _serviceOrderService;
        private readonly IServiceOrderViewService _serviceOrderViewService;
        private readonly IDepartmentRepository _departmentRepository;
        private readonly IExpanditUserService _userService;
        private readonly IServiceItemRepository _serviceItemRepository;
        private readonly IServiceItemService _serviceItemService;
        private readonly ICustomerService _customerService;
        private readonly ICustomerAddressDataService _customerAddressService;
        private readonly IServiceOrderReportService _reportService;

        public ServiceOrderController(
            IServiceOrderService serviceOrderService,
            IServiceOrderCommentLineRepository serviceOrderCommentLineRepository,
            IServiceAttachmentService serviceAttachmentService,
            IExpanditUserService userService,
            IServiceOrderPortalStatusRepository serviceOrderPortalStatusRepository,
            IServiceOrderViewService serviceOrderViewService,
            IDepartmentRepository departmentRepository, 
            IServiceItemRepository serviceItemRepository, 
            IServiceItemService serviceItemService, 
            ICustomerService customerService,
            ICustomerAddressDataService customerAddressService, 
            IServiceOrderReportService reportService)
            : base(userService)
        {
            _serviceOrderService = serviceOrderService;
            _serviceOrderCommentLineRepository = serviceOrderCommentLineRepository;
            _serviceAttachmentService = serviceAttachmentService;
            _userService = userService;
            _serviceOrderPortalStatusRepository = serviceOrderPortalStatusRepository;
            _serviceOrderViewService = serviceOrderViewService;
            _departmentRepository = departmentRepository;
            _serviceItemRepository = serviceItemRepository;
            _serviceItemService = serviceItemService;
            _customerService = customerService;
            _customerAddressService = customerAddressService;
            _reportService = reportService;
        }

        public ActionResult Index(string serviceOrderSearchType)
        {
            if (serviceOrderSearchType.IsNullOrEmpty())
                serviceOrderSearchType = "All";

            ViewBag.ServiceOrderSearchType = serviceOrderSearchType;
            
            return View(LoadServiceOrders());
        }

        public ServiceOrderViewModel LoadServiceOrders()
        {
            var userItem = _userService.GetPortalUser();
            Group group = GetGroup();

            var viewModel = new ServiceOrderViewModel
            {
                ShowFinishedOrders = false,
                CurrentUser = userItem,
                HtmlContent = @group.GetPropertyValue("HTMLDESC").NormalizeHtmlContent()
            };

            return viewModel;
        }

        public JsonResult GetServiceOrders(JQueryDataTableParamModel param, string searchType)
        {
            ServiceOrderSearchType st;
            if (!Enum.TryParse(searchType, true, out st))
            {
                st = ServiceOrderSearchType.Active;
            }
            var userItem = _userService.GetPortalUser();
            var itemViewModels = new List<ServiceOrderListViewModel>();
            
            param = SetDataTableParamsFromRequest(param);

            var serviceOrders = _serviceOrderService.GetAllItemsByUserRole(
                param.start,
                param.start + param.length,
                param.columnBeingSort,
                param.sortDir,
                param.searchValue,
                userItem,
                st
                );

            if (serviceOrders.Any())
            {
                itemViewModels = Mapper.Map<IEnumerable<ServiceOrderListViewModel>>(serviceOrders).ToList();
                var allServiceOrderTypes = _serviceOrderService.GetAllServiceOrderTypes();
                var allDepartmentItems = _departmentRepository.All();
                var statusItems = _serviceOrderPortalStatusRepository.All();
                foreach (var currentItem in itemViewModels)
                {
                    currentItem.StatusCode = ServiceOrderViewHelper.GetServiceOrderStatusTranslation(currentItem.StatusCode, "11");
                    currentItem.SelectedDepartment = allDepartmentItems.SingleOrDefault(x => x.DepartmentGuid == currentItem.DepartmentGuid);
                    currentItem.ServiceOrderTypeItem = allServiceOrderTypes.SingleOrDefault(x => x.Code == currentItem.ServiceOrderType);
                    currentItem.SelectedStatus = statusItems.SingleOrDefault(x => x.StatusGuid == currentItem.StatusCode);
                }
            }

            return Json(new
                {
                    param.draw,
                    recordsTotal = _serviceOrderService.CountAllItemsByUserRole(null, userItem, st),
                    recordsFiltered = _serviceOrderService.CountAllItemsByUserRole(param.searchValue, userItem, st),
                    data = itemViewModels
                });
        }

        private JQueryDataTableParamModel SetDataTableParamsFromRequest(JQueryDataTableParamModel param)
        {
            param.searchValue = Request["search[value]"];
            if (param.start != 0)
            {
                param.start++;
                param.length--;
            }
            var columnNumber = Convert.ToInt32(Request["order[0][column]"]);

            var theRequestKey = @"columns[" + columnNumber + "][data]";
            var strColumnName = Request[theRequestKey];
            switch (strColumnName)
            {
                default:
                    param.columnBeingSort = strColumnName;
                    break;
            }
            param.sortDir = Request["order[0][dir]"];

            return param;
        }

        public ActionResult Details(string serviceOrderGuid)
        {
            var viewModel = _serviceOrderViewService.PopulateDetails(serviceOrderGuid);
            ViewBag.UserGuid = _userService.UserGuid;
            return View(viewModel);
        }

        [HttpPost]
        public ActionResult Details(ServiceOrderDetailViewModel attachmentForm)
        {
            if (ModelState.IsValid)
            {
                var success = _serviceAttachmentService.SaveAttachment(attachmentForm.File, attachmentForm.ServiceOrderGuid, attachmentForm.AttachmentComment);
                if (success)
                {
                    return RedirectToAction("Details", "ServiceOrder", new {id = attachmentForm.ServiceOrderGuid});
                }

                SetModelStateErrors(string.Empty, Language.SO_ERROR_SAVEATTACHMENTFAILED);
                return RedirectToAction("Details", "ServiceOrder", new {serviceOrderGuid = attachmentForm.ServiceOrderGuid });
            }

            ServiceOrderDetailViewModel viewModel = _serviceOrderViewService.PopulateDetails(attachmentForm.ServiceOrderGuid);

            return View(viewModel);
        }

        public JsonResult GetServiceItems(JQueryDataTableParamModel param, string customerGuid, string addressGuid)
        {
            var user = _userService.GetPortalUser();
            param = SetDataTableParamsFromRequest(param);
            IEnumerable<ServiceItem> items = new List<ServiceItem>();
            
            if (addressGuid == "0")
                addressGuid = "";

            if (customerGuid.IsNullOrEmpty())
            {
                switch (user.RoleId)
                {
                    case "Admin":
                        items = _serviceItemRepository.GetServiceItems(param.start, param.start + param.length, param.columnBeingSort, param.sortDir, param.searchValue);
                        break;
                    case "CustomerGroup":
                        var customerGuidList = _customerService.GetCustomersForCustomerGroup(user.CustomerGroupId).Select(cg => cg.CustomerGuid);
                        items = _serviceItemRepository.GetServiceItemsByCustomerGroup(customerGuidList, param.start, param.start + param.length, param.columnBeingSort, param.sortDir,
                            param.searchValue);
                        break;
                    default:
                        items = _serviceItemRepository.GetServiceItems(param.start, param.start + param.length, param.columnBeingSort, param.sortDir, param.searchValue,
                            user.CustomerGuid, addressGuid);
                        break;
                }
            }
            else
            {
                items = _serviceItemRepository.GetServiceItems(param.start, param.start + param.length, param.columnBeingSort, param.sortDir, param.searchValue, customerGuid, addressGuid);
            }

            var viewModelList = new List<ServiceItemViewModel>();
            foreach (var item in items)
            {
                var itemModel = Mapper.Map<ServiceItemViewModel>(item);
                var company = _customerService.GetCustomer(itemModel.CustomerGuid);
                itemModel.CustomerName = company.CompanyName;
                if (itemModel.AddressGuid.IsNullOrEmpty())
                {
                    itemModel.CustomerAddress = company.Address + " " + company.Address2;
                    itemModel.CustomerAddress = itemModel.CustomerAddress.TrimEnd(' ') + ", " + company.CityName;
                } else {
					var customerAddress = _customerAddressService.CustomerDeliveryAddressById(itemModel.CustomerGuid, itemModel.AddressGuid);
                    itemModel.CustomerAddress = customerAddress.Address1 + " " + customerAddress.Address2;
                    itemModel.CustomerAddress = itemModel.CustomerAddress.TrimEnd(' ') + ", " + customerAddress.CityName;
                }
                
                viewModelList.Add(itemModel);
            }

            return Json(new {
                param.draw,
                recordsTotal = _serviceItemService.CountServiceItemsByUser(null, user, customerGuid, addressGuid),
                recordsFiltered = _serviceItemService.CountServiceItemsByUser(param.searchValue, user, customerGuid, addressGuid),
                data = viewModelList
            });
        }

        [UserAccess(AccessClass = "CreateServiceOrder")]
        public ActionResult Create(string addressGuid, string customerGuid)
        {
            ViewBag.CustomerGuid = customerGuid;
            ViewBag.AddressGuid = addressGuid;

            if (ConfigurationManager.AppSettings.Get("ENABLE_SERVICE_ITEM") == "0" && addressGuid == null && customerGuid == null)
                return RedirectToAction("CreateByCustomer");
            if (ConfigurationManager.AppSettings.Get("ENABLE_SERVICE_ITEM") == "0" && addressGuid != null)
                return RedirectToAction("NewServiceOrder", new {addressGuid});

            var form = _serviceOrderViewService.PrepareCreateFormModel();

            return View(form);
        }

        [HttpGet]
        [UserAccess(AccessClass = "CreateServiceOrder")]
        public ActionResult Edit(string serviceOrderGuid)
        {
            if (TempData["ViewData"] != null)
            {
                ViewData = (ViewDataDictionary)TempData["ViewData"];
            }

            var item = _serviceOrderService.GetById(serviceOrderGuid);
            var statusId = item.RepairStatusCode;
            
            var form = Mapper.Map<ServiceOrderItemFormModel>(item);

            form = _serviceOrderViewService.PrepareEditFormModel(form, serviceOrderGuid, statusId);
            form.IsInEditMode = _serviceOrderService.CanEditServiceOrder(serviceOrderGuid);

            return View(form);
        }

        [HttpPost]
        public ActionResult Edit(string serviceOrderGuid, ServiceOrderFormPostModel formPost)
        {
            var user = _userService.GetPortalUser();

            try
            {
                if (ModelState.IsValid)
                {
                    var editData = Mapper.Map<ServiceOrderEditData>(_serviceOrderService.GetById(serviceOrderGuid));
                    editData = Mapper.Map<ServiceOrderFormPostModel, ServiceOrderEditData>(formPost, editData);
                    Mapper.Map<ServiceOrderEditData>(formPost);
                    if (user.RoleId.Equals("Admin"))
                    {
                        editData.DepartmentGuid = formPost.SelectedDepartment;
                        editData.ResponsibleUserGuid = formPost.SelectedServiceManager;
                    }
                    editData.UserId = user.UserGuid;

                    if (!string.IsNullOrEmpty(formPost.LongDescription))
                    {
                        var commentLine = _serviceOrderCommentLineRepository.GetCommentLineByServiceOrderGuid(editData.ServiceOrderGuid) ?? new ServiceOrderCommentLineItem(formPost.LongDescription)
                        {
                            CommentLineNumber = GenerateUniqueId(_serviceOrderCommentLineRepository.GetHighestUniqueId(formPost.ServiceOrderGuid)),
                            CommentLineGuid = Guid.NewGuid().ToString(),
                            ServiceOrderNumber = formPost.ServiceOrderGuid,
                            ServiceOrderGuid = formPost.ServiceOrderGuid,
                            CommentText = formPost.LongDescription,
                            RecordAction = RECORD_ACTION,
                            BASClient = BAS_CLIENT,
                            UserGuid = user.UserGuid,
                            PortalContactName = user.ContactName,
                            ServiceItemLineNumber = SERVICE_ITEM_LINE_NUMBER,
                            BASVersion = 0
                        };
                        commentLine.CommentText = formPost.LongDescription;
                        _serviceOrderCommentLineRepository.Add(commentLine);
                    }
                        

                    bool isSuccess = _serviceOrderService.UpdateServiceOrder(editData);

                    if (!isSuccess)
                    {
                        SetModelStateErrors(string.Empty, Language.SO_ERROR_EDIT_UNKNOWNERROR);
                        TempData["ViewData"] = ViewData;
                        return RedirectToAction("Edit", new { serviceOrderGuid = formPost.ServiceOrderGuid});
                    }
                }
                else
                {
                    SetModelStateErrors(string.Empty, Language.SO_ERROR_VALIDATION);
                    TempData["ViewData"] = ViewData;
                    return RedirectToAction("Edit", new { serviceOrderGuid = formPost.ServiceOrderGuid, Id = "" });
                }

                return RedirectToAction("Details", new {serviceOrderGuid = formPost.ServiceOrderGuid});
            }
            catch (Exception e)
            {
                SetModelStateErrors(string.Empty, string.Format("{0}: {1}", Language.SO_ERROR_EDIT_UNKNOWNERROR, e.Message));
                TempData["ViewData"] = ViewData;
                return RedirectToAction("Edit", new { serviceOrderGuid = formPost.ServiceOrderGuid });
            }
        }

        public ActionResult Delete(string serviceOrderGuid)
        {
            try
            {
                _serviceOrderService.Remove(serviceOrderGuid);
                return RedirectToAction("Index");
            }
            catch
            {
                SetModelStateErrors(string.Empty, Language.SO_ERROR_DELETE_UNKOWNERROR);
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return RedirectToAction("Index");
            }
        }

        private void SetModelStateErrors(string fieldName, string errorMessage)
        {
            ModelState.AddModelError(fieldName, errorMessage);
        }

        private ServiceOrderAdditionalPostData BuildAdditionalPostData(ServiceOrderFormPostModel form)
        {
            var additonalData = new ServiceOrderAdditionalPostData
            {
                DepartmentGuid = form.SelectedDepartment,
                ResponsibleUserGuid = form.SelectedServiceManager
            };

            return additonalData;
        }

        private ServiceOrderJobPostData BuildJobData(ServiceOrderFormPostModel form)
        {
            var jobPostData = new ServiceOrderJobPostData
            {
                DepartmentId = form.SelectedDepartment,
                JobStatusId = form.SelectedStatus,
                JobTypeId = form.SelectedTask,
                ServiceManagerId = form.SelectedServiceManager,
                TechnicianId = form.SelectedTechnician
            };

            return jobPostData;
        }

        private ServiceItemPostData BuildServiceItem(ServiceOrderFormPostModel serviceOrderItemFormModel)
        {
            var postData = new ServiceItemPostData
            {
                CustomerGuid = serviceOrderItemFormModel.SelectedCustomerId,
                CustomerAddressGuid = serviceOrderItemFormModel.SelectedCustomerAddressId,
                Description = serviceOrderItemFormModel.Description,
                ItemNo = serviceOrderItemFormModel.ItemNo,
                ServiceItemNo = serviceOrderItemFormModel.ServiceItemNo
            };

            return postData;
        }

        private int GenerateUniqueId(int hightestUniqueId)
        {
            var rand = new Random();
            int randomNumber = rand.Next(0, 9999);
            return (hightestUniqueId + ((randomNumber * 9999) + 10000));
        }

        public class JQueryDataTableParamModel
        {
            public int draw { get; set; }
            public int length { get; set; }
            public int start { get; set; }
            public string searchValue { get; set; }
            public string columnBeingSort { get; set; }
            public string sortDir { get; set; }
        }

        public ActionResult NewServiceOrderFor(string serviceItem)
        {
            var item = _serviceItemRepository.GetById(serviceItem);
            var customer = _customerService.GetCustomer(item.CustomerGuid);
            var customerAddress = _customerAddressService.CustomerDeliveryAddressById(item.CustomerGuid, item.AddressGuid);
            
            var model = new ServiceOrderItemFormModel();
            model = _serviceOrderViewService.PrepareCreateFormModel();

            model.CurrentShopCustomer = customer;
            model.CurrentServiceItem = _serviceItemRepository.GetById(serviceItem);
            model.SelectedCustomerAddressId = item.AddressGuid;
            model.SelectedCustomerId = customer.CustomerGuid;
            
            if (customerAddress != null)
            {
                model.ShipToAddress = customerAddress.Address1;
                model.ShipToAddress2 = customerAddress.Address2;
                model.ShiptoContactName = customerAddress.ContactPerson;
                model.ShipToCompanyName = customerAddress.CompanyName;
                model.ShiptoEmailAddress = customerAddress.EmailAddress;
                model.ShipToCityName = customerAddress.CityName;
                model.ShipToPhoneNo = customerAddress.PhoneNo;
                model.ShipToZipCode = customerAddress.ZipCode;
            }
            else
            {
                model.ShipToAddress = customer.Address;
                model.ShipToAddress2 = customer.Address2;
                model.ShiptoContactName = customer.ContactName;
                model.ShipToCompanyName = customer.CompanyName;
                model.ShiptoEmailAddress = customer.EmailAddress;
                model.ShipToCityName = customer.CityName;
                model.ShipToZipCode = customer.ZipCode;
            }


            return View(model);
        }

        [HttpPost]
        [UserAccess(AccessClass = "CreateServiceOrder")]
        public ActionResult NewServiceOrderFor(ServiceOrderFormPostModel formPost)
        {
            var user = _userService.GetPortalUser();

            try
            {
                if (user.RoleId.Equals("Admin"))
                {
                    if (string.IsNullOrEmpty(formPost.SelectedDepartment))
                    {
                        SetModelStateErrors("SelectedDepartment", Language.SO_ERROR_MISSINGDEPARTMENT);
                    }
                    if (string.IsNullOrEmpty(formPost.SelectedCustomerId))
                    {
                        SetModelStateErrors("SelectedCustomerId", Language.SO_ERROR_MISSINGCUSTOMER);
                    }
                    if (ConfigurationManager.AppSettings.Get("ENABLE_SERVICE_ORDER_PROJECTS")=="1")
                    {
                        if (string.IsNullOrEmpty(formPost.SelectedProject) && ConfigurationManager.AppSettings.Get("ENABLE_SERVICE_ORDER_PROJECTS") == "1")
                        {
                            SetModelStateErrors("SelectedProject", Language.SO_HEADER_SELECT_PROJECT);
                        }
                    }
                    if (string.IsNullOrEmpty(formPost.SelectedTask))
                    {
                        SetModelStateErrors("SelectedTask", Language.SO_ERROR_VALIDATION_SERVICEORDER_TASK);
                    }
                    if (string.IsNullOrEmpty(formPost.SelectedCustomerAddressId) && string.IsNullOrEmpty(formPost.ShipToAddress))
                    {
                        SetModelStateErrors("SelectedCustomerAddressId", Language.SO_ERROR_MISSINGCUSTOMER);
                    }
                    if (string.IsNullOrEmpty(formPost.SelectedStatus))
                    {
                        SetModelStateErrors("SelectedStatus", Language.SO_ERROR_MISSINGSTATUS);
                    }
                }

                if (user.RoleId.Equals("CustomerGroup"))
                {
                    if (string.IsNullOrEmpty(formPost.SelectedCustomerId))
                    {
                        SetModelStateErrors("CustomerGuid", Language.SO_ERROR_MISSINGCUSTOMER);
                    }
                }

                if (string.IsNullOrEmpty(formPost.ServiceItemNo) &&
                    string.IsNullOrEmpty(formPost.ItemNo) &&
                    string.IsNullOrEmpty(formPost.CreateServiceItemDescription) &&
                    ConfigurationManager.AppSettings.Get("ENABLE_SERVICE_ITEM") == "1")
                {
                    SetModelStateErrors("ServiceItemNo", Language.SO_ERROR_VALIDATION + "ServiceItemNo");
                }

                if (ModelState.IsValid)
                {
                    ServiceOrderAdditionalPostData serviceOrderAdditionalPostData = null;
                    ServiceItemPostData serviceItemPostData = null;

                    var serviceOrderPostData = Mapper.Map<ServiceOrderPostData>(formPost);
                    serviceOrderPostData.PortalContactName = _userService.GetUser().ContactName;
                    serviceOrderPostData.UserId = _userService.GetUser().UserGuid;

                    var serviceOrderJobPostData = BuildJobData(formPost);

                    if (_userService.UserAccess("CreateServiceItem"))
                        serviceItemPostData = BuildServiceItem(formPost);

                    if (_userService.UserAccess("EditServiceOrderDepartment") && _userService.UserAccess("EditServiceOrderServiceManager"))
                        serviceOrderAdditionalPostData = BuildAdditionalPostData(formPost);

                    var strServiceOrderGuid = _serviceOrderService.CreateServiceOrder(user, serviceOrderPostData, serviceOrderJobPostData, serviceOrderAdditionalPostData, serviceItemPostData);

                    return RedirectToAction("Details", "ServiceOrder", new { serviceOrderGuid = strServiceOrderGuid });
                }

                SetModelStateErrors(string.Empty, Language.SO_ERROR_VALIDATION);
                ServiceOrderItemFormModel formModel = _serviceOrderViewService.PreparePostedFormModel(formPost);
                if (formModel.CurrentShopCustomer == null)
                {
                    var item = _serviceItemRepository.GetById(formPost.ServiceItemNo);
                    formModel.CurrentShopCustomer = _customerService.GetCustomer(item.CustomerGuid);
                    formModel.SelectedCustomerId = formModel.CurrentShopCustomer.CustomerGuid;
                }
                return View(formModel);
            }
            catch (Exception e)
            {
                SetModelStateErrors(string.Empty, string.Format("{0}: {1}", Language.SO_ERROR_CREATE_UNKNOWNERROR, e.Message));
                var formModel = _serviceOrderViewService.PreparePostedFormModel(formPost);

                if (formModel.CurrentShopCustomer == null)
                {
                    var item = _serviceItemRepository.GetById(formPost.ServiceItemNo);
                    formModel.CurrentShopCustomer = _customerService.GetCustomer(item.CustomerGuid);
                    formModel.SelectedCustomerId = formModel.CurrentShopCustomer.CustomerGuid;
                }

                return View(formModel);
            }
        }

        public ActionResult CreateByCustomer()
        {
            var user = _userService.GetUser();
            
            if (user.RoleId == "B2B")
                return RedirectToAction("CreateByCustomerAddress", "ServiceOrder", new {customerGuid = user.CustomerGuid});
   
            return View();
        }

        public ActionResult CreateByCustomerAddress(string customerGuid)
        {
            var addresses = _customerAddressService.AllByCustomerId(customerGuid);
            if (!addresses.IsNullOrEmpty())
            {
                ViewBag.CustomerGuid = customerGuid;
                var customer = _customerService.GetCustomers(0,1,"CustomerGuid", null, null, customerGuid);
                var customerViewModel = Mapper.Map<CustomerViewModel>(customer.FirstOrDefault());
                return View(customerViewModel);
            }
            
            var user = _userService.GetUser();
            return user.RoleId == "B2B" ? RedirectToAction("NewServiceOrder") : RedirectToAction("Create", new {customerGuid});
        }

        public JsonResult GetCustomers(JQueryDataTableParamModel param)
        {
            param = SetDataTableParamsFromRequest(param);
            var user = _userService.GetPortalUser();
            var customers = new List<CustomerItem>();
            switch (user.RoleId)
            {
                case "Admin":
                    customers = _customerService.GetCustomers(param.start, param.start + param.length, param.columnBeingSort, param.sortDir, param.searchValue);
                    break;
                case "CustomerGroup":
                    var customerGuidList = _customerService.GetCustomersForCustomerGroup(user.CustomerGroupId).Select(cg => cg.CustomerGuid);
                    customers = _customerService.GetCustomersByCustomerGroup(customerGuidList, param.start, param.start + param.length, param.columnBeingSort, param.sortDir, param.searchValue);
                    break;
                default:
                    customers = _customerService.GetCustomers(param.start, param.start + param.length, param.columnBeingSort, param.sortDir, param.searchValue, user.CustomerGuid);
                    break;
            }

            return Json(new
                {
                    param.draw,
                    recordsTotal = _customerService.CountCustomers(null, user),
                    recordsFiltered = _customerService.CountCustomers(param.searchValue, user),
                    data = customers
                });
        }

        public JsonResult GetAddressesForCustomer(JQueryDataTableParamModel param, string customerGuid)
        {
            param = SetDataTableParamsFromRequest(param);

            var addresses = _customerAddressService.GetAddressesForCustomer(param.start, param.start + param.length, param.columnBeingSort, param.sortDir, param.searchValue, customerGuid);

            return Json(new
                {
                    param.draw,
                    recordsTotal = _customerAddressService.CountAdressesForCustomer(null, customerGuid),
                    recordsFiltered = _customerAddressService.CountAdressesForCustomer(param.searchValue, customerGuid),
                    data = addresses
                });
        }

        public ActionResult Reports()
        {
            var group = GetGroup();
            var viewModel = new ReportViewModel
            {
                HtmlContent = @group.GetPropertyValue("HTMLDESC").NormalizeHtmlContent()
            };
            return View(viewModel);
        }

        public ActionResult Report(string reportGuid)
        {
            var report = _reportService.GetReport(reportGuid);

            if (report.PdfFileData.SequenceEqual(new byte[0]))
            {
                return RedirectToAction("Reports");
            }

            Response.Clear();
            Response.ClearHeaders();
            Response.ClearContent();
            Response.ContentType = "application/pdf";
            Response.AppendHeader("Content-Disposition", "inline;filename=data.pdf");
            Response.BufferOutput = true;
            Response.AddHeader("Content-Length", report.PdfFileData.Length.ToString());
            Response.BinaryWrite(report.PdfFileData);
            Response.End();

            return null;
        }

        public JsonResult GetReports(JQueryDataTableParamModel param)
        {
            param = SetDataTableParamsFromRequest(param);
            var reports = _reportService.GetReports(param.start, param.start + param.length, param.columnBeingSort, param.sortDir, param.searchValue);
            var completeReports = _reportService.CompleteReports(reports);
            return Json(new
            {
                param.draw,
                recordsTotal = _reportService.CountReports(null),
                recordsFiltered = _reportService.CountReports(param.searchValue),
                data = completeReports
            });
        }

        public ActionResult NewServiceOrder(string customerGuid = null, string addressGuid = null)
        {
            var user = _userService.GetUser();
            var customer = _customerService.GetCustomer(customerGuid ?? user.CustomerGuid);
            var model = _serviceOrderViewService.PrepareCreateFormModel();
            model.CurrentShopCustomer = customer;

            model.SelectedCustomerAddressId = addressGuid ?? string.Empty;

            model.SelectedCustomerId = customer.CustomerGuid;

            //if (customerAddress != null)
            //{
            //    model.ShipToAddress = customerAddress.Address1;
            //    model.ShipToAddress2 = customerAddress.Address2;
            //    model.ShiptoContactName = customerAddress.ContactPerson;
            //    model.ShipToCompanyName = customerAddress.CompanyName;
            //    model.ShiptoEmailAddress = customerAddress.EmailAddress;
            //    model.ShipToCityName = customerAddress.CityName;
            //    model.ShipToPhoneNo = customerAddress.PhoneNo;
            //    model.ShipToZipCode = customerAddress.ZipCode;
            //}
            //else
            //{
                model.ShipToAddress = customer.Address;
                model.ShipToAddress2 = customer.Address2;
                model.ShiptoContactName = customer.ContactName;
                model.ShipToCompanyName = customer.CompanyName;
                model.ShiptoEmailAddress = customer.EmailAddress;
                model.ShipToCityName = customer.CityName;
                model.ShipToZipCode = customer.ZipCode;
            //}
            return View(model);
        }

        [HttpPost]
        public ActionResult NewServiceOrder(ServiceOrderFormPostModel formPost)
        {
            var user = _userService.GetPortalUser();

            try
            {
                if (user.RoleId.Equals("Admin"))
                {
                    if (string.IsNullOrEmpty(formPost.Description))
                    {
                        SetModelStateErrors("Description", Language.SO_ERROR_SERVICEITEM_DESCRIPTION);
                    }
                    if (string.IsNullOrEmpty(formPost.SelectedDepartment))
                    {
                        SetModelStateErrors("SelectedDepartment", Language.SO_ERROR_MISSINGDEPARTMENT);
                    }
                    if (string.IsNullOrEmpty(formPost.SelectedCustomerId))
                    {
                        SetModelStateErrors("SelectedCustomerId", Language.SO_ERROR_MISSINGCUSTOMER);
                    }
                    if (ConfigurationManager.AppSettings.Get("ENABLE_SERVICE_ORDER_PROJECTS") == "1")
                    {
                        if (string.IsNullOrEmpty(formPost.SelectedProject) && ConfigurationManager.AppSettings.Get("ENABLE_SERVICE_ORDER_PROJECTS") == "1")
                        {
                            SetModelStateErrors("SelectedProject", Language.SO_HEADER_SELECT_PROJECT);
                        }
                    }
                    if (string.IsNullOrEmpty(formPost.SelectedTask))
                    {
                        SetModelStateErrors("SelectedTask", Language.SO_ERROR_VALIDATION_SERVICEORDER_TASK);
                    }
                    if (string.IsNullOrEmpty(formPost.SelectedCustomerAddressId) && string.IsNullOrEmpty(formPost.ShipToAddress))
                    {
                        SetModelStateErrors("SelectedCustomerAddressId", Language.SO_ERROR_MISSINGCUSTOMER);
                    }
                    if (string.IsNullOrEmpty(formPost.SelectedStatus))
                    {
                        SetModelStateErrors("SelectedStatus", Language.SO_ERROR_MISSINGSTATUS);
                    }
                }

                if (user.RoleId.Equals("CustomerGroup"))
                {
                    if (string.IsNullOrEmpty(formPost.SelectedCustomerId))
                    {
                        SetModelStateErrors("CustomerGuid", Language.SO_ERROR_MISSINGCUSTOMER);
                    }
                }

                if (string.IsNullOrEmpty(formPost.ServiceItemNo) &&
                    string.IsNullOrEmpty(formPost.ItemNo) &&
                    string.IsNullOrEmpty(formPost.CreateServiceItemDescription) &&
                    ConfigurationManager.AppSettings.Get("ENABLE_SERVICE_ITEM") == "1" && 
                    string.IsNullOrEmpty(formPost.Description))
                {
                    SetModelStateErrors("ServiceItemNo", Language.SO_ERROR_VALIDATION + "ServiceItemNo");
                }

                if (ModelState.IsValid)
                {
                    ServiceOrderAdditionalPostData serviceOrderAdditionalPostData = null;
                    ServiceItemPostData serviceItemPostData = null;

                    var serviceOrderPostData = Mapper.Map<ServiceOrderPostData>(formPost);
                    serviceOrderPostData.PortalContactName = _userService.GetUser().ContactName;
                    serviceOrderPostData.UserId = _userService.GetUser().UserGuid;

                    var serviceOrderJobPostData = BuildJobData(formPost);

                    if (_userService.UserAccess("CreateServiceItem"))
                        serviceItemPostData = BuildServiceItem(formPost);

                    if (_userService.UserAccess("EditServiceOrderDepartment") && _userService.UserAccess("EditServiceOrderServiceManager"))
                        serviceOrderAdditionalPostData = BuildAdditionalPostData(formPost);

                    var strServiceOrderGuid = _serviceOrderService.CreateServiceOrder(user, serviceOrderPostData, serviceOrderJobPostData, serviceOrderAdditionalPostData, serviceItemPostData);

                    return RedirectToAction("Details", "ServiceOrder", new { serviceOrderGuid = strServiceOrderGuid });
                }

                SetModelStateErrors(string.Empty, Language.SO_ERROR_VALIDATION);
                ServiceOrderItemFormModel formModel = _serviceOrderViewService.PreparePostedFormModel(formPost);
                if (formModel.CurrentShopCustomer == null)
                {
                    var item = _serviceItemRepository.GetById(formPost.ServiceItemNo);
                    formModel.CurrentShopCustomer = _customerService.GetCustomer(item.CustomerGuid);
                    formModel.SelectedCustomerId = formModel.CurrentShopCustomer.CustomerGuid;
                }
                return View(formModel);
            }
            catch (Exception e)
            {
                SetModelStateErrors(string.Empty, string.Format("{0}: {1}", Language.SO_ERROR_CREATE_UNKNOWNERROR, e.Message));
                var formModel = _serviceOrderViewService.PreparePostedFormModel(formPost);

                if (formModel.CurrentShopCustomer == null)
                {
                    var item = _serviceItemRepository.GetById(formPost.ServiceItemNo);
                    formModel.CurrentShopCustomer = _customerService.GetCustomer(item.CustomerGuid);
                    formModel.SelectedCustomerId = formModel.CurrentShopCustomer.CustomerGuid;
                }

                return View(formModel);
            }
            return RedirectToAction("Index");
        }
    }
}