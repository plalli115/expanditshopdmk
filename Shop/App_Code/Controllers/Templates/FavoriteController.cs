﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using CmsPublic.ModelLayer.Models;
using EISCS.ExpandITFramework.Util;
using EISCS.Shop.Attributes;
using EISCS.Shop.DO.Dto;
using EISCS.Shop.DO.Interface;
using EISCS.Shop.DO.Service;
using UIHelpers;
using ViewModels;

namespace Controllers.Templates
{
    [UserAccess(AccessClass = "Favorites")]
    public class FavoriteController : ExpanditTemplateController
    {
        private readonly IFavoritesRepository _favoritesRepository;
        private readonly IProductService _productService;

        public FavoriteController(IExpanditUserService userService, IFavoritesRepository favoritesRepository, IProductService productService)
            : base(userService)
        {
            _favoritesRepository = favoritesRepository;
            _productService = productService;
        }

        private FavoriteModel GetFavoriteModel()
        {
            Group group = GetGroup();

            var dataModel = new FavoriteModel { HtmlContent = @group.GetPropertyValue("HTMLDESC").NormalizeHtmlContent() };
            return dataModel;
        }

        public ActionResult Index(ListControl control)
        {

            IEnumerable<FavoriteItem> favorites = _favoritesRepository.GetFavoritesForUser(UserService.UserGuid);
            List<string> favoritesGuidList = favorites.Select(x => x.ProductGuid).ToList();


            ProductInfoNodeContainer container = _productService.GetProducts(ProductServiceHelper.CreateCartHeader(UserService), favoritesGuidList, control.FindSortColumnProductslist(), control.Direction);
            var model = new ProductsViewModel(container.ProductInfoNodes, UserService, null, control, container.Header, null);

            var favoriteModel = GetFavoriteModel();
            favoriteModel.ProductsView = model;

            return View(favoriteModel);
        }

        public ActionResult MarkAsFavorite(string productGuid)
        {
            var favorite = new FavoriteItem { ProductGuid = productGuid, UserGuid = UserService.UserGuid };
            _favoritesRepository.AddFavorite(favorite);

            return Json("ok", JsonRequestBehavior.AllowGet);
        }

        public ActionResult UnmarkAsFavorite(string productGuid)
        {
            _favoritesRepository.DeleteFavorite(UserService.UserGuid, productGuid);
            return Json("ok", JsonRequestBehavior.AllowGet);
        }
    }

    public class FavoriteModel
    {
        public ProductsViewModel ProductsView { get; set; }
        public string HtmlContent { get; set; }
    }
}