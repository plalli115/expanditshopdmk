﻿using System.Web.Mvc;
using EISCS.Shop.Attributes;
using EISCS.Shop.DO.Interface;
using ViewModels.Templates;

namespace Controllers.Templates
{
    [UserAccess(AccessClass = "HomePage")]
    public class ShopFrontPageController : ExpanditTemplateController
    {
        public ShopFrontPageController(IExpanditUserService expanditUserService)
            : base(expanditUserService)
        {
        }

        public ActionResult Index(string id)
        {
            var viewName = GetViewName();

            if (string.IsNullOrEmpty(viewName))
            {
                viewName = "FrontPage";
            }

            var group = GetGroup(id);

            var model = new ShopFrontPageViewModel(group);
            return View(viewName, model);
        }
    }
}