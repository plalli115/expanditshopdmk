﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using EISCS.ExpandITFramework.Util;
using EISCS.Shop.Attributes;
using EISCS.Shop.BO.BusinessLogic;
using EISCS.Shop.DO.Dto.Cart;
using EISCS.Shop.DO.Interface;
using EISCS.Shop.DO.Service;
using EISCS.Wrappers.Configuration;
using Resources;
using ViewModels;

namespace Controllers.Templates
{
    [UserAccess(AccessClass = "Cart")]
    [BrowserCache(PreventBrowserCaching = true)]
    public class CartController : ExpanditTemplateController
    {
        private readonly ICartService _cartService;
        private readonly IConfigurationObject _configuration;
        private readonly ICurrencyConverter _currencyConverter;
        private readonly IPaymentService _paymentService;
        private readonly ICartHeaderRepository _cartHeaderRepository;
        private readonly IProductService _productService;

        public CartController(ICartService cartService, IExpanditUserService expanditUserService, IConfigurationObject configuration, ICurrencyConverter currencyConverter, IPaymentService paymentService, ICartHeaderRepository cartHeaderRepository, IProductService productService)
            : base(expanditUserService)
        {
            _cartService = cartService;
            _configuration = configuration;
            _currencyConverter = currencyConverter;
            _paymentService = paymentService;
            _cartHeaderRepository = cartHeaderRepository;
            _productService = productService;
        }

        public ActionResult Index(string error = null)
        {
            var userGuid = UserService.UserGuid;
            var cartHeader = _cartService.GetCartById(userGuid);
            cartHeader.IsCalculated = false;
            var infoNodes = _cartService.GetProducts(cartHeader);
            // Calculate Shipping.

            cartHeader = _cartService.GetShippingHandlingPrice(userGuid);

            cartHeader.Total = cartHeader.Total + cartHeader.ShippingAmount + cartHeader.HandlingAmount;
            cartHeader.TotalInclTax = cartHeader.TotalInclTax + cartHeader.ShippingAmountInclTax + cartHeader.HandlingAmountInclTax;
            var shippingProviderName = _cartService.ShippingHandlingName(cartHeader);

            shippingProviderName = !string.IsNullOrEmpty(shippingProviderName)
                                               ? Language.ResourceManager.GetString(shippingProviderName)
                                               : Language.LABEL_NOT_SELECTED;

            _cartService.SaveCartHeader(cartHeader);

            var viewModel = new CartViewModel(cartHeader, infoNodes, UserService, shippingProviderName, _configuration, _currencyConverter, _paymentService);

            var group = GetGroup();
            var cmsEditedFields = new CartViewModel.CartCmsEditedFields { HtmlContent = @group.GetPropertyValue("HTMLDESC").NormalizeHtmlContent() };
            viewModel.CmsEditedFields = cmsEditedFields;

            if (viewModel.HasError)
            {
                ModelState.AddModelError("", Language.ERROR_THERE_WERE_ERRORS_CALC_PRICES_FOR_PRODUCTS);
                foreach (System.String k in viewModel.ErrorMessages.Keys)
                {
                    ExpandIT.Logging.FileLogger.log("model.HasError true: " + k + " => " + viewModel.ErrorMessages[k]);
                    ModelState.AddModelError(k, viewModel.ErrorMessages[k]);
                }
            }

            if (error != null)
            {
                ModelState.AddModelError("", String.Format(Language.LABEL_PRODUCT_NOT_EXIST).Replace("%1", error));
                foreach (System.String k in viewModel.ErrorMessages.Keys)
                {
                    ExpandIT.Logging.FileLogger.log("model.HasError true: " + k + " => " + viewModel.ErrorMessages[k]);
                    ModelState.AddModelError(k, viewModel.ErrorMessages[k]);
                }
            }

            return View("Index", viewModel);
        }

        [HttpPost]
        public ActionResult Index(IEnumerable<CartViewModel.ProductItem> items, string comment, string poNumber)
        {
            UpdateCartWithComments(items, comment, poNumber);
            return RedirectToAction("Index", "Cart");
        }

        [HttpPost]
        public ActionResult Purchase(IEnumerable<CartViewModel.ProductItem> items, string comment, string poNumber)
        {
            UpdateCartWithComments(items, comment, poNumber);
            return RedirectToAction("Index", "Checkout");
        }

        private void UpdateCartWithComments(IEnumerable<CartViewModel.ProductItem> items, string comment, string poNumber)
        {
            var info = Mapper.Map<IEnumerable<CartViewModel.ProductItem>, IEnumerable<CartUpdateInfo>>(items).ToList();

            var cartHeader = _cartService.Update(UserService.UserGuid, info);

            cartHeader.HeaderComment = comment;
            cartHeader.CustomerPONumber = poNumber;

            _cartService.SaveCartHeader(cartHeader);

            UpdateMiniCart(cartHeader);
        }

        [HttpPost]
        public ActionResult Add(FormCollection item)
        {
            double quantity;
            if (!double.TryParse(item["Quantity"], out quantity))
            {
                quantity = 1;
            }

            var cartHeader = _cartService.Add(UserService.UserGuid, item["ProductGuid"], item["ProductName"], quantity, "", item["VariantCode"], false);
            UpdateMiniCart(cartHeader);
            return RedirectToAction("Index", "Cart");
        }

        [HttpPost]
        public ActionResult QuickAdd(FormCollection item)
        {
            double quantity;
            if (!double.TryParse(item["Quantity"], out quantity))
            {
                quantity = 1;
            }

            var variantCode = item["VariantCode"] ?? "";
            var productGuid = item["ProductGuid"];
            var nodeContainer = _productService.GetProduct(productGuid, variantCode);

            if (nodeContainer == null)
            {
                return RedirectToAction("Index", "Cart", new { error = productGuid });
            }

            var productContainer = nodeContainer.ProductInfoNodes.FirstOrDefault();

            if (productContainer == null)
            {
                return RedirectToAction("Index", "Cart", new { error = productGuid });
            }

            if (productContainer.Prod == null)
            {
                return RedirectToAction("Index", "Cart", new { error = productGuid });
            }

            var cartHeader = _cartService.Add(UserService.UserGuid, productContainer.Prod.ProductGuid, productContainer.Prod.ProductName, quantity, "", variantCode, false);
            UpdateMiniCart(cartHeader);
            return RedirectToAction("Index", "Cart");
        }

        [HttpPost]
        public ActionResult AddAjax(FormCollection item)
        {
            double quantity;
            if (!double.TryParse(item["Quantity"], out quantity))
            {
                quantity = 1;
            }

            var cartHeader = _cartService.Add(UserService.UserGuid, item["ProductGuid"], item["ProductName"], quantity, "", item["VariantCode"], false);
            UpdateMiniCart(cartHeader);
            return RedirectToAction("_MiniCart", "Cart");
        }

        public ActionResult _MiniCart()
        {
            MiniCartItem miniCart = null;
            CartHeader cartHeader = _cartService.GetCartById(UserService.UserGuid);
            if (cartHeader != null)
            {
                _cartService.Calculate(cartHeader);
                miniCart = _cartService.UpdateMiniCart(cartHeader);
            }

            var viewModel = new ExpanditMenuViewModel(miniCart, UserService, cartHeader);

            return View(viewModel);
        }

        private void UpdateMiniCart(CartHeader header)
        {
            UserService.MiniCart = _cartService.UpdateMiniCart(header);
        }

        public ActionResult Delete(string productGuid, string variantCode)
        {
            var cartHeader = _cartService.Delete(UserService.UserGuid, productGuid, variantCode);
            UpdateMiniCart(cartHeader);
            return RedirectToAction("Index", "Cart");
        }

        public ActionResult Clear()
        {
            var cartHeader = _cartService.GetCartById(UserService.UserGuid);
            _cartService.ClearCartLines(cartHeader);
            return RedirectToAction("Index");
        }

        public void AddComment(string headerComment)
        {
            var cartHeader = _cartService.GetCartById(UserService.UserGuid);

            cartHeader.HeaderComment = headerComment;

            _cartService.SaveCartHeader(cartHeader);
        }

        public void AddCustomerPoNumber(string customerPoNumber)
        {
            var cartHeader = _cartService.GetCartById(UserService.UserGuid);

            cartHeader.CustomerPONumber = customerPoNumber;

            _cartService.SaveCartHeader(cartHeader);
        }

        public JsonResult IsProduct(string productGuid)
        {
            var productContainer = _productService.GetProduct(productGuid, "");
            var node = productContainer.ProductInfoNodes.FirstOrDefault();
            return node == null ? Json(false) : Json(node.Prod != null);
        }
    }
}