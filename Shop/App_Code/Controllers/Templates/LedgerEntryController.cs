﻿using System.Web.Mvc;
using EISCS.ExpandITFramework.Util;
using EISCS.Shop.DO.Interface;
using ViewModels.Ledger;

namespace Controllers.Templates
{
    /// <summary>
    ///     Summary description for OrderHistoryController
    /// </summary>

    public class LedgerEntryController : ExpanditTemplateController
    {
        private readonly ILedgerService _ledgerService;

        public LedgerEntryController(ILedgerService ledgerService, IExpanditUserService expanditUserService)
            : base(expanditUserService)
        {
            _ledgerService = ledgerService;
        }

        public ActionResult Index()
        {
            var ledgerEntryContainer = _ledgerService.GetLedgerEntriesByUser();
            var viewModel = new CustomerLedgerEntryViewModel { User = UserService.GetUser(), LedgerEntryContainer = ledgerEntryContainer };
            DisplayLedgerEntryViewModel model = new DisplayLedgerEntryViewModel { LedgerEntryViewModel = viewModel };
            var group = GetGroup();
            model.CmsEditedHtml = @group.GetPropertyValue("HTMLDESC").NormalizeHtmlContent();
            return View(model);
        }
    }
}