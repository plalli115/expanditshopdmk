﻿using System.Linq;
using System.Web.Mvc;
using CmsPublic.ModelLayer.Models;
using EISCS.Shop.Attributes;
using EISCS.Shop.DO.Interface;
using EISCS.Shop.DO.Service;
using UIHelpers;
using ViewModels;
using Language = Resources.Language;

namespace Controllers.Templates
{
    [UserAccess(AccessClass = "Catalog")]
    public class GroupProductController : ExpanditTemplateController
    {
        private readonly IProductService _productService;

        public GroupProductController(IProductService productService, IExpanditUserService expanditUserService)
            : base(expanditUserService)
        {
            _productService = productService;
        }

        public ActionResult Index(ListControl control)
        {
            Group group = GetGroup();

            ProductsViewModel model = LoadGroupModel(group, control);
            GenerateSortLinksForGroup(control);

            string viewName = GetViewName();

            if (model.HasError)
            {
                string errorMessage = model.ErrorMessages.Aggregate(Language.ERROR_THERE_WERE_ERRORS_CALC_PRICES_FOR_PRODUCTS + " [", (current, message) => current + (" " + message.Value + ". ")) + "]";

                ModelState.AddModelError("", errorMessage);
            }

            return View(viewName, model);
        }

        private ProductsViewModel LoadGroupModel(Group group, ListControl control)
        {
            int startIndex = control.GetStartIndex1Based();
            int endIndex = control.GetEndIndex1Based();


            ProductInfoNodeContainer infos = _productService.GetProducts(group.GroupGuid, startIndex, endIndex,
                control.FindSortColumnProductslist(), control.Direction);

            control.TotalItems = infos.TotalCountInDb;
            return new ProductsViewModel(infos.ProductInfoNodes, UserService, group, control, infos.Header, null);
        }

        private void GenerateSortLinksForGroup(ListControl control)
        {
            control.CreateLinkForGroupSort(Language.LABEL_SORT_CHOOSE, "", "");
            control.CreateLinkForGroupSort(Language.LABEL_SORT_PRODUCT_NAME_ASC, "productname", "asc");
            control.CreateLinkForGroupSort(Language.LABEL_SORT_PRODUCT_NAME_DESC, "productname", "desc");
            control.CreateLinkForGroupSort(Language.LABEL_SORT_PRICE_ASC, "price", "asc");
            control.CreateLinkForGroupSort(Language.LABEL_SORT_PRICE_DESC, "price", "desc");
        }
    }
}