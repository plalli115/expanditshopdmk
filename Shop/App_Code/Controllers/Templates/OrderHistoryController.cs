﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using EISCS.ExpandITFramework.Infrastructure;
using EISCS.ExpandITFramework.Util;
using EISCS.Shop.Attributes;
using EISCS.Shop.DO.Dto.Cart;
using EISCS.Shop.DO.Interface;
using EISCS.Wrappers.Configuration;
using ExpandIT;
using ViewModels.Order;
using ViewServices;

namespace Controllers.Templates
{
    [UserAccess(AccessClass = "Order")]
    public class OrderHistoryController : ExpanditTemplateController
    {
        private readonly IOrderService _orderService;
        private readonly IShippingHandlingProviderRepository _shippingHandlerRepository;
        private readonly IConfigurationObject _configuration;

        public OrderHistoryController(IOrderService orderService, IExpanditUserService expanditUserService, IShippingHandlingProviderRepository shippingHandlerRepository, IConfigurationObject configuration)
            : base(expanditUserService)
        {
            _orderService = orderService;
            _shippingHandlerRepository = shippingHandlerRepository;
            _configuration = configuration;
        }

        public ActionResult Index()
        {
            string userGuid = UserService.UserGuid;
            List<ShopSalesHeader> orders = _orderService.GetOrdersByUser(userGuid);
            List<OrderViewModel> viewOrders = Mapper.Map<IList<ShopSalesHeader>, List<OrderViewModel>>(orders);

            var group = GetGroup();
            OrderHistoryViewModel historyViewModel = new OrderHistoryViewModel { OrderViewList = viewOrders, CmsEditedHtml = @group.GetPropertyValue("HTMLDESC").NormalizeHtmlContent() };

            return View(historyViewModel);
        }

        public ActionResult Details(string headerGuid)
        {
            ShopSalesHeader order = _orderService.GetOrder(headerGuid);
            OrderViewModel viewOrder = Mapper.Map<ShopSalesHeader, OrderViewModel>(order);
            if (!string.IsNullOrEmpty(viewOrder.ShippingHandlingProviderGuid))
            {
                viewOrder.ShippingHandlingProvider = _shippingHandlerRepository.GetShippingHandlingProvider(viewOrder.ShippingHandlingProviderGuid);
            }

            // Make correct Vat/Total calculation for display in view
            ExpDictionary taxAmounts;
            var totalVatViewCalculator = new TotalVatViewCalculator();
            double shippingTaxPct = ExpanditLib2.ConvertToDbl(_configuration.Read("SHIPPING_TAX_PCT"));
            double total = totalVatViewCalculator.CalculateTotal(order, shippingTaxPct, out taxAmounts);

            viewOrder.Total = total;
            viewOrder.TaxAmounts = taxAmounts;

            string taxDisplayConfiguration = ExpanditLib2.CStrEx(_configuration.Read("SHOW_TAX_TYPE")).ToUpper();

            if (taxDisplayConfiguration == "CUST" && order.PricesIncludingVat)
            {
                taxDisplayConfiguration = "INCL";
            }

            if (taxDisplayConfiguration == "INCL")
            {
                viewOrder.InvoiceDiscount = viewOrder.InvoiceDiscountInclTax;
                viewOrder.HandlingAmount = viewOrder.HandlingAmountInclTax;
                viewOrder.PaymentFeeAmount = viewOrder.PaymentFeeAmountInclTax;
                viewOrder.ServiceCharge = viewOrder.ServiceChargeInclTax;
                viewOrder.ShippingAmount = viewOrder.ShippingAmountInclTax;
                viewOrder.SubTotal = viewOrder.SubTotalInclTax;
                viewOrder.PricesIncludingVat = true;

                foreach (OrderLineViewModel line in viewOrder.Lines)
                {
                    line.ListPrice = line.ListPriceInclTax;
                    line.LineDiscountAmount = line.LineDiscountAmountInclTax;
                    line.LineTotal = line.LineTotal * (1 + ExpanditLib2.ConvertToDbl(line.TaxPct) / 100);
                }
            }
            else
            {
                viewOrder.PricesIncludingVat = false;
            }

            var group = GetGroup();
            OrderHistoryViewModel historyViewModel = new OrderHistoryViewModel { OrderView = viewOrder, CmsEditedHtml = @group.GetPropertyValue("HTMLDESC").NormalizeHtmlContent() };

            return View(historyViewModel);
        }

        public ActionResult Reorder(string headerGuid)
        {
            PageMessages messages = new PageMessages();
            _orderService.Reorder(headerGuid, messages);
            if (messages.Warnings.Count > 0)
            {
                string errors = string.Join(", ", messages.Warnings);
                return RedirectToAction("Index", "Cart", new { error = errors });
            }
            return RedirectToAction("Index", "Cart");
        }

        [HttpPost]
        public JsonResult SearchByCustRef(string custRef)
        {
            string userGuid = UserService.UserGuid;
            List<ShopSalesHeader> result = _orderService.SearchByCustRef(userGuid, custRef);
            if (result != null && result.Count > 0)
            {
                return Json(result.Select(sh => new { sh.CustomerReference, sh.CreatedDate, sh.HeaderGuid }));
            }
            return Json(new object());
        }
    }
}