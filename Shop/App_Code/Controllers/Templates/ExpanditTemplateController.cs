﻿using System.Web.Mvc;
using System.Web.Routing;
using CmsPublic.ModelLayer.Models;
using EISCS.Catalog.Logic;
using EISCS.Routing.Providers;
using EISCS.Shop.DO.Interface;

namespace Controllers.Templates
{
    /// <summary>
    /// Summary description for ExpanditTemplateController
    /// </summary>
    public class ExpanditTemplateController : Controller
    {
        protected readonly IExpanditUserService UserService;
        protected RequestContext CurrentRequestContext;
        public ExpanditTemplateController(IExpanditUserService userService)
        {
            UserService = userService;
        }

        protected override void Initialize(RequestContext requestContext)
        {
            CurrentRequestContext = requestContext;
            base.Initialize(requestContext);
        }

        protected Group GetGroup()
        {
            string strGroupGuid = RouteManager.Route.GetGroupId(CurrentRequestContext);
            return GetGroup(strGroupGuid);
        }

        protected Group GetGroup(string strGroupGuid)
        {
            int groupGuid;
            if (!int.TryParse(strGroupGuid, out groupGuid))
            {
                groupGuid = 0;
            }
            var cl = new CatalogLogic();
            Group group = cl.GetGroup(groupGuid, UserService.LanguageGuid);
            return group;
        }

        protected string GetViewName()
        {
            return RouteManager.Route.GetViewName(CurrentRequestContext);
        }

        protected string GetProductId()
        {
            return RouteManager.Route.GetProductId(CurrentRequestContext);
        }
    }
}