﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using CmsPublic.DataProviders.Logging;
using CmsPublic.ModelLayer.Models;
using EISCS.ExpandITFramework.Util;
using EISCS.Shop.Attributes;
using EISCS.Shop.DO.Dto;
using EISCS.Shop.DO.Dto.Currency;
using EISCS.Shop.DO.Interface;
using EISCS.Shop.DO.PostData;
using EISCS.WebUserControlLogic.Country;
using EISCS.WebUserControlLogic.Language;
using FormModels;
using Google.Authenticator;
using Mailers;
using Mvc.Mailer;
using Language = Resources.Language;

namespace Controllers.Templates
{
    [UserAccess(AccessClass = "Profile")]
    public class ProfileController : ExpanditTemplateController
    {
        private readonly ICountryControlLogic _countryControlLogic;
        private readonly ICurrencyRepository _currencyRepository;
        private readonly IUserMailer _userMailer;
        private readonly ILanguageControlLogic _languageControlLogic;
        private readonly IUserManager _userManager;
        private readonly string _backendType = ConfigurationManager.AppSettings["BackendType"];
        private readonly string _siteDefaultCurrencyName = ConfigurationManager.AppSettings["DEFAULT_CURRENCY_NAME"];
        private readonly string _siteDefaultCurrency = ConfigurationManager.AppSettings["MULTICURRENCY_SITE_CURRENCY"];
        private readonly string _siteDefaultLanguage = ConfigurationManager.AppSettings["LANGUAGE_DEFAULT"];

        public ProfileController(ICountryControlLogic countryControlLogic,
            ILanguageControlLogic languageControlLogic,
            ICurrencyRepository currencyRepository,
            IUserManager userManager,
            IExpanditUserService expanditUserService,
            IUserMailer userMailer)
            : base(expanditUserService)
        {
            _countryControlLogic = countryControlLogic;
            _languageControlLogic = languageControlLogic;
            _currencyRepository = currencyRepository;
            _userManager = userManager;
            _userMailer = userMailer;
        }

        public ActionResult Index()
        {
            Group group = GetGroup();
            var dataModel = new EditUserDataModel { HtmlContent = @group.GetPropertyValue("HTMLDESC").NormalizeHtmlContent() };

            return View(dataModel);
        }

        public ActionResult Edit()
        {
            UserTable user = UserService.GetUser();
            var formModel = Mapper.Map<UserItemFormModel>(user);

            PopulateUserFormDropdowns(formModel);

            var dataModel = GetModel(formModel);

            return View(dataModel);
        }

        private EditUserDataModel GetModel(UserItemFormModel formModel)
        {
            Group group = GetGroup();

            var dataModel = new EditUserDataModel { FormModel = formModel, HtmlContent = @group.GetPropertyValue("HTMLDESC").NormalizeHtmlContent() };
            return dataModel;
        }

        [HttpPost, ValidateInput(false)]
        [IsLocalMode]
        public ActionResult Edit(UserItemFormModel updatedUserModel)
        {
            UserTable user;
            UserItemFormModel formModel;
            EditUserDataModel dataModel;
            if (ModelState.IsValid)
            {
                var postData = Mapper.Map<UserItemPostData>(updatedUserModel);
                var userData = Mapper.Map<UserItemPostData, UserTable>(postData);
                updatedUserModel.IsSuccess = _userManager.UpdateUser(userData);
                if (updatedUserModel.IsSuccess)
                {
                    UserService.CurrencyGuid = postData.CurrencyGuid;
                    UserService.LanguageGuid = postData.LanguageGuid;

                    PopulateUserFormDropdowns(updatedUserModel);

                    TempData["updatedUserModel"] = updatedUserModel;
                    return RedirectToAction("EditResult");
                }
                ModelState.AddModelError(string.Empty, Language.MESSAGE_PASSWORD_INCORRECT);

                user = UserService.GetUser();
                formModel = Mapper.Map<UserItemFormModel>(user);

                PopulateUserFormDropdowns(formModel);

                dataModel = GetModel(formModel);

                return View(dataModel);

            }
            ModelState.AddModelError(string.Empty, Language.LABEL_STARMARKED_INPUT_IS_REQUIRED);

            user = UserService.GetUser();
            formModel = Mapper.Map<UserItemFormModel>(user);

            PopulateUserFormDropdowns(formModel);

            dataModel = GetModel(formModel);

            return View(dataModel);
        }

        public ActionResult EditResult()
        {
            var updatedUserModel = (UserItemFormModel)TempData["updatedUserModel"];

            if (updatedUserModel == null)
            {
                return RedirectToAction("Edit");
            }

            try
            {
                MvcMailMessage msg = _userMailer.UserUpdate(updatedUserModel.EmailAddress, updatedUserModel.ContactName, updatedUserModel.UserLogin);
                msg.Send();
                ViewBag.MailSentMessage = Language.MESSAGE_MAIL_SEND_OK;
            }
            catch (Exception ex)
            {
                string errorMessage = ex.Message;
                if (ex.InnerException != null)
                {
                    errorMessage += " " + ex.InnerException.Message;
                }
                FileLogger.Log(errorMessage);
                ViewBag.MailSentMessage = Language.MESSAGE_MAIL_SEND_FAIL;
            }

            var dataModel = GetModel(updatedUserModel);

            return View("Edit", dataModel);
        }

        private void PopulateUserFormDropdowns(UserItemFormModel userItemFormModel)
        {
            DataRow[] countryRows = _countryControlLogic.LoadCountries().Select();
            Dictionary<string, string> countryDictionary = countryRows.ToDictionary(r => ExpanditLib2.CStrEx(r["CountryGuid"]), r => (string)r["CountryName"]);
            userItemFormModel.CountryList = countryDictionary;

            DataRow[] languageRows = _languageControlLogic.LoadLanguages(true).Select();
            Dictionary<string, string> languageDictionary = languageRows.ToDictionary(r => (string)r["LanguageGuid"], r => (string)r["LanguageName"]);
            userItemFormModel.LanguageList = languageDictionary;
            if (userItemFormModel.LanguageGuid.IsNullOrEmpty() || !userItemFormModel.LanguageList.ContainsKey(userItemFormModel.LanguageGuid))
                userItemFormModel.LanguageGuid = _siteDefaultLanguage;

            IEnumerable<CurrencyItem> currencyItems = GetValidCurrencyItems();
            userItemFormModel.CurrencyList = currencyItems.ToDictionary(c => c.Id, c => c.Name);
            if (_backendType.ToUpper() == "NAV" || _backendType.ToUpper() == "NF")
            {
                if (userItemFormModel.CurrencyList.ContainsKey(_siteDefaultCurrency))
                {
                    userItemFormModel.CurrencyList.Remove(_siteDefaultCurrency);
                }
                userItemFormModel.CurrencyList.Add("", _siteDefaultCurrencyName);
                if (userItemFormModel.CurrencyGuid == null || userItemFormModel.CurrencyGuid == _siteDefaultCurrency)
                {
                    userItemFormModel.CurrencyGuid = "";
                }
                if (!userItemFormModel.CurrencyList.ContainsKey(userItemFormModel.CurrencyGuid))
                {
                    var allCurrencies = _currencyRepository.All().ToList();
                    var theCurrency = allCurrencies.Where(x => x.Id == userItemFormModel.CurrencyGuid).Select(x => x.Name).First();
                    ModelState.AddModelError("InvalidCurrency", String.Format(Language.LABEL_INVALID_CURRENCY + ": " + theCurrency))
                        ;
                    userItemFormModel.CurrencyGuid = "";
                }
            }
        }

        private IEnumerable<CurrencyItem> GetValidCurrencyItems()
        {
            return _currencyRepository.GetValid();
        }

        public ActionResult UpdatePassword()
        {
            var userPasswordFormModel = new UserPasswordFormModel();
            return View(userPasswordFormModel);
        }

        [HttpPost, ValidateInput(false)]
        [IsLocalMode]
        public ActionResult UpdatePassword(UserPasswordFormModel updateUserPasswordFormModel)
        {
            if (ModelState.IsValid)
            {
                var postData = Mapper.Map<UserPasswordPostData>(updateUserPasswordFormModel);

                string message = Language.MESSAGE_PASSWORD_INCORRECT;

                if (updateUserPasswordFormModel.NewUserPassword == updateUserPasswordFormModel.ConfirmedNewUserPassword)
                {
                    updateUserPasswordFormModel.Success = _userManager.UpdatePassword(postData);

                    if (updateUserPasswordFormModel.Success)
                    {
                        message = Language.MESSAGE_USER_INFORMATION_UPDATED;
                        string userMail = UserService.UserEmail;
                        if (!string.IsNullOrEmpty(userMail))
                        {
                            try
                            {
                                MvcMailMessage msg = _userMailer.PasswordChange(userMail);
                                msg.Send();
                            }
                            catch (Exception ex)
                            {
                                string errorMessage = ex.Message;
                                if (ex.InnerException != null)
                                {
                                    errorMessage += " " + ex.InnerException.Message;
                                }
                                FileLogger.Log(errorMessage);
                            }
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("CurrentUserPassword", message);
                    }
                }

                updateUserPasswordFormModel.Message = message;
            }

            return View(updateUserPasswordFormModel);
        }

        public ActionResult MultiFactorOptions()
        {
            var user = UserService.GetUser();
            ViewBag.MultiFactorEnabled = user.MultiFactorAuthEnabled;
            //return View();
            var model = new QrCodeImageModel();
            if (!string.IsNullOrWhiteSpace(user.Secret))
            {
                model.GenerateSetupData("ExpandIT Shop", "ExpandIT", user.Secret);
            }
            else
            {
                user.Secret = Guid.NewGuid().ToString();
                if (_userManager.UpdateUser(user, true))
                {
                    model.GenerateSetupData("ExpandIT Shop", "ExpandIT", user.Secret);
                }
            }

            return View(model);
        }

        [HttpPost]
        public JsonResult AllowMultiFactor(bool state)
        {
            var user = UserService.GetUser();
            user.MultiFactorAuthEnabled = state;
            var result = _userManager.UpdateUser(user, true);
            return Json(new {Success = result});
        }
    }
    
    public class QrCodeImageModel
    {
        private readonly TwoFactorAuthenticator _tfa;
        public string BarcodeUrl { get; set; }
        public string ManualEntryCode { get; set; }

        public QrCodeImageModel()
        {
            _tfa = new TwoFactorAuthenticator();
        }

        public void GenerateSetupData(string appName, string appTitle, string key)
        {
            var setupInfo = _tfa.GenerateSetupCode(appName, appTitle, key, 300, 300);
            BarcodeUrl = setupInfo.QrCodeSetupImageUrl;
            ManualEntryCode = setupInfo.ManualEntryKey;
        }
    }
    
    public class EditUserDataModel
    {
        public UserItemFormModel FormModel { get; set; }
        public string HtmlContent { get; set; }
    }
}