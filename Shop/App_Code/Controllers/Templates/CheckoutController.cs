﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Threading;
using System.Web.Mvc;
using AutoMapper;
using CmsPublic.DataProviders.Logging;
using EISCS.ExpandIT;
using EISCS.ExpandITFramework.Util;
using EISCS.Shop.Attributes;
using EISCS.Shop.BO.BusinessLogic;
using EISCS.Shop.DO.Dto;
using EISCS.Shop.DO.Dto.Cart;
using EISCS.Shop.DO.Interface;
using EISCS.Shop.DO.Service;
using EISCS.WebUserControlLogic.Country;
using EISCS.Wrappers.Configuration;
using Filters;
using FormModels.ShippingAddress;
using Mailers;
using Mvc.Mailer;
using Resources;
using ViewModels;

namespace Controllers.Templates
{
    [IsLocalMode]
    [UserAccess(AccessClass = "Order")]
    [BrowserCache(PreventBrowserCaching = true)]
    public class CheckoutController : ExpanditTemplateController
    {
        private readonly IShippingAddressRepository _addressRepository;
        private readonly ICountryControlLogic _countryControlLogic;
        private readonly IShippingHandlingService _shippingHandlingService;
        private readonly ICurrencyConverter _currencyConverter;
        private readonly ICartHeaderRepository _cartHeaderRepository;
        private readonly IPaymentService _paymentService;
        private readonly ICartService _cartService;
        private readonly IConfigurationObject _configuration;
        private readonly IShopSalesService _shopSalesService;
        private readonly IUserMailer _userMailer;
        private readonly IUserManager _userManager;

        public CheckoutController(IShippingAddressRepository addressRepository, ICountryControlLogic countryControlLogic, IExpanditUserService userService, IShippingHandlingService shippingHandlingService, ICurrencyConverter currencyConverter, ICartHeaderRepository cartHeaderRepository, IPaymentService paymentService, ICartService cartService, IConfigurationObject configuration, IShopSalesService shopSalesService, IUserMailer userMailer, IUserManager userManager)
            : base(userService)
        {
            _addressRepository = addressRepository;
            _countryControlLogic = countryControlLogic;
            _shippingHandlingService = shippingHandlingService;
            _currencyConverter = currencyConverter;
            _cartHeaderRepository = cartHeaderRepository;
            _paymentService = paymentService;
            _cartService = cartService;
            _configuration = configuration;
            _shopSalesService = shopSalesService;
            _userMailer = userMailer;
            _userManager = userManager;
        }

        public ActionResult Index()
        {
            if (Request.UrlReferrer == null || !(IsGoodReferrer(Request.UrlReferrer.Segments, "Cart") || IsGoodReferrer(Request.UrlReferrer.Segments, "Checkout") || IsGoodReferrer(Request.UrlReferrer.Segments, "Accounts")))
            {
                return RedirectToAction("Index", "Cart");
            }

            var providers = ConvertCurrency(_shippingHandlingService.GetShippingHandlingProviders());
            var cartHeader = _cartService.GetShippingHandlingPrice(UserService.UserGuid);
            var shippingHandlingAmount = CurrencyFormatter.FormatAmount((cartHeader.ShippingAmount + cartHeader.HandlingAmount).ToString("R"), UserService.CurrencyGuid);
            var userType = UserService.UserType;
            var defaultPaymentProvider = _configuration.Read(userType == UserClass.B2B ? "B2B_DEFAULT_PAYMENT_PROVIDER" : "B2C_DEFAULT_PAYMENT_PROVIDER");

            var group = GetGroup();
            var checkoutViewModel = new CheckoutViewModel
                {
                    Providers = providers,
                    CartHeader = cartHeader,
                    ShippingHandlingAmount = shippingHandlingAmount,
                    DefaultPaymentProvider = defaultPaymentProvider,
                    Step = 1,
                    CmsEditedHtml = @group.GetPropertyValue("HTMLDESC").NormalizeHtmlContent()
                };

            return View(checkoutViewModel);
        }

        public ActionResult ShippingAddress()
        {
            if (Request.UrlReferrer == null || !IsGoodReferrer(Request.UrlReferrer.Segments, "Checkout"))
                return RedirectToAction("Index", "Cart");

            var cartHeader = _cartService.GetCartById(UserService.UserGuid);
            if (cartHeader.Lines.Count == 0)
                return RedirectToAction("Index", "Cart");

            // Redirect to Cart from Review if ShippingHandlingProvider == NONE
            if (IsGoodReferrer(Request.UrlReferrer.Segments, "Review") && cartHeader.ShippingHandlingProviderGuid == "NONE")
                return RedirectToAction("Index", "Cart");

            var userType = UserService.UserType;
            if (cartHeader.ContactName.IsNullOrEmpty() && userType != UserClass.Anonymous)
            {
                var user = UserService.GetUser();
                cartHeader = _cartHeaderRepository.MapBillingAddress(user);
            }

            // Redirect to Review from Shipping if ShippinghandlingProvider == NONE
            if (cartHeader.ShippingHandlingProviderGuid == "NONE" && userType != UserClass.Anonymous)
            {
                return RedirectToAction("UseThisAddress");
            }

            var countryRows = _countryControlLogic.LoadCountries().Select();
            var countryList = countryRows.ToDictionary(r => (string)r["CountryGuid"], r => (string)r["CountryName"]);

            var addresses = new List<ShippingAddress>();
            if (userType != UserClass.Anonymous)
                addresses = GetAddresses(userType);

            var b2BConfig = ReadB2BShippingAddressConfig();

            var form = Mapper.Map<CartHeader, AnonymousBillingAndShippingAddressForm>(cartHeader);
            form.CountryList = countryList;

            var group = GetGroup();

            var checkoutViewModel = new CheckoutViewModel
            {
                Addresses = Mapper.Map<List<ShippingAddressItemFormModel>>(addresses),
                UserType = userType.ToString(),
                CartHeader = cartHeader,
                CountryList = countryList,
                B2BShippingAddressConfig = b2BConfig,
                Step = 2,
                AnonymousBillingAndShippingAddressForm = form,
                CmsEditedHtml = @group.GetPropertyValue("HTMLDESC").NormalizeHtmlContent()
            };

            return View(checkoutViewModel);
        }

        [ImportModelStateTempDataFromTempData]
        public ActionResult Review()
        {
            var cartHeader = _cartService.GetCartById(UserService.UserGuid);
            if (cartHeader.Lines.Count == 0)
                return RedirectToAction("Index", "Cart");

            if (!cartHeader.PaymentStatus.IsNullOrEmpty())
            {
                ModelState.AddModelError("PAYMENT_ERROR", Language.LABEL_PAYMENT_ERROR);
                ModelState.AddModelError("DETAIL_ERROR", cartHeader.PaymentStatus);
                cartHeader.PaymentStatus = "";
                _cartHeaderRepository.Update(cartHeader);
            }
            if (Request.UrlReferrer == null || !Request.UrlReferrer.Authority.Contains("dibspayment.com"))
            {
                if (Request.UrlReferrer == null || !IsGoodReferrer(Request.UrlReferrer.Segments, "Checkout"))
                {
                    return RedirectToAction("Index", "Cart");
                }
            }

            var countryRow = _countryControlLogic.LoadCountries().Select().SingleOrDefault(c => (string)c["CountryGuid"] == cartHeader.ShipToCountryGuid);
            cartHeader.ShipToCountryName = (countryRow == default(DataRow)) ? string.Empty : (string)countryRow["CountryName"];

            var infoNodes = _cartService.GetProducts(cartHeader);
            // Calculate Shipping.
            _cartService.Calculate(cartHeader);
            cartHeader.Total = cartHeader.Total + cartHeader.ShippingAmount + cartHeader.HandlingAmount;
            cartHeader.TotalInclTax = cartHeader.TotalInclTax + cartHeader.ShippingAmountInclTax + cartHeader.HandlingAmountInclTax;
            var shippingProviderName = _cartService.ShippingHandlingName(cartHeader);

            shippingProviderName = !string.IsNullOrEmpty(shippingProviderName)
                                               ? Language.ResourceManager.GetString(shippingProviderName)
                                               : Language.LABEL_NOT_SELECTED;

            var cartViewModel = new CartViewModel(cartHeader, infoNodes, UserService, shippingProviderName, _configuration, _currencyConverter, _paymentService);
            var userCartViewModelSessionKey = string.Format("{0}-UserCart", UserService.UserGuid);
            Session[userCartViewModelSessionKey] = cartViewModel;

            var paymentOptions = _paymentService.GetPaymentTypeByProvider(cartHeader.ShippingHandlingProviderGuid, UserService.UserType.ToString());

            var defaultPaymentProvider = cartHeader.PaymentMethod ?? String.Empty;

            var group = GetGroup();

            var checkoutViewModel = new CheckoutViewModel
            {
                CartViewModel = cartViewModel,
                PaymentTypeList = paymentOptions,
                DefaultPaymentProvider = defaultPaymentProvider,
                Step = 3,
                CmsEditedHtml = @group.GetPropertyValue("HTMLDESC").NormalizeHtmlContent()
            };

            return View(checkoutViewModel);
        }

        public ActionResult ChangeShippingProvider()
        {
            if (Request.UrlReferrer == null || !(IsGoodReferrer(Request.UrlReferrer.Segments, "Cart") || IsGoodReferrer(Request.UrlReferrer.Segments, "Checkout")))
            {
                return RedirectToAction("Index", "Cart");
            }

            var providers = ConvertCurrency(_shippingHandlingService.GetShippingHandlingProviders());
            var cartHeader = _cartService.GetCartById(UserService.UserGuid);
            var shippingHandlingAmount = CurrencyFormatter.FormatAmount((cartHeader.ShippingAmount + cartHeader.HandlingAmount).ToString("R"), UserService.CurrencyGuid);
            var userType = UserService.UserType;
            var defaultPaymentProvider = _configuration.Read(userType == UserClass.B2B ? "B2B_DEFAULT_PAYMENT_PROVIDER" : "B2C_DEFAULT_PAYMENT_PROVIDER");

            var checkoutViewModel = new CheckoutViewModel
            {
                CartHeader = cartHeader,
                Providers = providers,
                ShippingHandlingAmount = shippingHandlingAmount,
                DefaultPaymentProvider = defaultPaymentProvider,
                Step = 1
            };

            return View(checkoutViewModel);
        }

        private bool IsGoodReferrer(string[] segments, string word)
        {
            return segments.Any(item => item.Trim('/').Equals(word));
        }

        public ActionResult Add()
        {
            var address = new ShippingAddress();
            var model = Mapper.Map<ShippingAddressItemFormModel>(address);

            var addresses = _addressRepository.GetAddresses(UserService.UserGuid);
            model.IsDefault = (addresses.Count == 0);

            model.AddressType = "B2C";

            var countryRows = _countryControlLogic.LoadCountries().Select();
            var countryDictionary = countryRows.ToDictionary(r => (string)r["CountryGuid"], r => (string)r["CountryName"]);
            model.CountryList = countryDictionary;
            return View(model);
        }

        [HttpPost]
        public ActionResult Add(ShippingAddressItemFormModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var shippingAddress = MapFormDataItemToShippingAddress(model);
                    shippingAddress.IsDefault = true;

                    _addressRepository.Add(shippingAddress);

                    var cartHeader = _cartHeaderRepository.UpdateShippingAddress(shippingAddress);
                    if (!_cartHeaderRepository.Update(cartHeader))
                    {
                        throw new Exception("Unknown error ocurred while creating shipping address.");
                    }
                    return RedirectToAction("ShippingAddress");
                }

                var countryRows = _countryControlLogic.LoadCountries().Select();
                var countryDictionary = countryRows.ToDictionary(r => (string)r["CountryGuid"], r => (string)r["CountryName"]);
                model.CountryList = countryDictionary;
                return View(model);
            }
            catch (Exception e)
            {
                ModelState.AddModelError(string.Empty, "Unknown error ocurred while creating shipping addresses. " + e.Message);
                var countryRows = _countryControlLogic.LoadCountries().Select();
                var countryDictionary = countryRows.ToDictionary(r => (string)r["CountryGuid"], r => (string)r["CountryName"]);
                model.CountryList = countryDictionary;
                return View(model);
            }
        }

        private ShippingAddress MapFormDataItemToShippingAddress(ShippingAddressItemFormModel model)
        {
            var countryRow = _countryControlLogic.LoadCountries().Select().SingleOrDefault(c => (string)c["CountryGuid"] == model.CountryGuid);
            var countryName = (countryRow == default(DataRow)) ? string.Empty : (string)countryRow["CountryName"];

            var shippingAddress = new ShippingAddress
            {
                Address1 = model.Address1,
                Address2 = model.Address2,
                CityName = model.CityName,
                CompanyName = model.CompanyName,
                ContactName = model.ContactName,
                CountryName = countryName,
                CountryGuid = model.CountryGuid,
                CountyName = model.CountyName,
                EmailAddress = model.EmailAddress,
                StateName = model.StateName,
                UserGuid = UserService.UserGuid,
                ZipCode = model.ZipCode,
                IsDefault = model.IsDefault,
                AddressType = model.AddressType,
                ShippingAddressGuid = model.ShippingAddressGuid
            };

            return shippingAddress;
        }

        [ImportModelStateTempDataFromTempData]
        public ActionResult EditShippingAddress(string shippingAddressGuid)
        {
            var address = _addressRepository.GetAddress(shippingAddressGuid);

            var model = Mapper.Map<ShippingAddressItemFormModel>(address);

            var countryRows = _countryControlLogic.LoadCountries().Select();
            var countryDictionary = countryRows.ToDictionary(r => (string)r["CountryGuid"], r => (string)r["CountryName"]);
            model.CountryList = countryDictionary;

            return View(model);
        }

        [ExportModelStateTempDataToTempData]
        public ActionResult SaveShippingAddress(ShippingAddressItemFormModel formModel)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return RedirectToAction("EditShippingAddress", new { shippingAddressGuid = formModel.ShippingAddressGuid });
                }
                var shippingAddress = MapFormDataItemToShippingAddress(formModel);
                _addressRepository.Update(shippingAddress);
                return RedirectToAction("ShippingAddress");
            }
            catch (Exception e)
            {
                ModelState.AddModelError(string.Empty, "Unknown error ocurred while updating shipping address. " + e.Message);
                return RedirectToAction("EditShippingAddress", new { shippingAddressGuid = formModel.ShippingAddressGuid });
            }
        }

        public ActionResult DeleteShippingAddress(FormCollection collection)
        {
            var addressGuid = collection["ShippingAddressGuid"];

            _addressRepository.Remove(addressGuid);

            return RedirectToAction("ShippingAddress");
        }

        public JsonResult UpdateShippingHandlingPrice(string providerGuid)
        {
            var providers = _shippingHandlingService.GetShippingHandlingProviders();
            var cartHeader = _cartService.GetCartById(UserService.UserGuid);
            cartHeader.ShippingHandlingProviderGuid = providerGuid;
            if (!_cartHeaderRepository.Update(cartHeader))
            {
                return null;
            }
            var i = 0;
            while (i < providers.Count && !providers[i].ShippingHandlingProviderGuid.Equals(providerGuid))
                i++;

            var provider = providers[i];

            var value = provider.ShippingHandlingProviderGuid.Equals("NONE") ? 0.0 : CalculateShippingHandlingPrice(provider.ShippingHandlingPrices, cartHeader.Total);

            if (provider.ShippingHandlingProviderGuid.Equals("NONE"))
            {
                cartHeader.HandlingAmount = 0.0;
                cartHeader.ShippingAmount = 0.0;

                if (!_cartHeaderRepository.Update(cartHeader))
                {
                    throw new Exception("Unknown error updating the Cart");
                }
            }
            var data = CurrencyFormatter.FormatAmount(_currencyConverter.ConvertCurrency(value, UserService.GetDefaultCurrency, UserService.CurrencyGuid).ToString("R"), UserService.CurrencyGuid);

            return Json(data);
        }

        private double CalculateShippingHandlingPrice(List<ShippingHandlingPrice> shippingHandlingPrices, double cartTotal)
        {
            var i = 0;
            while (i < shippingHandlingPrices.Count - 1 && cartTotal > _currencyConverter.ConvertCurrency(ExpanditLib2.ConvertToDbl(shippingHandlingPrices[i].CalculatedValue), UserService.GetDefaultCurrency, UserService.CurrencyGuid))
                i++;

            var cartHeader = _cartService.GetCartById(UserService.UserGuid);
            cartHeader.HandlingAmount = Math.Round(_currencyConverter.ConvertCurrency(shippingHandlingPrices[i].HandlingAmount, UserService.GetDefaultCurrency, UserService.CurrencyGuid), 2);
            cartHeader.ShippingAmount = Math.Round(_currencyConverter.ConvertCurrency(shippingHandlingPrices[i].ShippingAmount, UserService.GetDefaultCurrency, UserService.CurrencyGuid), 2);
            cartHeader.ShippingAmountInclTax = Math.Round(cartHeader.ShippingAmount * (1 + (ExpanditLib2.ConvertToDbl(_configuration.Read("SHIPPING_TAX_PCT"))) / 100), 2);
            cartHeader.HandlingAmountInclTax = Math.Round(cartHeader.HandlingAmount * (1 + (ExpanditLib2.ConvertToDbl(_configuration.Read("TAX_PCT"))) / 100), 2);


            if (!_cartHeaderRepository.Update(cartHeader))
            {
                throw new Exception("Unknown error updating the Cart");
            }

            return shippingHandlingPrices[i].HandlingAmount + shippingHandlingPrices[i].ShippingAmount;
        }


        public List<ShippingHandlingProviderViewModel> ConvertCurrency(List<ShippingHandlingProvider> providers)
        {
            var viewModelList = new List<ShippingHandlingProviderViewModel>(providers.Capacity);
            foreach (var provider in providers)
            {
                var viewModel = new ShippingHandlingProviderViewModel(provider)
                    {
                        ShippingHandlingPrices = new List<ShippingHandlingPricesViewModel>()
                    };

                var previousValue = -0.01;
                foreach (var price in provider.ShippingHandlingPrices)
                {
                    var priceViewModel = new ShippingHandlingPricesViewModel();

                    priceViewModel.PreviousCalculatedValue = CurrencyFormatter.FormatAmount((previousValue + 0.01).ToString("R"), UserService.CurrencyGuid);

                    priceViewModel.CalculatedValue = price.CalculatedValue.Equals(0) ? null : " - " + CurrencyFormatter.FormatAmount(_currencyConverter.ConvertCurrency(price.CalculatedValue, UserService.GetDefaultCurrency, UserService.CurrencyGuid).ToString("R"), UserService.CurrencyGuid);

                    if (priceViewModel.CalculatedValue.IsNullOrEmpty())
                        priceViewModel.PreviousCalculatedValue = WebUtility.HtmlDecode("&ge; ") + priceViewModel.PreviousCalculatedValue;

                    previousValue = _currencyConverter.ConvertCurrency(price.CalculatedValue, UserService.GetDefaultCurrency, UserService.CurrencyGuid);

                    priceViewModel.HandlingAmount = CurrencyFormatter.FormatAmount(Math.Round(_currencyConverter.ConvertCurrency(price.HandlingAmount, UserService.GetDefaultCurrency, UserService.CurrencyGuid), 2).ToString("R"), UserService.CurrencyGuid);
                    priceViewModel.ShippingAmount = CurrencyFormatter.FormatAmount(Math.Round(_currencyConverter.ConvertCurrency(price.ShippingAmount, UserService.GetDefaultCurrency, UserService.CurrencyGuid), 2).ToString("R"), UserService.CurrencyGuid);
                    viewModel.ShippingHandlingPrices.Add(priceViewModel);
                }
                viewModelList.Add(viewModel);
            }

            return viewModelList;
        }

        public void ChangeAddressToShip(string shippingAddressGuid)
        {
            _addressRepository.NewDefaultShippingAddressForUser(shippingAddressGuid, UserService.UserGuid);
            var cartHeader = SetAddressToShip(shippingAddressGuid);
            if (!_cartService.SaveCartHeader(cartHeader))
            {
                throw new Exception("There was an error with the database.");
            }
        }

        public JsonResult UpdatePaymentType(string providerGuid)
        {
            var newPaymentOptions = _paymentService.GetPaymentTypeByProvider(providerGuid, UserService.UserType.ToString());

            return Json(newPaymentOptions);
        }

        public void SelectPaymentType(string paymentType)
        {
            var cartHeader = _cartService.GetCartById(UserService.UserGuid);
            cartHeader.PaymentMethod = paymentType;
            _cartHeaderRepository.Update(cartHeader);
        }

        public ActionResult PayPalReturn()
        {
            string customerReference = Request["invoice"];
            if (string.IsNullOrEmpty(customerReference))
            {
                return RedirectToAction("Error");
            }
            var group = GetGroup();
            ShopSalesHeader shopSales = _shopSalesService.GetShopSalesHeader(customerReference);
            AcceptedViewModel acceptedViewModel = new AcceptedViewModel {Header = shopSales, CmsEditedHtml = @group.GetPropertyValue("HTMLDESC").NormalizeHtmlContent()};

            return shopSales == null ? View("Error") : View("Accepted", acceptedViewModel);
        }

        public ActionResult Accepted()
        {
            var userGuid = UserService.UserGuid;
            var paymentType = Request["PaymentType"];

            if (Request.UrlReferrer == null && paymentType != "AUTHNET" && paymentType != "DIBS")
            {
                return RedirectToAction("Index", "Cart");
            }

            if (paymentType == null)
            {
                var customerRef = Request["s_customerRef"];
                var shopSale = _shopSalesService.GetShopSalesHeader(customerRef);
                var i = 0;
                while (i < 5 && shopSale == null)
                {
                    var cart = _cartService.GetCartById(userGuid);
                    if (!cart.PaymentStatus.IsNullOrEmpty())
                        return RedirectToAction("Review");

                    Thread.Sleep(5000);
                    shopSale = _shopSalesService.GetShopSalesHeader(customerRef);
                    i++;
                }

                if (shopSale == null)
                {
                    var cart = _cartService.GetCartById(userGuid);
                    cart.PaymentStatus = "Unable to locate order on confirm page";
                    _cartHeaderRepository.Update(cart);
                    return RedirectToAction("Review");
                }
                paymentType = "DIBS";
            }

            var group = GetGroup();
            AcceptedViewModel acceptedViewModel = new AcceptedViewModel();
            acceptedViewModel.CmsEditedHtml = @group.GetPropertyValue("HTMLDESC").NormalizeHtmlContent();

            if (paymentType != "DIBS")
            {
                var cartHeader = _cartService.GetCartById(userGuid);
                var countryRow = _countryControlLogic.LoadCountries().Select().SingleOrDefault(c => (string)c["CountryGuid"] == cartHeader.ShipToCountryGuid);
                cartHeader.ShipToCountryName = (countryRow == default(DataRow)) ? string.Empty : (string)countryRow["CountryName"];

                var infoNodes = _cartService.GetProducts(cartHeader);
                // Calculate Shipping.
                _cartService.Calculate(cartHeader);
                cartHeader.Total = cartHeader.Total + cartHeader.ShippingAmount + cartHeader.HandlingAmount;
                cartHeader.TotalInclTax = cartHeader.TotalInclTax + cartHeader.ShippingAmountInclTax + cartHeader.HandlingAmountInclTax;
                var shippingProviderName = _cartService.ShippingHandlingName(cartHeader);

                shippingProviderName = !string.IsNullOrEmpty(shippingProviderName)
                    ? Language.ResourceManager.GetString(shippingProviderName)
                    : Language.LABEL_NOT_SELECTED;
                var viewModel = new CartViewModel(cartHeader, infoNodes, UserService, shippingProviderName, _configuration, _currencyConverter, _paymentService);
                
                try
                {
                    MvcMailMessage msg = _userMailer.OrderConfirmation(viewModel, cartHeader.EmailAddress);
                    msg.Send();
                    ViewBag.MailSentMessage = Language.MESSAGE_MAIL_SEND_OK;
                }
                catch (Exception ex)
                {
                    string errorMessage = ex.Message;
                    if (ex.InnerException != null)
                    {
                        errorMessage += " " + ex.InnerException.Message;
                    }
                    FileLogger.Log(errorMessage);
                    ViewBag.MailSentMessage = Language.MESSAGE_MAIL_SEND_FAIL;
                }

                var shopSales = _shopSalesService.CartHeader2ShopSalesHeader(cartHeader, UserService.UserGuid);
                if (shopSales == null)
                {
                    return RedirectToAction("Index", "Cart");
                }
                acceptedViewModel.Header = shopSales;
                return View(acceptedViewModel);
            }
            else
            {
                // This is DIBS
                var userCartViewModelSessionKey = string.Format("{0}-UserCart", UserService.UserGuid);
                CartViewModel viewModel = (CartViewModel) Session[userCartViewModelSessionKey];
                Session.Remove("userCartViewModelSessionKey");
                
                bool mailWasSent;
                try
                {
                    MvcMailMessage msg = _userMailer.OrderConfirmation(viewModel, viewModel.Billing.EmailAddress);
                    msg.Send();
                    mailWasSent = true;
                }
                catch (Exception ex)
                {
                    string errorMessage = ex.Message;
                    if (ex.InnerException != null)
                    {
                        errorMessage += " " + ex.InnerException.Message;
                    }
                    FileLogger.Log(errorMessage);
                    mailWasSent = false;
                }
                
                ViewBag.MailSentMessage = mailWasSent ? Language.MESSAGE_MAIL_SEND_OK : Language.MESSAGE_MAIL_SEND_FAIL;
                if (viewModel != null)
                {
                    acceptedViewModel.Header = _shopSalesService.GetShopSalesHeader(viewModel.DibsInfo.OrderId);
                }
                return View(acceptedViewModel);
            }

        }

        [ImportModelStateTempDataFromTempData]
        public ActionResult Error()
        {
            var cartHeader = _cartService.GetCartById(UserService.UserGuid);
            var countryRow = _countryControlLogic.LoadCountries().Select().SingleOrDefault(c => (string)c["CountryGuid"] == cartHeader.ShipToCountryGuid);
            cartHeader.ShipToCountryName = (countryRow == default(DataRow)) ? string.Empty : (string)countryRow["CountryName"];

            var infoNodes = _cartService.GetProducts(cartHeader);
            // Calculate Shipping.
            _cartService.Calculate(cartHeader);
            cartHeader.Total = cartHeader.Total + cartHeader.ShippingAmount + cartHeader.HandlingAmount;
            cartHeader.TotalInclTax = cartHeader.TotalInclTax + cartHeader.ShippingAmountInclTax + cartHeader.HandlingAmountInclTax;
            var shippingProviderName = _cartService.ShippingHandlingName(cartHeader);

            shippingProviderName = !string.IsNullOrEmpty(shippingProviderName)
                                               ? Language.ResourceManager.GetString(shippingProviderName)
                                               : Language.LABEL_NOT_SELECTED;

            var viewModel = new CartViewModel(cartHeader, infoNodes, UserService, shippingProviderName, _configuration, _currencyConverter, _paymentService);

            return View(viewModel);
        }

        public void SavePaymentMethod(string paymentMethod)
        {
            var cartHeader = _cartService.GetCartById(UserService.UserGuid);
            cartHeader.PaymentMethod = paymentMethod;
            _cartService.SaveCartHeader(cartHeader);
        }

        [HttpPost]
        public ActionResult ShippingAddress(AnonymousBillingAndShippingAddressForm form)
        {
            try
            {
                var cartHeader = _cartService.GetCartById(UserService.UserGuid);

                if (ModelState.IsValid)
                {
                    var countryRow = _countryControlLogic.LoadCountries().Select().SingleOrDefault(c => (string)c["CountryGuid"] == form.CountryGuid);
                    cartHeader.Address1 = form.Address1;
                    cartHeader.Address2 = form.Address2;
                    cartHeader.CityName = form.CityName;
                    cartHeader.ContactName = form.ContactName;
                    cartHeader.CompanyName = form.CompanyName;
                    cartHeader.CountryGuid = form.CountryGuid;
                    cartHeader.CountryName = (countryRow == default(DataRow)) ? string.Empty : (string)countryRow["CountryName"];
                    cartHeader.CountyName = form.CountyName;
                    cartHeader.EmailAddress = form.EmailAddress;
                    cartHeader.ZipCode = form.ZipCode;

                    countryRow = _countryControlLogic.LoadCountries().Select().SingleOrDefault(c => (string)c["CountryGuid"] == form.ShipToCountryGuid);
                    cartHeader.ShipToAddress1 = form.ShipToAddress1;
                    cartHeader.ShipToAddress2 = form.ShipToAddress2;
                    cartHeader.ShipToCityName = form.ShipToCityName;
                    cartHeader.ShipToContactName = form.ShipToContactName;
                    cartHeader.ShipToCompanyName = form.ShipToCompanyName;
                    cartHeader.ShipToCountryGuid = form.ShipToCountryGuid;
                    cartHeader.ShipToCountryName = (countryRow == default(DataRow)) ? string.Empty : (string)countryRow["CountryName"];
                    cartHeader.ShipToCountyName = form.ShipToCountyName;
                    cartHeader.ShipToEmailAddress = form.ShipToEmailAddress;
                    cartHeader.ShipToZipCode = form.ShipToZipCode;

                    _cartService.SaveCartHeader(cartHeader);

                    return RedirectToAction("Review");
                }
                var countryRows = _countryControlLogic.LoadCountries().Select();
                var countryList = countryRows.ToDictionary(r => (string)r["CountryGuid"], r => (string)r["CountryName"]);
                var userType = UserService.UserType;

                if (cartHeader.ContactName.IsNullOrEmpty() && userType != UserClass.Anonymous)
                {
                    var user = UserService.GetUser();
                    cartHeader = _cartHeaderRepository.MapBillingAddress(user);
                }

                var addresses = new List<ShippingAddress>();
                if (userType != UserClass.Anonymous)
                    addresses = GetAddresses(userType);

                var b2BConfig = ReadB2BShippingAddressConfig();

                form.CountryList = countryList;

                var checkoutViewModel = new CheckoutViewModel
                {
                    Addresses = Mapper.Map<List<ShippingAddressItemFormModel>>(addresses),
                    UserType = userType.ToString(),
                    CartHeader = cartHeader,
                    CountryList = countryList,
                    B2BShippingAddressConfig = b2BConfig,
                    Step = 2,
                    AnonymousBillingAndShippingAddressForm = form
                };

                return View(checkoutViewModel);
            }
            catch (Exception e)
            {
                ModelState.AddModelError(string.Empty, "Unknown error ocurred while updating shipping address. " + e.Message);
                return RedirectToAction("ShippingAddress");
            }
        }

        private ShippingAddress GetBillingAddress(UserTable user)
        {
            var shippingAddress = Mapper.Map<UserTable, ShippingAddress>(user);
            shippingAddress.AddressType = "B2C";
            shippingAddress.IsDefault = true;

            var countryRow = _countryControlLogic.LoadCountries().Select().SingleOrDefault(c => (string)c["CountryGuid"] == shippingAddress.CountryGuid);
            shippingAddress.CountryName = (countryRow == default(DataRow)) ? string.Empty : (string)countryRow["CountryName"];

            return shippingAddress;
        }

        private List<ShippingAddress> GetAddresses(UserClass userType)
        {
            var user = UserService.GetUser();
            var addresses = new List<ShippingAddress> { GetBillingAddress(user) };
            switch (userType)
            {
                case UserClass.B2C:
                    addresses.AddRange(_addressRepository.GetAddresses(UserService.UserGuid));
                    break;
                case UserClass.B2B:
                    {
                        var b2BConfig = ReadB2BShippingAddressConfig();
                        var customer = _userManager.GetCustomer(user.CustomerGuid);
                        switch (b2BConfig)
                        {
                            case B2BShippingAddressConfig.B2BUseB2BAddressOnly:
                                addresses.AddRange(_addressRepository.GetB2BAddresses(user.CustomerGuid));
                                foreach (var addr in addresses)
                                {
                                    if (addr.CountryName.IsNullOrEmpty())
                                    {
                                        var countryRow = _countryControlLogic.LoadCountries().Select().SingleOrDefault(c => (string)c["CountryGuid"] == addr.CountryGuid);
                                        addr.CountryName = (countryRow == default(DataRow)) ? string.Empty : (string)countryRow["CountryName"];
                                    }
                                    if (addr.EmailAddress.IsNullOrEmpty())
                                    {
                                        addr.EmailAddress = customer.EmailAddress;
                                    }
                                }
                                break;
                            case B2BShippingAddressConfig.B2BUseB2CAddressOnly:
                                addresses.AddRange(_addressRepository.GetAddresses(UserService.UserGuid));
                                break;
                            case B2BShippingAddressConfig.B2BUseB2BAndB2CAddress:
                                addresses.AddRange(_addressRepository.GetB2BAddresses(user.CustomerGuid));
                                foreach (var addr in addresses)
                                {
                                    if (addr.CountryName.IsNullOrEmpty())
                                    {
                                        var countryRow = _countryControlLogic.LoadCountries().Select().SingleOrDefault(c => (string)c["CountryGuid"] == addr.CountryGuid);
                                        addr.CountryName = (countryRow == default(DataRow)) ? string.Empty : (string)countryRow["CountryName"];
                                    }
                                    if (addr.EmailAddress.IsNullOrEmpty())
                                    {
                                        addr.EmailAddress = customer.EmailAddress;
                                    }
                                }
                                addresses.AddRange(_addressRepository.GetAddresses(UserService.UserGuid));
                                break;
                        }
                    }
                    break;
            }
            return addresses;
        }

        [HttpPost]
        public ActionResult UseThisAddress(FormCollection form)
        {
            var shippingAddressGuid = form["ShippingAddressGuid"];
            _addressRepository.NewDefaultShippingAddressForUser(shippingAddressGuid, UserService.UserGuid);
            CartHeader cartHeader;
            var user = UserService.GetUser();
            if (shippingAddressGuid.IsNullOrEmpty())
            {
                // Use Billing Address
                cartHeader = SetAddressToShip(user);
            }
            else
            {
                string customerGuid;
                if (!string.IsNullOrEmpty(customerGuid = form["CustomerGuid"]))
                {
                    // Is a B2BAddress
                    var shippingAddress = _addressRepository.GetB2BAddress(shippingAddressGuid, customerGuid);
                    var countryRow = _countryControlLogic.LoadCountries().Select().SingleOrDefault(c => (string)c["CountryGuid"] == shippingAddress.CountryGuid);
                    shippingAddress.CountryName = (countryRow == default(DataRow)) ? string.Empty : (string)countryRow["CountryName"];
                    var customer = _userManager.GetCustomer(user.CustomerGuid);
                    if (shippingAddress.EmailAddress.IsNullOrEmpty())
                    {
                        shippingAddress.EmailAddress = customer.EmailAddress;
                    }
                    cartHeader = SetAddressToShip(shippingAddress);
                }
                else // Is a B2C Address
                    cartHeader = SetAddressToShip(shippingAddressGuid);
            }

            _cartService.SaveCartHeader(cartHeader);

            return RedirectToAction("Review", "Checkout");
        }

        public ActionResult UseThisAddress()
        {
            var user = UserService.GetUser();
            if (user != null)
            {
                var cartHeader = SetAddressToShip(user);
                _cartService.SaveCartHeader(cartHeader);
            }

            return RedirectToAction("Review", "Checkout");
        }

        private CartHeader SetAddressToShip(string shippingAddressGuid)
        {
            return SetAddressToShip(_addressRepository.GetAddress(shippingAddressGuid));
        }

        private CartHeader SetAddressToShip(ShippingAddress address)
        {
            var cartHeader = _cartService.GetCartById(UserService.UserGuid);

            cartHeader.ShipToAddress1 = address.Address1;
            cartHeader.ShipToAddress2 = address.Address2;
            cartHeader.ShipToCityName = address.CityName;
            cartHeader.ShipToCompanyName = address.CompanyName;
            cartHeader.ShipToContactName = address.ContactName;
            cartHeader.ShipToCountryGuid = address.CountryGuid;
            cartHeader.ShipToCountryName = address.CountryName;
            cartHeader.ShipToCountyName = address.CountyName;
            cartHeader.ShipToEmailAddress = address.EmailAddress;
            cartHeader.ShipToShippingAddressGuid = address.ShippingAddressGuid;
            cartHeader.ShipToZipCode = address.ZipCode;

            return cartHeader;
        }

        private CartHeader SetAddressToShip(UserTable user)
        {
            var cartHeader = _cartService.GetCartById(UserService.UserGuid);
            var countryRow = _countryControlLogic.LoadCountries().Select().SingleOrDefault(c => (string)c["CountryGuid"] == user.CountryGuid);

            cartHeader.ShipToAddress1 = user.Address1;
            cartHeader.ShipToAddress2 = user.Address2;
            cartHeader.ShipToCityName = user.CityName;
            cartHeader.ShipToCompanyName = user.CompanyName;
            cartHeader.ShipToContactName = user.ContactName;
            cartHeader.ShipToCountryGuid = user.CountryGuid;
            cartHeader.ShipToCountryName = (countryRow == default(DataRow)) ? string.Empty : (string)countryRow["CountryName"];
            cartHeader.ShipToCountyName = user.CountyName;
            cartHeader.ShipToEmailAddress = user.EmailAddress;
            cartHeader.ShipToShippingAddressGuid = "BILLING_ADDRESS";
            cartHeader.ShipToStateName = user.StateName;
            cartHeader.ShipToZipCode = user.ZipCode;

            return cartHeader;
        }

        public B2BShippingAddressConfig ReadB2BShippingAddressConfig()
        {
            if (_configuration.Read("B2B_USE_B2B_ADDRESS") == "1")
                return _configuration.Read("B2B_USE_B2C_ADDRESS") == "1" ? B2BShippingAddressConfig.B2BUseB2BAndB2CAddress : B2BShippingAddressConfig.B2BUseB2BAddressOnly;
            return _configuration.Read("B2B_USE_B2C_ADDRESS") == "0" ? B2BShippingAddressConfig.B2BUseBillingAddressOnly : B2BShippingAddressConfig.B2BUseB2CAddressOnly;
        }

    }

    public enum B2BShippingAddressConfig
    {
        B2BUseB2BAndB2CAddress,
        B2BUseB2BAddressOnly,
        B2BUseB2CAddressOnly,
        B2BUseBillingAddressOnly
    }

    public class AcceptedViewModel
    {
        public ShopSalesHeader Header { get; set; }
        public string CmsEditedHtml { get; set; }
    }
}