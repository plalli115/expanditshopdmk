﻿using System.Web.Mvc;
using CmsPublic.ModelLayer.Models;
using EISCS.ExpandITFramework.Util;
using EISCS.Shop.Attributes;
using EISCS.Shop.DO.Dto.Cart;
using EISCS.Shop.DO.Interface;
using EISCS.Shop.DO.Service;
using EISCS.Wrappers.Configuration;
using UIHelpers;
using ViewModels;

namespace Controllers.Templates
{
    [UserAccess(AccessClass = "Catalog")]
    public class ProductSearchController : ExpanditTemplateController
    {
        private readonly IProductSearchService _productSearchService;
        private readonly IConfigurationObject _configuration;

        public ProductSearchController(IProductSearchService productSearchService, IExpanditUserService userService, IConfigurationObject configuration)
            : base(userService)
        {
            _productSearchService = productSearchService;
            _configuration = configuration;
        }

        public ActionResult Index(ListControl control)
        {
            if (!string.IsNullOrEmpty("aspxerrorpath"))
            {
                Response.StatusCode = 404;
                Response.TrySkipIisCustomErrors = true;
            }

            ProductInfoNodeContainer infos;
            ProductsViewModel model;
            if (!string.IsNullOrEmpty(control.SearchString))
            {
                var header = new CartHeader
                    {
                        UserGuid = UserService.UserGuid,
                        TaxPct = _configuration.Read("TAX_PCT"),
                        CurrencyGuid = UserService.CurrencyGuid
                    };
                infos = _productSearchService.GetResults(control.SearchString, UserService.LanguageGuid, _configuration.Read("LANGUAGE_DEFAULT"), "", header,
                    control.FindSortColumnProductslist(), control.Direction, control.GetStartIndex1Based(), control.GetEndIndex1Based());
                control.TotalItems = infos.TotalCountInDb;
                model = new ProductsViewModel(infos.ProductInfoNodes, UserService, null, control, infos.Header, null);
            }
            else
            {
                model = new ProductsViewModel(null, UserService, null, new ListControl(), null, null);
            }

            var productSearchModel = GetModel();
            productSearchModel.ProductsView = model;
            return View(productSearchModel);
        }

        private ProductSearchModel GetModel()
        {
            Group group = GetGroup();

            var dataModel = new ProductSearchModel { HtmlContent = @group.GetPropertyValue("HTMLDESC").NormalizeHtmlContent() };
            return dataModel;
        }
    }

    public class ProductSearchModel
    {
        public ProductsViewModel ProductsView { get; set; }
        public string HtmlContent { get; set; }
    }
}