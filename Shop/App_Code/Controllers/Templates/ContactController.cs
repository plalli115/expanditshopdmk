﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Configuration;
using System.Web.Mvc;
using AutoMapper;
using CmsPublic.DataProviders.Logging;
using CmsPublic.ModelLayer.Models;
using EISCS.ExpandITFramework.Util;
using EISCS.Shop.Attributes;
using EISCS.Shop.DO.Dto;
using EISCS.Shop.DO.Dto.Country;
using EISCS.Shop.DO.Interface;
using EISCS.WebUserControlLogic.Country;
using Mailers;
using Mvc.Mailer;
using ViewModels;
using ViewModels.Templates;
using Language = Resources.Language;

namespace Controllers.Templates
{
    [UserAccess(AccessClass = "HomePage")]
    public class ContactController : Controller
    {
        private readonly ICountryControlLogic _countryControlLogic;
        private readonly IExpanditUserService _expanditUserService;
        private readonly IUserMailer _userMailer;
        private readonly IGroupRepository _groupRepository;

        public ContactController(ICountryControlLogic countryControlLogic, IExpanditUserService expanditUserService, IUserMailer userMailer, IGroupRepository groupRepository)
        {
            _countryControlLogic = countryControlLogic;
            _expanditUserService = expanditUserService;
            _userMailer = userMailer;
            _groupRepository = groupRepository;
        }

        public ActionResult Index(string id)
        {
            int groupGuid;

            int.TryParse(id, out groupGuid);
            Group group = _groupRepository.GetGroup(groupGuid, _expanditUserService.LanguageGuid, true);

            var model = new ContactViewModel
                {
                    Title = @group.GetPropertyValue("TITLE"),
                    Name = @group.GetPropertyValue("NAME"),
                    Map = @group.GetPropertyValue("HTMLDESC").NormalizeHtmlContent(),
                    BOX4DESC = @group.GetPropertyValue("BOX4DESC").NormalizeHtmlContent(),
                    BOX5DESC = @group.GetPropertyValue("BOX5DESC").NormalizeHtmlContent(),
                    BOX6DESC = @group.GetPropertyValue("BOX6DESC").NormalizeHtmlContent()
                };

            if (string.IsNullOrWhiteSpace(model.Title))
            {
                model.Title = model.Name;
            }

            return View(model);
        }

        public ActionResult ContactUs()
        {
            UserTable user = _expanditUserService.GetUser();
            var viewModel = Mapper.Map<ContactUsViewModel>(user);

            viewModel.SelectedCountry = user.CountryGuid;
            viewModel.ZipCodeCity = user.ZipCode + " " + user.CityName;
            //viewModel.Subject = Language.LABEL_CONTACT_MAIL_SUBJ;
            LoadContactUsDropDown(viewModel);

            return View(viewModel);
        }

        [HttpPost]
        public ActionResult ContactUsReceipt(ContactUsViewModel model)
        {
            if (ModelState.IsValid)
            {
                var recipient = model.SelectedSendAddress;
                try
                {
                    MvcMailMessage msg = _userMailer.WebContact(model, recipient);
                    msg.Send();
                }
                catch (Exception ex)
                {
                    string errorMessage = ex.Message;
                    if (ex.InnerException != null)
                    {
                        errorMessage += " " + ex.InnerException.Message;
                    }
                    FileLogger.Log(errorMessage);
                }
            }
            LoadContactUsDropDown(model);
            return View(model);
        }


        private List<SelectListItem> GenerateAddressList()
        {
            var list = new List<SelectListItem>();

            var item = new SelectListItem { Text = Language.LABEL_CONTACT_ORDER, Value = WebConfigurationManager.AppSettings["MAIL_EMAIL_RESELLER"] };
            list.Add(item);
            item = new SelectListItem { Text = Language.LABEL_CONTACT_SUPPORT, Value = WebConfigurationManager.AppSettings["MAIL_EMAIL_SUPPORT"] };
            list.Add(item);
            item = new SelectListItem { Text = Language.LABEL_CONTACT_COMPLAINT, Value = WebConfigurationManager.AppSettings["MAIL_EMAIL_RECLAMATION"] };
            list.Add(item);
            item = new SelectListItem { Text = Language.LABEL_CONTACT_WEBMASTER, Value = WebConfigurationManager.AppSettings["MAIL_EMAIL_WEB_MASTER"] };
            list.Add(item);

            return list;
        }

        private void LoadContactUsDropDown(ContactUsViewModel viewModel)
        {
            viewModel.SendAddress = GenerateAddressList();
            DataRow[] countryRows = _countryControlLogic.LoadCountries().Select();
            Dictionary<string, string> countryDictionary = countryRows.ToDictionary(r => (string)r["CountryGuid"], r => (string)r["CountryName"]);

            List<CountryItem> countries = countryDictionary.Select(x => new CountryItem { CountryGuid = x.Key, CountryName = x.Value }).ToList();
            viewModel.Countries = countries;
        }
    }
}