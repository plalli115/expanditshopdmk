using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using CmsPublic.ModelLayer.Models;
using EISCS.Shop.Attributes;
using EISCS.Shop.DO.BAS.Interface;
using EISCS.Shop.DO.BAS.Service;
using EISCS.Shop.DO.Interface;
using Resources;
using ViewModels;

namespace Controllers.Templates
{
    [UserAccess(AccessClass = "Catalog")]
    public class ProductController : ExpanditTemplateController
    {
        private readonly IProductRepository _productRepository;
        private readonly IProductService _productService;

        public ProductController(IProductService productService, IExpanditUserService expanditUserService, IProductRepository productRepository)
            : base(expanditUserService)
        {
            _productService = productService;
            _productRepository = productRepository;
        }

        public ActionResult Index(FormCollection collection)
        {
            string viewName = GetViewName();
            string id = GetProductId();
            var group = GetGroup();

            string variantCode = collection["VariantCode"];

            var nodeContainer = _productService.GetProduct(id, variantCode);
            var relatedProductsContainer = _productService.GetRelatedProducts(id);
            var model = new ProductsViewModel(nodeContainer.ProductInfoNodes, UserService, group, null, nodeContainer.Header, relatedProductsContainer.ProductInfoNodes);

            if (model.HasError)
            {
                ModelState.AddModelError("", Language.ERROR_THERE_WERE_ERRORS_CALC_PRICES_FOR_PRODUCTS);
                foreach (System.String k in model.ErrorMessages.Keys)
                {
                    ExpandIT.Logging.FileLogger.log("model.HasError true: " + k + " => " + model.ErrorMessages[k]);
                    ModelState.AddModelError(k, model.ErrorMessages[k]);
                }
            }

            return View(viewName, model);
        }

        public JsonResult AjaxFindProducts(string searchQuery)
        {
            List<Product> products = _productRepository.FindProducts(searchQuery);
            List<ServiceItemClass> serviceItems = products.Select(productItem => new ServiceItemClass
                {
                    ServiceItemNo = string.Empty,
                    Description = productItem.ProductName,
                    ItemNo = productItem.ProductGuid
                }).ToList();

            return Json(serviceItems, JsonRequestBehavior.AllowGet);
        }

        public class ServiceItemClass
        {
            public string ServiceItemNo { get; set; }
            public string ItemNo { get; set; }
            public string Description { get; set; }
        }
    }
}