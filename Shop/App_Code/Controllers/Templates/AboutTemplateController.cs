﻿using System.Web.Mvc;
using CmsPublic.ModelLayer.Models;
using EISCS.ExpandITFramework.Util;
using EISCS.Shop;
using EISCS.Shop.Attributes;
using EISCS.Shop.DO.Interface;
using ViewModels.Templates;

namespace Controllers.Templates
{
    [UserAccess(AccessClass = "HomePage")]
	public class AboutTemplateController : Controller
	{
		private readonly ISessionManager _sessionManager;
        private readonly IGroupRepository _groupRepository;

		public AboutTemplateController(ISessionManager sessionManager, IGroupRepository groupRepository)
		{
			_sessionManager = sessionManager;
		    _groupRepository = groupRepository;
		}
        

		public ActionResult Index(int id)
		{

            Group group = _groupRepository.GetGroup(id, _sessionManager.GetLanguageGuid());
			
			var model = new AboutTemplateViewModel
			{
				HtmlDesc = group.GetPropertyValue("HTMLDESC").NormalizeHtmlContent(),
				Name = group.GetPropertyValue("NAME"),
                Title = group.GetPropertyValue("TITLE")
			};

		    if (string.IsNullOrWhiteSpace(model.Title))
		    {
		        model.Title = model.Name;
		    }
			
			return View(model);
		}
	}

}