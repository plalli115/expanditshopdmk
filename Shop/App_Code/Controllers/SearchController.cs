﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using EISCS.Shop.DO.BAS.Dto;
using EISCS.Shop.DO.BAS.Interface;
using EISCS.Shop.DO.BAS.Repository;
using ViewModels.CustomerAddress;
using ViewModels.ServiceItem;

namespace Controllers
{
    public class SearchController : Controller
    {
        private const int MAX_SEARCH_RESULTS = 10;
        private readonly ICustomerAddressDataService _customerAddressDataService;
        private readonly ICustomerDataService _customerDataService;
        private readonly IServiceItemRepository _serviceItemRepository;

        public SearchController(ICustomerDataService customerDataService,
                                IServiceItemRepository serviceItemRepository,
                                ICustomerAddressDataService customerAddressDataService)
        {
            _customerDataService = customerDataService;
            _serviceItemRepository = serviceItemRepository;
            _customerAddressDataService = customerAddressDataService;
        }

        public JsonResult FindPortalCustomer(string searchQuery)
        {
            IEnumerable<PortalCustomerItem> customers = _customerDataService.AjaxFindCustomer(searchQuery).Take(MAX_SEARCH_RESULTS);

            return Json(customers, JsonRequestBehavior.AllowGet);
        }

        public JsonResult FindServiceItem(string searchQuery)
        {
            IEnumerable<ServiceItem> serviceItems = _serviceItemRepository.AjaxFindServiceItem(searchQuery).Take(MAX_SEARCH_RESULTS);
            var serviceItemList = Mapper.Map<IEnumerable<ServiceItemViewModel>>(serviceItems);

            return Json(serviceItemList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult FindCustomerDeliveryAddressByCustomer(string customerId)
        {
            PortalCustomerItem customer = _customerDataService.CustomerById(customerId);
            var customerMainAddress = new CustomerAddressItem
                {
                    IsMainAddress = true,
                    AddressGuid = string.Empty,
                    Address1 = customer.Address,
                    Address2 = customer.Address2,
                    CityName = customer.CityName,
                    ZipCode = customer.ZipCode,
                    ContactPerson = customer.ContactName,
                    CompanyName = customer.CompanyName,
                    PhoneNo = customer.PhoneNo,
                    EmailAddress = customer.EmailAddress
                };

            var customerAddressItems = new List<CustomerAddressItem> {customerMainAddress};

            IEnumerable<CustomerAddressItem> customerAddressList = _customerAddressDataService.AllByCustomerId(customerId);
            customerAddressItems.AddRange(customerAddressList.ToList());

            var customerAddresses = Mapper.Map<IEnumerable<CustomerAddressViewModel>>(customerAddressItems);

            return Json(customerAddresses, JsonRequestBehavior.AllowGet);
        }

        public JsonResult FindCustomerAddressById(string customerId, string customerAddressId)
        {
            var customerAddress = new CustomerAddressItem();

            if (string.IsNullOrEmpty(customerAddressId))
            {
                PortalCustomerItem customer = _customerDataService.CustomerById(customerId);

                customerAddress.IsMainAddress = true;
                customerAddress.AddressGuid = string.Empty;
                customerAddress.Address1 = customer.Address;
                customerAddress.Address2 = customer.Address2;
                customerAddress.CityName = customer.CityName;
                customerAddress.ZipCode = customer.ZipCode;
                customerAddress.ContactPerson = customer.ContactName;
                customerAddress.CompanyName = customer.CompanyName;
                customerAddress.PhoneNo = customer.PhoneNo;
                customerAddress.EmailAddress = customer.EmailAddress;
            }
            else
				customerAddress = _customerAddressDataService.CustomerDeliveryAddressById(customerId, customerAddressId);
            
            return Json(customerAddress, JsonRequestBehavior.AllowGet);
        }
    }
}