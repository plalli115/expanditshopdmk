﻿using System.Web.Mvc;
using EISCS.Shop.DO.Dto;
using EISCS.Shop.DO.Interface;
using FileIO;

namespace Controllers
{
    public class ImpersonationController : Controller
    {
        public ImpersonationModel ImpModel { get; set; }
        private readonly ILoginService _loginService;

        public ImpersonationController(ILoginService loginService)
        {
            ImpModel = new ImpersonationModel();
            _loginService = loginService;
        }

        public ActionResult Index(string userGuid, string returnUrl)
        {
            string ipFilterFile = Server.MapPath("~") + "\\admin\\transport\\ipfilterstatic.txt";

            var ipFilter = FileToList.FileToStringList(ipFilterFile);

            if (ipFilter.Count == 0)
            {
                ipFilterFile = Server.MapPath("~") + "\\admin\\transport\\ipfilter.txt";
                ipFilter = FileToList.FileToStringList(ipFilterFile);
            }
            
            bool isAccessAllowed = ipFilter.Count == 0;

            foreach (string ip in ipFilter)
            {
                if (ip == Request.ServerVariables["REMOTE_ADDR"])
                {
                    isAccessAllowed = true;
                }
            }

            if (!isAccessAllowed)
            {
                ImpModel.Message = Resources.Language.MESSAGE_IMPERSONATE_ACCESS_DENIED.Replace("%1", Request.ServerVariables["REMOTE_ADDR"]);
                return View(ImpModel);
            }

            if (string.IsNullOrEmpty(userGuid))
            {
                ImpModel.Message = Resources.Language.MESSAGE_THE_CALL_WAS_MISSING_PARAMETERS;
                return View(ImpModel);
            }

            UserTable userTable = _loginService.Impersonate(userGuid);

            if (userTable == null)
            {
                ImpModel.Message = Resources.Language.MESSAGE_INVALID_LOGIN;
                return View(ImpModel);
            }

            if (!string.IsNullOrEmpty(returnUrl))
            {
                return RedirectToLocal(returnUrl);
            }

            TempData["ImpMessage"] = Resources.Language.MESSAGE_YOU_HAVE_NOW_ENTERED_THE_SHOP_AS_THIS_USER.Replace("%1", userTable.ContactName).Replace("%2", userTable.UserLogin);

            return RedirectToAction("LogInResult");
        }

        public ActionResult LogInResult()
        {
            ImpModel.Message = (string) TempData["ImpMessage"];
            return View("Index", ImpModel);
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                RedirectResult redirectResult = Redirect(returnUrl);
                return redirectResult;
            }
            return RedirectToAction("Login", "Accounts");
        }
    }

    public class ImpersonationModel
    {
        public string Message { get; set; }
    }
}