﻿using System.Web.Mvc;
using EISCS.Shop.DO.Interface;
using EISCS.Wrappers.Configuration;

namespace Controllers
{
	public class LocalModeController : Controller
	{
		private readonly IConfigurationRepository _configurationRepository;
	    private readonly IConfigurationObject _configurationObject;

	    public LocalModeController(IConfigurationRepository configurationRepository, IConfigurationObject configurationObject)
		{
		    _configurationRepository = configurationRepository;
		    _configurationObject = configurationObject;
		}

	    public ActionResult Index(string continueUrl)
	    {
	        var remoteProfile = _configurationObject.Read("REMOTE_PROFILE");
			var remoteUrl = _configurationRepository.GetRemoteUrl(remoteProfile);

			return View("Index", null, remoteUrl);
		}
	}
}