﻿using System.Collections.Generic;
using System.Web.Mvc;
using EISCS.ExpandIT;
using EISCS.Shop.BO.BusinessLogic;
using EISCS.Shop.DO.Dto;
using EISCS.Shop.DO.Interface;
using ViewModels;

namespace Controllers
{
	public class HandlingController : Controller
	{
		private readonly IExpanditUserService _userService;
		private readonly IShippingHandlingService _shippingHandlingService;
		private readonly ICurrencyConverter _currencyConverter;

		public HandlingController(IShippingHandlingService shippingHandlingService, IExpanditUserService userService, ICurrencyConverter currencyConverter)
		{
			_shippingHandlingService = shippingHandlingService;
			_userService = userService;
			_currencyConverter = currencyConverter;
		}

		public ActionResult Index()
		{
			var shippingHandlingProviders = _shippingHandlingService.GetShippingHandlingProviders();
			return View(ConvertCurrency(shippingHandlingProviders));
		}

		public List<ShippingHandlingProviderViewModel> ConvertCurrency(List<ShippingHandlingProvider> providers)
		{
			var viewModelList = new List<ShippingHandlingProviderViewModel>(providers.Capacity);
			for (var i = 0; i < providers.Count; i++)
			{
				var provider = providers[i];
				var viewModel = new ShippingHandlingProviderViewModel(provider);
				viewModel.ShippingHandlingPrices = new List<ShippingHandlingPricesViewModel>();

				var currencyDifference = _currencyConverter.ConvertCurrency(0.01, _userService.GetDefaultCurrency, _userService.CurrencyGuid);
				var previousValue = -0.01;
				for (var j = 0; j < provider.ShippingHandlingPrices.Count; j++)
				{
					var price = provider.ShippingHandlingPrices[j];
					var priceViewModel = new ShippingHandlingPricesViewModel();

                    priceViewModel.PreviousCalculatedValue = CurrencyFormatter.FormatAmount((previousValue + 0.01).ToString("R"), _userService.CurrencyGuid);

                    priceViewModel.CalculatedValue = price.CalculatedValue == 0 ? null : " - " + CurrencyFormatter.FormatAmount(_currencyConverter.ConvertCurrency(price.CalculatedValue, _userService.GetDefaultCurrency, _userService.CurrencyGuid).ToString("R"), _userService.CurrencyGuid);

					previousValue = _currencyConverter.ConvertCurrency(price.CalculatedValue, _userService.GetDefaultCurrency, _userService.CurrencyGuid);

                    priceViewModel.HandlingAmount = CurrencyFormatter.FormatAmount(System.Math.Round(_currencyConverter.ConvertCurrency(price.HandlingAmount, _userService.GetDefaultCurrency, _userService.CurrencyGuid), 2).ToString("R"), _userService.CurrencyGuid);
                    priceViewModel.ShippingAmount = CurrencyFormatter.FormatAmount(System.Math.Round(_currencyConverter.ConvertCurrency(price.ShippingAmount, _userService.GetDefaultCurrency, _userService.CurrencyGuid), 2).ToString("R"), _userService.CurrencyGuid);
                    viewModel.ShippingHandlingPrices.Add (priceViewModel);
				}
				viewModelList.Add(viewModel);
			}

			return viewModelList;
		}
	}
}