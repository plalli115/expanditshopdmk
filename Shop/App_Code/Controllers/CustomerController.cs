﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using EISCS.Shop.BO.BAS;
using EISCS.Shop.DO.BAS.Dto;
using EISCS.Shop.DO.BAS.Interface;
using EISCS.Shop.DO.Dto;
using EISCS.Shop.DO.Interface;
using ViewModels.Customer;

namespace Controllers
{
    public class CustomerController : Controller
    {
        private const int MAX_SEARCH_RESULTS = 10;
        private readonly ICustomerService _customerService;
        private readonly IProjectRepository _projectRepository;
        private readonly IServiceOrderService _serviceOrderService;
        private readonly IExpanditUserService _userService;

        public CustomerController(ICustomerService customerService,
                                  IProjectRepository projectRepository,
                                  IServiceOrderService serviceOrderService,
                                    IExpanditUserService userService)
        {
            _customerService = customerService;
            _projectRepository = projectRepository;
            _serviceOrderService = serviceOrderService;
            _userService = userService;
        }

        public JsonResult Find(string searchQuery)
        {
            UserTable user = _userService.GetPortalUser();
            IEnumerable<CustomerItem> customerResults = _customerService.FindCustomers(searchQuery, user.CustomerGroupId).Take(MAX_SEARCH_RESULTS);

            var customers = Mapper.Map<IEnumerable<CustomerViewModel>>(customerResults);

            return Json(customers, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DropdownDataByCustomerGuid(string customerGuid)
        {
            IEnumerable<CustomerPropertyItem> properties = _customerService.GetPropertiesByCustomerId(customerGuid);

            IEnumerable<CustomerPropertyItem> tasks =
                properties.Where(p => p.RelationTableType == CustomerPropertyItem.TableType.Tasks);
            IEnumerable<CustomerPropertyItem> serviceManagers =
                properties.Where(p => p.RelationTableType == CustomerPropertyItem.TableType.ServiceManager);
            IEnumerable<CustomerPropertyItem> technicians =
                properties.Where(p => p.RelationTableType == CustomerPropertyItem.TableType.Technician);

            IEnumerable<ServiceOrderTypeItem> allServiceOrderTypes = _serviceOrderService.GetAllServiceOrderTypes();
            Dictionary<string, string> allTechnicians = _customerService.GetAllTechnicians();
            Dictionary<string, string> allServiceManagers = _customerService.GetAllServiceManagers();

            IEnumerable<ServiceOrderTypeItem> selectedTasks =
                tasks.SelectMany(x => allServiceOrderTypes.Where(y => x.PropertyId == y.Code));
            IEnumerable<KeyValuePair<string, string>> selectedTechnicians =
                technicians.SelectMany(x => allTechnicians.Where(y => x.PropertyId == y.Key));
            IEnumerable<KeyValuePair<string, string>> selectedServiceManagers =
                serviceManagers.SelectMany(x => allServiceManagers.Where(y => x.PropertyId == y.Key));

            IEnumerable<ProjectItem> projects = _projectRepository.GetProjectsByCustomerId(customerGuid);


            return Json(new {selectedTasks, selectedServiceManagers, selectedTechnicians, projects},
                        JsonRequestBehavior.AllowGet);
        }
    }
}