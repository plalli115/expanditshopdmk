﻿using System;
using System.Web.Mvc;
using System.Web.UI;
using EISCS.Shop.DO.BAS.Dto;
using EISCS.Shop.DO.BAS.Interface;
using EISCS.Shop.DO.Interface;
using FormModels.ServiceOrderComment;
using ViewModels.ServiceOrderComment;

namespace Controllers
{
    public class ServiceOrderCommentController : Controller
    {
        private const string RECORD_ACTION = "NEW";
        private const int BAS_CLIENT = 0;
        private const int SERVICE_ITEM_LINE_NUMBER = 10000;
        private readonly IServiceOrderCommentLineRepository _commentLineRepository;
        private readonly IExpanditUserService _userService;

        public ServiceOrderCommentController(IServiceOrderCommentLineRepository commentLineRepository, IExpanditUserService userService)
        {
            _commentLineRepository = commentLineRepository;
            _userService = userService;
        }

        [HttpPost]
        public ActionResult Create(ServiceOrderCommentFormModel formModel)
        {
            var commentLineItem = new ServiceOrderCommentLineItem(formModel.Comment)
                {
                    CommentLineNumber =
                        GenerateUniqueId(_commentLineRepository.GetHighestUniqueId(formModel.ServiceOrderGuid)),
                    CommentLineGuid = Guid.NewGuid().ToString(),
                    ServiceOrderNumber = formModel.ServiceOrderGuid,
                    ServiceOrderGuid = formModel.ServiceOrderGuid,
                    CommentText = formModel.Comment,
                    RecordAction = RECORD_ACTION,
                    BASClient = BAS_CLIENT,
                    UserGuid = formModel.UserGuid,
                    UserType = formModel.UserType,
                    PortalContactName = _userService.GetUser(formModel.UserGuid).ContactName,
                    ServiceItemLineNumber = SERVICE_ITEM_LINE_NUMBER,
                    BASVersion = 0
                };
            ServiceOrderCommentLineItem insertedServiceOrderCommentLine = _commentLineRepository.Add(commentLineItem);
            
            var user = _userService.GetUser(formModel.UserGuid);

            var viewModel = new CommentLineCreateViewModel
                {
                    BASGuid = insertedServiceOrderCommentLine.BASGuid,
                    Comment = formModel.Comment,
                    UserName = user.ContactName,
                    DateCreated = commentLineItem.CommentDate,
                    CommentType = commentLineItem.CommentType
                };

            return PartialView(viewModel);
        }

        private int GenerateUniqueId(int hightestUniqueId)
        {
            var rand = new Random();
            int randomNumber = rand.Next(0, 9999);
            return (hightestUniqueId + ((randomNumber*9999) + 10000));
        }

        public ActionResult Delete(string basGuid)
        {
            _commentLineRepository.Remove(basGuid);
            return null;
        }
    }
}