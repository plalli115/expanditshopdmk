﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web.Mvc;
using CmsPublic.DataProviders.Logging;
using EISCS.ExpandITFramework.Util;
using EISCS.Shop.DO.Dto.Cart;
using EISCS.Shop.DO.Interface;
using EISCS.WebUserControlLogic.Country;
using EISCS.Wrappers.Configuration;

namespace Controllers
{
    /// <summary>
    /// Summary description for DibsCallbackController
    /// </summary>
    public class DibsCallbackController : Controller
    {
        private readonly ICartService _cartService;
        private readonly IConfigurationObject _configuration;
        private readonly ICountryControlLogic _countryControlLogic;
        private readonly IShopSalesService _shopSalesService;

        public DibsCallbackController(ICartService cartService, IConfigurationObject configuration, ICountryControlLogic countryControlLogic, IShopSalesService shopSalesService)
        {
            _cartService = cartService;
            _configuration = configuration;
            _countryControlLogic = countryControlLogic;
            _shopSalesService = shopSalesService;
        }

        public void CalculateShipping(CartHeader cartHeader, string currency, string defaultCurrency)
        {
            var countryRow = _countryControlLogic.LoadCountries().Select().SingleOrDefault(c => (string)c["CountryGuid"] == cartHeader.ShipToCountryGuid);
            cartHeader.ShipToCountryName = (countryRow == default(DataRow)) ? string.Empty : (string)countryRow["CountryName"];
            _cartService.Calculate(cartHeader, currency, defaultCurrency);
            cartHeader.Total = cartHeader.Total + cartHeader.ShippingAmount + cartHeader.HandlingAmount;
            cartHeader.TotalInclTax = cartHeader.TotalInclTax + cartHeader.ShippingAmountInclTax + cartHeader.HandlingAmountInclTax;
        }

        public ActionResult Callback(){
            var dibsPaymentType = _configuration.Read("DIBS_PAYMENT_TYPE");
            if (string.IsNullOrWhiteSpace(dibsPaymentType))
            {
                return null;
            }
            
            // Get user id from request
            var userGuid = Request["s_userguid"];
            
            // Load Order belonging to the current user
            var cartHeader = _cartService.GetCartById(userGuid);
            
            bool isValid;

            switch (dibsPaymentType)
            {
                case "D2A":
                    isValid = CheckDibsCallback(cartHeader);
                    break;
                case "D2B":
                    isValid = CheckDibsCallbackMd5(cartHeader);
                    break;
                default:
                    return null;
            }

            if (isValid)
            {
                var currency = Request["currency"];
                var defaultCurrency = _configuration.Read("MULTICURRENCY_SITE_CURRENCY");
                CalculateShipping(cartHeader, currency, defaultCurrency);
                cartHeader.PaymentMethod = "DIBS";
                _shopSalesService.CartHeader2ShopSalesHeader(cartHeader, userGuid);
            }

            return null;
        }

        private bool CheckDibsCallbackMd5(CartHeader cartHeader){
            
            var amount = Request["amount"];
            var currency = Request["currency"];
            var transact = Request["transact"];
            var authkey = Request["authkey"];
            
            if (!string.IsNullOrEmpty(authkey) && !string.IsNullOrEmpty(transact) && !string.IsNullOrEmpty(amount) && !string.IsNullOrEmpty(currency))
            {
                currency = _cartService.GetCurrencyIso4217(currency);
                MD5 md5 = MD5.Create();
                string inner = _configuration.Read("DIBS_MD5_KEY_1") + "transact=" + transact + "&amount=" + amount + "&currency=" + currency;
                byte[] innerMd5 = md5.ComputeHash(Encoding.UTF8.GetBytes(inner));
                string innerStringHash = BitConverter.ToString(innerMd5).Replace("-", "").ToLower();
                string outer = _configuration.Read("DIBS_MD5_KEY_2") + innerStringHash;
                byte[] outerMd5 = md5.ComputeHash(Encoding.UTF8.GetBytes(outer));
                string outerStringHash = BitConverter.ToString(outerMd5).Replace("-", "").ToLower();
                
                if (outerStringHash == authkey)
                {
                    return true;
                }
                cartHeader.PaymentStatus = "Authentication key does not match calculated key.";
                return false;
            }
            cartHeader.PaymentStatus = "No authentication key was received.";
            return false;
        }

        private bool CheckDibsCallback(CartHeader cartHeader)
        {
            var mac = Request["MAC"];
            var transaction = Request["transaction"];

            if (!transaction.IsNullOrEmpty())
            {
                var paramsDict = Request.Form.Cast<string>().ToDictionary(key => key, key => Request.Form[key]);

                paramsDict.Remove("MAC");

                var hmac = CalculateMac(paramsDict, _configuration.Read("DIBS_HMAC_KEY"));

                if (hmac == mac)
                {
                    cartHeader.PaymentStatus = Request["paymentStatus"];
                    return true;
                }
                cartHeader.PaymentStatus = "Authentication key does not match calculated key.";
                return false;
            }

            cartHeader.PaymentStatus = "No authentication key was received.";
            return false;
        }

        private static string CalculateMac(Dictionary<string, string> paramsDict, string kHexEnc)
        {
            //Create the message for MAC calculation sorted by the key
            var keys = paramsDict.Keys.ToList();
            keys.Sort();
            string msg = "";
            foreach (var key in keys)
            {
                if (key != keys[0]) msg += "&";
                msg += key + "=" + paramsDict[key];
            }

            //Decoding the secret Hex encoded key and getting the bytes for MAC calculation
            var kBytes = new byte[kHexEnc.Length / 2];
            for (int i = 0; i < kBytes.Length; i++)
            {
                kBytes[i] = byte.Parse(kHexEnc.Substring(i * 2, 2), NumberStyles.HexNumber);
            }

            //Getting bytes from message
            var encoding = new UTF8Encoding();
            byte[] msgBytes = encoding.GetBytes(msg);

            //Calculate MAC key
            var hash = new HMACSHA256(kBytes);
            byte[] macBytes = hash.ComputeHash(msgBytes);
            string mac = BitConverter.ToString(macBytes).Replace("-", "").ToLower();

            return mac;
        }
    }
}