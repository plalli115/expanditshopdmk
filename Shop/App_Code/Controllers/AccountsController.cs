﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using CmsPublic.DataProviders.Logging;
using EISCS.ExpandITFramework.Util;
using EISCS.Shop.Attributes;
using EISCS.Shop.DO.Dto;
using EISCS.Shop.DO.Dto.Currency;
using EISCS.Shop.DO.Interface;
using EISCS.Shop.DO.Service;
using EISCS.WebUserControlLogic.Country;
using EISCS.WebUserControlLogic.Language;
using EISCS.Wrappers.Configuration;
using Filters;
using Mailers;
using Mvc.Mailer;
using Resources;
using ViewModels;

namespace Controllers
{
    [Authorize]
    public class AccountsController : Controller
    {
        private readonly ICountryControlLogic _countryControlLogic;
        private readonly ICurrencyRepository _currencyRepository;
        private readonly ILanguageControlLogic _languageControlLogic;
        private readonly ILoginService _loginService;
        private readonly IHttpRuntimeWrapper _httpRuntimeWrapper;
        private readonly IUserManager _userManager;
        private readonly IConfigurationObject _configurationObject;
        private readonly IUserItemRepository _userItemRepository;
        private readonly IKeyPerformanceIndicatorsService _kpiService;
        private readonly IUserMailer _userMailer;
        private readonly IExpanditUserService _expanditUserService;

        public AccountsController(ICountryControlLogic countryControlLogic,
            ILanguageControlLogic languageControlLogic,
            ICurrencyRepository currencyRepository,
            IUserItemRepository userItemRepository,
            ILoginService loginService,
            IHttpRuntimeWrapper httpRuntimeWrapper,
            IUserManager userManager,
            IConfigurationObject configurationObject,
            IKeyPerformanceIndicatorsService kpiService,
            IUserMailer userMailer,
            IExpanditUserService expanditUserService)
        {
            _countryControlLogic = countryControlLogic;
            _languageControlLogic = languageControlLogic;
            _currencyRepository = currencyRepository;
            _userItemRepository = userItemRepository;
            _loginService = loginService;
            _httpRuntimeWrapper = httpRuntimeWrapper;
            _userManager = userManager;
            _configurationObject = configurationObject;
            _kpiService = kpiService;
            _userMailer = userMailer;
            _expanditUserService = expanditUserService;
        }

        [AllowAnonymous]
        public ActionResult Login(string retUrl)
        {
            ViewBag.ReturnUrl = retUrl;
            var model = new LoginModel();
            return View(model);
        }

        // POST: /Accounts/Login
        [HttpPost, ValidateInput(false)]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginModel model, string returnUrl)
        {
            if (string.IsNullOrEmpty(model.Password) || string.IsNullOrEmpty(model.UserName))
            {
                ModelState.AddModelError("", Language.LABEL_INVALID_PASSWORD_AND_LOGIN);
                return View(model);
            }

            if (string.IsNullOrEmpty(returnUrl))
            {
                returnUrl = VirtualRoot();
            }

            ExpanditLoginStatus status = _loginService.Login(model.UserName, model.Password);

            if (status == ExpanditLoginStatus.LoggedIn)
            {
                return RedirectToLocal(returnUrl);
            }

            if (status == ExpanditLoginStatus.AuthorizedNeedPin)
            {
                TempData["model"] = model;
                return RedirectToAction("SecondFactor", new { returnUrl });
            }

            if (status == ExpanditLoginStatus.NeedApproval)
            {
                ModelState.AddModelError("", "Account is not activated");
                return View(model);
            }

            // If we got this far, something failed, redisplay form
            ModelState.AddModelError("", Language.LABEL_INVALID_PASSWORD_AND_LOGIN);
            return View(model);
        }

        [ImportModelStateTempDataFromTempData]
        [AllowAnonymous]
        public ActionResult SecondFactor(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            var model = TempData["model"] as LoginModel;
            if (model != null)
            {
                model.Password = Encryption.EncryptXOrWay(model.Password);
                model.UserName = Encryption.EncryptXOrWay(model.UserName);
                return View(model);
            }
            // If no username or password provided - go back to original login view
            return RedirectToAction("Login", new { returnUrl });
        }

        [HttpPost, ValidateInput(false)]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        [ExportModelStateTempDataToTempData]
        public ActionResult SecondFactor(LoginModel model, string returnUrl)
        {
            if (model != null)
            {
                string password = Encryption.DecryptXOrWay(model.Password);
                string userName = Encryption.DecryptXOrWay(model.UserName);

                ExpanditLoginStatus status = _loginService.SecondFactorLogin(userName, password, model.Pin);

                if (status == ExpanditLoginStatus.LoggedIn)
                {
                    return RedirectToLocal(returnUrl);
                }
            }

            // If we got this far, something failed, redisplay form
            ModelState.AddModelError("", Language.LABEL_INVALID_PIN);
            return View(model);
        }

        private string VirtualRoot()
        {
            return _httpRuntimeWrapper.AppDomainAppVirtualPath.Equals("/")
                                                 ? "/"
                                                 : _httpRuntimeWrapper.AppDomainAppVirtualPath + "/";
        }

        [UserAccess(AccessClass = "OpenSite")]
        [AllowAnonymous]
        [IsLocalMode]
        public ActionResult Register()
        {
            var model = new RegisterUserFormModel();
            PopulateDropdowns(model);
            return View(model);
        }

        // POST: /Accounts/Register
        [HttpPost, ValidateInput(false)]
        [AllowAnonymous]
        [UserAccess(AccessClass = "OpenSite")]
        [ValidateAntiForgeryToken]
        public ActionResult Register(RegisterUserFormModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if ((_userItemRepository.GetUserByUserName(model.UserName) == null))
                    {
                        bool useEmailConfirmationSetup = Utilities.CBoolEx(_configurationObject.Read("USE_ACCOUNT_SETUP_REQUIRE_MAIL_CONFIRMATION"));

                        UserTable user = MapFormDataItemToUserTable(model);
                        string uniqueToken = Guid.NewGuid().ToString();

                        if (useEmailConfirmationSetup)
                        {
                            user.IsApproved = false;
                            user.PasswordResetRequestKey = uniqueToken;
                            user.PasswordResetRequestTime = DateTime.Now;
                        }
                        try
                        {
                            _userItemRepository.Add(user);
                        }
                        catch (Exception ex)
                        {
                            FileLogger.Log(ex.Message);
                            ModelState.AddModelError("ErrorAddingUser", Language.LABEL_ERROR);
                            PopulateDropdowns(model);
                            return View(model);
                        }
                        try
                        {
                            MvcMailMessage msg;
                            if (useEmailConfirmationSetup)
                            {
                                msg = _userMailer.WelcomeConfirmAccount(user.EmailAddress, user.ContactName, user.UserLogin, uniqueToken);
                                msg.Send();
                                return RedirectToAction("AccountCreatedNeedConfirmation", new { userName = user.UserLogin });
                            }
                            msg = _userMailer.Welcome(user.EmailAddress, user.ContactName, user.UserLogin);
                            msg.Send();
                        }
                        catch (Exception ex)
                        {
                            if (useEmailConfirmationSetup)
                            {
                                // If send mail fails - log the error. Set user as approved and continue as usual.
                                user.IsApproved = true;
                                UpdateApprovalStatus(user);
                            }
                            string errorMessage = ex.Message;
                            if (ex.InnerException != null)
                            {
                                errorMessage += " " + ex.InnerException.Message;
                            }
                            FileLogger.Log(errorMessage);
                        }


                        if (_loginService.Login(model.UserName, model.Password) == ExpanditLoginStatus.LoggedIn)
                        {
                            _kpiService.NewUser();
                            return RedirectToLocal(VirtualRoot());
                        }

                        return RedirectToAction("Login");
                    }
                    ModelState.AddModelError(string.Empty, String.Format(Language.MESSAGE_SELECTED_LOGIN_ALREADY_IN_USE).Replace("%1%", model.UserName));
                    PopulateDropdowns(model);
                    return View(model);
                }

                PopulateDropdowns(model);
                return View(model);
            }
            catch (Exception e)
            {
                ModelState.AddModelError(string.Empty, "Unknown Error ocurred while creating user. " + e.Message);
                PopulateDropdowns(model);
                return View(model);
            }
        }

        [UserAccess(AccessClass = "OpenSite")]
        public ActionResult AccountCreatedNeedConfirmation(string userName)
        {
            UserTable userTable = _userItemRepository.GetUserByUserName(userName);
            RegistrationModel model = new RegistrationModel();

            if (userTable != null)
            {
                model.ContactName = userTable.ContactName;
                model.UserLogin = userTable.UserLogin;
                model.IsSuccess = true;
                model.IsApproved = userTable.IsApproved;
            }
            return View(model);
        }

        [AllowAnonymous]
        [UserAccess(AccessClass = "OpenSite")]
        public ActionResult CompleteRegistration()
        {
            var token = Request["requesttoken"];
            var userName = Request["username"];
            var user = _userItemRepository.GetUserByUserName(userName);

            RegistrationModel model = new RegistrationModel();

            if (user != null && user.PasswordResetRequestKey == token)
            {
                user.IsApproved = true;
                model.IsSuccess = UpdateApprovalStatus(user);
                model.IsApproved = model.IsSuccess;
            }

            return View(model);
        }

        private bool UpdateApprovalStatus(UserTable user)
        {
            bool isSuccess = false;
            
            try
            {
                isSuccess = _userItemRepository.Update(user);
            }
            catch (Exception ex)
            {
                FileLogger.Log("Error updating user approval status. " + ex.Message + " " + ex.StackTrace);
            }
            return isSuccess;
        }

        private UserTable MapFormDataItemToUserTable(RegisterUserFormModel model)
        {
            var currency = model.CurrencyGuid.IsNullOrEmpty() ? _configurationObject.Read("MULTICURRENCY_SITE_CURRENCY") : model.CurrencyGuid;
            var chosenEncryptionType = ConfigurationManager.AppSettings["ENCRYPTION_TYPE"];
            Encryption.ValidateEncryptionType(chosenEncryptionType);
            var encryptionSalt = Encryption.GenerateSaltValue();
            var item = new UserTable
                {
                    ContactName = model.ContactName,
                    CompanyName = model.CompanyName,
                    EmailAddress = model.EmailAddress,
                    Address1 = model.Address1,
                    Address2 = model.Address2,
                    CityName = model.CityName,
                    ZipCode = model.ZipCode,
                    PhoneNo = model.PhoneNo,
                    CountryGuid = model.CountryGuid,
                    LanguageGuid = model.LanguageGuid,
                    CurrencyGuid = currency,
                    SecondaryCurrencyGuid = model.SecondaryCurrencyGuid,
                    UserLogin = model.UserName,
                    UserPassword = Encryption.Encrypt(model.Password, encryptionSalt, chosenEncryptionType),
                    UserCreated = DateTime.Now,
                    UserModified = DateTime.Now,
                    IsB2B = false,
                    Status = 0,
                    IsApproved = true,
                    IsLockedOut = false,
                    EncryptionType = chosenEncryptionType,
                    EncryptionSalt = Encryption.UseEncryptionSalt(chosenEncryptionType) ? encryptionSalt : "NULL",
                    RoleId = "B2C",
                    PasswordResetRequestTime = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue
                };

            return item;
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                RedirectResult redirectResult = Redirect(returnUrl);
                return redirectResult;
            }
            return RedirectToAction("Login", "Accounts");
        }

        [AllowAnonymous]
        public ActionResult Logout()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult UserLogout()
        {
            _loginService.Logout();
            return RedirectToAction("Logout", "Accounts", null);
        }

        [AllowAnonymous]
        [HttpPost, ValidateInput(false)]
        public ActionResult RequestNewPassword(RequestNewPasswordViewModel model)
        {
            string emailAddress = model.EmailAddress;
            string userName = model.UserName;

            string message = Language.MESSAGE_EMAIL_ADDRESS_NOT_VALID;
            if (ModelState.IsValid)
            {
                if (!Validate(emailAddress))
                {
                    ModelState.AddModelError("", message);
                    return View();
                }

                if (ExpanditLib2.CBoolEx(_configurationObject.Read("USE_ACCOUNT_PASSWORD_RESET_MAIL_LINK")))
                {
                    // Send email with link to password reset page

                    // 1. find user
                    var user = _userItemRepository.GetUserByEmailAndUserLogin(emailAddress, userName);

                    if (user == null)
                    {
                        ModelState.AddModelError("NoUserFound", Language.MESSAGE_LOST_PASSWORD_USER_DOESNT_EXIST);
                        return View();
                    }

                    // 2. if user is found generate random string and update database with string and timestamp
                    string uniqueToken = Guid.NewGuid().ToString();
                    user.PasswordResetRequestKey = uniqueToken;
                    user.PasswordResetRequestTime = DateTime.Now;
                    _userItemRepository.UpdateRequestPasswordKeyAndTimestamp(user);

                    // 3. create and send email with link
                    try
                    {
                        MvcMailMessage mail = _userMailer.ResetPassword(emailAddress, user.ContactName, model.UserName, uniqueToken);
                        mail.Send();
                        model.Message = Language.Label_password_reset_mail_sent_message;
                        model.Success = true;
                    }
                    catch (Exception ex)
                    {
                        string errorMessage = ex.Message;
                        if (ex.InnerException != null)
                        {
                            errorMessage += " " + ex.InnerException.Message;
                        }
                        FileLogger.Log(errorMessage);
                        message = Language.MESSAGE_MAIL_SEND_FAIL;
                        ModelState.AddModelError("ErrorSendingMail", message);
                        return View();
                    }
                }
                else
                {
                    // Create new password and send it with the email

                    int stringLenght = ExpanditLib2.CLngEx(_configurationObject.Read("SEND_FORGOTTEN_PASSWORD_LEN"));

                    string contactName;
                    string newPassword = _userManager.UpdateLostPassword(emailAddress, userName, stringLenght, out contactName);

                    if (string.IsNullOrEmpty(newPassword))
                    {
                        message = Language.MESSAGE_LOST_PASSWORD_USER_DOESNT_EXIST;
                        ModelState.AddModelError("", message);
                        return View();
                    }

                    try
                    {
                        MvcMailMessage msg = _userMailer.LostPassword(emailAddress, contactName, model.UserName, newPassword);
                        msg.Send();
                        model.Message = Language.MESSAGE_LOST_PASSWORD_SUCCCESSFULLY_SEND.Replace("%EMAIL_ADDRESS%", emailAddress);
                        model.Success = true;
                    }
                    catch (Exception ex)
                    {
                        string errorMessage = ex.Message;
                        if (ex.InnerException != null)
                        {
                            errorMessage += " " + ex.InnerException.Message;
                        }
                        FileLogger.Log(errorMessage);
                        message = Language.MESSAGE_MAIL_SEND_FAIL;
                        ModelState.AddModelError("ErrorSendingMail", message);
                        return View();
                    }
                }
            }

            return View(model);

        }

        [AllowAnonymous]
        public ActionResult RequestNewPassword()
        {
            RequestNewPasswordViewModel model = new RequestNewPasswordViewModel();
            return View(model);
        }

        [AllowAnonymous]
        public ActionResult ResetPassword()
        {
            var token = Request["resettoken"];
            ResetPasswordViewModel model = new ResetPasswordViewModel { ResetToken = token };

            // If request token is valid and time is not expired
            var user = _userItemRepository.GetUserByRequestResetPasswordToken(token);
            if (user != null)
            {
                int validTimeframeMinutes = ExpanditLib2.CLngEx(_configurationObject.Read("VALID_PASSWORD_RESET_TIMEFRAME_MINUTES"));
                int validTimeframeSeconds = validTimeframeMinutes * 60;
                if ((DateTime.Now - user.PasswordResetRequestTime).TotalSeconds > validTimeframeSeconds)
                {
                    // Time has expired
                    ModelState.AddModelError("TimeHasExpired", Language.AccountsController_ResetPassword_Time_has_expired);
                }
            }

            return View(model);
        }

        [AllowAnonymous]
        [HttpPost, ValidateInput(false)]
        public ActionResult UpdateResetPassword(ResetPasswordViewModel model)
        {
            string resetToken = model.ResetToken;

            if (ModelState.IsValid)
            {
                // Extra check
                if (string.IsNullOrWhiteSpace(model.Password) || string.IsNullOrWhiteSpace(model.ConfirmedPassword))
                {
                    ModelState.AddModelError("InvalidPasswordInfo", Language.AccountsController_UpdateResetPassword_Invalid_user_information);
                    return View(model);
                }
                // Find in db
                var user = _userItemRepository.GetUserByUserName(model.UserName);

                if (user != null && user.PasswordResetRequestKey == resetToken)
                {
                    int validTimeframeMinutes = ExpanditLib2.CLngEx(_configurationObject.Read("VALID_PASSWORD_RESET_TIMEFRAME_MINUTES"));
                    int validTimeframeSeconds = validTimeframeMinutes * 60;
                    if ((DateTime.Now - user.PasswordResetRequestTime).TotalSeconds <= validTimeframeSeconds)
                    {
                        var chosenEncryptionType = _configurationObject.Read("ENCRYPTION_TYPE");
                        Encryption.ValidateEncryptionType(chosenEncryptionType);
                        string newEncryptionSalt = Encryption.GenerateSaltValue();
                        bool useSalt = Encryption.UseEncryptionSalt(chosenEncryptionType);
                        model.Success = _userItemRepository.UpdatePassword(user.UserGuid, Encryption.Encrypt(model.Password, newEncryptionSalt, chosenEncryptionType),
                            chosenEncryptionType,
                            useSalt ? newEncryptionSalt : "NULL");
                        if (model.Success)
                        {
                            model.Message = Language.LABEL_THE_PASSWORD_HAS_BEEN_CHANGED;
                        }
                    }
                    else
                    {
                        // Time has expired
                        ModelState.AddModelError("TimeHasExpired", Language.AccountsController_ResetPassword_Time_has_expired);
                    }
                }
                else
                {
                    // Time has expired
                    ModelState.AddModelError("InvalidUserToken", Language.AccountsController_UpdateResetPassword_Invalid_user_information);
                }
            }

            return View(model);
        }

        private bool Validate(string emailAddress)
        {
            if (string.IsNullOrEmpty(emailAddress))
            {
                return false;
            }
            return IsMatch(@"^[-!#$%&'*+/0-9=?A-Z^_a-z{|}~](\.?[-!#$%&'*+/0-9=?A-Z^_a-z{|}~])*@[a-zA-Z](-?[a-zA-Z0-9])*(\.[a-zA-Z](-?[a-zA-Z0-9])*)+$", emailAddress);
        }

        private bool IsMatch(string pattern, string toTest)
        {
            var regex = new Regex(pattern, RegexOptions.IgnoreCase);
            return regex.IsMatch(toTest);
        }

        private void PopulateDropdowns(RegisterUserFormModel model)
        {
            DataRow[] countryRows = _countryControlLogic.LoadCountries().Select();
            Dictionary<string, string> countryDictionary = countryRows.ToDictionary(r => (string)r["CountryGuid"], r => (string)r["CountryName"]);
            model.CountryList = countryDictionary;

            DataRow[] languageRows = _languageControlLogic.LoadLanguages(true).Select();
            Dictionary<string, string> languageDictionary = languageRows.ToDictionary(r => (string)r["LanguageGuid"], r => (string)r["LanguageName"]);
            model.LanguageList = languageDictionary;

            IEnumerable<CurrencyItem> currencyItems = GetValidCurrencyItems();
            model.CurrencyList = currencyItems.ToDictionary(c => c.Id, c => c.Name);
        }

        private IEnumerable<CurrencyItem> GetValidCurrencyItems()
        {
            return _currencyRepository.GetValid();
        }
        [AllowAnonymous]
        public JsonResult GetUsername()
        {
            var user = _expanditUserService.GetUser();

            string username = user != null ? user.UserLogin : "Anonymous";
            return Json(new
            {
                username
            });
        }
    }

    public class RegistrationModel
    {
        public bool IsApproved { get; set; }
        public string ContactName { get; set; }
        public string UserLogin { get; set; }
        public bool IsSuccess { get; set; }
    }
}