﻿using System.Collections.Generic;
using System.Web.Mvc;
using EISCS.Shop.DO.BAS.Dto;
using EISCS.Shop.DO.BAS.Interface;

namespace Controllers
{
    public class ProjectController : Controller
    {
        private readonly IProjectRepository _projectRepository;
        
        public ProjectController(IProjectRepository projectRepository)
        {
            _projectRepository = projectRepository;
        }

        public JsonResult Find(string customerGuid)
        {
            IEnumerable<ProjectItem> projects = _projectRepository.GetProjectsByCustomerId(customerGuid);

            return Json(projects);
        }
    }
}