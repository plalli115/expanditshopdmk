﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using AutoMapper;
using CmsPublic.ModelLayer.Models;
using EISCS.Catalog.Logic;
using EISCS.ExpandITFramework.Util;
using EISCS.Routing.Providers;
using EISCS.Shop.DO.Dto;
using EISCS.Shop.DO.Dto.Cart;
using EISCS.Shop.DO.Dto.Currency;
using EISCS.Shop.DO.Interface;
using EISCS.Shop.DO.Service;
using EISCS.WebUserControlLogic.Country;
using EISCS.WebUserControlLogic.Language;
using FormModels;
using StackExchange.Profiling;
using ViewModels;

namespace Controllers
{
    /// <summary>
    ///     Summary description for ExpanditMenuController
    /// </summary>
    public class ExpanditMenuController : Controller
    {
        private const int TopMenuIndex = 0; // Temporary solution - should match the order of the menu placement on the page
        private readonly string _backendType = ConfigurationManager.AppSettings["BackendType"];
        private readonly ICartService _cartService;
        private readonly ICatalogLogic _catalogLogic;
        private readonly ICountryControlLogic _countryControlLogic;
        private readonly ICurrencyRepository _currencyRepository;
        private readonly ILanguageControlLogic _languageControlLogic;
        private readonly string _languageGuid;
        private readonly string _siteDefaultCurrency = ConfigurationManager.AppSettings["MULTICURRENCY_SITE_CURRENCY"];
        private readonly string _siteDefaultCurrencyName = ConfigurationManager.AppSettings["DEFAULT_CURRENCY_NAME"];
        private readonly IUserItemRepository _userItemRepository;
        private readonly IExpanditUserService _userService;

        public ExpanditMenuController(IExpanditUserService userService,
            IUserItemRepository userItemRepository,
            ICatalogLogic catalogLogic,
            ICurrencyRepository currencyRepository,
            ICountryControlLogic countryControlLogic,
            ILanguageControlLogic languageControlLogic,
            ICartService cartService)
        {
            _userService = userService;
            _userItemRepository = userItemRepository;
            _currencyRepository = currencyRepository;
            _countryControlLogic = countryControlLogic;
            _languageControlLogic = languageControlLogic;
            _cartService = cartService;
            _catalogLogic = catalogLogic;
            _languageGuid = _userService.LanguageGuid;
            string currencyGuid = _userService.CurrencyGuid;

            SetUserLanguage(_languageGuid);
            SetUserCurrency(currencyGuid);
            SetUiCulture();
        }

        private void SetUserLanguage(string languageGuid)
        {
            SetCookieValue("user", "LanguageGuid", languageGuid);
            System.Web.HttpContext.Current.Session["LanguageGuid"] = languageGuid;

            UserTable user = _userService.GetUser();

            if (user != null && _userService.UserType == UserClass.B2C)
            {
                bool success = _userItemRepository.UpdateLanguage(user, languageGuid);

                if (!success)
                {
                    throw new Exception("Could not update user Language");
                }
            }
        }

        private void SetUserCurrency(string currencyGuid)
        {
            if (_backendType.ToUpper() == "NAV" || _backendType.ToUpper() == "NF")
            {
                if (_siteDefaultCurrency == currencyGuid)
                {
                    System.Web.HttpContext.Current.Session["CurrencyGuid"] = currencyGuid;
                    SetCookieValue("user", "CurrencyGuid", currencyGuid);
                    currencyGuid = "";
                }
            }

            if (_userService.UserType == UserClass.B2C)
            {
                UserTable user = _userService.GetUser();
                if (user != null)
                {
                    if (string.IsNullOrEmpty(currencyGuid) && (_backendType.ToUpper() == "NAV" || _backendType.ToUpper() == "NF"))
                    {
                        _userItemRepository.UpdateCurrency(user, currencyGuid);
                    }
                    if (!string.IsNullOrEmpty(currencyGuid))
                    {
                        _userItemRepository.UpdateCurrency(user, currencyGuid);
                    }
                }
            }
            if (currencyGuid != "")
            {
                System.Web.HttpContext.Current.Session["CurrencyGuid"] = currencyGuid;
                SetCookieValue("user", "CurrencyGuid", currencyGuid);
            }
        }

        private void SetCookieValue(string cookieName, string cookieKey, string cookieValue)
        {
            if (cookieValue == null)
            {
                cookieValue = "";
            }
            var unprotectedBytes = Encoding.UTF8.GetBytes(cookieValue);
            var encodedCookieValue = MachineKey.Encode(unprotectedBytes, MachineKeyProtection.All);
            
            System.Web.HttpContext.Current.Response.Cookies[cookieName][cookieKey] = encodedCookieValue;
            System.Web.HttpContext.Current.Response.Cookies[cookieName].Path = System.Web.HttpContext.Current.Server.UrlPathEncode(AppUtil.GetVirtualRoot()) + "/";
        }

        private void SetUiCulture()
        {
            string languageGuid = _userService.LanguageGuid;
            var ci = new CultureInfo(languageGuid);
            Thread.CurrentThread.CurrentUICulture = ci;
        }

        public ActionResult TopMenu(int currentMenuId)
        {
            if (currentMenuId == -1)
            {
                RequestContext context = HttpContext.Request.RequestContext;

                string groupId = RouteManager.Route.GetGroupId(context);

                if (!string.IsNullOrWhiteSpace(groupId))
                {
                    currentMenuId = int.Parse(groupId);
                }
            }

            var profiler = MiniProfiler.Current;
            using (profiler.Step("Loading TopMenu"))
            {
                var menuGroupTree = _catalogLogic.GetMenuGroupTree(TopMenuIndex, _languageGuid);
                var children = menuGroupTree.Children;

                IEnumerable<ExpanditMenuViewModel.MenuItem> menuItems = GetMenuItems(currentMenuId, children);

                MiniCartItem miniCart = null;

                CartHeader cartHeader = _cartService.GetCartById(_userService.UserGuid);
                if (cartHeader != null)
                {
                    miniCart = _cartService.UpdateMiniCart(cartHeader);
                }

                var viewModel = new ExpanditMenuViewModel(miniCart, _userService, cartHeader, menuItems);

                return View(viewModel);
            }
        }

        public ActionResult ProductMenu(int currentMenuId)
        {
            if (currentMenuId == -1)
            {
                RequestContext context = HttpContext.Request.RequestContext;
                string groupId = RouteManager.Route.GetGroupId(context);
                if (!string.IsNullOrEmpty(groupId))
                {
                    currentMenuId = int.Parse(groupId);
                }
            }

            var profiler = MiniProfiler.Current;
            using (profiler.Step("Loading ProductMenu"))
            {
                Group catalogGroupTree = _catalogLogic.GetCatalogGroupTree(_userService.LanguageGuid);
                if (catalogGroupTree.Children.Count == 1)
                {
                    catalogGroupTree = catalogGroupTree.Children.Single();
                }

                IEnumerable<ExpanditMenuViewModel.MenuItem> menuItems;
                using (profiler.Step("Get MenuItems"))
                {
                    menuItems = GetMenuItems(currentMenuId, catalogGroupTree.Children);
                }

                var viewModel = new ExpanditMenuViewModel
                    {
                        MenuItems = menuItems
                    };
                return View("_ProductMenu", viewModel);
            }
        }

        public ActionResult ToolbarMenu()
        {
            UserTable userItem;
            try
            {
                userItem = _userService.GetUser();
            }
            catch (Exception)
            {
                userItem = null;
            }
            if (userItem == null)
            {
                userItem = new UserTable { CurrencyGuid = _userService.CurrencyGuid, LanguageGuid = _userService.LanguageGuid };
            }
            UserItemFormModel formModel = Mapper.Map<UserTable, UserItemFormModel>(userItem);

            PopulateUserFormDropdowns(formModel);

            return View("_ToolbarMenu", formModel);
        }

        private void PopulateUserFormDropdowns(UserItemFormModel userItemFormModel)
        {
            DataRow[] countryRows = _countryControlLogic.LoadCountries().Select();
            Dictionary<string, string> countryDictionary = countryRows.ToDictionary(r => ExpanditLib2.CStrEx(r["CountryGuid"]),
                r => ExpanditLib2.CStrEx(r["CountryName"]));
            userItemFormModel.CountryList = countryDictionary;

            DataRow[] languageRows = _languageControlLogic.LoadLanguages(true).Select();
            Dictionary<string, string> languageDictionary = languageRows.ToDictionary(r => (string)r["LanguageGuid"],
                r => (string)r["LanguageName"]);
            userItemFormModel.LanguageList = languageDictionary;

            IEnumerable<CurrencyItem> currencyItems = GetValidCurrencyItems();
            userItemFormModel.CurrencyList = currencyItems.ToDictionary(c => c.Id, c => c.Name);
            if (_backendType.ToUpper() == "NAV" || _backendType.ToUpper() == "NF")
            {
                if (!userItemFormModel.CurrencyList.ContainsKey(_siteDefaultCurrency))
                {
                    userItemFormModel.CurrencyList.Add(_siteDefaultCurrency, _siteDefaultCurrencyName);
                }
                userItemFormModel.CurrencyGuid = _userService.CurrencyGuid;
            }
        }

        private IEnumerable<CurrencyItem> GetValidCurrencyItems()
        {
            return _currencyRepository.All().Where(x => x.CurrencyEnabled);
        }

        private IEnumerable<ExpanditMenuViewModel.MenuItem> GetMenuItems(int currentMenuId, List<Group> children)
        {
            if (children == null || !children.Any())
            {
                return Enumerable.Empty<ExpanditMenuViewModel.MenuItem>().ToList();
            }

            List<ExpanditMenuViewModel.MenuItem> menuItems = (from g in children
                where IsStateVisible(g)
                select new ExpanditMenuViewModel.MenuItem
                    {
                        Text = g.GetPropertyValue("NAME"), Url = NavigationUtil.NavigationLink(g, g.GetPropertyValue("REDIRECTURL"), "{0}", _languageGuid), Selected = (g.ID == currentMenuId), Children = GetMenuItems(currentMenuId, g.Children)
                    }).ToList();
            return menuItems;
        }

        private bool IsStateVisible(Group grp)
        {
            DateTime? startDate = GetDateValue(grp, "STARTDATE");
            DateTime? endDate = GetDateValue(grp, "ENDDATE");
            if (startDate == null || endDate == null)
            {
                return true;
            }
            DateTime currentDate = DateTime.Now;
            return currentDate >= startDate.Value && currentDate <= endDate.Value;
        }

        private DateTime? GetDateValue(Group grp, string value)
        {
            if (grp.Properties == null || grp.Properties.PropDict == null)
            {
                return null;
            } 
            if (!grp.Properties.PropDict.ContainsKey(value))
            {
                return null;
            }

            if (string.IsNullOrEmpty(grp.Properties.PropDict[value]))
            {
                return null;
            }

            string dateString = grp.Properties.PropDict[value];
            DateTime dateTime;
            if (!DateTime.TryParse(dateString, out dateTime))
                return null;

            return dateTime;
        }

        public ActionResult ChangeLanguage(string lang)
        {
            SetUserLanguage(lang);
            SetUiCulture();

            return Redirect(System.Web.HttpContext.Current.Request.UrlReferrer.AbsoluteUri);
        }

        public ActionResult ChangeCurrency(string currency)
        {
            SetUserCurrency(currency);

            return Redirect(System.Web.HttpContext.Current.Request.UrlReferrer.AbsoluteUri);
        }
    }
}