﻿using System;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Text;
using System.Web.Mvc;
using EISCS.Shop.DO.Dto.Cart;
using EISCS.Shop.DO.Interface;
using EISCS.Shop.DO.Service;
using EISCS.Wrappers.Configuration;
using ExpandIT;
using Filters;
using Controllers.Templates;
using EISCS.ExpandITFramework.Util;
using System.Configuration;

/// <summary>
/// Summary description for EEPGProcessorController
/// </summary>
public class EEPGProcessorController : ExpanditTemplateController
{

    private readonly ICartService _cartService;
    private readonly IConfigurationObject _configuration;
    private readonly IShopSalesHeaderRepository _shopSalesHeaderRepository;
    private readonly IShopSalesService _shopSalesService;
    private readonly ILegacyDataService _legacyDataService;
    private readonly System.Web.HttpRequest WebRequest;
    private string ccnumber;
    private string expdate;
    private string ccv;
    private string FirstName;
    private string LastName;

    public EEPGProcessorController(ICartService cartService, IConfigurationObject configuration, IExpanditUserService userService, IShopSalesHeaderRepository shopSalesHeaderRepository, IShopSalesService shopSalesService, ILegacyDataService legacyDataService)
        :base(userService)
    {
        _cartService = cartService;
        _configuration = configuration;
        _shopSalesHeaderRepository = shopSalesHeaderRepository;
        _shopSalesService = shopSalesService;
        _legacyDataService = legacyDataService;
        WebRequest = System.Web.HttpContext.Current.Request;

        ccnumber = WebRequest["CCN"];
        expdate = WebRequest["ExpDate"];
        ccv = WebRequest["CCV"];

    }

    [ExportModelStateTempDataToTempData]
    public ActionResult Process()
    {

        var cartHeader = _cartService.GetCartById(UserService.UserGuid);

        string ccnumber = WebRequest["CCN"];
        string expdate = WebRequest["ExpDate"];
        string ccv = WebRequest["CCV"];
        bool IsValid = true;
        if (ccnumber.IsNullOrEmpty())
        {
            IsValid = false;
            ModelState.AddModelError("CCN Required", "Credit Card Number is required");
        }
        if (expdate.IsNullOrEmpty())
        {
            IsValid = false;
            ModelState.AddModelError("ExpDate Required", "Expiration Date is required");
        }
        else if (!(expdate.All(Char.IsDigit) && expdate.Length == 4))
        {
            IsValid = false;
            ModelState.AddModelError("ExpDate Format", "Expiration Date must be in MMYY format");
        }
        if (ccv.IsNullOrEmpty())
        {
            IsValid = false;
            ModelState.AddModelError("CCV Required", "CCV is required");
        }
        if (!IsValid)
        {
            return RedirectToAction("Review", "Checkout", new { PaymentType = "EEPG" });
        }

        ExpDictionary eepgDict = new ExpDictionary();
        eepgDict.Add("CustomerReference", cartHeader.CustomerReference);
        eepgDict.Add("DocumentGuid", cartHeader.HeaderGuid);
        eepgDict.Add("PaymentType", cartHeader.PaymentMethod);
        eepgDict.Add("PaymentGuid", System.Guid.NewGuid().ToString());
        eepgDict.Add("PaymentReference", GenCustRef());
        eepgDict.Add("PaymentDate", DateTime.Now);
        eepgDict.Add("UserGuid", cartHeader.UserGuid);
        eepgDict.Add("LedgerPayment", false);
        eepgDict.Add("PaymentFirstName", WebRequest["FirstName"]);
        eepgDict.Add("PaymentLastName", WebRequest["LastName"]);

        if(ExpanditLib2.CBoolEx(ConfigurationManager.AppSettings["PAYMENT_EEPG_REQUIRE_ADDRESS_INFO"]))
        {
            string address1St = WebRequest["Address1"];
            string address1No;
            if (address1St.IndexOf(" ") > 0)
            {
                address1No = address1St.Substring(0, address1St.IndexOf(" "));
                address1St = address1St.Substring(address1No.Length + 2);
            }
            else if (ExpanditLib2.CIntEx(address1St) > 0)
            {
                address1No = address1St;
                address1St = "";
            }
            else
            {
                address1No = "";
            }

            eepgDict.Add("PaymentAddress1No", address1No);
            eepgDict.Add("PaymentAddress1St", address1St);
            eepgDict.Add("PaymentAddress2", WebRequest["Address2"]);
            eepgDict.Add("PaymentCity", WebRequest["City"]);
            eepgDict.Add("PaymentEmail", WebRequest["Email"]);
            eepgDict.Add("PaymentPhone", WebRequest["Phone"]);
            eepgDict.Add("PaymentCountry", cartHeader.CountryGuid);
        }

        if (ExpanditLib2.CBoolEx(ConfigurationManager.AppSettings["PAYMENT_EEPG_REQUIRE_ZIP_CODE"]))
        {
            eepgDict.Add("PaymentZipCode", WebRequest["ZipCode"]);
        }

        EnterprisePaymentGatewayProcessor paymentProcessor = new EnterprisePaymentGatewayProcessor();
        ExpDictionary configOptions;
        configOptions = ExpandITLib.Sql2Dictionary("SELECT * FROM EECCConfiguredGateway WHERE ConfiguredGateway = '" + ExpanditLib2.CStrEx(ConfigurationManager.AppSettings["ConfiguredGateway"]) + "'");
        configOptions["CCCryptoPassPhrase"] = ExpanditLib2.CStrEx(ConfigurationManager.AppSettings["CCCryptoPassPhrase"]);
        configOptions["CCCryptoInitVector"] = ExpanditLib2.CStrEx(ConfigurationManager.AppSettings["CCCryptoInitVector"]);
        configOptions["CCCryptoSaltValue"] = ExpanditLib2.CStrEx(ConfigurationManager.AppSettings["CCCryptoSaltValue"]);
        configOptions["CCCryptoPasswordIterations"] = ExpanditLib2.CStrEx(ConfigurationManager.AppSettings["CCCryptoPasswordIterations"]);

        ExpDictionary OrderDict = new ExpDictionary();
        OrderDict.Add("UserGuid", cartHeader.UserGuid);
        OrderDict.Add("CurrencyGuid", cartHeader.CurrencyGuid);
        OrderDict.Add("TotalInclTax", WebRequest["Amount"]);
        OrderDict.Add("CustomerReference", cartHeader.CustomerReference);

        String result = paymentProcessor.EEPGProcessPayment(eepgDict, ccnumber, expdate, ccv, OrderDict, FirstName, LastName, WebRequest, configOptions);

        if (! result.StartsWith("ERRORENCRYPTING"))
        {
            String[] results = result.Split('*');
            if (results[0] == "True")
            {
                eepgDict.Add("PaymentTransactionID", results[1]);
                eepgDict.Add("PaymentTransactionAmount", results[2]);
                eepgDict.Add("PaymentEncryptedData", results[3]);
                eepgDict.Add("PaymentLastNumbers", "XXXX" + results[4].Substring(results[4].Length-4));
                eepgDict.Add("PaymentCardType", results[5]);
                ExpandITLib.Dict2Table(eepgDict, "PaymentTable", "");
                return RedirectToAction("Accepted", "Checkout", new { PaymentType = "EEPG" });
            }
            else
            {
                ModelState.AddModelError("EEPG Error", result);
                return RedirectToAction("Review", "Checkout", new { PaymentType = "EEPG" });
            }
        }
        else
        {
            ModelState.AddModelError("EEPG Encryption Error", result);
            return RedirectToAction("Review", "Checkout", new { PaymentType = "EEPG" });
        }
    }


    /* copied (but modified) from the Enterprise version.  Generates a random
     * 10 digit number, making sure it doesn't match any other reference numbers
     * in the database */
    private String GenCustRef()
    {
        Random rand = new Random();
        for (int i = 0; i<1000; i++)
        {
            String retv = "";
            /*Generate 10 random digits and concat them*/
            for (int d = 0; d<10; d++)
            {
                int r = rand.Next(0, 10);
                retv = retv + Utilities.CStrEx(r);
            }
            if(!ExpandITLib.HasRecords("SELECT TOP 1 * FROM CartHeader WHERE CustomerReference = " + ExpanditLib2.SafeString(retv)))
            { 
                if (!ExpandITLib.HasRecords("SELECT TOP 1 * FROM PaymentTable WHERE PaymentReference = " + ExpanditLib2.SafeString(retv)))
                {
                        return retv;
                }
            }
        }

        throw new Exception("Failed to create a PaymentReference after 1000 attempts");
    }

}