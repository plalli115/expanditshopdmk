﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using CmsPublic.DataProviders.Logging;
using EISCS.Shop.DO.Interface;
using EISCS.Shop.DO.Service;
using EISCS.Wrappers.Configuration;
using Mailers;
using Mvc.Mailer;
using Resources;
using ViewModels;

namespace Controllers.Controls
{
    public class MailTipController : Controller
    {
        private readonly IProductService _productService;
        private readonly IExpanditUserService _expanditUserService;
        private readonly IConfigurationObject _configurationObject;
        private readonly IUserMailer _userMailer;

        public MailTipController(IProductService productService, IExpanditUserService expanditUserService, IConfigurationObject configurationObject, IUserMailer userMailer)
        {
            _productService = productService;
            _expanditUserService = expanditUserService;
            _configurationObject = configurationObject;
            _userMailer = userMailer;
        }

        [HttpPost]
        public JsonResult SendMail(string sendersName, string sendersMailAddress, string displayName, string displayMailAddress, string productGuid, string currentUrl)
        {
            var nodeContainer = _productService.GetProduct(productGuid, null);
            var model = new ProductsViewModel(nodeContainer.ProductInfoNodes, _expanditUserService, null, null, nodeContainer.Header, null);
            
            try
            {
                MvcMailMessage msg = _userMailer.TipFriend(sendersName, displayMailAddress, sendersMailAddress, model, currentUrl);
                msg.Send();
            }
            catch (Exception ex)
            {
                string errorMessage = ex.Message;
                if (ex.InnerException != null)
                {
                    errorMessage += " " + ex.InnerException.Message;
                }
                FileLogger.Log(errorMessage);
                return Json(new { msg = Language.MESSAGE_MAIL_SEND_FAIL });
            }
            return Json(new { msg = Language.MESSAGE_MAIL_SEND_OK });
        }
    }
}