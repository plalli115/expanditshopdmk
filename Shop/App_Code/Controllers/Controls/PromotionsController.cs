﻿using System.Collections.Generic;
using System.Web.Mvc;
using EISCS.Shop.DO.Dto.Cart;
using EISCS.Shop.DO.Interface;
using EISCS.Shop.DO.Service;
using EISCS.Wrappers.Configuration;
using ViewModels;
using EISCS.Shop.DO.Dto.Promotions;
using System;
using System.Linq;

namespace Controllers.Controls
{
    /// <summary>
    /// Summary description for PromotionsController
    /// </summary>
    public class PromotionsController : Controller
    {

        private readonly IConfigurationObject _configuration;
        private readonly ICartService _cartService;
        private readonly IExpanditUserService _userService;
        private readonly PromotionsServices _promotionsServices;

        public PromotionsController(IConfigurationObject configuration, ICartService cartService, IExpanditUserService userService, PromotionsServices promotionsServices)
        {
            _configuration = configuration;
            _cartService = cartService;
            _userService = userService;
            _promotionsServices = promotionsServices;
        }

        public ActionResult PromotionBox()
        {
            var model = new PromotionsViewModel(_cartService.GetCartById(_userService.UserGuid), _promotionsServices);
            return View("PromotionBox", model);
        }

        [HttpPost]
        public ActionResult AddPromoCode(PromotionsViewModel model)
        {
            if (!String.IsNullOrEmpty(model.NewPromoCode))
            {
                Promotion newPromo = _promotionsServices.getPromoByCode(model.NewPromoCode);
                if (newPromo == null)
                {
                    TempData["PROMO_ERROR"] = "Invalid Promotion Code: '" + model.NewPromoCode + "'";
                }
                else
                {
                    string promoValidMsg = _promotionsServices.isValid(newPromo, _userService.UserGuid);
                    if (String.IsNullOrEmpty(promoValidMsg))
                    {
                        CartHeader cartHeader = _cartService.GetCartById(_userService.UserGuid);
                        cartHeader.PromotionCode = model.NewPromoCode;
                        _cartService.SaveCartHeader(cartHeader);
                        cartHeader = _cartService.PromoGiftDiscounts(cartHeader);
                        _cartService.Calculate(cartHeader);
                        _cartService.SaveCartHeader(cartHeader);
                    } 
                    else
                    {
                        TempData["PROMO_ERROR"] = promoValidMsg;
                    }
                }
            }
            return RedirectToAction("Index", "Cart");
        }

        public ActionResult DelPromoCode(PromotionsViewModel model)
        {
            CartHeader cartHeader = _cartService.GetCartById(_userService.UserGuid);
            cartHeader.PromotionCode = null;
            _cartService.SaveCartHeader(cartHeader);
            _cartService.Calculate(cartHeader);
            _cartService.SaveCartHeader(cartHeader);
            return RedirectToAction("Index", "Cart");
        }
    }
}