﻿using System.Collections.Generic;
using System.Web.Mvc;
using Mailers;
using ViewModels;

namespace Controllers.Controls
{
    /// <summary>
    /// Summary description for MailProductPromotionController
    /// </summary>
    public class MailProductPromotionController : Controller
    {
        public MailProductPromotionController()
        {

        }

        public ActionResult MailPromotedProducts(MailPromotionContainer promotion)
        {
            return View(promotion);
        }
    }
}