﻿using System.Collections.Generic;
using System.Web.Mvc;
using EISCS.Shop.DO.Dto.Cart;
using EISCS.Shop.DO.Interface;
using EISCS.Shop.DO.Service;
using EISCS.Wrappers.Configuration;
using ViewModels;

namespace Controllers.Controls
{
    public class MostPopularController : Controller
    {
        private readonly IConfigurationObject _configuration;
        private readonly IExpanditUserService _expanditUserService;
        private readonly IProductService _productService;

        public MostPopularController(IProductService productService, IExpanditUserService expanditUserService, IConfigurationObject configuration)
        {
            _productService = productService;
            _expanditUserService = expanditUserService;
            _configuration = configuration;
        }

        public ActionResult PopularProducts()
        {
            List<ProductInfoNode> infos = null;
            CartHeader header = null;
            string strObject = _configuration.Read("SHOW_NUMBER_OF_MOST_POPULAR_ITEMS");
            int nrOfItems = ExpanditLib2.CLngEx(strObject);
            ProductInfoNodeContainer container = _productService.GetMostPopularProducts(nrOfItems);
            if (container != null)
            {
                infos = container.ProductInfoNodes;
                header = container.Header;
            }

            var model = new ProductsViewModel(infos, _expanditUserService, null, null, header, null);

            return View("_PopularProducts", model);
        }

        public ActionResult PopularProductsMail()
        {
            List<ProductInfoNode> infos = null;
            CartHeader header = null;
            string strObject = _configuration.Read("SHOW_NUMBER_OF_MOST_POPULAR_ITEMS");
            int nrOfItems = ExpanditLib2.CLngEx(strObject);
            ProductInfoNodeContainer container = _productService.GetMostPopularProducts(nrOfItems);
            if (container != null)
            {
                infos = container.ProductInfoNodes;
                header = container.Header;
            }

            var model = new ProductsViewModel(infos, _expanditUserService, null, null, header, null);

            return View("_PopularProductsMail", model);
        }
    }
}