﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net;
using System.Net.Http;
using System.Runtime.Caching;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace Filters
{
    public class HmacApiAuthenticationAttribute : AuthorizationFilterAttribute
    {
        private static readonly Dictionary<string, string> AllowedApps = new Dictionary<string, string>();
        private readonly UInt64 _requestMaxAgeInSeconds;
        private readonly string _authenticationScheme;

        public HmacApiAuthenticationAttribute()
        {
            var apiKey = ConfigurationManager.AppSettings["EXPANDIT-MAIL-API-KEY"];
            var appId = ConfigurationManager.AppSettings["EXPANDIT-MAIL-APP-ID"];
            _authenticationScheme = ConfigurationManager.AppSettings["EXPANDIT-MAIL-APP-SCHEME"];
            string seconds = ConfigurationManager.AppSettings["EXPANDIT-MAIL-APP-REQUEST-MAX-AGE-SECONDS"];
            _requestMaxAgeInSeconds = UInt64.Parse(seconds);
            if (AllowedApps.Count == 0)
            {
                AllowedApps.Add(appId, apiKey);
            }
        }

        public override void OnAuthorization(HttpActionContext actionContext)
        {
            var request = actionContext.Request;

            if (request.Headers.Authorization == null || !_authenticationScheme.Equals(request.Headers.Authorization.Scheme, StringComparison.OrdinalIgnoreCase))
            {
                Challenge(actionContext);
                return;
            }

            var rawHeader = request.Headers.Authorization.Parameter;
            var authorizedHeaders = GetAutherizationHeaderValues(rawHeader);

            if (authorizedHeaders == null)
            {
                Challenge(actionContext);
                return;
            }

            var appId = authorizedHeaders[0];
            var incomingBase64Signature = authorizedHeaders[1];
            var nonce = authorizedHeaders[2];
            var requestTimeStamp = authorizedHeaders[3];

            var isValid = IsValidRequest(request, appId, incomingBase64Signature, nonce, requestTimeStamp);

            if (isValid.Result)
            {
                return;
            }
            Challenge(actionContext);
        }

        private void Challenge(HttpActionContext actionContext)
        {
            var host = actionContext.Request.RequestUri.DnsSafeHost;
            actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized);
            actionContext.Response.Headers.Add("WWW-Authenticate", string.Format("Basic realm=\"{0}\"", host));
        }

        private async Task<bool> IsValidRequest(HttpRequestMessage req, string appId, string incomingBase64Signature, string nonce, string requestTimeStamp)
        {
            string requestContentBase64String = "";
            string requestUri = HttpUtility.UrlEncode(req.RequestUri.AbsoluteUri.ToLower());
            string requestHttpMethod = req.Method.Method;

            if (!AllowedApps.ContainsKey(appId))
            {
                return false;
            }

            var sharedKey = AllowedApps[appId];

            if (IsReplayRequest(nonce, requestTimeStamp))
            {
                return false;
            }

            byte[] hash = await ComputeHash(req.Content);

            if (hash != null)
            {
                requestContentBase64String = Convert.ToBase64String(hash);
            }

            string data = String.Format("{0}{1}{2}{3}{4}{5}", appId, requestHttpMethod, requestUri, requestTimeStamp, nonce, requestContentBase64String);

            var secretKeyBytes = Convert.FromBase64String(sharedKey);

            byte[] signature = Encoding.UTF8.GetBytes(data);

            using (HMACSHA256 hmac = new HMACSHA256(secretKeyBytes))
            {
                byte[] signatureBytes = hmac.ComputeHash(signature);

                return (incomingBase64Signature.Equals(Convert.ToBase64String(signatureBytes), StringComparison.Ordinal));
            }

        }

        private bool IsReplayRequest(string nonce, string requestTimeStamp)
        {
            if (MemoryCache.Default.Contains(nonce))
            {
                return true;
            }

            DateTime epochStart = new DateTime(1970, 01, 01, 0, 0, 0, 0, DateTimeKind.Utc);
            TimeSpan currentTs = DateTime.UtcNow - epochStart;

            var serverTotalSeconds = Convert.ToUInt64(currentTs.TotalSeconds);
            var requestTotalSeconds = Convert.ToUInt64(requestTimeStamp);

            if ((serverTotalSeconds - requestTotalSeconds) > _requestMaxAgeInSeconds)
            {
                return true;
            }

            MemoryCache.Default.Add(nonce, requestTimeStamp, DateTimeOffset.UtcNow.AddSeconds(_requestMaxAgeInSeconds));

            return false;
        }

        private static async Task<byte[]> ComputeHash(HttpContent httpContent)
        {
            using (MD5 md5 = MD5.Create())
            {
                byte[] hash = null;
                var content = await httpContent.ReadAsByteArrayAsync();
                if (content.Length != 0)
                {
                    hash = md5.ComputeHash(content);
                }
                return hash;
            }
        }


        public override bool AllowMultiple
        {
            get { return false; }
        }

        private string[] GetAutherizationHeaderValues(string rawAuthzHeader)
        {
            var credArray = rawAuthzHeader.Split(':');

            return credArray.Length == 4 ? credArray : null;
        }
    }
}
