﻿using System;
using System.Configuration;
using EISCS.Routing.Interfaces;
using EISCS.Routing.Providers;
using System.Web;
using ExpandIT.Logging;
using System.Globalization;
using System.Threading;
using System.ComponentModel;
using System.Web.Routing;

namespace ExpandIT
{
    public class Page : System.Web.UI.Page, IRoutablePage
    {

        // Page Properties
        protected string Description;
        protected string Keywords;
        protected string MRuntimeMasterPageFile = null;

        protected string MCrumbName = "";

        private bool _requiresSsl;
        /// <summary>
        /// Use property to set the page to use SSL
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        [Browsable(true)]
        [Description("Indicates if the page should use SSL or not")]
        public virtual bool RequireSsl
        {
            get { return _requiresSsl; }
            set { _requiresSsl = value; }
        }

        /// <summary>
        /// Handles Init event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks>Added 080825</remarks>
        protected void Page_Init(object sender, EventArgs e)
        {
            PushSsl();
        }

        /// <summary>
        /// Force request into the desired channel, http or https.
        /// </summary>
        private void PushSsl()
        {
            const string secure = "https://";
            const string unsecure = "http://";

            //Force required into secure channel
            if (RequireSsl && Request.IsSecureConnection == false)
            {
                Response.Redirect(Request.Url.ToString().Replace(unsecure, secure));
            }

            //Force non-required out of secure channel
            if (!RequireSsl && Request.IsSecureConnection)
            {
                Response.Redirect(Request.Url.ToString().Replace(secure, unsecure));
            }
        }

        public string RuntimeMasterPageFile
        {
            get { return MRuntimeMasterPageFile; }
            set { MRuntimeMasterPageFile = value; }
        }

        public string CrumbName
        {
            get { return MCrumbName; }
            set { MCrumbName = value; }
        }


        protected override void InitializeCulture()
        {
            string langGuid = GetLanguageGuid();

            try
            {
                CultureInfo ci = new CultureInfo(langGuid);

                Thread.CurrentThread.CurrentUICulture = ci;
            }
            catch (Exception)
            {
            }

            base.InitializeCulture();

        }

        protected string GetLanguageGuid()
        {
            string langGuid = string.Empty;

            //If RouteManager.Route.IsRoutingEnabled AndAlso RouteContext IsNot Nothing Then
            //    'Routing is enabled and the current page is using routing...
            //    Dim languageGuid As String = RouteManager.Route.GetLanguage(RouteContext)
            //    Try
            //        Session["LanguageGuid"] = languageGuid
            //        HttpContext.Current.Request.Cookies("user")("LanguageGuid") = languageGuid
            //    Catch ex As Exception
            //        FileLogger.log(ex.Message & " " & ex.StackTrace)
            //    End Try

            //End If
            // Try to get LanguageGuid from session
            if (HttpContext.Current.Session != null)
            {
                try
                {
                    langGuid = HttpContext.Current.Session["LanguageGuid"].ToString();
                }
                catch (Exception ex)
                {
                    FileLogger.log(ex.Message + " " + ex.StackTrace);
                }
            }

            // If session is empty then look if LanguageGuid is set in cookie
            if (string.IsNullOrEmpty(langGuid))
            {
                try
                {
                    langGuid = HttpContext.Current.Request.Cookies["user"]["LanguageGuid"];
                }
                catch (Exception ex)
                {
                    FileLogger.log(ex.Message + " " + ex.StackTrace);
                }
            }
            return langGuid;
        }

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if ((MRuntimeMasterPageFile != null))
            {
                if (string.IsNullOrEmpty(MRuntimeMasterPageFile))
                    MRuntimeMasterPageFile = "main.master";
                MasterPageFile = ConfigurationManager.AppSettings["MasterPageFolder"] + "/" + MRuntimeMasterPageFile;
            }
        }

        private void Page_Load(Object sender, EventArgs e)
        {
            // Set header information
            if (Page.Header != null)
            {
                Title = (string) HttpContext.GetGlobalResourceObject("Language", "SITE_TITLE");
            }
        }

        public RouteProviderBase ExpRoute
        {
            get { return RouteManager.Route; }
        }

        public RequestContext RouteContext { get; set; }

        public Page()
        {
            Load += Page_Load;
            PreInit += Page_PreInit;
            Init += Page_Init;
        }

    }
}