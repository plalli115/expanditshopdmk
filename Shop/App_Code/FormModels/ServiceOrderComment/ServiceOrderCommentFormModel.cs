﻿namespace FormModels.ServiceOrderComment
{
    public class ServiceOrderCommentFormModel
    {
        public string Comment { get; set; }
        public string ServiceOrderGuid { get; set; }
        public string UserGuid { get; set; }
        public string UserType { get; set; }
        public string PortalUserName { get; set; }
    }
}