﻿using System.ComponentModel.DataAnnotations;
using EISCS.Shop.Attributes;
using Resources;

namespace FormModels
{
    public class UserPasswordFormModel
    {
        [Display(ResourceType = typeof(Language), Name = "LABEL_USERDATA_PASSWORD")]
        [Required(ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "LABEL_STARMARKED_INPUT_IS_REQUIRED")]
        [PasswordStringLength(40, ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "MESSAGE_THE_VALUE_IN_FIELD_IS_NOT_CORRECT_1")]
        public string CurrentUserPassword { get; set; }

        [Display(ResourceType = typeof(Language), Name = "LABEL_USERDATA_NEW_PASSWORD")]
        [Required(ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "LABEL_STARMARKED_INPUT_IS_REQUIRED")]
        [PasswordStringLength(40, ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "MESSAGE_THE_VALUE_IN_FIELD_IS_NOT_CORRECT_1")]
        [DataType(DataType.Password)]
        public string NewUserPassword { get; set; }

        [Display(ResourceType = typeof(Language), Name = "LABEL_USERDATA_NEW_PASSWORD_CONFIRM")]
        [Required(ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "LABEL_STARMARKED_INPUT_IS_REQUIRED")]
        [PasswordStringLength(40, ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "MESSAGE_THE_VALUE_IN_FIELD_IS_NOT_CORRECT_1")]
        [DataType(DataType.Password)]
        [System.Web.Mvc.Compare("NewUserPassword", ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "MESSAGE_NEW_PASSWORDS_ARE_INCORRECT")]
        public string ConfirmedNewUserPassword { get; set; }

        public string Message { get; set; }

        public bool Success { get; set; }
    }
}