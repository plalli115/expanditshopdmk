﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using EISCS.Shop.Attributes;
using Resources;

namespace FormModels
{
    /// <summary>
    /// Summary description for RegisterCmsUserFormModel
    /// </summary>
    public class RegisterCmsUserFormModel
    {
        [StringLength(50, ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "MESSAGE_THE_VALUE_IN_FIELD_IS_TOO_LONG")]
        [Required(ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "ERROR_VALIDATION_CONTACTPERSON")]
        [Display(ResourceType = typeof(CmsLanguage), Name = "CATMAN_LABEL_USERNAME")]
        public string UserName { get; set; }

        [PasswordStringLength(40, ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "MESSAGE_THE_VALUE_IN_FIELD_IS_NOT_CORRECT_1")]
        [DataType(DataType.Password)]
        [Required(ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "ERROR_VALIDATION_PASSWORD")]
        public string Password { get; set; }

        [PasswordStringLength(40, ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "MESSAGE_THE_VALUE_IN_FIELD_IS_NOT_CORRECT_1")]
        [Required(ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "ERROR_VALIDATION_CONFIRM_PASSWORD")]
        [System.Web.Mvc.Compare("Password", ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "MESSAGE_THE_PASSWORDS_MUST_MATCH")]
        [DataType(DataType.Password)]
        [Display(ResourceType = typeof(Language), Name = "LABEL_USERDATA_CONFIRM_PASSWORD")]
        public string ConfirmedPassword { get; set; }

        public string RoleId { get; set; }
        public Dictionary<string, string> RoleList { get; set; }
    }
}