﻿using System.ComponentModel.DataAnnotations;
using System.Web;

namespace FormModels.ServiceOrderAttachment
{
    public class ServiceAttachmentFormModel
    {
        public string ServiceOrderGuid { get; set; }
        public int BASGuid { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Language), ErrorMessageResourceName = "SO_ERROR_ATTACHMENT")]
        public HttpPostedFileBase File { get; set; }

        [StringLength(800, ErrorMessageResourceType = typeof(Resources.Language), ErrorMessageResourceName = "SO_ERROR_ATTACHMENT_COMMENT")]
        public string AttachmentComment { get; set; }
    }
}