﻿using System;

namespace FormModels.Cms2.FormModels
{
    public class MailSettingsStaleCartFormModel
    {
        public bool SendReminderEmail { get; set; }
        public bool SendReminderEmailCurrentSetting { get; set; }
        public int SendAfterStalePeriod { get; set; }
        public int RepeatPeriod { get; set; }
        public TimeSpan SendTime { get; set; }
    }
}
