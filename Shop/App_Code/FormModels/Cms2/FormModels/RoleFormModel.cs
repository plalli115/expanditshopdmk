﻿using System.ComponentModel.DataAnnotations;
using Resources;

namespace FormModels.Cms2.FormModels
{
    public class RoleFormModel
    {
        [Required(ErrorMessageResourceType = typeof(CmsLanguage), ErrorMessageResourceName = "ERROR_VALIDATION_ROLE_ID")]
        public string RoleId { get; set; }

        [Required(ErrorMessageResourceType = typeof(CmsLanguage), ErrorMessageResourceName = "ERROR_VALIDATION_ROLE_DESCRIPTION")]
        public string RoleDescription { get; set; }

        public string ReadOnly { get; set; }
    }
}