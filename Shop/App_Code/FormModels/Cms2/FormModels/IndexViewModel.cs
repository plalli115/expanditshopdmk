﻿namespace FormModels.Cms2.FormModels
{
    /// <summary>
    /// Summary description for IndexViewModel
    /// </summary>
    public class IndexViewModel
    {
        public MailServerSettingsFormModel MailServerSettingsFormModel { get; set; }
        public MailSettingsGeneralFormModel GeneralMailSettingsFormModel { get; set; }
        public MailSettingsStaleCartFormModel StaleCartMailSettingsFormModel { get; set; }
    }
}