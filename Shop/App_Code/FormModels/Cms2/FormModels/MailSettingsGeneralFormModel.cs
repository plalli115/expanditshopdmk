﻿using System.ComponentModel.DataAnnotations;
using Resources;

namespace FormModels.Cms2.FormModels
{
    public class MailSettingsGeneralFormModel
    {
        [StringLength(50, ErrorMessageResourceType = typeof(Resources.Language), ErrorMessageResourceName = "MESSAGE_THE_VALUE_IN_FIELD_IS_TOO_LONG")]
        public string SendAsName { get; set; }

        [Required(ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "ERROR_VALIDATION_EMAIL")]
        [StringLength(50, ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "MESSAGE_THE_VALUE_IN_FIELD_IS_TOO_LONG")]
        [RegularExpression(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$",
            ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "ERROR_VALIDATION_EMAIL")]
        public string SendAsAddress { get; set; }
        
    
        [StringLength(50, ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "MESSAGE_THE_VALUE_IN_FIELD_IS_TOO_LONG")]
        [RegularExpression(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$",
            ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "ERROR_VALIDATION_EMAIL")]
    public string CC { get; set; }

    
        [StringLength(50, ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "MESSAGE_THE_VALUE_IN_FIELD_IS_TOO_LONG")]
        [RegularExpression(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$",
            ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "ERROR_VALIDATION_EMAIL")]
        public string BCC { get; set; }

        

    }
}
