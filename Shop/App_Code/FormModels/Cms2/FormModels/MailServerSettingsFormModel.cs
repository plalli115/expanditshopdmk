﻿namespace FormModels.Cms2.FormModels
{
    /// <summary>
    /// Summary description for MailServerSettingsFormModel
    /// </summary>
    public class MailServerSettingsFormModel
    {
        public bool ForceSsl { get; set; }
        public string Host { get; set; }
        public string Port { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}