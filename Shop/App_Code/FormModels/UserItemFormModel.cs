﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using EISCS.Shop.Attributes;
using EISCS.Shop.DO.Dto;
using EISCS.Shop.DO.Interface;
using EISCS.Shop.DO.Service;
using Resources;
using ViewModels.ExpandItMenu;

namespace FormModels
{
    public class UserItemFormModel
    {
        public ToolbarMenuViewModel ToolbarMenuViewModel { get; set; }
        
        public string UserGuid { get; set; }

        [Required(ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "ERROR_VALIDATION_CONTACTPERSON")]
        [StringLength(50, ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "MESSAGE_THE_VALUE_IN_FIELD_IS_TOO_LONG")]
        public string ContactName { get; set; }

		[Required(ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "ERROR_VALIDATION_EMAIL")]
        [RegularExpression(@"^[-!#$%&'*+/0-9=?A-Z^_a-z{|}~](\.?[-!#$%&'*+/0-9=?A-Z^_a-z{|}~])*@[a-zA-Z](-?[a-zA-Z0-9])*(\.[a-zA-Z](-?[a-zA-Z0-9])*)+$", ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "MESSAGE_EMAIL_ADDRESS_NOT_VALID")]
        [StringLength(50, ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "MESSAGE_THE_VALUE_IN_FIELD_IS_TOO_LONG")]
        public string EmailAddress { get; set; }

        public string CustomerGuid { get; set; }

        [StringLength(50, ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "MESSAGE_THE_VALUE_IN_FIELD_IS_TOO_LONG")]
        public string CompanyName { get; set; }

        [StringLength(50, ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "MESSAGE_THE_VALUE_IN_FIELD_IS_TOO_LONG")]
        public string CountryGuid { get; set; }

        public string CurrencyGuid { get; set; }

        public string SecondaryCurrencyGuid { get; set; }

		[Required(ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "ERROR_VALIDATION_ADDRESS")]
        [StringLength(50, ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "MESSAGE_THE_VALUE_IN_FIELD_IS_TOO_LONG")]
        public string Address1 { get; set; }

        [StringLength(50, ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "MESSAGE_THE_VALUE_IN_FIELD_IS_TOO_LONG")]
        public string Address2 { get; set; }

        [StringLength(50, ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "MESSAGE_THE_VALUE_IN_FIELD_IS_TOO_LONG")]
        public string CityName { get; set; }

        [StringLength(30, ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "MESSAGE_THE_VALUE_IN_FIELD_IS_TOO_LONG")]
        public string PhoneNo { get; set; }

		[Required(ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "ERROR_VALIDATION_ZIP")]
        [StringLength(50, ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "MESSAGE_THE_VALUE_IN_FIELD_IS_TOO_LONG")]
        public string ZipCode { get; set; }

        [StringLength(50, ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "MESSAGE_THE_VALUE_IN_FIELD_IS_TOO_LONG")]
        public string CountryName { get; set; }

        public string LanguageGuid { get; set; }

        public Dictionary<string, string> CountryList { get; set; }
        public Dictionary<string, string> LanguageList { get; set; }
        public Dictionary<string, string> CurrencyList { get; set; }

        public DateTime UserCreated { get; set; }
        public DateTime UserModified { get; set; }

        [Required(ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "ERROR_VALIDATION_LOGIN")]
        [StringLength(20, ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "MESSAGE_THE_VALUE_IN_FIELD_IS_TOO_LONG")]
        public string UserLogin { get; set; }

        [Required(ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "ERROR_VALIDATION_PASSWORD")]
        [DataType(DataType.Password)]
        [PasswordStringLength(40, ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "MESSAGE_THE_VALUE_IN_FIELD_IS_NOT_CORRECT_1")]
        public string UserPassword { get; set; }

        public bool isB2B { get; set; }
        public bool IsSuccess { get; set; }

        public bool HasAccess(string accessClass)
        {
            IExpanditUserService userService = DependencyResolver.Current.GetService<IExpanditUserService>();
            return userService.UserAccess(accessClass);
        }
    }
}