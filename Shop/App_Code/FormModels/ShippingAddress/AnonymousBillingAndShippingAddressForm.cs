﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Resources;

namespace FormModels.ShippingAddress
{
	public class AnonymousBillingAndShippingAddressForm
	{
		[Required(ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "ERROR_VALIDATION_ADDRESS")]
        [StringLength(50, ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "MESSAGE_THE_VALUE_IN_FIELD_IS_TOO_LONG")]
		public string Address1 { get; set; }

        [StringLength(50, ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "MESSAGE_THE_VALUE_IN_FIELD_IS_TOO_LONG")]
		public string Address2 { get; set; }

		[Required(ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "ERROR_VALIDATION_CITY")]
        [StringLength(50, ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "MESSAGE_THE_VALUE_IN_FIELD_IS_TOO_LONG")]
		public string CityName { get; set; }

        [StringLength(50, ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "MESSAGE_THE_VALUE_IN_FIELD_IS_TOO_LONG")]
		public string CompanyName { get; set; }

		[Required(ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "ERROR_VALIDATION_CONTACTPERSON")]
        [StringLength(50, ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "MESSAGE_THE_VALUE_IN_FIELD_IS_TOO_LONG")]
		public string ContactName { get; set; }

		[Required(ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "ERROR_VALIDATION_COUNTRY")]
		public string CountryGuid { get; set; }

		[StringLength(255)]
		public string CountryName { get; set; }

        [StringLength(50, ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "MESSAGE_THE_VALUE_IN_FIELD_IS_TOO_LONG")]
		public string CountyName { get; set; }

		[Required(ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "ERROR_VALIDATION_EMAIL")]
        [StringLength(50, ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "MESSAGE_THE_VALUE_IN_FIELD_IS_TOO_LONG")]
		public string EmailAddress { get; set; }

        [StringLength(50, ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "MESSAGE_THE_VALUE_IN_FIELD_IS_TOO_LONG")]
		public string StateName { get; set; }

		[Required(ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "ERROR_VALIDATION_ZIP")]
        [StringLength(50, ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "MESSAGE_THE_VALUE_IN_FIELD_IS_TOO_LONG")]
		public string ZipCode { get; set; }

		[Required(ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "ERROR_VALIDATION_ADDRESS")]
        [StringLength(50, ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "MESSAGE_THE_VALUE_IN_FIELD_IS_TOO_LONG")]
		public string ShipToAddress1 { get; set; }

        [StringLength(50, ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "MESSAGE_THE_VALUE_IN_FIELD_IS_TOO_LONG")]
		public string ShipToAddress2 { get; set; }

		[Required(ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "ERROR_VALIDATION_CITY")]
        [StringLength(50, ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "MESSAGE_THE_VALUE_IN_FIELD_IS_TOO_LONG")]
		public string ShipToCityName { get; set; }

        [StringLength(50, ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "MESSAGE_THE_VALUE_IN_FIELD_IS_TOO_LONG")]
		public string ShipToCompanyName { get; set; }

		[Required(ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "SO_ERROR_VALIDATION_CONTACTPERSON")]
        [StringLength(50, ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "MESSAGE_THE_VALUE_IN_FIELD_IS_TOO_LONG")]
		public string ShipToContactName { get; set; }

		[Required(ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "ERROR_VALIDATION_COUNTRY")]
		public string ShipToCountryGuid { get; set; }

        [StringLength(255, ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "MESSAGE_THE_VALUE_IN_FIELD_IS_TOO_LONG")]
		public string ShipToCountryName { get; set; }

        [StringLength(50, ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "MESSAGE_THE_VALUE_IN_FIELD_IS_TOO_LONG")]
		public string ShipToCountyName { get; set; }

		[Required(ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "ERROR_VALIDATION_EMAIL")]
        [StringLength(50, ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "MESSAGE_THE_VALUE_IN_FIELD_IS_TOO_LONG")]
		public string ShipToEmailAddress { get; set; }

        [StringLength(50, ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "MESSAGE_THE_VALUE_IN_FIELD_IS_TOO_LONG")]
		public string ShipToStateName { get; set; }

		[Required(ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "ERROR_VALIDATION_ZIP")]
        [StringLength(50, ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "MESSAGE_THE_VALUE_IN_FIELD_IS_TOO_LONG")]
		public string ShipToZipCode { get; set; }

		public Dictionary<string, string> CountryList { get; set; }
	}
}