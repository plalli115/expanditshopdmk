﻿using System.ComponentModel.DataAnnotations;
using Resources;

namespace FormModels
{
    /// <summary>
    /// Summary description for CompanyInfoFormModel
    /// </summary>
    public class CompanyInfoFormModel
    {
        public string CompanyGuid { get; set; }

        [Display(ResourceType = typeof(Language), Name = "LABEL_USERDATA_COMPANY")]
        [StringLength(50, ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "MESSAGE_THE_VALUE_IN_FIELD_IS_TOO_LONG")]
        public string CompanyName { get; set; }
        
        //[Display(ResourceType = typeof(Language), Name = "LABEL_COMPANY_VAT_NO")]
        //public string CompanyVATNo { get; set; }
        
        [StringLength(20, ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "MESSAGE_THE_VALUE_IN_FIELD_IS_TOO_LONG")]
        [Display(ResourceType = typeof(Language), Name = "LABEL_FAXNO")]
        public string FaxNo { get; set; }
        
        [StringLength(15, ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "MESSAGE_THE_VALUE_IN_FIELD_IS_TOO_LONG")]
        [Display(ResourceType = typeof(Language), Name = "LABEL_USERDATA_ZIPCODE")]
        public string ZipCode { get; set; }

        [StringLength(250, ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "MESSAGE_THE_VALUE_IN_FIELD_IS_TOO_LONG")]
        [Display(ResourceType = typeof(Language), Name = "LABEL_EMAIL_ADDRESS")]
        public string EmailAddress { get; set; }

        [StringLength(250, ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "MESSAGE_THE_VALUE_IN_FIELD_IS_TOO_LONG")]
        [Display(ResourceType = typeof(Language), Name = "LABEL_WEBSITE")]
        public string HomePage { get; set; }

        [StringLength(38, ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "MESSAGE_THE_VALUE_IN_FIELD_IS_TOO_LONG")]
        [Display(ResourceType = typeof(Language), Name = "LABEL_USERDATA_COUNTRY")]
        public string CountryGuid { get; set; }

        [StringLength(50, ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "MESSAGE_THE_VALUE_IN_FIELD_IS_TOO_LONG")]
        [Display(ResourceType = typeof(Language), Name = "LABEL_COUNTRY_CODE")]
        public string CountryName { get; set; }

        [StringLength(200, ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "MESSAGE_THE_VALUE_IN_FIELD_IS_TOO_LONG")]
        [Display(ResourceType = typeof(Language), Name = "LABEL_USERDATA_ADDRESS_1")]
        public string Address1 { get; set; }

        [StringLength(200, ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "MESSAGE_THE_VALUE_IN_FIELD_IS_TOO_LONG")]
        [Display(ResourceType = typeof(Language), Name = "LABEL_USERDATA_ADDRESS_2")]
        public string Address2 { get; set; }

        [StringLength(50, ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "MESSAGE_THE_VALUE_IN_FIELD_IS_TOO_LONG")]
        [Display(ResourceType = typeof(Language), Name = "LABEL_USERDATA_CITY")]
        public string CityName { get; set; }

        [StringLength(20, ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "MESSAGE_THE_VALUE_IN_FIELD_IS_TOO_LONG")]
        [Display(ResourceType = typeof(Language), Name = "LABEL_PHONENO")]
        public string PhoneNo { get; set; }

        public bool IsSuccess { get; set; }
    }
}