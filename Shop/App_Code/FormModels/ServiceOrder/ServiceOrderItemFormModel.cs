using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using EISCS.Shop.DO.BAS.Dto;
using EISCS.Shop.DO.Dto;
using Resources;
using ViewModels.PortalStatus;

namespace FormModels.ServiceOrder
{
    public class ServiceOrderItemFormModel
    {
        public ServiceOrderItemFormModel()
        {
            CustomerAddressList = new Dictionary<string, string>();
            Projects = new Dictionary<string, string>();
            Tasks = new Dictionary<string, string>();
            Technicians = new Dictionary<string, string>();
            DepartmentItems = new Dictionary<string, string>();
            ServiceManagers = new Dictionary<string, string>();
            ServiceOrderStatusList = Enumerable.Empty<PortalStatusViewModel>();
        }

        public bool IsInEditMode { get; set; }

        public string SelectedCustomerId { get; set; }
        public string SelectedCustomerAddressId { get; set; }
        public string ServiceOrderGuid { get; set; }

        public ServiceItem CurrentServiceItem { get; set; }
        public CustomerItem CurrentShopCustomer { get; set; }
        public PortalCustomerItem CurrentPortalCustomer { get; set; }
        public PortalStatusItem CurrentStatusItem { get; set; }

        public UserTable User { get; set; }
        public ServiceOrderCommentLineItem ServiceOrderCommentLineItem { get; set; }
        
        public Dictionary<string, string> DepartmentItems { get; set; }
        public Dictionary<string, string> CustomerAddressList { get; set; }
        public Dictionary<string, string> Projects { get; set; }
        public Dictionary<string, string> Tasks { get; set; }
        public Dictionary<string, string> Technicians { get; set; }

        public string SelectedTechnician { get; set; }

        [Display(ResourceType = typeof(Language), Name = "SO_LABEL_SERVICEMANAGER")]
        public Dictionary<string, string> ServiceManagers { get; set; }

        [Display(ResourceType = typeof (Language), Name = "SO_LABEL_SERVICEORDER_STARTDATE")]
        public DateTime? SelectedStartDate { get; set; }

        [Display(ResourceType = typeof (Language), Name = "SO_LABEL_SERVICEORDER_ENDDATE")]
        public DateTime? SelectedFinishDate { get; set; }

        [Display(ResourceType = typeof (Language), Name = "SO_LABEL_CONTACTPERSON")]
        public string ShiptoContactName { get; set; }

        [Display(ResourceType = typeof (Language), Name = "SO_LABEL_DELIVERYTOCOMPANY")]
        public string ShipToCompanyName { get; set; }

        [Display(ResourceType = typeof (Language), Name = "SO_LABEL_SHIPPINGADDRESS")]
        public string ShipToAddress { get; set; }

        [Display(ResourceType = typeof (Language), Name = "SO_LABEL_SHIPPINGADDRESS2")]
        public string ShipToAddress2 { get; set; }

        [Display(ResourceType = typeof (Language), Name = "SO_LABEL_SHIPPINGCITY")]
        public string ShipToCityName { get; set; }

        [Display(ResourceType = typeof (Language), Name = "SO_LABEL_SHIPPINGEMAIL")]
        public string ShiptoEmailAddress { get; set; }

        [Display(ResourceType = typeof (Language), Name = "SO_LABEL_SHIPPINGCUSTOMER")]
        public string ShipToCompanyName2 { get; set; }

        [Display(ResourceType = typeof (Language), Name = "SO_LABEL_SHIPPINGPHONE")]
        public string ShipToPhoneNo { get; set; }

        [Display(ResourceType = typeof (Language), Name = "SO_LABEL_SHIPPINGPHONE2")]
        public string ShipToPhoneNo2 { get; set; }

        [Display(ResourceType = typeof (Language), Name = "SO_LABEL_SHIPPINGZIP")]
        public string ShipToZipCode { get; set; }

        [Display(ResourceType = typeof (Language), Name = "SO_LABEL_SERVICEORDER_SHORTDESCRIPTION")]
        public string JobDescription { get; set; }

        [Display(ResourceType = typeof (Language), Name = "SO_LABEL_SERVICEORDER_LONGDESCRIPTION")]
        public string LongDescription { get; set; }

        [Display(ResourceType = typeof(Language), Name = "SO_LABEL_SERVICE_ORDER_EXISTING_SERVICE_ITEM")]
        public string ServiceItemNo { get; set; }

        [Display(ResourceType = typeof(Language), Name = "SO_LABEL_SERVICE_ORDER_SELECTED_SERVICE_ITEM")]
        public string ItemNo { get; set; }

        [Display(ResourceType = typeof(Language), Name = "SO_LABEL_SERVICE_ORDER_NEW_SERVICE_ITEM")]
        public string Description { get; set; }

        [Display(ResourceType = typeof(Language), Name = "SO_LABEL_CUSTOMERADDRESS")]
        public string SelectedCustomerDeliveryAddress { get; set; }
        
        [Display(ResourceType = typeof (Language), Name = "SO_LABEL_SERVICEORDERSTATUS")]
        public IEnumerable<PortalStatusViewModel> ServiceOrderStatusList { get; set; }

        [Display(ResourceType = typeof (Language), Name = "SO_LABEL_SERVICEORDERTASKTYPE")]
        public string SelectedTask { get; set; }

        [Display(ResourceType = typeof (Language), Name = "SO_LABEL_SERVICEORDERSTATUS")]
        public string SelectedStatus { get; set; }

        [Display(ResourceType = typeof (Language), Name = "SO_LABEL_DEPARTMENT")]
        public string SelectedDepartment { get; set; }
        
        [Display(ResourceType = typeof (Language), Name = "SO_LABEL_SERVICEMANAGER")]
        public string SelectedServiceManager { get; set; }

        [Display(ResourceType = typeof (Language), Name = "SO_LABEL_SERVICEORDER_PROJECT")]
        public string SelectedProject { get; set; }
        
        public IEnumerable<KeyValuePair<string, string>> StatusList()
        {
            // LINQ filter for all levels
            var statusCodes = ServiceOrderStatusList.Where(status => status.IsActive == false && status.AssignmentComplete == false && status.Display == true).OrderBy(x => x.SortIndex);
            switch (User.RoleId)
            {
                case "CustomerGroup":
                    return statusCodes.Where(x => x.AllowRoleExtended).ToDictionary(key => key.StatusGuid.Trim(), value => value.StatusName.Trim());
                case "Admin":
                    return statusCodes.ToDictionary(key => key.StatusGuid.Trim(), value => value.StatusName.Trim());
                case "Customer":
                    return statusCodes.Where(x => x.AllowRoleBasic).ToDictionary(key => key.StatusGuid.Trim(), value => value.StatusName.Trim());
            }

            return statusCodes.ToDictionary(key => key.StatusGuid.Trim(), value => value.StatusName.Trim());
        }
    }
}