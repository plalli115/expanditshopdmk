﻿using System;
using System.ComponentModel.DataAnnotations;
using Resources;

namespace FormModels.ServiceOrder
{
    public class ServiceOrderFormPostModel
    {
        public string ServiceOrderGuid { get; set; }
        

        [Required(ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "SO_ERROR_VALIDATION_STARTDATE")]
        public DateTime SelectedStartDate { get; set; }
        public DateTime? SelectedFinishDate { get; set; }

        [StringLength(50)]
        [Required(ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "SO_ERROR_VALIDATION_CONTACTPERSON")]
        public string ShiptoContactName { get; set; }

        [StringLength(60)]
        [Required(ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "SO_ERROR_VALIDATION_DELIVERYTOCOMPANY")]
        public string ShipToCompanyName { get; set; }

        [StringLength(50)]
        [Required(ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "SO_ERROR_VALIDATION_SHIPPINGADDRESS")]
        public string ShipToAddress { get; set; }

        [StringLength(50)]
        public string ShipToAddress2 { get; set; }

        [StringLength(60)]
        [Required(ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "SO_ERROR_VALIDATION_SHIPPINGCITY")]
        public string ShipToCityName { get; set; }

        [StringLength(80)]
        [Required(ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "SO_ERROR_VALIDATION_SHIPPINGEMAIL")]
        public string ShiptoEmailAddress { get; set; }

        [StringLength(60)]
        public string ShipToCompanyName2 { get; set; }

        public string ShipToPhoneNo { get; set; }
        public string ShipToPhoneNo2 { get; set; }
        public string ShipToZipCode { get; set; }

        [StringLength(50)]
        public string JobDescription { get; set; }

        [StringLength(800)]
        public string LongDescription { get; set; }

        //[Required(ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "SO_ERROR_VALIDATION_SERVICEORDER_TASK")]
        public string SelectedTask { get; set; }

        //[Required(ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "SO_ERROR_VALIDATION_SERVICEORDER_STATUS")]
        public string SelectedStatus { get; set; }

        //[Required(ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "SO_ERROR_MISSINGDEPARTMENT")]
        public string SelectedDepartment { get; set; }
        public string SelectedServiceManager { get; set; }
        public string SelectedProject { get; set; }
        public string SelectedTechnician { get; set; }
        
        public string SelectedCustomerId { get; set; }
        public string SelectedCustomerAddressId { get; set; }

        public string ServiceItemNo { get; set; }
        public string ItemNo { get; set; }
        public string CreateServiceItemDescription { get; set; }
        public string Description { get; set; }
    }
}