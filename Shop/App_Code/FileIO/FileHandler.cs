﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.Hosting;
using CmsPublic.DataProviders.Logging;
using EISCS.CMS.Build;
using EISCS.CMS.CentralManagement.Configuration.Util;

namespace FileIO
{
    ///<summary>
    ///</summary>
    public class FileHandler
    {
        ///<summary>
        ///</summary>
        ///<param name="relativePath"></param>
        ///<returns></returns>
        public string ReadFileFromRelativePath(string relativePath)
        {
            string path = RealtiveToAbsolute(relativePath);
            return ReadFile(path);
        }

        private string RealtiveToAbsolute(string relativePath)
        {
            string temp = relativePath.Replace("/", @"\");
            return AppDomain.CurrentDomain.BaseDirectory + temp;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fullPath"></param>
        /// <returns></returns>
        public string ReadFile(string fullPath)
        {
            string content;
            try
            {
                using (var sr = new StreamReader(File.OpenRead(fullPath)))
                {
                    content = sr.ReadToEnd();
                }
            }
            catch (Exception)
            {
                content = "Error reading content, could not open file. path = " + fullPath;
            }
            return content;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fullPath"></param>
        /// <param name="conditions"></param>
        /// <param name="endCondition"></param>
        /// <param name="isCsharp"></param>
        /// <returns></returns>
        public List<string> ReadFile(string fullPath, string[] conditions, string endCondition, out bool isCsharp)
        {
            isCsharp = false;
            var contents = new List<string>();
            string content = null;
            try
            {
                using (var sr = new StreamReader(File.OpenRead(fullPath)))
                {
                    while ((content = sr.ReadLine()) != null)
                    {
                        if (content.Contains("Page Language=\"C#\""))
                        {
                            isCsharp = true;
                        }
                        contents.AddRange(from s in conditions where content.Contains(s) select content);
                        if (content.Contains(endCondition))
                        {
                            break;
                        }
                    }
                }
            }
            catch (Exception)
            {
                //TODO: we do nothing in here since content is not used
                //perhaps its bad naming conent and contents    
                content = "Error reading content, could not open file. path = " + fullPath;
            }
            return contents;
        }


        ///<summary>
        ///</summary>
        ///<param name="relativePath"></param>
        ///<returns></returns>
        public bool DeleteFile(string relativePath)
        {
            if (IsFileExist(relativePath))
            {
                string path = RealtiveToAbsolute(relativePath);
                File.Delete(path);
                return true;
            }
            return false;
        }

        // relativePath is of type "/AppData/fileName"
        ///<summary>
        ///</summary>
        ///<param name="relativePath"></param>
        ///<returns></returns>
        public bool IsFileExist(string relativePath)
        {
            bool isFileExist = false;
            string path = RealtiveToAbsolute(relativePath);
            try
            {
                var info = new FileInfo(path);
                isFileExist = info.Exists;
            }
            catch (Exception ex)
            {
                FileLogger.Log("In IsFileExist: " + ex.Message);
            }
            return isFileExist;
        }

        // relativePath is of type "/AppData"
        ///<summary>
        ///</summary>
        ///<param name="relativePath"></param>
        ///<param name="fileName"></param>
        ///<param name="content"></param>
        ///<param name="errorMessage"></param>
        ///<returns></returns>
        public bool CreateFile(string relativePath, string fileName, string content, out string errorMessage)
        {
            errorMessage = null;
            string path = RealtiveToAbsolute(relativePath);

            path = path + "/" + fileName;
            bool isSuccess = false;
            try
            {
                var fs = new FileStream(path, FileMode.Create, FileAccess.Write, FileShare.ReadWrite);
                var fw = new StreamWriter(fs);
                try
                {
                    fw.WriteLine(content);
                    fw.Flush();
                    isSuccess = true;
                }
                catch (IOException)
                {
                    isSuccess = false;
                }
                finally
                {
                    fw.Close();
                    fs.Close();
                }
            }
            catch (UnauthorizedAccessException)
            {
                errorMessage = "UnauthorizedAccessException";
            }
            catch (Exception)
            {
                errorMessage = "Exception";
            }
            return isSuccess;
        }

        ///<summary>
        ///</summary>
        ///<param name="relativePath"></param>
        ///<param name="searchFilter"></param>
        ///<returns></returns>
        public List<string> ListAllFileNames(string relativePath, string searchFilter)
        {
            var fileList = new List<string>();
            string path = RealtiveToAbsolute(relativePath);
            var di = new DirectoryInfo(path);
            foreach (FileInfo fi in di.GetFiles(searchFilter))
            {
                fileList.Add(fi.Name);
            }
            return fileList;
        }

        ///<summary>
        ///</summary>
        ///<param name="relativePath"></param>
        ///<param name="searchFilter"></param>
        ///<returns></returns>
        public List<string> ListFullFileNames(string relativePath, string searchFilter)
        {
            var fileList = new List<string>();
            if (relativePath == null)
            {
                return fileList;
            }
            string path = RealtiveToAbsolute(relativePath);
            var di = new DirectoryInfo(path);
            try
            {
                foreach (FileInfo fi in di.GetFiles(searchFilter))
                {
                    fileList.Add(path + @"\" + fi.Name);
                }
            }
            catch (DirectoryNotFoundException exception)
            {
                FileLogger.Log(exception.Message);
            }
            return fileList;
        }

        ///<summary>
        ///</summary>
        ///<param name="relativePath"></param>
        ///<param name="searchFilter"></param>
        ///<returns></returns>
        public List<string> ListFiles(string relativePath, string searchFilter)
        {
            var fileList = new List<string>();
            DirectoryInfo di = GetDirectoryInfo(relativePath);
            foreach (DirectoryInfo subDi in di.GetDirectories())
            {
                fileList.Add("dir()=" + relativePath + "/" + subDi.Name);
            }
            foreach (FileInfo fi in di.GetFiles(searchFilter))
            {
                fileList.Add(relativePath + "/" + fi.Name);
            }
            return fileList;
        }

        protected DirectoryInfo GetDirectoryInfo(string relativePath)
        {
            return new DirectoryInfo(GetPath(relativePath));
        }

        protected string GetPath(string relativePath)
        {
            string virtualDirectory = ConfigUtil.OptionalVirtualDirectory();
            if (virtualDirectory != null)
            {
                string path = HostingEnvironment.MapPath("/" + virtualDirectory) + relativePath.Replace("/", @"\");
                return path;
            }
            return RealtiveToAbsolute(relativePath);
        }

        ///<summary>
        ///</summary>
        ///<param name="fileNamePath"></param>
        ///<returns></returns>
        public string GetImageInformationString(string fileNamePath)
        {
            var sb = new StringBuilder();
            string path = GetPath(fileNamePath);
            var fi = new FileInfo(path);

            try
            {
                Image img = Image.FromFile(path);
                string sWidth = img.Width.ToString();
                string sHeight = img.Height.ToString();

                sb.Append(fi.Name + "/n");
                sb.Append(CustomResourceHandler.CATMAN_LABEL_SIZE + ": " + fi.Length.ToString() + " bytes/n");
                sb.Append(CustomResourceHandler.CATMAN_LABEL_DIMENSIONS + ": " + sWidth + " x ");
                sb.Append(sHeight + " pixels");

                img.Dispose();
            }
            catch (FileNotFoundException)
            {
                return "/n/n";
            }
            return sb.ToString();
        }

        ///<summary>
        ///</summary>
        ///<param name="path"></param>
        ///<param name="stream"></param>
        public void WriteFileContent(string path, Stream stream)
        {
            try
            {
                const int length = 256;
                var buffer = new Byte[length];

                // write the required bytes
                using (var fs = new FileStream(path, FileMode.Create))
                {
                    int bytesRead;
                    do
                    {
                        bytesRead = stream.Read(buffer, 0, length);
                        fs.Write(buffer, 0, bytesRead);
                    } while (bytesRead == length);
                }

                stream.Dispose();
            }
            catch (Exception ex)
            {
                FileLogger.Log(ex.Message + "; " + ex.StackTrace);
            }
        }
    }
}