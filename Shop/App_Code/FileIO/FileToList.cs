﻿using System;
using System.Collections.Generic;
using System.IO;

namespace FileIO
{
    /// <summary>
    /// Summary description for FileToList
    /// </summary>
    public class FileToList
    {
        public static List<string> FileToStringList(string location)
        {
            List<string> strings = new List<string>();
            
            try
            {
                StreamReader ipFile = new StreamReader(location);
                while (!ipFile.EndOfStream)
                {
                    strings.Add(ipFile.ReadLine());
                }
                ipFile.Close();
                ipFile = null;
            }
            catch (Exception)
            {
                
            }
            
            return strings;
        } 
    }
}