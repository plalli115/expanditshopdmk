/*਍   吀甀攀猀搀愀礀Ⰰ 䐀攀挀攀洀戀攀爀 ㄀㌀Ⰰ ㈀　㄀㘀㄀㄀㨀㐀　㨀　㜀 䄀䴀ഀഀ
   User: nsc਍   匀攀爀瘀攀爀㨀 匀儀䰀ഀഀ
   Database: EIS52_LaserSales਍   䄀瀀瀀氀椀挀愀琀椀漀渀㨀 ഀഀ
*/਍ഀഀ
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/਍䈀䔀䜀䤀一 吀刀䄀一匀䄀䌀吀䤀伀一ഀഀ
SET QUOTED_IDENTIFIER ON਍匀䔀吀 䄀刀䤀吀䠀䄀䈀伀刀吀 伀一ഀഀ
SET NUMERIC_ROUNDABORT OFF਍匀䔀吀 䌀伀一䌀䄀吀开一唀䰀䰀开夀䤀䔀䰀䐀匀开一唀䰀䰀 伀一ഀഀ
SET ANSI_NULLS ON਍匀䔀吀 䄀一匀䤀开倀䄀䐀䐀䤀一䜀 伀一ഀഀ
SET ANSI_WARNINGS ON਍䌀伀䴀䴀䤀吀ഀഀ
BEGIN TRANSACTION਍䜀伀ഀഀ
ALTER TABLE dbo.ShopSalesHeader ADD਍ऀ倀爀漀洀漀琀椀漀渀䌀漀搀攀 瘀愀爀挀栀愀爀⠀㔀　⤀ 一唀䰀䰀ഀഀ
GO਍䄀䰀吀䔀刀 吀䄀䈀䰀䔀 搀戀漀⸀匀栀漀瀀匀愀氀攀猀䠀攀愀搀攀爀 匀䔀吀 ⠀䰀伀䌀䬀开䔀匀䌀䄀䰀䄀吀䤀伀一 㴀 吀䄀䈀䰀䔀⤀ഀഀ
GO਍䌀伀䴀䴀䤀吀ഀഀ
select Has_Perms_By_Name(N'dbo.ShopSalesHeader', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.ShopSalesHeader', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.ShopSalesHeader', 'Object', 'CONTROL') as Contr_Per 