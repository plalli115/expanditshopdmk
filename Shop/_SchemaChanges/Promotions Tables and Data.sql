USE [EIS52_2015W1];
GO
/****** Object:  Table [dbo].[PromotionItemEntry]    Script Date: Thu 12 15 2016 12:52:22 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PromotionItemEntry]') AND type in (N'U'))
DROP TABLE [dbo].[PromotionItemEntry]
GO
/****** Object:  Table [dbo].[PromotionEntries]    Script Date: Thu 12 15 2016 12:52:22 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PromotionEntries]') AND type in (N'U'))
DROP TABLE [dbo].[PromotionEntries]
GO
/****** Object:  Table [dbo].[Promotion]    Script Date: Thu 12 15 2016 12:52:22 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Promotion]') AND type in (N'U'))
DROP TABLE [dbo].[Promotion]
GO
/****** Object:  Table [dbo].[Promotion]    Script Date: Thu 12 15 2016 12:52:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Promotion]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Promotion](
	[PromotionGuid] [int] NULL,
	[PromotionCode] [nvarchar](20) NULL,
	[PromotionName] [nvarchar](30) NULL,
	[PromotionShortDescription] [nvarchar](125) NULL,
	[PromotionDescription] [nvarchar](250) NULL,
	[PromotionStartDate] [datetime] NULL,
	[PromotionEndDate] [datetime] NULL,
	[NoOfOpportunities] [int] NULL,
	[NoOfOpportunitiesPerUser] [int] NULL,
	[PromotionStatusCode] [nvarchar](20) NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[PromotionEntries]    Script Date: Thu 12 15 2016 12:52:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PromotionEntries]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[PromotionEntries](
	[PromotionGuid] [int] NULL,
	[LineNo] [int] NULL,
	[PromotionType] [int] NULL,
	[Code] [nvarchar](20) NULL,
	[SubCode] [nvarchar](20) NULL,
	[LinkedCode] [nvarchar](20) NULL,
	[DiscountPercentage] [float] NULL,
	[DiscountAmount] [float] NULL,
	[MaxDiscountAmount] [float] NULL,
	[AmountOrQtyRequired] [float] NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[PromotionItemEntry]    Script Date: Thu 12 15 2016 12:52:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PromotionItemEntry]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[PromotionItemEntry](
	[EntryNo] [int] NULL,
	[PromotionItemListCode] [nvarchar](20) NULL,
	[ItemNo] [nvarchar](20) NULL,
	[ItemDescription] [nvarchar](30) NULL
) ON [PRIMARY]
END
GO
INSERT [dbo].[Promotion] ([PromotionGuid], [PromotionCode], [PromotionName], [PromotionShortDescription], [PromotionDescription], [PromotionStartDate], [PromotionEndDate], [NoOfOpportunities], [NoOfOpportunitiesPerUser], [PromotionStatusCode]) VALUES (100, N'S5OFF', N'Single $5 Off', N'$5 off a single item', N'$5 off Product 93367', CAST(N'2016-01-01 00:00:00.000' AS DateTime), CAST(N'2050-12-31 23:59:59.000' AS DateTime), 100, 50, N'huh?')
INSERT [dbo].[Promotion] ([PromotionGuid], [PromotionCode], [PromotionName], [PromotionShortDescription], [PromotionDescription], [PromotionStartDate], [PromotionEndDate], [NoOfOpportunities], [NoOfOpportunitiesPerUser], [PromotionStatusCode]) VALUES (101, N'S30POFF', N'Single 30% Off', N'30% off a single item', N'30% off Product 93367', CAST(N'2016-01-01 00:00:00.000' AS DateTime), CAST(N'2050-12-31 23:59:59.000' AS DateTime), 100, 50, N'huh?')
INSERT [dbo].[Promotion] ([PromotionGuid], [PromotionCode], [PromotionName], [PromotionShortDescription], [PromotionDescription], [PromotionStartDate], [PromotionEndDate], [NoOfOpportunities], [NoOfOpportunitiesPerUser], [PromotionStatusCode]) VALUES (102, N'Lnk4Off', N'Linked $4 Off', N'$4 off linked item', N'$4 off Product 93367 when you buy Product 93011', CAST(N'2016-01-01 00:00:00.000' AS DateTime), CAST(N'2050-12-31 23:59:59.000' AS DateTime), 100, 50, N'huh?')
INSERT [dbo].[Promotion] ([PromotionGuid], [PromotionCode], [PromotionName], [PromotionShortDescription], [PromotionDescription], [PromotionStartDate], [PromotionEndDate], [NoOfOpportunities], [NoOfOpportunitiesPerUser], [PromotionStatusCode]) VALUES (103, N'6Off20', N'Invoice $6 off $20', N'Invoice $6 off $20', N'$6 off a subtotal of $20 or more', CAST(N'2016-01-01 00:00:00.000' AS DateTime), CAST(N'2050-12-31 23:59:59.000' AS DateTime), 100, 50, N'huh?')
INSERT [dbo].[Promotion] ([PromotionGuid], [PromotionCode], [PromotionName], [PromotionShortDescription], [PromotionDescription], [PromotionStartDate], [PromotionEndDate], [NoOfOpportunities], [NoOfOpportunitiesPerUser], [PromotionStatusCode]) VALUES (104, N'10POff20', N'Invoice 10% off $20', N'Invoice 10% off $20', N'10% off a subtotal of $20 or more', CAST(N'2016-01-01 00:00:00.000' AS DateTime), CAST(N'2050-12-31 23:59:59.000' AS DateTime), 100, 50, N'huh?')
INSERT [dbo].[Promotion] ([PromotionGuid], [PromotionCode], [PromotionName], [PromotionShortDescription], [PromotionDescription], [PromotionStartDate], [PromotionEndDate], [NoOfOpportunities], [NoOfOpportunitiesPerUser], [PromotionStatusCode]) VALUES (105, N'10List', N'$10 off $30 List', N'$10 off $30 List of Items', N'$10 off $30 spent on 93367, 93011, 43021, 93062', CAST(N'2016-01-01 00:00:00.000' AS DateTime), CAST(N'2050-12-31 23:59:59.000' AS DateTime), 100, 50, N'huh?')
INSERT [dbo].[Promotion] ([PromotionGuid], [PromotionCode], [PromotionName], [PromotionShortDescription], [PromotionDescription], [PromotionStartDate], [PromotionEndDate], [NoOfOpportunities], [NoOfOpportunitiesPerUser], [PromotionStatusCode]) VALUES (106, N'20PList', N'20% off $30 List', N'20% off $30 List of Items', N'20% off $30 spent on 93367, 93011, 43021, 93062', CAST(N'2016-01-01 00:00:00.000' AS DateTime), CAST(N'2050-12-31 23:59:59.000' AS DateTime), 100, 50, N'huh?')
INSERT [dbo].[Promotion] ([PromotionGuid], [PromotionCode], [PromotionName], [PromotionShortDescription], [PromotionDescription], [PromotionStartDate], [PromotionEndDate], [NoOfOpportunities], [NoOfOpportunitiesPerUser], [PromotionStatusCode]) VALUES (107, N'30Items', N'$30 off 3 items', N'$30 off 3 items of a group', N'$30 off 3 items in 93367, 93011, 43021, 93062', CAST(N'2016-01-01 00:00:00.000' AS DateTime), CAST(N'2050-12-31 23:59:59.000' AS DateTime), 100, 50, N'huh?')
INSERT [dbo].[Promotion] ([PromotionGuid], [PromotionCode], [PromotionName], [PromotionShortDescription], [PromotionDescription], [PromotionStartDate], [PromotionEndDate], [NoOfOpportunities], [NoOfOpportunitiesPerUser], [PromotionStatusCode]) VALUES (108, N'30AllItems', N'$30 off all items', N'$30 off all items of a group', N'$30 off buying all items in 93367, 93011, 43021, 93062', CAST(N'2016-01-01 00:00:00.000' AS DateTime), CAST(N'2050-12-31 23:59:59.000' AS DateTime), 100, 50, N'huh?')
INSERT [dbo].[Promotion] ([PromotionGuid], [PromotionCode], [PromotionName], [PromotionShortDescription], [PromotionDescription], [PromotionStartDate], [PromotionEndDate], [NoOfOpportunities], [NoOfOpportunitiesPerUser], [PromotionStatusCode]) VALUES (109, N'5Ship', N'$5 off Shipping', N'$5 off Shipping', N'$5 off Shipping', CAST(N'2016-01-01 00:00:00.000' AS DateTime), CAST(N'2050-12-31 23:59:59.000' AS DateTime), 100, 50, N'huh?')
INSERT [dbo].[Promotion] ([PromotionGuid], [PromotionCode], [PromotionName], [PromotionShortDescription], [PromotionDescription], [PromotionStartDate], [PromotionEndDate], [NoOfOpportunities], [NoOfOpportunitiesPerUser], [PromotionStatusCode]) VALUES (110, N'HalfShip', N'50% off Shipping', N'50% off Shipping', N'50% off Shipping', CAST(N'2016-01-01 00:00:00.000' AS DateTime), CAST(N'2050-12-31 23:59:59.000' AS DateTime), 100, 50, N'huh?')
INSERT [dbo].[Promotion] ([PromotionGuid], [PromotionCode], [PromotionName], [PromotionShortDescription], [PromotionDescription], [PromotionStartDate], [PromotionEndDate], [NoOfOpportunities], [NoOfOpportunitiesPerUser], [PromotionStatusCode]) VALUES (111, N'LinkShip', N'$2 off Shipping w/ Item', N'$2 off Shipping w/ 93062', N'$2 off Shipping w/ purchase of 93062', CAST(N'2016-01-01 00:00:00.000' AS DateTime), CAST(N'2050-12-31 23:59:59.000' AS DateTime), 100, 50, N'huh?')
INSERT [dbo].[Promotion] ([PromotionGuid], [PromotionCode], [PromotionName], [PromotionShortDescription], [PromotionDescription], [PromotionStartDate], [PromotionEndDate], [NoOfOpportunities], [NoOfOpportunitiesPerUser], [PromotionStatusCode]) VALUES (112, N'Gift', N'93062 with $40', N'93062 free with $40 purchase', N'93062 free with $40 purchase', CAST(N'2016-01-01 00:00:00.000' AS DateTime), CAST(N'2050-12-31 23:59:59.000' AS DateTime), 3, 2, N'huh?')
INSERT [dbo].[Promotion] ([PromotionGuid], [PromotionCode], [PromotionName], [PromotionShortDescription], [PromotionDescription], [PromotionStartDate], [PromotionEndDate], [NoOfOpportunities], [NoOfOpportunitiesPerUser], [PromotionStatusCode]) VALUES (113, N'GiftList', N'93062 with $30 on list', N'93062 with $30 on list', N'93062 with $30 on list', CAST(N'2016-01-01 00:00:00.000' AS DateTime), CAST(N'2050-12-31 23:59:59.000' AS DateTime), 100, 50, N'huh?')
INSERT [dbo].[Promotion] ([PromotionGuid], [PromotionCode], [PromotionName], [PromotionShortDescription], [PromotionDescription], [PromotionStartDate], [PromotionEndDate], [NoOfOpportunities], [NoOfOpportunitiesPerUser], [PromotionStatusCode]) VALUES (114, N'GiftQty', N'93061 with 3 bought', N'93061 with 3 bought', N'93061 with 3 bought', CAST(N'2016-01-01 00:00:00.000' AS DateTime), CAST(N'2050-12-31 23:59:59.000' AS DateTime), 100, 50, N'huh?')
INSERT [dbo].[Promotion] ([PromotionGuid], [PromotionCode], [PromotionName], [PromotionShortDescription], [PromotionDescription], [PromotionStartDate], [PromotionEndDate], [NoOfOpportunities], [NoOfOpportunitiesPerUser], [PromotionStatusCode]) VALUES (115, N'GiftAll', N'93061 with all group', N'93061 with all group', N'93061 with all group', CAST(N'2016-01-01 00:00:00.000' AS DateTime), CAST(N'2050-12-31 23:59:59.000' AS DateTime), 100, 50, N'huh?')
INSERT [dbo].[PromotionEntries] ([PromotionGuid], [LineNo], [PromotionType], [Code], [SubCode], [LinkedCode], [DiscountPercentage], [DiscountAmount], [MaxDiscountAmount], [AmountOrQtyRequired]) VALUES (101, 30000, 1, N'93062', NULL, NULL, 30, 0, 5, 0)
INSERT [dbo].[PromotionEntries] ([PromotionGuid], [LineNo], [PromotionType], [Code], [SubCode], [LinkedCode], [DiscountPercentage], [DiscountAmount], [MaxDiscountAmount], [AmountOrQtyRequired]) VALUES (100, 10000, 1, N'93367', NULL, NULL, 0, 5, 5, 0)
INSERT [dbo].[PromotionEntries] ([PromotionGuid], [LineNo], [PromotionType], [Code], [SubCode], [LinkedCode], [DiscountPercentage], [DiscountAmount], [MaxDiscountAmount], [AmountOrQtyRequired]) VALUES (101, 20000, 1, N'93367', NULL, NULL, 30, 0, 5, 0)
INSERT [dbo].[PromotionEntries] ([PromotionGuid], [LineNo], [PromotionType], [Code], [SubCode], [LinkedCode], [DiscountPercentage], [DiscountAmount], [MaxDiscountAmount], [AmountOrQtyRequired]) VALUES (102, 40000, 2, N'93367', NULL, N'93011', 0, 4, 4, 0)
INSERT [dbo].[PromotionEntries] ([PromotionGuid], [LineNo], [PromotionType], [Code], [SubCode], [LinkedCode], [DiscountPercentage], [DiscountAmount], [MaxDiscountAmount], [AmountOrQtyRequired]) VALUES (103, 50000, 5, NULL, NULL, NULL, 0, 6, 6, 20)
INSERT [dbo].[PromotionEntries] ([PromotionGuid], [LineNo], [PromotionType], [Code], [SubCode], [LinkedCode], [DiscountPercentage], [DiscountAmount], [MaxDiscountAmount], [AmountOrQtyRequired]) VALUES (104, 60000, 5, NULL, NULL, NULL, 10, 0, 5, 20)
INSERT [dbo].[PromotionEntries] ([PromotionGuid], [LineNo], [PromotionType], [Code], [SubCode], [LinkedCode], [DiscountPercentage], [DiscountAmount], [MaxDiscountAmount], [AmountOrQtyRequired]) VALUES (105, 70000, 6, N'MyGroup', NULL, NULL, 0, 10, 10, 30)
INSERT [dbo].[PromotionEntries] ([PromotionGuid], [LineNo], [PromotionType], [Code], [SubCode], [LinkedCode], [DiscountPercentage], [DiscountAmount], [MaxDiscountAmount], [AmountOrQtyRequired]) VALUES (106, 70000, 6, N'MyGroup', NULL, NULL, 20, 0, 10, 30)
INSERT [dbo].[PromotionEntries] ([PromotionGuid], [LineNo], [PromotionType], [Code], [SubCode], [LinkedCode], [DiscountPercentage], [DiscountAmount], [MaxDiscountAmount], [AmountOrQtyRequired]) VALUES (107, 80000, 7, N'MyGroup', NULL, NULL, 0, 30, 30, 3)
INSERT [dbo].[PromotionEntries] ([PromotionGuid], [LineNo], [PromotionType], [Code], [SubCode], [LinkedCode], [DiscountPercentage], [DiscountAmount], [MaxDiscountAmount], [AmountOrQtyRequired]) VALUES (108, 80000, 8, N'MyGroup', NULL, NULL, 30, 0, 30, NULL)
INSERT [dbo].[PromotionEntries] ([PromotionGuid], [LineNo], [PromotionType], [Code], [SubCode], [LinkedCode], [DiscountPercentage], [DiscountAmount], [MaxDiscountAmount], [AmountOrQtyRequired]) VALUES (109, 90000, 4, N'GENERIC', NULL, NULL, 0, 5, 5, NULL)
INSERT [dbo].[PromotionEntries] ([PromotionGuid], [LineNo], [PromotionType], [Code], [SubCode], [LinkedCode], [DiscountPercentage], [DiscountAmount], [MaxDiscountAmount], [AmountOrQtyRequired]) VALUES (110, 100000, 4, N'GENERIC', NULL, NULL, 0, 10, 0, NULL)
INSERT [dbo].[PromotionEntries] ([PromotionGuid], [LineNo], [PromotionType], [Code], [SubCode], [LinkedCode], [DiscountPercentage], [DiscountAmount], [MaxDiscountAmount], [AmountOrQtyRequired]) VALUES (111, 110000, 3, N'GENERIC', NULL, N'93062', 0, 2, 0, NULL)
INSERT [dbo].[PromotionEntries] ([PromotionGuid], [LineNo], [PromotionType], [Code], [SubCode], [LinkedCode], [DiscountPercentage], [DiscountAmount], [MaxDiscountAmount], [AmountOrQtyRequired]) VALUES (112, 120000, 9, NULL, N'93062', NULL, 0, 0, 0, 40)
INSERT [dbo].[PromotionEntries] ([PromotionGuid], [LineNo], [PromotionType], [Code], [SubCode], [LinkedCode], [DiscountPercentage], [DiscountAmount], [MaxDiscountAmount], [AmountOrQtyRequired]) VALUES (113, 130000, 10, N'MyGroup', N'93062', NULL, 0, 0, 0, 30)
INSERT [dbo].[PromotionEntries] ([PromotionGuid], [LineNo], [PromotionType], [Code], [SubCode], [LinkedCode], [DiscountPercentage], [DiscountAmount], [MaxDiscountAmount], [AmountOrQtyRequired]) VALUES (114, 140000, 11, N'MyGroup', N'93061', NULL, 0, 0, 0, 3)
INSERT [dbo].[PromotionEntries] ([PromotionGuid], [LineNo], [PromotionType], [Code], [SubCode], [LinkedCode], [DiscountPercentage], [DiscountAmount], [MaxDiscountAmount], [AmountOrQtyRequired]) VALUES (115, 150000, 12, N'MyGroup', N'93061', NULL, 0, 0, 0, NULL)
INSERT [dbo].[PromotionItemEntry] ([EntryNo], [PromotionItemListCode], [ItemNo], [ItemDescription]) VALUES (10000, N'MyGroup', N'93367', N'Item 1')
INSERT [dbo].[PromotionItemEntry] ([EntryNo], [PromotionItemListCode], [ItemNo], [ItemDescription]) VALUES (20000, N'MyGroup', N'93011', N'Item 2')
INSERT [dbo].[PromotionItemEntry] ([EntryNo], [PromotionItemListCode], [ItemNo], [ItemDescription]) VALUES (30000, N'MyGroup', N'43021', N'Item 3')
INSERT [dbo].[PromotionItemEntry] ([EntryNo], [PromotionItemListCode], [ItemNo], [ItemDescription]) VALUES (40000, N'MyGroup', N'93062', N'Item 4')
