USE [EIS52_2015W1]
GO
/****** Object:  Table [dbo].[EECCEncryptionMask]    Script Date: Mon 10 3 2016 8:21:31 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EECCEncryptionMask]') AND type in (N'U'))
DROP TABLE [dbo].[EECCEncryptionMask]
GO
/****** Object:  Table [dbo].[EECCEncryptionMask]    Script Date: Mon 10 3 2016 8:21:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EECCEncryptionMask]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EECCEncryptionMask](
	[CreditCardNoLength] [int] NULL,
	[CreditCardNoMask] [nvarchar](30) NULL,
	[MonthMask] [nvarchar](2) NULL,
	[YearMask] [nvarchar](4) NULL
) ON [PRIMARY]
END
GO
INSERT [dbo].[EECCEncryptionMask] ([CreditCardNoLength], [CreditCardNoMask], [MonthMask], [YearMask]) VALUES (13, N'XXXXXXXXX-####', N'##', N'####')
INSERT [dbo].[EECCEncryptionMask] ([CreditCardNoLength], [CreditCardNoMask], [MonthMask], [YearMask]) VALUES (14, N'XXXXXXXXXX-####', N'##', N'####')
INSERT [dbo].[EECCEncryptionMask] ([CreditCardNoLength], [CreditCardNoMask], [MonthMask], [YearMask]) VALUES (15, N'XXXXXXXXXXX-####', N'##', N'####')
INSERT [dbo].[EECCEncryptionMask] ([CreditCardNoLength], [CreditCardNoMask], [MonthMask], [YearMask]) VALUES (16, N'XXXXXXXXXXXX-####', N'##', N'####')
