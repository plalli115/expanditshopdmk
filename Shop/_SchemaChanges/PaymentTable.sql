USE [EIS52_2015W1]
GO

/****** Object:  Table [dbo].[PaymentTable]    Script Date: Fri 9 30 2016 4:52:55 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[PaymentTable](
	[CustomerReference] [nvarchar](50) NULL,
	[PaymentGuid] [nvarchar](50) NOT NULL,
	[PaymentReference] [nvarchar](10) NULL,
	[PaymentDate] [datetime] NULL,
	[UserGuid] [nvarchar](50) NULL,
	[DocumentGuid] [nvarchar](50) NULL,
	[LedgerPayment] [bit] NULL,
	[PaymentType] [nvarchar](10) NULL,
	[PaymentTransactionID] [nvarchar](50) NULL,
	[PaymentTransactionAmount] [float] NULL,
	[PaymentTransactionSignature] [nvarchar](50) NULL,
	[PaymentAddress1No] [nvarchar](50) NULL,
	[PaymentAddress1St] [nvarchar](50) NULL,
	[PaymentAddress2] [nvarchar](50) NULL,
	[PaymentCity] [nvarchar](50) NULL,
	[PaymentCountry] [nvarchar](50) NULL,
	[PaymentEmail] [nvarchar](50) NULL,
	[PaymentState] [nvarchar](50) NULL,
	[PaymentZipCode] [nvarchar](50) NULL,
	[PaymentPhone] [nvarchar](50) NULL,
	[PaymentEncryptedData] [nvarchar](max) NULL,
	[PaymentFName] [nvarchar](50) NULL,
	[PaymentLName] [nvarchar](50) NULL,
	[PaymentLastNumbers] [nvarchar](50) NULL,
	[PaymentCardType] [nvarchar](50) NULL,
	[BankAccountNum] [nvarchar](150) NULL,
	[BankAccountOwner] [nvarchar](150) NULL,
	[BankRegNum] [nvarchar](150) NULL,
	[OrderCompleted] [bit] NULL,
	[Instance] [nvarchar](20) NULL,
	[PaymentCCMonth] [nvarchar](2) NULL,
	[PaymentCCYear] [nvarchar](4) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO


