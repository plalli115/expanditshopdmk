USE [EIS52_2015W1]
GO
/****** Object:  Table [dbo].[EECCGatewayAdditionalKeys]    Script Date: Mon 10 3 2016 8:21:31 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EECCGatewayAdditionalKeys]') AND type in (N'U'))
DROP TABLE [dbo].[EECCGatewayAdditionalKeys]
GO
/****** Object:  Table [dbo].[EECCGatewayAdditionalKeys]    Script Date: Mon 10 3 2016 8:21:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EECCGatewayAdditionalKeys]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EECCGatewayAdditionalKeys](
	[GatewayCode] [nvarchar](10) NULL,
	[ConfiguredGateway] [nvarchar](20) NULL,
	[KeyValue] [nvarchar](150) NULL,
	[KeyCode] [nvarchar](50) NULL,
	[TransactionType] [int] NULL,
	[TestOnlyKey] [int] NULL,
	[GatewayDataDirection] [int] NULL
) ON [PRIMARY]
END
GO
INSERT [dbo].[EECCGatewayAdditionalKeys] ([GatewayCode], [ConfiguredGateway], [KeyValue], [KeyCode], [TransactionType], [TestOnlyKey], [GatewayDataDirection]) VALUES (N'1', N'', N'9U62Z228Yz3SpdzB', N'x_tran_key', 1, 0, 0)
INSERT [dbo].[EECCGatewayAdditionalKeys] ([GatewayCode], [ConfiguredGateway], [KeyValue], [KeyCode], [TransactionType], [TestOnlyKey], [GatewayDataDirection]) VALUES (N'1', N'', N'74V2A76Kmazn4vTn', N'x_tran_key', 1, 1, 0)
INSERT [dbo].[EECCGatewayAdditionalKeys] ([GatewayCode], [ConfiguredGateway], [KeyValue], [KeyCode], [TransactionType], [TestOnlyKey], [GatewayDataDirection]) VALUES (N'205', N'', N'APIUserTest', N'USERID', 1, 0, 0)
INSERT [dbo].[EECCGatewayAdditionalKeys] ([GatewayCode], [ConfiguredGateway], [KeyValue], [KeyCode], [TransactionType], [TestOnlyKey], [GatewayDataDirection]) VALUES (N'33', N'', N'C:\EEPaymentManager\1909043030.pem', N'SSLCERT', 1, 2, 0)
INSERT [dbo].[EECCGatewayAdditionalKeys] ([GatewayCode], [ConfiguredGateway], [KeyValue], [KeyCode], [TransactionType], [TestOnlyKey], [GatewayDataDirection]) VALUES (N'46', N'', N'Us+JZngcmDKskCk+D2eGeLFdAXlD0MhrrbG/V0x4PaFh0TG2iO+rK1OEu8eXygnkQZsPExY8BQDBpll6XbOpAkAom8yXk2nG57yV0O/afutxTKqZ3WkfNrXkgaqzyu3cy/TsLOuBbmf4RD+T6fMiX', N'EXP_PASSWORD_EXT_1', 1, 2, 0)
INSERT [dbo].[EECCGatewayAdditionalKeys] ([GatewayCode], [ConfiguredGateway], [KeyValue], [KeyCode], [TransactionType], [TestOnlyKey], [GatewayDataDirection]) VALUES (N'46', N'', N'RMrxMNLNGHjbxw57Oeh2QuhUSO4RNhNEYmCK3elO4Z4sV0BeUPQyGutsb9XTHg9oWHRMbaI76srU4S7x5fKCeRBmw8TFjwFAMGmWXpds6kCQCibzJeTacbnvJXQ79p+63FMqpndaR82teSBqrPK7d', N'EXP_PASSWORD_EXT_2', 1, 2, 0)
INSERT [dbo].[EECCGatewayAdditionalKeys] ([GatewayCode], [ConfiguredGateway], [KeyValue], [KeyCode], [TransactionType], [TestOnlyKey], [GatewayDataDirection]) VALUES (N'46', N'', N'zL9Ows64FuZ/hEP5Pp8yJdEyvEw0s0YeNvHDns56HZCw==', N'EXP_PASSWORD_EXT_3', 1, 2, 0)
INSERT [dbo].[EECCGatewayAdditionalKeys] ([GatewayCode], [ConfiguredGateway], [KeyValue], [KeyCode], [TransactionType], [TestOnlyKey], [GatewayDataDirection]) VALUES (N'1', N'AUTH.NET EXP TEST', N'9U62Z228Yz3SpdzB', N'x_tran_key', 1, 0, 0)
INSERT [dbo].[EECCGatewayAdditionalKeys] ([GatewayCode], [ConfiguredGateway], [KeyValue], [KeyCode], [TransactionType], [TestOnlyKey], [GatewayDataDirection]) VALUES (N'1', N'AUTH.NET EXP TEST 2', N'9U62Z228Yz3SpdzB', N'x_tran_key', 1, 2, 0)
INSERT [dbo].[EECCGatewayAdditionalKeys] ([GatewayCode], [ConfiguredGateway], [KeyValue], [KeyCode], [TransactionType], [TestOnlyKey], [GatewayDataDirection]) VALUES (N'1', N'AUTH.NET TEST 3', N'9U62Z228Yz3SpdzB', N'x_tran_key', 1, 2, 0)
INSERT [dbo].[EECCGatewayAdditionalKeys] ([GatewayCode], [ConfiguredGateway], [KeyValue], [KeyCode], [TransactionType], [TestOnlyKey], [GatewayDataDirection]) VALUES (N'69', N'EPDQ-CUST', N'https://mdepayments.epdq.co.uk/ncol/test/', N'!#GATEWAY', 1, 2, 0)
INSERT [dbo].[EECCGatewayAdditionalKeys] ([GatewayCode], [ConfiguredGateway], [KeyValue], [KeyCode], [TransactionType], [TestOnlyKey], [GatewayDataDirection]) VALUES (N'69', N'EPDQ-CUST', N'bv2009', N'USERID', 1, 2, 0)
INSERT [dbo].[EECCGatewayAdditionalKeys] ([GatewayCode], [ConfiguredGateway], [KeyValue], [KeyCode], [TransactionType], [TestOnlyKey], [GatewayDataDirection]) VALUES (N'33', N'FIRSTDATA', N'C:\EEPaymentManager\1909043030.pem', N'SSLCERT', 1, 0, 0)
INSERT [dbo].[EECCGatewayAdditionalKeys] ([GatewayCode], [ConfiguredGateway], [KeyValue], [KeyCode], [TransactionType], [TestOnlyKey], [GatewayDataDirection]) VALUES (N'33', N'FIRSTDATA', N'', N'tdate', 2, 0, 1)
INSERT [dbo].[EECCGatewayAdditionalKeys] ([GatewayCode], [ConfiguredGateway], [KeyValue], [KeyCode], [TransactionType], [TestOnlyKey], [GatewayDataDirection]) VALUES (N'33', N'FIRSTDATATEST', N'C:\EEPaymentManager\1909043030.pem', N'SSLCERT', 1, 2, 0)
INSERT [dbo].[EECCGatewayAdditionalKeys] ([GatewayCode], [ConfiguredGateway], [KeyValue], [KeyCode], [TransactionType], [TestOnlyKey], [GatewayDataDirection]) VALUES (N'10', N'PAYFLOW EXP TEST', N'PayPal', N'PARTNER', 1, 2, 0)
INSERT [dbo].[EECCGatewayAdditionalKeys] ([GatewayCode], [ConfiguredGateway], [KeyValue], [KeyCode], [TransactionType], [TestOnlyKey], [GatewayDataDirection]) VALUES (N'6', N'PAYFLOW2', N'PayPal', N'PARTNER', 1, 0, 0)
INSERT [dbo].[EECCGatewayAdditionalKeys] ([GatewayCode], [ConfiguredGateway], [KeyValue], [KeyCode], [TransactionType], [TestOnlyKey], [GatewayDataDirection]) VALUES (N'55', N'SAGE-ENCRYPT', N'', N'ApprovalCode', 1, 0, 1)
INSERT [dbo].[EECCGatewayAdditionalKeys] ([GatewayCode], [ConfiguredGateway], [KeyValue], [KeyCode], [TransactionType], [TestOnlyKey], [GatewayDataDirection]) VALUES (N'55', N'SAGE-ENCRYPT', N'', N'RelatedSecurityKey', 1, 0, 1)
INSERT [dbo].[EECCGatewayAdditionalKeys] ([GatewayCode], [ConfiguredGateway], [KeyValue], [KeyCode], [TransactionType], [TestOnlyKey], [GatewayDataDirection]) VALUES (N'55', N'SAGE-ENCRYPT', N'', N'RelatedTxAuthNo', 1, 0, 1)
INSERT [dbo].[EECCGatewayAdditionalKeys] ([GatewayCode], [ConfiguredGateway], [KeyValue], [KeyCode], [TransactionType], [TestOnlyKey], [GatewayDataDirection]) VALUES (N'55', N'SAGE-ENCRYPT', N'', N'SecurityKey', 1, 0, 1)
INSERT [dbo].[EECCGatewayAdditionalKeys] ([GatewayCode], [ConfiguredGateway], [KeyValue], [KeyCode], [TransactionType], [TestOnlyKey], [GatewayDataDirection]) VALUES (N'55', N'SAGE-ENCRYPT', N'', N'TxAuthNo', 1, 0, 1)
INSERT [dbo].[EECCGatewayAdditionalKeys] ([GatewayCode], [ConfiguredGateway], [KeyValue], [KeyCode], [TransactionType], [TestOnlyKey], [GatewayDataDirection]) VALUES (N'55', N'SAGE-ENCRYPT', N'3.00', N'VPSProtocol', 1, 0, 0)
INSERT [dbo].[EECCGatewayAdditionalKeys] ([GatewayCode], [ConfiguredGateway], [KeyValue], [KeyCode], [TransactionType], [TestOnlyKey], [GatewayDataDirection]) VALUES (N'33', N'TEMPFIRSTDATA', N'C:\EEPaymentManager\1909852471.pem', N'SSLCERT', 1, 2, 0)
