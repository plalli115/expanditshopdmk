USE [EIS52_2015W1]
GO
/****** Object:  Table [dbo].[EECCGateway]    Script Date: Mon 10 3 2016 8:21:31 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EECCGateway]') AND type in (N'U'))
DROP TABLE [dbo].[EECCGateway]
GO
/****** Object:  Table [dbo].[EECCGateway]    Script Date: Mon 10 3 2016 8:21:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EECCGateway]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EECCGateway](
	[Code] [nvarchar](10) NULL,
	[Website] [nvarchar](80) NULL,
	[TestingWebsite] [nvarchar](80) NULL,
	[Name] [nvarchar](30) NULL
) ON [PRIMARY]
END
GO
INSERT [dbo].[EECCGateway] ([Code], [Website], [TestingWebsite], [Name]) VALUES (N'1', N'https://secure.authorize.net/gateway/transact.dll', N'https://test.authorize.net/gateway/transact.dll', N'AuthorizeNet')
INSERT [dbo].[EECCGateway] ([Code], [Website], [TestingWebsite], [Name]) VALUES (N'100', N'https://api.paypal.com/nvp', N'https://api.sandbox.paypal.com/nvp', N'PayPal - DirectPayment')
INSERT [dbo].[EECCGateway] ([Code], [Website], [TestingWebsite], [Name]) VALUES (N'101', N'https://orbitalvar1.paymentech.net/', N'', N'Chase PaymentTech')
INSERT [dbo].[EECCGateway] ([Code], [Website], [TestingWebsite], [Name]) VALUES (N'11', N'http://www.rtware.net/', N'', N'RTWare')
INSERT [dbo].[EECCGateway] ([Code], [Website], [TestingWebsite], [Name]) VALUES (N'12', N'http://www.viaklix.com', N'', N'ViaKlix')
INSERT [dbo].[EECCGateway] ([Code], [Website], [TestingWebsite], [Name]) VALUES (N'13', N'http://bankofamerica.com/merchantservices', N'', N'BankOfAmerica')
INSERT [dbo].[EECCGateway] ([Code], [Website], [TestingWebsite], [Name]) VALUES (N'14', N'http://www.plugnpay.com', N'', N'PlugNPay')
INSERT [dbo].[EECCGateway] ([Code], [Website], [TestingWebsite], [Name]) VALUES (N'15', N'http://www.planetpayment.com/', N'', N'PlanetPayment')
INSERT [dbo].[EECCGateway] ([Code], [Website], [TestingWebsite], [Name]) VALUES (N'16', N'http://www.merchantcommerce.net/', N'', N'MPCS')
INSERT [dbo].[EECCGateway] ([Code], [Website], [TestingWebsite], [Name]) VALUES (N'17', N'http://payments.intuit.com/', N'', N'IntuitPaymentSolutions')
INSERT [dbo].[EECCGateway] ([Code], [Website], [TestingWebsite], [Name]) VALUES (N'18', N'http://www.ecxweb.com/', N'', N'ECX')
INSERT [dbo].[EECCGateway] ([Code], [Website], [TestingWebsite], [Name]) VALUES (N'2', N'http://www.eProcessingNetwork.com', N'', N'Eprocessing')
INSERT [dbo].[EECCGateway] ([Code], [Website], [TestingWebsite], [Name]) VALUES (N'20', N'http://www.innovativegateway.com', N'', N'Innovative')
INSERT [dbo].[EECCGateway] ([Code], [Website], [TestingWebsite], [Name]) VALUES (N'205', N'', N'https://secure.ogone.com/ncol/test/orderdirect.asp', N'Ogone')
INSERT [dbo].[EECCGateway] ([Code], [Website], [TestingWebsite], [Name]) VALUES (N'21', N'http://www.merchantanywhere.com/', N'', N'MerchantAnywhere')
INSERT [dbo].[EECCGateway] ([Code], [Website], [TestingWebsite], [Name]) VALUES (N'22', N'http://www.skipjack.com', N'https://developer.skipjackic.com/scripts/evolvcc.dll?AuthorizeAPI', N'Skipjack')
INSERT [dbo].[EECCGateway] ([Code], [Website], [TestingWebsite], [Name]) VALUES (N'23', N'https://orbitalvar1.paymentech.net', N'https://orbitalvar1.paymentech.net/authorize', N'Orbital')
INSERT [dbo].[EECCGateway] ([Code], [Website], [TestingWebsite], [Name]) VALUES (N'24', N'http://www.3dsi.com', N'', N'3DSI')
INSERT [dbo].[EECCGateway] ([Code], [Website], [TestingWebsite], [Name]) VALUES (N'25', N'http://www.TrustCommerce.com', N'', N'TrustCommerce')
INSERT [dbo].[EECCGateway] ([Code], [Website], [TestingWebsite], [Name]) VALUES (N'26', N'http://www.psigate.com', N'', N'PSIGate')
INSERT [dbo].[EECCGateway] ([Code], [Website], [TestingWebsite], [Name]) VALUES (N'27', N'http://www.firstnationalmerchants.com/', N'https://test5x.clearcommerce.com:11500', N'PayFuse')
INSERT [dbo].[EECCGateway] ([Code], [Website], [TestingWebsite], [Name]) VALUES (N'28', N'http://www.verisign.com', N'', N'PayFlowLink')
INSERT [dbo].[EECCGateway] ([Code], [Website], [TestingWebsite], [Name]) VALUES (N'29', N'', N'https://secure.ogone.com/ncol/test/orderdirect.asp', N'Ogone')
INSERT [dbo].[EECCGateway] ([Code], [Website], [TestingWebsite], [Name]) VALUES (N'3', N'http://www.intellipay.com', N'', N'Intellipay')
INSERT [dbo].[EECCGateway] ([Code], [Website], [TestingWebsite], [Name]) VALUES (N'30', N'http://www.linkpoint.com', N'http://staging.linkpt.net:1129', N'LinkPoint')
INSERT [dbo].[EECCGateway] ([Code], [Website], [TestingWebsite], [Name]) VALUES (N'31', N'http://www.moneris.com', N'https://esqa.moneris.com/HPPDP/index.php', N'Moneris')
INSERT [dbo].[EECCGateway] ([Code], [Website], [TestingWebsite], [Name]) VALUES (N'32', N'http://gateway.usight.com', N'', N'USight')
INSERT [dbo].[EECCGateway] ([Code], [Website], [TestingWebsite], [Name]) VALUES (N'33', N'', N'http://staging.linkpt.net:1129', N'FirstData')
INSERT [dbo].[EECCGateway] ([Code], [Website], [TestingWebsite], [Name]) VALUES (N'34', N'http://www.networkmerchants.com/', N'', N'NetworkMerchants')
INSERT [dbo].[EECCGateway] ([Code], [Website], [TestingWebsite], [Name]) VALUES (N'35', N'https://www.ogone.com/', N'https://secure.ogone.com/ncol/test/orderdirect.asp', N'Ogone')
INSERT [dbo].[EECCGateway] ([Code], [Website], [TestingWebsite], [Name]) VALUES (N'36', N'http://www.concordefsnet.com/', N'', N'EFSNet')
INSERT [dbo].[EECCGateway] ([Code], [Website], [TestingWebsite], [Name]) VALUES (N'37', N'http://www.cybersource.com/', N'https://ics2wstest.ic3.com/commerce/1.x/transactionProcessor/', N'CyberSource')
INSERT [dbo].[EECCGateway] ([Code], [Website], [TestingWebsite], [Name]) VALUES (N'38', N'https://www.eway.com.au/gateway_cvn/xmlpayment.asp', N'https://www.eway.com.au/gateway_cvn/xmltest/testpage.asp', N'Eway')
INSERT [dbo].[EECCGateway] ([Code], [Website], [TestingWebsite], [Name]) VALUES (N'39', N'http://www.optimalpayments.com/', N'https://realtime.test.firepay.com/servlet/DPServlet', N'Optimal')
INSERT [dbo].[EECCGateway] ([Code], [Website], [TestingWebsite], [Name]) VALUES (N'4', N'http://www.itransact.com', N'', N'ITransact')
INSERT [dbo].[EECCGateway] ([Code], [Website], [TestingWebsite], [Name]) VALUES (N'40', N'http://www.merchantpartners.com/', N'', N'MerchantPartners')
INSERT [dbo].[EECCGateway] ([Code], [Website], [TestingWebsite], [Name]) VALUES (N'41', N'http://www.cybercash.net/', N'', N'CyberCash')
INSERT [dbo].[EECCGateway] ([Code], [Website], [TestingWebsite], [Name]) VALUES (N'43', N'http://www.yourpay.com', N'', N'YourPay')
INSERT [dbo].[EECCGateway] ([Code], [Website], [TestingWebsite], [Name]) VALUES (N'44', N'http://www.ach-payments.com', N'https://www.paymentsgateway.net/cgi-bin/posttest.pl', N'ACHPayments')
INSERT [dbo].[EECCGateway] ([Code], [Website], [TestingWebsite], [Name]) VALUES (N'45', N'http://www.paymentsgateway.com/', N'', N'PaymentsGateway')
INSERT [dbo].[EECCGateway] ([Code], [Website], [TestingWebsite], [Name]) VALUES (N'48', N'http://www.goemerchant.com/', N'', N'GoEMerchant')
INSERT [dbo].[EECCGateway] ([Code], [Website], [TestingWebsite], [Name]) VALUES (N'49', N'http://www.paystream.com.au', N'', N'PayStream')
INSERT [dbo].[EECCGateway] ([Code], [Website], [TestingWebsite], [Name]) VALUES (N'50', N'http://www.transfirst.com', N'', N'TransFirst')
INSERT [dbo].[EECCGateway] ([Code], [Website], [TestingWebsite], [Name]) VALUES (N'51', N'http://www.chase.com', N'', N'Chase')
INSERT [dbo].[EECCGateway] ([Code], [Website], [TestingWebsite], [Name]) VALUES (N'52', N'http://www.psigate.com', N'https://dev.psigate.com:7989/Messenger/XMLMessenger', N'PSIGateXML')
INSERT [dbo].[EECCGateway] ([Code], [Website], [TestingWebsite], [Name]) VALUES (N'53', N'http://www.thompsonmerchant.com/', N'', N'NexCommerce')
INSERT [dbo].[EECCGateway] ([Code], [Website], [TestingWebsite], [Name]) VALUES (N'54', N'http://www.worldpay.com', N'', N'WorldPay')
INSERT [dbo].[EECCGateway] ([Code], [Website], [TestingWebsite], [Name]) VALUES (N'55', N'http://www.sagepay.com', N'https://test.sagepay.com', N'SagePay')
INSERT [dbo].[EECCGateway] ([Code], [Website], [TestingWebsite], [Name]) VALUES (N'56', N'http://www.paygea.com/', N'', N'PayGea')
INSERT [dbo].[EECCGateway] ([Code], [Website], [TestingWebsite], [Name]) VALUES (N'57', N'http://www.sterlingpayment.com', N'', N'Sterling')
INSERT [dbo].[EECCGateway] ([Code], [Website], [TestingWebsite], [Name]) VALUES (N'58', N'http://www.payjunction.com', N'', N'PayJunction')
INSERT [dbo].[EECCGateway] ([Code], [Website], [TestingWebsite], [Name]) VALUES (N'59', N'http://www.secpay.com', N'', N'SecPay')
INSERT [dbo].[EECCGateway] ([Code], [Website], [TestingWebsite], [Name]) VALUES (N'6', N'https://payflowpro.paypal.com', N'https://pilot-payflowpro.paypal.com', N'PayFlowPro')
INSERT [dbo].[EECCGateway] ([Code], [Website], [TestingWebsite], [Name]) VALUES (N'60', N'http://www.paymentexpress.com', N'', N'PaymentExpress')
INSERT [dbo].[EECCGateway] ([Code], [Website], [TestingWebsite], [Name]) VALUES (N'61', N'http://www.myvirtualmerchant.com', N'', N'MyVirtualMerchant')
INSERT [dbo].[EECCGateway] ([Code], [Website], [TestingWebsite], [Name]) VALUES (N'62', N'http://www.sagepayments.com', N'', N'SagePayments')
INSERT [dbo].[EECCGateway] ([Code], [Website], [TestingWebsite], [Name]) VALUES (N'63', N'http://www.securepay.com', N'', N'SecurePay')
INSERT [dbo].[EECCGateway] ([Code], [Website], [TestingWebsite], [Name]) VALUES (N'64', N'http://www.moneris.com', N'https://esplusqa.moneris.com/usmpg/index.php', N'MonerisUSA')
INSERT [dbo].[EECCGateway] ([Code], [Website], [TestingWebsite], [Name]) VALUES (N'65', N'http://beanstream.com', N'', N'Beanstream')
INSERT [dbo].[EECCGateway] ([Code], [Website], [TestingWebsite], [Name]) VALUES (N'66', N'http://www.verifi.com', N'', N'Verifi')
INSERT [dbo].[EECCGateway] ([Code], [Website], [TestingWebsite], [Name]) VALUES (N'67', N'http://www.sagepay.com', N'https://test.sagepay.com', N'SagePay')
INSERT [dbo].[EECCGateway] ([Code], [Website], [TestingWebsite], [Name]) VALUES (N'68', N'http://merchante-solutions.com/', N'https://test.merchante-solutions.com/mes-api/tridentApi', N'MerchantESolutions')
INSERT [dbo].[EECCGateway] ([Code], [Website], [TestingWebsite], [Name]) VALUES (N'69', N'http://www.payleap.com', N'', N'ePDQ')
INSERT [dbo].[EECCGateway] ([Code], [Website], [TestingWebsite], [Name]) VALUES (N'7', N'http://www.usaepay.com', N'', N'USAePay')
INSERT [dbo].[EECCGateway] ([Code], [Website], [TestingWebsite], [Name]) VALUES (N'70', N'http://www.paypoint.net', N'', N'PayPoint')
INSERT [dbo].[EECCGateway] ([Code], [Website], [TestingWebsite], [Name]) VALUES (N'71', N'http://www.worldpay.com', N'https://secure-test.wp3.rbsworldpay.com/jsp/merchant/xml/paymentService.jsp', N'WorldPayXML')
INSERT [dbo].[EECCGateway] ([Code], [Website], [TestingWebsite], [Name]) VALUES (N'9', N'http://www.netbilling.com', N'', N'NetBilling')
