﻿$(window).load(function () {

    var isIe7 = false;
    var isIe8 = false;
    try { isIe7 = ie7; } catch (e) { isIe7 = false; }
    try { isIe8 = ie8; } catch (e) { isIe8 = false; }

    var isLtIe9 = isIe7 || isIe8 ? true : false;

    $(".managementTable table tbody tr td:first-child").css({ backgroundColor: '#DECED5' });
    $(".managementTable table tbody tr th:first-child").css({ backgroundColor: '#967884' });

    $(window).resize(function () {
        setBarWitdh();
    });
    
    function setBarWitdh() {

        var tableWidth = $('.managementTable table').width();

        if (isIe7) {
            $(".headerFieldset").css({ width: (tableWidth - 10) + 'px' });
            $(".page").css({ marginTop: 20 + 'px' });
        } else if (isIe8) {
            $(".headerFieldset").css({ width: (tableWidth - 36) + 'px' });
        }
        else {
            $(".headerFieldset").css({ width: (tableWidth - 60) + 'px' });
        }

    }

    setBarWitdh();

    function refreshPage() {
        location.reload();
    }

    $('#Languages').change(function () {
        var selectedLanguage = $('#Languages').val();
        var nameValueParameter = "languages=" + selectedLanguage;
        var pathString = rootUrl + "CMS/Lang";
        commonPostAction(pathString, nameValueParameter);
        setTimeout(refreshPage, 500);
    });

    function commonPostAction(pathString, parameterValue) {
        var callbackResult = null;
        $.ajax({
            type: "POST",
            async: false,
            url: pathString,
            data: parameterValue,
            success: function (result) {
                callbackResult = result;
            }, error: function (req, status, error) { alert(error); }
        });
        return callbackResult;
    }

    function getIntGroupGuid(groupGuid) { return groupGuid.substring(3, groupGuid.length); }


    /* "theDragStartParentId" is the parent ID of the item selected on drag start (what it's is moving FROM) 
    Needed by the drop function, so the item can be removed from it's former parent visually.
    */
    var theDragStartParentId;
    /* Need to keep knowledge of draggable here */
    var isDraggableChild = false;
    var draggableGroupGuid;
    var draggableParentGuid;
    var hasChildrenOnDemand = false;
    var isUngroupedMoved = false;

    setInitDraggable();
    makeSitemapDroppable();
    createExpandToggle();

    function setInitDraggable() {
        var ctop; var cleft; if (isIe7) { ctop = 4; cleft = 20; } else { ctop = 20; cleft = 22; }
        $('.class1 li').draggable({
            handle: ' > dl',
            opacity: 0.8,
            addClasses: false,
            helper: 'clone',
            cursorAt: { top: ctop, left: cleft },
            zIndex: 100,
            start: function () {
                theDragStartParentId = $(this).parents('li:first').attr('id');
            },
            stop: function () {
                var newParent;
                if (hasChildrenOnDemand) {
                    newParent = $(this).parents('li:first'); // Moved here because "this" does not get rigth object inside timeout function.
                }

                setTimeout(function () {
                    if (isDraggableChild) {
                        // Make ajax call in drag stop as a workaround for the strange behaviour when using timeout at the end of drop event.
                        // Drag stop fires AFTER drop event completes...
                        // Make blocking call here
                        saveListRegrouping(draggableParentGuid, draggableGroupGuid);
                        draggableGroupGuid = '';
                        draggableParentGuid = '';
                        isDraggableChild = false;
                    }
                    if (hasChildrenOnDemand) {
                        //newParent = $(this).parents('li:first'); // Declaration moved outside timeout function...
                        hasChildrenOnDemand = false;
                        // Make blocking call here
                        loadSubGroups(newParent);
                        newParent.children('span:first').removeClass('contract');
                    }

                }, 100);
            }
        });
    }

    function makeSitemapDroppable() {
        $('.class1 dl,.class1 .dropzone, .sortablezone').droppable({
            tolerance: 'pointer',
            drop: function (e, ui) {
                $(this).removeClass('dropzoneProduct-hover');
                $(this).removeClass('dropzoneGroup-hover');

                // TEST TO FIND OUT WHAT WE ARE DRAGGING
                if ($(ui.draggable).is('#groupproducts li')) {

                }
                var theSelectedId = $(ui.draggable).attr('id');
                var theDraggedTopUlId = $(ui.draggable).parentsUntil('.class1').parent().attr('id');
                var theDroppableTopUlId = $(this).parentsUntil('.class1').parent().attr('id');

                var theDraggedTruth = /0$/i.test(theDraggedTopUlId);
                var theDroppableTruth = /0$/i.test(theDroppableTopUlId);
                if (!theDraggedTruth && theDroppableTruth) { return false; }
                if (!theDraggedTruth && (theDraggedTopUlId != theDroppableTopUlId)) { return false; }


                var sort = $(this).hasClass('sortablezone');
                var parentGuid = '';
                var groupGuid = ui.draggable.attr('id');
                if (sort) {
                    var from = theDraggedTopUlId.split('_')[1];
                    var to = theDroppableTopUlId.split('_')[1];

                    //var pli = $(ui.draggable).parents('li');
                    var sibling = $(this).parent();
                    var newParent = $(sibling).parents('li');
                    var newParentGuid = newParent.attr('id');
                    if (newParentGuid == null) {
                        newParentGuid = "id_0_" + to;
                    }
                    var siblingGuid = sibling.attr('id');
                    if (theDraggedTopUlId == theDroppableTopUlId) {
                        sibling.before(ui.draggable);
                    } else {
                        sibling.before(ui.draggable.clone());
                    }

                    // Make the call after wait
                    setTimeout(function () {
                        saveListChanges(newParentGuid.replace(new RegExp("id_"), ""), groupGuid.replace(new RegExp("id_"), ""), siblingGuid.replace(new RegExp("id_"), ""), from, to);
                        setInitDraggable();
                        makeSitemapDroppable();
                    }, 300);
                } else {
                    var li = $(this).parent();
                    parentGuid = li.attr('id');
                    groupGuid = ui.draggable.attr('id');
                    var child = !$(this).hasClass('dropzone');
                    if (child && li.children('ul').length == 0) {
                        li.append('<ul/>');
                    }
                    if (child) {
                        // Add knowledge to global here to be used by drag stop event
                        isDraggableChild = true;
                        var theDiv = li.children('div:first');
                        if ($('#' + parentGuid + ' span:first').hasClass('isparent')) {
                            hasChildrenOnDemand = true;
                        }
                        if (!hasChildrenOnDemand) {
                            if (li.children('span:first').attr('class') == undefined) {
                                theDiv.after('<span class="expand" />');
                                createExpandToggle();
                            }
                        }
                        var hasChildrenLeft = false;
                        if ($('#' + theDragStartParentId + ' span:first').hasClass('isparent') && ($('#' + theDragStartParentId + ' span:first').hasClass('contract'))) {
                            hasChildrenLeft = true;
                        } else {
                            var theRestOfTheChildren = $('#' + theDragStartParentId + ' ul > li');
                            $(theRestOfTheChildren).each(function () {
                                var currentId = $(this).attr('id');
                                var childsParent = $(this).parent().parent().attr('id');
                                if (currentId != groupGuid) {
                                    if (childsParent != groupGuid) {
                                        hasChildrenLeft = true;
                                        return false;
                                    }
                                }
                            });
                        }
                        // Append draggable to the new parent
                        if (theDraggedTopUlId == theDroppableTopUlId) {
                            li.children('ul').append(ui.draggable);
                        } else {
                            var siteMapNr = theDroppableTopUlId.split('_')[1];
                            var groupGuidNr = groupGuid.split('_')[3];
                            var groupMasterGuid = groupGuid.split('_')[4];
                            var mgArray = null;
                            if (groupMasterGuid != '') {
                                mgArray = $("li[id*=" + siteMapNr + '_' + groupGuidNr + '_' + groupMasterGuid + "]");
                            }
                            if (mgArray == null || $(mgArray).length == 0) {
                                var theClone = ui.draggable.clone();
                                var theCloneId = $(theClone).attr('id');
                                li.children('ul').append(theClone); // Clone if this is a "copy" action else append the draggable as it is
                            }
                        }

                        if (!hasChildrenLeft) {
                            // Remove the plus sign
                            $('#' + theDragStartParentId + ' span:first').replaceWith('');
                        }
                        // Add knowledge to global here to be used by drag stop event
                        draggableParentGuid = parentGuid.replace(new RegExp("id_"), "");
                        draggableGroupGuid = groupGuid.replace(new RegExp("id_"), "");
                        // Do Ajax call in drag stop event...

                        // Assign draggable and droppable again if this was a "copy" action
                        setInitDraggable();
                        makeSitemapDroppable();
                    }
                }

            },
            over: function (e, ui) {
                if ($(this).is('.sortablezone') && (ui.draggable.is('.class1 li'))) {
                    $(this).addClass('dropzoneGroup-hover');
                } else if ($(this).is('.ui-droppable') && (!$(this).is('.sortablezone'))) {
                    $(this).addClass('dropzoneProduct-hover');
                }
            },
            out: function () {
                $(this).removeClass('dropzoneProduct-hover');
                $(this).removeClass('dropzoneGroup-hover');
            }
        });
    }


    /* Moving Groups */
    // Change SortIndex in subgroup
    function saveListChanges(parentGuid, groupGuid, siblingGuid, from, to) {
        //alert("parentGuid = " + parentGuid + " groupGuid = " + groupGuid + " siblingGuid = " + siblingGuid + " from = " + from + " to = " + to);
        var pgArray = parentGuid.split('_');
        var theParent = pgArray[0];
        var ggArray = groupGuid.split('_');
        var theGroupGuid = ggArray[0];
        var siblingArray = siblingGuid.split('_');
        var siblingGroupId = siblingArray[0];

        var isSameDb = from == to ? true : false;
        if (!isSameDb) {
            var draggedObject = $('#sitemap_' + to + ' #id_' + groupGuid);
            $(draggedObject).attr('id', 'id_' + theGroupGuid + '_' + to + '_' + ggArray[2] + '_' + ggArray[3]);
            // Remove the plus sign
            var theCloneId = $(draggedObject).attr('id');
            $('#' + theCloneId + ' span:first').replaceWith('');
        }

        $.ajax({
            type: "POST",
            url: rootUrl + "CentralManagement/CopyGroup",
            data: "groupGuid=" + theGroupGuid + "&parentGuid=" + theParent + "&siblingGuid=" + siblingGroupId + "&from=" + from + "&to=" + to,
            success: function (result) {
                var isChanged = /^NewGuid/.test(result);
                if (isChanged) {
                    var modGroup = $('#sitemap_' + to + ' #id_' + theGroupGuid + '_' + to + '_' + ggArray[2] + '_' + ggArray[3]);
                    var newGuid = result.split(',')[1];
                    $(modGroup).attr('id', 'id_' + newGuid + '_' + to + '_' + ggArray[2] + '_' + ggArray[3]);
                }
                updateRouting(to);
            }, error: function () { alert(CATMAN_LABEL_AN_ERROR_OCCURRED); }
        });
    }
    // Change Parent
    function saveListRegrouping(parentGuid, groupGuid) {
        //alert('parentGuid = ' + parentGuid);
        var pgArray = parentGuid.split('_');
        var theNewParent = pgArray[0];
        var theNewParentDbId = pgArray[1];
        var ggArray = groupGuid.split('_');
        var theGroupGuid = ggArray[0];
        var theGroupFromDbId = ggArray[1];

        var isSameDb = theGroupFromDbId == theNewParentDbId ? true : false;

        if (!isSameDb) {
            var draggedObject = $('#sitemap_' + theNewParentDbId + ' #id_' + groupGuid);
            $(draggedObject).attr('id', 'id_' + theGroupGuid + '_' + theNewParentDbId + '_' + ggArray[2] + '_' + ggArray[3]);
            // Remove the plus sign
            var theCloneId = $(draggedObject).attr('id');
            $('#' + theCloneId + ' span:first').replaceWith('');
        }
        $.ajax({
            type: "POST",
            async: false,
            url: rootUrl + "CentralManagement/CopyGroup",
            data: "groupGuid=" + theGroupGuid + "&parentGuid=" + theNewParent + "&siblingGuid=0" + "&from=" + theGroupFromDbId + "&to=" + theNewParentDbId,
            success: function (result) {
                var isChanged = /^NewGuid/.test(result);
                if (isChanged) {
                    var modGroup = $('#sitemap_' + theNewParentDbId + ' #id_' + theGroupGuid + '_' + theNewParentDbId + '_' + ggArray[2] + '_' + ggArray[3]);
                    var newGuid = result.split(',')[1];
                    $(modGroup).attr('id', 'id_' + newGuid + '_' + theNewParentDbId + '_' + ggArray[2] + '_' + ggArray[3]);
                }
                $('#allproductscontent').html(result);
                updateRouting(theNewParentDbId);
            },
            error: function () {
                //alert(CATMAN_LABEL_AN_ERROR_OCCURRED);
            }
        });
    }

    // *** KEEP ROUTING TABLES IN ORDER

    function updateRouting(to) {
        
        var callbackResult = null;
        $.ajax({
            type: "POST",
            async: false,
            url: rootUrl + "CentralManagement/GetUpdateRouteUrl",
            data: "to=" + to,
            success: function (result) {
                callbackResult = result;
            }, error: function (req, status, error) { alert(error); }
        });

        commonPostAction2(callbackResult, 'createRoutes');
    }

    function commonPostAction2(pathString, parameterValue) {
        var callbackResult = null;
        $.ajax({
            type: "GET",
            async: false,
            url: pathString,
            success: function (result) {
                callbackResult = result;
            }, error: function (req, status, error) { alert(error); }
        });
        return callbackResult;
    }

    function makeBaseAuth(username, password) {
        var tok = username + ':' + password;
        var hash = btoa(tok);
        return "Basic " + hash;
    }


    var contractedItems = new Array();
    if (jQuery.cookie) {
        if (jQuery.cookie('contractedItems')) {
            contractedItems = jQuery.cookie('contractedItems').split(',');
            for (var j = 0; j < contractedItems.length; ++j) {
                if ($(jQuery('#' + contractedItems[j])).is('.expanded')) { jQuery('#' + contractedItems[j]).removeClass('expanded'); }
                if (!$(jQuery('#' + contractedItems[j])).is('.contracted')) { jQuery('#' + contractedItems[j]).addClass('contracted'); }
                $('.contracted').children('span').addClass('contract');
            }
            $('.contracted').children('ul').hide();
        }
    }

    function makeDraggable(result) {
        createExpandToggle();
        setInitDraggable();
        //$('#sitemap li').showMenu({ opacity: 1.8, query: "#myMenu", zindex: 2000 }); 
        makeSitemapDroppable();
    }

    function loadSubGroups(theParent) {
        var parentGuid = $(theParent).attr('id');
        var pgArray = parentGuid.split('_');
        var groupGuid = pgArray[1];
        var dbId = pgArray[2];

        $.ajax({
            type: "POST", async: false, url: rootUrl + "CentralManagement/LoadSubGroups", data: "groupGuid=" + groupGuid + "&dbIndex=" + dbId, success: function (result) {
                var theDiv;
                if (theParent.children('ul').length == 0) {
                    $(theParent).append(result);
                    if ($(theParent).children('.expand').length == 0) {
                        theDiv = theParent.children('div:first');
                        theDiv.after('<span class="expand" />');
                    }
                } else {
                    theParent.children('ul').replaceWith('');
                    theParent.append('<ul>');
                    $(theParent).children('ul:first').replaceWith(result);
                    if ($(theParent).children('.expand').length == 0) {
                        theDiv = theParent.children('div:first');
                        theDiv.after('<span class="expand" />');
                    }
                }
                makeDraggable(result);
            }, error: function () { alert(CATMAN_LABEL_AN_ERROR_OCCURRED); }
        });
    }

    $.fn.toggleClick = function () {
        var functions = arguments;
        return this.click(function () {
            var iteration = $(this).data('iteration') || 0;
            functions[iteration].apply(this, arguments);
            iteration = (iteration + 1) % functions.length;
            $(this).data('iteration', iteration);
        });
    };

    function createExpandToggle() {
        $('.expand').click(function () {
            var that = this;
            if ($(this).is('.isparent')) {
                if (!$(this).is('.appended')) {
                    var theParent = $(this).parent('li');
                    loadSubGroups(theParent);
                    $(this).addClass('appended');
                }
            }
            $(this).parent('li').toggleClick(function (event) {
                if ($(that).is('.contract')) {
                    $(this).children('ul').show();
                    $(this).removeClass('expanded');
                    $(this).addClass('contracted');
                    $(that).removeClass('contract');
                } else {
                    $(this).children('ul').hide();
                    $(this).removeClass('contracted');
                    $(this).addClass('expanded');
                    $(that).addClass('contract');
                }
                event.stopImmediatePropagation();
                updateArray($(this).attr('id'));
            }, function (event) {
                if (!$(that).is('.contract')) {
                    $(this).children('ul').hide();
                    $(this).addClass('expanded');
                    $(this).removeClass('contracted');
                    $(that).addClass('contract');
                } else {
                    $(this).children('ul').show();
                    $(this).addClass('contracted');
                    $(this).removeClass('expanded');
                    $(that).removeClass('contract');
                }
                event.stopImmediatePropagation();
                updateArray($(this).attr('id'));
            });
        });
    }

    function updateArray(eId) {
        var arrPos = inArray(eId, contractedItems); if (arrPos == -1) { contractedItems.push(eId); } else { contractedItems.splice(arrPos, 1); }
        //$.cookie('contractedItems', contractedItems, { path: "/", expires: 7 });
    }
    function inArray(elem, array) {
        for (var i = 0; i < array.length; i++) { if (array[i] === elem) { return i; } }
        return -1;
    }
    
});