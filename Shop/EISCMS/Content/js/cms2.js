﻿var rootUrl = '@Url.Content("~/")';
var pictureValidExtensions = 'bmp,gif,jpg,jpeg,png';
var selectedValue;
var selectedMultiImageValue;
var virtualUrl;
var templateFolder;
var bHasUnsavedData = false;
var fileName = null;
var _uploadBaseFolder;

/* Global Error handler */
$(function () {
    $(document).ajaxError(function (event, request, options) {

        if (request.status === 401) {
            $("#session-timeout-dialog").modal({
                backdrop: "static"
            });
        }
    });
});

/* Global */
var ExpandIT_BaseUploadFolder = {
    UploadFolder : function() {
        return _uploadBaseFolder == null || _uploadBaseFolder == '' ? 'catalog' : _uploadBaseFolder;
    }
}

var ExpandIT_ProductTable = {
    Table: {},
    AddProducts: function (productArray, groupGuid) {
        $.ajax({
            type: "POST",
            async: false,
            url: rootUrl + "cms/Catalog/AddProducts",
            data: "productGuidString=" + productArray.toString() + "&groupGuid=" + groupGuid,
            success: function (result) { }, error: function () { }
        });
    },
    RemoveProducts: function (groupProductGuidArray, groupGuid) {
        var table = ExpandIT_ProductTable.Table;
        $.ajax({
            type: "POST",
            async: false,
            url: rootUrl + "cms/Catalog/RemoveProducts",
            data: "groupProductGuidString=" + groupProductGuidArray.toString() + "&groupGuid=" + groupGuid,
            success: function(result) {
                table.fnDraw();
            },
            error: function () { }
        });
    }
}

var ExpandIT_MailProductPromotionTable = {
    Table: {}
}

function initializeModals() {
    $("#selectPictureModal").on("shown.bs.modal", function (e) {
        setTimeout(function () {
            var containerToSearch = $(".directory.expanded .jqueryFileTree>li:contains(" + fileName + ")");
            if (fileName != null) {
                try {
                    $('#picFileTree').scrollTo(containerToSearch[0]);    
                }catch(error){}
            }
        }, 1000);
    });

    $("#fb_filter_text").keyup(function () {
        delay(function () {
            $("#picFileTree").empty();
            $("#attachmentFileTree").empty();
            loadFileTree(null, null);
            loadAttachmentFileTree(null);
        }, 500);
    });

    $("#sideMenu li>a.active").parents("#sideMenu>li").addClass("active");
}

$(document).ready(function () {
    initializeModals();
});

var delay = (function () {
    var timer = 0;
    return function (callback, ms) {
        clearTimeout(timer);
        timer = setTimeout(callback, ms);
    };
})();

(function ($) {
    $.fn.showMenu = function (options) {
        var opts = $.extend({},
            $.fn.showMenu.defaults, options);
        $(this).bind("contextmenu", function (e) {
            opts.before(e);
            var newLeft = e.pageX;
            var newTop = e.pageY;
            $(opts.query).show().css({ top: newTop + "px", left: newLeft + "px", position: "absolute", opacity: opts.opacity, zIndex: opts.zindex }).data("target", e.target);
            return false;
        });
        $(document).bind("click", function () {
            $(opts.query).hide();
        });
        $(this).mousedown(function () {
            $(this).mouseup(function () {
                $(opts.query).hide();
            });
        });
    };
    $.fn.showMenu.defaults = { zindex: 2000, query: document, opacity: 1.0, before: function () { } };
})(jQuery);

jQuery.fn.center = function () {
    this.css("position", "absolute");
    this.css("top", ($(window).height() - this.outerHeight()) / 2 + $(window).scrollTop() + "px");
    this.css("left", ($(window).width() - this.outerWidth()) / 2 + $(window).scrollLeft() + "px");
    return this;
};

// ** Functions for modals/filetree/picture selection
/*** Modal windows ***/
///* Image popup */

var buttonFor;
var selectedValue = "";
var firstItem = "";
function getSelectedItem() { return $("#" + buttonFor); }
function getSelectedValue() { return selectedValue; }

function loadModalMultiImagePopUpInit() {
    $(".multi_image_button").click(function () {
        var buttonFor = $(".multi_image_txt").attr('id');
        makeMultiImagePopUpModal(rootUrl, buttonFor);
    });
}

function loadModalImagePopUpInit() {
    $(".show-modal-btn").click(function () {
        $(".picture-modal").show();
        $(".picture-save-modal").show();
        $(".attachment-modal").hide();
        selectedValue = $(this).data("picture");
        initialPic(selectedValue);
        var myId = $(this).attr('id');
        var inputId = myId.substring(8);
        buttonFor = inputId;
        loadFileTree(null, null);
    });
    $('#imageSaveButton').click(function () {
        var content = getSelectedValue();
        var inputElement = getSelectedItem();
        if (content != '') {
            $(inputElement).val(content);

            if ($(inputElement)[0].defaultValue != content) {
                bHasUnsavedData = true;
                $(inputElement).addClass('isDirtyData');
                $('#savemessagetext').html(getTranslation("CATMAN_LABEL_UNSAVED_DATA", 'Unsaved changes')).css({ color: 'Red' });
            } else {
                try { $(inputElement).removeClass('isDirtyData'); } catch (er) { }
                if ($('.isDirtyData').length == 0) {
                    bHasUnsavedData = false;
                    $('#savemessagetext').html("");
                }
            }
            var imgId = 'img_' + buttonFor;
            $('#' + imgId).parent().html("<img id='" + imgId + "' src='" + rootUrl + "cms/Catalog/ShowThumbnail?imageName=" + content + "&width=80&height=80&nfimg=" + new Date().getTime() + "' class='max-height-80' />");
        }
        $('#selectPictureModal').modal('hide');
        try { $('#helpContent').remove(); } catch (e) { }
    });
}

function getImageInfo(imagePath) {
    $.ajax({
        type: "POST",
        url: rootUrl + "cms/Catalog/ThumbnailInfo",
        data: "imagePath=" + imagePath,
        success: function (result) {
            var myArray = result.split('/n');
            if ($("#data").length > 0) {
                $("#data").html('');
            }
            if (myArray.length > 0) {
                $('#data').html(myArray[0] + "<br/>" + myArray[1] + "<br/>" + myArray[2]);
            }
        },
        error: function () { }
    });
}

function loadModalAttachmentPopUpInit() {
    $("#showAttachmentModal").click(function () {
        $(".picture-modal").hide();
        $(".attachment-modal").show();
        loadAttachmentFileTree(null);
    });
    
    $('#attachmentSaveButton').click(function () {
        var content = getSelectedValue();
        var inputElement = $("#ATTACHMENT");
        if ($(inputElement).val() != content) {
            bHasUnsavedData = true;
            $(inputElement).addClass('isDirtyData');
            $('#savemessagetext').html(getTranslation("CATMAN_LABEL_UNSAVED_DATA", 'Unsaved changes')).css({ color: 'Red' }).show();
            $(inputElement).val(content);
        } else {
            try { $(inputElement).removeClass('isDirtyData'); } catch (er) { }
            if ($('.isDirtyData').length == 0) {
                bHasUnsavedData = false;
                $('#savemessagetext').html("").hide();
            }
        }
        $('#selectPictureModal').modal('hide');
    });

}

function loadAttachmentFileTree(currentFolder) {
    var filterVal = $("#fb_filter_text").val();
    fileName = null;
    var toExpand = currentFolder;

    if (toExpand == null) {
        //toExpand = "/catalog/documents/";
        // Default folder for attachments
        toExpand = "/" + ExpandIT_BaseUploadFolder.UploadFolder() + "/documents/";
    }

    if (toExpand.charAt(0) != '/') {
        toExpand = '/' + toExpand;
    }

    makeAjaxUpload(toExpand);
    $("#attachmentFileTree").fileTree({
        root: '/' + ExpandIT_BaseUploadFolder.UploadFolder() + '/',
        script: rootUrl + 'cms/Catalog/FileTree?validExtensionArray=' + '&filter=' + filterVal,
        multiFolder: false,
        expandSpeed: 0,
        expanded: toExpand
    }, function (file, type) {
        if (type == "file") {
            $(this).css("background-color", "blue");
            selectedValue = file;
        } else {
            $("#upload_folder").val(file);
            makeAjaxUpload(file);
        }
    });
}

function loadFileTree(currentFolder, currentImage) {
    var filterVal = $('#fb_filter_text').val();
    fileName = null;

    if (currentFolder == null) {
        var onLoadPicture = $('#picture_' + buttonFor).data("picture");
        var arrVars = onLoadPicture.split("/");
        fileName = arrVars.pop();
        var toExpand = arrVars.join("/") + '/';
    } else {
        toExpand = currentFolder;
        if (currentImage != null) {
            fileName = currentImage;
        }
    }

    if (toExpand.charAt(0) != '/') {
        toExpand = '/' + toExpand;
    }

    makeAjaxUpload(toExpand);
    
    var validExtensions = 'bmp,gif,jpg,jpeg,png';
    $('#picFileTree').fileTree({
        root: '/' + ExpandIT_BaseUploadFolder.UploadFolder() + '/',
        script: rootUrl + 'cms/Catalog/FileTree?validExtensionArray=' + validExtensions + '&filter=' + filterVal,
        multiFolder: false,
        expandSpeed: 0,
        collapseSpeed: 1000,
        expanded: toExpand // Should look like: '/catalog/images/'
    }, function (file, type) {
        if (type == 'file') {
            displayCurrentImage(file);
            getImageInfo(file);
            selectedValue = file;
            selectedMultiImageValue = file;
        }
        else {
            $('#upload_folder').val(file);
            makeAjaxUpload(file);
        }
    });
}

function makeAjaxUpload(uploadFolder) {
    if (uploadFolder == null || uploadFolder == 'undefined') {
        //uploadFolder = '/catalog/images';
        uploadFolder = '/' + ExpandIT_BaseUploadFolder() + '/images';
    }

    $("#upload_folder").val(uploadFolder);

    var au = new AjaxUpload('#upload_button', {
        action: rootUrl + 'cms/Catalog/AsyncUpload?fileType=image&upload_folder=' + uploadFolder, name: 'userfile', data: { example_key1: 'example_value', example_key2: 'example_value2' },
        autoSubmit: false,
        responseType: false,
        onChange: function (file, extension) {
            $('#upload_input').val(file);
        },
        onSubmit: function (file, extension) {
            if (extension && /^(jpg|png|jpeg|gif|bmp)$/.test(extension)) {
                $('#file_upload_response').fadeIn('slow');
                $('#file_upload_response').html(getTranslation('CATMAN_LABEL_UPLOADING', 'Uploading ') + ' ' + file);
            } else {
                $('#file_upload_response').fadeIn('slow');
                $('#file_upload_response').html(getTranslation('CATMAN_IMAGEUPLOAD_WARNING_MESSAGE', 'Only jpg, png, jpeg, bmp and gif files are allowed'));
                $('#upload_input').val('');
                return false;
            }
        },
        onComplete: function (file, response) {
            $('#upload_input').val('');
            var responseText = response.toLowerCase();
            if (responseText.indexOf("success") >= 0) {
                loadFileTree(uploadFolder, file);
                setTimeout(function () {
                    var containerToSearch = $(".directory.expanded .jqueryFileTree>li:contains(" + file + ")");
                    if (fileName != null) {
                        try {
                            $('#picFileTree').scrollTo(containerToSearch[0]);
                        }catch (error){}
                        displayCurrentImage(uploadFolder + file);
                    }
                }, 500);
                $('#file_upload_response').css({ color: "green" });
                $('#file_upload_response').html(file + ' ' + getTranslation('CATMAN_LABEL_FILE_UPLOADED', 'successfully uploaded'));
                setTimeout(function () { $('#file_upload_response').fadeOut('slow'); }, 5000);
            } else {
                $('#file_upload_response').css({ color: "red" });
                $('#file_upload_response').html($(response + 'pre').html() + ' ');
                setTimeout(function () { $('#file_upload_response').fadeOut('slow'); }, 8000);
            }
        }
    });
    $('#sendfilebutton').click(function () { au.submit(); });
}

function displayCurrentImage(imagePath) {
    var thePathUrl = rootUrl;
    if (virtualUrl != null) {
        thePathUrl = virtualUrl;
    }

    imagePath = thePathUrl + imagePath;
    imagePath = imagePath.replace(/\/\//g, '\/');

    $("#filePictureThumbnail").html("<img src='" + imagePath + "?nfimg=" + new Date().getTime() + "'/>");
}

function initialPic(fileName) {
    if (fileName.length > 0) {
        var urlSplit = fileName.split('/');
        try {
            var namePart = urlSplit[urlSplit.length - 1];
            var theTr = listTextSearch('li', namePart);
        } catch (e) {
            alert('Invalid url. Must be of type "/parentfolder/foldername"');
        }
        if (theTr != null) {
            displayCurrentImage(fileName);
            getImageInfo(fileName);
        }
    } else {
        displayUnknownImage();
    }
}

function displayUnknownImage() {
    var thePathUrl = rootUrl;
    if (virtualUrl != null) {
        thePathUrl = virtualUrl;
    }

    var imagePath = thePathUrl + "/Content/images/unknown.png";
    imagePath = imagePath.replace(/\/\//g, '\/');

    $("#filePictureThumbnail").html("<img src='" + imagePath + "?nfimg=" + new Date().getTime() + "'/>");
}

function listTextSearch(el, theText) {
    var theElement = $(el + ':contains(' + theText + ')');
    var retVal = new Array();
    for (var i = 0; i < theElement.length; i++) {
        if ($.trim($(theElement[i]).text()) == $.trim(theText)) {
            retVal.push(theElement[i]);
        }
    }
    return retVal;
}
// ** End functions for modals/filetree/picture selection

// ** Global Methods 
function commonPostAction(pathString, parameterValue) {
    var callbackResult = null;
    $.ajax({
        type: "POST",
        async: false,
        url: pathString,
        data: parameterValue,
        success: function (result) {
            callbackResult = result;
        },
        error: function () { }
    });
    return callbackResult;
}

function getPageLanguageGuid() {
    return $('#pageLangId').attr('value');
}

function getLanguageGuid() {
    return $('#langid').attr('value');
}

function getTranslation(keyValue, defaultValue) {
    var translated = '';
    $.ajax({
        type: "POST",
        async: false,
        url: rootUrl + "cms/Catalog/GetTranslation",
        data: "key=" + keyValue,
        success: function (result) { translated = result; },
        error: function () {
            translated = defaultValue;
        }
    });
    return translated;
}

function getTemplateFolder() {
    templateFolder = commonPostAction(rootUrl + "cms/Catalog/GetTemplateFolder", "key=0");
}
// ** End Global

$(window).load(function () {

    function setExternalLinkclick(elementId, queryElement, groupGuid, productGuid, languageGuid) {
        $('#' + elementId).click(function (event) {
            event.preventDefault();
            //queryElement == 'GroupGuid' ? saveGroup() : saveProduct();
            bHasUnsavedData = false;
            var templateUrl = rootUrl + 'link.aspx?GroupGuid=' + groupGuid + '&ProductGuid=' + productGuid + '&LanguageGuid=' + languageGuid;
            templateUrl += "&nfd=" + new Date().getTime();
            window.open(templateUrl);
        });
    }

    // Language
    $('#Languages').change(function () { // Language in dropdown is already changed. Have to set it back to previous value
        if (alertForUnsavedData()) {
            var selectedLanguage = $('#Languages').val();
            var nameValueParameter = "languages=" + selectedLanguage;
            var pathString = rootUrl + "cms/Catalog/Lang";
            commonPostAction(pathString, nameValueParameter);
            var groupTreeArray = new Array();
            var liref = $('.grouptreelink');
            liref.each(function () {
                var li = $(this).parents('li:first');
                var groupGuid = getIntGroupGuid(li.attr('id'));
                groupTreeArray.push(groupGuid);
            });
            pathString = rootUrl + "cms/Catalog/ChangeLanguage";
            nameValueParameter = "jsArray=" + groupTreeArray.toString();
            var result = commonPostAction(pathString, nameValueParameter);
            if (result !== "") {
                var liref2;
                var groupNames = (typeof result) == 'string' ? eval('(' + result + ')') : result;
                for (var ij = 0; ij < groupNames.length; ij++) {
                    liref2 = $('#' + groupNames[ij].GroupGuid + ' .grouptreelink:first');
                    liref2.html(groupNames[ij].NAME);
                }
            }
            var pageGroupGuid = $('#PageGRP');
            var pageProductGuid = $('#PagePRD');
            var grpProductGuid = $('#grpProductGuid').val();
            var productListId = $('#productListId').val();
            var pid = $('#pid').val();
            var gid = $('#id').val();
            if (pageGroupGuid.length > 0) {
                updateGroupEdit(gid);
            }
            else if (grpProductGuid != undefined && grpProductGuid.length > 0) {
                loadGroupProductView(grpProductGuid);
            } else if (productListId != undefined && productListId.length > 0) {
                //loadProductsView(); TODO: Check this. The call in this method goes to a no longer existing action in CatalogController. Should it be there?
            }
            else if (pageProductGuid.length > 0) {
                updateProductEdit(gid, pid);
            }
            //updateAllProducts(gid);
        }
        else {
            var strPrevLang = $('#langid').val();
            $('#Languages').val(strPrevLang);
        }
    });
    function getIntGroupGuid(groupGuid) { return groupGuid.substring(3, groupGuid.length); }

    function updateProductEdit(groupGuid, productGuid) {
        $.ajax({
            type: "POST",
            async: false,
            url: rootUrl + "cms/Catalog/_ProductEdit",
            data: "groupGuid=" + groupGuid + "&productGuid=" + productGuid,
            success: function (result) { $('#editorcontent').html(result).fadeIn('slow'); },
            error: function () { },
            complete: function () {
                try {
                    if (tinyMCE.editors.length > 0) {
                        try {
                            tinymce.remove();
                        } catch (e) {

                        }
                    }
                } catch (err) { }

                setUpGroupEditPost();
                setMultiLanguageClick();
                setDateTimePickers();
                loadModalMultiImagePopUpInit();
                loadModalImagePopUpInit();
                loadModalAttachmentPopUpInit();
                initializeModals();

                setTimeout(function () {
                    setTinyMce();
                }, 1000);
            }
        });
    }

    // Check for unsaved data    
    function alertForUnsavedData() {
        var bIsHandled;
        if (bHasUnsavedData) {
            var confirmationText = getTranslation("CATMAN_CONFIRM_UNSAVED_DATA_MESSAGE", "You have unsaved data. If you continue the changes will be lost. Do you wish to continue?");
            if (confirm(confirmationText)) {
                bIsHandled = true;
                bHasUnsavedData = false; // Reset parameter
            } else {
                bIsHandled = false;
            }
        }
        else {
            bIsHandled = true;
        }
        return bIsHandled;
    }

    var isDragMode = true;

    var selectedItem = $(".active");
    var parent = $(selectedItem).parents('ul').first();
    if ($(parent).attr('id') == 'catalog') {
        $('#catalog').addClass('in');
    }

    // ** Initialization **
    setTinyMce();
    setSitemapLinkClickToLoadGroupEditAndSelectedProducts();
    setInitDraggable();
    makeSitemapDroppable();
    createExpandToggle();
    setUpGroupEditPost();
    setDateTimePickers();
    loadModalMultiImagePopUpInit();
    loadModalImagePopUpInit();
    loadModalAttachmentPopUpInit();
    setMultiLanguageClick();
    // ** End Initialization **

    // ** DateTime Pickers **
    function setDateTimePickers() {

        // Clear any cached values
        $('#STARTDATE').datetimepicker("destroy");
        $('#ENDDATE').datetimepicker("destroy");

        var initialDate = $('#STARTDATE').val();

        var lang = getPageLanguageGuid();

        if (lang == 'en') { lang = ''; }

        $.datepicker.setDefaults($.datepicker.regional[lang]);
        $.timepicker.setDefaults($.timepicker.regional[lang]);

        initStartDatetimePicker();
        initEndDatetimePicker();

        function initStartDatetimePicker() {
            $('#STARTDATE').datetimepicker({
                dateFormat: 'yy-mm-dd', showSecond: true, timeFormat: 'hh:mm:ss',
                onClose: function (dateText, inst) {
                    var endDateTextBox = $('#ENDDATE');
                    if (endDateTextBox.val() != '') {
                        if ($(this).val() != '') {
                            var dateParts = $(this).val().split(' ');
                            var theDate = dateParts[0] + 'T' + dateParts[1];
                            var testStartDate = new Date(theDate);
                            var endDateParts = endDateTextBox.val().split(' ');
                            theDate = endDateParts[0] + 'T' + endDateParts[1];
                            var testEndDate = new Date(theDate);
                            if (testStartDate > testEndDate) {
                                endDateTextBox.val(dateText);
                            }
                        }
                    }
                    else {
                        endDateTextBox.val(dateText);
                    }
                },
                onSelect: function (selectedDateTime) {
                    var start = $(this).datetimepicker('getDate');

                }
            });
        }

        function initEndDatetimePicker() {
            $('#ENDDATE').datetimepicker({
                dateFormat: 'yy-mm-dd', showSecond: true, timeFormat: 'hh:mm:ss',
                onClose: function (dateText, inst) {
                    var startDateTextBox = $('#STARTDATE');
                    if (startDateTextBox.val() != '') {
                        if ($(this).val() != '') {
                            var dateParts = $(this).val().split(' ');
                            var theDate = dateParts[0] + 'T' + dateParts[1];
                            var testEndDate = new Date(theDate);

                            var startDateParts = startDateTextBox.val().split(' ');
                            var startDate = startDateParts[0] + 'T' + startDateParts[1];
                            var testStartDate = new Date(startDate);

                            if (testStartDate > testEndDate) {
                                startDateTextBox.val(dateText);
                            }
                        }
                    }
                    else {
                        startDateTextBox.val(dateText);
                    }
                },
                onSelect: function (selectedDateTime) {
                    var end = $(this).datetimepicker('getDate');
                    setDatetimePickerMaxDate(end.getTime());
                }
            });
        }

        function setDatetimePickerMaxDate(endTime) {
            // Clear any cached values
            $('#STARTDATE').datetimepicker('option', { minDate: null, maxDate: null });
            $('#STARTDATE').datetimepicker("destroy");
            initStartDatetimePicker();
        }

        $('#STARTDATE').val(initialDate);

        // Prevent user to add/change values manually

        $('#STARTDATE').keydown(function (e) {
            if (e.keyCode == 46 || e.keyCode == 8) {
                //Delete and backspace clear text 
                $(this).val(''); //Clear text
                $(this).datepicker('option', { minDate: null, maxDate: null });
                $(this).datepicker("hide"); //Hide the datepicker calendar if displayed
                $(this).blur(); //aka "unfocus"
            }
            //Prevent user from manually entering in a date - have to use the datepicker box
            e.preventDefault();
        });

        $('#ENDDATE').keydown(function (e) {
            if (e.keyCode == 46 || e.keyCode == 8) {
                //Delete and backspace clear text 
                $(this).val(''); //Clear text
                $(this).datepicker('option', { minDate: null, maxDate: null });
                $(this).datepicker("hide"); //Hide the datepicker calendar if displayed
                $(this).blur(); //aka "unfocus"
            }
            //Prevent user from manually entering in a date - have to use the datepicker box
            e.preventDefault();
        });

        //Hide date picker on scroll because of absolute position problem over overflow auto area

        var dtPckr1 = $('#STARTDATE');
        var dtPckr2 = $('#ENDDATE');

        $(".edit").scroll(function () {
            dtPckr1.datepicker('hide');
            dtPckr2.datepicker('hide');

            $("input").blur();

        });

    }
    // ** End DateTime Pickers **

    // ** The sitemap menu functionality **
    $('#groupproducts li').showMenu({ opacity: 1.8, query: "#myMenu2", zindex: 2000 });

    $('#myMenu2').click((function () {
        var ungrouped = $('.ungrouped');
        ungrouped.each(function () { });
    }));

    $('#sitemap dl').showMenu({ opacity: 1.8, query: "#myMenu", zindex: 2000 });

    $('#myMenu').click((function (e) {
        var clicked = $('#myMenu').data("target");
        var li = $($(clicked).parents('li')[0]);
        var elementId = li.attr('id');
        var actionId = $(e.target).attr('id');
        if (actionId == 'mcreate') {
            handleSitemapCookie(elementId);
            createNewGroup(elementId.replace(new RegExp("id_"), ""), 1, 1, li);
        } else if (actionId == 'mdelete') {
            handleSitemapCookie(elementId);
            var question = getTranslation('CATMAN_CONFIRM_DELETE', 'Are you sure?');
            if (confirm(question)) {
                deleteGroup(elementId.replace(new RegExp("id_"), ""), li);
            }
        }
    }));

    $('#myMenu li').hover(function () { $(this).addClass('mmenu-hover'); }, function () { $(this).removeClass('mmenu-hover'); });

    function handleSitemapCookie(elementId) {
        var arrayPosition = inArray(elementId, contractedItems);
        if (arrayPosition >= 0) {
            contractedItems.splice(arrayPosition, 1);
            $.cookie('contractedItems', contractedItems, { path: "/", expires: 7 });
        }
    }

    function loadProductsView() {
        $.ajax({
            type: "POST",
            async: true,
            url: rootUrl + "cms/Catalog/_Products",
            success: function (result) {
                $('#editorcontent').html(result).fadeIn('fast');
            },
            error: function () {

            }
        });
    }

    function loadGroupProductView(groupGuid) {
        $.ajax({
            type: "POST",
            async: true,
            url: rootUrl + "cms/Catalog/_GroupProducts",
            data: "id=" + groupGuid,
            success: function (result) {
                $('#editorcontent').html(result).fadeIn('fast');
                initializeModals();
            },
            error: function () {

            }
        });
    }

    function deleteGroup(groupGuid, theParent) {
        $.ajax({
            type: "POST",
            url: rootUrl + "cms/Catalog/DeleteGroup",
            data: "groupGuid=" + groupGuid,
            success: function (result) {
                var iResult = 0;
                try {
                    iResult = parseInt(result, 10);
                } catch (er) {
                }
                if (iResult == -100) {
                    window.location = rootUrl + "CMS";
                } else {
                    var theParentUl = theParent.parents('ul:first');
                    var theParentLi = theParentUl.parents('li:first');
                    var theDiv = theParentLi.children('.expand');
                    theParent.replaceWith('');
                    if (theParentUl.children().length == 0) {
                        theDiv.replaceWith('');
                    }
                    updateGroupEdit(iResult);
                    //updateAllProducts(iResult);
                    var theElement = $('#id_' + iResult + ' a:first');
                    setSiteMapSelectedFontWeight(theElement);
                }
            },
            error: function () { }
        });
    }

    function setSiteMapSelectedFontWeight(element) {
        $('.grouptreelink').css({ fontWeight: 'normal' }).removeClass("active");
        element.css({ fontWeight: "bold" }).addClass("active");
    }

    function createNewGroup(parentGuid, groupGuid, siblingGuid, theParent) {
        $.ajax({
            type: "POST",
            async: false,
            url: rootUrl + "cms/Catalog/CreateNewGroup",
            data: "parentGuid=" + parentGuid + "&siblingGuid=" + siblingGuid,
            success: function (result) {
                var iResult = parseInt(result, 10);
                loadSubGroups(theParent);
                try {
                    theParent.children('span:first').removeClass('contract');
                } catch (e) {
                }
                var theElement = $('#id_' + iResult + ' a:first');
                setSiteMapSelectedFontWeight(theElement);
                updateGroupEdit(iResult);
                //updateAllProducts(iResult);
            },
            error: function () { }
        });
    }

    // ** End sitemap menu functionality **

    /* "theDragStartParentId" is the parent ID of the item selected on drag start (what it's is moving FROM) 
    Needed by the drop function, so the item can be removed from it's former parent visually.
    */
    var theDragStartParentId;
    /* Need to keep knowledge of draggable here */
    var isDraggableChild = false;
    var draggableGroupGuid;
    var draggableParentGuid;
    var hasChildrenOnDemand = false;
    var isUngroupedMoved = false;

    function setInitDraggable() {
        var ctop = 20;
        var cleft = 22;
        $('#sitemap li').draggable({
            handle: ' > dl',
            opacity: 0.8,
            addClasses: false,
            helper: 'clone',
            cursorAt: { top: ctop, left: cleft },
            zIndex: 100,
            start: function () {
                theDragStartParentId = $(this).parents('li:first').attr('id');
            },
            stop: function () {
                var newParent;
                if (hasChildrenOnDemand) {
                    newParent = $(this).parents('li:first'); // Moved here because "this" does not get rigth object inside timeout function.
                }
                // Add spinner here
                //addSpinner();
                setTimeout(function () {
                    if (isDraggableChild) {
                        // Make ajax call in drag stop as a workaround for the strange behaviour when using timeout at the end of drop event.
                        // Drag stop fires AFTER drop event completes...
                        // Make blocking call here
                        saveListRegrouping(draggableParentGuid, draggableGroupGuid);
                        draggableGroupGuid = '';
                        draggableParentGuid = '';
                        isDraggableChild = false;
                    }
                    if (hasChildrenOnDemand) {
                        //newParent = $(this).parents('li:first'); // Declaration moved outside timeout function...
                        hasChildrenOnDemand = false;
                        // Make blocking call here
                        loadSubGroups(newParent);
                        newParent.children('span:first').removeClass('contract');
                    }
                    // Remove spinner here
                    //removeSpinner();
                }, 100);
            }
        });
    }

    function makeSitemapDroppable() {

        $('#sitemap dl,#sitemap .dropzone, .sortablezone').droppable({
            accept: '#sitemap li, #groupProductTable tr, #productTable tr',
            tolerance: 'pointer',
            drop: function (e, ui) {
                $(this).removeClass('dropzoneProduct-hover');
                $(this).removeClass('dropzoneGroup-hover');
                var li;
                var parentGuid;
                if ($(ui.draggable).is('#groupProductTable tr')) {
                    // Here MOVING products between groups is handled         
                    var groupedArray = new Array();
                    var groupGuid1 = $('#grpProductGuid').attr('value');
                    var deleteArray = new Array();
                    var table = ExpandIT_ProductTable.Table;
                    $('.ungrouped').each(function (j) {
                        var groupProductGuid = $(this).find('.chk').attr('name');
                        var arrPos0 = inArray(groupProductGuid, deleteArray);
                        if (arrPos0 == -1) { deleteArray.push(groupProductGuid); }
                        // ProductGuid
                        var productGuid = $(this).find('.chk').attr("name").trim();
                        var arrPos = inArray(productGuid, groupedArray);
                        if (arrPos == -1) {
                            groupedArray.push(productGuid);
                        }
                        var aPos = table.fnGetPosition($(this).closest('tr')[0]);
                        table.fnDeleteRow(aPos);
                    });
                    ExpandIT_ProductTable.RemoveProducts(deleteArray, groupGuid1);
                    isUngroupedMoved = true;
                    li = $(this).parent();
                    parentGuid = li.attr('id');
                    ExpandIT_ProductTable.AddProducts(groupedArray, parentGuid.replace(new RegExp("id_"), "")); // Ajax call                
                    isDragMode = true;
                } else if ($(ui.draggable).is('#productTable tr')) {
                    // Here adding products from product table to specific group is handled
                    li = $(this).parent();
                    parentGuid = li.attr('id');
                    groupGuid = parentGuid.replace(new RegExp("id_"), "");
                    var theArray = new Array();
                    $('.ungrouped').each(function (j) {
                        // ProductGuid
                        var productGuid = $(this).find('.chk').attr("name").trim();
                        var arrPos = inArray(productGuid, theArray);
                        if (arrPos == -1) {
                            theArray.push(productGuid);
                        }
                    });
                    if (theArray.length > 0) {
                        ExpandIT_ProductTable.AddProducts(theArray, groupGuid);
                        isDragMode = false;
                    }
                } else {
                    var sort = $(this).hasClass('sortablezone');
                    var groupGuid = ui.draggable.attr('id');
                    if (sort) {
                        var sibling = $(this).parent();
                        var newParent = $(sibling).parents('li');
                        var newParentGuid = newParent.attr('id');
                        if (newParentGuid == null) {
                            newParentGuid = "id_0";
                        }
                        var siblingGuid = sibling.attr('id');
                        sibling.before(ui.draggable);
                        // Add the spinner here
                        addSpinner();
                        // Make the call after wait
                        setTimeout(function () {
                            saveListChanges(newParentGuid.replace(new RegExp("id_"), ""), groupGuid.replace(new RegExp("id_"), ""), siblingGuid.replace(new RegExp("id_"), ""));
                            // Remove spinner here
                            removeSpinner();
                        }, 300);
                    } else {
                        li = $(this).parent();
                        parentGuid = li.attr('id');
                        groupGuid = ui.draggable.attr('id');
                        var child = !$(this).hasClass('dropzone');
                        if (child && li.children('ul').length == 0) {
                            li.append('<ul/>');
                        }
                        if (child) {
                            // Add knowledge to global here to be used by drag stop event
                            isDraggableChild = true;
                            var theDiv = li.children('div:first');
                            if ($('#' + parentGuid + ' span:first').hasClass('isparent')) {
                                hasChildrenOnDemand = true;
                            }
                            if (!hasChildrenOnDemand) {
                                if (li.children('span:first').attr('class') == undefined) {
                                    theDiv.after('<span class="expand" />');
                                    createExpandToggle();
                                }
                            }
                            var hasChildrenLeft = false;
                            if ($('#' + theDragStartParentId + ' span:first').hasClass('isparent') && ($('#' + theDragStartParentId + ' span:first').hasClass('contract'))) {
                                hasChildrenLeft = true;
                            } else {
                                var theRestOfTheChildren = $('#' + theDragStartParentId + ' ul > li');
                                $(theRestOfTheChildren).each(function () {
                                    var currentId = $(this).attr('id');
                                    var childsParent = $(this).parent().parent().attr('id');
                                    if (currentId != groupGuid) {
                                        if (childsParent != groupGuid) {
                                            hasChildrenLeft = true;
                                            return false;
                                        }
                                    }
                                });
                            }
                            // Append draggable to the new parent
                            li.children('ul').append(ui.draggable);
                            if (!hasChildrenLeft) {
                                // Remove the plus sign
                                $('#' + theDragStartParentId + ' span:first').replaceWith('');
                            }
                            // Add knowledge to global here to be used by drag stop event
                            draggableParentGuid = parentGuid.replace(new RegExp("id_"), "");
                            draggableGroupGuid = groupGuid.replace(new RegExp("id_"), "");
                            // Do Ajax call in drag stop event...
                        }
                    }
                }
            },
            over: function (e, ui) {
                if ($(this).is('.sortablezone') && (ui.draggable.is('#sitemap li'))) {
                    $(this).addClass('dropzoneGroup-hover');
                } else if ($(this).is('.ui-droppable') && (!$(this).is('.sortablezone'))) {
                    $(this).addClass('dropzoneProduct-hover');
                }
            },
            out: function () {
                $(this).removeClass('dropzoneProduct-hover');
                $(this).removeClass('dropzoneGroup-hover');
            }
        });
    }

    var contractedItems = new Array();
    if (jQuery.cookie) {
        if (jQuery.cookie('contractedItems')) {
            contractedItems = jQuery.cookie('contractedItems').split(',');
            for (var i = 0; i < contractedItems.length; ++i) {
                if ($(jQuery('#' + contractedItems[i])).is('.expanded')) {
                    jQuery('#' + contractedItems[i]).removeClass('expanded');
                }
                if (!$(jQuery('#' + contractedItems[i])).is('.contracted')) {
                    jQuery('#' + contractedItems[i]).addClass('contracted');
                }
                $('.contracted').children('span').addClass('contract');
            }
            $('.contracted').children('ul').hide();
        }
    }

    $.fn.toggleClick = function () {
        var functions = arguments;
        return this.click(function () {
            var iteration = $(this).data('iteration') || 0;
            functions[iteration].apply(this, arguments);
            iteration = (iteration + 1) % functions.length;
            $(this).data('iteration', iteration);
        });
    };

    function createExpandToggle() {
        $('.expand').click(function () {
            var that = this;
            if ($(this).is('.isparent')) {
                if (!$(this).is('.appended')) {
                    var theParent = $(this).parent('li');
                    loadSubGroups(theParent);
                    $(this).addClass('appended');
                }
            }
            $(this).parent('li').toggleClick(function (event) {
                if ($(that).is('.contract')) {
                    $(this).children('ul').show();
                    $(this).removeClass('expanded');
                    $(this).addClass('contracted');
                    $(that).removeClass('contract');
                } else {
                    $(this).children('ul').hide();
                    $(this).removeClass('contracted');
                    $(this).addClass('expanded');
                    $(that).addClass('contract');
                }
                event.stopImmediatePropagation();
                updateArray($(this).attr('id'));
            }, function (event) {
                if (!$(that).is('.contract')) {
                    $(this).children('ul').hide();
                    $(this).addClass('expanded');
                    $(this).removeClass('contracted');
                    $(that).addClass('contract');
                } else {
                    $(this).children('ul').show();
                    $(this).addClass('contracted');
                    $(this).removeClass('expanded');
                    $(that).removeClass('contract');
                }
                event.stopImmediatePropagation();
                updateArray($(this).attr('id'));
            });
        });
    }

    function loadSubGroups(theParent) {
        var groupGuid = $(theParent).attr('id');
        groupGuid = groupGuid.substring(3, groupGuid.length);
        $.ajax({
            type: "POST",
            async: false,
            url: rootUrl + "cms/Catalog/LoadSubGroups",
            data: "groupGuid=" + groupGuid,
            success: function (result) {
                var theDiv;
                if (theParent.children('ul').length == 0) {
                    $(theParent).append(result);
                    if ($(theParent).children('.expand').length == 0) {
                        theDiv = theParent.children('div:first');
                        theDiv.after('<span class="expand" />');
                    }
                } else {
                    theParent.children('ul').replaceWith('');
                    theParent.append('<ul>');
                    $(theParent).children('ul:first').replaceWith(result);
                    if ($(theParent).children('.expand').length == 0) {
                        theDiv = theParent.children('div:first');
                        theDiv.after('<span class="expand" />');
                    }
                }
                makeDraggable(result);
            },
            error: function () { }
        });
    }

    function makeDraggable(result) {
        createExpandToggle();
        setInitDraggable();
        $('#sitemap li').showMenu({ opacity: 1.8, query: "#myMenu", zindex: 2000 });
        makeSitemapDroppable();
        setSitemapLinkClickToLoadGroupEditAndSelectedProducts();
    }

    function updateArray(eId) {
        var arrPos = inArray(eId, contractedItems);
        if (arrPos == -1) {
            contractedItems.push(eId);
        } else {
            contractedItems.splice(arrPos, 1);
        }
        $.cookie('contractedItems', contractedItems, { path: "/", expires: 7 });
    }

    function inArray(elem, array) {
        for (i = 0; i < array.length; i++) {
            if (array[i] === elem) {
                return i;
            }
        }
        return -1;
    }

    /* Moving Groups */
    function saveListChanges(parentGuid, groupGuid, siblingGuid) {
        $.ajax({
            type: "POST",
            url: rootUrl + "cms/Catalog/SaveGroupTableChanges",
            data: "parentGuid=" + parentGuid + "&groupGuid=" + groupGuid + "&siblingGuid=" + siblingGuid,
            success: function (result) { },
            error: function () { }
        });
    }

    function saveListRegrouping(parentGuid, groupGuid) {
        $.ajax({
            type: "POST",
            async: false,
            url: rootUrl + "cms/Catalog/SaveGroupTableReordering",
            data: "parentGuid=" + parentGuid + "&groupGuid=" + groupGuid,
            success: function (result) { },
            error: function () { }
        });
    }

    /* Handles Sitemap click event */
    function setSitemapLinkClickToLoadGroupEditAndSelectedProducts() {
        $('.grouptreelink').bind('click', function (event) {
            event.preventDefault();
            if (alertForUnsavedData()) {
                isDragMode = true;
                setSiteMapSelectedFontWeight($(this));
                $("#catalog>li>a").removeClass("active");
                $("#catalogSitemap").addClass("active");

                var parentGuid = $(this).parents('li:first').attr('id');
                parentGuid = parentGuid.replace(new RegExp("id_"), "");
                updateGroupEdit(parentGuid);
                event.stopImmediatePropagation();
            }
            event.stopImmediatePropagation();
        });
    }

    function updateGroupEdit(groupGuid) {
        $.ajax({
            type: "POST",
            async: false,
            url: rootUrl + "cms/Catalog/_TabbedGroupEdit",
            data: "id=" + groupGuid,
            success: function (result) {
                try {
                    if (tinyMCE.editors.length > 0) {
                        try {
                            tinymce.remove();
                        } catch (e) {

                        }
                    }
                } catch (err) { }
                $('#editorcontent').html(result).fadeIn('fast');
                document.title = "ExpandIT Shop - " + $(result).find("h1.page-header").html();
                ExpandIT_ProductTable.Table = $("#groupProductTable").dataTable({
                    "order": [1, 'asc'],
                    "processing": true,
                    "serverSide": true,
                    "ajax": {
                        "url": rootUrl + "cms/Catalog/GetGroupProducts",
                        "type": "POST",
                        "data": function (d) {
                            d.groupGuid = groupGuid;
                        }
                    },
                    "language": {
                        "url": rootUrl + "Content/datatables/i18n/en.txt",
                    },
                    "columns": [
                        { "data": "CheckboxHtml" },
                        { "data": "ProductGuid" },
                        { "data": "PictureHtml" },
                        { "data": "ProductNameTranslate" },
                        { "data": "ListPrice" },
                        { "data": "ProductGrp" },
                        { "data": "EditButtonHtml" }
                    ],
                    "columnDefs": [
                        { 'orderable': false, 'aTargets': [0, 2, -1] }
                    ],
                    "drawCallback": function () {
                        $("#select_all").attr("checked", false);
                        setDataTableDraggable();
                    }
                });

                function setDataTableDraggable() {
                    $.fn.dataTable.ext.errMode = 'throw';
                    $("#select_all").change(function () {
                        if (this.checked) {
                            $(".chk").each(function () {
                                this.checked = true;
                                $(this).parent().parent().addClass("ungrouped");
                            });
                        } else {
                            $(".chk").each(function () {
                                this.checked = false;
                                $(this).parent().parent().removeClass("ungrouped");
                            });
                        }
                    });

                    $(".dataTable tbody tr").click(function (e) {
                        if (e.target.type != "checkbox") {
                            $(this).find("input[type=checkbox]").click();
                        }
                    });

                    $(".chk").click(function () {
                        if (!$(this).is(":checked")) {
                            $("#select_all").prop("checked", false);
                            $(this).parent().parent().removeClass("ungrouped");
                        } else {
                            var flag = 0;
                            $(".chk").each(function () {
                                if (!this.checked) {
                                    flag = 1;
                                    $(this).parent().parent().removeClass("ungrouped");
                                } else {
                                    $(this).parent().parent().addClass("ungrouped");
                                }
                            });
                            if (flag == 0) { $("#select_all").prop("checked", true); }
                        }
                    });

                    // Remove
                    $('#button1, #button2').click(function () {
                        var groupGuid = $('#grpProductGuid').attr('value');
                        var deleteArray = new Array();
                        var table = ExpandIT_ProductTable.Table;
                        $(".ungrouped").each(function () {
                            var groupProductGuid = $(this).find(".chk").attr('name');
                            var arrPos = inArray(groupProductGuid, deleteArray);
                            if (arrPos == -1) { deleteArray.push(groupProductGuid); }
                            var aPos = table.fnGetPosition($(this).closest('tr')[0]);
                            table.fnDeleteRow(aPos);
                        });
                        ExpandIT_ProductTable.RemoveProducts(deleteArray, groupGuid);
                    });

                    function inArray(elem, array) {
                        for (var i = 0; i < array.length; i++) { if (array[i] === elem) { return i; } }
                        return -1;
                    }

                    // Drag
                    $('#groupProductTable tr').draggable({
                        helper: function () {
                            var selected = $('.ungrouped');
                            var bgColor = selected.css('backgroundColor');
                            var container = $('<table/>').attr('id', 'draggingContainer');
                            $(container).attr('class', 'table-striped');
                            $(container).css({ zIndex: '2000' });
                            var theClone = selected.clone();
                            $(theClone).css({ backgroundColor: bgColor, borderColor: '#FFFFFF', opacity: '0.75' });
                            $(theClone).css({ backgroundImage: 'url(../../../Content/images/copy.png")', backgroundPosition: "0px 0px", backgroundRepeat: 'no-repeat', paddingLeft: '20px' });
                            container.append(theClone);
                            return container;
                        },
                        appendTo: 'body',
                        cursorAt: { top: 2, left: -22 },
                        cursor: 'move'
                    });
                }
            },
            error: function () {

            },
            complete: function () {
                setUpGroupEditPost();
                setMultiLanguageClick();
                setDateTimePickers();
                loadModalMultiImagePopUpInit();
                loadModalImagePopUpInit();
                loadModalAttachmentPopUpInit();
                initializeModals();

                setTimeout(function () {
                    setTinyMce();
                }, 1000);
            }
        });

        //loadModalAttachmentPopUpInit();

        //setUpProductEditPost();

    }

    function setUpGroupEditPost() {

        $('.dynamicLinkSelect').change(function () {
            // get id
            var theFullId = $(this).attr('id');
            var arr = theFullId.split('_');
            var theId = arr[arr.length - 1];
            var selectedValue = $(this).val();
            if (selectedValue == '----------') { selectedValue = ''; }
            $('#' + theId).val(selectedValue);
        });

        $('.dynamicAspxLinkSelect').change(function () {
            // get id
            var theFullId = $(this).attr('id');
            var arr = theFullId.split('_');
            var theId = arr[arr.length - 1];
            var selectedValue = $(this).val();
            if (selectedValue == '----------') { selectedValue = ''; }
            $('#' + theId).val(selectedValue);
        });

        $('#Templates').change(function () {
            var selectedTemplate = $('#Templates').val();
            var templateTextId = $('#template_text_id').val();
            $('#' + templateTextId).val(selectedTemplate);
            var groupGuid = $('#id').val();
            // Add spinner here
            addSpinner();
            setTimeout(function () {
                saveTemplate();
                // Remove spinner here
                removeSpinner();
                updateGroupEdit(groupGuid);
            }, 100);
        });

        setExternalLinkclick("previewbutton1, #previewbutton2", "GroupGuid", $('#id').val(), '', $('#langid').val());

        $("[name=savebutton]").click(function () {
            // Add spinner here
            addSpinner();
            setTimeout(function () {
                saveGroup();
                // Remove spinner here
                removeSpinner();
            }, 100);
        });
    }

    function saveTemplate() {
        tinyMCE.triggerSave();
        var groupGuid = $('#id').val();
        var groupEditForm = $('#groupeditform').serialize();
        $.ajax({
            type: "POST",
            async: false,
            url: rootUrl + "cms/Catalog/TemplateEdit",
            data: "id=" + groupGuid + "&collection=" + groupEditForm,
            success: function (result) {
                saveDisplay($("#savebutton"), result, 100, -25);
            },
            error: function (result) {

            }
        });
    }

    function saveGroup() {
        tinyMCE.triggerSave();
        var groupGuid = $('#id').val();
        var groupEditForm = $('#groupeditform').serialize();
        $.ajax({
            type: "POST",
            async: false,
            url: rootUrl + "cms/Catalog/GroupEdit",
            data: "id=" + groupGuid + "&collection=" + groupEditForm,
            success: function (result) {
                var theName = $('#NAME');
                var liref = $('#id_' + groupGuid + ' .grouptreelink:first');
                var theVal = theName.val();
                if (theVal == '') { theVal = "Unknown Property"; }
                liref.html(theVal);
                $('.page-header').html(theVal);
                saveDisplay($("#savebutton"), result, 100, -25);
            },
            error: function (result) {

            }
        });
    }

    function saveDisplay(element, message, offLeft, offTop) {
        $('#savemessagetext').html(message).center().fadeIn();
        setTimeout(function () { $('#savemessagetext').fadeOut(); }, 3000);
    }


    function setTinyMce() {
        var localRootUrl;
        if (rootUrl == '') {
            localRootUrl = '/';
        } else {
            localRootUrl = rootUrl;
        }
        tinymce.init({
            selector: '.mceEditor',
            theme: 'modern',
            skin: 'lightgray',
            plugins: [
                "advlist autolink autosave link image lists charmap print preview hr anchor pagebreak spellchecker",
                "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                "table contextmenu directionality emoticons template textcolor paste textcolor colorpicker textpattern"
            ],
            //plugins: "example, advlist, autolink, autosave, link, image, lists, charmap, print, preview, hr, anchor, pagebreak, spellchecker, searchreplace, wordcount, visualblocks, visualchars, code, fullscreen, insertdatetime, media, nonbreaking, table, contextmenu, directionality, emoticons, template, textcolor, paste, textcolor, colorpicker, textpattern",
            //image_advtab: true,
            //image_list: [
            //{title: 'Dog', value: 'mydog.jpg'},
            //{title: 'Cat', value: 'mycat.gif'}
            //],
            contextmenu: "link image inserttable | cell row column deletetable",

            toolbar1: "undo redo | styleselect formatselect fontselect fontsizeselect | cut copy paste | searchreplace | print fullscreen",
            toolbar2: "bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | bullist numlist | outdent indent blockquote | insertdatetime preview | forecolor backcolor",
            toolbar3: "link unlink anchor image media code | table | hr removeformat | subscript superscript | charmap emoticons | ltr rtl |  visualchars visualblocks nonbreaking pagebreak | spellchecker example",

            //menu : { // this is the complete default configuration
            //    file   : {title : 'File'  , items : 'newdocument'},
            //    edit   : {title : 'Edit'  , items : 'undo redo | cut copy paste pastetext | selectall'},
            //    insert : {title : 'Insert', items : 'link media | template hr'},
            //    view   : {title : 'View'  , items : 'visualaid'},
            //    format : {title : 'Format', items : 'bold italic underline strikethrough superscript subscript | formats | removeformat'},
            //    table  : {title : 'Table' , items : 'inserttable tableprops deletetable | cell row column'},
            //    tools  : {title : 'Tools' , items : 'spellchecker code example'}
            //},
            menubar: false,
            relative_urls: true,
            document_base_url: localRootUrl + 'templates/'
        });
    }

    $("#TemplatesP").change(function () {
        $("#TEMPLATE").val(($(this).val()));
    });

    // ** MULTI LANGUAGE CHECKBOXES
    function getPropOwnerRefGuid() { return $('#pid').attr('value'); }
    function setMultiLanguageClick() {
        $('.mlchk').click(function () {
            if (bHasUnsavedData) {
                $(this).is(':checked') ? $(this).attr('checked', false) : $(this).attr('checked', true);
                alert('Save your changes before trying to change this setting');
                return;
            }
            var propGuid = $(this).attr('name');
            if (propGuid.length > 0) {
                propGuid = propGuid.substring(1, propGuid.length);
                var propOwnerRefGuid = getPropOwnerRefGuid();
                var languageGuid = getLanguageGuid();
                var defaultValue;
                var transString;
                var replacedString;
                if ($(this).is(':checked')) {
                    defaultValue = "You have changed the property '%REPLACE%' to multi language. Do you want to copy the current value to all other languages?";
                    transString = getTranslation("CATMAN_CONFIRM_ML_QUESTION", defaultValue);
                    replacedString = transString.replace('%REPLACE%', propGuid);
                    if (confirm(replacedString)) { null2MultiLanguage(propOwnerRefGuid, propGuid, languageGuid, "MULTI"); }
                    else { $(this).attr('checked', false); }
                }
                else {
                    defaultValue = "You are about to disable the multi language setting for the property '%REPLACE%'. This will permanently delete all translated values of this property. Only the value for the current language will be preserved.\n\n Do you want to continue?";
                    transString = getTranslation("CATMAN_CONFIRM_ML_WARNING", defaultValue);
                    replacedString = transString.replace('%REPLACE%', propGuid); if (confirm(replacedString)) { multiLanguage2Null(propOwnerRefGuid, propGuid, languageGuid); }
                    else { $(this).attr('checked', true); }
                }
            }
            else { }
        });
    }
    function multiLanguage2Null(propOwnerRefGuid, propGuid, languageGuid) {
        $.ajax({
            type: "POST",
            url: rootUrl + "cms/Catalog/MultiLanguageToNull",
            data: "propOwnerRefGuid=" + propOwnerRefGuid + "&propGuid=" + propGuid + "&languageGuid=" + languageGuid,
            success: function (result) { },
            error: function () {

            }
        });
    }
    function null2MultiLanguage(propOwnerRefGuid, propGuid, languageGuid, nullParam) {
        $.ajax({
            type: "POST",
            url: rootUrl + "cms/Catalog/NullToMultiLanguage",
            data: "propOwnerRefGuid=" + propOwnerRefGuid + "&propGuid=" + propGuid + "&languageGuid=" + languageGuid + "&nullParam=" + nullParam,
            success: function (result) { },
            error: function () {

            }
        });
    }
    // ** END MULTI LANGAUE CHECKBOXES **
});

// ** END WINDOW.LOAD ** //

function addSpinner() {
    $('#throbber').remove();
    $("body").append('<div id="throbber"><img src="' + rootUrl + 'EISCMS/Content/images/spinner.gif" alt="wait"/></div>');
    $("#throbber").center();
    $("#throbber").css({ zIndex: '200000' });
}

function removeSpinner() {
    $('#throbber').remove();
}

/*
Multi Image Popup
*/

var buttonForMultiImage;
var selectedMultiImageValue = "";
var firstMultiImageItem = "";

// The Image Object (used by multi image)
function ImageObject(img, alt, target, text, link, time, id) {
    this.Img = img;
    this.Alt = alt;
    this.Target = target;
    this.Text = text;
    this.Link = link;
    this.Time = time;
    this.Id = id;
}

function ImageObjectsToString(imageObjects) {
    // First sort images according to their sort index
    imageObjects.sort(sortBySortIndex);
    // Now output persistence string
    var imgObjString = '';
    for (var i = 0; i < imageObjects.length; i++) {
        imgObjString += imageObjects[i].Img + '|' + imageObjects[i].Alt + '|' + imageObjects[i].Target + '|' + imageObjects[i].Text + '|' + imageObjects[i].Link + '|' + imageObjects[i].Time + '|';
    }
    return imgObjString.substring(0, imgObjString.length - 1);
}

function sortBySortIndex(obj1, obj2) {
    return obj1.SortIndex - obj2.SortIndex;
}

function getSelectedMultiImageItem() { return $("#" + buttonForMultiImage); }
function getSelectedMultiImageValue() { return selectedMultiImageValue; }

function makeMultiImagePopUpModal(rootUrl, pButtonFor) {
    selectedMultiImageValue = "";
    buttonForMultiImage = pButtonFor; // Probably MULTIIMAGE
    var onLoadPicture = "";
    try { onLoadPicture = $('#' + buttonForMultiImage).val(); } catch (e) { }
    var imageObjects = initImageObjects(onLoadPicture);
    try { $('#multiImagePopupModal').remove(); } catch (e) { }
    $('body').append('<div id="multiImagePopupModal" \/>');

    $('#multiImagePopupModal').dialog({
        autoOpen: false,
        bgiframe: false,
        resizable: false,
        width: 850,
        height: 615,
        modal: true,
        position: { my: 'top', at: 'top+150' },
        dialogClass: 'modalWithoutTitleBar',
        show: 'slideDown',
        hide: 'slideUp',
        close: function () {
            imageObjects = null; // Called twice?
            selectedMultiImageValue = "";
            $(this).remove();
            try { $('#helpContent').remove(); } catch (err) { }
        },
        beforeclose: function () {
            $('#picPop').remove();
        }
    }).load(rootUrl + "cms/Catalog/MultiImageBrowser", {"selectedValue" : escape(onLoadPicture)}, function () {
        //$(this).dialog('option', 'title', 'HANDLE_IMAGES');
        try {
            $('#popupModalHeaderSpan').remove();
        } catch (err) { }
        $('#ui-dialog-title-popupModal').after('<span id="popupModalHeaderSpan" \/>');
        $('#popupModalHeaderSpan').html('<span id="popupModalHelpWrap"><input type="button" id="btnPopupModalHelpOpen" class="helpButton" value="." /></span>');

        InsertPics(imageObjects, rootUrl);
        FillMultiImageTextboxes(imageObjects[0]);
        initCurrentImage(imageObjects);

        setBtnMultiSelectClick(imageObjects); // Init btnSelect click
        setMultiImageBtnCancelClick();
        setMultiImageBtnSaveClick(imageObjects);

        $(this).dialog("open");

        setTimeout(function () {
            loadMultiFileTree(null);
        }, 900);
    });

    function loadMultiFileTree(currentFolder, currentImage) {
        var validExtensions = 'bmp,gif,jpg,jpeg,png,txt';
        $('#multiPicFileTree').fileTree({
            root: '/' + ExpandIT_BaseUploadFolder.UploadFolder() + '/',
            script: rootUrl + 'cms/Catalog/FileTree?validExtensionArray=' + validExtensions + '&filter='
        }, function (file, type) {
            if (type == 'file') {
                DisplayCurrentImage(file);
                selectedValue = file;
                selectedMultiImageValue = file;
                ResetMultiImageTextboxes();
            }
            else {
                $('#upload_folder').val(file);
                //makeAjaxUpload(file);
            }
        });
    }

    function initCurrentImage(pImageObjects) {
        if (pImageObjects.length > 0) {
            DisplayCurrentImage(pImageObjects[0].Img);
        }
    }

    // Split string, create ImageObjects with properties and add the objects to an Array
    function initImageObjects(propertyString) {
        var myImageObjects = new Array();
        var splittedProperties = propertyString.split('|');
        var propertiesLength = splittedProperties.length;
        if (propertiesLength >= 6) {
            var y = 0;
            for (var i = 7; i < propertiesLength + 6; i += 6) {
                var imgObj = new ImageObject(splittedProperties[i - 6], splittedProperties[i - 5], splittedProperties[i - 4], splittedProperties[i - 3], splittedProperties[i - 2], splittedProperties[i - 1], 'multiImage_' + y++);
                myImageObjects.push(imgObj);
            }
        }

        // Add sort index
        for (var i = 0; i < myImageObjects.length; i++) {
            myImageObjects[i].SortIndex = i;
        }

        return myImageObjects;
    }
}

function setMultiImageBtnSaveClick(imageObjects) {
    $('#multiImageSaveButton').unbind('click');
    $('#multiImageSaveButton').bind('click', imageObjects, function () {
        var content = ImageObjectsToString(imageObjects);
        content = $('#multi_image_slide_time').val() + '|' + content;
        var inputElement = getSelectedMultiImageItem();
        $(inputElement).val(content);
        if ($(inputElement)[0].defaultValue != content) {
            bHasUnsavedData = true;
            $(inputElement).addClass('isDirtyData');
            $('#savemessagetext').html(getTranslation("CATMAN_LABEL_UNSAVED_DATA", 'Unsaved changes')).css({ color: 'Red' });
        } else {
            try { $(inputElement).removeClass('isDirtyData'); } catch (er) { }
            if ($('.isDirtyData').length == 0) {
                bHasUnsavedData = false;
                $('#savemessagetext').html("");
            }
        }
        imageObjects = null;
        $('#multiImagePopupModal').dialog('close');
    });
}

function setMultiImageBtnCancelClick() {
    $('#multiImageCancelButton').click(function () {
        imageObjects = null;
        $('#multiImagePopupModal').dialog('close');
    });
}

function setBtnMultiSelectClick(imageObjects) {
    // Must have access to the Array of Image Objects here
    // Add
    $('#multi_image_btn_add').unbind();
    $('#multi_image_btn_add').bind('click', function () {
        if (selectedMultiImageValue != '') {
            imgObj = new ImageObject(selectedMultiImageValue, $('#multi_image_alt').val(), $('#multi_image_target').val(), $('#multi_image_text').val(), $('#multi_image_href').val(), $('#multi_image_time').val(), 'multiImage_' + imageObjects.length);
            imageObjects.push(imgObj);
            InsertPics(imageObjects, rootUrl);
            imgObj = null;
        }
    });
    // Update
    $('#multi_image_btn_update').unbind();
    $('#multi_image_btn_update').bind('click', function () {
        imgObj = findImageObject(imageObjects, $('#multi_image_selected_id').val());
        //alert(imgObj);
        if (imgObj) {
            imgObj.Alt = $('#multi_image_alt').val();
            imgObj.Target = $('#multi_image_target').val();
            imgObj.Text = $('#multi_image_text').val();
            imgObj.Link = $('#multi_image_href').val();
            imgObj.Time = $('#multi_image_time').val();
        }
    });
    // Remove
    $('#multi_image_btn_remove').unbind();
    $('#multi_image_btn_remove').bind('click', function () {
        imgObj = findImageObject(imageObjects, $('#multi_image_selected_id').val());
        if (imgObj) {
            imageObjects = $.grep(imageObjects, function (item) {
                return item.Id != imgObj.Id;
            });
            //alert(imageObjects.length);
            InsertPics(imageObjects, rootUrl);
            $('#hata').html('');
            setMultiImageBtnSaveClick(imageObjects);
        }
    });
}

function findImageObject(imageObjects, imageId) {
    var result = $.grep(imageObjects, function (item) {
        return item.Id == imageId;
    });
    if (result.length > 0) {
        return result[0];
    }
    return null;
};

function InsertPics(imageObjects, rootUrl) {
    $('#picPop').remove();
    $('#multifileuploadcontent').append('<div id="picPop"><div id="picPopArea" \></div>');
    var thePathUrl = rootUrl;
    if (virtualUrl != null) {
        thePathUrl = virtualUrl;
    }
    // for each image object in array spit out html and "insert" image in container
    var htmlString = '<table><tr id="multiImageSortable">';
    $(imageObjects).each(function () {
        htmlString += '<td><img class="multiImageObject" id="' + this.Id + '" src="' + thePathUrl + this.Img + '" sortindex="' + this.SortIndex + '" height="50"/>&nbsp;</td>';
        htmlString = htmlString.replace(/\/\//g, '\/');
    });
    htmlString += '</tr></table>';

    $('#picPopArea').html(htmlString);

    $('.multiImageObject').click(function () {
        var id = $(this).attr('id');
        var imageObject = findImageObject(id);
        if (imageObject != null) {
            DisplayCurrentImage(imageObject.Img);
            FillMultiImageTextboxes(imageObject);
            $('#multi_image_selected_id').val(imageObject.Id);
        }
    });

    // inner function
    function findImageObject(imageId) {
        var result = $.grep(imageObjects, function (item) {
            return item.Id == imageId;
        });
        if (result.length > 0) {
            return result[0];
        }
        return null;
    };

    $('#multiImageSortable').sortable({ axis: 'x', placeholder: 'multi_image_placeholder', delay: 200 });

    $("#multiImageSortable").on("sortstop", function (event, ui) {
        // Change imageObjects sort indexes according to the new sortorder which we read from the sortable container
        var sortIndexes = $("#multiImageSortable img.multiImageObject").map(function () {
            return $(this).attr("sortindex");
        }).get();
        for (var i = 0; i < sortIndexes.length; i++) {
            imageObjects[sortIndexes[i]].SortIndex = i;
        }
    });
}

function undefined2EmtyString(anObj, property) {
    return anObj == undefined ? '' : anObj[property];
}

function FillMultiImageTextboxes(imgObject) {
    $('#multi_image_alt').val(undefined2EmtyString(imgObject, 'Alt'));
    $('#multi_image_target').val(undefined2EmtyString(imgObject, 'Target'));
    $('#multi_image_text').val(undefined2EmtyString(imgObject, 'Text'));
    $('#multi_image_href').val(undefined2EmtyString(imgObject, 'Link'));
    $('#multi_image_time').val(undefined2EmtyString(imgObject, 'Time'));
}

function ResetMultiImageTextboxes() {
    $('#multi_image_alt').val('');
    $('#multi_image_target').val('');
    $('#multi_image_text').val('');
    $('#multi_image_href').val('');
}

function DisplayCurrentImage(imagePath) {
    var thePathUrl = rootUrl;
    if (virtualUrl != null) {
        thePathUrl = virtualUrl;
    }
    imagePath = thePathUrl + imagePath;
    imagePath = imagePath.replace(/\/\//g, '\/');

    $("#hata").html("<img src='" + imagePath + "?nfimg=" + new Date().getTime() + "' style='height:68px'/>");
}

/* End Multi Image Popup */

// ** AjaxUpload **

(function () {
    var d = document, w = window; function get(element) { if (typeof element == "string") element = d.getElementById(element); return element; } function addEvent(el, type, fn) {
        if (w.addEventListener) { try { el.addEventListener(type, fn, false); } catch (e) { } } else if (w.attachEvent) {
            var f = function () { fn.call(el, w.event); }; try {
                el.attachEvent('on' + type, f);
            } catch (e) { }
        }
    } var toElement = function () {
        var div = d.createElement('div'); return function (html) { div.innerHTML = html; var el = div.childNodes[0]; div.removeChild(el); return el; };
    }(); function hasClass(ele, cls) { return ele.className.match(new RegExp('(\\s|^)' + cls + '(\\s|$)')); } function addClass(ele, cls) { if (!hasClass(ele, cls)) ele.className += " " + cls; } function removeClass(ele, cls) { var reg = new RegExp('(\\s|^)' + cls + '(\\s|$)'); ele.className = ele.className.replace(reg, ' '); } if (document.documentElement["getBoundingClientRect"]) {
        var getOffset = function (el) { var box = el.getBoundingClientRect(), doc = el.ownerDocument, body = doc.body, docElem = doc.documentElement, clientTop = docElem.clientTop || body.clientTop || 0, clientLeft = docElem.clientLeft || body.clientLeft || 0, zoom = 1; if (body.getBoundingClientRect) { var bound = body.getBoundingClientRect(); zoom = (bound.right - bound.left) / body.clientWidth; } if (zoom > 1) { clientTop = 0; clientLeft = 0; } var top = box.top / zoom + (window.pageYOffset || docElem && docElem.scrollTop / zoom || body.scrollTop / zoom) - clientTop, left = box.left / zoom + (window.pageXOffset || docElem && docElem.scrollLeft / zoom || body.scrollLeft / zoom) - clientLeft; return { top: top, left: left }; };
    } else {
        var getOffset = function (el) { if (w.jQuery) { return jQuery(el).offset(); } var top = 0, left = 0; do { top += el.offsetTop || 0; left += el.offsetLeft || 0; } while (el == el.offsetParent); return { left: left, top: top }; };
    } function getBox(el) { var left, right, top, bottom; var offset = getOffset(el); left = offset.left; top = offset.top; right = left + el.offsetWidth; bottom = top + el.offsetHeight; return { left: left, right: right, top: top, bottom: bottom }; }
    function getMouseCoords(e) { if (!e.pageX && e.clientX) { var zoom = 1; var body = document.body; if (body.getBoundingClientRect) { var bound = body.getBoundingClientRect(); zoom = (bound.right - bound.left) / body.clientWidth; } return { x: e.clientX / zoom + d.body.scrollLeft + d.documentElement.scrollLeft, y: e.clientY / zoom + d.body.scrollTop + d.documentElement.scrollTop }; } return { x: e.pageX, y: e.pageY }; } var getUID = function () {
        var id = 0; return function () { return 'ValumsAjaxUpload' + id++; };
    }();
    function fileFromPath(file) {
        return file.replace(/.*(\/|\\)/, "");
    }
    function getExt(file) { return (/[.]/.exec(file)) ? /[^.]+$/.exec(file.toLowerCase()) : ''; }
    var getXhr = function () {
        var xhr;
        return function () {
            if (xhr) return xhr;
            if (typeof XMLHttpRequest !== 'undefined') {
                xhr = new XMLHttpRequest();
            }
            else {
                var v = ["Microsoft.XmlHttp", "MSXML2.XmlHttp.5.0", "MSXML2.XmlHttp.4.0", "MSXML2.XmlHttp.3.0", "MSXML2.XmlHttp.2.0"];
                for (var i = 0; i < v.length; i++) {
                    try {
                        xhr = new ActiveXObject(v[i]);
                        break;
                    } catch (e) { }
                }
            }
            return xhr;
        };
    }();
    Ajax_upload = AjaxUpload = function (button, options) {
        if (button.jquery) {
            button = button[0];
        } else if (typeof button == "string" && /^#.*/.test(button)) {
            button = button.slice(1);
        }
        button = get(button);
        this._input = null;
        this._button = button;
        this._disabled = false;
        this._submitting = false;
        this._justClicked = false;
        this._parentDialog = d.body; if (window.jQuery && jQuery.ui && jQuery.ui.dialog) { var parentDialog = jQuery(this._button).parents('.ui-dialog'); if (parentDialog.length) { this._parentDialog = parentDialog[0]; } } this._settings = { action: 'upload.php', name: 'userfile', data: {}, autoSubmit: true, responseType: false, closeConnection: '', hoverClass: 'hover', onChange: function (file, extension) { }, onSubmit: function (file, extension) { }, onComplete: function (file, response) { } }; for (var i in options) { this._settings[i] = options[i]; } this._createInput(); this._rerouteClicks();
    };
    AjaxUpload.prototype = {
        setData: function (data) {
            this._settings.data = data;
        },
        disable: function () {
            this._disabled = true;
        },
        enable: function () {
            this._disabled = false;
        },
        destroy: function () {
            if (this._input) {
                if (this._input.parentNode) {
                    this._input.parentNode.removeChild(this._input);
                }
                this._input = null;
            }
        },
        _createInput: function () {
            var self = this;
            var input = d.createElement("input");
            input.setAttribute('type', 'file');
            input.setAttribute('name', this._settings.name);
            input.setAttribute('multiple', 'multiple');
            var styles = { 'position': 'absolute', 'margin': '-5px 0 0 -175px', 'padding': 0, 'width': '220px', 'height': '30px', 'fontSize': '14px', 'opacity': 0, 'cursor': 'pointer', 'display': 'none', 'zIndex': 2147483583 };
            for (var i in styles) {
                input.style[i] = styles[i];
            }
            if (!(input.style.opacity === "0")) { input.style.filter = "alpha(opacity=0)"; }
            this._parentDialog.appendChild(input);
            addEvent(input, 'change', function () {
                var file = fileFromPath(this.value);
                if (self._settings.onChange.call(self, file, getExt(file)) == false) {
                    return;
                }
                if (self._settings.autoSubmit) {
                    self.submit();
                }
            });
            addEvent(input, 'click', function () {
                self._justClicked = true;
                setTimeout(function () {
                    self._justClicked = false;
                }, 2500);
            });
            this._input = input;
        },
        _rerouteClicks: function () {
            var self = this;
            var box, dialogOffset = { top: 0, left: 0 }, over = false;
            addEvent(self._button, 'mouseover', function (e) {
                if (!self._input || over) return;
                over = true;
                box = getBox(self._button);
                if (self._parentDialog != d.body) {
                    dialogOffset = getOffset(self._parentDialog);
                }
            });
            addEvent(document, 'mousemove', function (e) { var input = self._input; if (!input || !over) return; if (self._disabled) { removeClass(self._button, self._settings.hoverClass); input.style.display = 'none'; return; } var c = getMouseCoords(e); if ((c.x >= box.left) && (c.x <= box.right) && (c.y >= box.top) && (c.y <= box.bottom)) { input.style.top = c.y - dialogOffset.top + 'px'; input.style.left = c.x - dialogOffset.left + 'px'; input.style.display = 'block'; addClass(self._button, self._settings.hoverClass); } else { over = false; var check = setInterval(function () { if (self._justClicked) { return; } if (!over) { input.style.display = 'none'; } clearInterval(check); }, 25); removeClass(self._button, self._settings.hoverClass); } });
        },
        _createIframe: function () {
            var id = getUID();
            var iframe = toElement('<iframe src="javascript:false;" name="' + id + '" />');
            iframe.id = id;
            iframe.style.display = 'none';
            d.body.appendChild(iframe);
            return iframe;
        },
        submit: function () {
            var self = this, settings = this._settings;
            if (this._input.value === '') {
                return;
            }
            var file = fileFromPath(this._input.value);
            if (!(settings.onSubmit.call(this, file, getExt(file)) == false)) {
                var iframe = this._createIframe();
                var form = this._createForm(iframe);
                form.appendChild(this._input);
                if (settings.closeConnection && /AppleWebKit|MSIE/.test(navigator.userAgent)) {
                    var xhr = getXhr();
                    xhr.open('GET', settings.closeConnection, false);
                    xhr.send('');
                }
                form.submit();
                d.body.removeChild(form); form = null; this._input = null; this._createInput(); var toDeleteFlag = false; addEvent(iframe, 'load', function (e) { if (iframe.src == "javascript:'%3Chtml%3E%3C/html%3E';" || iframe.src == "javascript:'<html></html>';") { if (toDeleteFlag) { setTimeout(function () { d.body.removeChild(iframe); }, 0); } return; } var doc = iframe.contentDocument ? iframe.contentDocument : frames[iframe.id].document; if (doc.readyState && doc.readyState != 'complete') { return; } if (doc.body && doc.body.innerHTML == "false") { return; } var response; if (doc.XMLDocument) { response = doc.XMLDocument; } else if (doc.body) { response = doc.body.innerHTML; if (settings.responseType && settings.responseType.toLowerCase() == 'json') { if (doc.body.firstChild && doc.body.firstChild.nodeName.toUpperCase() == 'PRE') { response = doc.body.firstChild.firstChild.nodeValue; } if (response) { response = window["eval"]("(" + response + ")"); } else { response = {}; } } } else { var response = doc; } settings.onComplete.call(self, file, response); toDeleteFlag = true; iframe.src = "javascript:'<html></html>';"; });
            } else { d.body.removeChild(this._input); this._input = null; this._createInput(); }
        }, _createForm: function (iframe) { var settings = this._settings; var form = toElement('<form method="post" enctype="multipart/form-data"></form>'); form.style.display = 'none'; form.action = settings.action; form.target = iframe.name; d.body.appendChild(form); for (var prop in settings.data) { var el = d.createElement("input"); el.type = 'hidden'; el.name = prop; el.value = settings.data[prop]; form.appendChild(el); } return form; }
    };
})();

// ** End AjaxUpload **

// ** jQuery.cookie **

jQuery.cookie = function (name, value, options) { if (typeof value != 'undefined') { options = options || {}; if (value === null) { value = ''; options.expires = -1; } var expires = ''; if (options.expires && (typeof options.expires == 'number' || options.expires.toUTCString)) { var date; if (typeof options.expires == 'number') { date = new Date(); date.setTime(date.getTime() + (options.expires * 24 * 60 * 60 * 1000)); } else { date = options.expires; } expires = '; expires=' + date.toUTCString(); } var path = options.path ? '; path=' + (options.path) : ''; var domain = options.domain ? '; domain=' + (options.domain) : ''; var secure = options.secure ? '; secure' : ''; document.cookie = [name, '=', encodeURIComponent(value), expires, path, domain, secure].join(''); } else { var cookieValue = null; if (document.cookie && document.cookie != '') { var cookies = document.cookie.split(';'); for (var i = 0; i < cookies.length; i++) { var cookie = jQuery.trim(cookies[i]); if (cookie.substring(0, name.length + 1) == (name + '=')) { cookieValue = decodeURIComponent(cookie.substring(name.length + 1)); break; } } } return cookieValue; } };; (function (d) {
    var k = d.scrollTo = function (a, i, e) { d(window).scrollTo(a, i, e); }; k.defaults = { axis: 'xy', duration: parseFloat(d.fn.jquery) >= 1.3 ? 0 : 1 }; k.window = function (a) { return d(window)._scrollable(); }; d.fn._scrollable = function () {
        return this.map(function () {
            var a = this, i = !a.nodeName || d.inArray(a.nodeName.toLowerCase(), ['iframe', '#document', 'html', 'body']) != -1; if (!i) return a; var e = (a.contentWindow || a).document || a.ownerDocument || a; return d.browser.safari || e.compatMode == 'BackCompat' ? e.body : e.documentElement;
        });
    }; d.fn.scrollTo = function (n, j, b) {
        if (typeof j == 'object') {
            b = j; j = 0;
        } if (typeof b == 'function') b = { onAfter: b }; if (n == 'max') n = 9e9; b = d.extend({}, k.defaults, b); j = j || b.speed || b.duration; b.queue = b.queue && b.axis.length > 1; if (b.queue) j /= 2; b.offset = p(b.offset); b.over = p(b.over); return this._scrollable().each(function () {
            var q = this, r = d(q), f = n, s, g = {}, u = r.is('html,body'); switch (typeof f) {
                case 'number': case 'string': if (/^([+-]=)?\d+(\.\d+)?(px|%)?$/.test(f)) {
                    f = p(f); break;
                } f = d(f, this); case 'object': if (f.is || f.style) s = (f = d(f)).offset();
            } d.each(b.axis.split(''), function (a, i) {
                var e = i == 'x' ? 'Left' : 'Top', h = e.toLowerCase(), c = 'scroll' + e, l = q[c], m = k.max(q, i); if (s) {
                    g[c] = s[h] + (u ? 0 : l - r.offset()[h]); if (b.margin) {
                        g[c] -= parseInt(f.css('margin' + e)) || 0; g[c] -= parseInt(f.css('border' + e + 'Width')) || 0;
                    } g[c] += b.offset[h] || 0; if (b.over[h]) g[c] += f[i == 'x' ? 'width' : 'height']() * b.over[h];
                } else {
                    var o = f[h]; g[c] = o.slice && o.slice(-1) == '%' ? parseFloat(o) / 100 * m : o;
                } if (/^\d+$/.test(g[c])) g[c] = g[c] <= 0 ? 0 : Math.min(g[c], m); if (!a && b.queue) {
                    if (l != g[c]) t(b.onAfterFirst); delete g[c];
                }
            }); t(b.onAfter); function t(a) {
                r.animate(g, j, b.easing, a && function () { a.call(this, n, b); });
            }
        }).end();
    }; k.max = function (a, i) {
        var e = i == 'x' ? 'Width' : 'Height', h = 'scroll' + e; if (!d(a).is('html,body')) return a[h] - d(a)[e.toLowerCase()](); var c = 'client' + e, l = a.ownerDocument.documentElement, m = a.ownerDocument.body; return Math.max(l[h], m[h]) - Math.min(l[c], m[c]);
    }; function p(a) {
        return typeof a == 'object' ? a : { top: a, left: a };
    }
})(jQuery);

// ** End jQuery.cookie **

// Draw Flot Graph -- Sales -- Business Center
function drawGraph(days) {
    var initDate = Date.now();
    initDate -= 1000 * 60 * 60 * 24 * days;
    initDate = new Date(initDate);
    var tomorrow = new Date();
    tomorrow.setDate(tomorrow.getDate() + 1);
    var currencyGuid;

    var options = {
        xaxis: {
            mode: "time",
            minTickSize: [2, "day"],
            min: initDate.getTime(),
            max: tomorrow.getTime(),
            timeformat: "%d/%m/%Y"
        },
        yaxis: {
            tickFormatter: function (value) {
                return value + " " + currencyGuid;
            }
        },
        lines: {
            show: true,
            fill: true
        },
        grid: {
            hoverable: true
        },
        series: { label: "Sales" }
    };

    $("<div id='tooltip'></div>").css({
        position: "absolute",
        maxWidth: "200px",
        "color": "#fff",
        display: "none",
        padding: "3px 8px",
        "background-color": "#000",
        "border-radius": "4px",
        opacity: 0.80
    }).appendTo("body");

    $("#salesChart").bind("plothover", function (event, pos, item) {
        if (item) {
            var x = item.datapoint[0],
                y = item.datapoint[1].toFixed(2);

            $("#tooltip").html(y + " " + currencyGuid)
                .css({ top: item.pageY + 5, left: item.pageX + 5 })
                .fadeIn(200);
        } else {
            $("#tooltip").hide();
        }
    });

    var dataList = [];

    $.ajax({
        type: "POST",
        dataType: "json",
        url: rootUrl + 'cms/BusinessCenter/GetLatestSalesInXDays?days=365',
        data: "{}",
        success: function (data) {
            $.map(data, function (item) {
                var list = [];
                currencyGuid = item.Currency;
                list.push(item.DateTimeJs);
                list.push(item.Total);
                dataList.push(list);
            });
            $.plot($("#salesChart"), [dataList], options);
        }
    });
}

function convertNETDateToString(data) {
    if (data == null) return null;
    if (data instanceof Date) return data;
    var r = /\/Date\((-?[0-9]+)\)\//i;
    var matches = data.match(r);
    if (matches.length == 2) {
        if (matches[1] == -62135596800000) {
            return "";
        }
        return new Date(parseInt(matches[1])).toLocaleString();
    }
    else {
        return data;
    }
}

function applyPeriod() {
    var isValid = true;
    if ($("input[name=startDate]").val() == "") {
        $("input[name=startDate]").parent().addClass("has-error");
        $("input[name=startDate]").tooltip("show");
        isValid = false;
    }
    if ($("input[name=endDate]").val() == "") {
        $("input[name=endDate]").parent().addClass("has-error");
        $("input[name=endDate]").tooltip("show");
        isValid = false;
    }
    if (isValid) {
        var startDate = $("input[name=startDate]").val();
        var endDate = $("input[name=endDate]").val();
        var period = $("select[name=period]").val();
        ajaxCallToSelectPeriod(startDate, endDate, period);
    }
}

function ajaxCallToSelectPeriod(startDate, endDate, period) {
    $.ajax({
        type: "POST",
        async: false,
        url: rootUrl + "cms/BusinessCenter/SelectPeriod",
        data: {
            'startDate': startDate,
            'endDate': endDate,
            'period': period
        },
        success: function (data) {
            $("input[name=startDate]").parent().removeClass("has-error");
            $("input[name=endDate]").parent().removeClass("has-error");
            $(".startDateField").each(function (i) {
                var date = data[i].EntryDate;
                var theEndDate = data[i].EndDate;
                var re = /-?\d+/;
                var m = re.exec(date);
                var m2 = re.exec(theEndDate);
                var d = new Date(parseInt(m[0]));
                var d2 = new Date(parseInt(m2[0]));
                $(this).html($.datepicker.formatDate("dd/mm/yy", d) + "<br />" + $.datepicker.formatDate("dd/mm/yy", d2));
            });
            $(".totalLoginsField").each(function (i) {
                $(this).html(data[i].TotalLogins);
            });
            $(".newUsersField").each(function (i) {
                $(this).html(data[i].NewUsers);
            });
            $(".totalRevenueNoVATField").each(function (i) {
                $(this).html(data[i].TotalRevenueWithoutVATString);
            });
            $(".totalOrdersField").each(function (i) {
                $(this).html(data[i].TotalOrders);
            });
            $(".totalItemsField").each(function (i) {
                $(this).html(data[i].TotalItems);
            });
            $(".avgItemPrizeField").each(function (i) {
                $(this).html(data[i].AvgItemPrizeString);
            });
            $(".avgOrderSizeField").each(function (i) {
                $(this).html(data[i].AvgOrderSizeString);
            });

            $(".conversionRateField").each(function (i) {
                if (data[i].TotalLogins == 0)
                    $(this).html("N/A");
                else
                    $(this).html((data[i].TotalOrders / data[i].TotalLogins * 100).toFixed(2) + '%');
            });

            $("td.kpi-percentage").each(function () {
                var firstPeriod = $(this).siblings(".firstPeriod").html();
                var secondPeriod = $(this).siblings(".secondPeriod").html();
                firstPeriod = parseFloat(firstPeriod.replace(/[^\d\.]/g, '')); // delete everything but numbers
                secondPeriod = parseFloat(secondPeriod.replace(/[^\d\.]/g, '')); // delete everything but numbers

                var result;
                if (secondPeriod == 0 || isNaN(secondPeriod)) {
                    result = "N/A";
                    $(this).html(result);
                } else {
                    result = (firstPeriod * 100 / secondPeriod - 100).toFixed(2);
                    $(this).html(result + '%');
                }
                if (result >= 0) {
                    $(this).removeClass("negative-value").addClass("positive-value");
                } else {
                    $(this).removeClass("positive-value").addClass("negative-value");
                }
            });

        }
    });
}

function getTodaysDate() {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1;
    var yyyy = today.getFullYear();

    if (dd < 10) {
        dd = '0' + dd;
    }

    if (mm < 10) {
        mm = '0' + mm;
    }

    today = dd + '/' + mm + '/' + yyyy;

    return today;
}

function getOneMonthDate() {
    var theDate = new Date();
    theDate.setMonth(theDate.getMonth() - 1);
    var dd = theDate.getDate();
    var mm = theDate.getMonth() + 1;
    var yyyy = theDate.getFullYear();

    if (dd < 10) {
        dd = '0' + dd;
    }

    if (mm < 10) {
        mm = '0' + mm;
    }

    theDate = dd + '/' + mm + '/' + yyyy;

    return theDate;
}