﻿<%@ Page Language="C#" %>

<%@ Import Namespace="EISCS.ExpandITFramework.Util" %>

<!DOCTYPE html>

<html>
<head>
    <title>The site is currently being updated</title>
    <script language="JavaScript">
	<!--
    function myReLoad() {
        // Timeout of 10 seconds before refresh script is activated.
        setTimeout("submitpage()", 10 * 1000);
    }

    function submitpage() {
        // This code will submit the the the form "formClosed".
        formClosed.submit();
    }
    //-->
    </script>
    <style>
        <!--
        TD
        {
            FONT-SIZE: 10pt;
            COLOR: #003366;
            FONT-FAMILY: Verdana;
        }

        H1
        {
            FONT-WEIGHT: bold;
            FONT-SIZE: 20pt;
        }

        .MainWindow
        {
            BORDER-RIGHT: #999999 1px;
            BORDER-TOP: #999999 1px solid;
            BACKGROUND-IMAGE: url(images/watermark.gif);
            BORDER-LEFT: #999999 1px;
            BORDER-BOTTOM: #999999 1px solid;
            BACKGROUND-REPEAT: repeat;
        }
        -->
    </style>

</head>
<body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" onload="myReLoad()">
    <table cellspacing="0" cellpadding="0" width="100%" height="100%">
        <tr>
            <td class="MainWindow" valign="TOP" align="LEFT" height="100%">
                <h1>The ExpandIT Internet Shop is currently being updated</h1>
                This Internet Shop is currently being updated. It will be back online in a matter of seconds.
				<br />
                <br />
                This page will automatically reload. You will be transferred back to where you came from once the Internet Shop is back online.
					<form action="<%= AppUtil.GetVirtualRoot()%>" method="POST" id="form1" name="formClosed">
                        If the page are not reloaded automatically, please reload the page manually by pressing this button.<br />
                        <br />
                        <input type="submit" value="Reload page" name="Reload page">
                    </form>
                <br />
                <br />
                <br />
                Kind regards,<br />
                ExpandIT Solutions a/s

            </td>
        </tr>
    </table>
</body>
</html>

