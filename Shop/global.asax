﻿<%@ Application Language="C#" %>
<%@ Import Namespace="System.Activities.Statements" %>
<%@ Import Namespace="System.Globalization" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Net" %>
<%@ Import Namespace="System.Threading" %>
<%@ Import Namespace="System.Web.Http" %>
<%@ Import Namespace="CmsPublic.ModelLayer.Models" %>
<%@ Import Namespace="EISCS.CMS.Services.PropertyServices" %>
<%@ Import Namespace="EISCS.ExpandITFramework.Infrastructure" %>
<%@ Import Namespace="EISCS.ExpandITFramework.Util" %>
<%@ Import Namespace="EISCS.Routing.Providers" %>
<%@ Import Namespace="EISCS.Shop.DO.Interface" %>
<%@ Import Namespace="EISCS.Shop.Users" %>
<%@ Import Namespace="ExpandIT.Logging" %>
<%@ Import Namespace="Hangfire" %>
<%@ Import Namespace="StackExchange.Profiling" %>
<%@ Import Namespace="WebServices" %>
<script RunAt="server">

    private static bool _appDomainIsShuttingDown = false;
    private static bool _useMiniProfiler = false;
    private BackgroundJobServer _backgroundJobServer;
    private static bool isFirstRequest = true;

    private void Application_Start(object sender, EventArgs e)
    {
        UnityBootstrapper.Initialise();
        Register(System.Web.Http.GlobalConfiguration.Configuration);
        // Setup "shop" MVC routing and add a catch all route for all routing.
        RegisterRoutes(RouteTable.Routes);

        ViewEngines.Engines.Clear();
        ViewEngines.Engines.Add(new RazorEmbeddedResourceViewEngine());
        //Trigger the WebformsRouting
        RouteManager.Route.Register();
        //Setup the cache dependencies

        if (!DataAccess.IsLocalMode)
        {
            Application["IsLocalMode"] = false;
            bool changed = ConfigurationHelper.SyncWebConfigSettingForRemote();
            if (changed)
            {
                HttpRuntime.UnloadAppDomain();
                _appDomainIsShuttingDown = true;
                FileLogger.log("web.config has been changed - Restarting IIS - new stateserver: " + ConfigurationManager.AppSettings["RemoteASPSessionStateServerConnectionString"]);
            }
        }
        else
        {
            Application["IsLocalMode"] = true;
        }

        Application.Lock();

        CacheManager.InitializeCachedTables();

        // Monitor schema changes on a typical extract table. This is done so we can purge the type mapping in dapper.
        object dbSchemaCacheMonitor = new object();
        CacheManager.CacheSetAggregated("dbSchemaCacheMonitor", dbSchemaCacheMonitor, new[] { "CurrencyTable" }, CacheRemovedCallback);

        Application["Closed"] = false;
        Application.UnLock();

        AutoMapperBootstrapper.RegisterTypes();
        _useMiniProfiler = ExpanditLib2.CBoolEx(ConfigurationManager.AppSettings["ACTIVATE_MINIPROFILER"]);
    }

    public void CacheRemovedCallback(string key, object value, CacheItemRemovedReason reason)
    {
        if (reason.Equals(CacheItemRemovedReason.DependencyChanged))
        {
            // Purge the type mapping
            Dapper.SqlMapper.PurgeQueryCache();
        }
    }

    public static void Register(HttpConfiguration config)
    {
        config.Routes.MapHttpRoute(
        name: "ApiById",
        routeTemplate: "api/{controller}/{id}",
        defaults: new { id = System.Web.Http.RouteParameter.Optional },
        constraints: new { id = @"^[0-9]+$" }
    );

        config.Routes.MapHttpRoute(
            name: "ApiByName",
            routeTemplate: "api/{controller}/{action}/{name}",
            defaults: null,
            constraints: new { name = @"^[a-z]+$" }
        );

        config.Routes.MapHttpRoute(
            name: "ApiByAction",
            routeTemplate: "api/{controller}/{action}",
            defaults: new { action = "Get" }
        );
    }

    private static void RegisterRoutes(RouteCollection routes)
    {
        routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

        routes.MapRoute("Accounts",
            "Accounts/{action}/{id}",
            new { controller = "Accounts", action = "Index", id = UrlParameter.Optional });
        routes.MapRoute("CMS",
            "{path}/{controller}/{action}/{id}",
            new { controller = "BusinessCenter", action = "Index", id = "" }, new { path = "CMS" });
        routes.MapRoute("CentralManagement",
                            "CentralManagement/{action}/{id}",
                            new { controller = "CentralManagement", action = "IndexM", id = "" });
        routes.MapRoute(
            "Default",
            "{controller}/{action}/{id}",
            new { controller = "DefaultPath", action = "Index", id = ConfigurationManager.AppSettings["FRONTPAGE_GROUP"] });
        routes.MapRoute(
            "Default2",
            "{controller}/{id}",
            new { action = "Index", id = UrlParameter.Optional });
        // Web Forms default
        routes.MapPageRoute(
            "WebFormDefault",
            "",
            "~/default.aspx"
        );
        // Add a 'Catch All' route to make the CustomErrors section defined in web.config to work with Webforms Routing. 
        // Does not look nice but is working.
        routes.MapPageRoute("404", "{*url}", "~/DummyPathToMakeWebConfigCustomErrorsSectionToWork.aspx");
    }

    private void Application_End(object sender, EventArgs e)
    {
        //  Code that runs on application shutdown
        _backgroundJobServer.Dispose();
    }

    private void Application_Error(object sender, EventArgs e)
    {
        // Code that runs when an unhandled error occurs
    }

    private void Session_Start(object sender, EventArgs e)
    {
        // Code that runs when a new session is started
    }

    private void Session_End(object sender, EventArgs e)
    {
        // Code that runs when a session ends. 
        // Note: The Session_End event is raised only when the sessionstate mode
        // is set to InProc in the Web.config file. If session mode is set to StateServer 
        // or SQLServer, the event is not raised.
    }

    private void Application_BeginRequest(object sender, EventArgs e)
    {
        if (_useMiniProfiler)
        {
            if (Request.IsLocal)
            {
                MiniProfiler.Start();
                MiniProfiler.Settings.MaxJsonResponseSize = 2147483647;
            }
        }

        if (isFirstRequest)
        {
            try
            {
                Hangfire.GlobalConfiguration.Configuration
                    .UseSqlServerStorage("ExpandITConnectionString");
                _backgroundJobServer = new BackgroundJobServer();
                // Set initial Stale Cart Reminder Mail task
                var client = DependencyResolver.Current.GetService<IMailSendClient>();
                client.InitializeMailSendRecurringTask();
                if (!RouteManager.IsInitialized)
                {
                    RouteManager.Initialize();
                }
                isFirstRequest = false;
            }
            catch (Exception)
            {
                isFirstRequest = true;
            }
        }

        // Enable caching, but expire data immediately, forcing the browser to request, only to get a 304 back for static content
        Response.Cache.SetExpires(DateTime.UtcNow.AddHours(-1));
        if (_appDomainIsShuttingDown)
        {
            Response.Redirect(Request.Url.ToString());
        }
    }

    protected void Application_EndRequest()
    {
        MiniProfiler.Stop();
    }

    private void Application_AcquireRequestState(object sender, EventArgs e)
    {
        string languageGuid = ConfigurationManager.AppSettings["LANGUAGE_DEFAULT"];

        var user = DependencyResolver.Current.GetService<IExpanditUserService>();
        if (!string.IsNullOrWhiteSpace(user.LanguageGuid))
        {
            languageGuid = user.LanguageGuid;
        }

        var cultureInfo = new CultureInfo(languageGuid);
        Thread.CurrentThread.CurrentCulture = cultureInfo; // beware - need da-DK not da
        Thread.CurrentThread.CurrentUICulture = cultureInfo;
    }

    protected void Application_AuthenticateRequest(object sender, EventArgs e)
    {

    }

</script>
