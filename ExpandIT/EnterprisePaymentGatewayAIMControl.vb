Imports Microsoft.VisualBasic
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib
Imports System.Configuration.ConfigurationManager

Namespace ExpandIT.SimpleUIControls
    'AM2010041301 - ENTERPRISE PAYMENT GATEWAY - Start

    ''' <summary>
    ''' This is the new EnterprisePaymentGatewayAIMControl that inherits from EnterprisePaymentGatewayControl
    ''' The extended functionality involves three new properties that handles creditcard
    ''' information.
    ''' The class overrides it's baseclass CreateChildControls() method, and overloads the baseclass's 
    ''' buttonMode method.
    ''' The purpouses of these overrides/overloads is to create a slightly different user interface,
    ''' providing three textboxes to collect creditcard information from the customer.                    
    ''' </summary>
    ''' <remarks></remarks>
    Public Class EnterprisePaymentGatewayAIMControl
        Inherits EnterprisePaymentGatewayControl

        'Declaring the variables needed to get the data needed.
        ' Private variables
        Private tbxCCN As TextBox
        Protected validatorCCN As RequiredFieldValidator
        Private tbxExpDate As TextBox
        Protected validatorExpDate As RequiredFieldValidator
        Private tbxCCV As TextBox
        Protected validatorCCV As RequiredFieldValidator
        Private tbxFName As TextBox
        Protected validatorFName As RequiredFieldValidator
        Private tbxLName As TextBox
        Protected validatorLName As RequiredFieldValidator
        Private tbxAddress1 As TextBox
        Protected validatorAddress1 As RequiredFieldValidator
        Private tbxAddress2 As TextBox
        Private tbxCity As TextBox
        Protected validatorCity As RequiredFieldValidator
        Private tbxState As TextBox
        Protected validatorState As RequiredFieldValidator
        Private tbxPhone As TextBox
        Private tbxEmail As TextBox
        Protected validatorEmail As RequiredFieldValidator
        Private tbxZipCode As TextBox
        Protected validatorZipCode As RequiredFieldValidator
        Private cbxAddress As CheckBox
        Private cbxZipCode As CheckBox
        'AM2010091001 - ONLINE PAYMENTS - Start
        Private tbxAmount As TextBox
        Protected validatorAmount As RequiredFieldValidator
        Private tbxAmountStartValue As String
        Private onlinePayment As Boolean
        'AM2010091001 - ONLINE PAYMENTS - End

        '<!-- JA2011042101 - EEPG USE EXISTING CREDIT CARDS - Start -->
        Private tbxCC As TextBox
        '<!-- JA2011042101 - EEPG USE EXISTING CREDIT CARDS - End -->


        Dim colspan As Integer = 0

        ''' <summary>
        ''' Get the creditcard number
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property CCN() As String
            Get
                Return tbxCCN.Text
            End Get
        End Property

        ''' <summary>
        ''' Get the expiring date
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property EXPDATE() As String
            Get
                Return tbxExpDate.Text
            End Get
        End Property

        ''' <summary>
        ''' Retreive the CCV
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property CCV() As String
            Get
                Return CStrEx(tbxCCV.Text)
            End Get
        End Property

        ''' <summary>
        ''' Get the First Name
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property FName() As String
            Get
                Return tbxFName.Text
            End Get
        End Property

        ''' <summary>
        ''' Get the Last Name
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property LName() As String
            Get
                Return tbxLName.Text
            End Get
        End Property

        ''' <summary>
        ''' Get/Set the Address1
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property Address1() As String
            Get
                Return tbxAddress1.Text
            End Get
            Set(ByVal value As String)
                tbxAddress1.Text = value
            End Set
        End Property

        ''' <summary>
        ''' Get/Set the Address2
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property Address2() As String
            Get
                Return tbxAddress2.Text
            End Get
            Set(ByVal value As String)
                tbxAddress2.Text = value
            End Set
        End Property

        ''' <summary>
        ''' Get/Set the City
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property City() As String
            Get
                Return tbxCity.Text
            End Get
            Set(ByVal value As String)
                tbxCity.Text = value
            End Set
        End Property

        ''' <summary>
        ''' Get/Set the State
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property State() As String
            Get
                Return tbxState.Text
            End Get
            Set(ByVal value As String)
                tbxState.Text = value
            End Set
        End Property

        ''' <summary>
        ''' Get/Set the Phone
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property Phone() As String
            Get
                Return tbxPhone.Text
            End Get
            Set(ByVal value As String)
                tbxPhone.Text = value
            End Set
        End Property

        ''' <summary>
        ''' Get/Set the Email
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property Email() As String
            Get
                Return tbxEmail.Text
            End Get
            Set(ByVal value As String)
                tbxEmail.Text = value
            End Set
        End Property

        ''' <summary>
        ''' Get/Set the ZipCode
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property ZipCode() As String
            Get
                Return tbxZipCode.Text
            End Get
            Set(ByVal value As String)
                tbxZipCode.Text = value
            End Set
        End Property
        'AM2010091001 - ONLINE PAYMENTS - Start
        ''' <summary>
        ''' Get the Amount
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property Amount() As String
            Get
                Return tbxAmount.Text
            End Get
            Set(ByVal value As String)
                tbxAmountStartValue = value
            End Set
        End Property

        ''' <summary>
        ''' Online Payment
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property isOnlinePayment() As Boolean
            Get
                Return onlinePayment
            End Get
            Set(ByVal value As Boolean)
                onlinePayment = value
            End Set
        End Property
        'AM2010091001 - ONLINE PAYMENTS - End




        '<!-- JA2011042101 - EEPG USE EXISTING CREDIT CARDS - Start -->
        Public Property decryptData() As String
            Get
                Return tbxCC.Text
            End Get
            Set(ByVal value As String)
                tbxCC.Text = value
            End Set
        End Property
        '<!-- JA2011042101 - EEPG USE EXISTING CREDIT CARDS - End -->



        '''' <summary>
        '''' Get the cbxZipCode
        '''' </summary>
        '''' <value></value>
        '''' <returns></returns>
        '''' <remarks></remarks>
        'Public ReadOnly Property cbxZipCodeChecked() As Boolean
        '    Get
        '        Return cbxZipCode.Checked
        '    End Get
        'End Property

        '''' <summary>
        '''' Get the cbxAddress
        '''' </summary>
        '''' <value></value>
        '''' <returns></returns>
        '''' <remarks></remarks>
        'Public ReadOnly Property cbxAddressChecked() As Boolean
        '    Get
        '        Return cbxAddress.Checked
        '    End Get
        'End Property



        'CreateChilControls function
        Protected Overrides Sub CreateChildControls()

            Dim obj As New ObjectTagBuilder()
            If CBoolEx(AppSettings("PAYMENT_EEPG_REQUIRE_ADDRESS_INFO")) Or CBoolEx(AppSettings("PAYMENT_EEPG_REQUIRE_ZIP_CODE")) Then
                colspan = 2
            Else
                colspan = 1
            End If

            ' create the table
            Dim table As New Table()
            table.Width = TableWidth

            ' create the first row
            Dim tr As New TableRow()

            ' add a cell to the row
            Dim td As New TableCell()
            td.ColumnSpan = colspan
            tr.Cells.Add(td)
            table.Rows.Add(tr)

            ' create the second row
            tr = New TableRow()

            ' add a header cell to the row
            Dim th As New TableHeaderCell()
            th.ColumnSpan = colspan

            ' add a label to the header cell
            hdrText = New Label()
            hdrText.Text = HeaderText
            th.Controls.Add(hdrText)
            tr.Cells.Add(th)
            table.Rows.Add(tr)

            tr = New TableRow()
            td = New TableCell()
            td.ColumnSpan = colspan
            tr.Cells.Add(td)
            table.Rows.Add(tr)
            ' Show Error Text
            trError = New TableRow()
            td = New TableCell()
            td.ColumnSpan = colspan
            lblErrorText = New Label()
            lblErrorText.ForeColor = Drawing.Color.Red
            td.Controls.Add(lblErrorText)
            trError.Cells.Add(td)
            trError.Visible = False
            table.Rows.Add(trError)
            If RadioButtonMode Then
                radioMode(table)
            Else
                buttonMode(table)
            End If
            ' Add the table to the control's ControlCollection
            Me.Controls.Add(table)
        End Sub
        'Function that creates the table that gets displayed on the payment page for gathering the required information.
        Protected Overloads Sub buttonMode(ByRef table As Table)
            If colspan = 0 Then
                If CBoolEx(AppSettings("PAYMENT_EEPG_REQUIRE_ADDRESS_INFO")) Or CBoolEx(AppSettings("PAYMENT_EEPG_REQUIRE_ZIP_CODE")) Then
                    colspan = 2
                Else
                    colspan = 1
                End If
            End If

            ' First row
            Dim tr As New TableRow()
            Dim td As New TableCell()
            td.ColumnSpan = colspan
            LblText = New Label()
            LblText.Text = LabelText
            td.Controls.Add(LblText)
            tr.Cells.Add(td)
            table.Rows.Add(tr)

            ' Second row
            tr = New TableRow()
            td = New TableCell()
            td.ColumnSpan = colspan
            Dim txt As New LiteralControl()
            txt.Text = "&nbsp;"
            td.Controls.Add(txt)
            tr.Cells.Add(td)
            table.Rows.Add(tr)

            ' Third row
            tr = New TableRow()
            td = New TableCell()
            Dim CCTable As New Table()
            CCTable = buildCCTable()
            td.Controls.Add(CCTable)
            tr.Cells.Add(td)
            If colspan = 2 Then
                td = New TableCell()
                Dim BillToTable As New Table()
                BillToTable = buildBillToTable()
                td.Controls.Add(BillToTable)

                'Loading Div start
                'Dim BillToDiv As New HtmlGenericControl("DIV")
                'BillToDiv = buildWaitingDiv()
                'td.Controls.Add(BillToDiv)
                'Loading Div end

                tr.Cells.Add(td)
            End If
            table.Rows.Add(tr)

            tr = New TableRow()
            td = New TableCell()
            td.ColumnSpan = colspan
            td.Style.Add(System.Web.UI.HtmlTextWriterStyle.Width, "100%")
            td.Style.Add(System.Web.UI.HtmlTextWriterStyle.TextAlign, "right")

            clickBtn = New Button()
            clickBtn.Text = ButtonText
            clickBtn.ID = "cbtn"
            clickBtn.CssClass = "AddButton"
            AddHandler clickBtn.Click, AddressOf BtnAccept_Click
            clickBtn.Attributes.Add("onclick", "openLoadingDiv();")
            td.Controls.Add(clickBtn)
            tr.Cells.Add(td)
            table.Rows.Add(tr)
            tr = New TableRow()
            td = New TableCell()
            td.ColumnSpan = colspan
            td.Style.Add(System.Web.UI.HtmlTextWriterStyle.Width, "100%")
            td.Style.Add(System.Web.UI.HtmlTextWriterStyle.Height, "45px")
            td.Style.Add(System.Web.UI.HtmlTextWriterStyle.TextAlign, "right")


            Dim newDivLoading As New HtmlGenericControl("div")
            newDivLoading.ID = "LoadingBox"
            newDivLoading.Style.Add("position", "relative")
            newDivLoading.Style.Add("float", "right")
            newDivLoading.Style.Add("top", "-35px")
            newDivLoading.Style.Add("background-color", "white")
            newDivLoading.Style.Add("height", "45px")
            newDivLoading.Style.Add("width", "140px")
            newDivLoading.Style.Add("z-index", "10000000")
            newDivLoading.Style.Add("text-align", "center")
            newDivLoading.Style.Add("display", "none")

            Dim imgLoading As New Image()
            imgLoading.ImageUrl = "~/script/stars/loadRating.gif"

            newDivLoading.Controls.Add(imgLoading)

            td.Controls.Add(newDivLoading)
            tr.Cells.Add(td)
            table.Rows.Add(tr)

        End Sub

        'Public Function buildWaitingDiv()
        '    Dim BillToDiv As New HtmlGenericControl("DIV")
        '    BillToDiv.ID = "Loading"
        '    BillToDiv.Style.Add(System.Web.UI.HtmlTextWriterStyle.BackgroundColor, "White")
        '    BillToDiv.Style.Add(System.Web.UI.HtmlTextWriterStyle.TextAlign, "center")
        '    BillToDiv.Style.Add(System.Web.UI.HtmlTextWriterStyle.Position, "absolute")
        '    BillToDiv.Style.Add(System.Web.UI.HtmlTextWriterStyle.Display, "none")

        '    Dim imageDiv As New Image()
        '    imageDiv.ID = "LoadingImg"
        '    imageDiv.AlternateText = ""
        '    imageDiv.ImageUrl = "~/script/stars/loadRating.gif"

        '    BillToDiv.Controls.Add(imageDiv)

        '    Return BillToDiv
        'End Function


        Public Function buildCCTable()
            Dim CCTable As New Table
            Dim tr As TableRow
            Dim td As TableCell

            'AM2010091001 - ONLINE PAYMENTS - Start
            If isOnlinePayment() Then
                tr = New TableRow()
                td = New TableCell()
                'td.Style.Add(System.Web.UI.HtmlTextWriterStyle.Width, "45%")
                td.Style.Add(System.Web.UI.HtmlTextWriterStyle.TextAlign, "left")
                td.HorizontalAlign = HorizontalAlign.Left
                Dim lblAmount As New Label()
                lblAmount.Text = Resources.Language.LABEL_EEPG_AMOUNT
                tr.Cells.Add(td)
                td.Controls.Add(lblAmount)
                td = New TableCell()
                'td.Style.Add(System.Web.UI.HtmlTextWriterStyle.Width, "55%")
                td.Style.Add(System.Web.UI.HtmlTextWriterStyle.TextAlign, "left")
                td.HorizontalAlign = HorizontalAlign.Left

                tbxAmount = New TextBox()
                tbxAmount.ID = "Amount"
                tbxAmount.Width = 80
                tbxAmount.MaxLength = 16
                tbxAmount.CssClass = "borderTextBox"
                tbxAmount.Text = CDblEx(tbxAmountStartValue)
                td.Controls.Add(tbxAmount)

                validatorAmount = New RequiredFieldValidator
                validatorAmount.Text = Resources.Language.LABEL_EEPG_REQUIRED
                validatorAmount.ControlToValidate = "Amount"
                validatorAmount.ID = "AmountValidator"
                td.Controls.Add(validatorAmount)

                tr.Cells.Add(td)

                CCTable.Rows.Add(tr)
            End If
            'AM2010091001 - ONLINE PAYMENTS - End


            ' Third row
            tr = New TableRow()
            td = New TableCell()
            'td.Style.Add(System.Web.UI.HtmlTextWriterStyle.Width, "45%")
            td.Style.Add(System.Web.UI.HtmlTextWriterStyle.TextAlign, "left")
            td.HorizontalAlign = HorizontalAlign.Left
            Dim lblFName As New Label()
            lblFName.Text = Resources.Language.LABEL_EEPG_FIRST_NAME
            tr.Cells.Add(td)
            td.Controls.Add(lblFName)
            td = New TableCell()
            'td.Style.Add(System.Web.UI.HtmlTextWriterStyle.Width, "55%")
            td.Style.Add(System.Web.UI.HtmlTextWriterStyle.TextAlign, "left")
            td.HorizontalAlign = HorizontalAlign.Left

            tbxFName = New TextBox()
            tbxFName.ID = "FName"
            tbxFName.Width = 80
            tbxFName.MaxLength = 16
            tbxFName.CssClass = "borderTextBox"
            td.Controls.Add(tbxFName)

            validatorFName = New RequiredFieldValidator
            validatorFName.Text = Resources.Language.LABEL_EEPG_REQUIRED
            validatorFName.ControlToValidate = "FName"
            validatorFName.ID = "FNameValidator"
            td.Controls.Add(validatorFName)

            tr.Cells.Add(td)

            CCTable.Rows.Add(tr)

            ' Third row
            tr = New TableRow()
            td = New TableCell()
            'td.Style.Add(System.Web.UI.HtmlTextWriterStyle.Width, "45%")
            td.Style.Add(System.Web.UI.HtmlTextWriterStyle.TextAlign, "left")
            td.HorizontalAlign = HorizontalAlign.Left
            Dim lblLName As New Label()
            lblLName.Text = Resources.Language.LABEL_EEPG_LAST_NAME
            tr.Cells.Add(td)
            td.Controls.Add(lblLName)
            td = New TableCell()
            'td.Style.Add(System.Web.UI.HtmlTextWriterStyle.PaddingRight, "180px")
            'td.Style.Add(System.Web.UI.HtmlTextWriterStyle.Width, "55%")
            td.Style.Add(System.Web.UI.HtmlTextWriterStyle.TextAlign, "left")
            td.HorizontalAlign = HorizontalAlign.Left

            tbxLName = New TextBox()
            tbxLName.ID = "LName"
            tbxLName.Width = 110
            tbxLName.MaxLength = 16
            tbxLName.CssClass = "borderTextBox"
            td.Controls.Add(tbxLName)

            validatorLName = New RequiredFieldValidator
            validatorLName.Text = Resources.Language.LABEL_EEPG_REQUIRED
            validatorLName.ControlToValidate = "LName"
            validatorLName.ID = "LNameValidator"
            td.Controls.Add(validatorLName)

            tr.Cells.Add(td)
            CCTable.Rows.Add(tr)

            ' Third row
            tr = New TableRow()
            td = New TableCell()
            'td.Style.Add(System.Web.UI.HtmlTextWriterStyle.Width, "45%")
            td.Style.Add(System.Web.UI.HtmlTextWriterStyle.TextAlign, "left")
            td.HorizontalAlign = HorizontalAlign.Left
            Dim lblCCNR As New Label()
            lblCCNR.Text = Resources.Language.LABEL_EEPG_CC_NUMBER
            tr.Cells.Add(td)
            td.Controls.Add(lblCCNR)
            td = New TableCell()
            'td.Style.Add(System.Web.UI.HtmlTextWriterStyle.Width, "55%")
            td.Style.Add(System.Web.UI.HtmlTextWriterStyle.TextAlign, "left")
            td.HorizontalAlign = HorizontalAlign.Left

            tbxCCN = New TextBox()
            tbxCCN.ID = "CCN"
            tbxCCN.Width = 135
            tbxCCN.MaxLength = 16
            tbxCCN.CssClass = "borderTextBox"
            td.Controls.Add(tbxCCN)

            validatorCCN = New RequiredFieldValidator
            validatorCCN.Text = Resources.Language.LABEL_EEPG_REQUIRED
            validatorCCN.ControlToValidate = "CCN"
            validatorCCN.ID = "CCNValidator"
            td.Controls.Add(validatorCCN)

            tr.Cells.Add(td)
            CCTable.Rows.Add(tr)

            'Fourth row
            tr = New TableRow()
            td = New TableCell()
            'td.Style.Add(System.Web.UI.HtmlTextWriterStyle.Width, "45%")
            td.HorizontalAlign = HorizontalAlign.Left
            Dim lblExpDate As New Label()
            lblExpDate.Text = Resources.Language.LABEL_EEPG_EXP_DATE
            tr.Cells.Add(td)
            td.Controls.Add(lblExpDate)
            td = New TableCell()
            'td.Style.Add(System.Web.UI.HtmlTextWriterStyle.Width, "55%")
            'td.HorizontalAlign = HorizontalAlign.Left

            tbxExpDate = New TextBox()
            tbxExpDate.ID = "ExpDate"
            tbxExpDate.Width = 50
            tbxExpDate.MaxLength = 4
            tbxExpDate.CssClass = "borderTextBox"
            td.Controls.Add(tbxExpDate)

            validatorExpDate = New RequiredFieldValidator
            validatorExpDate.Text = Resources.Language.LABEL_EEPG_REQUIRED
            validatorExpDate.ControlToValidate = "ExpDate"
            validatorExpDate.ID = "ExpDateValidator"
            td.Controls.Add(validatorExpDate)

            tr.Cells.Add(td)
            CCTable.Rows.Add(tr)

            'Fifth(row)
            tr = New TableRow()
            td = New TableCell() 'table cell 1
            'td.Style.Add(System.Web.UI.HtmlTextWriterStyle.Width, "45%")
            td.HorizontalAlign = HorizontalAlign.Left
            Dim lblCCV As New Label()
            lblCCV.Text = Resources.Language.LABEL_EEPG_CVV2
            tr.Cells.Add(td)
            td.Controls.Add(lblCCV)
            td = New TableCell() 'table cell 2
            'td.Style.Add(System.Web.UI.HtmlTextWriterStyle.Width, "55%")

            tbxCCV = New TextBox()
            tbxCCV.ID = "CCV"
            tbxCCV.Width = 35
            tbxCCV.MaxLength = 4
            tbxCCV.CssClass = "borderTextBox"
            td.Controls.Add(tbxCCV)

            If CBoolEx(AppSettings("PAYMENT_EEPG_CVV_MANDATORY")) Then
                validatorCCV = New RequiredFieldValidator
                validatorCCV.Text = Resources.Language.LABEL_EEPG_REQUIRED
                validatorCCV.ControlToValidate = "CCV"
                validatorCCV.ID = "CCVDateValidator"
                td.Controls.Add(validatorCCV)
            End If

            tr.Cells.Add(td)
            CCTable.Rows.Add(tr)

            '<!-- JA2011042101 - EEPG USE EXISTING CREDIT CARDS - Start -->Dim tbxCC As TextBox
            tr = New TableRow()
            td = New TableCell()
            td.ColumnSpan = 2
            tbxCC = New TextBox()
            tbxCC.ID = "decryptData"
            tbxCC.Style.Add(System.Web.UI.HtmlTextWriterStyle.Display, "none")
            td.Controls.Add(tbxCC)
            tr.Cells.Add(td)
            CCTable.Rows.Add(tr)
            '<!-- JA2011042101 - EEPG USE EXISTING CREDIT CARDS - End -->



            'Sixth row
            tr = New TableRow()
            td = New TableCell()
            'td.Style.Add(System.Web.UI.HtmlTextWriterStyle.Width, "100%")
            td.Style.Add(System.Web.UI.HtmlTextWriterStyle.TextAlign, "left")

            Dim img1 As New Image()
            img1.ImageUrl = "~/images/cards/CVV_image.gif"
            td.Controls.Add(img1)
            'If ckeckAmexAvailable() Then
            Dim img2 As New Image()
            img2.ImageUrl = "~/images/cards/CVV_amex.png"
            td.Controls.Add(img2)
            'End If


            td.ColumnSpan = 2
            tr.Cells.Add(td)
            CCTable.Rows.Add(tr)
            CCTable.Style.Add(System.Web.UI.HtmlTextWriterStyle.Width, "330px")

            Return CCTable

        End Function

        Public Function buildBillToTable()
            Dim BillToTable As New Table
            Dim tr As TableRow
            Dim td As TableCell

            ' Third row
            tr = New TableRow()
            td = New TableCell()
            'td.ColumnSpan = 2
            td.Style.Add(System.Web.UI.HtmlTextWriterStyle.TextAlign, "left")
            td.HorizontalAlign = HorizontalAlign.Left
            If CBoolEx(AppSettings("PAYMENT_EEPG_REQUIRE_ADDRESS_INFO")) Then
                cbxAddress = New CheckBox()
                'cbxAddress.Text = Resources.Language.LABEL_EEPG_USE_BILL_TO_ADDRESS
                cbxAddress.ID = "cbxAddress"
                cbxAddress.InputAttributes.Add("style", "margin-top:-3px")
                cbxAddress.AutoPostBack = True
                AddHandler cbxAddress.CheckedChanged, AddressOf cbxChanged
                tr.Cells.Add(td)
                td.Controls.Add(cbxAddress)

                td = New TableCell()
                Dim lblCbxAddress As New Label()
                lblCbxAddress.Text = Resources.Language.LABEL_EEPG_USE_BILL_TO_ADDRESS
                lblCbxAddress.ID = "lblCbxAddress"
                tr.Cells.Add(td)
                td.Controls.Add(lblCbxAddress)
            Else
                cbxZipCode = New CheckBox()
                'cbxZipCode.Text = Resources.Language.LABEL_EEPG_USE_BILL_TO_ZIPCODE
                cbxZipCode.ID = "cbxZipCode"
                cbxZipCode.AutoPostBack = True
                AddHandler cbxZipCode.CheckedChanged, AddressOf cbxChanged
                tr.Cells.Add(td)
                td.Controls.Add(cbxZipCode)

                td = New TableCell()
                Dim lblCbxZipCode As New Label()
                lblCbxZipCode.Text = Resources.Language.LABEL_EEPG_USE_BILL_TO_ZIPCODE
                lblCbxZipCode.ID = "lblCbxZipCode"
                tr.Cells.Add(td)
                td.Controls.Add(lblCbxZipCode)
            End If
            tr.Cells.Add(td)

            BillToTable.Rows.Add(tr)

            ' Third row
            tr = New TableRow()
            td = New TableCell()
            td.Style.Add(System.Web.UI.HtmlTextWriterStyle.Width, "30%")
            td.Style.Add(System.Web.UI.HtmlTextWriterStyle.TextAlign, "left")
            td.HorizontalAlign = HorizontalAlign.Left
            If CBoolEx(AppSettings("PAYMENT_EEPG_REQUIRE_ADDRESS_INFO")) Then
                Dim lblAddress1 As New Label()
                lblAddress1.Text = Resources.Language.LABEL_EEPG_ADDRESS1
                tr.Cells.Add(td)
                td.Controls.Add(lblAddress1)
            Else
                Dim lblZipCode As New Label()
                lblZipCode.Text = Resources.Language.LABEL_EEPG_ZIPCODE
                tr.Cells.Add(td)
                td.Controls.Add(lblZipCode)
            End If

            td = New TableCell()
            td.Style.Add(System.Web.UI.HtmlTextWriterStyle.Width, "70%")
            td.Style.Add(System.Web.UI.HtmlTextWriterStyle.TextAlign, "left")
            td.HorizontalAlign = HorizontalAlign.Left
            If CBoolEx(AppSettings("PAYMENT_EEPG_REQUIRE_ADDRESS_INFO")) Then
                tbxAddress1 = New TextBox()
                tbxAddress1.ID = "Address1"
                tbxAddress1.Width = 110
                tbxAddress1.MaxLength = 50
                tbxAddress1.CssClass = "borderTextBox"
                td.Controls.Add(tbxAddress1)

                validatorAddress1 = New RequiredFieldValidator
                validatorAddress1.Text = Resources.Language.LABEL_EEPG_REQUIRED
                validatorAddress1.ControlToValidate = "Address1"
                validatorAddress1.ID = "Address1Validator"
                td.Controls.Add(validatorAddress1)
            Else
                tbxZipCode = New TextBox()
                tbxZipCode.ID = "ZipCode"
                tbxZipCode.Width = 110
                tbxZipCode.MaxLength = 50
                tbxZipCode.CssClass = "borderTextBox"
                td.Controls.Add(tbxZipCode)

                validatorZipCode = New RequiredFieldValidator
                validatorZipCode.Text = Resources.Language.LABEL_EEPG_REQUIRED
                validatorZipCode.ControlToValidate = "ZipCode"
                validatorZipCode.ID = "ZipCodeValidator"
                td.Controls.Add(validatorZipCode)
            End If
            tr.Cells.Add(td)
            BillToTable.Rows.Add(tr)

            ' Third row

            If CBoolEx(AppSettings("PAYMENT_EEPG_REQUIRE_ADDRESS_INFO")) Then
                tr = New TableRow()
                td = New TableCell()
                td.Style.Add(System.Web.UI.HtmlTextWriterStyle.Width, "30%")
                td.Style.Add(System.Web.UI.HtmlTextWriterStyle.TextAlign, "left")
                td.HorizontalAlign = HorizontalAlign.Left

                Dim lblAddress2 As New Label()
                lblAddress2.Text = Resources.Language.LABEL_EEPG_ADDRESS2
                tr.Cells.Add(td)
                td.Controls.Add(lblAddress2)

                td = New TableCell()
                td.Style.Add(System.Web.UI.HtmlTextWriterStyle.Width, "70%")
                td.Style.Add(System.Web.UI.HtmlTextWriterStyle.TextAlign, "left")
                td.HorizontalAlign = HorizontalAlign.Left
                tbxAddress2 = New TextBox()
                tbxAddress2.ID = "Address2"
                tbxAddress2.Width = 110
                tbxAddress2.MaxLength = 50
                tbxAddress2.CssClass = "borderTextBox"
                td.Controls.Add(tbxAddress2)

                tr.Cells.Add(td)
                BillToTable.Rows.Add(tr)

                'Fourth row
                tr = New TableRow()
                td = New TableCell()
                td.Style.Add(System.Web.UI.HtmlTextWriterStyle.Width, "30%")
                td.Style.Add(System.Web.UI.HtmlTextWriterStyle.TextAlign, "left")
                td.HorizontalAlign = HorizontalAlign.Left

                Dim lblCity As New Label()
                lblCity.Text = Resources.Language.LABEL_EEPG_CITY
                tr.Cells.Add(td)
                td.Controls.Add(lblCity)

                td = New TableCell()
                td.Style.Add(System.Web.UI.HtmlTextWriterStyle.Width, "70%")
                td.Style.Add(System.Web.UI.HtmlTextWriterStyle.TextAlign, "left")
                td.HorizontalAlign = HorizontalAlign.Left
                tbxCity = New TextBox()
                tbxCity.ID = "City"
                tbxCity.Width = 110
                tbxCity.MaxLength = 50
                tbxCity.CssClass = "borderTextBox"
                td.Controls.Add(tbxCity)

                validatorCity = New RequiredFieldValidator
                validatorCity.Text = Resources.Language.LABEL_EEPG_REQUIRED
                validatorCity.ControlToValidate = "City"
                validatorCity.ID = "CityValidator"
                td.Controls.Add(validatorCity)

                tr.Cells.Add(td)
                BillToTable.Rows.Add(tr)

                'Fifth(row)
                tr = New TableRow()
                td = New TableCell()
                td.Style.Add(System.Web.UI.HtmlTextWriterStyle.Width, "30%")
                td.Style.Add(System.Web.UI.HtmlTextWriterStyle.TextAlign, "left")
                td.HorizontalAlign = HorizontalAlign.Left

                Dim lblState As New Label()
                lblState.Text = Resources.Language.LABEL_EEPG_STATE
                tr.Cells.Add(td)
                td.Controls.Add(lblState)

                td = New TableCell()
                td.Style.Add(System.Web.UI.HtmlTextWriterStyle.Width, "70%")
                td.Style.Add(System.Web.UI.HtmlTextWriterStyle.TextAlign, "left")
                td.HorizontalAlign = HorizontalAlign.Left
                tbxState = New TextBox()
                tbxState.ID = "State"
                tbxState.Width = 110
                tbxState.MaxLength = CIntEx(AppSettings("NUMBER_CHARACTERS_STATE"))
                tbxState.CssClass = "borderTextBox"
                td.Controls.Add(tbxState)

                validatorState = New RequiredFieldValidator
                validatorState.Text = Resources.Language.LABEL_EEPG_REQUIRED
                validatorState.ControlToValidate = "State"
                validatorState.ID = "StateValidator"
                td.Controls.Add(validatorState)

                tr.Cells.Add(td)
                BillToTable.Rows.Add(tr)

                'Sixth row
                tr = New TableRow()
                td = New TableCell()
                td.Style.Add(System.Web.UI.HtmlTextWriterStyle.Width, "30%")
                td.Style.Add(System.Web.UI.HtmlTextWriterStyle.TextAlign, "left")
                td.HorizontalAlign = HorizontalAlign.Left

                Dim lblPhone As New Label()
                lblPhone.Text = Resources.Language.LABEL_EEPG_PHONE
                tr.Cells.Add(td)
                td.Controls.Add(lblPhone)

                td = New TableCell()
                td.Style.Add(System.Web.UI.HtmlTextWriterStyle.Width, "70%")
                td.Style.Add(System.Web.UI.HtmlTextWriterStyle.TextAlign, "left")
                td.HorizontalAlign = HorizontalAlign.Left
                tbxPhone = New TextBox()
                tbxPhone.ID = "Phone"
                tbxPhone.Width = 110
                tbxPhone.MaxLength = 30
                tbxPhone.CssClass = "borderTextBox"
                td.Controls.Add(tbxPhone)

                tr.Cells.Add(td)
                BillToTable.Rows.Add(tr)
                'Seventh row

                tr = New TableRow()
                td = New TableCell()
                td.Style.Add(System.Web.UI.HtmlTextWriterStyle.Width, "30%")
                td.Style.Add(System.Web.UI.HtmlTextWriterStyle.TextAlign, "left")
                td.HorizontalAlign = HorizontalAlign.Left

                Dim lblEmail As New Label()
                lblEmail.Text = Resources.Language.LABEL_EEPG_EMAIL
                tr.Cells.Add(td)
                td.Controls.Add(lblEmail)

                td = New TableCell()
                td.Style.Add(System.Web.UI.HtmlTextWriterStyle.Width, "70%")
                td.Style.Add(System.Web.UI.HtmlTextWriterStyle.TextAlign, "left")
                td.HorizontalAlign = HorizontalAlign.Left
                tbxEmail = New TextBox()
                tbxEmail.ID = "Email"
                tbxEmail.Width = 110
                tbxEmail.MaxLength = 80
                tbxEmail.CssClass = "borderTextBox"
                td.Controls.Add(tbxEmail)

                validatorEmail = New RequiredFieldValidator
                validatorEmail.Text = Resources.Language.LABEL_EEPG_REQUIRED
                validatorEmail.ControlToValidate = "Email"
                validatorEmail.ID = "EmailValidator"
                td.Controls.Add(validatorEmail)

                tr.Cells.Add(td)
                BillToTable.Rows.Add(tr)

                'Seventh row

                If CBoolEx(AppSettings("PAYMENT_EEPG_REQUIRE_ZIP_CODE")) Then
                    tr = New TableRow()
                    td = New TableCell()
                    td.Style.Add(System.Web.UI.HtmlTextWriterStyle.Width, "30%")
                    td.Style.Add(System.Web.UI.HtmlTextWriterStyle.TextAlign, "left")
                    td.HorizontalAlign = HorizontalAlign.Left

                    Dim lblZipCode As New Label()
                    lblZipCode.Text = Resources.Language.LABEL_EEPG_ZIPCODE
                    tr.Cells.Add(td)
                    td.Controls.Add(lblZipCode)

                    td = New TableCell()
                    td.Style.Add(System.Web.UI.HtmlTextWriterStyle.Width, "70%")
                    td.Style.Add(System.Web.UI.HtmlTextWriterStyle.TextAlign, "left")
                    td.HorizontalAlign = HorizontalAlign.Left
                    tbxZipCode = New TextBox()
                    tbxZipCode.ID = "ZipCode"
                    tbxZipCode.Width = 110
                    tbxZipCode.MaxLength = 50
                    tbxZipCode.CssClass = "borderTextBox"
                    td.Controls.Add(tbxZipCode)

                    validatorZipCode = New RequiredFieldValidator
                    validatorZipCode.Text = Resources.Language.LABEL_EEPG_REQUIRED
                    validatorZipCode.ControlToValidate = "ZipCode"
                    validatorZipCode.ID = "ZipCodeValidator"
                    td.Controls.Add(validatorZipCode)

                    tr.Cells.Add(td)
                    BillToTable.Rows.Add(tr)
                End If

            End If
            Return BillToTable
        End Function

        'Public Function ckeckAmexAvailable()
        '    Dim SQL As String = "SELECT ApprovedForUse FROM EECCApprovedCreditCardType WHERE (GatewayCardTypeCode = 3) AND (ApprovedForUse = 1) AND ConfiguredGateway=" & SafeString(eis.getEnterpriseConfigurationValue("ConfiguredGateway"))
        '    Dim retv As Boolean = CBoolEx(getSingleValueDB(SQL))
        '    Return retv
        'End Function


    End Class
    'AM2010041301 - ENTERPRISE PAYMENT GATEWAY - End
End Namespace

