﻿Imports System.Web

Namespace ExpandIT
    Public Class Resource

        Public Shared Function GetLabel(ByVal key As String) As String
            Return HttpContext.GetGlobalResourceObject("Language", key).ToString()
        End Function

    End Class
End Namespace