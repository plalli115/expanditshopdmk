Option Strict On

Imports System.Security.Cryptography
Imports System.IO
Imports System.Text

Namespace ExpandIT

    ''' <summary>
    ''' Library functions to assist in integration using SIM method
    ''' </summary>
    Public Class Authorizenet_Simlib

        ''' <summary>
        ''' Creates a MD5 HMAC
        ''' </summary>
        ''' <param name="key">The key</param>
        ''' <param name="text">The string to hash</param>
        ''' <returns>A string in hex format</returns>
        Private Shared Function HMAC(ByVal key As String, ByVal text As String) As String
            Dim myKey As Byte() = Encoding.Default.GetBytes(key)
            Dim myText As Byte() = Encoding.Default.GetBytes(text)
            Dim hmacVB As HMACMD5 = New HMACMD5(myKey)
            Dim hash As Byte() = hmacVB.ComputeHash(myText)
            Dim ret As String = ""
            For Each a As Byte In hash
                If (a < 16) Then
                    ret += "0" + a.ToString("x")
                Else
                    ret += a.ToString("x")
                End If
            Next
            Return ret
        End Function

        ''' <summary>
        ''' Returns the fingerprint
        ''' Use this when you need control of HTML output
        ''' Currency is optional. If you send x_currency_code then you must pass the same currency code here
        ''' </summary>
        ''' <param name="loginid"></param>
        ''' <param name="txnkey"></param>
        ''' <param name="amount"></param>
        ''' <param name="sequence"></param>
        ''' <param name="tstamp"></param>
        ''' <param name="currency"></param>
        ''' <returns></returns>
        Public Shared Function CalculateFP(ByVal loginid As String, ByVal txnkey As String, ByVal amount As String, ByVal sequence As String, ByVal tstamp As String, ByVal currency As String) As String
            If currency = Nothing Then currency = ""
            Dim fingerprint As String = HMAC(txnkey, loginid & "^" & sequence & "^" & tstamp & "^" & amount & "^" & currency)
            Return fingerprint
        End Function
        
        ''' <summary>
        ''' Return the calculated fingerprint fields
        ''' </summary>
        ''' <param name="loginid"></param>
        ''' <param name="txnkey"></param>
        ''' <param name="amount"></param>
        ''' <param name="sequence"></param>
        ''' <param name="currency"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetFingerprintFields(ByVal loginid As String, ByVal txnkey As String, ByVal amount As String, ByVal sequence As String, ByVal currency As String) As FingerprintFields
            Dim tstamp As String = GetSecondsSince1970().ToString()
            Dim fingerprint As String = CalculateFP(loginid, txnkey, amount, sequence, tstamp, currency)
            Dim fp As New FingerprintFields()
            fp.Sequence = sequence
            fp.Timestamp = tstamp
            fp.Fingerprint = fingerprint
            Return fp
        End Function

        ''' <summary>
        ''' Returns the number of seconds elapsed since Jan 1, 1970 UTC
        ''' </summary>
        ''' <returns></returns>
        Private Shared Function GetSecondsSince1970() As Integer
            Dim t As TimeSpan = DateTime.UtcNow - New DateTime(1970, 1, 1, 0, 0, 0, 0)
            Return CInt(t.TotalSeconds)
        End Function

    End Class

    Public Class FingerprintFields
        Public Property Sequence As String
        Public Property Timestamp As String
        Public Property Fingerprint As String
    End Class
End Namespace