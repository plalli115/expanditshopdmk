Option Strict On
Namespace ExpandIT
    Public Class Crypto
        ''' <summary>
        ''' Creates hash from input string
        ''' </summary>
        ''' <param name="toHash">The string that we want to create a hash for</param>
        ''' <returns>A string in hex format</returns>
        Public Shared Function coreMD5(ByVal toHash As String) As String
            Dim textBytes As Byte() = Text.Encoding.Default.GetBytes(toHash)
            Try
                Dim cryptHandler As New Security.Cryptography.MD5CryptoServiceProvider()
                Dim hash As Byte() = cryptHandler.ComputeHash(textBytes)
                Dim ret As String = ""
                For Each a As Byte In hash
                    If (a < 16) Then
                        ret += "0" + a.ToString("x")
                    Else
                        ret += a.ToString("x")
                    End If
                Next
                Return ret
            Catch
                Throw
            End Try
        End Function

        Public Shared Function coreSHA1(ByVal toHash As String) As String
            Dim textBytes As Byte() = Text.Encoding.Default.GetBytes(toHash)
            Try
                Dim cryptHandler As New Security.Cryptography.SHA1CryptoServiceProvider()
                Dim hash As Byte() = cryptHandler.ComputeHash(textBytes)
                Dim ret As String = ""
                For Each a As Byte In hash
                    If (a < 16) Then
                        ret += "0" + a.ToString("x")
                    Else
                        ret += a.ToString("x")
                    End If
                Next
                Return ret
            Catch
                Throw
            End Try
        End Function

        Public Shared Function manSHA256(ByVal toHash As String) As String
            Dim textBytes As Byte() = Text.Encoding.Default.GetBytes(toHash)
            Try
                Dim cryptHandler As New Security.Cryptography.SHA256Managed
                Dim hash As Byte() = cryptHandler.ComputeHash(textBytes)
                Dim ret As String = ""
                For Each a As Byte In hash
                    If (a < 16) Then
                        ret += "0" + a.ToString("x")
                    Else
                        ret += a.ToString("x")
                    End If
                Next
                Return ret
            Catch
                Throw
            End Try
        End Function

    End Class
End Namespace
