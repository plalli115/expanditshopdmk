Imports System.Data.SqlClient
Imports System.Text
Imports System.Web
Imports System.Configuration
Imports ExpandIT.Logging

Namespace ExpandIT

    Public Class ExpandITLib

        Private Shared ConnStr As String
        Private Shared ConnStrName As String
        Private Shared isLocalModeVB As Boolean
        Private Shared virtualRoot As String
        Private Shared appRoot As String
        
        Public Shared ReadOnly Property VRoot() As String
            Get
                Return virtualRoot
            End Get
        End Property

        Public Shared ReadOnly Property ApplicationRoot() As String
            Get
                Return appRoot
            End Get
        End Property

        ''' <summary>
        ''' Checks if the site is running in local or remote mode
        ''' </summary>
        ''' <value></value>
        ''' <returns>True if the site is running in local mode</returns>
        ''' <remarks></remarks>
        Public Shared ReadOnly Property IsLocalMode() As Boolean
            Get
                Return isLocalModeVB
            End Get
        End Property

        ''' <summary>
        ''' Get or Set the current ConnectionStringName used by this module
        ''' </summary>
        ''' <value>String</value>
        ''' <returns>The name of the connectionstring</returns>
        ''' <remarks></remarks>
        Public Shared Property CurrentConnectionString() As String
            Get
                Return ConnStr
            End Get
            Set(ByVal value As String)
                Try
                    ConnStr = value
                Catch ex As Exception

                End Try
            End Set
        End Property

        ''' <summary>
        ''' Get or Set the current ConnectionStringName used by this module
        ''' </summary>
        ''' <value>String</value>
        ''' <returns>The name of the connectionstring</returns>
        ''' <remarks></remarks>
        Public Shared Property CurrentConnectionStringName() As String
            Get
                Return ConnStrName
            End Get
            Set(ByVal value As String)
                Try
                    ConnStrName = value
                Catch ex As Exception

                End Try
            End Set
        End Property

        Shared Sub New()

            Try
                If Not isRemoteMode() Then
                    isLocalModeVB = True
                End If
                ConnStr = ConfigurationManager.ConnectionStrings("ExpandITConnectionString").ConnectionString
                ConnStrName = "ExpandITConnectionString"
            Catch ex As Exception

            End Try

            If String.IsNullOrEmpty(ConnStr) Then
                Dim arr As ConfigurationElement() = {}
                Try
                    Array.Resize(arr, ConfigurationManager.ConnectionStrings.Count)
                    ConfigurationManager.ConnectionStrings.CopyTo(arr, 0)
                    If arr.Length > 1 Then
                        ConnStr = arr(1).ToString()
                    Else
                        ConnStr = arr(0).ToString()
                    End If
                Catch ex As Exception
                    ConnStr = ""
                End Try
            End If

            virtualRoot = getVirtualRoot()
            appRoot = getShopRoot()
        End Sub
        
        ''' <summary>
        ''' Finds out if this is a remote or local site
        ''' </summary>
        ''' <returns>Boolean</returns>
        ''' <remarks></remarks>
        Private Shared Function isRemoteMode() As Boolean
            Dim str As String = HttpRuntime.AppDomainAppPath & "expandITRemote.xml"
            Return System.IO.File.Exists(str)
        End Function

        ''' <summary>
        ''' Determines the absolute path for the virtual root of the current ASP application.
        ''' </summary>
        ''' <returns>The absolute path for the virtual root</returns>
        ''' <remarks></remarks>
        Private Shared Function getVirtualRoot() As String
            Dim vpath As String = HttpRuntime.AppDomainAppVirtualPath
            If vpath.Equals("/") Then Return ""
            Return HttpRuntime.AppDomainAppVirtualPath
        End Function

        ''' <summary>
        ''' GetShopRoot determines the physical root path of the shop installation.
        ''' </summary>
        ''' <returns>The physical root path of the shop installation</returns>
        ''' <remarks></remarks>
        Private Shared Function getShopRoot() As String
            Return HttpRuntime.AppDomainAppPath.TrimEnd("\")
        End Function

#Region "DB"

        ''' <summary>
        ''' Returns an open SqlConnection Object
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetDBConnection() As SqlConnection
            Dim conn As New SqlConnection(ConnStr)
            conn.Open()
            Return conn
        End Function

        ''' <summary>
        ''' Creates a dictionary with the values of the fields in a single DataRow.
        ''' </summary>
        ''' <param name="row">DataRow</param>
        ''' <returns>ExpDictionary</returns>
        ''' <remarks></remarks>
        Public Shared Function SetDictionary(ByVal row As DataRow) As ExpDictionary
            Dim i As Integer
            Dim retv As New ExpDictionary()

            For i = 0 To row.Table.Columns.Count - 1
                retv.Add(row.Table.Columns(i).ColumnName, row(i))
            Next

            Return retv
        End Function

        ''' <summary>
        ''' Function returns a single value as a result from a database query
        ''' </summary>
        ''' <param name="sql">The SQL-Statement to excecute</param>
        ''' <returns>A single value of type Object</returns>
        ''' <remarks></remarks>
        Public Shared Function getSingleValueDB(ByVal sql As String) As Object
            Dim oc As SqlCommand
            Dim ret As Object = Nothing
            Using localConn As New SqlConnection(ConnStr)
                Try
                    localConn.Open()
                    oc = New SqlCommand(sql, localConn)
                    oc.CommandType = CommandType.Text
                    ret = oc.ExecuteScalar()
                Catch ex As Exception
                End Try
            End Using
            Return ret
        End Function


        Public Shared Function HasRecords(ByVal sql As String) As Boolean
            Dim dr As SqlDataReader
            Dim conn As SqlConnection
            Dim cmd As SqlCommand
            Dim retv As Boolean

            conn = GetDBConnection()
            cmd = New SqlCommand(sql, conn)
            dr = cmd.ExecuteReader()
            retv = dr.Read
            dr.Close()
            conn.Close()
            Return retv
        End Function


        ''' <summary>
        ''' Populates a DataTable with the result from the provided SQL-Query 
        ''' </summary>
        ''' <param name="sql"></param>
        ''' <returns>DataTable</returns>
        ''' <remarks></remarks>
        Public Shared Function SQL2DataTable(ByVal sql As String) As DataTable
            Dim da As SqlDataAdapter
            Dim conn As SqlConnection = GetDBConnection()
            Dim cmd As New SqlCommand(sql, conn)
            cmd.CommandTimeout = 0 'Don't time out
            Dim dt As New DataTable
            da = New SqlDataAdapter(cmd)
            da.Fill(dt)
            da.Dispose()
            conn.Close()
            Return dt
        End Function

        ''' <summary>
        ''' Creates a dictionary of multiple dictionaries from a DataTable
        ''' </summary>
        ''' <param name="dt">DataTable</param>
        ''' <param name="FieldName">Specifies the field to retreive (optional)</param>
        ''' <param name="recMax">Maxium number of records to retreive (optional)</param>
        ''' <returns>A multilevel dictionary</returns>
        ''' <remarks>
        ''' This function can retreive all records of all fields from a DB-Table. It is also possible to retreive only one selected field.
        ''' Furthermore, it'salso possible to reduce the number of retrieved records.
        ''' </remarks>
        Public Shared Function DT2Dicts(ByVal dt As DataTable, Optional ByVal FieldName As String = "", Optional ByVal recMax As Integer = -1) As ExpDictionary
            Dim retv As New ExpDictionary
            Dim key As String
            Dim cnt As Integer = 0
            Dim ht As ExpDictionary
            Dim fieldno As Integer = 0

            If FieldName <> "" Then fieldno = dt.Columns.IndexOf(FieldName)

            For Each row As DataRow In dt.Rows
                cnt = cnt + 1
                If FieldName = "" Then key = cnt.ToString Else key = CType(DBNull2Nothing(row(fieldno)), String)
                ht = SetDictionary(row)
                If key IsNot Nothing Then '<< Changed here >>
                    If retv.ContainsKey(key) Then
                        retv(key) = ht
                    Else
                        retv.Add(key, ht)
                    End If
                End If
            Next
            Return retv
        End Function

        Public Shared Function SQL2Dicts(ByVal sql As String) As ExpDictionary
            Return SQL2DictsEx(sql, Nothing, -1)
        End Function

        Public Shared Function SQL2Dicts(ByVal sql As String, ByVal fieldname As String) As ExpDictionary
            Return SQL2DictsEx(sql, fieldname, -1)
        End Function

        Public Shared Function SQL2Dicts(ByVal sql As String, ByVal fieldnames() As String) As ExpDictionary
            Return SQL2DictsEx(sql, fieldnames, -1)
        End Function

        Public Shared Function SQL2Dicts(ByVal sql As String, ByVal FieldName As Object, ByVal nMax As Integer) As ExpDictionary
            Return SQL2DictsEx(sql, FieldName, nMax)
        End Function

        Public Shared Function SQL2DictsEx(ByVal sql As String, ByVal FieldName As Object, ByVal nMax As Integer) As ExpDictionary
            Dim dt As DataTable = SQL2DataTable(sql)
            Return DT2Dicts(dt, FieldName, nMax)
        End Function
        

        ''' <summary>
        ''' Creates a dictionary with the values of the fields in all records in a DataTable.
        ''' </summary>
        ''' <param name="rs">DataTable</param>
        ''' <param name="keyfields">Object</param>
        ''' <returns>ExpDictionary</returns>
        ''' <remarks>The function returns the dictionary containing the values from the DataTable.
        ''' Top level dictionaries are named using the values of the key fields for each record.</remarks>
        Public Shared Function SetDictionaries(ByVal rs As DataTable, ByVal keyfields As Object) As ExpDictionary
            Return SetDictionariesEx(rs, keyfields, 0)
        End Function

        ''' <summary>
        ''' This is a helper function for SetDictionaries.
        ''' </summary>
        ''' <param name="dt">DataTable</param>
        ''' <param name="keyfields">Object - Array or string with keyfields or keyfield.</param>
        ''' <param name="top">Integer - The number of records to return</param>
        ''' <returns>ExpDictionary</returns>
        ''' <remarks>The function returns the dictionary containing the values from the DataTable.
        ''' Top level dictionaries are named using the values of the key fields for each record.</remarks>
        Public Shared Function SetDictionariesEx(ByVal dt As DataTable, ByVal keyfields As Object, Optional ByVal top As Integer = -1) As ExpDictionary
            Dim retv As ExpDictionary
            Dim key As String
            Dim i As Integer
            Dim cnt As Integer
            Dim a() As Integer

            If TypeName(keyfields) = "String" Then
                ReDim a(0)
                a(0) = dt.Columns.IndexOf(keyfields)
                keyfields = a
            Else
                ReDim a(UBound(keyfields))
                For i = 0 To UBound(keyfields)
                    a(i) = dt.Columns.IndexOf(keyfields(i))
                Next
                keyfields = a
            End If

            cnt = 0
            retv = New ExpDictionary
            For Each row As DataRow In dt.Rows
                If Not (cnt < top Or top <= 0) Then Exit For
                key = ""
                For i = 0 To UBound(keyfields)
                    If i > 0 Then key = key & ","
                    key = key & ExpanditLib2.SafeString(row(keyfields(i)))
                Next
                retv(key) = SetDictionary(row)
                cnt = cnt + 1
            Next
            SetDictionariesEx = retv
        End Function

        ''' <summary>
        ''' This version creates multilevel dictionaries.
        ''' </summary>
        ''' <param name="dt">Datatable</param>
        ''' <param name="keyfields">Array or string with keyfields or keyfield.</param>
        ''' <param name="top">The number of records to return.</param>
        ''' <returns>A multilevelDictionary</returns>
        ''' <remarks>
        ''' The function returns the dictionary containing the values from the recordset.
        ''' Top level dictionaries are named using the values of the key fields for each record.
        ''' One level of dictionaries will be added for each keyfield specified.
        ''' </remarks>
        Public Shared Function SetDictionaries2Ex(ByVal dt As DataTable, ByVal keyfields As Object, Optional ByVal top As Integer = -1) As ExpDictionary
            Dim retv As ExpDictionary
            Dim key As String
            Dim i As Integer
            Dim cnt As Integer
            Dim item As ExpDictionary
            Dim a() As Integer

            If TypeName(keyfields) = "String" Then
                ReDim a(0)
                a(0) = dt.Columns.IndexOf(keyfields)
                keyfields = a
            Else
                ReDim a(UBound(keyfields))
                For i = 0 To UBound(keyfields)
                    a(i) = dt.Columns.IndexOf(keyfields(i))
                Next
                keyfields = a
            End If

            cnt = 0
            retv = New ExpDictionary
            For Each row As DataRow In dt.Rows
                key = ""
                item = retv
                For i = 0 To UBound(keyfields) - 1
                    If item(ExpanditLib2.SafeString(row(keyfields(i)))) Is Nothing Then
                        item(ExpanditLib2.SafeString(row(keyfields(i)))) = New ExpDictionary
                    End If
                    item = item(ExpanditLib2.SafeString(row(keyfields(i))))
                Next
                item(ExpanditLib2.SafeString(row(keyfields(i)))) = SetDictionary(row)
                cnt = cnt + 1
            Next

            SetDictionaries2Ex = retv
        End Function

        ''' <summary>
        ''' Returns the first record from a sql result in a dictionary
        ''' </summary>
        ''' <param name="sql">SQL statement.</param>
        ''' <returns>Dictionary</returns>
        ''' <remarks>This function executes a sql query and returns the result as a dictionary.</remarks>
        Public Shared Function Sql2Dictionary(ByVal sql As String) As ExpDictionary
            Dim dt As DataTable = SQL2DataTable(sql)
            If dt.Rows.Count > 0 Then
                Return SetDictionary(dt.Rows(0))
            Else
                Return Nothing
            End If
        End Function

        ''' <summary>
        ''' Returns a dictionary with all the records from a sql result
        ''' using keyfields as top level element in dictionary
        ''' </summary>
        ''' <param name="sql">SQL statement.</param>
        ''' <param name="keyfields">Array or string with keyfields or keyfield.</param>
        ''' <returns>Dictionary</returns>
        ''' <remarks>Uses Sql2DictionariesEx internally</remarks>
        Public Shared Function Sql2Dictionaries(ByVal sql As String, ByVal keyfields As Object) As ExpDictionary
            Return Sql2DictionariesEx(sql, keyfields, 0)
        End Function

        ''' <summary>
        ''' Returns a dictionary with all the records from sql result
        ''' using each keyfield as a level in the resulting dictionary.
        ''' </summary>
        ''' <param name="sql">SQL statement.</param>
        ''' <param name="keyfields">Array or string with keyfields or keyfield.</param>
        ''' <returns>Dictionary</returns>
        ''' <remarks>Uses Sql2Dictionaries2Ex internally</remarks>
        Public Shared Function Sql2Dictionaries2(ByVal sql As String, ByVal keyfields As Object) As ExpDictionary
            Return Sql2Dictionaries2Ex(sql, keyfields, 0)
        End Function

        ''' <summary>
        ''' This function returns a number of records from the result of a sql query.
        ''' </summary>
        ''' <param name="sql">SQL statement.</param>
        ''' <param name="keyfields">Array or string with keyfields or keyfield.</param>
        ''' <param name="top">The number of records to return.</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function Sql2DictionariesEx(ByVal sql As String, Optional ByVal keyfields As Object = Nothing, Optional ByVal top As Integer = -1) As ExpDictionary
            Dim retv As ExpDictionary
            Dim dt As DataTable = SQL2DataTable(sql)
            retv = SetDictionariesEx(dt, keyfields, top)
            Return retv
        End Function

        ''' <summary>
        ''' This function returns a number of records from the result of a sql query.
        ''' Each keyfield will result in a level in the resulting dictionary.
        ''' </summary>
        ''' <param name="sql"></param>
        ''' <param name="keyfields"></param>
        ''' <param name="top"></param>
        ''' <returns></returns>
        ''' <remarks>The result is returned in a dictionary.</remarks>
        Public Shared Function Sql2Dictionaries2Ex(ByVal sql As String, ByVal keyfields As Object, ByVal top As Integer) As ExpDictionary
            Dim retv As ExpDictionary
            Dim dt As DataTable = SQL2DataTable(sql)
            retv = SetDictionaries2Ex(dt, keyfields, top)
            Return retv
        End Function

        ''' <summary>
        ''' Assigns values to the fields of a recordset from a dictionary.
        ''' </summary>
        ''' <param name="d">A dictionary containing the values.</param>
        ''' <param name="row">The row to receive the values.</param>
        ''' <remarks></remarks>
        Public Shared Sub SetDataRowEx(ByRef d As ExpDictionary, ByRef row As DataRow)
            Dim v As Object
            Dim byteArray As Byte()

            For i As Integer = 0 To row.Table.Columns.Count - 1
                Try
                    v = d(row.Table.Columns(i).ColumnName)
                Catch ex As KeyNotFoundException
                    v = ""
                End Try
                If Not row.Table.Columns(i).AutoIncrement Then
                    Select Case TypeName(v)
                        Case "String"
                            Dim t As Type = row.Table.Columns(i).DataType
                            If t.Name.Equals("Boolean") Then
                                v = ExpanditLib2.CBoolEx(v)
                                Continue For
                            End If
                            If Not t.Name.Equals("Byte[]") Then
                                If row.Table.Columns(i).MaxLength >= 0 Then
                                    v = Left(v, row.Table.Columns(i).MaxLength) 'MaxLength property must be set on all fields!!
                                End If
                            Else
                                Try
                                    byteArray = Encoding.Default.GetBytes(v)
                                    row.Item(i) = byteArray
                                Catch ex As Exception
                                    FileLogger.log("SOME ERROR: " & ex.Message)
                                End Try
                                Continue For
                            End If
                    End Select
                    If Not v Is Nothing Then
                        Try
                            row.Item(i) = v
                        Catch ex As Exception
                            FileLogger.log("ERROR:" & ex.Message)
                        End Try
                    End If
                End If
            Next
        End Sub

        ''' <summary>
        ''' Updates the record located by the SQL statement with the values from the dict dictionary.
        ''' </summary>
        ''' <param name="dict"></param>
        ''' <param name="tableName"></param>
        ''' <param name="sql"></param>
        ''' <remarks>Overloaded Sub is acting as a wrapper around SetDataRowEx</remarks>
        Public Shared Sub SetDictUpdateEx(ByVal dict As ExpDictionary, ByVal tableName As String, _
            ByVal sql As String)

            Dim dt As New DataTable()
            Dim da As New SqlDataAdapter()
            Dim dr As DataRow
            Dim isNew As Boolean = False

            If sql = "" Then
                sql = "SELECT * FROM [" & tableName & "] WHERE 1=2"
                isNew = True
            End If

            dt = getDataTable(sql, da)

            ' Add a new row if none is available
            If dt.Rows.Count = 0 Then isNew = True

            Try
                If isNew Then
                    dr = dt.NewRow()
                    SetDataRowEx(dict, dr)
                    dt.Rows.Add(dr)
                Else
                    SetDataRowEx(dict, dt.Rows(0))
                End If
                da.Update(dt)
            Catch ex As Exception
            End Try

        End Sub

        ''' <summary>
        ''' Inserts a new record in the table given by the tablename parameter.
        ''' </summary>
        ''' <param name="dict">Dictionary with the values to insert.</param>
        ''' <param name="tableName">Name of the table that should have the new record.</param>
        ''' <remarks>Overloaded Sub is acting as a wrapper around SetDataRowEx</remarks>
        Public Shared Sub SetDictUpdateEx(ByVal dict As ExpDictionary, ByVal tableName As String)
            SetDictUpdateEx(dict, tableName, "")
        End Sub

        ''' <summary>
        ''' Executes Any SQL-statement. Does not return a reultset. 
        ''' </summary>
        ''' <param name="cmdText">SQL-Statement</param>
        ''' <remarks>
        ''' Useful for queries that don't require a resultset
        ''' </remarks>
        Public Shared Function excecuteNonQueryDb(ByVal cmdText As String) As String
            Dim oc As SqlCommand
            Dim message As String = String.Empty

            Using localConn As New SqlConnection(ConnStr)
                Try
                    localConn.Open()
                    oc = New SqlCommand(cmdText, localConn)
                    oc.CommandType = CommandType.Text
                    oc.ExecuteNonQuery()
                Catch ex As Exception
                    message = ex.Message
                End Try
            End Using
            Return message
        End Function

        ''' <summary>
        ''' This function returns a filled DataTable
        ''' </summary>
        ''' <param name="inSqlCommand">A SQL-Statement</param>
        ''' <param name="da">A SqlDataAdapter</param>
        ''' <returns>DataTable</returns>
        ''' <remarks></remarks>
        Public Shared Function getDataTable(ByVal inSqlCommand As String, Optional ByRef da As SqlDataAdapter = Nothing) As DataTable
            Dim od As SqlDataAdapter
            Dim dt As New DataTable()

            If da IsNot Nothing Then
                da = getDataAdapter(, inSqlCommand)
                da.Fill(dt)
            Else
                od = getDataAdapter(, inSqlCommand)
                od.Fill(dt)
            End If
            getDataTable = dt

        End Function

        ''' <summary>
        ''' This function returns a DataAdapter tied to a Database table
        ''' </summary>
        ''' <param name="tableName">The name of the db-table</param>
        ''' <param name="sqlSelectCommand">The SQL-Statement</param>
        ''' <returns>SqlDataAdapter</returns>
        ''' <remarks>
        ''' This Function creates commands for delete, insert and update, by using a CommandBuilder. 
        ''' It uses the MissingSchemaAction.AddWithKey option, so the db-table must contain at least
        ''' a unique index, to make the CommandBuilder work.        
        ''' </remarks>
        Public Shared Function getDataAdapter(Optional ByVal tableName As String = Nothing, _
            Optional ByVal sqlSelectCommand As String = Nothing) As SqlDataAdapter

            Dim da As SqlDataAdapter
            Dim dcmd As SqlCommandBuilder
            Dim localConnStr As String = ConnStr

            Select Case True
                Case sqlSelectCommand Is Nothing And tableName IsNot Nothing
                    da = New SqlDataAdapter("SELECT * FROM " & tableName & " WHERE 1 = 2", New String(localConnStr))
                Case sqlSelectCommand IsNot Nothing And tableName Is Nothing
                    da = New SqlDataAdapter(sqlSelectCommand, New String(localConnStr))
                Case sqlSelectCommand IsNot Nothing And tableName IsNot Nothing
                    da = New SqlDataAdapter(sqlSelectCommand & tableName, New String(localConnStr))
                Case Else
                    Return Nothing
            End Select

            Try
                dcmd = New SqlCommandBuilder(da)
                da.DeleteCommand = dcmd.GetDeleteCommand()
                da.InsertCommand = dcmd.GetInsertCommand()
                da.UpdateCommand = dcmd.GetUpdateCommand()
                da.MissingSchemaAction = MissingSchemaAction.AddWithKey
            Catch ex As Exception
            End Try

            Return da

        End Function
        
        ''' <summary>
        ''' Insert the dictionary (dict) values into a record in the TableName
        ''' </summary>
        ''' <param name="dict">Source dictionary</param>
        ''' <param name="TableName">Destination table.</param>
        ''' <param name="DeleteWhere">Insert a delete statement</param>
        ''' <returns>True on success, else false</returns>        
        ''' <remarks>Deletes a specified set of records if requested before saving data.</remarks>
        Public Shared Function Dict2Table(ByVal dict As ExpDictionary, ByVal TableName As String, ByVal DeleteWhere As String) As Boolean

            Dim sql As String
            Dim dt As New DataTable()
            Dim da As New SqlDataAdapter()
            Dim dr As DataRow
            Dim result As Boolean = True
            ' Delete records if so required
            If DeleteWhere <> "" Then
                sql = "DELETE FROM " & TableName & " WHERE " & DeleteWhere
                excecuteNonQueryDb(sql)
            End If

            sql = "SELECT * FROM " & TableName & " WHERE 1 = 2"

            dt = getDataTable(sql, da)

            Try
                dr = dt.NewRow()
                SetDataRowEx(dict, dr)
                dt.Rows.Add(dr)
                da.Update(dt)
            Catch ex As Exception
                result = False
            End Try
            Return result
        End Function

        ''' <summary>
        ''' Insert the dictionaries (dicts) and corresponding values into a number of records in the TableName
        ''' </summary>
        ''' <param name="dicts">Source dictionary with sub dictinaries.</param>
        ''' <param name="TableName">Destination table.</param>
        ''' <param name="DeleteWhere">Insert a delete statement.</param>
        ''' <remarks>Deletes a specified set of records if requested before saving data.</remarks>
        Public Shared Sub Dicts2Table(ByVal dicts As ExpDictionary, ByVal TableName As String, ByVal DeleteWhere As String)
            Dim sql As String
            Dim dict As ExpDictionary
            Dim dt As New DataTable()
            Dim da As New SqlDataAdapter()
            Dim row As DataRow

            ' Delete records if so required
            If DeleteWhere <> "" Then
                sql = "DELETE FROM [" & TableName & "] WHERE " & DeleteWhere
                excecuteNonQueryDb(sql)
            End If

            sql = "SELECT * FROM [" & TableName & "] WHERE 1=2"
            dt = getDataTable(sql, da)

            For Each dict In dicts.Values
                row = dt.NewRow()
                SetDataRowEx(dict, row)
                dt.Rows.Add(row)
            Next
            da.Update(dt)
        End Sub

#End Region
#Region "wrappers"

        ''' <summary>
        ''' Replacement of the built in ASP version of server.htmlencode.
        ''' </summary>
        ''' <param name="v">Object to encode.</param>
        ''' <returns>The html encoded string.</returns>
        ''' <remarks></remarks>
        Public Shared Function HTMLEncode(ByVal v As Object) As String
            v = ExpanditLib2.CStrEx(v)
            If v = "" Then HTMLEncode = "" Else HTMLEncode = HttpContext.Current.Server.HtmlEncode(v)
        End Function

        Public Shared Function DBNull2Nothing(ByVal v As Object) As Object
            If v Is DBNull.Value Then
                Return Nothing
            Else
                Return v
            End If
        End Function

        Public Shared Function IsNull(ByVal v As Object) As Boolean
            Return (v Is DBNull.Value)
        End Function

#End Region

        Public Shared Function SafeRetUrl(retUrl As String) As String
            If Uri.IsWellFormedUriString(retUrl, UriKind.Relative) Then
                Return retUrl
            Else
                Return VRoot & "/"
            End If
        End Function

    End Class

End Namespace