Imports System.IO
Imports System.Web

Namespace ExpandIT.Logging

    Public Class FileLogger

        Private Shared logpath As String = HttpRuntime.AppDomainAppPath.TrimEnd("\") & "\App_Data\filelog.txt"
        Private Shared format As String = "{0}: {1}"

        Public Shared Sub log(ByVal message As String)
            Try
                Dim fs As New FileStream(logpath, FileMode.Append, FileAccess.Write, FileShare.ReadWrite)
                Dim fw As New StreamWriter(fs)
                Try
                    fw.WriteLine(String.Format(format, System.DateTime.Now().ToLongTimeString(), message))
                    fw.Flush()
                Catch ex As IOException
                Finally
                    fw.Close()
                    fs.Close()
                End Try
            Catch ex As Exception

            End Try

        End Sub

        Public Shared Sub log(ByVal message As Object)
            Try
                Dim fs As New FileStream(logpath, FileMode.Append, FileAccess.Write, FileShare.ReadWrite)
                Dim fw As New StreamWriter(fs)
                Try
                    fw.WriteLine(String.Format(format, System.DateTime.Now().ToLongTimeString(), message))
                    fw.Flush()
                Catch ex As IOException
                Finally
                    fw.Close()
                    fs.Close()
                End Try
            Catch ex As Exception

            End Try

        End Sub

    End Class
End Namespace

