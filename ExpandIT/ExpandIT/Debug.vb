Imports System.Web
Imports System.Text.RegularExpressions
Imports System.Text
Imports ExpandIT.ExpandITLib

Namespace ExpandIT

    ''' <summary>
    ''' This library contains debugging functionality.
    ''' </summary>
    ''' <remarks>Dump of values etc.</remarks>
    Public Class Debug

        ''' <summary>
        ''' Renders a dictionary in a readable HTML representation, with no indentation.
        ''' </summary>
        ''' <param name="d">Dictionary to render.</param>
        ''' <returns>An HTML formatted string</returns>
        ''' <remarks>This function is very useful when debugging the content of a dictionary.
        ''' See Also DebugValue, HTMLDumpDictionary, DumpDictionaryBranch</remarks>
        Public Shared Function DumpDictionary(ByVal d As ExpDictionary) As String
            Return DumpDictionaryBranch(d, 0)
        End Function

        ''' <summary>
        ''' Helper function for DumpDictionary.
        ''' </summary>
        ''' <param name="d">Dictionary to render.</param>
        ''' <param name="indent">Level of indentation</param>
        ''' <returns>An HTML formatted string</returns>
        ''' <remarks>Loops through the dictionary preparing the output.</remarks>
        Public Shared Function DumpDictionaryBranch(ByVal d As ExpDictionary, ByVal indent As Integer) As String
            Dim retv As String
            Dim i As Integer
            Dim names As Object

            retv = DumpTypeName(d)
            names = d.Keys
            For i = 0 To d.Count - 1
                If TypeName(d(names(i))) <> "ExpDictionary" Then
                    retv = retv & vbCrLf & Strings.StrDup(indent, " ") & names(i) & " = " & DebugType(d(names(i)), indent + 4)
                End If
            Next
            For i = 0 To d.Count - 1
                If TypeName(d(names(i))) = "ExpDictionary" Then
                    retv = retv & vbCrLf & Strings.StrDup(indent, " ") & names(i) & " = " & DebugType(d(names(i)), indent + 4)
                End If
            Next
            Return retv
        End Function

        Public Shared Function DumpNetDictionaryBranch(ByRef d As Object, ByVal indent As Integer) As String
            Dim retv As String
            Dim k As String

            retv = DumpTypeName(d)
            For Each k In d.keys
                If TypeName(d(k)) <> "Hashtable" And TypeName(d(k)) <> "SortedList" Then
                    retv = retv & vbCrLf & Strings.StrDup(indent, " ") & k & " = " & DebugType(d(k), indent + 4)
                End If
            Next
            For Each k In d.keys
                If TypeName(d(k)) = "Hashtable" Or TypeName(d(k)) = "SortedList" Then
                    retv = retv & vbCrLf & Strings.StrDup(indent, " ") & k & " = " & DebugType(d(k), indent + 4)
                End If
            Next
            Return retv
        End Function

        ''' <summary>
        ''' Helper function for DebugValue.
        ''' </summary>
        ''' <param name="v">Data for which the type should be rendered.</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function DumpTypeName(ByVal v As Object) As String
            DumpTypeName = "<span style=""color: #999999"">[" & TypeName(v) & "]</span>"
        End Function

        ''' <summary>
        ''' Helper function for DebugValue.
        ''' </summary>
        ''' <param name="v">Request object that should be rendered.</param>
        ''' <param name="indent"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function DumpRequest(ByVal v As HttpRequest, ByVal indent As Integer) As String
            Dim retv As String = DumpTypeName(v)
            retv = retv & vbCrLf & Strings.StrDup(indent, " ") & "QueryString = " & DebugType(v.QueryString, indent + 4)
            retv = retv & vbCrLf & Strings.StrDup(indent, " ") & "Form = " & DebugType(v.Form, indent + 4)
            Return retv
        End Function

        ''' <summary>
        ''' Helper function for DebugValue.
        ''' </summary>
        ''' <param name="v">RequestDictionary object that should be rendered.</param>
        ''' <param name="indent"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function DumpRequestDictionary(ByVal v As Object, ByVal indent As Integer) As String
            Dim retv As String = DumpTypeName(v)
            For i As Integer = 1 To v.Count
                retv = retv & vbCrLf & Strings.StrDup(indent, " ") & HTMLEncode(v.Key(i)) & " = " & DebugType(v(i), indent + 4)
            Next
            Return retv
        End Function

        ''' <summary>
        ''' Helper function for DebugValue.
        ''' </summary>
        ''' <param name="v">StringList object that should be rendered.</param>
        ''' <param name="indent"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function DumpStringList(ByVal v As Object, ByVal indent As Integer) As String

            Dim retv As String = DumpTypeName(v)
            For i As Integer = 1 To v.Count
                retv = retv & vbCrLf & Strings.StrDup(indent, " ") & "(" & i & ") = " & DebugType(v(i), indent)
            Next

            Return retv
        End Function

        ''' <summary>
        ''' Helper function for DebugValue.
        ''' </summary>
        ''' <param name="v">Match object that should be rendered.</param>
        ''' <param name="indent"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function DumpMatch(ByVal v As Match, ByVal indent As Integer) As String
            Dim retv As String
            Dim i As Integer

            retv = DumpTypeName(v)
            retv = retv & vbCrLf & Strings.StrDup(indent, " ") & "FirstIndex = " & v.Index & _
            vbCrLf & Strings.StrDup(indent, " ") & "Length = " & HTMLEncode(v.Length) & _
            vbCrLf & Strings.StrDup(indent, " ") & "Value = " & DebugType(v.Value, indent) & _
            vbCrLf & Strings.StrDup(indent, " ") & "Captures.Count = " & v.Captures.Count

            For i = 0 To v.Captures.Count - 1
                Dim capture As Text.RegularExpressions.Capture = v.Captures(i)
                retv = retv & vbCrLf & Strings.StrDup(indent + 4, " ") & "(" & i & ") = " & HTMLEncode(capture.ToString)
            Next

            retv = retv & vbCrLf & Strings.StrDup(indent, " ") & "Groups"
            For i = 0 To v.Groups.Count
                Dim grp As Text.RegularExpressions.Group
                grp = v.Groups(i)
                retv = retv & vbCrLf & Strings.StrDup(indent + 4, " ") & "(" & i & ") = " & HTMLEncode(grp.Value)
            Next

            Return retv
        End Function

        ''' <summary>
        ''' Helper function for DebugValue.
        ''' </summary>
        ''' <param name="v">Collection object that should be rendered.</param>
        ''' <param name="indent"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function DumpCollection(ByVal v As Object, ByVal indent As Integer) As String

            Dim retv As String = DumpTypeName(v)
            retv = retv & vbCrLf & Strings.StrDup(indent, " ") & "Count = " & v.Count

            For i As Integer = 0 To v.Count - 1
                retv = retv & vbCrLf & Strings.StrDup(indent + 4, " ") & "(" & i & ") = " & DebugType(v(i), indent + 8)
            Next

            Return retv
        End Function

        ''' <summary>
        ''' Helper function for DebugValue.
        ''' </summary>
        ''' <param name="v">Array that should be rendered.</param>
        ''' <param name="indent"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function DumpArray(ByVal v As Object, ByVal indent As Integer) As String

            Dim retv As String = DumpTypeName(v)
            retv = Replace(retv, "()", "(" & LBound(v) & ".." & UBound(v) & ")")

            For i As Integer = LBound(v) To UBound(v)
                retv = retv & vbCrLf & Strings.StrDup(indent + 4, " ") & "(" & i & ") = " & DebugType(v(i), indent + 8)
            Next

            DumpArray = retv
        End Function

        Public Shared Function DumpArrayList(ByVal v As ArrayList, ByVal indent As Integer) As String

            Dim retv As String = DumpTypeName(v)
            retv = Replace(retv, "()", "(0.." & v.Count & ")")

            For i As Integer = 0 To v.Count - 1
                retv = retv & vbCrLf & Strings.StrDup(indent + 4, " ") & "(" & i & ") = " & DebugType(v(i), indent + 8)
            Next

            Return retv
        End Function

        ''' <summary>
        ''' Helper function for DebugValue.
        ''' </summary>
        ''' <param name="dt">DataTable that should be rendered.</param>
        ''' <param name="indent"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function DumpDataTable(ByVal dt As DataTable, ByVal indent As Integer) As String
            Dim sb As New StringBuilder
            Dim i As Integer

            sb.Append("DataTable")

            sb.Append(DumpTypeName(dt))
            sb.Append("<br />TableName=" & HTMLEncode(dt.TableName))
            sb.Append("<br />Rows.Count=" & dt.Rows.Count)

            ' Create table header
            sb.Append("<table border=1><tr><th>#</th>")
            For i = 0 To dt.Columns.Count - 1
                sb.Append("<th style=""FONT-FAMILY: Arial, geneva, Helvetica, Verdana; FONT-SIZE: 10px;"">" & HTMLEncode(dt.Columns(i).ColumnName) & "<br />" & HTMLEncode(dt.Columns(i).DataType.ToString) & "</th>")
            Next
            sb.Append("</tr>")

            ' Add rows
            Dim cnt As Integer = 0
            For Each row As DataRow In dt.Rows
                cnt = cnt + 1
                sb.Append("<tr><td>" & cnt & "</td>")
                For i = 0 To dt.Columns.Count - 1
                    sb.Append("<td style=""FONT-SIZE: 10px;"">" & row(i).ToString & "</td>")
                Next
                sb.Append("</tr>")
            Next
            sb.Append("</table>")

            Return sb.ToString
        End Function

        ''' <summary>
        ''' Outputs a readable HTML representation of a simple variable or object.
        ''' </summary>
        ''' <param name="v">Variable to render.</param>
        ''' <param name="name">Description or caption for the output.</param>
        ''' <remarks></remarks>
        Public Shared Sub DebugValue(ByVal v As Object, ByVal name As Object)
            Dim tablestyle, trstyle, tdstyle, thstyle As String
            Dim t As Object

            tablestyle = "BACKGROUND-COLOR: #ffffff; BORDER-TOP: #000000 1px solid; BORDER-BOTTOM: #000000 1px solid; BORDER-RIGHT: #000000 1px solid; BORDER-LEFT: #000000 1px solid;"
            trstyle = ""
            thstyle = _
                "BACKGROUND-COLOR: #aa0000;" & _
                "COLOR: #ffff00;" & _
                "FONT-FAMILY: Arial, geneva, Helvetica, Verdana" & _
                "PADDING-RIGHT: 2px;" & _
                "PADDING-LEFT: 2px;" & _
                "FONT-SIZE: 12px;" & _
                "PADDING-BOTTOM: 2px;" & _
                "PADDING-TOP: 2px;" & _
                "BORDER-BOTTOM: #000000 1px solid;" & _
                "BORDER-TOP: #000000 1px solid;" & _
                "BORDER-LEFT: #000000 1px solid;" & _
                "BORDER-RIGHT: #000000 1px solid;" & _
                "FONT-FAMILY: Arial, geneva, Helvetica, Verdana"
            tdstyle = _
                "BACKGROUND-COLOR: #ffffff;" & _
                "COLOR: #00000;" & _
                "FONT-FAMILY: Arial, geneva, Helvetica, Verdana;" & _
                "VERTICAL-ALIGN: TOP;"
            t = DebugType(v, 0)
            HttpContext.Current.Response.Write("<table style=""" & tablestyle & """>")
            HttpContext.Current.Response.Write("<tr style=""" & trstyle & """><th style=""" & thstyle & """ colspan=2>DEBUG INFORMATION</th></tr>")
            HttpContext.Current.Response.Write("<tr style=""" & trstyle & """><td style=""" & tdstyle & "TEXT-ALIGN: RIGHT;BACKGROUND-COLOR: #ffcccc;"">Name:</td><td style=""" & tdstyle & """>" & HTMLEncode(name) & "</td></tr>")
            HttpContext.Current.Response.Write("<tr style=""" & trstyle & """><td style=""" & tdstyle & "TEXT-ALIGN: RIGHT;BACKGROUND-COLOR: #ffcccc;"">Typename:</td><td style=""" & tdstyle & """>" & TypeName(v) & "</td></tr>")
            HttpContext.Current.Response.Write("<tr style=""" & trstyle & """><td style=""" & tdstyle & "TEXT-ALIGN: RIGHT;BACKGROUND-COLOR: #ffcccc;"">Value:</td><td style=""" & tdstyle & """><pre><font size=1 face=verdana>" & t & "</font></pre></td></tr>")
            HttpContext.Current.Response.Write("</table>")
            HttpContext.Current.Response.Flush()
        End Sub

        ''' <summary>
        ''' Helper function for DebugValue.
        ''' </summary>
        ''' <param name="v">Variable that should be rendered.</param>
        ''' <param name="indent"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function DebugType(ByVal v As Object, ByVal indent As Object) As Object
            Dim retv As Object

            Select Case TypeName(v)
                Case "HttpRequest"
                    retv = DumpRequest(v, indent)
                Case "IStringList"
                    retv = DumpStringList(v, indent)
                Case "ArrayList"
                    retv = DumpArrayList(v, indent)
                Case "Match", "IMatch2"
                    retv = DumpMatch(v, indent)
                Case "MatchCollection", "IMatchCollection2"
                    retv = DumpCollection(v, indent)
                    retv = retv & vbCrLf & Strings.StrDup(indent, " ") & vbCrLf & "<a target=""_blank"" href=""http://msdn2.microsoft.com/en-us/library/f97kw5ka.aspx"">Click here for pattern help</a>"
                Case "IRequestDictionary"
                    retv = DumpRequestDictionary(v, indent)
                Case "Dictionary"
                    retv = DumpDictionaryBranch(v, indent)
                Case "Hashtable", "SortedList", "ExpDictionary", "HttpValueCollection"
                    retv = DumpNetDictionaryBranch(v, indent)
                Case "Single", "Double", "Decimal"
                    retv = DumpTypeName(v) & " " & HTMLEncode(v)
                Case "Boolean"
                    retv = IIf(v, "True", "False")
                    If retv <> CStr(v) Then retv = retv & " (" & HTMLEncode(v) & ")"
                    retv = DumpTypeName(v) & " " & retv
                Case "Integer", "Long"
                    retv = DumpTypeName(v) & " " & v & " (0x" & Hex(v) & ")"
                Case "Date"
                    retv = DumpTypeName(v) & " " & HTMLEncode(v) & " (" & ExpanditLib2.CDblEx(v) & ")"
                Case "String"
                    retv = DumpTypeName(v) & " <span style=""background-color: #ffff99"">" & HTMLEncode(v) & "</span>"
                Case "DBNull", "Nothing"
                    retv = DumpTypeName(v)
                Case "DataTable"
                    retv = DumpDataTable(v, indent)
                Case Else
                    If TypeName(v).ToString.EndsWith("()") Then
                        retv = DumpArray(v, indent)
                    ElseIf TypeName(v).ToString.StartsWith("Dictionary(") Then
                        retv = DumpNetDictionaryBranch(v, indent)
                    Else
                        retv = "Unable to debug this type of data (" & TypeName(v) & ")"
                    End If
            End Select

            DebugType = retv
        End Function

        ''' <summary>
        ''' Outputs a readable HTML representation of an array.
        ''' </summary>
        ''' <param name="TheArray">Array to output.</param>
        ''' <returns></returns>
        ''' <remarks>This function is very useful when debugging the contents of arrays.</remarks>
        Private Shared Function DebugPrintArray(ByVal TheArray As Object) As String
            Dim i, j As Integer
            Dim retval As String = ""

            For i = LBound(TheArray, 1) To UBound(TheArray, 1)
                For j = LBound(TheArray, 2) To UBound(TheArray, 2)
                    retval = retval & TheArray(i, j) & vbTab
                Next
                retval = retval & vbCrLf
            Next
            Return retval
        End Function

        ''' <summary>
        ''' Outputs a sql statement with limited colorcoding and formatting to
        ''' make the statement more readable
        ''' </summary>
        ''' <param name="sql">Sql string</param>
        ''' <remarks>This function is useful when debugging sql statements.</remarks>
        Public Shared Sub DebugPrintSql(ByVal sql As Object)

            Const KEYWORDMATCH As Integer = 1
            Const STATEMENTMATCH As Integer = 2
            Const BREAKMATCH As Integer = 3
            Const STRINGMATCH As Integer = 5
            Const SUBQUERYMATCH As Integer = 7
            Const LEFTPARMATCH As Integer = 10
            Const RIGHTPARMATCH As Integer = 11

            Const INDENTPXL As Integer = 30

            Dim r As Regex
            Dim mc As MatchCollection
            Dim retv As String
            Dim m As Match
            Dim i As Integer
            Dim v As String
            Dim nv As String
            Dim displacement, indent As Integer
            Dim nesting As ExpDictionary
            Dim tmp As Integer

            retv = sql

            retv = Replace(retv, "crow", "cr''ow")

            ' filter out double white spaces
            retv = Replace(retv, Chr(9), " ")
            retv = Replace(retv, Chr(10), " ")
            retv = Replace(retv, Chr(13), " ")
            While InStr(1, retv, "  ") > 0
                retv = Replace(retv, "  ", " ")
            End While

            r = New Regex("\b(BY|ASC|DESC|AS|LIKE|IS|OR|AND|NOT|IN|TOP|ON|DISTINCT|NULL|SET)\b|\b(DELETE|DROP|SELECT|UPDATE|INSERT|ALTER)\b|\b(FROM|WHERE|HAVING|ORDER|(LEFT|RIGHT|INNER|OUTER|)\s+JOIN|UNION)\b|('([^']|'')*')|(([(])\s*\b(SELECT)\b)|([(])|([)])", RegexOptions.IgnoreCase)
            mc = r.Matches(retv)
            displacement = 0
            indent = 0
            nesting = New ExpDictionary
            For i = 0 To mc.Count - 1
                m = mc(i)
                Select Case True
                    Case m.Groups(KEYWORDMATCH).Value <> ""
                        v = m.Value
                        nv = "<span style=""color: #cc00cc; font-weight: normal"">" & v & "</span>"
                        retv = Left(retv, m.Index + displacement) & nv & Mid(retv, m.Index + displacement + 1 + Len(v))
                        displacement = displacement + Len(nv) - Len(v)

                    Case m.Groups(STATEMENTMATCH).Value <> ""
                        v = m.Value
                        nv = "<span style=""font-weight: bold; color: #0000ff"">" & v & "</span>"
                        retv = Left(retv, m.Index + displacement) & nv & Mid(retv, m.Index + displacement + 1 + Len(v))
                        displacement = displacement + Len(nv) - Len(v)

                    Case m.Groups(BREAKMATCH).Value <> ""
                        v = m.Value
                        nv = "<br /><span style=""font-weight: bold; color: #0000ff"">" & v & "</span>"
                        retv = Left(retv, m.Index + displacement) & nv & Mid(retv, m.Index + displacement + 1 + Len(v))
                        displacement = displacement + Len(nv) - Len(v)

                    Case m.Groups(STRINGMATCH).Value <> ""
                        v = m.Value
                        nv = "<span style=""color: #009900"">" & v & "</span>"
                        retv = Left(retv, m.Index + displacement) & nv & Mid(retv, m.Index + displacement + 1 + Len(v))
                        displacement = displacement + Len(nv) - Len(v)

                    Case m.Groups(SUBQUERYMATCH).Value <> ""
                        nesting("k" & nesting.Count + 1) = -100
                        nesting("k" & nesting.Count + 1) = 1
                        v = m.Value
                        nv = "<br /><span style=""color: #999999; font-weight: bold"">" & m.Groups(SUBQUERYMATCH + 1).Value & "</span>"
                        indent = indent + INDENTPXL
                        nv = nv & "<p style=""margin-top:0; margin-left: " & indent & """><span style=""font-weight: bold; color: #0000ff"">" & m.Groups(SUBQUERYMATCH + 2).Value & "</span>"
                        retv = Left(retv, m.Index + displacement) & nv & Mid(retv, m.Index + displacement + 1 + Len(v))
                        displacement = displacement + Len(nv) - Len(v)

                    Case m.Groups(LEFTPARMATCH).Value <> ""
                        nesting("k" & nesting.Count + 1) = 1

                    Case m.Groups(RIGHTPARMATCH).Value <> ""
                        tmp = nesting.Count
                        nesting("k" & tmp) = nesting("k" & tmp) - 1

                        If nesting("k" & tmp) = 0 Then
                            nesting.Remove("k" & tmp)

                            tmp = nesting.Count
                            If nesting("k" & tmp) < 0 Then
                                nesting.Remove("k" & tmp)
                                indent = indent - INDENTPXL
                                v = m.Value
                                nv = "<p style=""margin-top:0; margin-left: " & indent & """><span style=""color: #999999; font-weight: bold"">" & v & "</span>"
                                retv = Left(retv, m.Index + displacement) & nv & Mid(retv, m.Index + displacement + 1 + Len(v))
                                displacement = displacement + Len(nv) - Len(v)
                            End If
                        End If

                End Select
            Next

            r = Nothing

            retv = "<table><tr><td style=""font-family: verdana; font-size: 11px; background-color: #ffffe0; padding: 10; border: 1 solid black"">" & retv & "</td></tr></table>"
            HttpContext.Current.Response.Write(retv)
            HttpContext.Current.Response.Flush()

        End Sub

    End Class

End Namespace
