﻿
Public Class ExpanditLib2

    Private Shared RoundingMode As MidpointRounding

    Public Shared Function IsNull(ByVal v As Object) As Boolean
        Return (v Is DBNull.Value)
    End Function

    Public Shared Function Abs(ByVal v As Object) As Object
        Return Math.Abs(v)
    End Function

    ''' <summary>
    ''' Makes a safe conversion to a string.
    ''' </summary>
    ''' <param name="v">Value to convert.</param>
    ''' <returns>String</returns>
    ''' <remarks>
    ''' Returns the string representing v. If v cannot be converted to a string,
    ''' the function returns the empty string.
    ''' </remarks>
    Public Shared Function CStrEx(ByVal v As Object) As String
        If IsNothing(v) Then
            Return ""
        End If
        Try
            Return v.ToString
        Catch
            Return ""
        End Try
    End Function

    ''' <summary>
    ''' Makes a safe conversion to a long.
    ''' </summary>
    ''' <param name="v">Value to convert.</param>
    ''' <returns>Int32</returns>
    ''' <remarks>
    ''' Returns the long representing v. If v cannot be converted to a long the function returns 0 (zero).
    ''' </remarks>
    Public Shared Function CLngEx(ByVal v As Object) As Int32
        Dim retv As Int32 = 0
        Int32.TryParse(v, retv)
        Return retv
    End Function

    ''' <summary>
    ''' Makes a safe conversion to an Integer.
    ''' </summary>
    ''' <param name="v">Value to convert.</param>
    ''' <returns>Integer</returns>
    ''' <remarks>
    ''' Returns the integer representing v. If v cannot be converted to a integer the function returns 0 (zero).
    ''' </remarks>
    Public Shared Function CIntEx(ByVal v As Object) As Integer
        Dim retv As Integer = 0
        Integer.TryParse(v, retv)
        Return retv
    End Function

    ''' <summary>
    ''' Makes a safe conversion to a Decimal.
    ''' </summary>
    ''' <param name="v">Value to convert.</param>
    ''' <returns>Decimal</returns>
    ''' <remarks>
    ''' Returns the Decimal representing v. If v cannot be converted to a Decimal the function returns 0 (zero).
    ''' </remarks>
    Public Shared Function CDblEx(ByVal v As Object) As Decimal
        If IsNothing(v) Then
            Return 0D
        End If
        Try
            Return CDec(v)
        Catch
            Return 0D
        End Try
    End Function

    Public Shared Function ConvertToDbl(ByVal v As Object) As Double
        If IsNothing(v) Then
            Return 0.0
        End If
        Try
            Return CDbl(v)
        Catch
            Return 0.0
        End Try
    End Function

    ''' <summary>
    ''' Makes a safe conversion to a boolean.
    ''' </summary>
    ''' <param name="v">Value to convert.</param>
    ''' <returns>Boolean</returns>
    ''' <remarks>
    ''' Returns the boolean representing v. If v cannot be converted to a boolean the function returns false.
    ''' </remarks>
    Public Shared Function CBoolEx(ByVal v As Object) As Boolean
        If IsNothing(v) Then
            Return False
        End If
        Try
            Return CBool(v)
        Catch
            Return False
        End Try
    End Function

    ''' <summary>
    ''' Makes a safe conversion to a date.
    ''' </summary>
    ''' <param name="v">Value to convert.</param>
    ''' <returns>Date</returns>
    ''' <remarks>
    ''' Returns the date representing v. If v cannot be converted to a date the function returns CDate(0).
    ''' </remarks>
    Public Shared Function CDateEx(ByVal v As Object) As Date
        If IsNothing(v) Then
            Return Date.MinValue
        End If
        Try
            Return CDate(v)
        Catch
            Return Date.MinValue
        End Try
    End Function

    ''' <summary>
    ''' Converts a value to a float.
    ''' </summary>
    ''' <param name="s">Value to convert.</param>
    ''' <returns>Decimal</returns>
    ''' <remarks>The function returns the float represented by the string.</remarks>
    Public Shared Function String2Float(ByVal s As Object) As Decimal
        Dim f As Object

        If Len(s) > 9 Then s = "1"
        s = Replace(s, ",", Mid(1.1, 2, 1))
        s = Replace(s, ".", Mid(1.1, 2, 1))
        f = 0
        On Error Resume Next
        f = CDec(s)
        On Error GoTo 0
        String2Float = f
    End Function

    ''' <summary>
    ''' Left pads a string to a specified width.
    ''' </summary>
    ''' <param name="s">String to pad.</param>
    ''' <param name="ch">Character used for padding.</param>
    ''' <param name="n">Width of resulting string.</param>
    ''' <returns>The string s padded to length n using the character ch.</returns>
    ''' <remarks></remarks>
    Public Shared Function LPad(ByVal s As String, ByVal ch As Char, ByVal n As Integer) As String
        Return s.ToString.PadLeft(n, ch)
    End Function

    ''' <summary>
    ''' Right pads a string to a specified width.
    ''' </summary>
    ''' <param name="s">String to pad.</param>
    ''' <param name="ch">Character used for padding.</param>
    ''' <param name="n">Width of resulting string.</param>
    ''' <returns>The string s padded to length n using the character ch.</returns>
    ''' <remarks></remarks>
    Public Shared Function RPad(ByVal s As String, ByVal ch As Char, ByVal n As Integer) As String
        Return s.ToString.PadRight(n, ch)
    End Function

    ''' <summary>
    ''' Encodes a string for use in a SQL statement.
    ''' </summary>
    ''' <param name="s">String to encode.</param>
    ''' <returns>The encoded string. Null values are encoded as NULL.</returns>
    ''' <remarks></remarks>
    Public Shared Function SafeString(ByVal s As Object, Optional ByVal isPreserveEmptyString As Boolean = False) As String
        s = CStrEx(s)
        If s = "" Then
            Return IIf(isPreserveEmptyString, "N'" & Replace(s, "'", "''") & "'", "NULL")
        Else
            Return "N'" & Replace(s, "'", "''") & "'"
        End If
    End Function

    ''' <summary>
    ''' This function is used to generate an equal part of a sql statement. The function handles NULL values.
    ''' </summary>
    ''' <param name="v">String to encode.</param>
    ''' <returns>String</returns>
    ''' <remarks>
    ''' If v is null (determined by SafeString) the function returns the string "IS NULL"
    ''' If v is value, the function returns the string "= 'value'"
    ''' The return value is used in SQL statements where the value can be null.
    '''</remarks>
    Public Shared Function SafeEqualString(ByVal v As Object) As String
        v = SafeString(v)
        If CStrEx(v) = "NULL" Then
            Return "IS NULL"
        End If
        Return "= " & v
    End Function

    ''' <summary>
    ''' Encodes a value to an unsigned float for use in a SQL statement.
    ''' </summary>
    ''' <param name="f">Value to encode.</param>
    ''' <returns>An encoded String</returns>
    ''' <remarks>
    ''' The function returns the encoded string. If the passed parameter is a negative number,
    ''' it will be converted to a positive number.
    ''' </remarks>
    Public Shared Function SafeUFloat(ByVal f As Object) As String
        Dim s As String

        If IsNull(f) Then f = 0
        f = Math.Round(f, 10)
        f = Replace(f, ",", Mid(1.1, 2, 1))
        f = Replace(f, ".", Mid(1.1, 2, 1))
        On Error Resume Next
        f = Abs(CDec(f))
        If Err.Number <> 0 Then f = 0
        On Error GoTo 0
        s = CStr(Int(f)) & Mid(1.1, 2, 1) & Mid(CStr(Math.Round(f - Int(f), 10)), 3)
        If Right(s, 1) = "." Or Right(s, 1) = "," Then s = s & "0"
        Return s
    End Function

    ''' <summary>
    ''' Encodes a value to a float for use in a SQL statement.
    ''' </summary>
    ''' <param name="f">Value to encode.</param>
    ''' <returns>An encoded String</returns>
    ''' <remarks></remarks>
    Public Shared Function SafeFloat(ByVal f As Object) As String
        Dim s As String

        If IsNull(f) Then f = 0
        f = RoundEx(f, 10)
        s = CStr(f)
        ' Get the commaseparator and replace that by dot (.)
        ' This is done to make ADO understand the comma.
        Dim commaSeparator As String
        commaSeparator = Mid(CDblEx(11 / 10), 2, 1)
        Return Replace(s, commaSeparator, ".")
    End Function

    ''' <summary>
    ''' Encodes a value to a long for use in a SQL statement.
    ''' </summary>
    ''' <param name="v">Value to encode.</param>
    ''' <returns>Integer</returns>
    ''' <remarks></remarks>
    Public Shared Function SafeLong(ByVal v As Object) As Integer
        Return CLngEx(v)
    End Function

    ''' <summary>
    ''' Encodes a value to an unsigned long for use in a SQL statement.
    ''' </summary>
    ''' <param name="v">Value to encode.</param>
    ''' <returns>Integer</returns>
    ''' <remarks>
    ''' The function returns an Integer. If the passed parameter is a negative number,
    ''' it will be converted to a positive number.
    ''' </remarks>
    Public Shared Function SafeULong(ByVal v As Object) As Integer
        Dim retval As Integer

        retval = SafeLong(v)
        If retval < 0 Then retval = 0
        SafeULong = retval
    End Function

    ''' <summary>
    ''' Returns Zero (0) as Integer
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetZeroDate() As Integer
        Return 0
    End Function

    ''' <summary>
    ''' Encodes a value to a date for use in a SQL statement.
    ''' </summary>
    ''' <param name="d">Value to encode.</param>
    ''' <returns>The encoded string</returns>
    ''' <remarks>
    ''' The function returns the encoded string. This function returns different encodings 
    ''' based on the actual database engine. 
    '''</remarks>
    Public Shared Function SafeDatetime(ByVal d As Object) As Object
        Dim retv As String
        Dim dateval As Object

        dateval = DBNull.Value
        On Error Resume Next
        dateval = CDate(d)
        On Error GoTo 0

        If Not IsNull(dateval) Then
            retv = "{ts '" & LPad(Year(dateval), "0", 4) & "-" & LPad(Month(dateval), "0", 2) & "-" & LPad(Day(dateval), "0", 2) & " " & LPad(Hour(dateval), "0", 2) & ":" & LPad(Minute(dateval), "0", 2) & ":" & LPad(Second(dateval), "0", 2) & "'}"
        Else
            retv = "NULL"
        End If
        SafeDatetime = retv
    End Function

    ''' <summary>
    ''' This function rounds "value" to "decimal" number of decimals.
    ''' </summary>
    ''' <param name="value">The value to do rounding on.</param>
    ''' <param name="decimals">Number of decimals to round value to.</param>
    ''' <returns>Decimal</returns>
    ''' <remarks>Returns the rounded value</remarks>
    Public Shared Function RoundEx(ByVal value As Decimal, ByVal decimals As Integer) As Decimal
        Return Math.Round(value, decimals, RoundingMode)
    End Function

    Public Shared Function RoundEx(ByVal value As Double, ByVal decimals As Integer) As Double
        Return Math.Round(value, decimals, RoundingMode)
    End Function

End Class