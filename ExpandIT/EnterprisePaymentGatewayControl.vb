Imports Microsoft.VisualBasic
Imports ExpandIT.EISClass
Imports ExpandIT.ExpandITLib
Imports System.Configuration.ConfigurationManager

Namespace ExpandIT.SimpleUIControls

    'AM2010041301 - ENTERPRISE PAYMENT GATEWAY - Start
    Public Class EnterprisePaymentGatewayControl
        Inherits GenericPaymentControl
        Implements INamingContainer

        Protected Overrides Sub Render(ByVal writer As System.Web.UI.HtmlTextWriter)
            If ShowEEPGErrorMessage() <> "" Then
                Me.lblErrorText.Text = ShowEEPGErrorMessage()
                Me.trError.Visible = True
            End If
            MyBase.Render(writer)
        End Sub

        Protected Overridable Function ShowEEPGErrorMessage() As String

            'Here is where the error message gets handled
            If HttpContext.Current.Request("EEPG_ErrorString") <> "" Then
                Return HttpContext.GetGlobalResourceObject("Language", "LABEL_EEPG") & "&nbsp;" & HttpContext.GetGlobalResourceObject("Language", "LABEL_ERROR") & _
                    "<br />" & HttpContext.Current.Request("EEPG_ErrorString")
            End If

            Return ""

        End Function

    End Class
    'AM2010041301 - ENTERPRISE PAYMENT GATEWAY - End

End Namespace