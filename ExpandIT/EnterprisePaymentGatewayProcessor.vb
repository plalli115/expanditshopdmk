﻿'AM2010041301 - ENTERPRISE PAYMENT GATEWAY - Start
Imports Microsoft.VisualBasic
Imports System.Configuration.ConfigurationManager
Imports System.Net
Imports System.IO
Imports ExpandIT.ExpandITLib
Imports EEPaymentManager.EEPM
Imports ExpandIT.EISClass

Namespace ExpandIT
    ''' <summary>
    ''' Handles the communication between the shop and the enterprise payment gateway
    ''' during integrated credit card payments.
    ''' The class also handles the validation of enterprise gateway responses.    
    ''' </summary>
    ''' <remarks></remarks>
    Public Class EnterprisePaymentGatewayProcessor

        'Variables needed to communicate with nsoftware. Taken from PayPalPro. 
        Dim errString As String = String.Empty
        ' Create an instance of the class that will handle the payment process.
        Public paymentProcessor As New EEGateway
        Public paymentValidator As New EECCValidator

        'Authentication values
        Private URL As String
        Private UserName As String
        Private Password As String
        Private Signature As String
        Private SaleAuth As String
        Private GatewayCode As String
        Private AuthorizeOverageType As Integer
        Private AuthorizeOverageAmount As Double
        Private CryptoPassPhrase As String
        Private CryptoInitVector As String
        Private CryptoSaltValue As String
        Private CryptoPasswordIterations As String

        Public Property EEPG_URL() As String
            Get
                Return CStrEx(URL)
            End Get
            Set(ByVal value As String)
                URL = CStrEx(value)
            End Set
        End Property

        Public Property EEPG_UserName() As String
            Get
                Return CStrEx(UserName)
            End Get
            Set(ByVal value As String)
                UserName = CStrEx(value)
            End Set
        End Property

        Public Property EEPG_Password() As String
            Get
                Return CStrEx(Password)
            End Get
            Set(ByVal value As String)
                Password = CStrEx(value)
            End Set
        End Property

        Public Property EEPG_Signature() As String
            Get
                Return CStrEx(Signature)
            End Get
            Set(ByVal value As String)
                Signature = CStrEx(value)
            End Set
        End Property

        Public Property EEPG_SaleAuth() As Integer
            Get
                Return CIntEx(SaleAuth)
            End Get
            Set(ByVal value As Integer)
                SaleAuth = CIntEx(value)
            End Set
        End Property

        Public Property EEPG_GatewayCode() As String
            Get
                Return CStrEx(GatewayCode)
            End Get
            Set(ByVal value As String)
                GatewayCode = CStrEx(value)
            End Set
        End Property

        Public Property EEPG_AuthorizeOverageType() As Integer
            Get
                Return CIntEx(AuthorizeOverageType)
            End Get
            Set(ByVal value As Integer)
                AuthorizeOverageType = CIntEx(value)
            End Set
        End Property

        Public Property EEPG_AuthorizeOverageAmount() As Double
            Get
                Return CDblEx(AuthorizeOverageAmount)
            End Get
            Set(ByVal value As Double)
                AuthorizeOverageAmount = CDblEx(value)
            End Set
        End Property

        Public Property EEPG_CryptoPassPhrase() As String
            Get
                Return CStrEx(CryptoPassPhrase)
            End Get
            Set(ByVal value As String)
                CryptoPassPhrase = CStrEx(value)
            End Set
        End Property


        Public Property EEPG_CryptoInitVector() As String
            Get
                Return CStrEx(CryptoInitVector)
            End Get
            Set(ByVal value As String)
                CryptoInitVector = CStrEx(value)
            End Set
        End Property

        Public Property EEPG_CryptoSaltValue() As String
            Get
                Return CStrEx(CryptoSaltValue)
            End Get
            Set(ByVal value As String)
                CryptoSaltValue = CStrEx(value)
            End Set
        End Property

        Public Property EEPG_CryptoPasswordIterations() As String
            Get
                Return CStrEx(CryptoPasswordIterations)
            End Get
            Set(ByVal value As String)
                CryptoPasswordIterations = CStrEx(value)
            End Set
        End Property

        ''' <summary>
        ''' The class's public entry point.
        ''' Issues a request by calling the EEPG Gateway. 
        ''' Sends the response to validation.
        ''' </summary>
        ''' <param name="ccn"></param>
        ''' <param name="expdate"></param>
        ''' <param name="ccvNr"></param>
        ''' <param name="OrderDict"></param>
        ''' <param name="FName"></param>
        ''' <param name="LName"></param>
        ''' <param name="request"></param>
        ''' <returns>True if response passes validation</returns>
        ''' <remarks></remarks>
        ''' AM2010091001 - ONLINE PAYMENTS - Start
        Public Function EEPGProcessPayment(ByVal paymentTable As ExpDictionary, ByVal ccn As String, ByVal expdate As String, _
        ByVal ccvNr As String, ByVal OrderDict As ExpDictionary, ByVal FName As String, _
        ByVal LName As String, ByVal request As System.Web.HttpRequest, Optional ByVal _
        isLedgerPayment As Boolean = False, Optional ByVal chargedAmount As Double = 0) As String
            'AM2010091001 - ONLINE PAYMENTS - End
            'Getting the gateway information from the EESetup from Navision
            Dim dictGatewayConfig As ExpDictionary
            Dim dictGatewayAdditionalKeys As ExpDictionary
            Dim addressNo As String
            Dim address1 As String
            Dim transactionAmount As Double = 0
            Dim cardType As String = ""
            Dim expMonth As String = ""
            Dim expYear As String = ""
            Dim encryptedCard As String = ""
            Dim nonNumericCharacters As New System.Text.RegularExpressions.Regex("[^0-9]")
            'JA2010092201 - RECURRING ORDERS - START
            Dim isModifyRecurrency As Boolean
            Dim intEncryptCounter As Integer = 0

            If CBoolEx(AppSettings("RECURRING_ORDERS_SHOW")) Then
                isModifyRecurrency = CBoolEx(OrderDict("ModifiedRecurrency"))
            End If
            'JA2010092201 - RECURRING ORDERS - END

            ccn = ccn.Trim()
            ccn = nonNumericCharacters.Replace(ccn, String.Empty)

            'AM2011051901 - ENTERPRISE CONFIGURATION KEYS - Start
            'Dim sql As String = "SELECT CCGatewayCode,CCGatewayUserLive,CCGatewayPasswordLive,CCGatewayUserTest,CCGatewayPasswordTest,CCAuthorizeOverageType,CCAuthorizeOverageAmount,CCGatewayBusinessModel,CCSimulationMode,CCCryptoPassPhrase,CCCryptoInitVector,CCCryptoSaltValue,CCCryptoPasswordIterations FROM EESetup"
            'dictGatewayConfig = Sql2Dictionary(sql)
            Try
                Dim sql As String = ""
                Dim pageobj As ExpandIT.Page = CType(System.Web.HttpContext.Current.Handler, ExpandIT.Page)

                cardType = CStrEx(paymentValidator.GetCardType(ccn))
                If Not validateCardType(cardType, pageobj) Then
                    Return Resources.Language.CARD_TYPE_NOT_APPROVED_FOR_USE
                End If
                expMonth = CStrEx(Mid(expdate, 1, 2))
                expYear = CStrEx("20" & Mid(expdate, 3, 2))
                'JA2010092201 - RECURRING ORDERS - START
                If Not CBoolEx(AppSettings("RECURRING_ORDERS_SHOW")) OrElse Not isModifyRecurrency Then
                    'JA2010092201 - RECURRING ORDERS - END
                    'AM2010091001 - ONLINE PAYMENTS - Start
                    If CBoolEx(AppSettings("EXPANDIT_US_USE_EEPG_ONLINE_PAYMENTS")) And isLedgerPayment Then
                        EEPG_SaleAuth = 0
                    Else
                        EEPG_SaleAuth = CIntEx(pageobj.eis.getEnterpriseConfigurationValue("CCGatewayBusinessModel"))
                    End If
                    'AM2010091001 - ONLINE PAYMENTS - End
                    EEPG_GatewayCode = CStrEx(pageobj.eis.getEnterpriseConfigurationValue("CCGatewayCode"))
                    If CBoolEx(pageobj.eis.getEnterpriseConfigurationValue("CCSimulationMode")) Then
                        EEPG_UserName = CStrEx(pageobj.eis.getEnterpriseConfigurationValue("CCGatewayUserTest"))
                        EEPG_Password = CStrEx(pageobj.eis.getEnterpriseConfigurationValue("CCGatewayPasswordTest"))
                        EEPG_URL = CStrEx(getSingleValueDB("SELECT TestingWebsite FROM EECCGateway WHERE Code=" & SafeString(EEPG_GatewayCode)))
                    Else
                        EEPG_UserName = CStrEx(pageobj.eis.getEnterpriseConfigurationValue("CCGatewayUserLive"))
                        EEPG_Password = CStrEx(pageobj.eis.getEnterpriseConfigurationValue("CCGatewayPasswordLive"))
                        EEPG_URL = CStrEx(getSingleValueDB("SELECT Website FROM EECCGateway WHERE Code=" & SafeString(EEPG_GatewayCode)))
                    End If

                    EEPG_AuthorizeOverageType = CIntEx(pageobj.eis.getEnterpriseConfigurationValue("CCAuthorizeOverageType"))
                    EEPG_AuthorizeOverageAmount = CDblEx(pageobj.eis.getEnterpriseConfigurationValue("CCAuthorizeOverageAmount"))
                    'JA2010092201 - RECURRING ORDERS - START
                End If
                'JA2010092201 - RECURRING ORDERS - END
                EEPG_CryptoPassPhrase = CStrEx(pageobj.eis.getEnterpriseConfigurationValue("CCCryptoPassPhrase"))
                EEPG_CryptoInitVector = CStrEx(pageobj.eis.getEnterpriseConfigurationValue("CCCryptoInitVector"))
                EEPG_CryptoSaltValue = CStrEx(pageobj.eis.getEnterpriseConfigurationValue("CCCryptoSaltValue"))
                EEPG_CryptoPasswordIterations = CStrEx(pageobj.eis.getEnterpriseConfigurationValue("CCCryptoPasswordIterations"))


                'Adding the gateway and credit card information.
                Try
                    'Encrypt the credit card before processing, to make sure there are no errors encripting it
                    While intEncryptCounter < 5
                        intEncryptCounter += 1
                        encryptedCard = Me.encryptCard(ccn, expMonth, expYear)
                        If Not encryptedCard.StartsWith("ERRORENCRYPTING") Then Exit While
                    End While

                    If Not encryptedCard.StartsWith("ERRORENCRYPTING") Then
                        'JA2010092201 - RECURRING ORDERS - START
                        If Not CBoolEx(AppSettings("RECURRING_ORDERS_SHOW")) OrElse Not isModifyRecurrency Then
                            'JA2010092201 - RECURRING ORDERS - END
                            paymentProcessor.GatewayID = EEPG_GatewayCode
                            paymentProcessor.GatewayLogin = EEPG_UserName
                            paymentProcessor.GatewayPassword = EEPG_Password
                            paymentProcessor.GatewayURL = EEPG_URL

                            paymentProcessor.PaymentType = 0
                            paymentProcessor.AddNameValue("CCTYPE", cardType)
                            paymentProcessor.AddNameValue("CCNUMBER", CStrEx(ccn))
                            paymentProcessor.AddNameValue("CCEXPMONTH", expMonth)
                            paymentProcessor.AddNameValue("CCEXPYEAR", expYear)
                            paymentProcessor.AddNameValue("CCCCV", CStrEx(ccvNr))
                            paymentProcessor.AddNameValue("LNAME", LName)
                            paymentProcessor.AddNameValue("FNAME", FName)
                            paymentProcessor.AddNameValue("CUSTOMERNUMBER", CStrEx(OrderDict("UserGuid")))
                            paymentProcessor.AddNameValue("CURRENCY", CStrEx(OrderDict("CurrencyGuid")))

                            If CBoolEx(AppSettings("PAYMENT_EEPG_REQUIRE_ADDRESS_INFO")) Then
                                'JA2011030701 - PAYMENT TABLE - START
                                paymentProcessor.AddNameValue("ADDRESSNUMBER", CStrEx(paymentTable("PaymentAddress1No")))
                                paymentProcessor.AddNameValue("ADDRESS", CStrEx(paymentTable("PaymentAddress1St")))
                                paymentProcessor.AddNameValue("ADDRESS2", CStrEx(paymentTable("PaymentAddress2")))
                                paymentProcessor.AddNameValue("CITY", CStrEx(paymentTable("PaymentCity")))
                                paymentProcessor.AddNameValue("STATE", CStrEx(paymentTable("PaymentState")))
                                paymentProcessor.AddNameValue("COUNTRYCODE", CStrEx(paymentTable("PaymentCountry")))
                                paymentProcessor.AddNameValue("PHONE", CStrEx(paymentTable("PaymentPhone")))
                                paymentProcessor.AddNameValue("EMAIL", CStrEx(paymentTable("PaymentEmail")))
                                'JA2011030701 - PAYMENT TABLE - END
                            End If

                            If CBoolEx(AppSettings("PAYMENT_EEPG_REQUIRE_ZIP_CODE")) Then
                                'JA2011030701 - PAYMENT TABLE - START
                                'paymentProcessor.AddNameValue("ZIPCODE", CStrEx(OrderDict("CCZipCode")))
                                paymentProcessor.AddNameValue("ZIPCODE", CStrEx(paymentTable("PaymentZipCode")))
                                'JA2011030701 - PAYMENT TABLE - END
                            End If

                            'Adding the additional gateway keys

                            sql = "SELECT KeyCode, KeyValue FROM EECCGatewayAdditionalKeys WHERE GatewayCode=" & SafeString(EEPG_GatewayCode) & " AND ConfiguredGateway=" & SafeString(pageobj.eis.getEnterpriseConfigurationValue("ConfiguredGateway"))


                            If CBoolEx(pageobj.eis.getEnterpriseConfigurationValue("CCSimulationMode")) Then
                                sql &= " AND TestOnlyKey <> 1"
                            Else
                                sql &= " AND TestOnlyKey <> 2"
                            End If


                            If (EEPG_SaleAuth = 0) Then
                                sql &= " AND TransactionType IN (1,3)"
                            ElseIf (EEPG_SaleAuth = 1) Then
                                sql &= " AND TransactionType IN (1,2)"
                            Else
                                Return "Payment processor setup incorrect."
                            End If

                            dictGatewayAdditionalKeys = SQL2Dicts(sql)
                            If Not dictGatewayAdditionalKeys Is Nothing Then
                                For Each addGatewayKey As ExpDictionary In dictGatewayAdditionalKeys.Values
                                    paymentProcessor.AddNameValue(CStrEx(addGatewayKey("KeyCode")), CStrEx(addGatewayKey("KeyValue")))
                                Next
                            End If

                            'Calculating the amount to be authorized or charged
                            'AM2010091001 - ONLINE PAYMENTS - Start
                            If CBoolEx(AppSettings("EXPANDIT_US_USE_EEPG_ONLINE_PAYMENTS")) And isLedgerPayment Then
                                transactionAmount = chargedAmount
                            Else
                                transactionAmount = CDblEx(OrderDict("TotalInclTax"))
                            End If
                            'AM2010091001 - ONLINE PAYMENTS - End
                            If EEPG_AuthorizeOverageAmount > 0 Then
                                If EEPG_AuthorizeOverageType = 1 Then 'Percentage
                                    transactionAmount += transactionAmount * EEPG_AuthorizeOverageAmount / 100
                                ElseIf EEPG_AuthorizeOverageType = 2 Then 'Value
                                    transactionAmount += EEPG_AuthorizeOverageAmount
                                End If
                            End If
                            paymentProcessor.AddNameValue("TRANSACTIONAMOUNT", CStrEx(transactionAmount))
                            paymentProcessor.AddNameValue("TRANSACTIONDOCUMENTNUMBER", CStrEx(OrderDict("CustomerReference")))
                            paymentProcessor.AddNameValue("TRANSACTIONDESC", "WEB ORDER " & CStrEx(OrderDict("CustomerReference")))
                            'JA2010092201 - RECURRING ORDERS - START
                        End If
                        If CBoolEx(AppSettings("RECURRING_ORDERS_SHOW")) AndAlso isModifyRecurrency Then
                            'Processing the Authorization or Sale, depending on the SaleAuth value.
                            'encryptedCard = Me.encryptCard(ccn, expMonth, expYear)
                            Return "True*" & "ModifiedRecurrency" & "*" & CDblEx(OrderDict("TotalInclTax")) & "*" & encryptedCard & "*" & ccn & "*" & cardType
                            'JA2010092201 - RECURRING ORDERS - END
                        ElseIf (EEPG_SaleAuth = 0) Then
                            If paymentProcessor.DirectSale() Then
                                'encryptedCard = Me.encryptCard(ccn, expMonth, expYear)
                                Return "True*" & CStrEx(paymentProcessor.GetNameValue("TRANSACTIONID")) & "*" & transactionAmount & "*" & encryptedCard & "*" & ccn & "*" & cardType
                            Else
                                Return validateResponse(OrderDict)
                            End If
                        ElseIf (EEPG_SaleAuth = 1) Then
                            If paymentProcessor.Authorize() Then
                                'encryptedCard = Me.encryptCard(ccn, expMonth, expYear)
                                Return "True*" & CStrEx(paymentProcessor.GetNameValue("TRANSACTIONID")) & "*" & transactionAmount & "*" & encryptedCard & "*" & ccn & "*" & cardType
                            Else
                                Return validateResponse(OrderDict)
                            End If
                        Else
                            Return "Payment processor setup incorrect."
                        End If
                    Else
                        Return encryptedCard
                    End If
                Catch
                    Return Err.Number & ":" & Err.Description
                End Try

            Catch ex As Exception
            End Try
            'AM2011051901 - ENTERPRISE CONFIGURATION KEYS - End

            Return "Payment processor setup incorrect."

        End Function

        Public Overridable Function validateResponse(ByVal orderDict As ExpDictionary)
            'Getting the result and returning it to payment.aspx page
            Return CStrEx(paymentProcessor.ResponseCode) & ":" & CStrEx(paymentProcessor.ResponseDescription)
        End Function

        Public Function encryptCard(ByVal ccn As String, ByVal expmonth As String, ByVal expyear As String)
            Dim encryptObj As New EECryptography.EECrypto.EECrypto()
            Dim string2encrypt As String = ""
            Dim encryptedString As String = ""

            encryptObj.AddNameValue("!#PASSPHRASE", EEPG_CryptoPassPhrase)
            encryptObj.AddNameValue("!#INITIALIZATIONVECTOR", EEPG_CryptoInitVector)
            encryptObj.AddNameValue("!#SALTVALUE", EEPG_CryptoSaltValue)
            encryptObj.AddNameValue("!#PASSWORDITERATIONS", EEPG_CryptoPasswordIterations)
            string2encrypt = ccn & expmonth & expyear

            encryptedString = encryptObj.Encrypt(string2encrypt)
            If CStrEx(encryptedString) = "" Then
                FileLogger.log("Error Encrypting CC: ")
                FileLogger.log("  Following Values Supplied: ")
                FileLogger.log("  PASSPHRASE: " & EEPG_CryptoPassPhrase)
                FileLogger.log("  INITIALIZATIONVECTOR: " & EEPG_CryptoInitVector)
                FileLogger.log("  SALTVALUE: " & EEPG_CryptoSaltValue)
                FileLogger.log("  PASSWORDITERATIONS: " & EEPG_CryptoPasswordIterations)
                FileLogger.log("  encryptedString: IS BLANK.")

                Return "ERRORENCRYPTING: ER:" & CStrEx(encryptObj.ResponseCode) & "-" & CStrEx(encryptObj.ResponseDescription)
                'Return "ERRORENCRYPTING"
            Else
                Return encryptedString
            End If

        End Function

        Public Function validateCardType(ByVal cardType As Integer, ByVal pageobj As Object)
            Dim sql As String

            sql = "SELECT COUNT(*) FROM EECCApprovedCreditCardType WHERE GatewayCardTypeCode=" & cardType & " AND ApprovedForUse=1 AND ConfiguredGateway=" & SafeString(pageobj.eis.getEnterpriseConfigurationValue("ConfiguredGateway"))

            If CIntEx(getSingleValueDB(sql)) > 0 Then
                Return True
            End If

            Return False
        End Function

    End Class
    'AM2010041301 - ENTERPRISE PAYMENT GATEWAY - End
End Namespace

