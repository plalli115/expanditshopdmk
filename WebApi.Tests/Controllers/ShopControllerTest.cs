﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using WebApi.Controllers;
using WebApi.Models.shop;
using WebApi.Services.ShopService;

namespace WebApi.Tests.Controllers
{
    [TestClass]
    public class ShopControllerTest
    {
        [TestMethod]
        public void TestGetAll()
        {
            var shops = getShopController().GetAllShops();
            Assert.AreEqual(2, shops.Count);
        }

        private ShopController getShopController()
        {
            var shopService = new Mock<IShopService>();
            var item = new Shop {CompanyName = "name1"};
            var item2 = new Shop { CompanyName = "name2" };
            shopService.Setup(s => s.GetAllShops()).Returns(new List<Shop> {item, item2});

            return new ShopController(shopService.Object);
        }
    }

   
}
