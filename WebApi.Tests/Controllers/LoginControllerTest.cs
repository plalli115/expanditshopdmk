﻿using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Hosting;
using System.Web.Http.Routing;
using EISCS.Shop.DO.Interface;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using WebApi.Controllers;
using WebApi.Models.login;


namespace WebApi.Tests.Controllers
{
    [TestClass]
    public class LoginControllerTest
    {
        [TestMethod]
        public void TestLoginAccessDenied()
        {
            var loginService = new Mock<ILoginService>();
            loginService.Setup(s => s.Login(It.IsAny<string>(),It.IsAny<string>())).Returns(ExpanditLoginStatus.Unauthorized);

            var noAccessCredentials = new LoginCredentials() { Username = "unknownUser", Password = "orIncorrectPassword" };
            var res = getLoginController(loginService).Authenticate(noAccessCredentials);
            
            Assert.AreEqual(HttpStatusCode.Unauthorized, res.StatusCode);

            if (res.Headers.Any() && res.Headers.GetValues("Set-Cookie") != null)
            {
                var cookieValues = res.Headers.GetValues("Set-Cookie").ToList();
                Assert.AreEqual(false, cookieValues.Any(x => x.Contains(".ASPXAUTH")));
            }

        }

        [TestMethod]
        public void TestLoginAccessGranted()
        {
            var loginService = new Mock<ILoginService>();
            loginService.Setup(s => s.Login(It.IsAny<string>(), It.IsAny<string>())).Returns(ExpanditLoginStatus.LoggedIn);

            var credentials = new LoginCredentials() {Username = "loggedIn", Password = "correctPassword"};
            
            var res = getLoginController(loginService).Authenticate(credentials);

            Assert.AreEqual(HttpStatusCode.OK, res.StatusCode);
            Assert.AreEqual(true,res.Headers.Contains("Set-Cookie"));
            
            var cookieValues = res.Headers.GetValues("Set-Cookie").ToList();
            Assert.AreEqual(true, cookieValues.Any(x => x.Contains(".ASPXAUTH")));
        }

        private LoginController getLoginController(Mock<ILoginService> loginService)
        {
            
            var config = new HttpConfiguration();
            //the urlroute content isn't used - so it doesn't matter what the content are
            var request = new HttpRequestMessage(HttpMethod.Post, "http://localhost/api/products");
            
            //the urlroute content isn't used - so it doesn't matter what the content are
            var route = config.Routes.MapHttpRoute("DefaultApi", "api/{controller}/{id}");
            var routeData = new HttpRouteData(route, new HttpRouteValueDictionary { { "controller", "products" } });

            var controller = new LoginController(loginService.Object)
                {
                    ControllerContext = new HttpControllerContext(config, routeData, request),
                    Request = request
                    
                };

            controller.Request.Properties[HttpPropertyKeys.HttpConfigurationKey] = config;
            
       

            return controller;
        }

        [TestMethod]
        public void TestLoginNoInputBadRequest()
        {
            var loginService = new Mock<ILoginService>();
            loginService.Setup(s => s.Login(It.IsAny<string>(), It.IsAny<string>())).Returns(ExpanditLoginStatus.Unauthorized);

            
            var res = getLoginController(loginService).Authenticate(null);

            Assert.AreEqual(HttpStatusCode.BadRequest, res.StatusCode);

        }

    }
}
