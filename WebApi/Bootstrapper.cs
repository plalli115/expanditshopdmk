using System.Web.Http;
using Microsoft.Practices.Unity;
using WebApi.Services.LoginService;
using WebApi.Services.ShopService;

namespace WebApi
{
  public static class Bootstrapper
  {
    public static IUnityContainer Initialise()
    {
        var container = BuildUnityContainer();

        //line to registrate mvc4 web controller
        //DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        GlobalConfiguration.Configuration.DependencyResolver= new Unity.WebApi.UnityDependencyResolver(container);
      

        return container;
    }

    private static IUnityContainer BuildUnityContainer()
    {
      var container = new UnityContainer();
      RegisterTypes(container);
      
      return container;
    }

    public static void RegisterTypes(IUnityContainer container)
    {
        container.RegisterType<IShopService, ShopServiceStub>();
        container.RegisterType<ILoginService, LoginServiceStub>();
    }
  }
}