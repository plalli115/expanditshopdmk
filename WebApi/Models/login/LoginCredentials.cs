﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.Models.login
{
    public class LoginCredentials
    {
        public String Username { get; set; }
        public String Password { get; set; }
    
        public Boolean validate()
        {
            if (!string.IsNullOrEmpty(Username) &&
                !string.IsNullOrEmpty(Password))
            {
                return true;
            }

            return false;
        }
    }

       
}