﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace WebApi.Models.ProductCatalog
{
    //[Serializable, XmlRoot("ProductList")]
    public class ExternalProductList
    {
        public IEnumerable<ExternalProduct> ExternalProducts { get; set; }
    }
}