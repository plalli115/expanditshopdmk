﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace WebApi.Models.ProductCatalog
{
    [Serializable ]
    [XmlType(TypeName = "Product")]
    public class ExternalProduct
    {
        [XmlElement(ElementName = "ProductName")] 
        public string ProductName;//{ get; set; }

        [XmlElement(ElementName = "ProductNumber")]
        public string ProductNumber;

        [XmlElement(ElementName = "ListPriceInclTax")] 
        public double ListPriceInclTax;


        [XmlElement(ElementName = "ListPrice")] 
        public double ListPrice;

        [XmlElement(ElementName = "TaxPct")] 
        public double TaxPct;

        [XmlElement(ElementName = "CurrencyGuid")] 
        public String CurrencyGuid;

        [XmlElement(ElementName = "Description")]
        public String Description;

        [XmlElement(ElementName = "Variants")]
        public IEnumerable<ExternalProductVariant> Variants;


    }
    
}
