﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.Models.ProductCatalog
{
    public class ExternalProductVariant
    {
        public string VariantCode;
        public string VariantName;
    }
}