﻿using System;

namespace WebApi.Models.shop
{
    public class Adress
    {
        public String StreetName { get; set; }
        public String StreetNumber { get; set; }
        public String ZipCode { get; set; }
        public String City { get; set; }
        public String Country { get; set; }

    }
}
