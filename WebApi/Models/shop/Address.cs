﻿using System;

namespace WebApi.Models.shop
{
    public class Address
    {
        public String StreetName1 { get; set; }
        public String StreetName2 { get; set; }
        public String ZipCode { get; set; }
        public String PlaceName { get; set; }
        public String City { get; set; }
        public String Country { get; set; }

    }
}
