﻿using System;

namespace WebApi.Models.shop
{
    public class Shop
    {
        public String Id { get; set; }
        public String CompanyName { get; set; }
        public String Email { get; set; }
        public Address Address { get; set; }
        public TimeSpan OpeningTime { get; set; }
        public TimeSpan ClosingTime { get; set; }
    }
}