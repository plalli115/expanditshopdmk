﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;

namespace WebApi.Security
{
    public class ShopMembershipProvider : MembershipProvider
    {
        public override bool ValidateUser(string username, string password)
        {
            // this is where u should validate your user credentials against your database.
            // i've made an extra class so i can send more parameters 
            //(in this case it's the CurrentTerritoryID parameter which i used as 
            //one of the MyMembershipProvider class properties). 

            if (username == "kim" && password == "1234")
            {
                return true;
            }

            return false;

        }
    }
}