﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;
using EISCS.Shop.DO.Interface;
using WebApi.Models.login;
using System.Web.Security;




namespace WebApi.Controllers
{
    public class LoginController : ApiController
    {
  
        private readonly ILoginService _loginService;



        public LoginController(ILoginService loginService)
        {
            _loginService = loginService;
        }

        [HttpPost]
        [ActionName("authenticate")]
        public HttpResponseMessage Authenticate(LoginCredentials credentials)
        {

            if (credentials == null || !credentials.validate() )
            {
                return Request.CreateResponse<String>(HttpStatusCode.BadRequest, "The input could not be validated");
            }

            var response = Request.CreateResponse<String>(HttpStatusCode.Unauthorized, "");
            
            if (_loginService.Login(credentials.Username, credentials.Password) == ExpanditLoginStatus.LoggedIn)
            {
                CookieHeaderValue cookie = CreateSecurityCookie(credentials, response);
                
                var cookieHeaderValues = new List<CookieHeaderValue> {cookie};
                response.Headers.AddCookies(cookieHeaderValues);
            }
            
            

            return response;
        }

        [HttpGet]
        [ActionName("logout")]
        public HttpResponseMessage Logout()
        {
            _loginService.Logout();
            var response = Request.CreateResponse<String>(HttpStatusCode.OK, "");
            var cookie = new CookieHeaderValue(FormsAuthentication.FormsCookieName, "")
            {
                Path = HttpRuntime.AppDomainAppVirtualPath
                
            };

            var cookieHeaderValues = new List<CookieHeaderValue> { cookie };
            response.Headers.AddCookies(cookieHeaderValues);
            return response;
        }

        private static CookieHeaderValue CreateSecurityCookie(LoginCredentials credentials, HttpResponseMessage response)
        {
            var ticket = new FormsAuthenticationTicket(1, credentials.Username, DateTime.Now, DateTime.Now.AddMinutes(30), false, "");
            string encryptedTicket = FormsAuthentication.Encrypt(ticket);
            response.StatusCode = HttpStatusCode.OK;

            var cookie = new CookieHeaderValue(FormsAuthentication.FormsCookieName, encryptedTicket)
                {
                    Path = HttpRuntime.AppDomainAppVirtualPath
                };
            return cookie;
        }
    }
}
