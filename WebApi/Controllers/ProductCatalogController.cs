﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using AutoMapper;
using CmsPublic.ModelLayer.Models;
using EISCS.Shop.DO.Interface;
using EISCS.Shop.DO.Service;
using WebApi.Models.ProductCatalog;

namespace WebApi.Controllers
{
    public class ProductCatalogController : ApiController
    {
        private readonly IProductService _productService;
        private readonly IExpanditUserService _expanditUserService;
        private readonly IProductRepository _productRepository;

        public ProductCatalogController(IProductService productService, IExpanditUserService expanditUserService, IProductRepository productRepository)
        {
            _productService = productService;
            _expanditUserService = expanditUserService;
            _productRepository = productRepository;
        }

        [HttpGet]
        [ActionName("allproducts")]
        [Authorize]
        public ExternalProductList GetAllProducts()
        {
            List<Product> all = _productRepository.GetAllProducts(_expanditUserService.LanguageGuid, true);
            ICollection<string> guids = all.Select(x => x.ProductGuid).ToList();

            var res = _productService.GetProducts(ProductServiceHelper.CreateCartHeader(_expanditUserService), guids);
            var product = Mapper.Map<ExternalProductList>(res);
            return product;
        }
    }
}