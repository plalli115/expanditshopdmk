﻿using System.Collections.Generic;
using System.Web.Http;
using WebApi.Models.shop;
using WebApi.Services.ShopService;

namespace WebApi.Controllers
{
    [Authorize]
    public class ShopController : ApiController
    {
        private readonly IShopService _shopService;

        

        public ShopController(IShopService shopService)
        {
            _shopService = shopService;
        }

        [ActionName("all")]
        public List<Shop> GetAllShops()
        {
            return _shopService.GetAllShops();
        }

    }
}
