using System.Configuration;
using System.Web;
using System.Web.Http;
using CmsPublic.DataRepository;
using EISCS.Shop;
using EISCS.Shop.BO.BusinessLogic;
using EISCS.Shop.BO.BusinessLogic.Axapta;
using EISCS.Shop.BO.BusinessLogic.C5;
using EISCS.Shop.BO.BusinessLogic.Navision.Nav;
using EISCS.Shop.BO.BusinessLogic.Navision.Nf;
using EISCS.Shop.BO.BusinessLogic.None;
using EISCS.Shop.BO.BusinessLogic.Xal;
using EISCS.Shop.DO;
using EISCS.Shop.DO.Interface;
using EISCS.Shop.DO.Repository;
using EISCS.Shop.DO.Service;
using EISCS.ExpandITFramework.Util;
using EISCS.Wrappers.Configuration;
using Microsoft.Practices.Unity;
using WebApi.Services.ShopService;
using HttpContextWrapper = System.Web.HttpContextWrapper;

namespace WebApi
{
  public static class UnityBootstrapper
  {
    public static ConnectionStringSettings ExpanditShopConnection = ConfigurationManager.ConnectionStrings["ExpandITConnectionString"];
    public static string BackendType = ConfigurationManager.AppSettings["BackendType"];
      
    public static IUnityContainer Initialise()
    {
        var container = BuildUnityContainer();

        //line to registrate mvc4 web controller
        //DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        GlobalConfiguration.Configuration.DependencyResolver= new Unity.WebApi.UnityDependencyResolver(container);
      

        return container;
    }

    private static IUnityContainer BuildUnityContainer()
    {
      var container = new UnityContainer();
      RegisterTypes(container);
      
      return container;
    }

    public static void RegisterTypes(IUnityContainer container)
    {
        container.RegisterType<IShopService, ShopServiceStub>();


        container.RegisterType<HttpContextBase>(new InjectionFactory(c => new HttpContextWrapper(HttpContext.Current)));

        container.RegisterType<IConfigurationObject, ConfigurationObject>();
        container.RegisterType<ICurrencyConverter, CurrencyConverter>();

        // Services
        container.RegisterType<IProductService, ProductService>();
        container.RegisterType<IExpanditUserService, ExpanditUserService>();
        container.RegisterType<ILoginService, LoginService>();
        container.RegisterType<ICartDataService, CartDataService>();
        container.RegisterType<ICartService, CartService>();
        container.RegisterType<IShippingHandlingService, ShippingHandlingService>();
        container.RegisterType<IPaymentService, PaymentService>();
        container.RegisterType<IProductVariantService, ProductVariantService>();
        container.RegisterType<IPropertiesDataService, PropertiesDataService>();
        container.RegisterType<IKeyPerformanceIndicatorsService, KeyPerformanceIndicatorsService>();
        container.RegisterType<ILegacyDataService, LegacyDataService>();
        
        // Repositories
        container.RegisterType<IProductRepository, ProductRepository>();
        container.RegisterType<IFavoritesRepository, FavoritesRepository>();
        container.RegisterType<ICartHeaderRepository, CartHeaderRepository>();
        container.RegisterType<ICartLineRepository, CartLineRepository>();
        container.RegisterType<IShippingHandlingProviderRepository, ShippingHandlingProviderRepository>();
        container.RegisterType<IShippingHandlingPriceRepository, ShippingHandlingPriceRepository>();
        container.RegisterType<IPaymentTypeRepository, PaymentTypeRepository>();
        container.RegisterType<IUserItemRepository, UserItemRepository>();
        container.RegisterType<IKeyPerformanceIndicatorsRepository, KeyPerformanceIndicatorsRepository>();
        container.RegisterType<IShopSalesHeaderRepository, ShopSalesHeaderRepository>();
        container.RegisterType<IShopSalesLineRepository, ShopSalesLineRepository>();

        container.RegisterType<IHttpRuntimeWrapper, HttpRuntimeWrapper>();
           
        RegisterBackend(container);
        RegisterFactories(container);

        container.RegisterType<ICurrencyRepository, CurrencyRepository>();
        container.RegisterType<IUserStorageService, UserStorageService>(
            new InjectionConstructor(typeof(IExpanditDbFactory), typeof(IConfigurationObject)));
    }

    private static void RegisterBackend(IUnityContainer container)
    {
        switch (BackendType)
        {
            case "NAV":
                container.RegisterType<IBuslogic, BuslogicNav>();
                container.RegisterType<INavDataService, NavDataService>();
                break;
            case "AX":
            case "AX2012":
                container.RegisterType<IBuslogic, BuslogicAx>();
                container.RegisterType<IAxDataService, AxDataService>();
                break;
            case "XAL":
                container.RegisterType<IBuslogic, BuslogicXal>();
                container.RegisterType<IXalDataService, XalDataService>();
                break;
            case "C5":
                container.RegisterType<IBuslogic, BuslogicC5>();
                container.RegisterType<IC5DataService, C5DataService>();
                break;
            case "NF":
                container.RegisterType<IBuslogic, BuslogicNf>();
                container.RegisterType<INfDataService, NfDataService>();
                break;
            default:
                container.RegisterType<IBuslogic, BuslogicNone>();
                container.RegisterType<INoneDataService, NoneDataService>();
                break;
        }
    }

      private static void RegisterFactories(IUnityContainer container)
      {
          container.RegisterType<IExpanditDbFactory, ExpanditDbFactory>(new InjectionConstructor(ExpanditShopConnection.ConnectionString, ExpanditShopConnection.ProviderName));
          container.RegisterType<IShopDatabaseConnectionFactory, ShopDatabaseConnectionFactory>(new InjectionConstructor(ExpanditShopConnection));

      }
  }
}