﻿using System.Collections.Generic;
using WebApi.Models.shop;

namespace WebApi.Services.ShopService
{
    public interface IShopService
    {
        List<Shop> GetAllShops();
    }
}
