﻿using System.Collections.Generic;
using WebApi.Models.shop;

namespace WebApi.Services.ShopService
{
    public class ShopServiceStub : IShopService
    {
        public List<Shop> GetAllShops()
        {
            var shop = new Shop { CompanyName = "Kim's test shop" };
            var adr = new Address
            {
                StreetName1 = "Lillegade",
                StreetName2 = "",
                ZipCode = "8500",
                City = "Grenaa",
                Country = "Denmark",
            };

            shop.Address = adr;

            var shop1 = new Shop { CompanyName = "Tom's test shop" };
            var adr1 = new Address
            {
                StreetName1 = "Langkærvej",
                StreetName2 = "Langkærvej",
                ZipCode = "8381",
                City = "Tilst",
                Country = "Denmark",
            };

            shop1.Address = adr1;

            return new List<Shop> { shop, shop1 };
        }
    }
}
