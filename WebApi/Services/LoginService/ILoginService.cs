﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebApi.Models.login;

namespace WebApi.Services.LoginService
{
    public interface ILoginService
    {
        Boolean ValidateUser(LoginCredentials credentials);
    }
}
