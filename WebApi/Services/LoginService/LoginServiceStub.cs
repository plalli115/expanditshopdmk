﻿using System;
using WebApi.Models.login;

namespace WebApi.Services.LoginService
{
    public class LoginServiceStub : ILoginService
    {
        public Boolean ValidateUser(LoginCredentials credentials)
        {
            if(credentials.Password == credentials.Username){
                return true;
            }
            
            return false;
            
        }
    }
}