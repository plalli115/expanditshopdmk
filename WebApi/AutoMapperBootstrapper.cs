﻿using System.Collections.Generic;
using AutoMapper;
using EISCS.Shop.DO.Dto.Product;
using EISCS.Shop.DO.Service;
using WebApi.Models.ProductCatalog;
using WebApi.Models.shop;

namespace WebApi
{
    public class AutoMapperBootstrapper
    {
        public static void RegisterTypes()
        {
            //area for definitions for automapper
            Mapper.CreateMap<ProductInfoNode, ExternalProduct>()
                    .ForMember(dest => dest.ProductName, opt => opt.MapFrom(src => src.Prod.ProductName))
                    .ForMember(dest => dest.ProductNumber, opt => opt.MapFrom(src => src.Prod.ProductGuid))
                    .ForMember(dest => dest.ListPriceInclTax, opt => opt.MapFrom(src => src.Line.ListPriceInclTax))
                    .ForMember(dest => dest.ListPrice, opt => opt.MapFrom(src => src.Line.ListPrice))
                    .ForMember(dest => dest.TaxPct, opt => opt.MapFrom(src => src.Line.TaxPct))
                    .ForMember(dest => dest.CurrencyGuid, opt => opt.MapFrom(src => src.Line.CurrencyGuid))
                    .ForMember(dest => dest.Description, opt => opt.MapFrom(src => src.Prod.GetPropertyValue("DESCRIPTION")))
                    .ForMember(dest => dest.Variants, opt => opt.MapFrom(src => src.VariantDict.Values))
                    ;
            

            Mapper.CreateMap<ProductInfoNodeContainer, ExternalProductList>()
                  .ForMember(dest => dest.ExternalProducts, opt => opt.MapFrom(src => src.ProductInfoNodes));

            Mapper.CreateMap<ProductVariant , ExternalProductVariant>()
                  .ForMember(dest => dest.VariantCode, opt => opt.MapFrom(src => src.VariantCode))
                  .ForMember(dest => dest.VariantName, opt => opt.MapFrom(src => src.GetDisplayName()));    
        }



    }
}

