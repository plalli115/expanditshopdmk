This is the source file for the standard internet shop of ExpandIT 

For build automation we use psake ( https://github.com/psake/psake )

To build the code from the commandline:
- start powershell
- cd into the root folder of this project
- run .\psake.cmd

Other psake tasks exists and can be found in the default.ps1 file.
To list these 
.\psake.cmd -docs