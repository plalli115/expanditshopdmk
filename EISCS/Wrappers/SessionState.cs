﻿using System.Web.SessionState;

namespace EISCS.Wrappers
{
    public class SessionState : ISessionState
    {
        private readonly HttpSessionState _session;

        public SessionState(HttpSessionState session)
        {
            _session = session;
        }

        public void Clear()
        {
            _session.RemoveAll();
        }

        public void Delete(string key)
        {
            _session.Remove(key);
        }

        public object Get(string key)
        {
            return _session[key];
        }

        public void Store(string key, object value)
        {
            _session[key] = value;
        }
    }
}