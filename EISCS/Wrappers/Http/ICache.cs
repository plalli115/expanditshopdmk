﻿using System;
using System.Web.Caching;

namespace EISCS.Wrappers.Http {
    public interface ICache {
        void Add(string key, object value, CacheDependency dependencies,
            DateTime absoluteExpiration, TimeSpan slidingExpiration, CacheItemPriority priority, CacheItemRemovedCallback onRemoveCallback);
        void Count();
        void Insert();
    }
}
