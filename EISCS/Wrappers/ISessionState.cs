﻿namespace EISCS.Wrappers
{
    public interface ISessionState
    {
        void Clear();
        void Delete(string key);
        object Get(string key);
        void Store(string key, object value);
    }

    public static class SessionExtensions
    {
        public static T Get<T>(this ISessionState sessionState, string key) where T : class
        {
            return sessionState.Get(key) as T;
        }
    }
}