﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace EISCS.Wrappers.Configuration
{
    public interface IConfigurationObject
    {
        string Read(string value);
    }
}