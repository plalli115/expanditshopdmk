﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace EISCS.Wrappers.Configuration
{
    public class ConfigurationObject : IConfigurationObject
    {
        public string Read(string value)
        {
            return ConfigurationManager.AppSettings[value];
        }
    }
}