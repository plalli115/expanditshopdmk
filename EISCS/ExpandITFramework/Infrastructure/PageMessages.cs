﻿using System.Collections;
using System.Collections.Generic;
using System.Web;
using System;
using System.Text;
using CmsPublic.DataProviders.Logging;
using EISCS.ExpandITFramework.Util;

namespace EISCS.ExpandITFramework.Infrastructure {

    public class PageMessages {
        public List<string> Actions;
        public List<string> Errors;
        public List<string> Warnings;
        public List<string> Messages;


        public enum MessageType : byte {
            actions,
            errors,
            warnings,
            messages
        }

        public PageMessages() {

            Actions = GetMsgsFromContext(MessageType.actions);
            Errors = GetMsgsFromContext(MessageType.errors);
            Messages = GetMsgsFromContext(MessageType.messages);
            Warnings = GetMsgsFromContext(MessageType.warnings);

        }

        private static List<string> GetMsgsFromContext(MessageType messType) {
            List<string> retv = new List<string>();
            try {
                string messTypeName = Enum.GetName(typeof(MessageType), messType);
                //1. get from request
                string[] strings = Utilities.Split(HttpContext.Current.Request[messTypeName], "\t");
                if (strings != null) {
                    foreach (string s in strings) {
                        if (!string.IsNullOrEmpty(s)) retv.Add(s);
                    }
                }
                //2. get from context, and merge
                strings = Utilities.Split((string)HttpContext.Current.Items[messTypeName], "\t");
                if (strings != null) {
                    foreach (string s in strings) {
                        if (!string.IsNullOrEmpty(s) && (!retv.Contains(s))) {
                            retv.Add(s);
                        }
                    }
                }
            } catch (Exception ex) {
                FileLogger.Log(ex.Message + "\r\n" + ex.StackTrace);
            }
            return retv;
        }

        public string ContextItems() {
            return ContextItems(MessageType.messages);
        }

        public string ContextItems(MessageType messType) {
            StringBuilder builder = new StringBuilder();
            List<string> msgList = null;
            switch (messType) {
                case MessageType.actions:
                    msgList = Actions;
                    break;
                case MessageType.errors:
                    msgList = Errors;
                    break;
                case MessageType.messages:
                    msgList = Messages;
                    break;
                case MessageType.warnings:
                    msgList = Warnings;
                    break;
                default:
                    break;
            }
            if (msgList != null) {
                foreach (string itm in msgList) {
                    builder.AppendFormat("{0}\t", itm);
                }
            }
            return builder.ToString();
        }

        public void SaveToContextItems(IDictionary contextItems) {
            foreach (MessageType mt in Enum.GetValues(typeof(MessageType))) {
                string messTypeName = Enum.GetName(typeof(MessageType), mt);
                string messValue = ContextItems(mt);
                if (!string.IsNullOrEmpty(messValue)) {
                    contextItems.Add(messTypeName, messValue);
                }
            }
        }

        /// <summary> 
        /// Returns the messages in the current object as query string parameters. 
        /// </summary> 
        /// <returns>A part of a query string.</returns> 
        /// <remarks>Use this function when you want to pass the state of the message object to another page.</remarks> 
        public string QueryString() {
            string retv = "";

            string s = "";
            foreach (string itm in Actions) {
                s = s + (!string.IsNullOrEmpty(s) ? "%09" : "") + HttpUtility.UrlEncode(itm);
            }
            if (!string.IsNullOrEmpty(s)) retv = retv + (!string.IsNullOrEmpty(retv) ? "&" : "") + "actions=" + s;

            s = "";
            foreach (string itm in Errors) {
                s = s + (!string.IsNullOrEmpty(s) ? "%09" : "") + HttpUtility.UrlEncode(itm);
            }
            if (!string.IsNullOrEmpty(s)) retv = retv + (!string.IsNullOrEmpty(retv) ? "&" : "") + "errors=" + s;

            s = "";
            foreach (string itm in Warnings) {
                s = s + (!string.IsNullOrEmpty(s) ? "%09" : "") + HttpUtility.UrlEncode(itm);
            }
            if (!string.IsNullOrEmpty(s)) retv = retv + (!string.IsNullOrEmpty(retv) ? "&" : "") + "warnings=" + s;

            s = "";
            foreach (string itm in Messages) {
                s = s + (!string.IsNullOrEmpty(s) ? "%09" : "") + HttpUtility.UrlEncode(itm);
            }
            if (!string.IsNullOrEmpty(s)) retv = retv + (!string.IsNullOrEmpty(retv) ? "&" : "") + "messages=" + s;

            return retv;
        }
    }
}
