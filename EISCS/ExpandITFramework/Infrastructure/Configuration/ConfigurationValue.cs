﻿using EISCS.ExpandITFramework.Infrastructure.Configuration.Interfaces;

namespace EISCS.ExpandITFramework.Infrastructure.Configuration {
    public class ConfigurationValue : IConfigurationValue {

        #region IAppSettingValue Members

        public string AppSettings(string name) {
            return System.Configuration.ConfigurationManager.AppSettings[name];
        }

        #endregion
    }
}
