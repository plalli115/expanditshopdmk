﻿namespace EISCS.ExpandITFramework.Infrastructure.Configuration.Interfaces {
    public interface IConfigurationValue {
        string AppSettings(string name);
    }
}
