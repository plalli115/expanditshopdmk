﻿using System.Configuration;
using System.Web.Caching;
using System.Web.Mvc;
using CmsPublic.DataRepository;
using EISCS.ExpandITFramework.Infrastructure.Configuration;
using EISCS.ExpandITFramework.Infrastructure.Configuration.Interfaces;
using EISCS.ExpandITFramework.Infrastructure.SqlDependencyCaching.Interfaces;
using System;
using System.Collections;
using EISCS.ExpandITFramework.Infrastructure.SqlDependencyCaching;
using EISCS.ExpandITFramework.Util;
using EISCS.Shop.DO.Service;

namespace EISCS.ExpandITFramework.Infrastructure {

    enum SqlCacheDependencyReturnValues {
        Ok = 0,
        NotInCorrectRoles = 1,
        NotSufficientPermissions = 2,
        ExceptionOccurred = 3
    }

    /// <summary> 
    /// CacheManager Class is used to setup and maintain ASP.Net Caching with SQL Cache Dependency. 
    /// This class is made to work on Security Level "Medium Trust". Thats why we are setting up all 
    /// infrastructure via SQL statements instead of using the built in SqlCacheDependencyAdmin class, 
    /// which demands "Full Trust" to operate. 
    /// </summary> 
    /// <remarks></remarks> 
    public class CacheManager {

        private static bool _isInitialized;
        
        private static ISqlCacheDependencyContext _context;
        private static ISqlCacheDependencyManager _sqlCacheDependencyManager;        
        // Default ConnectionStringName        
        private const string ConnectionStringName = "ExpandITConnectionString";
        private static ILegacyDataService _testService;
        
        private static void Initialize() {
            if (!_isInitialized) {
                if (_context == null) {
                    _context = new SqlCacheDependencyContext();
                }
                ISqlCacheDependencyConfigSettingsReader configReader = new SqlCacheDependencyConfigSettingsReader(_context, ConnectionStringName);
                ISqlCacheDependencyDatabaseInfrastructureManager infraMan = new SqlCacheDependencyDatabaseInfrastructureManager();
                ISqlCacheDependencyPermissionChecker depChecker = new SqlCacheDependencyPermissionChecker();
                IConfigurationValue configValue = new ConfigurationValue();

                ILegacyDataService legacyDataService = _testService ?? DependencyResolver.Current.GetService<ILegacyDataService>();

                // Dirty workaround for unit tests
                if (legacyDataService == null)
                {
                    ConnectionStringSettings expanditShopConnection = ConfigurationManager.ConnectionStrings["ExpandITConnectionString"];
                    IExpanditDbFactory dbFactory = new ExpanditDbFactory(expanditShopConnection.ConnectionString, expanditShopConnection.ProviderName);
                    legacyDataService = new LegacyDataService(dbFactory, new HttpRuntimeWrapper());
                }
                

                if (_sqlCacheDependencyManager == null) {
                    _sqlCacheDependencyManager = new SqlCacheDependencyManager(_context, configReader, infraMan, depChecker, legacyDataService, configValue);
                }
            }
            _isInitialized = true;
        }

        public static void Initialize(ISqlCacheDependencyContext context, ILegacyDataService service) {
            if (context != null) {
                _context = context;
            }
            if (service != null)
            {
                _testService = service;
            }
            Initialize();
        }

        // Prepare tables for dependency caching if roles and permisions are in order
        public static bool InitializeCachedTables() {
            Initialize();
            return _sqlCacheDependencyManager.InitializeCachedTables();
        }

        public static string DataBaseName {            
            get {
                Initialize();
                return _sqlCacheDependencyManager.DataBaseName; 
            }
        }

        /// <summary> 
        /// Find out if dependency caching is enabled on specified table 
        /// </summary> 
        /// <param name="tableName"></param> 
        /// <value></value> 
        /// <returns></returns> 
        /// <remarks></remarks> 
        public static bool CacheEnabled(string tableName) {
            Initialize();
            return _sqlCacheDependencyManager.CacheEnabled(tableName);
        }

        /// <summary> 
        /// During a full upload some or all of the tables are dropped and recreated, and our SqlCacheUpdate-triggers 
        /// no longer exist.When this happens we need to invalidate the cache and recreate the triggers in order to 
        /// maintain a cache with the correct SqlDependency. In SQL Server 2005 and later we are using ddl-triggers 
        /// and stored procedures to do this work, but in SQL Server 2000 there are no ddl-triggers. 
        /// In case of running on a SQL Server 2000 we have to do this in code. 
        /// Also if ALTER ANY DATABASE DDL TRIGGER is not granted to the current user then this code will be run.
        /// </summary> 
        /// <param name="tableArray">An ArrayList containing the names of the tables that has been recreated</param> 
        /// <remarks></remarks> 
        public static void TablesRecreated(ArrayList tableArray) {
            Initialize();
            _sqlCacheDependencyManager.TablesRecreated(tableArray);
        }        

        #region INSERT INTO CACHE

        /// <summary> 
        /// Insert object with single dependency into dependency cache 
        /// </summary> 
        /// <param name="tableName"></param> 
        /// <param name="cacheObject"></param> 
        /// <returns></returns> 
        /// <remarks>True on success</remarks> 
        public static bool InsertIntoCache(string tableName, object cacheObject) {
            Initialize();
            return _sqlCacheDependencyManager.InsertIntoCache(tableName, cacheObject);
        }

        /// <summary> 
        /// Overloaded Function inserts object with single dependency into dependency cache 
        /// </summary> 
        /// <param name="key"></param> 
        /// <param name="tableName"></param> 
        /// <param name="cacheObject"></param> 
        /// <returns></returns> 
        /// <remarks></remarks> 
        public static bool InsertIntoCache(string key, string tableName, object cacheObject) {
            Initialize();
            return _sqlCacheDependencyManager.InsertIntoCache(key, tableName, cacheObject);
        }

        /// <summary> 
        /// Insert object with aggregated dependency into dependency cache 
        /// </summary> 
        /// <param name="key"></param> 
        /// <param name="cacheObject"></param> 
        /// <param name="aggDependency"></param> 
        /// <returns></returns> 
        /// <remarks></remarks> 
        public static bool InsertIntoCache(string key, object cacheObject, AggregateCacheDependency aggDependency) {
            Initialize();
            return _sqlCacheDependencyManager.InsertIntoCache(key, cacheObject, aggDependency);
        }

        public static bool InsertIntoCache(string key, object cacheObject, CacheDependency dependency, DateTime d, TimeSpan t, CacheItemPriority cip, CacheItemRemovedCallback circ) {
            Initialize();
            return _sqlCacheDependencyManager.InsertIntoCache(key, cacheObject, dependency, d, t, cip, circ);
        }

        public static object CacheGet(string key)
        {
            Initialize();
            return _sqlCacheDependencyManager.CacheGet(key);
        }

        /// <summary> 
        /// Insert an object with single dependency into cache. 
        /// </summary> 
        /// <param name="key">The key that will identify the object in cache</param> 
        /// <param name="v">The object to cache</param> 
        /// <param name="tableName">String containing the tablename that the object depends on</param> 
        /// <remarks> 
        /// Adds an object to cache with a dependency on a single table. 
        /// A change in the dependent table will invalidate and empty the cache. 
        /// </remarks> 
        public static bool CacheSet(string key, object v, string tableName) {
            Initialize();
            return _sqlCacheDependencyManager.CacheSet(key, v, tableName);
        }

        /// <summary> 
        /// Insert an aggregated dependency into cache 
        /// </summary> 
        /// <param name="key">The key that will identify the object in cache</param> 
        /// <param name="v">The object to cache</param> 
        /// <param name="tableNames">A string array with the tablenames that the object depends on</param> 
        /// <remarks> 
        /// Adds an object to cache with dependencies on multiple tables. 
        /// A change in one of the dependent tables will invalidate and empty the cache. 
        /// </remarks> 
        public static bool CacheSetAggregated(string key, object v, string[] tableNames) {
            Initialize();
            return _sqlCacheDependencyManager.CacheSetAggregated(key, v, tableNames);
        }

        public static bool CacheSetAggregated(string key, object v, string[] tableNames, CacheItemRemovedCallback rem) {
            Initialize();
            return _sqlCacheDependencyManager.CacheSetAggregated(key, v, tableNames, rem);
        }
        #endregion

        /// <summary> 
        /// Enable dependency caching on specified table 
        /// </summary> 
        /// <returns>True on success</returns> 
        /// <remarks></remarks> 
        public static bool EnableCaching(string tableName) {
            Initialize();
            return _sqlCacheDependencyManager.EnableCaching(tableName);
        }

        /// <summary> 
        /// Enable dependency caching on specified tables 
        /// </summary> 
        /// <param name="tableNames"></param> 
        /// <returns></returns> 
        /// <remarks></remarks> 
        public static bool EnableCaching(string[] tableNames) {
            Initialize();
            return _sqlCacheDependencyManager.EnableCaching(tableNames);
        }

        /// <summary> 
        /// Disable dependency caching on specified table 
        /// </summary> 
        /// <param name="tableName"></param> 
        /// <returns></returns> 
        /// <remarks></remarks> 
        public static bool DisableCaching(string tableName) {
            Initialize();
            return _sqlCacheDependencyManager.DisableCaching(tableName);
        }

        /// <summary> 
        /// Disable dependency caching on specified tables 
        /// </summary> 
        /// <param name="tableNames"></param> 
        /// <returns></returns> 
        /// <remarks></remarks> 
        public static bool DisableCaching(string[] tableNames) {
            Initialize();
            return _sqlCacheDependencyManager.DisableCaching(tableNames);
        }
    }
}