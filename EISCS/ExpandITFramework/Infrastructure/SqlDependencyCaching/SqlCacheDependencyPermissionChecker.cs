﻿using System;
using System.Text;
using EISCS.ExpandITFramework.Infrastructure.SqlDependencyCaching.Interfaces;

namespace EISCS.ExpandITFramework.Infrastructure.SqlDependencyCaching {
    public class SqlCacheDependencyPermissionChecker : ISqlCacheDependencyPermissionChecker {
        /// <summary>
        /// Check that the current user has the correct permissions
        /// on ASP_NET CacheDependency table and sp's 
        /// </summary>
        /// <returns></returns>
        public bool CheckSqlCacheDependencyPermissions() {

            string sql = SqlCacheDependencyPermissionScript();
            object oResult = DataAccess.GetSingleValueDb(sql);

            return oResult == DBNull.Value || oResult == null;
        }

        public bool CheckDdlTriggerPermission() {
            return DataAccess.GetSingleValueDb(
                "SELECT permission_name FROM fn_my_permissions(NULL, 'DATABASE') " + 
                "WHERE permission_name = 'ALTER ANY DATABASE DDL TRIGGER'") != DBNull.Value;
        }
        
        /// <summary>
        /// Check that the current user is in the necessary roles for ASP_NET CacheDependency Management
        /// </summary>
        /// <returns></returns>
        public bool CheckRoles() {
            int iResult = 0;
            const string sql = "IF IS_MEMBER('db_ddladmin') = 1 AND IS_MEMBER('db_securityadmin') = 1 " +
                               "SELECT 1 " +
                               "ELSE " +
                               "SELECT 0";
            object oResult = DataAccess.GetSingleValueDb(sql);
            if (oResult != DBNull.Value && oResult != null) {
                iResult = (int)oResult;
            }
            return iResult == 1;
        }

        /// <summary>
        /// Script checks the permissions on ASP_NET CacheDependencyTable and sp's
        /// </summary>
        /// <returns></returns>
        private static string SqlCacheDependencyPermissionScript() {
            StringBuilder sb = new StringBuilder();

            /* CHECK FOR PRESENCE OF TABLE AND SPS */
            sb.Append("IF NOT EXISTS (SELECT name FROM sysobjects WHERE name = 'AspNet_SqlCacheTablesForChangeNotification' AND type = 'U') ");
            sb.Append("BEGIN ");
            sb.Append("SELECT 0 ");
            sb.Append("END ");
            /* CHECK TABLE PERMISSIONS */
            sb.Append("IF NOT EXISTS( ");
            sb.Append("SELECT permission_name ");
            sb.Append("FROM fn_my_permissions('dbo.AspNet_SqlCacheTablesForChangeNotification', 'OBJECT') ");
            sb.Append("WHERE permission_name = 'ALTER') ");
            sb.Append("BEGIN ");

            sb.Append("SELECT 0 ");
            sb.Append("END ");
            sb.Append("IF NOT EXISTS( ");
            sb.Append("SELECT permission_name ");
            sb.Append("FROM fn_my_permissions('dbo.AspNet_SqlCacheTablesForChangeNotification', 'OBJECT') ");
            sb.Append("WHERE permission_name = 'UPDATE') ");
            sb.Append("BEGIN ");

            sb.Append("SELECT 0 ");
            sb.Append("END ");
            sb.Append("IF NOT EXISTS( ");
            sb.Append("SELECT permission_name ");
            sb.Append("FROM fn_my_permissions('dbo.AspNet_SqlCacheTablesForChangeNotification', 'OBJECT') ");
            sb.Append("WHERE permission_name = 'INSERT') ");
            sb.Append("BEGIN ");

            sb.Append("SELECT 0 ");
            sb.Append("END ");
            sb.Append("IF NOT EXISTS( ");
            sb.Append("SELECT permission_name ");
            sb.Append("FROM fn_my_permissions('dbo.AspNet_SqlCacheTablesForChangeNotification', 'OBJECT') ");
            sb.Append("WHERE permission_name = 'DELETE') ");
            sb.Append("BEGIN ");

            sb.Append("SELECT 0 ");
            sb.Append("END ");
            sb.Append("IF NOT EXISTS( ");
            sb.Append("SELECT permission_name ");
            sb.Append("FROM fn_my_permissions('dbo.AspNet_SqlCacheTablesForChangeNotification', 'OBJECT') ");
            sb.Append("WHERE permission_name = 'SELECT') ");
            sb.Append("BEGIN ");

            sb.Append("SELECT 0 ");
            sb.Append("END ");
            /* CHECK SP PERMISSIONS */
            sb.Append("IF NOT EXISTS( ");
            sb.Append("SELECT permission_name ");
            sb.Append("FROM fn_my_permissions('dbo.AspNet_SqlCachePollingStoredProcedure', 'OBJECT') ");
            sb.Append("WHERE permission_name = 'EXECUTE') ");
            sb.Append("BEGIN ");

            sb.Append("SELECT 0 ");
            sb.Append("END ");
            sb.Append("IF NOT EXISTS( ");
            sb.Append("SELECT permission_name ");
            sb.Append("FROM fn_my_permissions('dbo.AspNet_SqlCacheQueryRegisteredTablesStoredProcedure', 'OBJECT') ");
            sb.Append("WHERE permission_name = 'EXECUTE') ");
            sb.Append("BEGIN ");

            sb.Append("SELECT 0 ");
            sb.Append("END ");
            sb.Append("IF NOT EXISTS( ");
            sb.Append("SELECT permission_name ");
            sb.Append("FROM fn_my_permissions('dbo.AspNet_SqlCacheRegisterTableStoredProcedure', 'OBJECT') ");
            sb.Append("WHERE permission_name = 'EXECUTE') ");
            sb.Append("BEGIN ");

            sb.Append("SELECT 0 ");
            sb.Append("END ");
            sb.Append("IF NOT EXISTS( ");
            sb.Append("SELECT permission_name ");
            sb.Append("FROM fn_my_permissions('dbo.AspNet_SqlCacheUnRegisterTableStoredProcedure', 'OBJECT') ");
            sb.Append("WHERE permission_name = 'EXECUTE') ");
            sb.Append("BEGIN ");

            sb.Append("SELECT 0 ");
            sb.Append("END ");
            sb.Append("IF NOT EXISTS( ");
            sb.Append("SELECT permission_name ");
            sb.Append("FROM fn_my_permissions('dbo.AspNet_SqlCacheUpdateChangeIdStoredProcedure', 'OBJECT') ");
            sb.Append("WHERE permission_name = 'EXECUTE') ");
            sb.Append("BEGIN ");

            sb.Append("SELECT 0 ");
            sb.Append("END ");

            return sb.ToString();
        }
    }
}
