﻿using System.Xml;
using EISCS.ExpandITFramework.Infrastructure.SqlDependencyCaching.Interfaces;

namespace EISCS.ExpandITFramework.Infrastructure.SqlDependencyCaching {
    public class SqlCacheDependencyConfigSettingsReader : ISqlCacheDependencyConfigSettingsReader {

        private readonly ISqlCacheDependencyContext _context;
        private readonly string _connectionStringName;

        public SqlCacheDependencyConfigSettingsReader(ISqlCacheDependencyContext context, string connectionStringName) {
            _context = context;
            _connectionStringName = connectionStringName;
        }

        /// <summary> 
        /// Read web.config cacheSettings 
        /// </summary> 
        /// <remarks></remarks> 
        public string ReadConfigCacheSettings() {
            string retval = "";
            string path = _context.MapPath("~/web.config");
            XmlDocument configs = new XmlDocument();

            configs.Load(path);
            XmlNamespaceManager mgr = new XmlNamespaceManager(configs.NameTable);
            string prefix = string.Empty;

            if (configs.DocumentElement != null) {
                if (!string.IsNullOrEmpty(configs.DocumentElement.NamespaceURI)) {
                    prefix = "x:";
                    mgr.AddNamespace("x", configs.DocumentElement.NamespaceURI);
                }
            }
            XmlNodeList xnl = configs.SelectNodes(string.Format(
                "{0}configuration/{1}system.web/{2}caching/{3}sqlCacheDependency/{4}databases/{5}add", prefix, prefix, prefix, prefix, prefix, prefix), mgr);

            if (xnl != null) {
                foreach (XmlNode xn in xnl) {
                    if (xn.Attributes == null) continue;
                    if (_connectionStringName == null) continue;
                    if (!string.Equals(xn.Attributes.GetNamedItem("connectionStringName").Value, _connectionStringName))
                        continue;
                    retval = xn.Attributes.GetNamedItem("name").Value;
                    break;
                }
            }
            return retval;
        }
    }
}
