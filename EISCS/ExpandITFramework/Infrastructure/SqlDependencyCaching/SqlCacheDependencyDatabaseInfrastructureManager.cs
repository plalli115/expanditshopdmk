﻿using System.Text;
using EISCS.ExpandITFramework.Infrastructure.SqlDependencyCaching.Interfaces;

namespace EISCS.ExpandITFramework.Infrastructure.SqlDependencyCaching {
    /// <summary>
    /// Creates and deletes the SqlCacheDependency Table and SP's
    /// </summary>
    public class SqlCacheDependencyDatabaseInfrastructureManager : ISqlCacheDependencyDatabaseInfrastructureManager {

        public bool CreateSqlCacheDependencyStructure() {
            string sql = CreateSqlCacheDependencyScript();
            string message = DataAccess.ExcecuteNonQueryDb(sql);
            return string.IsNullOrEmpty(message);
        }

        public string CreateDdlTrigger() {
            return DataAccess.ExcecuteNonQueryDb(DdlTriggerScript());
        }

        public bool CreateSpRestoreCache() {
            string message = DataAccess.ExcecuteNonQueryDb(SpTriggerScript());
            return string.IsNullOrEmpty(message);
        }

        public string DeleteSqlCacheDependencyStructure() {
            string sql = DeleteCahceDependencyScript();
            return DataAccess.ExcecuteNonQueryDb(sql);                        
        }

        #region Scripts Creation

        /// <summary> 
        /// Returns the script needed to set up a ddl-trigger on the DataBase defined in web.config 
        /// </summary> 
        /// <returns></returns> 
        /// <remarks></remarks> 
        private static string DdlTriggerScript() {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("IF NOT EXISTS(SELECT name FROM sys.triggers WHERE name = 'TableCreated' AND type = 'TR') ");
            sb.AppendLine("EXEC(' ");
            sb.AppendLine("CREATE TRIGGER [TableCreated] ");
            sb.AppendLine("ON DATABASE ");
            sb.AppendLine("FOR CREATE_TABLE, ALTER_TABLE ");
            sb.AppendLine("AS ");
            sb.AppendLine("SET NOCOUNT ON ");
            sb.AppendLine("DECLARE @xmlEventData XML, ");
            sb.AppendLine("@tableName VARCHAR(450) ");
            sb.AppendLine("SET @xmlEventData = EVENTDATA() ");
            sb.AppendLine("SET @tableName = CONVERT(VARCHAR(300),@xmlEventData.query(''data(/EVENT_INSTANCE/ObjectName)'')) ");
            sb.AppendLine("IF EXISTS (SELECT name FROM sysobjects WITH (NOLOCK) WHERE name = ''RestoreCache'' AND type = ''P'') ");
            sb.AppendLine("IF EXISTS (SELECT name FROM sysobjects WITH (TABLOCKX) WHERE name = ''RestoreCache'' AND type = ''P'') ");
            sb.AppendLine("EXEC RestoreCache @tableName') ");

            return sb.ToString();
        }

        /// <summary> 
        /// Returns the script needed to set up the "RestoreCache" stored procedure on the DataBase defined in web.config 
        /// </summary> 
        /// <returns></returns> 
        /// <remarks></remarks> 
        private static string SpTriggerScript() {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine(" CREATE PROCEDURE RestoreCache ");
            sb.AppendLine(" @p1 varchar(450) = '' ");
            sb.AppendLine(" AS ");
            sb.AppendLine(" BEGIN ");
            sb.AppendLine(" SET NOCOUNT ON; ");
            sb.AppendLine(" DECLARE @triggerCreate varchar(5000) ");
            sb.AppendLine(" DECLARE @triggerName varchar(1000) ");

            sb.AppendLine(" DECLARE @tableName VARCHAR(450), @pattern_position INT ");
            sb.AppendLine(" SELECT @pattern_position = CHARINDEX('$', @p1) ");
            sb.AppendLine(" IF @pattern_position > 0 ");
            sb.AppendLine(" SELECT @pattern_position = CHARINDEX('$', RIGHT(@p1, LEN(@p1) -1)) +1 ");
            sb.AppendLine(" SET @tableName = SUBSTRING(@p1, @pattern_position +1, LEN(@p1)) ");


            sb.AppendLine(" SET @triggerName = @tableName+'_AspNet_SqlCacheNotification_Trigger' ");
            sb.AppendLine(" SET @triggerCreate = 'CREATE TRIGGER ['+@triggerName+'] ON ['+@p1+'] ");
            sb.AppendLine(" FOR INSERT, UPDATE, DELETE AS BEGIN ");
            sb.AppendLine(" SET NOCOUNT ON ");
            sb.AppendLine(" EXEC AspNet_SqlCacheUpdateChangeIdStoredProcedure N'''+@tableName+''' ");
            sb.AppendLine(" END' ");
            sb.AppendLine(" END ");

            sb.AppendLine(" IF EXISTS(SELECT tableName FROM AspNet_SqlCacheTablesForChangeNotification WHERE tableName = @tableName) ");
            sb.AppendLine(" EXEC AspNet_SqlCacheUpdateChangeIdStoredProcedure @tableName ");

            sb.AppendLine(" DECLARE @dropTrigger VARCHAR(500) ");
            sb.AppendLine(" SET @dropTrigger = 'DROP TRIGGER ['+@triggerName+']' ");

            sb.AppendLine(" IF EXISTS (SELECT name FROM sysobjects WITH (NOLOCK) WHERE name = @triggerName AND type = 'TR') ");
            sb.AppendLine(" EXEC(@dropTrigger) ");
            sb.AppendLine(" IF EXISTS (SELECT name FROM sysobjects WITH (TABLOCKX) WHERE name = @triggerName AND type = 'TR') ");
            sb.AppendLine(" EXEC(@dropTrigger) ");
            sb.AppendLine(" IF EXISTS(SELECT tableName FROM AspNet_SqlCacheTablesForChangeNotification WHERE tableName = @tableName) ");
            sb.AppendLine(" EXEC(@triggerCreate) ");

            return sb.ToString();

        }

        /// <summary> 
        /// Returns the script needed to enable SqlCacheDependency 
        /// </summary> 
        /// <returns></returns> 
        /// <remarks></remarks> 
        private static string CreateSqlCacheDependencyScript() {
            StringBuilder sql = new StringBuilder();

            sql.AppendLine("SET ARITHABORT ON ");

            /* Create SQL Cache Table */
            sql.AppendLine(" IF NOT EXISTS (SELECT name FROM sysobjects WITH (NOLOCK) WHERE name = 'AspNet_SqlCacheTablesForChangeNotification' AND type = 'U') ");
            sql.AppendLine(" IF NOT EXISTS (SELECT name FROM sysobjects WITH (TABLOCKX) WHERE name = 'AspNet_SqlCacheTablesForChangeNotification' AND type = 'U') ");
            sql.AppendLine(" CREATE TABLE dbo.AspNet_SqlCacheTablesForChangeNotification ( ");
            sql.AppendLine(" tableName NVARCHAR(450) NOT NULL PRIMARY KEY, ");
            sql.AppendLine(" notificationCreated DATETIME NOT NULL DEFAULT(GETDATE()), ");
            sql.AppendLine(" changeId INT NOT NULL DEFAULT(0) ");
            sql.AppendLine(" ) ");
            /* Create polling SP */
            sql.AppendLine(" IF NOT EXISTS (SELECT name FROM sysobjects WITH (NOLOCK) WHERE name = 'AspNet_SqlCachePollingStoredProcedure' AND type = 'P') ");
            sql.AppendLine(" IF NOT EXISTS (SELECT name FROM sysobjects WITH (TABLOCKX) WHERE name = 'AspNet_SqlCachePollingStoredProcedure' AND type = 'P') ");
            sql.AppendLine(" EXEC('CREATE PROCEDURE dbo.AspNet_SqlCachePollingStoredProcedure AS ");
            sql.AppendLine(" SELECT tableName, changeId FROM dbo.AspNet_SqlCacheTablesForChangeNotification ");
            sql.AppendLine(" RETURN 0') ");
            /* Create SP for registering a table. */
            sql.AppendLine(" IF NOT EXISTS (SELECT name FROM sysobjects WITH (NOLOCK) WHERE name = 'AspNet_SqlCacheRegisterTableStoredProcedure' AND type = 'P') ");
            sql.AppendLine(" IF NOT EXISTS (SELECT name FROM sysobjects WITH (TABLOCKX) WHERE name = 'AspNet_SqlCacheRegisterTableStoredProcedure' AND type = 'P') ");
            sql.AppendLine(" EXEC('CREATE PROCEDURE dbo.AspNet_SqlCacheRegisterTableStoredProcedure ");
            sql.AppendLine(" @tableName NVARCHAR(450) ");
            sql.AppendLine(" AS ");
            sql.AppendLine(" BEGIN ");
            sql.AppendLine(" DECLARE @triggerName AS NVARCHAR(3000) ");
            sql.AppendLine(" DECLARE @fullTriggerName AS NVARCHAR(3000) ");
            sql.AppendLine(" DECLARE @canonTableName NVARCHAR(3000) ");
            sql.AppendLine(" DECLARE @quotedTableName NVARCHAR(3000) ");
            sql.AppendLine(" DECLARE @schemaName NVARCHAR(3000) ");
            sql.AppendLine(" IF(CHARINDEX(''.'',@tableName) <> 0)  ");
            sql.AppendLine(" BEGIN ");
            sql.AppendLine(" SET @schemaName = SUBSTRING(@tableName,0,CHARINDEX(''.'',@tableName)) ");
            sql.AppendLine(" SET @tableName =  SUBSTRING(@tableName,CHARINDEX(''.'',@tableName) + 1,LEN(@tableName) - CHARINDEX(''.'',@tableName)) ");
            sql.AppendLine(" END ");
            sql.AppendLine(" ELSE ");
            sql.AppendLine(" BEGIN ");
            sql.AppendLine(" SELECT @schemaName = default_schema_name ");
            sql.AppendLine(" FROM sys.database_principals WHERE type = ''S'' ");
            sql.AppendLine(" AND name = USER_NAME(); ");
            sql.AppendLine(" END ");
            /* Create the trigger name */
            sql.AppendLine(" SET @triggerName = REPLACE(@tableName, ''['', ''__o__'') ");
            sql.AppendLine(" SET @triggerName = REPLACE(@triggerName, '']'', ''__c__'') ");
            sql.AppendLine(" SET @triggerName = @triggerName + ''_AspNet_SqlCacheNotification_Trigger'' ");
            sql.AppendLine(" IF(@schemaName IS NOT NULL) ");
            sql.AppendLine(" SET @fullTriggerName =''['' + @schemaName + ''].['' + @triggerName + '']''  ");
            sql.AppendLine(" ELSE ");
            sql.AppendLine(" SET @fullTriggerName = ''dbo.['' + @triggerName + '']''  ");
            /* Create the cannonicalized table name for trigger creation */
            /* Do not touch it if the name contains other delimiters */
            sql.AppendLine(" IF (CHARINDEX(''.'', @tableName) <> 0 OR ");
            sql.AppendLine(" CHARINDEX(''['', @tableName) <> 0 OR ");
            sql.AppendLine(" CHARINDEX('']'', @tableName) <> 0) ");
            sql.AppendLine(" SET @canonTableName = @tableName ");
            sql.AppendLine(" ELSE ");
            sql.AppendLine(" SET @canonTableName = ''['' + @tableName + '']'' ");
            /* First make sure the table exists */
            sql.AppendLine(" IF(@schemaName IS NULL) ");
            sql.AppendLine(" BEGIN ");
            sql.AppendLine(" IF (SELECT OBJECT_ID(@tableName, ''U'')) IS NULL  ");
            sql.AppendLine(" BEGIN  ");
            sql.AppendLine(" RAISERROR (''00000001'', 16, 1)  ");
            sql.AppendLine(" RETURN  ");
            sql.AppendLine(" END ");
            sql.AppendLine(" END ");
            sql.AppendLine(" ELSE ");
            sql.AppendLine(" BEGIN ");
            sql.AppendLine(" IF (SELECT OBJECT_ID(@schemaName + ''.'' + @tableName, ''U'')) IS NULL  ");
            sql.AppendLine(" BEGIN  ");
            sql.AppendLine(" RAISERROR (''00000001'', 16, 1)  ");
            sql.AppendLine(" RETURN  ");
            sql.AppendLine(" END  ");
            sql.AppendLine(" END ");
            sql.AppendLine(" BEGIN TRAN ");
            /* Insert the value into the notification table */
            sql.AppendLine(" IF NOT EXISTS (SELECT tableName FROM dbo.AspNet_SqlCacheTablesForChangeNotification WITH (NOLOCK) WHERE tableName = @tableName) ");
            sql.AppendLine(" IF NOT EXISTS (SELECT tableName FROM dbo.AspNet_SqlCacheTablesForChangeNotification WITH (TABLOCKX) WHERE tableName = @tableName) ");
            sql.AppendLine(" INSERT dbo.AspNet_SqlCacheTablesForChangeNotification ");
            sql.AppendLine(" VALUES (@tableName, GETDATE(), 0) ");
            /* Create the trigger */
            sql.AppendLine(" SET @quotedTableName = QUOTENAME(@tableName, '''''''') ");
            sql.AppendLine(" IF NOT EXISTS (SELECT name FROM sysobjects WITH (NOLOCK) WHERE name = @triggerName AND type = ''TR'') ");
            sql.AppendLine(" IF NOT EXISTS (SELECT name FROM sysobjects WITH (TABLOCKX) WHERE name = @triggerName AND type = ''TR'') ");
            sql.AppendLine(" EXEC(''CREATE TRIGGER '' + @fullTriggerName + '' ON '' + @canonTableName +'' ");
            sql.AppendLine(" FOR INSERT, UPDATE, DELETE AS BEGIN ");
            sql.AppendLine(" SET NOCOUNT ON ");
            sql.AppendLine(" EXEC AspNet_SqlCacheUpdateChangeIdStoredProcedure N'' + @quotedTableName + '' ");
            sql.AppendLine(" END ");
            sql.AppendLine(" '') ");
            sql.AppendLine(" COMMIT TRAN ");
            sql.AppendLine(" END ");
            sql.AppendLine(" ') ");
            /* Create SP for updating the change Id of a table. */
            sql.AppendLine(" IF NOT EXISTS (SELECT name FROM sysobjects WITH (NOLOCK) WHERE name = 'AspNet_SqlCacheUpdateChangeIdStoredProcedure' AND type = 'P') ");
            sql.AppendLine(" IF NOT EXISTS (SELECT name FROM sysobjects WITH (TABLOCKX) WHERE name = 'AspNet_SqlCacheUpdateChangeIdStoredProcedure' AND type = 'P') ");
            sql.AppendLine(" EXEC('CREATE PROCEDURE dbo.AspNet_SqlCacheUpdateChangeIdStoredProcedure ");
            sql.AppendLine(" @tableName NVARCHAR(450) ");
            sql.AppendLine(" AS ");
            sql.AppendLine(" BEGIN ");
            sql.AppendLine(" UPDATE dbo.AspNet_SqlCacheTablesForChangeNotification WITH (ROWLOCK) SET changeId = changeId + 1 ");
            sql.AppendLine(" WHERE tableName = @tableName ");
            sql.AppendLine(" END ");
            sql.AppendLine(" ') ");
            /* Create SP for unregistering a table. */
            sql.AppendLine(" IF NOT EXISTS (SELECT name FROM sysobjects WITH (NOLOCK) WHERE name = 'AspNet_SqlCacheUnRegisterTableStoredProcedure' AND type = 'P') ");
            sql.AppendLine(" IF NOT EXISTS (SELECT name FROM sysobjects WITH (TABLOCKX) WHERE name = 'AspNet_SqlCacheUnRegisterTableStoredProcedure' AND type = 'P') ");
            sql.AppendLine(" EXEC('CREATE PROCEDURE dbo.AspNet_SqlCacheUnRegisterTableStoredProcedure ");
            sql.AppendLine(" @tableName NVARCHAR(450) ");
            sql.AppendLine(" AS ");
            sql.AppendLine(" BEGIN ");
            sql.AppendLine(" BEGIN TRAN ");
            sql.AppendLine(" DECLARE @triggerName AS NVARCHAR(3000) ");
            sql.AppendLine(" DECLARE @fullTriggerName AS NVARCHAR(3000) ");
            sql.AppendLine(" SET @triggerName = REPLACE(@tableName, ''['', ''__o__'') ");
            sql.AppendLine(" SET @triggerName = REPLACE(@triggerName, '']'', ''__c__'') ");
            sql.AppendLine(" SET @triggerName = @triggerName + ''_AspNet_SqlCacheNotification_Trigger'' ");
            sql.AppendLine(" SET @fullTriggerName = ''['' + @triggerName + '']'' ");
            /* Remove the table-row from the notification table */
            sql.AppendLine(" IF EXISTS (SELECT name FROM sysobjects WITH (NOLOCK) WHERE name = ''AspNet_SqlCacheTablesForChangeNotification'' AND type = ''U'') ");
            sql.AppendLine(" IF EXISTS (SELECT name FROM sysobjects WITH (TABLOCKX) WHERE name = ''AspNet_SqlCacheTablesForChangeNotification'' AND type = ''U'') ");
            sql.AppendLine(" DELETE FROM dbo.AspNet_SqlCacheTablesForChangeNotification WHERE tableName = @tableName ");
            /* Remove the trigger */
            sql.AppendLine(" IF EXISTS (SELECT name FROM sysobjects WITH (NOLOCK) WHERE name = @triggerName AND type = ''TR'') ");
            sql.AppendLine(" IF EXISTS (SELECT name FROM sysobjects WITH (TABLOCKX) WHERE name = @triggerName AND type = ''TR'') ");
            sql.AppendLine(" EXEC(''DROP TRIGGER '' + @fullTriggerName) ");
            sql.AppendLine(" COMMIT TRAN ");
            sql.AppendLine(" END ");
            sql.AppendLine(" ') ");
            /* Create SP for querying all registered table */
            sql.AppendLine(" IF NOT EXISTS (SELECT name FROM sysobjects WITH (NOLOCK) WHERE name = 'AspNet_SqlCacheQueryRegisteredTablesStoredProcedure' AND type = 'P') ");
            sql.AppendLine(" IF NOT EXISTS (SELECT name FROM sysobjects WITH (TABLOCKX) WHERE name = 'AspNet_SqlCacheQueryRegisteredTablesStoredProcedure' AND type = 'P') ");
            sql.AppendLine(" EXEC('CREATE PROCEDURE dbo.AspNet_SqlCacheQueryRegisteredTablesStoredProcedure ");
            sql.AppendLine(" AS ");
            sql.AppendLine(" SELECT tableName FROM dbo.AspNet_SqlCacheTablesForChangeNotification ') ");
            /* Create roles and grant them access to SP */
            sql.AppendLine(" IF NOT EXISTS (SELECT name FROM sysusers WHERE issqlrole = 1 AND name = N'aspnet_ChangeNotification_ReceiveNotificationsOnlyAccess') ");
            sql.AppendLine(" EXEC sp_addrole N'aspnet_ChangeNotification_ReceiveNotificationsOnlyAccess' ");
            sql.AppendLine(" GRANT EXECUTE ON AspNet_SqlCachePollingStoredProcedure to aspnet_ChangeNotification_ReceiveNotificationsOnlyAccess ");

            return sql.ToString();
        }


        private static string DeleteCahceDependencyScript() {

            StringBuilder sb = new StringBuilder();

            /* Drop triggers on cache dependency enabled tables and the RestoreCache SP */

            sb.AppendLine("IF EXISTS (SELECT name FROM sysobjects WITH (NOLOCK) WHERE name = 'AspNet_SqlCacheTablesForChangeNotification' AND type = 'U') ");
            sb.AppendLine("IF EXISTS (SELECT name FROM sysobjects WITH (TABLOCKX) WHERE name = 'AspNet_SqlCacheTablesForChangeNotification' AND type = 'U') ");
            sb.AppendLine("BEGIN ");
            sb.AppendLine("DECLARE @triggerName NVARCHAR(450) ");
            sb.AppendLine("DECLARE @fullTriggerName NVARCHAR(450) ");

            sb.AppendLine("IF EXISTS( ");
            sb.AppendLine("  SELECT permission_name ");
            sb.AppendLine("  FROM fn_my_permissions('dbo.AspNet_SqlCacheTablesForChangeNotification', 'OBJECT') ");
            sb.AppendLine("  WHERE permission_name = 'SELECT') ");
            sb.AppendLine("  BEGIN ");

            sb.AppendLine("  DECLARE TGCursor CURSOR FOR ");
            sb.AppendLine("  SELECT QUOTENAME(SCHEMA_NAME(schema_id)) + '.[' + name + '_AspNet_SqlCacheNotification_Trigger]', name + '_AspNet_SqlCacheNotification_Trigger' ");
            sb.AppendLine("  AS SchemaTable ");
            sb.AppendLine("  FROM sys.tables ");
            sb.AppendLine("  WHERE name IN(SELECT tableName FROM AspNet_SqlCacheTablesForChangeNotification) ");
            sb.AppendLine("  OPEN TGCursor ");
            sb.AppendLine("  FETCH NEXT FROM TGCursor INTO @fullTriggerName, @triggerName ");

            sb.AppendLine("  WHILE @@FETCH_STATUS = 0 ");
            sb.AppendLine("  BEGIN ");
            sb.AppendLine("    IF EXISTS(SELECT name FROM sysobjects WHERE name = @triggerName AND type = 'TR') ");
            sb.AppendLine("      BEGIN ");
            sb.AppendLine("        EXEC('DROP TRIGGER ' + @fullTriggerName) ");
            sb.AppendLine("      END ");
            sb.AppendLine("      FETCH next FROM TGCursor INTO @fullTriggerName, @triggerName ");
            sb.AppendLine("  END ");
            sb.AppendLine("  CLOSE TGCursor ");
            sb.AppendLine("  DEALLOCATE TGCursor ");
            sb.AppendLine("  END ");

            sb.AppendLine("  ELSE ");
            sb.AppendLine("  BEGIN ");
            sb.AppendLine("    DECLARE @tableName NVARCHAR(450) ");
            sb.AppendLine("    DECLARE @lastPart NVARCHAR(50) ");
            sb.AppendLine("    SET @lastPart = '_AspNet_SqlCacheNotification_Trigger' ");
            sb.AppendLine("    DECLARE TGCursor CURSOR FOR ");
            sb.AppendLine("    SELECT name FROM sys.triggers WHERE name LIKE '%' + @lastPart ");
            sb.AppendLine("    OPEN TGCursor ");
            sb.AppendLine("    FETCH NEXT FROM TGCursor INTO @triggerName ");
            sb.AppendLine("    WHILE @@FETCH_STATUS = 0 ");
            sb.AppendLine("    BEGIN ");
            sb.AppendLine("      BEGIN ");
            sb.AppendLine("        SET @tableName = SUBSTRING(@triggerName, 0, CHARINDEX('_', @triggerName)) ");
            sb.AppendLine("        DECLARE TCursor CURSOR FOR ");
            sb.AppendLine("        SELECT TOP 1 QUOTENAME(SCHEMA_NAME(schema_id)) + '.[' + name + @lastPart + ']' FROM sys.tables WHERE name = @tableName ");
            sb.AppendLine("        OPEN TCursor ");
            sb.AppendLine("        FETCH NEXT FROM TCursor INTO @tableName ");
            sb.AppendLine("        WHILE @@FETCH_STATUS = 0 ");
            sb.AppendLine("        BEGIN ");
            sb.AppendLine("          IF EXISTS(SELECT name FROM sysobjects WHERE name = @triggerName AND type = 'TR') ");
            sb.AppendLine("            BEGIN ");
            sb.AppendLine("              EXEC('DROP TRIGGER ' + @tableName) ");
            sb.AppendLine("            END ");
            sb.AppendLine("          FETCH NEXT FROM TCursor INTO @tableName ");
            sb.AppendLine("        END ");
            sb.AppendLine("        CLOSE TCursor ");
            sb.AppendLine("        DEALLOCATE TCursor ");
            sb.AppendLine("      END	");
            sb.AppendLine("      FETCH next FROM TGCursor INTO @triggerName ");
            sb.AppendLine("    END ");
            sb.AppendLine("    CLOSE TGCursor ");
            sb.AppendLine("    DEALLOCATE TGCursor ");
            sb.AppendLine("  END ");

            sb.AppendLine("  DECLARE @fullProcName NVARCHAR(450) ");
            sb.AppendLine("  IF EXISTS(SELECT name FROM sys.procedures WHERE name = 'RestoreCache') ");
            sb.AppendLine("  BEGIN ");
            sb.AppendLine("    SELECT @fullProcName = QUOTENAME(SCHEMA_NAME(schema_id)) + '.' + QUOTENAME(name)  ");
            sb.AppendLine("    FROM sys.procedures ");
            sb.AppendLine("    WHERE name = 'RestoreCache'	 ");
            sb.AppendLine("    EXEC('DROP PROCEDURE ' + @fullProcName) ");
            sb.AppendLine("  END ");
            sb.AppendLine("END ");

            /* Drop the AspNet_SqlCacheTablesForChangeNotification table and the Asp_NetSqlCachePolling SP's */

            sb.AppendLine("IF EXISTS (SELECT name FROM sysobjects WITH (NOLOCK) WHERE name = 'AspNet_SqlCacheTablesForChangeNotification' AND type = 'U') ");
            sb.AppendLine("IF EXISTS (SELECT name FROM sysobjects WITH (TABLOCKX) WHERE name = 'AspNet_SqlCacheTablesForChangeNotification' AND type = 'U') ");
            sb.AppendLine("DROP TABLE AspNet_SqlCacheTablesForChangeNotification ");

            sb.AppendLine("IF EXISTS (SELECT name FROM sysobjects WITH (NOLOCK) WHERE name = 'AspNet_SqlCachePollingStoredProcedure' AND type = 'P') ");
            sb.AppendLine("IF EXISTS (SELECT name FROM sysobjects WITH (TABLOCKX) WHERE name = 'AspNet_SqlCachePollingStoredProcedure' AND type = 'P') ");
            sb.AppendLine("DROP PROCEDURE AspNet_SqlCachePollingStoredProcedure ");

            sb.AppendLine("IF EXISTS (SELECT name FROM sysobjects WITH (NOLOCK) WHERE name = 'AspNet_SqlCacheRegisterTableStoredProcedure' AND type = 'P') ");
            sb.AppendLine("IF EXISTS (SELECT name FROM sysobjects WITH (TABLOCKX) WHERE name = 'AspNet_SqlCacheRegisterTableStoredProcedure' AND type = 'P') ");
            sb.AppendLine("DROP PROCEDURE AspNet_SqlCacheRegisterTableStoredProcedure  ");

            sb.AppendLine("IF EXISTS (SELECT name FROM sysobjects WITH (NOLOCK) WHERE name = 'AspNet_SqlCacheQueryRegisteredTablesStoredProcedure' AND type = 'P') ");
            sb.AppendLine("IF EXISTS (SELECT name FROM sysobjects WITH (TABLOCKX) WHERE name = 'AspNet_SqlCacheQueryRegisteredTablesStoredProcedure' AND type = 'P') ");
            sb.AppendLine("DROP PROCEDURE AspNet_SqlCacheQueryRegisteredTablesStoredProcedure ");

            sb.AppendLine("IF EXISTS (SELECT name FROM sysobjects WITH (NOLOCK) WHERE name = 'AspNet_SqlCacheUnRegisterTableStoredProcedure' AND type = 'P') ");
            sb.AppendLine("IF EXISTS (SELECT name FROM sysobjects WITH (TABLOCKX) WHERE name = 'AspNet_SqlCacheUnRegisterTableStoredProcedure' AND type = 'P') ");
            sb.AppendLine("DROP PROCEDURE AspNet_SqlCacheUnRegisterTableStoredProcedure ");

            sb.AppendLine("IF EXISTS (SELECT name FROM sysobjects WITH (NOLOCK) WHERE name = 'AspNet_SqlCacheUpdateChangeIdStoredProcedure' AND type = 'P') ");
            sb.AppendLine("IF EXISTS (SELECT name FROM sysobjects WITH (TABLOCKX) WHERE name = 'AspNet_SqlCacheUpdateChangeIdStoredProcedure' AND type = 'P') ");
            sb.AppendLine("DROP PROCEDURE AspNet_SqlCacheUpdateChangeIdStoredProcedure ");


            /* DROP THE DATABASE TRIGGER */
            sb.AppendLine("IF EXISTS(");
            sb.AppendLine("SELECT permission_name FROM fn_my_permissions(NULL, 'DATABASE') " +
                "WHERE permission_name = 'ALTER ANY DATABASE DDL TRIGGER'");
            sb.AppendLine(")");
            sb.AppendLine("IF EXISTS(SELECT name FROM sys.triggers WHERE name = 'TableCreated' AND type = 'TR') ");
            sb.AppendLine("DROP TRIGGER TableCreated ON DATABASE ");

            return sb.ToString();
        }

        #endregion

    }
}
