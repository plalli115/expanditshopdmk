﻿namespace EISCS.ExpandITFramework.Infrastructure.SqlDependencyCaching.Interfaces {
    public interface ISqlCacheDependencyDatabaseInfrastructureManager {
        string CreateDdlTrigger();
        bool CreateSpRestoreCache();
        bool CreateSqlCacheDependencyStructure();
        string DeleteSqlCacheDependencyStructure();
    }
}
