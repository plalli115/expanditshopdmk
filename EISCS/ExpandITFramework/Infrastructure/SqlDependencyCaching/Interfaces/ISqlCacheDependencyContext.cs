﻿using System;
using System.Web.Caching;

namespace EISCS.ExpandITFramework.Infrastructure.SqlDependencyCaching.Interfaces {
    public interface ISqlCacheDependencyContext {
        string MapPath(string path);
        void CacheInsert(string key, object value, CacheDependency dependencies);
        void CacheInsert(string key, object value, CacheDependency dependencies,
            DateTime absoluteExpiration, TimeSpan slidingExpiration);
        void CacheInsert(string key, object value, CacheDependency dependencies,
            DateTime absoluteExpiration, TimeSpan slidingExpiration, CacheItemUpdateCallback onUpdateCallback);
        void CacheInsert(string key, object value, CacheDependency dependencies,
            DateTime absoluteExpiration, TimeSpan slidingExpiration, CacheItemPriority priority, CacheItemRemovedCallback onRemoveCallback);

        object CacheGet(string key);
    }
}
