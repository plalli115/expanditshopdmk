﻿namespace EISCS.ExpandITFramework.Infrastructure.SqlDependencyCaching.Interfaces {
    public interface ISqlCacheDependencyConfigSettingsReader {
        string ReadConfigCacheSettings();
    }
}