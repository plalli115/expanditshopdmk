﻿using System;

namespace EISCS.ExpandITFramework.Infrastructure.SqlDependencyCaching.Interfaces {
    public interface ISqlCacheDependencyManager {
        bool CacheEnabled(string tableName);
        bool CacheSet(string key, object v, string tableName);
        object CacheGet(string key);
        bool CacheSetAggregated(string key, object v, string[] tableNames);
        bool CacheSetAggregated(string key, object v, string[] tableNames, System.Web.Caching.CacheItemRemovedCallback rem);
        string DataBaseName { get; }
        bool DisableCaching(string tableName);
        bool DisableCaching(string[] tableNames);
        bool EnableCaching(string tableName);
        bool EnableCaching(string[] tableNames);
        bool InitializeCachedTables();
        bool InsertIntoCache(string key, object cacheObject, System.Web.Caching.AggregateCacheDependency aggDependency);
        bool InsertIntoCache(string key, object cacheObject, System.Web.Caching.CacheDependency dependency, DateTime d, TimeSpan t, System.Web.Caching.CacheItemPriority cip, System.Web.Caching.CacheItemRemovedCallback circ);
        bool InsertIntoCache(string key, string tableName, object cacheObject);
        bool InsertIntoCache(string tableName, object cacheObject);
        void TablesRecreated(System.Collections.ArrayList tableArray);
    }
}
