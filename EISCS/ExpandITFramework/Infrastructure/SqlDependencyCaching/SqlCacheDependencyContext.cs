﻿using System;
using System.Web;
using System.Web.Caching;
using EISCS.ExpandITFramework.Infrastructure.SqlDependencyCaching.Interfaces;
using EISCS.Wrappers.Http;

namespace EISCS.ExpandITFramework.Infrastructure.SqlDependencyCaching {
    public class SqlCacheDependencyContext : ISqlCacheDependencyContext, ICache {

        #region ISqlCacheDependencyContext Members

        public string MapPath(string path) {
            return HttpContext.Current.Server.MapPath(path);
        }        

        public void CacheInsert(string key, object value, CacheDependency dependencies) {
            HttpContext.Current.Cache.Insert(key, value, dependencies);
        }

        public void CacheInsert(string key, object value, CacheDependency dependencies, 
            DateTime absoluteExpiration, TimeSpan slidingExpiration) {
            HttpContext.Current.Cache.Insert(key, value, dependencies, absoluteExpiration, slidingExpiration);
        }

        public void CacheInsert(string key, object value, CacheDependency dependencies, 
            DateTime absoluteExpiration, TimeSpan slidingExpiration, CacheItemUpdateCallback onUpdateCallback) {
            HttpContext.Current.Cache.Insert(key, value, dependencies, absoluteExpiration, slidingExpiration, onUpdateCallback);
        }

        public void CacheInsert(string key, object value, CacheDependency dependencies, 
            DateTime absoluteExpiration, TimeSpan slidingExpiration, CacheItemPriority priority, CacheItemRemovedCallback onRemoveCallback) {
            HttpContext.Current.Cache.Insert(key, value, dependencies, absoluteExpiration, slidingExpiration, priority, onRemoveCallback);
        }

        public object CacheGet(string key)
        {
            return HttpContext.Current.Cache.Get(key);
        }

        #endregion

        #region ICache Members

        public void Add(string key, object value, CacheDependency dependencies, 
            DateTime absoluteExpiration, TimeSpan slidingExpiration, CacheItemPriority priority, CacheItemRemovedCallback onRemoveCallback) {
                HttpContext.Current.Cache.Add(key, value, dependencies, absoluteExpiration, slidingExpiration, priority, onRemoveCallback);
        }

        public void Count() {
            throw new NotImplementedException();
        }

        public void Insert() {
            throw new NotImplementedException();
        }

        #endregion
    }
}
