﻿using System;
using System.Data;
using System.Text;
using System.Threading;
using System.Web.Caching;
using EISCS.ExpandITFramework.Infrastructure.Configuration.Interfaces;
using EISCS.ExpandITFramework.Infrastructure.SqlDependencyCaching.Interfaces;
using EISCS.ExpandITFramework.Util;
using EISCS.Shop.DO.Service;

namespace EISCS.ExpandITFramework.Infrastructure.SqlDependencyCaching {

    enum SqlCacheDependencyMode {
        Unitialized = 0,
        MissingRoles = 1,
        MissingPermissions = 2,
        AllSet = 3,
        Disaster = 4
    };

    public class SqlCacheDependencyManager : ISqlCacheDependencyManager {

        private SqlCacheDependencyMode _cacheDependencyMode;
        private readonly ISqlCacheDependencyConfigSettingsReader _sqlCacheDependencyConfigSettingsReader;
        private readonly ISqlCacheDependencyContext _context;
        private readonly ISqlCacheDependencyDatabaseInfrastructureManager _sqlCacheDepInfraManager;
        private readonly ISqlCacheDependencyPermissionChecker _permChecker;
        private readonly ILegacyDataService _sqlExecutor;
        private readonly IConfigurationValue _configurationValue;

        private readonly string[] _aggregatedTables;
        private readonly string[] _singleTables;
        private readonly bool _isLocalMode;
        private readonly bool _isLocalModeUseCache;

        private bool _hasDdlPermission;
        private string _dbName;

        private bool IsInitialized() {
            return _cacheDependencyMode != SqlCacheDependencyMode.Unitialized;
        }

        /// <summary>
        /// Checks database user roles and permissions and creates the SqlCacheDependency if permitted
        /// This method is expected to be called called once in the applications lifetime
        /// </summary>
        /// <returns></returns>
        private SqlCacheDependencyMode Initialize() {

            if (IsInitialized()) {
                return _cacheDependencyMode;
            }

            /*
            * 1. Check Roles - If correct roles Then continue Else set flag to "MissingRoles" and return
            * 2. Create dbo.AspNet_SqlCacheTablesForChangeNotification table and sp's
            * 3. Check for presence of table and sp's
            * 4. Check Permissions on table and sp's - If permissions OK Then set flag to "AllSet" and create triggers 
            *    Else set flag to "MissingPermissions" and run without Caching
            */

            if (!_permChecker.CheckRoles()) {
                // User not in the required roles - no reason to even try to setup SqlCacheDependecy since it will fail anyway 
                _cacheDependencyMode = SqlCacheDependencyMode.MissingRoles;
                return _cacheDependencyMode;
            }
            // User is in the required roles - go ahead and create table and SP's            
            _sqlCacheDepInfraManager.CreateSqlCacheDependencyStructure();
            // Check for DDL trigger permission
            _hasDdlPermission = _permChecker.CheckDdlTriggerPermission();
            // Check Permissions on newly created dbo.AspNet_SqlCacheTablesForChangeNotification table
            // and SP's in the dbo schema
            if (_permChecker.CheckSqlCacheDependencyPermissions()) {
                if (_hasDdlPermission) {
                    _sqlCacheDepInfraManager.CreateDdlTrigger();
                }
                // All permissions are in place - create the RestoreCache SP
                // and enable SqlCacheDependency caching
                _sqlCacheDepInfraManager.CreateSpRestoreCache();
                _cacheDependencyMode = SqlCacheDependencyMode.AllSet;
            }
            else {
                // Leave it as it is - No SqlDependency caching can/will be used.
                // Make sure to ask your DBA for permissions on
                // the dbo.AspNet_SqlCacheTablesForChangeNotification table 
                // and the SP's you have created in the dbo schema.
                _cacheDependencyMode = SqlCacheDependencyMode.MissingPermissions;
            }

            // If ALL_SET enable caching on tables marked for dependency caching in web.config
            Thread.Sleep(500);
            InternalInitializeCachedTables();

            return _cacheDependencyMode;
        }

        private bool InternalInitializeCachedTables() {
            bool isAggregateSuccess = false;
            bool isSingleSuccess = false;
            if (_cacheDependencyMode.Equals(SqlCacheDependencyMode.AllSet)) {
                isAggregateSuccess = EnableCaching(_aggregatedTables);
                isSingleSuccess = EnableCaching(_singleTables);
            }
            return isAggregateSuccess && isSingleSuccess;
        }

        public SqlCacheDependencyManager(ISqlCacheDependencyContext context, ISqlCacheDependencyConfigSettingsReader configReader,
            ISqlCacheDependencyDatabaseInfrastructureManager sqlCacheDependencyInfraManager,
            ISqlCacheDependencyPermissionChecker permissionChecker, ILegacyDataService sqlExecutor, IConfigurationValue confVal) {
            // Set dependencies
            _context = context;
            _sqlCacheDependencyConfigSettingsReader = configReader;
            _sqlCacheDepInfraManager = sqlCacheDependencyInfraManager;
            _permChecker = permissionChecker;
            _sqlExecutor = sqlExecutor;
            _configurationValue = confVal;
            // Read configuration values
            try {
                _aggregatedTables = Utilities.Split(_configurationValue.AppSettings("GroupProductRelatedTablesForCaching"), "|");
                _singleTables = Utilities.Split(_configurationValue.AppSettings("TablesForCaching"), "|");
                _isLocalModeUseCache = Utilities.CBoolEx(_configurationValue.AppSettings("LOCALMODE_USE_CACHE"));
                _isLocalMode = _sqlExecutor.IsLocalMode;
            }
            catch (Exception) {
                _cacheDependencyMode = SqlCacheDependencyMode.Disaster;
                return;
            }
            // Set initial state
            _cacheDependencyMode = SqlCacheDependencyMode.Unitialized;
        }

        #region ISqlCacheDependencyManager Members

        public bool CacheEnabled(string tableName) {
            Initialize();
            if (!_cacheDependencyMode.Equals(SqlCacheDependencyMode.AllSet)) {
                return false;
            }
            string sql = "SELECT TOP 1 tableName " +
                         "FROM AspNet_SqlCacheTablesForChangeNotification " +
                         "WHERE tableName = " + Utilities.SafeString(tableName);

            return _sqlExecutor.GetSingleValueDb<string>(sql) != null;

        }

        public bool CacheSet(string key, object v, string tableName) {
            Initialize();
            bool isSuccess = false;
            if (_cacheDependencyMode.Equals(SqlCacheDependencyMode.AllSet)) {
                if (!_isLocalMode || _isLocalModeUseCache) {
                    try {
                        isSuccess = InsertIntoCache(key, tableName, v);
                    }
                    catch (Exception) {
                        return false;
                    }
                }
            }
            return isSuccess;
        }

        public object CacheGet(string key)
        {
            object toReturn;
            try
            {
                toReturn = _context.CacheGet(key);
            }
            catch (Exception)
            {
                return null;
            }
            return toReturn;
        }

        public bool CacheSetAggregated(string key, object v, string[] tableNames) {
            Initialize();
            bool isSuccess = false;
            if (_cacheDependencyMode.Equals(SqlCacheDependencyMode.AllSet)) {
                if (tableNames != null) {
                    if (!_isLocalMode || _isLocalModeUseCache) {
                        using (AggregateCacheDependency aggDep = new AggregateCacheDependency()) {
                            int counter = 0;
                            string dbName = DataBaseName;
                            for (int i = 0; i < tableNames.Length; i++) {
                                string tableName = tableNames[i];
                                if (!CacheEnabled(tableName)) continue;
                                using (SqlCacheDependency dependency = new SqlCacheDependency(dbName, tableName)) {
                                    aggDep.Add(dependency);
                                    counter++;
                                }
                            }
                            if (counter == tableNames.Length)
                                isSuccess = InsertIntoCache(key, v, aggDep);
                        }
                    }
                }
            }
            return isSuccess;
        }

        public bool CacheSetAggregated(string key, object v, string[] tableNames, CacheItemRemovedCallback rem) {
            Initialize();
            bool isSuccess = false;
            if (_cacheDependencyMode.Equals(SqlCacheDependencyMode.AllSet)) {
                if (tableNames != null) {
                    if (!_isLocalMode || _isLocalModeUseCache) {
                        using (AggregateCacheDependency aggDep = new AggregateCacheDependency()) {
                            int counter = 0;
                            string dbName = DataBaseName;
                            for (int i = 0; i < tableNames.Length; i++) {
                                string tableName = tableNames[i];
                                if (!CacheEnabled(tableName)) continue;
                                aggDep.Add(new SqlCacheDependency(dbName, tableName));
                                counter++;
                            }
                            if (counter == tableNames.Length)
                                isSuccess = InsertIntoCache(key, v, aggDep, Cache.NoAbsoluteExpiration, TimeSpan.Zero, CacheItemPriority.High, rem);
                        }
                    }
                }
            }
            return isSuccess;
        }

        public string DataBaseName {
            get {
                if (string.IsNullOrEmpty(_dbName)) {
                    _dbName = _sqlCacheDependencyConfigSettingsReader.ReadConfigCacheSettings();
                }
                return _dbName;
            }
        }

        public bool DisableCaching(string tableName) {
            Initialize();
            if (_cacheDependencyMode.Equals(SqlCacheDependencyMode.AllSet)) {
                string sql = "exec AspNet_SqlCacheUnRegisterTableStoredProcedure N'" + tableName + "'";
                try
                {
                    _sqlExecutor.ExcecuteNonQueryDb(sql);
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
            return false;
        }

        public bool DisableCaching(string[] tableNames) {
            Initialize();
            bool isSuccess = false;
            if (_cacheDependencyMode.Equals(SqlCacheDependencyMode.AllSet)) {
                if (tableNames != null) {
                    for (int i = 0; i <= tableNames.Length - 1; i++) {
                        isSuccess = DisableCaching(tableNames[i]);
                    }
                }
            }
            return isSuccess;
        }

        public bool EnableCaching(string tableName) {
            Initialize();
            if (_cacheDependencyMode.Equals(SqlCacheDependencyMode.AllSet)) {
                try
                {
                    string sql = "exec AspNet_SqlCacheRegisterTableStoredProcedure N'" + tableName + "'";
                    _sqlExecutor.ExcecuteNonQueryDb(sql);
                    return true;
                }
                catch (System.Security.SecurityException)
                {
                    return false;
                }
                catch (Exception)
                {
                    return false;
                }
            }
            return false;
        }

        public bool EnableCaching(string[] tableNames) {
            Initialize();
            bool isSuccess = false;
            if (_cacheDependencyMode.Equals(SqlCacheDependencyMode.AllSet)) {
                for (int i = 0; i < tableNames.Length; i++) {
                    isSuccess = EnableCaching(tableNames[i]);
                }
            }
            return isSuccess;
        }

        public bool InitializeCachedTables() {
            Initialize();
            return InternalInitializeCachedTables();
        }

        public bool InsertIntoCache(string key, object cacheObject, AggregateCacheDependency aggDependency) {
            Initialize();
            if (!_cacheDependencyMode.Equals(SqlCacheDependencyMode.AllSet)) {
                return false;
            }
            try {
                _context.CacheInsert(key, cacheObject, aggDependency);
                return true;
            }
            catch (Exception) {
                return false;
            }
        }

        public bool InsertIntoCache(string key, object cacheObject, CacheDependency dependency, DateTime d, TimeSpan t, CacheItemPriority cip, CacheItemRemovedCallback circ) {
            Initialize();
            if (!_cacheDependencyMode.Equals(SqlCacheDependencyMode.AllSet)) {
                return false;
            }
            try {
                _context.CacheInsert(key, cacheObject, dependency, d, t, cip, circ);
                return true;
            }
            catch (Exception) {
                return false;
            }
        }

        public bool InsertIntoCache(string key, string tableName, object cacheObject) {
            Initialize();
            if (!CacheEnabled(tableName)) {
                return false;
            }
            try {
                using (SqlCacheDependency dependency = new SqlCacheDependency(DataBaseName, tableName)) {
                    _context.CacheInsert(key, cacheObject, dependency);
                    return true;
                }
            }
            catch (Exception ex) {
                throw new Exception("Could not use table " + tableName + " for SQLCacheDependency", ex);
            }
        }

        public bool InsertIntoCache(string tableName, object cacheObject) {
            Initialize();
            if (!CacheEnabled(tableName)) {
                return false;
            }
            try {
                using (SqlCacheDependency dependency = new SqlCacheDependency(DataBaseName, tableName)) {
                    _context.CacheInsert(tableName, cacheObject, dependency);
                    return true;
                }
            }
            catch (Exception ex) {
                throw new Exception("Could not use table " + tableName + " for SQLCacheDependency", ex);
            }
        }

        public void TablesRecreated(System.Collections.ArrayList tableArray) {
            Initialize();
            if (!_cacheDependencyMode.Equals(SqlCacheDependencyMode.AllSet)) return;
            EnableCaching(_aggregatedTables);
            EnableCaching(_singleTables);
            if (_hasDdlPermission) return;
            if (tableArray != null && tableArray.Count > 0) {
                for (int i = 0; i < tableArray.Count; i++) {
                    if (CacheEnabled((string)tableArray[i])) {
                        RecreateDependency((string)tableArray[i]);
                    }
                }
            }
            else {
                const string sql = "SELECT tableName FROM AspNet_SqlCacheTablesForChangeNotification";
                DataTable dt = _sqlExecutor.SQL2DataTable(sql);
                foreach (DataRow row in dt.Rows) {
                    RecreateDependency(row[0].ToString());
                }
            }
        }

        #endregion

        /// <summary> 
        /// During a full upload some or all of the tables are dropped and recreated, and our SqlCacheUpdate-triggers 
        /// no longer exist. When this happens we need to invalidate the cache and recreate the triggers in order to 
        /// maintain a cache with the correct SqlDependency 
        /// </summary> 
        /// <param name="tableName"></param> 
        /// <remarks></remarks> 
        private void RecreateDependency(string tableName) {
            if (!_cacheDependencyMode.Equals(SqlCacheDependencyMode.AllSet)) return;
            string sql = "SELECT TOP 1 name FROM " + DataBaseName + ".sys.triggers WHERE NAME = '" + tableName + "_AspNet_SqlCacheNotification_Trigger'";
            object o = _sqlExecutor.GetSingleValueDb<string>(sql);
            // Notify cache that a change has occurred in a dependent table 
            _sqlExecutor.ExcecuteNonQueryDb("AspNet_SqlCacheUpdateChangeIdStoredProcedure @tableName = " + tableName);
            if (o != null) return;
            // Recreate the trigger 
            RecreateTrigger(tableName);
        }

        /// <summary> 
        /// Helper function for recreateDependency. It creates a SqlCacheNotification-trigger on the specified table 
        /// </summary> 
        /// <param name="tableName"></param> 
        /// <remarks></remarks> 
        private void RecreateTrigger(string tableName) {
            if (!_cacheDependencyMode.Equals(SqlCacheDependencyMode.AllSet)) return;
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("CREATE TRIGGER [" + tableName + "_AspNet_SqlCacheNotification_Trigger] ON [" + tableName + "]");
            sb.AppendLine(" FOR INSERT, UPDATE, DELETE AS BEGIN");
            sb.AppendLine(" SET NOCOUNT ON");
            sb.AppendLine(" EXEC AspNet_SqlCacheUpdateChangeIdStoredProcedure N'" + tableName + "'");
            sb.AppendLine(" END");
            _sqlExecutor.ExcecuteNonQueryDb(sb.ToString());
        }
    }
}
