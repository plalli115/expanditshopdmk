﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace EISCS.ExpandITFramework.Infrastructure {
    public class RandomSequenceGenerator {
        public string GenerateRandomSequence(int length) {
            Guid guid = Guid.NewGuid();
            string originalSequence = guid.ToString();

            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] originalBytes = Encoding.Default.GetBytes(originalSequence);
            byte[] encodedBytes = md5.ComputeHash(originalBytes);

            string hashed = BitConverter.ToString(encodedBytes);

            string replacedHash = hashed.Replace("-", "");
            int replacedLength = replacedHash.Length;

            if (length <= 0) {
                length = replacedLength - 1;
            }
            if (replacedLength <= length) {
                length = replacedLength - 1;
            }

            return hashed.Replace("-", "").Substring(0, length);
        }
    }
}
