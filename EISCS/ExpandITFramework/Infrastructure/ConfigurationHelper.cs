﻿using System;
using System.Configuration;
using System.IO;
using System.Web.Configuration;
using EISCS.ExpandITFramework.Util;

namespace EISCS.ExpandITFramework.Infrastructure
{
    public class ConfigurationHelper
    {
        // This method is for changing the remote content of the web.config if this can't be done with octopus or programatically
        public static bool SyncWebConfigSettingForRemote()
        {
            bool useStateServer = Utilities.CBoolEx(ConfigurationManager.AppSettings["UseRemoteASPSessionStateServer"], false);
            if (!useStateServer)
            {
                return false;
            }


            var map = new ExeConfigurationFileMap();
            map.ExeConfigFilename = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "web.config");



            System.Configuration.Configuration config = ConfigurationManager.OpenMappedExeConfiguration(map, ConfigurationUserLevel.None);
            var session = (SessionStateSection)config.GetSection("system.web/sessionState");


            var sessionStateServer = ConfigurationManager.AppSettings["RemoteASPSessionStateServerConnectionString"];

            if (!String.IsNullOrEmpty(sessionStateServer) && session.StateConnectionString != sessionStateServer)
            {

                session.StateConnectionString = sessionStateServer;
                config.Save();
                return true;
            }

            return false;
        }
    }
}
