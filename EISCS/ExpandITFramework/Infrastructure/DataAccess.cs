﻿using System;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using CmsPublic.DataProviders.Logging;

namespace EISCS.ExpandITFramework.Infrastructure
{
    [Obsolete("Will be removed")]
    public class DataAccess
    {
        private static string _connStr;
        private static string _connStrName;

        static DataAccess()
        {
            _connStr = System.Configuration.ConfigurationManager.ConnectionStrings["ExpandITConnectionString"].ConnectionString;
            _connStrName = "ExpandITConnectionString";
        }

        /// <summary> 
        /// Finds out if this is a remote or local site 
        /// </summary> 
        /// <returns>Boolean</returns> 
        /// <remarks></remarks> 
        private static bool IsRemoteMode()
        {
            try
            {
                string str = HttpRuntime.AppDomainAppPath + "expandITRemote.xml";
                return System.IO.File.Exists(str);
            }
            catch
            {
                return false;
            }
        }

        /// <summary> 
        /// Checks if the site is running in local or remote mode 
        /// </summary> 
        /// <value></value> 
        /// <returns>True if the site is running in local mode</returns> 
        /// <remarks></remarks> 
        public static bool IsLocalMode
        {
            get { return !IsRemoteMode(); }
        }



        /// <summary> 
        /// Get or Set the current ConnectionStringName used by this module 
        /// </summary> 
        /// <value>String</value> 
        /// <returns>The name of the connectionstring</returns> 
        /// <remarks></remarks> 
        public static string CurrentConnectionString
        {
            get { return _connStr; }
            set
            {
                try
                {
                    _connStr = value;
                }
                catch (Exception ex)
                {
                    FileLogger.Log(ex.Message + "\r\n" + ex.StackTrace);
                }
            }
        }

        /// <summary> 
        /// Get or Set the current ConnectionStringName used by this module 
        /// </summary> 
        /// <value>String</value> 
        /// <returns>The name of the connectionstring</returns> 
        /// <remarks></remarks> 
        public static string CurrentConnectionStringName
        {
            get { return _connStrName; }
            set
            {
                try
                {
                    _connStrName = value;
                }
                catch (Exception ex)
                {
                    FileLogger.Log(ex.Message + "\r\n" + ex.StackTrace);
                }
            }
        }

        #region "Data Access"

        /// <summary> 
        /// Function returns a single value as a result from a database query 
        /// </summary> 
        /// <param name="sql">The SQL-Statement to excecute</param> 
        /// <returns>A single value of type Object</returns> 
        /// <remarks></remarks> 
        public static object GetSingleValueDb(string sql)
        {
            SqlCommand oc;
            object ret;
            using (SqlConnection localConn = new SqlConnection(_connStr))
            {
                localConn.Open();
                oc = new SqlCommand(sql, localConn) { CommandType = CommandType.Text };
                ret = oc.ExecuteScalar();
            }
            return ret;
        }


        /// <summary> 
        /// Executes Any SQL-statement. Does not return a reultset. 
        /// </summary> 
        /// <param name="cmdText">SQL-Statement</param> 
        /// <remarks> 
        /// Useful for queries that don't require a resultset 
        /// </remarks> 
        public static string ExcecuteNonQueryDb(string cmdText)
        {
            SqlCommand oc;
            string message = string.Empty;

            using (SqlConnection localConn = new SqlConnection(_connStr))
            {
                try
                {
                    localConn.Open();
                    oc = new SqlCommand(cmdText, localConn) { CommandType = CommandType.Text };
                    oc.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    message = ex.Message;
                }
            }
            return message;
        }

        /// <summary> 
        /// This function returns a filled DataTable 
        /// </summary> 
        /// <param name="inSqlCommand">A SQL-Statement</param> 
        /// <param name="da">A SqlDataAdapter</param> 
        /// <returns>DataTable</returns> 
        /// <remarks></remarks> 
        public static DataTable GetDataTable(string inSqlCommand, ref SqlDataAdapter da)
        {
            SqlDataAdapter od;
            DataTable dt = new DataTable();

            if (da != null)
            {
                da = GetDataAdapter(null, inSqlCommand);
                da.Fill(dt);
            }
            else
            {
                od = GetDataAdapter(null, inSqlCommand);
                od.Fill(dt);
            }
            return dt;

        }

        /// <summary> 
        /// This function returns a DataAdapter tied to a Database table 
        /// </summary> 
        /// <param name="tableName">The name of the db-table</param> 
        /// <param name="sqlSelectCommand">The SQL-Statement</param> 
        /// <returns>SqlDataAdapter</returns> 
        /// <remarks> 
        /// This Function creates commands for delete, insert and update, by using a CommandBuilder. 
        /// It uses the MissingSchemaAction.AddWithKey option, so the db-table must contain at least 
        /// a unique index, to make the CommandBuilder work. 
        /// </remarks> 
        public static SqlDataAdapter GetDataAdapter(string tableName, string sqlSelectCommand)
        {

            SqlDataAdapter da = default(SqlDataAdapter);
            SqlCommandBuilder dcmd;

            if (sqlSelectCommand == null && tableName != null)
            {
                da = new SqlDataAdapter("SELECT * FROM " + tableName + " WHERE 1 = 2", _connStr);
            }
            if (sqlSelectCommand != null && tableName == null)
            {
                da = new SqlDataAdapter(sqlSelectCommand, _connStr);
            }
            if (sqlSelectCommand != null && tableName != null)
            {
                da = new SqlDataAdapter(sqlSelectCommand + tableName, _connStr);
            }

            try
            {
                dcmd = new SqlCommandBuilder(da);
                if (da != null)
                {
                    da.DeleteCommand = dcmd.GetDeleteCommand();
                    da.InsertCommand = dcmd.GetInsertCommand();
                    da.UpdateCommand = dcmd.GetUpdateCommand();
                    da.MissingSchemaAction = MissingSchemaAction.AddWithKey;
                }
            }
            catch (Exception ex)
            {
                FileLogger.Log(ex.Message + "\r\n" + ex.StackTrace);
            }

            return da;

        }

        /// <summary> 
        /// Populates a DataTable with the result from the provided SQL-Query 
        /// </summary> 
        /// <param name="sql"></param> 
        /// <returns>DataTable</returns> 
        /// <remarks></remarks> 
        public static DataTable Sql2DataTable(string sql)
        {
            SqlConnection conn = GetDbConnection();
            //Don't time out 
            SqlCommand cmd = new SqlCommand(sql, conn) { CommandTimeout = 0 };
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            da.Dispose();
            conn.Close();
            return dt;
        }

        /// <summary> 
        /// Returns an open SqlConnection Object 
        /// </summary> 
        /// <returns></returns> 
        /// <remarks></remarks> 
        private static SqlConnection GetDbConnection()
        {
            SqlConnection conn = new SqlConnection(_connStr);
            conn.Open();
            return conn;
        }

        #endregion
    }
}