﻿using EISCS.ExpandITFramework.Infrastructure;

namespace EISCS.ExpandITFramework.Util {
    public class HttpContextWrapper {
        public virtual object Get(string key){
            return CacheManager.CacheGet(key);
        }
    }
}
