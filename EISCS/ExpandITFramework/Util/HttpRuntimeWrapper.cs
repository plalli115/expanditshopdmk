﻿using System.Web;

namespace EISCS.ExpandITFramework.Util
{
    public interface IHttpRuntimeWrapper
    {
        string AppDomainAppVirtualPath { get; }
        string AppDomainAppPath { get; }
    }

    public class HttpRuntimeWrapper : IHttpRuntimeWrapper
    {
        public virtual string AppDomainAppVirtualPath
        {
            get { return HttpRuntime.AppDomainAppVirtualPath ?? ""; }
        }

        public string AppDomainAppPath { get { return HttpRuntime.AppDomainAppPath ?? ""; } }
    }
}
