﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Text;

namespace EISCS.ExpandITFramework.Util
{
    [Flags]
    public enum TruncateOptions
    {
        ///<summary>
        ///</summary>
        None = 0x0,

        ///<summary>
        ///</summary>
        FinishWord = 0x1,

        ///<summary>
        ///</summary>
        AllowLastWordToGoOverMaxLength = 0x2,

        ///<summary>
        ///</summary>
        IncludeEllipsis = 0x4
    }

    public static class AppUtil
    {

        private static readonly HttpRuntimeWrapper HttpRuntimeWrapper = new HttpRuntimeWrapper();

        public static string GetAppRootUrl()
        {
            StringBuilder builder = new StringBuilder(HttpContext.Current.Request.Url.Scheme);
            builder.Append("://");
            builder.Append(HttpContext.Current.Request.Url.Host);
            builder.Append(HttpContext.Current.Request.ApplicationPath);
            return builder.ToString();
        }

        public static string GetAppRoot()
        {
            StringBuilder builder = new StringBuilder(HttpContext.Current.Request.Url.Host);
            builder.Append(HttpContext.Current.Request.ApplicationPath);
            return builder.ToString();
        }

        public static string GetVirtualRoot()
        {
            return HttpRuntimeWrapper.AppDomainAppVirtualPath.Equals("/") ? "" : HttpRuntimeWrapper.AppDomainAppVirtualPath;
        }

        public static string NormalizeHtmlContent(this string content)
        {
            if (!string.IsNullOrEmpty(content))
            {
                string virtualPath = GetVirtualRoot();
                return ReplaceHref(ReplaceSrc(content, virtualPath), virtualPath);
            }
            return "";
        }

        public static string NormalizeLinkString(this string urlValue)
        {
            if (string.IsNullOrEmpty(urlValue))
            {
                return urlValue;
            }
            if (urlValue.StartsWith("http:") || urlValue.StartsWith("https:"))
            {
                return urlValue;
            }
            if (urlValue.StartsWith("www."))
            {
                return "http://" + urlValue;
            }
            return GetVirtualRoot() + "/" + urlValue;
        }

        public static string Truncate(this string value, int maxLength)
        {
            if (string.IsNullOrEmpty(value))
            {
                return value;
            }
            return value.Length <= maxLength ? value : value.Substring(0, maxLength);
        }

        public static string TruncateString
            (this string valueToTruncate, int maxLength, TruncateOptions options)
        {
            if (valueToTruncate == null)
            {
                return "";
            }

            if (valueToTruncate.Length <= maxLength)
            {
                return valueToTruncate;
            }

            bool includeEllipsis = (options & TruncateOptions.IncludeEllipsis) ==
                                   TruncateOptions.IncludeEllipsis;
            bool finishWord = (options & TruncateOptions.FinishWord) ==
                              TruncateOptions.FinishWord;
            bool allowLastWordOverflow =
                (options & TruncateOptions.AllowLastWordToGoOverMaxLength) ==
                TruncateOptions.AllowLastWordToGoOverMaxLength;

            string retValue = valueToTruncate.Substring(0, maxLength - 1);

            if (includeEllipsis)
            {
                maxLength -= 1;
            }

            int lastSpaceIndex = retValue.LastIndexOf(" ", StringComparison.CurrentCultureIgnoreCase);

            if (!finishWord)
            {
                retValue = retValue.Remove(maxLength);
            }
            else if (allowLastWordOverflow)
            {
                int spaceIndex = retValue.IndexOf(" ",
                                                  maxLength, StringComparison.CurrentCultureIgnoreCase);
                if (spaceIndex != -1)
                {
                    retValue = retValue.Remove(spaceIndex);
                }
            }
            else if (lastSpaceIndex > -1)
            {
                retValue = retValue.Remove(lastSpaceIndex);
            }

            if (includeEllipsis && retValue.Length < valueToTruncate.Length)
            {
                retValue += "&hellip;";
            }
            return retValue;
        }

        private static string ReplaceSrc(string test, string replacement)
        {
            test = test.Replace("../", string.Empty);
            test = test.Replace("\"", "'");
            const string srcPattern = @"src='/";
            test = Regex.Replace(test, srcPattern, @"src='");
            const string pattern = @"(?<=src=['])(?!http|https[:]//.*)([^']*)(?=['])";
            return Regex.Replace(test, pattern, replacement + @"/$0", RegexOptions.Multiline);
        }

        public static string ReplaceHref(string test, string replacement)
        {
            if (test.Contains("mailto:"))
            {
                return test;
            }
            test = test.Replace("../", string.Empty);
            test = test.Replace("\"", "'");
            const string srcPattern = @"href='/";
            test = Regex.Replace(test, srcPattern, @"href='");
            const string pattern = @"(?<=href=['])(?!http|https[:]//.*)([^']*)(?=['])";
            return Regex.Replace(test, pattern, replacement + @"/$0", RegexOptions.Multiline);
        }


        public static string ShopFullPath()
        {
            return ShopFullPathServerOnly() + GetVirtualRoot();
        }

        public static string ShopFullPathServerOnly()
        {
            string protocol = "http://";
            string port = HttpContext.Current.Request.ServerVariables["SERVER_PORT"];
            if (port == "80")
                port = "";
            else
                port = ":" + port;
            if (HttpContext.Current.Request.IsSecureConnection)
            {
                if (port == ":443")
                    port = "";
                protocol = "https://";
            }
            string serverName = HttpContext.Current.Request.ServerVariables["HTTP_HOST"];
            int colonIndex = serverName.IndexOf(":", StringComparison.Ordinal);
            if (colonIndex > 0)
            {
                serverName = serverName.Substring(0, colonIndex);
            }
            return protocol + serverName + port;
        }

        public static bool IsNullOrEmpty<T>(this IEnumerable<T> items)
        {
            return items == null || !items.Any();
        }

        public static string PictureLink(string filename)
        {
            string trimmedFilename = string.Empty;
            if (!string.IsNullOrEmpty(filename))
            {
                trimmedFilename = filename.Trim();
                if (trimmedFilename.StartsWith("/"))
                    trimmedFilename = filename.Remove(0, 1);
            }

            return HttpContext.Current.Server.UrlPathEncode(GetVirtualRoot() + "/" + trimmedFilename);
        }
    }
}