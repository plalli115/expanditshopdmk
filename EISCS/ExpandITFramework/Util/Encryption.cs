﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using ExpandIT.Logging;
using Microsoft.VisualBasic;

namespace EISCS.ExpandITFramework.Util
{
    public static class Encryption
    {
        private static byte[] hashKey = new byte[4];
        private const int SaltValueSize = 4;

        public enum EncryptionTypes
        {
            ClearText,
            XOR,
            HMAC_MD5,
            HMAC_SHA1,
            HMAC_SHA256,
            HMAC_SHA384,
            HMAC_SHA512,
            HMAC_RIPEMD160,
            BCrypt
        }

        public static string Encrypt(string textToEncrypt, string encryptionSalt, string chosenEncryptionType)
        {
            bool useEncryptionSalt = UseEncryptionSalt(chosenEncryptionType);
            bool isBCrypt = chosenEncryptionType.Equals(EncryptionTypes.BCrypt.ToString());
            string encryptedText = string.Empty;


            // XOR and ClearText don't use encryption salt and BCrypt uses a specialized salt and base64 encoding
            if (useEncryptionSalt)
            {
                encryptionSalt = Base64Decode(encryptionSalt);
            }
            // Check if chosen encryption type in webconfig is valid.
            bool isValidEncryptionType = ValidateEncryptionType(chosenEncryptionType);
            EncryptionTypes choosEncryptionType = (EncryptionTypes) Enum.Parse(typeof (EncryptionTypes), chosenEncryptionType);

            if (isValidEncryptionType)
            {
                switch (choosEncryptionType)
                {
                    case EncryptionTypes.ClearText:
                        encryptedText = textToEncrypt;
                        break;
                    case EncryptionTypes.HMAC_MD5:
                        encryptedText = Hash(textToEncrypt, encryptionSalt, new HMACMD5(hashKey));
                        break;
                    case EncryptionTypes.HMAC_SHA1:
                        encryptedText = Hash(textToEncrypt, encryptionSalt, new HMACSHA1(hashKey));
                        break;
                    case EncryptionTypes.HMAC_SHA256:
                        encryptedText = Hash(textToEncrypt, encryptionSalt, new HMACSHA256(hashKey));
                        break;
                    case EncryptionTypes.HMAC_SHA384:
                        encryptedText = Hash(textToEncrypt, encryptionSalt, new HMACSHA384(hashKey));
                        break;
                    case EncryptionTypes.HMAC_SHA512:
                        encryptedText = Hash(textToEncrypt, encryptionSalt, new HMACSHA512(hashKey));
                        break;
                    case EncryptionTypes.HMAC_RIPEMD160:
                        encryptedText = Hash(textToEncrypt, encryptionSalt, new HMACRIPEMD160(hashKey));
                        break;
                    case EncryptionTypes.XOR:
                        encryptedText = EncryptXOrWay(textToEncrypt);
                        break;
                    case EncryptionTypes.BCrypt:
                        encryptionSalt = BCrypt.GenerateSalt();
                        string bcryptHash = BCrypt.HashPassword(textToEncrypt, encryptionSalt);
                        encryptedText = bcryptHash;
                        break;

                    default:
                        encryptedText = textToEncrypt;
                        break;
                }


                // BCrypt uses its built-in specialized Base64 encoding and salt generator.
                // ClearText doesn't uses salt or Base64.
                // XOR doesn't uses salt or Base64 because we can't use the decrypt tool then.
                if (!isBCrypt && useEncryptionSalt)
                {
                    encryptedText = Base64Encode(encryptedText);
                }
            }
            return encryptedText;
        }

        public static bool ValidateEncryptionType(string chosenEncryptionType)
        {
            foreach (var encryptionType in Enum.GetValues(typeof (EncryptionTypes)))
            {
                if (chosenEncryptionType.Equals(encryptionType.ToString()))
                {
                    return true;
                }
            }
            // If we get here it is a invalid encryption type
            FileLogger.log("ERROR: unknown encryption type (" + chosenEncryptionType + ")");
            throw new ArgumentException("ERROR: unknown encryption type", "chosenEncryptionType");
        }

        public static string Hash(string value, string salt, HashAlgorithm hashAlgorithm)
        {
            byte[] saltedValue = Encoding.UTF8.GetBytes(value).Concat(Encoding.UTF8.GetBytes(salt)).ToArray();
            return Encoding.UTF8.GetString(hashAlgorithm.ComputeHash(saltedValue));
        }

        public static string GenerateSaltValue()
        {
            var saltBytes = new byte[SaltValueSize];
            using (var provider = new RNGCryptoServiceProvider())
                provider.GetNonZeroBytes(saltBytes);
            return Base64Encode(Encoding.UTF8.GetString(saltBytes));
        }

        /// <summary>
        /// Encrypts as string by the Xor algorithm 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static string EncryptXOrWay(string s)
        {
            int ch;
            string r = "";

            try
            {
                for (int i = 0; i < s.Length; i++)
                {
                    ch = Convert.ToInt32(Convert.ToChar(Utilities.Mid(s, i, 1)));
                    ch = ch ^ 111;
                    r = r + Utilities.Right("0" + ch.ToString("X"), 2);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return r;
        }

        public static string DecryptXOrWay(string s)
        {
            string r = null;

            for (int i = 2; i <= s.Length; i += 2)
            {
                string temp = s.Substring(i - 2, 2);
                temp = Utilities.UnHex(temp);
                int ch = Strings.Asc(temp);
                ch = ch ^ 111;
                char tmp = Strings.Chr(ch);
                r += tmp;
            }
            return r;
        }

        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = Encoding.UTF8.GetBytes(plainText);
            return Convert.ToBase64String(plainTextBytes);
        }

        public static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = Convert.FromBase64String(base64EncodedData);
            return Encoding.UTF8.GetString(base64EncodedBytes);
        }

        /// <summary>
        /// XOR doesn't uses salt or Base64 because we can't use the decrypt tool then.
        /// ClearText doesn't uses salt or Base64.
        /// BCrypt uses its specialized salt generator method and Base64 method.
        /// </summary>
        /// <param name="chosenEncryptionType"></param>
        /// <returns></returns>
        public static bool UseEncryptionSalt(string chosenEncryptionType)
        {
            return !chosenEncryptionType.Equals(EncryptionTypes.XOR.ToString()) && !chosenEncryptionType.Equals(EncryptionTypes.BCrypt.ToString()) && !chosenEncryptionType.Equals(EncryptionTypes.ClearText.ToString());
        }
    }
}
    


