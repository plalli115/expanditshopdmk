﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using EISCS.Shop.DO.Interface;
using ExpandIT.Logging;
using Microsoft.VisualBasic;

namespace EISCS.ExpandITFramework.Util
{
    public static class Utilities
    {
        public static string GetRandomPassword(int length)
        {
            //Here put all the characters that you would need to create the password
            char[] chars = "abcdefghijklmnopqrstuvwxyz+-*@#%=?!_;./ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789".ToCharArray();
            //Here the password will contain small characters,capital characters, some special symbols and numbers
            string strPassword = string.Empty;
            Random random = new Random();
            for (int i = 0; i < length; i++)
            {
                int x = random.Next(1, chars.Length);
                //For avoiding Repetation of Characters
                if (!strPassword.Contains(chars.GetValue(x).ToString()))
                    strPassword += chars.GetValue(x);
                else
                    i--;
            }
            return strPassword;
        }

        public static string CreateGuid()
        {
            return "{" + Guid.NewGuid().ToString() + "}";
        }

        ///<summary>
        /// Determines if a variable contains a value. 
        /// </summary> 
        /// <param name="v">Value to check.</param> 
        /// <returns>Boolean</returns> 
        /// <remarks> 
        /// NaV is short for Not a Value. If v is a valid value this function returns true. Otherwise it returns false. 
        /// Valid values are values that are not empty or null. 
        /// </remarks> 
        public static bool NaV(object v)
        {
            try
            {
                return v == null;
            }
            catch
            {
                return true;
            }
        }

        public static object SafeDbNull(object obj)
        {
            if (obj == null)
            {
                return null;
            }
            return (obj.Equals(DBNull.Value) ? null : obj);
        }


        public static string ToInClauseString(IList<string> list, string identifier = null)
        {
            if (list == null || !list.Any())
            {
                return string.Format("{0}IS NULL", string.IsNullOrEmpty(identifier) ? "" : identifier + " ");
            }
            var listStrValues = list.Select(s => "'" + s + "'").ToList();
            return string.Format("{0}IN ({1})", string.IsNullOrEmpty(identifier) ? "" : identifier + " ", string.Join(",", listStrValues));
        }

        public static string ToInClauseString(IList<int> list, string identifier = null)
        {
            if (list == null || !list.Any())
            {
                return string.Format("{0}IS NULL", string.IsNullOrEmpty(identifier) ? "" : identifier + " ");
            }
            return string.Format("{0}IN ({1})", string.IsNullOrEmpty(identifier) ? "" : identifier + " ", string.Join(",", list));
        }

        /// <summary>
        /// Makes a SQL safe string 
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        public static string SafeString(Object v)
        {
            string str = CStrEx(v);
            if (str == "")
            {
                return "NULL";
            }
            return "N'" + Replace(str, "'", "''") + "'";
        }

        public static string Replace(string str1, string str2, string str3)
        {
            return Regex.Replace(str1, str2, str3);
        }

        /// <summary>
        /// Makes a safe conversion to a String. 
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        public static string CStrEx(object v)
        {
            if (v == DBNull.Value)
            {
                return "";
            }
            if (v == null)
            {
                return "";
            }
            try
            {
                return (string)v;
            }
            catch (Exception)
            {
                return "";
            }
        }

        /// <summary>
        /// Makes a safe conversion to an Integer. 
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        public static Int32 CLngEx(object v)
        {
            if (v == DBNull.Value)
            {
                return 0;
            }
            if (v == null)
            {
                return 0;
            }
            if (v is int)
            {
                return (int)v;
            }
            try
            {
                return Convert.ToInt32(v);
            }
            catch (Exception)
            {
                return 0;
            }
        }

        /// <summary> 
        /// Makes a safe conversion to an Integer. 
        /// </summary> 
        /// <param name="v">Value to convert.</param> 
        /// <returns>Integer</returns> 
        /// <remarks> 
        /// Returns the integer representing v. If v cannot be converted to a integer the function returns 0 (zero). 
        /// </remarks> 
        public static int CIntEx(object v)
        {
            if (v == DBNull.Value)
            {
                return 0;
            }
            if (v == null)
            {
                return 0;
            }
            if (v is int)
            {
                return (int)v;
            }
            try
            {
                return Convert.ToInt32(v);
            }
            catch (Exception)
            {
                return 0;
            }
        }

        /// <summary> 
        /// Makes a safe conversion to a Decimal. 
        /// </summary> 
        /// <param name="v">Value to convert.</param> 
        /// <returns>Decimal</returns> 
        /// <remarks> 
        /// Returns the Decimal representing v. If v cannot be converted to a Decimal the function returns 0 (zero). 
        /// </remarks> 
        public static decimal CDblEx(object v)
        {
            if (v == DBNull.Value)
            {
                return 0;
            }
            if (v == null)
            {
                return 0;
            }
            if (v is decimal)
            {
                return (decimal)v;
            }
            try
            {
                return Convert.ToDecimal(v);
            }
            catch (Exception)
            {
                return 0;
            }
        }

        /// <summary> 
        /// Makes a safe conversion to a boolean. 
        /// </summary> 
        /// <param name="v">Value to convert.</param> 
        /// <returns>Boolean</returns> 
        /// <remarks> 
        /// Returns the boolean representing v. If v cannot be converted to a boolean the function returns false. 
        /// </remarks> 
        public static bool CBoolEx(object v)
        {
            return CBoolEx(v, false);
        }

        /// <summary> 
        /// Makes a safe conversion to a boolean. 
        /// </summary> 
        /// <param name="v">Value to convert.</param>
        /// <param name="defaultValue">Default value to use if the specified value cannot be converted.</param>
        /// <returns>Boolean</returns> 
        /// <remarks> 
        /// Returns the boolean representing v. If v cannot be converted to a boolean the function returns defaultValue. 
        /// </remarks> 
        public static bool CBoolEx(object v, bool defaultValue)
        {
            if (v == null || v == DBNull.Value)
            {
                return defaultValue;
            }
            if (v is bool)
            {
                return (bool)v;
            }
            try
            {
                string rawString = ((string)v).Trim();
                switch (rawString)
                {
                    case "0":
                        return false;
                    case "1":
                        return true;
                    default:
                        bool result;
                        if (bool.TryParse(rawString, out result))
                        {
                            return result;
                        }
                        return defaultValue;
                }
            }
            catch
            {
                return defaultValue;
            }
        }

        public static bool RegExpTest(string testStr, string pattern)
        {
            Regex regex = new Regex(pattern, RegexOptions.IgnoreCase);
            return regex.IsMatch(testStr);
        }

        public static string UnHex(string hex)
        {
            if (hex == null)
                throw new ArgumentNullException("hex");
            hex = hex.Replace(",", "");
            hex = hex.Replace("\n", "");
            hex = hex.Replace("\\", "");
            hex = hex.Replace(" ", "");
            if (hex.Length%2 != 0)
            {
                hex += "20"; //space
            }

            byte[] bytes = new byte[hex.Length/2];

            for (int i = 0; i < bytes.Length; i++)
            {
                try
                {
                    // 2 chars in 1 byte? 
                    bytes[i] = byte.Parse(hex.Substring(i*2, 2),
                        NumberStyles.HexNumber);
                }
                catch
                {
                    // Rethrow an exception with custom message. 
                    throw new ArgumentException("hex is not a valid hex number!", "hex");
                }
            }
            Encoding chs = Encoding.Default;
            return chs.GetString(bytes);
        }

        public static Dictionary<string, List<T>> ConvertToDictionary<T>(List<T> srcList, Func<T, string> pred)
        {
            var results = new Dictionary<string, List<T>>();

            foreach (var x in srcList)
            {
                string key = pred.Invoke(x);
                if (results.ContainsKey(key))
                {
                    results[key].Add(x);
                }
                else
                {
                    results[key] = new List<T>() { x };
                }

            }

            return results;
        }

        #region "VB Functionality"

        /// <summary>
        /// Retrieves a substring of a given length starting from the beginning of the string
        /// </summary>
        /// <param name="param"></param>
        /// <param name="length"></param>
        /// <returns></returns>
        public static string Left(string param, int length)
        {
            //we start at 0 since we want to get the characters starting from the
            //left and with the specified lenght and assign it to a variable
            string result = param.Substring(0, length);
            //return the result of the operation
            return result;
        }

        /// <summary>
        /// Retrieves a substring of a given length starting from the end of the string
        /// </summary>
        /// <param name="param"></param>
        /// <param name="length"></param>
        /// <returns></returns>
        public static string Right(string param, int length)
        {
            //start at the index based on the lenght of the sting minus
            //the specified lenght and assign it a variable
            string result = param.Substring(param.Length - length, length);
            //return the result of the operation
            return result;
        }

        /// <summary>
        /// Retreives a substring of a given length from a given start position
        /// </summary>
        /// <param name="param"></param>
        /// <param name="startIndex"></param>
        /// <param name="length"></param>
        /// <returns></returns>
        public static string Mid(string param, int startIndex, int length)
        {
            //start at the specified index in the string ang get N number of
            //characters depending on the lenght and assign it to a variable
            string result = param.Substring(startIndex, length);
            //return the result of the operation
            return result;
        }

        /// <summary>
        /// Retreives a substring from a given start position
        /// </summary>
        /// <param name="param"></param>
        /// <param name="startIndex"></param>
        /// <returns></returns>
        public static string Mid(string param, int startIndex)
        {
            //start at the specified index and return all characters after it
            //and assign it to a variable
            string result = param.Substring(startIndex);
            //return the result of the operation
            return result;
        }

        /// <summary>
        /// Returns a string array that contains the substrings that are delimited
        /// by the provided delimiter string
        /// </summary>
        /// <param name="param"></param>
        /// <param name="delimiter"></param>
        /// <returns></returns>
        public static string[] Split(string param, string delimiter)
        {
            if (param == null)
                return null;
            try
            {
                char[] delim = delimiter.ToCharArray();
                return param.Split(delim);
            }
            catch (Exception)
            {
                return null;
            }
        }

        #endregion
    }
}