﻿using System.Data;
using ExpandIT;

namespace EISCS.WebUserControlLogic.Country
{
    public interface ICountryControlLogic
    {
        DataTable LoadCountries();
        string SelectedValue(ExpDictionary userDict);
    }
}
