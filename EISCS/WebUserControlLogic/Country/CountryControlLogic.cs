﻿using System;
using System.Configuration;
using System.Data;
using EISCS.Shop.DO.Service;
using ExpandIT;

namespace EISCS.WebUserControlLogic.Country
{
    public class CountryControlLogic : ICountryControlLogic
    {
        private readonly ILegacyDataService _legacyDataService;

        public CountryControlLogic(ILegacyDataService legacyDataService){
            _legacyDataService = legacyDataService;
        }

        private DataTable _countryDataTable;
        /// <summary>
        /// Loads all the availale countries in the shop.
        /// </summary>
        /// <returns></returns>
        public DataTable LoadCountries()
        {
            _countryDataTable = null;

            const string sql = "SELECT CountryGuid, CountryName FROM CountryTable ORDER BY CountryName";
            _countryDataTable = _legacyDataService.SQL2DataTable(sql);

            return _countryDataTable;
        }

        public string SelectedValue(ExpDictionary userDict)
        {
            if (HasCountries())
            {
                return IsUserCountryGuidInDataTable(userDict) ? (ExpanditLib2.CStrEx(userDict["CountryGuid"]).ToUpper()) : (ExpanditLib2.CStrEx(ConfigurationManager.AppSettings["DEFAULT_COUNTRYGUID"]).ToUpper());
            }
            return null;
        }

        protected bool HasCountries()
        {
            try
            {
                return _countryDataTable.Rows.Count > 0;
            }
            catch (Exception)
            {
                return false;
            }
        }

        protected bool IsUserCountryGuidInDataTable(ExpDictionary userDict)
        {
            foreach (DataRow row in _countryDataTable.Rows)
            {
                if (row[0].Equals(userDict["CountryGuid"]))
                {
                    return true;
                }
            }
            return false;
        }

    }
}
