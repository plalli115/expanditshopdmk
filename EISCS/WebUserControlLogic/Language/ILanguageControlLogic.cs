﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace EISCS.WebUserControlLogic.Language
{
    public interface ILanguageControlLogic
    {
        DataTable LoadLanguages(bool load);
        string LoadLanguagesSqlString();
    }
}
