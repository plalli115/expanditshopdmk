﻿using System;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Web;
using CmsPublic.DataProviders.Logging;
using EISCS.ExpandITFramework.Infrastructure;
using EISCS.Shop.DO.Service;
using ExpandIT;

namespace EISCS.WebUserControlLogic.Language
{
    [DataObject]
    public class LanguageControlLogic : ILanguageControlLogic
    {
        private ILegacyDataService _legacyDataService;

        public LanguageControlLogic(ILegacyDataService legacyDataService)
        {
            _legacyDataService = legacyDataService;
        }

        /// <summary>
        ///     Loads all the availible languages in the shop.
        /// </summary>
        /// <returns>DataTable with all languages</returns>
        /// <remarks></remarks>
        public DataTable LoadLanguages(bool load)
        {
            var dt = (DataTable) CacheGet("DataTableSiteLanguages");
            if (dt == null)
            {
                dt = _legacyDataService.SQL2DataTable(LoadLanguagesSqlString());
                CacheSet("DataTableSiteLanguages", dt, "LanguageTable");
            }

            return dt;
        }


        /// <summary>
        ///     Returns a SQL-Statement used to load all availible languages in the shop
        /// </summary>
        /// <returns>A string as SQL-Statement</returns>
        /// <remarks></remarks>
        public string LoadLanguagesSqlString()
        {
            const string sql = @"  
    SELECT LanguageGuid, LanguageNameNative as LanguageName FROM LanguageTable WHERE LanguageEnabled <> 0 
    AND LanguageNameNative IS NOT NULL 
    UNION 
    SELECT LanguageGuid, LanguageName FROM LanguageTable WHERE LanguageEnabled <> 0 
    AND LanguageNameNative IS NULL 
    ORDER BY LanguageName ";

            return sql;
        }

        public void CacheSet(string key, object v, string tableName)
        {
            if (!ExpandITLib.IsLocalMode | ExpanditLib2.CBoolEx(ConfigurationManager.AppSettings["LOCALMODE_USE_CACHE"]))
            {
                try
                {
                    CacheManager.InsertIntoCache(key, tableName, v);
                }
                catch (Exception ex)
                {
                    FileLogger.Log(ex.Message + " " + ex.StackTrace);
                }
            }
        }


        /// <summary>
        ///     Get an object from the cache
        /// </summary>
        /// <param name="key">String that identifies the object in cache</param>
        /// <returns>The cached object</returns>
        /// <remarks></remarks>
        public object CacheGet(string key)
        {
            return CacheManager.CacheGet(key);
        }
    }
}