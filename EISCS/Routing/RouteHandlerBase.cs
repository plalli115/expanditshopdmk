﻿namespace EISCS.Routing {
    /// <summary>
    /// Summary description for RouteHandlerBase
    /// </summary>
    public class RouteHandlerBase {
        public string VirtualPath { get; set; }
        public bool CheckPhysicalAccess { get; set; }

        public RouteHandlerBase(bool checkPhysicalUrlAccess) {
            CheckPhysicalAccess = checkPhysicalUrlAccess;
        }
    }
}