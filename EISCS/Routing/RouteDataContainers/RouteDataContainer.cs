﻿using System;
using System.Collections.Generic;
using System.Reflection;
using EISCS.Routing.Concrete;
using EISCS.Routing.Interfaces;

namespace EISCS.Routing.RouteDataContainers {
    public abstract class RouteDataContainer{
        public abstract IRouteDataProvider<RouteDataNode> GetProvider(IRouteCreationModule<RouteDataNode> creationModule, INodeListLookup<RouteDataNode> nodeListLookup, Dictionary<string, Dictionary<int, RouteDataNode>> fastGroupLookup);

        public static RouteDataContainer GetContainer(string dataProviderTypeName){
            Type type = Type.GetType(dataProviderTypeName, true, true);
            return (RouteDataContainer)CreateInstance(type);
        }

        protected static object CreateInstance(Type t, params object[] args) {
            object instance;
            Type[] tArgs;

            if (args != null) {
                tArgs = new Type[args.Length];
                for (int i = 0; i < args.Length; i++) {
                    tArgs[i] = args[i].GetType();
                }
            }
            else {
                tArgs = new Type[0];
            }
            ConstructorInfo constructor = t.GetConstructor(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic, null, tArgs, new ParameterModifier[0]);
            try {
                instance = constructor.Invoke(args);
            }
            catch (Exception e) {
                throw new Exception("The object of type " + t + " could not be created.", e);
            }
            return instance;
        }
    }
}
