﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using CmsPublic.DataRepository;
using EISCS.Routing.Concrete;
using EISCS.Routing.Interfaces;
using EISCS.Routing.RouteDataProviders;
using EISCS.Shop.DO.Interface;

namespace EISCS.Routing.RouteDataContainers
{
    public class SqlCachedRouteDataContainer : RouteDataContainer
    {
        public override IRouteDataProvider<RouteDataNode> GetProvider(IRouteCreationModule<RouteDataNode> creationModule, INodeListLookup<RouteDataNode> nodeListLookup, Dictionary<string, Dictionary<int, RouteDataNode>> fastGroupLookup)
        {
            var db = DependencyResolver.Current.GetService<IExpanditDbFactory>();
            // Make routeDataProvider
            IRouteDataProvider<RouteDataNode> routeDataProvider =
                new SqlCachedRouteDataProvider<RouteDataNode>(nodeListLookup, creationModule, db);

            return routeDataProvider;
        }
    }
}
