﻿using System.Collections.Generic;
using EISCS.Routing.Concrete;
using EISCS.Routing.Interfaces;
using EISCS.Routing.RouteDataProviders;

namespace EISCS.Routing.RouteDataContainers {
    class ListRouteDataContainer : RouteDataContainer {
        public override IRouteDataProvider<RouteDataNode> GetProvider(IRouteCreationModule<RouteDataNode> recreator, INodeListLookup<RouteDataNode> nodeListLookup, Dictionary<string, Dictionary<int, RouteDataNode>> fastGroupLookup) {

            List<RouteDataNode> routeDataNodes = new List<RouteDataNode>();

            IRouteDataProvider<RouteDataNode> routeDataProvider =
                new ListRouteDataProvider<RouteDataNode>(routeDataNodes, nodeListLookup, recreator, fastGroupLookup);

            // Fill the list
            routeDataProvider.RecreateAll();

            return routeDataProvider;
        }
    }
}
