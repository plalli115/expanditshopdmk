﻿using System.Collections.Generic;
using System.Web.Mvc;
using CmsPublic.DataRepository;
using EISCS.Routing.Concrete;
using EISCS.Routing.Interfaces;
using EISCS.Routing.RouteDataProviders;
using EISCS.Shop.DO.Interface;

namespace EISCS.Routing.RouteDataContainers {

    /// <summary>
    /// The purpouse of this class is to create the RouteData provider.
    /// </summary>
    public class SqlRouteDataContainer : RouteDataContainer {
        public override IRouteDataProvider<RouteDataNode> GetProvider(IRouteCreationModule<RouteDataNode> recreator, INodeListLookup<RouteDataNode> nodeListLookup, Dictionary<string, Dictionary<int, RouteDataNode>> fastGroupLookup) {

            var db = DependencyResolver.Current.GetService<IExpanditDbFactory>();
            // Make routeDataProvider
            IRouteDataProvider<RouteDataNode> routeDataProvider =
                new SqlRouteDataProvider<RouteDataNode>(nodeListLookup, recreator,db);

            routeDataProvider.RecreateAll();
            return routeDataProvider;
        }
    }
}
