﻿using System.Collections.Generic;
using System.Web;

namespace EISCS.Routing.Interfaces {
    public interface INodeListLookup<T> where T : class {
        T LookUpGroup(List<T> nodeList, string groupGuid, string languageGuid);
        T LookUpGroupProduct(List<T> nodeList, string groupGuid, string productGuid, string languageGuid);
        T LookUpProduct(List<T> nodeList, int rootGuid, string productGuid, string languageGuid);
        T LookUpProduct(List<T> nodeList, string productGuid, string languageGuid);
        T LookUpRoute(List<T> nodeList, string path, HttpContextBase httpContext);
    }
}
