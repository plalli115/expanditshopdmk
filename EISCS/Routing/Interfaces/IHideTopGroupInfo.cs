﻿using CmsPublic.ModelLayer.Models;

namespace EISCS.Routing.Interfaces{
    public interface IHideTopGroupInfo{
        bool IsToHideRoot(Group group);
    }
}