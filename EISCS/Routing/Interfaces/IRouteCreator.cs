﻿using System.Collections.Generic;
using CmsPublic.ModelLayer.Models;

namespace EISCS.Routing.Interfaces {
    public interface IRouteCreator<T> {
        void CreateProductRoutes(IList<T> routeNodeList, string languageGuid);
        void CreateProductRoutes(IList<T> routeNodeList, Product product, string languageGuid);
        void CreateAll(IList<T> routeNodeList, Group catalog, string languageGuid);
        void CreateGroupProductRoutes(IList<T> routeNodeList, Group catalog, string languageGuid);
        void CreateGroupProductRoutes(IList<T> routeNodeList, Group catalog, Product product, string languageGuid);
    }
}
