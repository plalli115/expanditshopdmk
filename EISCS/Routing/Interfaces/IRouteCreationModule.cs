﻿using System.Collections.Generic;

namespace EISCS.Routing.Interfaces{
    public interface IRouteCreationModule<T> where T : class{
        IList<T> PrepareCreation(IRouteDataProvider<T> dataProvider);
    }
}