﻿namespace EISCS.Routing.Interfaces{
    public interface IHideTopGroupInfoContainer{
        IHideTopGroupInfo GetInfo();
    }
}