﻿namespace EISCS.Routing.Interfaces
{
    public interface IRouteDataNode
    {
        int GroupGuid { get; set; }
        string LanguageGuid { get; set; }
        string TargetUrl { get; set; }
        string ProductGuid { get; set; }
        string RouteUrl { get; set; }
        string TypeCode { get; set; }
        string Controller { get; set; }
        string ViewFile { get; set; }
    }
}
