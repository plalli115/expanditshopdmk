﻿using System.Web.Routing;

namespace EISCS.Routing.Interfaces {
    public interface IRoutablePage {
        RequestContext RouteContext { get; set; }
    }
}