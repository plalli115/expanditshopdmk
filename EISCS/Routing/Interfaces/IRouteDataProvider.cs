﻿using System.Collections.Generic;
using System.Web;
using CmsPublic.ModelLayer.Models;

namespace EISCS.Routing.Interfaces {
    public interface IRouteDataProvider<T> where T : class {
        T FillByGroupGuid(int groupGuid, string languageGuid);
        T FillByGroupProductGuid(int groupGuid, string productGuid, string languageGuid);
        T FillByProductGuid(string productGuid, string languageGuid);
        T FillByRoute(string route, HttpContextBase context);
        T FillByControllerName(string controllerName);
        int GetGroupGuidByProductGuid(string productGuid, string languageGuid);
        void Add(T t);
        int AddRange(ICollection<T> t);
        int AddRangeFlushable(ICollection<T> t);
        void RemoveByGroupGuid(ICollection<int> guids);
        void RemoveByProductGuid(ICollection<string> guids);
        void RemoveByProductGuid(ICollection<string> guids, string languageGuid);
        void RemoveByGroupGuidProductGuid(ICollection<GroupProduct> t);
        int RecreateAll();
        List<string> GetInternalLinkUrls(string languageGuid);
        int Count();
    }
}
