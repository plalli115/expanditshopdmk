﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Compilation;
using System.Web.Routing;
using System.Web.UI;

namespace EISCS.Routing {
    public class DefaultRouteHandler : IRouteHandler {

        private string _physicalFile;

        public DefaultRouteHandler(string physicalFile) {
            _physicalFile = physicalFile;
        }

        public IHttpHandler GetHttpHandler(RequestContext requestContext) {
            HttpContext.Current.Items["RouteData"] = requestContext.RouteData;
            return BuildManager.CreateInstanceFromVirtualPath(_physicalFile, typeof(Page)) as Page;
        }
    }
}
