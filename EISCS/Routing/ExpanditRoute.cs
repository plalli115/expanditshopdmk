﻿using System.Collections.Specialized;
using System.Globalization;
using System.IO;
using System.Web;
using System.Web.Routing;
using EISCS.Routing.Concrete;
using EISCS.Routing.Interfaces;

namespace EISCS.Routing
{
    public class ExpanditRoute : Route
    {
        private readonly IRouteDataProvider<RouteDataNode> _routeDataProvider;

        #region ctor

        public ExpanditRoute(string url, IRouteHandler routeHandler, IRouteDataProvider<RouteDataNode> routeDataProvider)
            : base(url, routeHandler)
        {
            _routeDataProvider = routeDataProvider;
        }

        public ExpanditRoute(string url, IRouteHandler routeHandler)
            : base(url, routeHandler)
        {
        }

        public ExpanditRoute(string url, RouteValueDictionary defaults, IRouteHandler routeHandler)
            : base(url, defaults, routeHandler)
        {
        }

        public ExpanditRoute(string url, RouteValueDictionary defaults, RouteValueDictionary constraints, IRouteHandler routeHandler)
            : base(url, defaults, constraints, routeHandler)
        {
        }

        public ExpanditRoute(string url, RouteValueDictionary defaults, RouteValueDictionary constraints, RouteValueDictionary dataTokens, IRouteHandler routeHandler)
            : base(url, defaults, constraints, dataTokens, routeHandler)
        {
        }

        #endregion

        public override RouteData GetRouteData(HttpContextBase httpContext)
        {
            string virtualPath =
                string.Concat(httpContext.Request.AppRelativeCurrentExecutionFilePath.Substring(2), httpContext.Request.PathInfo);

            var data =
                new RouteData(this, RouteHandler);

            IRouteDataNode pNode = _routeDataProvider.FillByRoute(virtualPath, httpContext);

            string action = "index";

            if (pNode == null)
            {
                var pathChunks = virtualPath.Split('/');
                pNode = _routeDataProvider.FillByControllerName(pathChunks[0]);

                if (pNode == null || pNode.GroupGuid == 0)
                {
                    pNode = _routeDataProvider.FillByRoute(pathChunks[0], httpContext);
                }

                if (pNode == null)
                {
                    return null;
                }

                if (pathChunks.Length == 2)
                {
                    action = pathChunks[1];
                }
            }

            if (!string.IsNullOrEmpty(pNode.ProductGuid))
            {
                if (data.Values.ContainsKey("productId"))
                {
                    data.Values["productId"] = pNode.ProductGuid;
                }
                else
                {
                    data.Values.Add("productId", pNode.ProductGuid);
                }
            }

            if (data.Values.ContainsKey("id"))
            {
                data.Values["id"] = pNode.GroupGuid.ToString(CultureInfo.InvariantCulture);
            }
            else
            {
                data.Values.Add("id", pNode.GroupGuid.ToString(CultureInfo.InvariantCulture));
            }

            // add the target as well
            if (!string.IsNullOrEmpty(pNode.TargetUrl))
            {
                if (data.Values.ContainsKey("target"))
                {
                    data.Values["target"] = pNode.TargetUrl;
                }
                else
                {
                    data.Values.Add("target", pNode.TargetUrl);
                }
            }

            // and the langaugeGuid
            if (!string.IsNullOrEmpty(pNode.LanguageGuid))
            {
                if (data.Values.ContainsKey("languageGuid"))
                {
                    data.Values["languageGuid"] = pNode.LanguageGuid;
                }
                else
                {
                    data.Values.Add("languageGuid", pNode.LanguageGuid);
                }
            }

            // The controller
            if (!string.IsNullOrEmpty(pNode.Controller))
            {
                if (data.Values.ContainsKey("languageGuid"))
                {
                    data.Values["controller"] = pNode.Controller;
                    data.Values["action"] = action;
                }
                else
                {
                    data.Values.Add("controller", pNode.Controller);
                    data.Values.Add("action", action);
                }
            }

            // The view
            if (!string.IsNullOrEmpty(pNode.ViewFile))
            {
                if (data.Values.ContainsKey("viewName"))
                {
                    data.Values["viewName"] = pNode.ViewFile;
                }
                else
                {
                    data.Values.Add("viewName", pNode.ViewFile);
                }
            }

            DataTokens =
                new RouteValueDictionary(data.Values);

            return data;
        }

        public override VirtualPathData GetVirtualPath(RequestContext requestContext, RouteValueDictionary values)
        {
            return base.GetVirtualPath(requestContext, values);
        }
    }
}