﻿using System.Configuration.Provider;

namespace EISCS.Routing.Providers {
    public class ExpanditProviderCollection : ProviderCollection {
        //Return an instance of RouteProviderBase by name
        new public RouteProviderBase this[string name] {
            get { return (RouteProviderBase)base[name]; }
        }
    }
}
