﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using CmsPublic.DataProviders.Logging;
using CmsPublic.ModelLayer.Models;
using EISCS.CMS.Services.PropertyServices;
using EISCS.ExpandITFramework.Infrastructure.Configuration;
using EISCS.ExpandITFramework.Infrastructure.Configuration.Interfaces;
using EISCS.ExpandITFramework.Util;
using EISCS.Routing.Concrete;
using EISCS.Routing.Concrete.RouteCreation;
using EISCS.Routing.Concrete.RoutePatternFormatting;
using EISCS.Routing.Interfaces;
using EISCS.Routing.RouteDataContainers;
using EISCS.Shop.DO.Interface;

namespace EISCS.Routing.Providers
{
    public class ExpanditRouteProvider : RouteProviderBase
    {

        private IRouteCreator<RouteDataNode> _routeCreator;
        public override IRouteCreator<RouteDataNode> PublicRouteCreator { get { return _routeCreator; } }
        private IRouteDataProvider<RouteDataNode> _routeDataProvider;
        public override IRouteDataProvider<RouteDataNode> PublicRouteDataProvider { get { return _routeDataProvider; } }
        private string _defaultLanguageGuid;
        private readonly IHttpRuntimeWrapper _httpRuntimeWrapper;
        private readonly IPropertiesDataService _propertiesDataService;
        private readonly IGroupRepository _groupRepository;
        private LinkCreator _linkCreator;

        public ExpanditRouteProvider()
        {
            _httpRuntimeWrapper = new HttpRuntimeWrapper();
            _propertiesDataService = DependencyResolver.Current.GetService<IPropertiesDataService>();
            _groupRepository = DependencyResolver.Current.GetService<IGroupRepository>();
        }

        public ExpanditRouteProvider(IHttpRuntimeWrapper httpRuntimeWrapper, IPropertiesDataService propertiesDataService, IGroupRepository groupRepository)
        {
            _httpRuntimeWrapper = httpRuntimeWrapper ?? new HttpRuntimeWrapper();
            _propertiesDataService = propertiesDataService;
            _groupRepository = groupRepository;
        }
        
        /// <summary>
        /// Read the configuration and set up the collaborating classes
        /// </summary>
        /// <param name="name"></param>
        /// <param name="config"></param>
        public override void Initialize(string name, NameValueCollection config)
        {
            try
            {
                // Verify that config is not null
                if (config == null)
                    throw new ArgumentNullException("config");
                base.Initialize(name, config);

                // Read RouteProvider configuration values
                bool isExtensionless = String2Bool(config["extensionless"]);

                string extension = isExtensionless ? string.Empty : config["extension"];
                char replacement = config["replacementchar"].ToCharArray()[0];
                char groupSeparator = config["groupseparator"].ToCharArray()[0];
                bool isReverseGroups = String2Bool(config["ReverseGroupOrder"]);
                bool isTestMode = String2Bool(config["IsTestMode"]);

                string configGroupFormatString = config["GroupFormatString"];
                string configGroupProductFormatString = config["GroupProductFormatString"];
                string configProductFormatString = config["ProductFormatString"];

                // Read additional web.config configuration values
                IConfigurationValue confVal = new ConfigurationValue();
                var defaultGrpTemplate = confVal.AppSettings("DEFAULT_GROUP_TEMPLATE");
                var defaultPrdTemplate = confVal.AppSettings("DEFAULT_PRODUCT_TEMPLATE");
                _defaultLanguageGuid = confVal.AppSettings("LANGUAGE_DEFAULT");
                string neglist = confVal.AppSettings("CATALOG_HIDE_NEGLIST");
                string poslist = confVal.AppSettings("CATALOG_SHOW_POSLIST");
                string templateFolder = confVal.AppSettings("TEMPLATE_FOLDER");

                // initialization moved from global.asax:
                try
                {
                    //Setup Templates
                    if (!isTestMode)
                    {
                        var propertyManager = new PropertyManager(_propertiesDataService);
                        propertyManager.Initialize();

                        string templateFolderTemp = confVal.AppSettings("TEMPLATE_FOLDER");
                        List<TemplateQuerySet> propData = propertyManager.ScanFiles(templateFolderTemp);
                        propertyManager.PersistData(propData);
                    }
                }
                catch (Exception ex)
                {
                    // Swallow exception because an error here indicates that the site is under upload and init will be called again as soon as the upload is finished!
                    return;
                }

                // Get RouteUrlPatternsInfo class - used by the RouteCreator
                RouteUrlPatternsInfo routeUrlPatternInfo = new RouteUrlPatternsInfo
                {
                    GroupPattern =
                        configGroupFormatString,
                    GroupProductPattern =
                        configGroupProductFormatString,
                    ProductPattern =
                        configProductFormatString
                };

                // Make CharReplacemets class - used by the RouteCreator
                bool replaceAscii8 = String2Bool(config["replaceAscii8"]);
                CharReplacements charReplacement = new CharReplacements(replaceAscii8);

                // Make HideTopGroupInfoInfoContainer - used by the RouteCreator
                IHideTopGroupInfoContainer hideTopGroupInfoContainer = new HideTopGroupInfoInfoContainer(poslist, neglist, _defaultLanguageGuid,
                                                                                         _groupRepository);
                // Make routeCreator
                IProductRepository productRepository = DependencyResolver.Current.GetService<IProductRepository>();

                var templateInfos = _propertiesDataService.GetAllTemplateInfo();

                var defaultGroupTemplate = templateInfos.Find(x => x.TemplateFileName == defaultGrpTemplate);

                if (defaultGroupTemplate != null)
                {
                    defaultGrpTemplate = defaultGroupTemplate.TemplateGuid;
                }

                var defaultProductTemplate = templateInfos.Find(x => x.TemplateFileName == defaultPrdTemplate);

                if (defaultProductTemplate != null)
                {
                    defaultPrdTemplate = defaultProductTemplate.TemplateGuid;
                }

                Dictionary<string, Dictionary<int, RouteDataNode>> fastGroupLookup = new Dictionary<string, Dictionary<int, RouteDataNode>>();
                _routeCreator =
                    new RouteCreator<RouteDataNode>(extension, replacement, defaultGrpTemplate, defaultPrdTemplate,
                        routeUrlPatternInfo, charReplacement, groupSeparator, isReverseGroups, hideTopGroupInfoContainer, productRepository, fastGroupLookup, _propertiesDataService);

                // Make "create all routes" module - used by the RouteDataProvider
                IRouteCreationModule<RouteDataNode> recreator = new RouteCreationModule<RouteDataNode>(confVal, _routeCreator);

                // Make lookup - used by the RouteDataProvider
                INodeListLookup<RouteDataNode> nodeListLookup = new NodeListLookup<RouteDataNode>(_defaultLanguageGuid);

                // Get RouteDataProvider
                string dataProviderType = config["DataProvider"];
                _routeDataProvider = RouteDataContainer.GetContainer(dataProviderType).GetProvider(recreator, nodeListLookup, fastGroupLookup);

                // Make LinkCreator - uses the RouteDataProvider
                _linkCreator = new LinkCreator(_routeDataProvider, _httpRuntimeWrapper);

                if (_linkCreator == null)
                    throw new Exception("LinkCreator = null");

                RegisterRoutes();
            }
            catch (Exception ex)
            {
                FileLogger.Log(ex.Message + " " + ex.StackTrace);
                return;
            }
            try
            {
                RouteManager.IsInitialized = true;
            }
            catch (Exception ex)
            {
                // Might be null if unit testing ? 
                FileLogger.Log(ex.Message + " " + ex.StackTrace);
            }
        }

        /// <summary>
        /// Creates an object of type IRouteHandler (ExpanditRouteHandler), which is used by the RouteObject.
        /// Creates the Route object and inserts it into the RouteTable.Routes.
        /// </summary>
        protected override void RegisterRoutes()
        {

            List<RouteBase> toRemove = new List<RouteBase>();
            foreach (var route in RouteTable.Routes)
            {
                if (route is ExpanditRoute)
                {
                    toRemove.Add(route);
                }
            }

            foreach (var route in toRemove)
            {
                RouteTable.Routes.Remove(route);
            }

            // Must define some pattern, else this routing interfers with MVC routing
            const string expanditGroupProductUrlPattern = "{groupId}";

            ExpanditRoute expanditRoute = new ExpanditRoute(expanditGroupProductUrlPattern, new ExpanditMvcIHttpHandler(), _routeDataProvider);



            if (!toRemove.Any())
            {
                RouteTable.Routes.Insert(0, expanditRoute);
                RouteTable.Routes.Insert(0, new Route("{resource}.axd/{*pathInfo}", new StopRoutingHandler()));
            }
            else
            {
                RouteTable.Routes.Insert(1, expanditRoute);
            }
        }

        /// <summary>
        /// Used by the ExpanditRouteHandler to get actual virtual path for the request.
        /// Searches the RequestContext.RouteData Dictionary for a matching target
        /// </summary>
        /// <param name="requestContext"></param>
        /// <returns>A virtual path</returns>
        public override string GetVirtualGroupProductPath(RequestContext requestContext)
        {
            string target = null;
            if (requestContext != null && requestContext.RouteData.Values.ContainsKey("target"))
            {
                target = (string)requestContext.RouteData.Values["target"];
            }
            return target;
        }

        public override string GetControllerName(RequestContext requestContext)
        {
            string controllerName = null;
            if (requestContext != null && requestContext.RouteData.Values.ContainsKey("controller"))
            {
                controllerName = (string)requestContext.RouteData.Values["controller"];
            }
            return controllerName;
        }

        public override string GetViewName(RequestContext requestContext)
        {
            string viewName = null;
            if (requestContext != null && requestContext.RouteData.Values.ContainsKey("viewName"))
            {
                viewName = (string)requestContext.RouteData.Values["viewName"];
            }
            return viewName;
        }

        public override void Register()
        {

        }

        public override string GetGroupId(RequestContext requestContext)
        {
            return requestContext != null && requestContext.RouteData.Values.ContainsKey("id")
                       ? requestContext.RouteData.Values["id"].ToString()
                       : HttpContext.Current.Request["GroupGuid"];
        }

        public override string GetProductId(RequestContext requestContext)
        {
            return requestContext != null && requestContext.RouteData.Values.ContainsKey("productID")
                       ? requestContext.RouteData.Values["productID"].ToString()
                       : HttpContext.Current.Request["ProductGuid"];
        }

        public override string GetLanguage(RequestContext requestContext)
        {
            if (requestContext != null && requestContext.RouteData.Values.ContainsKey("languageGuid"))
            {
                return (string)requestContext.RouteData.Values["languageGuid"];
            }
            return requestContext != null ? GetLanguage(requestContext.HttpContext) : _defaultLanguageGuid;
        }

        public override string GetLanguage(HttpContextBase httpContext)
        {
            HttpCookie cookie = httpContext.Request.Cookies["user"];
            string languageGuid = cookie != null ? cookie["LanguageGuid"] : _defaultLanguageGuid;
            return languageGuid;
        }

        public override string CreateTemplateGroupLink(int groupGuid, string languageGuid)
        {
            return _linkCreator.CreateTemplateGroupLink(groupGuid, languageGuid);
        }

        public override string CreateTemplateGroupLink(Group grp, string languageGuid)
        {
            return _linkCreator.CreateTemplateGroupLink(grp, languageGuid);
        }

        public override string CreateTemplateProductLink(object productguid, object groupguid, string languageGuid)
        {
            return _linkCreator.CreateTemplateProductLink(productguid, groupguid, languageGuid);
        }

        public override string CreateTemplateGroupProductLink(object productguid, object groupguid, string languageGuid)
        {
            return _linkCreator.CreateTemplateGroupProductLink(productguid, groupguid, languageGuid);
        }

        public override void ActiveDateHandler(HttpContextBase httpContext, string languageGuid)
        {
            httpContext.Response.StatusCode = 302;
            //httpContext.Response.AddHeader("Location", path);
        }

        private static bool String2Bool(string strBool)
        {
            bool bBool;
            bool.TryParse(strBool, out bBool);
            return bBool;
        }

        private string VirtualRoot()
        {
            return _httpRuntimeWrapper.AppDomainAppVirtualPath.Equals("/")
                                                 ? ""
                                                 : _httpRuntimeWrapper.AppDomainAppVirtualPath;
        }
    }
}
