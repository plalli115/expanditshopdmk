﻿using System;
using System.Configuration;
using System.Web.Configuration;

namespace EISCS.Routing.Providers {
    public static class RouteManager {
        public static RouteProviderBase Route { get; private set; }
        public static ExpanditProviderCollection Providers { get; private set; }
        public static bool IsInitialized { get; set; }

        static RouteManager() {
            Initialize();
        }

        public static void Initialize() {
            ExpanditProviderConfiguration configuration =
                (ExpanditProviderConfiguration)ConfigurationManager.GetSection("ExpanditRoutingProvider");
            if (configuration == null) {
                throw new Exception("ExpandIT configuration is missing");
            }

            Providers = new ExpanditProviderCollection();

            ProvidersHelper.InstantiateProviders(configuration.Providers, Providers, typeof(RouteProviderBase));

            Providers.SetReadOnly();

            Route = Providers[configuration.Default];
        }
    }
}
