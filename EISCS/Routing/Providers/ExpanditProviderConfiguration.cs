﻿using System.Configuration;

namespace EISCS.Routing.Providers
{
    public class ExpanditProviderConfiguration : ConfigurationSection
    {
        [ConfigurationProperty("providers")]
        public ProviderSettingsCollection Providers
        {
            get { return (ProviderSettingsCollection)base["providers"]; }
        }

        [ConfigurationProperty("default", DefaultValue = "ExpanditRouteProvider")]
        public string Default
        {
            get
            {
                return (string)base["default"];
            }
            set
            {
                base["default"] = value;
            }
        }

        [ConfigurationProperty("extensionless", DefaultValue = "false")]
        public string Extensionless
        {
            get
            {
                return (string)base["extensionless"];
            }
            set
            {
                base["extensionless"] = value;
            }
        }

        [ConfigurationProperty("extension", DefaultValue = ".aspx")]
        public string Extension
        {
            get
            {
                return (string)base["extension"];
            }
            set
            {
                base["extension"] = value;
            }
        }

        [ConfigurationProperty("replacementchar", DefaultValue = "-")]
        public string Replacementchar
        {
            get
            {
                return (string)base["replacementchar"];
            }
            set
            {
                base["replacementchar"] = value;
            }
        }

        [ConfigurationProperty("groupseparator", DefaultValue = "-")]
        public string Groupseparator
        {
            get
            {
                return (string)base["groupseparator"];
            }
            set
            {
                base["groupseparator"] = value;
            }
        }

        [ConfigurationProperty("replaceAscii8", DefaultValue = "true")]
        public string ReplaceAscii8
        {
            get
            {
                return (string)base["ReplaceAscii8"];
            }
            set
            {
                base["ReplaceAscii8"] = value;
            }
        }

        [ConfigurationProperty("GroupFormatString", DefaultValue = "{grouppath}")]
        public string GroupFormatstring
        {
            get
            {
                return (string)base["GroupFormatString"];
            }
            set
            {
                base["GroupFormatString"] = value;
            }
        }

        [ConfigurationProperty("GroupProductFormatString", DefaultValue = "{grouppath}/{productname}")]
        public string GroupProductFormatstring
        {
            get
            {
                return (string)base["GroupProductFormatString"];
            }
            set
            {
                base["GroupProductFormatString"] = value;
            }
        }

        [ConfigurationProperty("ProductFormatString", DefaultValue = "{productname}")]
        public string ProductFormatstring
        {
            get
            {
                return (string)base["ProductFormatString"];
            }
            set
            {
                base["ProductFormatString"] = value;
            }
        }

        [ConfigurationProperty("DataProvider", DefaultValue = "EISCS.Routing.RouteDataContainers.SqlRouteDataContainer, EISCS")]
        public string DataProvider
        {
            get
            {
                return (string)base["DataProvider"];
            }
            set
            {
                base["DataProvider"] = value;
            }
        }

        [ConfigurationProperty("ReverseGroupOrder", DefaultValue = "false")]
        public string ReverseGroupOrder
        {
            get
            {
                return (string)base["ReverseGroupOrder"];
            }
            set
            {
                base["ReverseGroupOrder"] = value;
            }
        }

        [ConfigurationProperty("IsTestMode", DefaultValue = "false")]
        public string IsTestMode
        {
            get
            {
                return (string)base["IsTestMode"];
            }
            set
            {
                base["IsTestMode"] = value;
            }
        }
    }
}
