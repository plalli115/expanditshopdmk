﻿using System.Configuration.Provider;
using System.Web;
using System.Web.Routing;
using CmsPublic.ModelLayer.Models;
using EISCS.Routing.Concrete;
using EISCS.Routing.Interfaces;

namespace EISCS.Routing.Providers
{
    public abstract class RouteProviderBase : ProviderBase
    {
        protected abstract void RegisterRoutes();
        /// <summary>
        /// Creates Route Url's
        /// </summary>
        public abstract IRouteCreator<RouteDataNode> PublicRouteCreator { get; }
        /// <summary>
        /// Manages the Routing Data Store. Responsible for Create, Update, Delete and Lookup operations.
        /// </summary>
        public abstract IRouteDataProvider<RouteDataNode> PublicRouteDataProvider { get; }
        /// <summary>
        /// Searches the RequestContext.RouteData Dictionary for a matching target.
        /// </summary>
        /// <param name="requestContext"></param>
        /// <returns>Virtual path</returns>
        public abstract string GetVirtualGroupProductPath(RequestContext requestContext);

        public abstract string GetGroupId(RequestContext requestContext);
        public abstract string GetProductId(RequestContext requestContext);
        public abstract string GetLanguage(RequestContext requestContext);
        public abstract string GetLanguage(HttpContextBase httpContext);

        public abstract string CreateTemplateGroupLink(int groupGuid, string languageGuid);
        public abstract string CreateTemplateGroupLink(Group grp, string languageGuid);
        public abstract string CreateTemplateProductLink(object productguid, object groupguid, string languageGuid);
        public abstract string CreateTemplateGroupProductLink(object productguid, object groupguid, string languageGuid);

        public abstract void ActiveDateHandler(HttpContextBase httpContext, string languageGuid);
        
        public abstract string GetControllerName(RequestContext requestContext);
        public abstract string GetViewName(RequestContext requestContext);
        public abstract void Register();
    }
}
