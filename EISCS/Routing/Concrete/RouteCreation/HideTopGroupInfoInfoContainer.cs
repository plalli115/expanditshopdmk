﻿using EISCS.Routing.Interfaces;
using EISCS.Shop.DO.Interface;

namespace EISCS.Routing.Concrete.RouteCreation {
    public class HideTopGroupInfoInfoContainer : IHideTopGroupInfoContainer{

        private readonly string _poslist;
        private readonly string _neglist;
        private readonly string _defaultLanguage;
        private readonly IGroupRepository _groupRepository;

        public HideTopGroupInfoInfoContainer(string poslist, string neglist, string defaultLanguage, IGroupRepository groupRepository){
            _poslist = poslist;
            _neglist = neglist;
            _defaultLanguage = defaultLanguage;
            _groupRepository = groupRepository;
        }

        public IHideTopGroupInfo GetInfo(){
            return new HideTopGroupInfo(_poslist,_neglist,_defaultLanguage, _groupRepository);
        }
    }
}
