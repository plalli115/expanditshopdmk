﻿using System;
using System.Collections.Generic;
using System.Linq;
using CmsPublic.ModelLayer.Models;
using EISCS.Routing.Interfaces;
using EISCS.Shop.DO.Interface;

namespace EISCS.Routing.Concrete.RouteCreation {
    public class Tokens {
        public int Index { get; set; }
        public int Guid { get; set; }
    }

    public class TokensComparer : IEqualityComparer<Tokens>{

        #region IEqualityComparer<Tokens> Members

        public bool Equals(Tokens x, Tokens y) {
            //Check whether the compared objects reference the same data.
            if (ReferenceEquals(x, y)) return true;

            //Check whether any of the compared objects is null.
            if (ReferenceEquals(x, null) || ReferenceEquals(y, null))
                return false;

            //Check whether the Guid properties are equal.
            return x.Guid == y.Guid;
        }

        public int GetHashCode(Tokens obj) {
            //Check whether the object is null
            if (ReferenceEquals(obj, null)) {return 0;}

            //Get hash code for the Guid field.
            int hashGuid = obj.Guid.GetHashCode();

            //Get hash code for the Index field.
            int hashIndex = obj.Index.GetHashCode();

            //Calculate the hash code for the token.
            return hashGuid ^ hashIndex;
        }

        #endregion
    }

    public class HideTopGroupInfo : IHideTopGroupInfo{

        public IGroupRepository Gr { get; private set; }

        private enum MenuMode {
            Positive, Negative, None
        }
        
        private readonly List<int> _menuTokens;
        private readonly MenuMode _menuMode;
        private readonly List<Group> _topGroupList;

        public HideTopGroupInfo(string poslist, string neglist, string defaultLanguage, IGroupRepository groupRepository) {
            _menuTokens = new List<int>();
            _menuMode = SetMenuMode(poslist, neglist);
            Gr = groupRepository;
            _topGroupList = Gr.GetTopGroups(defaultLanguage, false);
        }

        private bool FillMenuTokens(string numbers) {
            var tokens = numbers.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            if (tokens.Count() > 0) {
                foreach (var token in tokens) {
                    int iToken;
                    if (int.TryParse(token, out iToken)) {
                        _menuTokens.Add(iToken);
                    }
                }
                return true;
            }
            return false;
        }

        /// <summary>
        /// Determine MenuMode Usage
        /// </summary>
        /// <param name="poslist"></param>
        /// <param name="neglist"></param>
        /// <returns></returns>
        private MenuMode SetMenuMode(string poslist, string neglist) {
            if (!string.IsNullOrEmpty(poslist)) {
                bool hasTokens = FillMenuTokens(poslist);
                if (hasTokens) {
                    return MenuMode.Positive;
                }
            }
            if (!string.IsNullOrEmpty(neglist)) {
                bool hasTokens = FillMenuTokens(neglist);
                if (hasTokens) {
                    return MenuMode.Negative;
                }
            }
            return MenuMode.None;
        }

        public bool IsToHideRoot(Group group) {
            if (_menuMode == MenuMode.None) {
                return false;
            }
            // 1. Create a list of indexes and GroupGuids from the _topGroupList
            List<Tokens> topGroupTokens = _topGroupList.Select((groupTree, i) => new Tokens { Index = i, Guid = groupTree.GroupGuid }).ToList();
            // 2. Create a list to store the results from filtering below
            List<Tokens> topGroupGuids = new List<Tokens>();
            // 3. Filter on the values in the menuToken list to get the corresponding top groups
            //    If it is a positive list then the topGroupGuids will hold the Catalog Roots (Guids). 
            //    If it is a negative list then the topGroupGuids will hold the Menu Roots (Guids).
            foreach (int menuToken in _menuTokens) {
                int index;
                if (menuToken < 0) {
                    index = topGroupTokens.Count + menuToken;
                }
                else {
                    index = menuToken;
                }
                if (topGroupTokens.Exists(x => x.Index == index)) {
                    topGroupGuids.Add(new Tokens { Guid = topGroupTokens[index].Guid, Index = topGroupTokens[index].Index });
                }
            }

            List<Tokens> menus = null;
            List<Tokens> catalogs = null;

            if (_menuMode == MenuMode.Negative) {
                // The topGroupGuids list contains Menu Root Guids, have to find the Catalog Root Guids
                menus = topGroupGuids;
                catalogs = Enumerable2List(topGroupTokens.Except(topGroupGuids, new TokensComparer()));
            }
            if (_menuMode == MenuMode.Positive) {
                // The topGroupGuids list contains Catalog Root Guids, have to find the Menu Root Guids
                menus = Enumerable2List(topGroupTokens.Except(topGroupGuids, new TokensComparer()));
                catalogs = topGroupGuids;
            }

            // Get root (ID) of current group
            Group root = GetRoot(group, new Group());
            int rootGuid = root.ID;

            // If part of menu - hide root in url
            if (menus != null) {
                if (menus.Exists(x => x.Guid == rootGuid)) {
                    return true;
                }
            }

            // If part of catalog - hide root in url if this is the only catalog
            if(catalogs != null)
            {
                if(catalogs.Exists(x => x.Guid == rootGuid))
                {
                    return catalogs.Count == 1;
                }
            }

            return false;
        }

        private static List<Tokens> Enumerable2List(IEnumerable<Tokens> enumerable) {
            List<Tokens> retList = new List<Tokens>();
            retList.AddRange(enumerable);
            return retList;
        }


        public Group GetRoot(Group group, Group dummy) {
            if (group.ParentGuid != 0) {
                dummy = group.Parent;
                dummy = GetRoot(group.Parent, dummy);
            }
            return dummy;
        }
    }
}
