﻿using System;
using System.Linq;
using System.Text;

namespace EISCS.Routing.Concrete.RouteCreation {
    public class CharReplacer {

        private static readonly char[] StandardReplacements = { ' ', '¤', '#', '%', '*', '{', '}', '<', '>', '+', '?', ',', ';', '.', ':', '"', '/', '\\', '&' };

        public static string ReplaceUnwanted(string url, CharReplacements charReplacements) {
            return charReplacements.ReplacementList.Aggregate(url, (current, charReplacement) => ReplaceUnwanted(current, charReplacement.Replacement, charReplacement.ToReplace));
        }

        public static string ReplaceUnwanted(string url, char repl, char[] toReplace) {
            if (String.IsNullOrEmpty(url)) return "";
            // to lowercase, trim extra spaces
            url = url.ToLower().Trim();
            var len = url.Length;
            var sb = new StringBuilder(len);

            for (int i = 0; i < url.Length; i++) {
                char c = url[i];
                sb.Append(toReplace.Any(x => x == c) ? repl : c);
                if (i == 150) { break; }
            }
            return sb.ToString();
        }

        public static string ReplaceIllegal(string url, char repl) {
            if (String.IsNullOrEmpty(url)) return repl.ToString();
            // to lowercase, trim extra spaces
            url = url.ToLower().Trim();
            var len = url.Length;
            var sb = new StringBuilder(len);
            bool prevrepl = false;
            for (int i = 0; i < url.Length; i++) {
                char c = url[i];
                if (StandardReplacements.Any(x => x == c)) {
                    if (!prevrepl) {
                        sb.Append(repl);
                        prevrepl = true;
                    }
                }
                else {
                    sb.Append(c);
                    prevrepl = false;
                }
                if (i == 150) { break; }
            }
            url = sb.ToString();
            // remove trailing illegal
            if (url.EndsWith(repl.ToString())) {
                url = url.Substring(0, url.Length - 1);
            }
            return url;
        }
    }
}
