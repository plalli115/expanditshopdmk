﻿using System.Collections.Generic;

namespace EISCS.Routing.Concrete.RouteCreation {
    public class CharReplacementNode {
        public char[] ToReplace { get; set; }
        public char Replacement { get; set; }
    }
    public class CharReplacements {
        public List<CharReplacementNode> ReplacementList { get; set; }

        public CharReplacements()
            : this(false) {
        }

        public CharReplacements(bool useDefault) {
            if (useDefault)
                DefaultReplacements();
            else {
                ReplacementList = new List<CharReplacementNode>();
            }
        }

        public void DefaultReplacements() {
            CharReplacementNode node1 = new CharReplacementNode {
                Replacement = 'a',
                ToReplace =
                    new[] { 'à', 'À', 'á', 'Á', 'â', 'Â', 'ã', 'Ã', 'å', 'Å', 'ä', 'Ä', 'æ', 'Æ', 'Ą', 'ą', 'Ă', 'ă' }
            };
            CharReplacementNode node2 = new CharReplacementNode {
                Replacement = 'c',
                ToReplace =
                    new[] { 'Ç', 'ç', 'Ć', 'ć', 'Č', 'č' }
            };
            CharReplacementNode node3 = new CharReplacementNode {
                Replacement = 'd',
                ToReplace =
                    new[] { 'Ð', 'ð' }
            };
            CharReplacementNode node4 = new CharReplacementNode {
                Replacement = 'e',
                ToReplace =
                    new[] { 'È', 'è', 'É', 'é', 'Ê', 'ê', 'Ë', 'ë', 'Ę', 'ę', 'Ě', 'ě' }
            };
            CharReplacementNode node5 = new CharReplacementNode {
                Replacement = 'g',
                ToReplace =
                    new[] { 'Ğ', 'ğ' }
            };
            CharReplacementNode node6 = new CharReplacementNode {
                Replacement = 'i',
                ToReplace =
                    new[] { 'Ì', 'ì', 'Í', 'í', 'Î', 'î', 'Ï', 'ï', 'İ', 'ı' }
            };

            CharReplacementNode node7 = new CharReplacementNode {
                Replacement = 'l',
                ToReplace =
                    new[] { 'Ł', 'ł', 'Ĺ', 'ĺ' }
            };
            CharReplacementNode node8 = new CharReplacementNode {
                Replacement = 'n',
                ToReplace =
                    new[] { 'Ñ', 'ñ', 'Ń', 'ń', 'Ň', 'ň' }
            };
            CharReplacementNode node9 = new CharReplacementNode {
                Replacement = 'o',
                ToReplace =
                    new[] { 'Ò', 'ò', 'Ó', 'ó', 'Ô', 'ô', 'Õ', 'õ', 'Ö', 'ö', 'Ø', 'ø', 'Œ', 'œ' }
            };
            CharReplacementNode node10 = new CharReplacementNode {
                Replacement = 'p',
                ToReplace =
                    new[] { 'Þ', 'þ' }
            };
            CharReplacementNode node11 = new CharReplacementNode {
                Replacement = 'r',
                ToReplace =
                    new[] { 'Ŕ', 'ŕ', 'Ř', 'ř' }
            };
            CharReplacementNode node12 = new CharReplacementNode {
                Replacement = 's',
                ToReplace =
                    new[] { 'Ş', 'ş', 'Ś', 'ś', 'ß', 'Š', 'š' }
            };
            CharReplacementNode node13 = new CharReplacementNode {
                Replacement = 'u',
                ToReplace =
                    new[] { 'Ù', 'ù', 'Ú', 'ú', 'Û', 'û', 'Ü', 'ü' }
            };
            CharReplacementNode node14 = new CharReplacementNode {
                Replacement = 'y',
                ToReplace =
                    new[] { 'Ý', 'ý', 'Ÿ', 'ÿ' }
            };
            CharReplacementNode node15 = new CharReplacementNode {
                Replacement = 'z',
                ToReplace =
                    new[] { 'Ź', 'ź', 'Ż', 'ż', 'Ž', 'ž' }
            };
            

            List<CharReplacementNode> replacementNodes =
                new List<CharReplacementNode>
                    {
                        node1, node2, node3, node4, node5, node6, node7, node8, node9, node10, node11, node12, node13, node14, node15
                    };
            ReplacementList = replacementNodes;
        }
    }
}
