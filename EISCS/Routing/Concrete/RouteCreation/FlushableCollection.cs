﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using EISCS.Routing.Interfaces;

namespace EISCS.Routing.Concrete.RouteCreation {
    public class FlushableCollection<T> : Collection<T> where T : class, IRouteDataNode {
        private readonly IRouteDataProvider<T> _dataProvider;
        private readonly int _maxItems;

        public FlushableCollection(IRouteDataProvider<T> dataProvider)
            : this(dataProvider, 9000) {
        }

        public FlushableCollection(IRouteDataProvider<T> dataProvider, int maxItems) {
            _dataProvider = dataProvider;
            _maxItems = maxItems;
        }

        private void Flush() {
            List<T> myList = this.ToList();

            _dataProvider.AddRangeFlushable(myList);

            foreach (T t in myList) {
                Remove(t);
            }
        }

        protected override void InsertItem(int index, T item) {
            base.InsertItem(Count, item);
            if (Count == _maxItems) {
                Flush();
            }

        }
    }
}
