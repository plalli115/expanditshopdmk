﻿using System.Collections.Generic;
using System.Web.Mvc;
using CmsPublic.ModelLayer.Models;
using EISCS.Catalog.Logic;
using EISCS.ExpandITFramework.Infrastructure.Configuration.Interfaces;
using EISCS.Routing.Interfaces;
using EISCS.Shop.DO.Dto;
using EISCS.Shop.DO.Interface;
using StackExchange.Profiling;

namespace EISCS.Routing.Concrete.RouteCreation {
    public class RouteCreationModule<T> : IRouteCreationModule<T> where T : class , IRouteDataNode{
        private readonly IConfigurationValue _confVal;
        private readonly IRouteCreator<T> _routeCreator;
        private ILanguageRepository _languagesRepository;
        private IGroupRepository _groupRepository;

        public RouteCreationModule(IConfigurationValue conf, IRouteCreator<T> routeCreator){
            _confVal = conf;
            _routeCreator = routeCreator;
        }

        public IList<T> PrepareCreation(IRouteDataProvider<T> dataProvider) {
            var profiler = MiniProfiler.Current;
            using (profiler.Step("Loading TopMenu"))
            {
                _languagesRepository = DependencyResolver.Current.GetService<ILanguageRepository>();
                _groupRepository = DependencyResolver.Current.GetService<IGroupRepository>();

                List<LanguageTable> languages = _languagesRepository.LoadLanguages();
                CatalogLogic catLogic = new CatalogLogic(_confVal, _groupRepository);
                FlushableCollection<T> routeDataNodes = new FlushableCollection<T>(dataProvider);

                foreach (var language in languages)
                {
                    string languageGuid = language.LanguageGuid.ToLower();

                    // Load all groups as a tree structure
                    Group group = catLogic.GetAllGroupTree(languageGuid);

                    _routeCreator.CreateAll(routeDataNodes, group, languageGuid);

                }

                return routeDataNodes;
            }
        }
    }
}
