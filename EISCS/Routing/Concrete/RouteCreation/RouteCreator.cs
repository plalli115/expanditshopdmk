﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using CmsPublic.ModelLayer.Models;
using EISCS.ExpandITFramework.Infrastructure.Configuration;
using EISCS.ExpandITFramework.Infrastructure.Configuration.Interfaces;
using EISCS.Routing.Concrete.RoutePatternFormatting;
using EISCS.Routing.Interfaces;
using EISCS.Shop.DO.Interface;

namespace EISCS.Routing.Concrete.RouteCreation
{
    public class RouteCreator<T> : IRouteCreator<T> where T : class, IRouteDataNode, new()
    {
        private readonly string _extension;
        private readonly char _replacement;
        private string _defaultGrpTemplate;
        private readonly string _defaultPrdTemplate;
        private readonly IProductRepository _pr;
        private readonly IPropertiesDataService _propertiesDataService;
        private readonly RouteUrlPatternsInfo _routeUrlPatternInfo;
        private readonly CharReplacements _charReplacements;
        private readonly char _groupSeparator;
        private readonly bool _isReverseGroups;
        private IHideTopGroupInfo _hideGroupInfo;
        private readonly IHideTopGroupInfoContainer _hideTopGroupInfoContainer;
        private readonly IConfigurationValue _confVal = new ConfigurationValue();
        private readonly string _defaultLanguageGuid;
        // Holds all groupProducts
        private List<GroupProduct> _groupProducts;
        // Dictionary of languageguid, (dictionary of groupguid, nodes)
        private readonly Dictionary<string, Dictionary<int, T>> _fastGroupLookup;
        private ConcurrentDictionary<int, GroupDataNode> _groupDataNodes;

        public class GroupDataNode
        {
            public string LongPath { get; set; }
            public string ShortPath { get; set; }
        }

        public RouteCreator(string extension, char replacement, string defaultGrpTemplate, string defaultPrdTemplate, RouteUrlPatternsInfo routeUrlPatternsInfo, CharReplacements charReplacements, char groupSeparator, bool isReverseGroups, IHideTopGroupInfoContainer hideTopGroupInfoContainer, IProductRepository productRepository, Dictionary<string, Dictionary<int, T>> fastGroupLookup, IPropertiesDataService propertiesDataService)
        {
            _extension = extension;
            _replacement = replacement;
            _defaultGrpTemplate = defaultGrpTemplate;
            _defaultPrdTemplate = defaultPrdTemplate;
            _pr = productRepository;
            _propertiesDataService = propertiesDataService;
            _routeUrlPatternInfo = routeUrlPatternsInfo;
            _charReplacements = charReplacements;
            _groupSeparator = groupSeparator;
            _isReverseGroups = isReverseGroups;
            _hideTopGroupInfoContainer = hideTopGroupInfoContainer;
            _defaultLanguageGuid = _confVal.AppSettings("LANGUAGE_DEFAULT");
            _fastGroupLookup = fastGroupLookup;
            ReadTemplateTable();
        }

        #region IRouteCreator Members

        public void CreateAll(IList<T> routeNodeList, Group catalog, string languageGuid)
        {
            CreateProductRoutes(routeNodeList, languageGuid);
            CreateGroupProductRoutes(routeNodeList, catalog, languageGuid);
        }

        /// <summary>
        /// Create product name routes for all products
        /// </summary>
        /// <param name="routeNodeList"></param>
        /// <param name="languageGuid"></param>
        public void CreateProductRoutes(IList<T> routeNodeList, string languageGuid)
        {
            IEnumerable<Product> products = _pr.GetAllProducts(languageGuid, true);
            foreach (var product in products)
            {
                CreateProductRoutes(routeNodeList, product, languageGuid);
            }
        }

        /// <summary>
        /// Create product name route for a single product
        /// </summary>
        /// <param name="routeNodeList"></param>
        /// <param name="product"></param>
        /// <param name="languageGuid"></param>
        public void CreateProductRoutes(IList<T> routeNodeList, Product product, string languageGuid)
        {
            string trail = DefineProductPath(product.ProductName, languageGuid, product.ProductGuid);
            string templateGuid = SafePropDict(product.Properties.PropDict, "TEMPLATE");
            T t = new T
            {
                GroupGuid = 0,
                LanguageGuid = languageGuid,
                ProductGuid = product.ProductGuid,
                RouteUrl = trail,
                TargetUrl = EnsurePrdTmpl(templateGuid),
                TypeCode = "$Auto",
                Controller = EnsureController(templateGuid, PropOwnerTypeInfo.Product),
                ViewFile = EnsureView(templateGuid, PropOwnerTypeInfo.Product)
            };
            routeNodeList.Add(t);
            if (string.IsNullOrEmpty(SafePropDict(product.Properties.PropDict, "ALIAS"))) return;
            trail = DefineProductPath(product.Properties.PropDict["ALIAS"], languageGuid, product.ProductGuid);
            T t2 = new T
            {
                GroupGuid = 0,
                LanguageGuid = languageGuid,
                ProductGuid = product.ProductGuid,
                RouteUrl = trail,
                TargetUrl = EnsurePrdTmpl(templateGuid),
                TypeCode = "$Alias",
                Controller = EnsureController(templateGuid, PropOwnerTypeInfo.Product),
                ViewFile = EnsureView(templateGuid, PropOwnerTypeInfo.Product)
            };
            routeNodeList.Add(t2);
        }

        /// <summary>
        /// Create path routes for all groups and products underneath the (catalog) tree node
        /// </summary>
        /// <param name="routeNodeList"></param>
        /// <param name="treeNode"></param>
        /// <param name="languageGuid"></param>
        public void CreateGroupProductRoutes(IList<T> routeNodeList, Group treeNode, string languageGuid)
        {
            // Load all GroupProducts and store them in the _groupProducts list
            _groupProducts = _pr.GetAllGroupProducts(languageGuid, true);
            if (_groupProducts == null)
            {
                throw new Exception("_groupProducts = null");
            }

            // Get hide top group information
            _hideGroupInfo = _hideTopGroupInfoContainer.GetInfo();
            CreateGroupProductRoutesInternal(routeNodeList, treeNode, languageGuid);
            CreateGroupProductRoutes(routeNodeList, languageGuid);
            // Dispose List when done
            _groupProducts = null;
        }

        /// <summary>
        /// Create path routes for a single product
        /// </summary>
        /// <param name="routeNodeList"></param>
        /// <param name="catalog"></param>
        /// <param name="product"></param>
        /// <param name="languageGuid"></param>
        public void CreateGroupProductRoutes(IList<T> routeNodeList, Group catalog, Product product, string languageGuid)
        {
            // Get hide top group information
            _hideGroupInfo = _hideTopGroupInfoContainer.GetInfo();

            string longPath = CreateSeoName(catalog, _replacement);
            string shortPath = CreateShortSeoName(catalog, _replacement);
            string productString = CharReplacer.ReplaceUnwanted(CharReplacer.ReplaceIllegal(product.ProductName, _replacement),
                                                                    _charReplacements);
            string productGuid = CharReplacer.ReplaceUnwanted(CharReplacer.ReplaceIllegal(product.ProductGuid, _replacement),
                                                                    _charReplacements);
            string ptrail = _routeUrlPatternInfo.GroupProductPattern
                            .Replace("{grouppath}", longPath)
                            .Replace("{groupname}", shortPath)
                            .Replace("{groupguid}", catalog.GroupGuid.ToString(CultureInfo.InvariantCulture))
                            .Replace("{productname}", productString)
                            .Replace("{language}", languageGuid.ToLower())
                            .Replace("{productguid}", productGuid)
                            + _extension;

            string templateGuid = SafePropDict(product.Properties.PropDict, "TEMPLATE");

            T t1 = new T
            {
                RouteUrl = ptrail,
                GroupGuid = catalog.ID,
                LanguageGuid = languageGuid,
                ProductGuid = product.ProductGuid,
                TargetUrl = EnsurePrdTmpl(templateGuid),
                TypeCode = "$Auto",
                Controller = EnsureController(templateGuid, PropOwnerTypeInfo.Product),
                ViewFile = EnsureView(templateGuid, PropOwnerTypeInfo.Product)
            };
            routeNodeList.Add(t1);

            if (string.IsNullOrEmpty(SafePropDict(product.Properties.PropDict, "ALIAS"))) return;

            string alias = product.Properties.PropDict["ALIAS"];

            productString = CharReplacer.ReplaceUnwanted(CharReplacer.ReplaceIllegal(alias, _replacement),
                                                                    _charReplacements);
            productGuid = CharReplacer.ReplaceUnwanted(CharReplacer.ReplaceIllegal(product.ProductGuid, _replacement),
                                                                    _charReplacements);
            ptrail = _routeUrlPatternInfo.GroupProductPattern
                            .Replace("{grouppath}", longPath)
                            .Replace("{groupname}", shortPath)
                            .Replace("{groupguid}", catalog.GroupGuid.ToString(CultureInfo.InvariantCulture))
                            .Replace("{productname}", productString)
                            .Replace("{language}", languageGuid.ToLower())
                            .Replace("{productguid}", productGuid)
                            + _extension;

            T t2 = new T
            {
                RouteUrl = ptrail,
                GroupGuid = catalog.ID,
                LanguageGuid = languageGuid,
                ProductGuid = product.ProductGuid,
                TargetUrl = EnsurePrdTmpl(SafePropDict(product.Properties.PropDict, "TEMPLATE")),
                TypeCode = "$Alias",
                Controller = EnsureController(templateGuid, PropOwnerTypeInfo.Product),
                ViewFile = EnsureView(templateGuid, PropOwnerTypeInfo.Product)
            };
            routeNodeList.Add(t2);
        }

        #endregion

        /// <summary>
        /// Create path routes for all groups and products underneath the (catalog) tree node
        /// </summary>
        /// <param name="routeNodeList"></param>
        /// <param name="catalog"></param>
        /// <param name="languageGuid"></param>
        private void CreateGroupProductRoutesInternal(IList<T> routeNodeList, Group catalog, string languageGuid)
        {
            _groupDataNodes = new ConcurrentDictionary<int, GroupDataNode>();

            foreach (Group group in catalog.Children)
            {

                string longPath = CreateSeoName(group, _replacement);
                string shortPath = CreateShortSeoName(group, _replacement);

                // Save calculated group paths for use in GroupProduct path creation
                GroupDataNode groupDataNode = new GroupDataNode { LongPath = longPath, ShortPath = shortPath };
                if (!_groupDataNodes.ContainsKey(group.ID))
                {
                    _groupDataNodes.TryAdd(group.ID, groupDataNode);
                }

                string trail = _routeUrlPatternInfo.GroupPattern
                    .Replace("{grouppath}", longPath)
                    .Replace("{groupname}", shortPath)
                    .Replace("{language}", languageGuid.ToLower())
                    .Replace("{groupguid}", group.GroupGuid.ToString(CultureInfo.InvariantCulture))
                    + _extension;

                T t = new T
                {
                    RouteUrl = trail,
                    GroupGuid = group.ID,
                    LanguageGuid = languageGuid,
                    ProductGuid = null,
                    TargetUrl = EnsureGrpTmpl(SafePropDict(group.Properties.PropDict, "TEMPLATE")),
                    TypeCode = "$Auto",
                    Controller = EnsureController(SafePropDict(group.Properties.PropDict, "TEMPLATE"), PropOwnerTypeInfo.Group), // TODO: Check load of group.TemplateController & group.TemplateFileName
                    ViewFile = EnsureView(SafePropDict(group.Properties.PropDict, "TEMPLATE"), PropOwnerTypeInfo.Group)
                };
                routeNodeList.Add(t);

                // Maintain dictionary for group info
                if (!_fastGroupLookup.ContainsKey(languageGuid))
                {
                    _fastGroupLookup.Add(languageGuid, new Dictionary<int, T> { { @group.ID, t } });
                }
                else if (!_fastGroupLookup[languageGuid].ContainsKey(group.ID))
                {
                    _fastGroupLookup[languageGuid].Add(group.ID, t);
                }

                if (group.HasChildren())
                {
                    CreateGroupProductRoutesInternal(routeNodeList, group, languageGuid);
                }
            }
        }

        private void CreateGroupProductRoutes(IList<T> routeNodeList, string languageGuid)
        {
            foreach (var item in _groupDataNodes)
            {
                // Get the products for the current group
                IEnumerable<GroupProduct> groupProducts = GetGroupProducts(item.Key, languageGuid);
                foreach (var groupProduct in groupProducts)
                {
                    try
                    {
                        string product = CharReplacer.ReplaceUnwanted(CharReplacer.ReplaceIllegal(groupProduct.ProductName, _replacement),
                                                                    _charReplacements);
                        string productGuid = CharReplacer.ReplaceUnwanted(CharReplacer.ReplaceIllegal(groupProduct.ProductGuid, _replacement),
                                                                    _charReplacements);
                        string ptrail = _routeUrlPatternInfo.GroupProductPattern
                            .Replace("{grouppath}", item.Value.LongPath)
                            .Replace("{groupname}", item.Value.ShortPath)
                            .Replace("{groupguid}", groupProduct.GroupGuid.ToString(CultureInfo.InvariantCulture))
                            .Replace("{productname}", product)
                            .Replace("{language}", languageGuid.ToLower())
                            .Replace("{productguid}", productGuid)
                            + _extension;

                        string templateGuid = SafePropDict(groupProduct.Properties.PropDict, "TEMPLATE");

                        T t2 = new T
                        {
                            RouteUrl = ptrail,
                            GroupGuid = item.Key,
                            LanguageGuid = languageGuid,
                            ProductGuid = groupProduct.ProductGuid,
                            TargetUrl = EnsurePrdTmpl(templateGuid),
                            TypeCode = "$Auto",
                            Controller = EnsureController(templateGuid, PropOwnerTypeInfo.Product),
                            ViewFile = EnsureView(templateGuid, PropOwnerTypeInfo.Product)
                        };
                        routeNodeList.Add(t2);
                        if (string.IsNullOrEmpty(SafePropDict(groupProduct.Properties.PropDict, "ALIAS"))) continue;

                        string alias = groupProduct.Properties.PropDict["ALIAS"];

                        product = CharReplacer.ReplaceUnwanted(CharReplacer.ReplaceIllegal(alias, _replacement),
                                                                    _charReplacements);
                        productGuid = CharReplacer.ReplaceUnwanted(CharReplacer.ReplaceIllegal(groupProduct.ProductGuid, _replacement),
                                                                    _charReplacements);
                        ptrail = _routeUrlPatternInfo.GroupProductPattern
                            .Replace("{grouppath}", item.Value.LongPath)
                            .Replace("{groupname}", item.Value.ShortPath)
                            .Replace("{groupguid}", groupProduct.GroupGuid.ToString(CultureInfo.InvariantCulture))
                            .Replace("{productname}", product)
                            .Replace("{language}", languageGuid.ToLower())
                            .Replace("{productguid}", productGuid)
                            + _extension;

                        T t3 = new T
                        {
                            RouteUrl = ptrail,
                            GroupGuid = item.Key,
                            LanguageGuid = languageGuid,
                            ProductGuid = groupProduct.ProductGuid,
                            TargetUrl = EnsurePrdTmpl(templateGuid),
                            TypeCode = "$Alias",
                            Controller = EnsureController(templateGuid, PropOwnerTypeInfo.Product),
                            ViewFile = EnsureView(templateGuid, PropOwnerTypeInfo.Product)
                        };
                        routeNodeList.Add(t3);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        Console.WriteLine(ex.StackTrace);
                        throw;
                    }
                }
            }
        }

        // Get GroupProducts by groupGuid
        private IEnumerable<GroupProduct> GetGroupProducts(int groupGuid, string languageGuid)
        {
            if (_groupProducts == null)
            {
                // Initial loading has gone wrong, try to reload once
                if ((_groupProducts = _pr.GetAllGroupProducts(languageGuid, true)) == null)
                {
                    throw new Exception("Failed to load groupProducts in RouteCreator.");
                }
            }
            return _groupProducts.FindAll(x => x.GroupGuid == groupGuid);
        }

        // ProductName
        private string DefineProductPath(string productName, string languageGuid, string productGuid)
        {

            string product = CharReplacer.ReplaceUnwanted(CharReplacer.ReplaceIllegal(productName, _replacement),
                                                        _charReplacements);
            productGuid = CharReplacer.ReplaceUnwanted(CharReplacer.ReplaceIllegal(productGuid, _replacement),
                                                                    _charReplacements);
            string trail = _routeUrlPatternInfo.ProductPattern
                .Replace("{productname}", product)
                .Replace("{language}", languageGuid.ToLower())
                .Replace("{productguid}", productGuid);

            return trail + _extension;

        }

        // GroupName only
        private string CreateShortSeoName(Group grp, char repl)
        {
            string name = grp.Properties.PropDict["NAME"];
            if (grp.Properties.PropDict.ContainsKey("ALIAS"))
            {
                string alias = grp.Properties.PropDict["ALIAS"];
                if (!string.IsNullOrEmpty(alias))
                {
                    name = alias;
                }
            }
            // Fallback to default language
            if (string.IsNullOrEmpty(name))
            {
                name = _propertiesDataService.GetSingleProperty(grp.ID.ToString(CultureInfo.InvariantCulture), "GRP", "NAME", _defaultLanguageGuid);
            }
            return CharReplacer.ReplaceUnwanted(CharReplacer.ReplaceIllegal(name, repl), _charReplacements);
        }

        // GroupPath
        private string CreateSeoName(Group grp, char repl)
        {
            string returnString = "";
            if (grp != null)
            {
                List<string> stringList = new List<string>();
                CreateSeoTrail(grp, stringList);
                // Remove last item in list here if only one top catalog group!
                if (_hideGroupInfo.IsToHideRoot(grp))
                {
                    stringList.RemoveAt(stringList.Count - 1);
                }
                if (!_isReverseGroups)
                    stringList.Reverse();
                returnString = stringList.Aggregate(returnString, (current, s) =>
                    current + (CharReplacer.ReplaceUnwanted(CharReplacer.ReplaceIllegal(s, repl), _charReplacements) + _groupSeparator));
                if (stringList.Count > 0)
                {
                    returnString = returnString.Remove(returnString.Length - 1);
                }
            }
            return returnString;
        }

        // Add group names to the list of strings
        private void CreateSeoTrail(Group grp, ICollection<string> strList)
        {
            if (grp == null) return;
            if (grp.Properties != null && grp.Properties.PropDict != null)
            {
                string name = SafePropDict(grp.Properties.PropDict, "NAME");
                if (grp.Properties.PropDict.ContainsKey("ALIAS"))
                {
                    string alias = grp.Properties.PropDict["ALIAS"];
                    if (!string.IsNullOrEmpty(alias))
                    {
                        name = alias;
                    }
                }
                // Fallback to default language
                if (string.IsNullOrEmpty(name))
                {
                    name = _propertiesDataService.GetSingleProperty(grp.ID.ToString(CultureInfo.InvariantCulture), "GRP", "NAME", _defaultLanguageGuid);
                }
                strList.Add(name);
            }
            if (grp.Parent != null && grp.Parent.ID != 0)
            {
                CreateSeoTrail(grp.Parent, strList);
            }
        }

        private static string SafePropDict(IDictionary<string, string> dict, string propName)
        {
            if (dict == null)
            {
                return "";
            }
            return dict.ContainsKey(propName) ? dict[propName] : "";
        }

        private string EnsureGrpTmpl(string templateGuid)
        {
            return string.IsNullOrEmpty(templateGuid) ? _defaultGrpTemplate : templateGuid;
        }

        private string EnsurePrdTmpl(string templateGuid)
        {
            return string.IsNullOrEmpty(templateGuid) ? _defaultPrdTemplate : templateGuid;
        }

        private string EnsureController(string templateGuid, PropOwnerTypeInfo info)
        {
            string controller = null;
            var firstOrDefault = _templateTable.FirstOrDefault(t => t.TemplateGuid == templateGuid);
            if (firstOrDefault != null)
            {
                controller = firstOrDefault.TemplateController;
            }

            string defaultController = info == PropOwnerTypeInfo.Group ? _defaultGroupController : _defaultProductController;

            return string.IsNullOrEmpty(controller) ? defaultController : controller;
        }

        private string EnsureView(string templateGuid, PropOwnerTypeInfo info)
        {
            string view = null;
            var firstOrDefault = _templateTable.FirstOrDefault(t => t.TemplateGuid == templateGuid);
            if (firstOrDefault != null)
            {
                view = firstOrDefault.TemplateFileName;
            }

            string defaultView = info == PropOwnerTypeInfo.Group ? _defaultGroupView : _defaultProductView;

            return string.IsNullOrEmpty(view) ? defaultView : view;
        }

        private List<TemplateQuerySet> _templateTable;
        private string _defaultGroupController;
        private string _defaultProductController;
        private string _defaultGroupView;
        private string _defaultProductView;

        private enum PropOwnerTypeInfo
        {
            Group,
            Product
        };

        private void ReadTemplateTable()
        {
            _templateTable = _propertiesDataService.GetAllTemplateInfo();
            var groupTemplate = _templateTable.FirstOrDefault(t => t.TemplateName == "Group");
            if (groupTemplate != null)
            {
                _defaultGrpTemplate = groupTemplate.TemplateGuid;
                _defaultGroupController = groupTemplate.TemplateController;
                _defaultGroupView = groupTemplate.TemplateFileName;
            }
            var productTemplate = _templateTable.FirstOrDefault(t => t.TemplateName == "Product");
            if (productTemplate != null)
            {
                _defaultGrpTemplate = productTemplate.TemplateGuid;
                _defaultProductController = productTemplate.TemplateController;
                _defaultProductView = productTemplate.TemplateFileName;
            }
        }
    }
}
