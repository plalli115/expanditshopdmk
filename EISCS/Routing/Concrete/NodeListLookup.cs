﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EISCS.Routing.Interfaces;

namespace EISCS.Routing.Concrete {
    public class NodeListLookup<T> : INodeListLookup<T> where T : class, IRouteDataNode {

        private readonly string _defaultLanguageGuid;

        public NodeListLookup(string defaultLanguageGuid) {
            _defaultLanguageGuid = defaultLanguageGuid;
        }

        /* General Search Rules */
        // Find a GroupGuid | GroupGuid & ProductGuid | ProductGuid | Route that matches. 
        // If more than one node matches the criteria select first or the first one where TypeCode != "$Auto"

        #region INodeListLookup<IRouteDataNode> Members

        //GET node for Group Link
        public T LookUpGroup(List<T> nodeList, string groupGuid, string languageGuid) {
            List<T> matchList = new List<T>();
            if (nodeList != null)
                foreach (var routeDataNode in nodeList.Where(
                    routeDataNode => routeDataNode.GroupGuid.ToString() == groupGuid &&
                        string.IsNullOrEmpty(routeDataNode.ProductGuid) && routeDataNode.LanguageGuid == languageGuid).Where(
                    routeDataNode => Compare(routeDataNode, ref matchList))) {
                    break;
                }
            return matchList.Count > 0 ? matchList[0] : null;
        }

        //GET node for GroupProduct Link
        public T LookUpGroupProduct(List<T> nodeList, string groupGuid, string productGuid, string languageGuid) {
            List<T> matchList = new List<T>();
            if (nodeList != null)
                foreach (var routeDataNode in nodeList.Where(
                    routeDataNode => routeDataNode.GroupGuid.ToString() == groupGuid &&
                        routeDataNode.ProductGuid == productGuid && routeDataNode.LanguageGuid == languageGuid).Where(
                    routeDataNode => Compare(routeDataNode, ref matchList))) {
                    break;
                }
            return matchList.Count > 0 ? matchList[0] : null;
        }

        //GET node for Product Link
        public T LookUpProduct(List<T> nodeList, int rootGuid, string productGuid, string languageGuid) {
            List<T> matchList = new List<T>();
            if (nodeList != null)
                foreach (var routeDataNode in
                    nodeList.Where(routeDataNode => routeDataNode.GroupGuid == rootGuid &&
                        routeDataNode.ProductGuid == productGuid && routeDataNode.LanguageGuid == languageGuid).Where(
                    routeDataNode => Compare(routeDataNode, ref matchList))) {
                    break;
                }
            return matchList.Count > 0 ? matchList[0] : LookUpProduct(nodeList, productGuid, languageGuid);
        }

        //GET node for Product Link
        public T LookUpProduct(List<T> nodeList, string productGuid, string languageGuid) {
            List<T> matchList = new List<T>();
            if (nodeList != null)
                foreach (var routeDataNode in
                    nodeList.Where(routeDataNode => routeDataNode.ProductGuid == productGuid &&
                        routeDataNode.LanguageGuid == languageGuid).Where(
                    routeDataNode => Compare(routeDataNode, ref matchList))) {
                    break;
                }
            return matchList.Count > 0 ? matchList[0] : null;
        }

        // Get node from path
        // Must handle possibility of multiple languages
        public T LookUpRoute(List<T> nodeList, string path, HttpContextBase httpContext) {
            List<T> matchList = new List<T>();
            if (nodeList != null) {
                if (LanguageCount(nodeList) > 1) {
                    nodeList = NarrowListByLanguage(nodeList, httpContext);
                }
                foreach (var routeDataNode in nodeList.Where(
                    rnode => rnode.RouteUrl.ToLower() == path.ToLower()).Where(rnode => Compare(rnode, ref matchList))) {
                    break;
                }
            }
            return matchList.Count > 0 ? matchList[0] : null;
        }

        #endregion

        // Narrow list to use either cookie language or default language if found
        public List<T> NarrowListByLanguage(List<T> nodeList, HttpContextBase httpContext) {
            IEnumerable<T> narrowedList = nodeList.Where(x => x.LanguageGuid == GetLanguageGuid(httpContext));
            // If neither cookie language or default language was found - return original list
            return narrowedList.Count() > 0 ? narrowedList.ToList() : nodeList;
        }

        // Find number of unique languages in the list
        public int LanguageCount(List<T> nodeList) {
            var q = from x in nodeList
                    group x by x.LanguageGuid
                        into g
                        let count = g.Count()
                        orderby count descending
                        select new { LanguageGuid = g.Key };
            return q.Count();
        }

        // Find language from cookie or return default language
        private string GetLanguageGuid(HttpContextBase httpContext) {
            try {
                HttpCookie cookie = httpContext.Request.Cookies["user"];
                if (cookie != null) {
                    if (cookie["LanguageGuid"] != null) {
                        if (!string.IsNullOrEmpty(cookie["LanguageGuid"])) {
                            return cookie["LanguageGuid"];
                        }
                    }
                }
            }
            catch (Exception) {
                return _defaultLanguageGuid;
            }
            return _defaultLanguageGuid;
        }

        private static bool Compare(T routeDataNode, ref List<T> matchList) {
            if (matchList.Count == 0) {
                matchList.Add(routeDataNode);
                if (!string.IsNullOrEmpty(routeDataNode.TypeCode) && routeDataNode.TypeCode != "$Auto") {
                    return true;
                }
            }
            else if (!string.IsNullOrEmpty(routeDataNode.TypeCode) && routeDataNode.TypeCode != "$Auto") {
                matchList[0] = routeDataNode;
                return true;
            }
            return false;
        }
    }
}
