﻿using CmsPublic.Attributes;
using EISCS.Routing.Interfaces;

namespace EISCS.Routing.Concrete
{
    public class RouteDataNode : IRouteDataNode
    {

        public RouteDataNode(IRouteDataNode node){
            GroupGuid = node.GroupGuid;
            LanguageGuid = node.LanguageGuid;
            TargetUrl = node.TargetUrl;
            ProductGuid = node.ProductGuid;
            RouteUrl = node.RouteUrl;
            TypeCode = node.TypeCode;
            Controller = node.Controller;
            ViewFile = node.ViewFile;
        }

        public RouteDataNode(){}

        #region IRouteDataNode Members

        public int GroupGuid { get; set; }
        public string LanguageGuid { get; set; }
        public string TargetUrl { get; set; }
        public string ProductGuid { get; set; }
        public string RouteUrl { get; set; }
        public string TypeCode { get; set; }
        [NoInsert]
        public string Controller { get; set; }
        [NoInsert]
        public string ViewFile { get; set; }

        #endregion
    }
}
