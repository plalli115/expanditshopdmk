﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace EISCS.Routing.Concrete.RoutePatternFormatting {
    public class IndexNode {
        public int StartIndex { get; set; }
        public string PlaceHolder { get; set; }
        public int PlaceHolderIndex { get; set; }
    }
    public class MissingNode {
        public int Index { get; set; }
        public string FormatItem { get; set; }
    }
    public class StringParser {
        
        /// <summary>
        /// Find all name format tokens
        /// </summary>
        /// <param name="str">The name format string to search</param>
        /// <returns>A list of name format tokens</returns>
        public List<string> FindAllFormats(string str) {
            const string expression = @"\{([^}]+)\}";
            Regex matches = new Regex(expression, RegexOptions.IgnoreCase);
            MatchCollection mc = matches.Matches(str);

            List<string> strings = new List<string>();

            for (int i = 0; i < mc.Count; i++) {
                strings.Add(mc[i].Value);
            }

            return strings;
        }
        
        /// <summary>
        /// Look for unsanctioned formats in the name format string ( fx {notInPlaceholder} ) and remove them
        /// </summary>
        /// <param name="str">The name format string to search</param>
        /// <param name="placeholders">The items to compare to</param>
        /// <returns>A cleaned name format string</returns>
        private string PrepareString(string str, IEnumerable<string> placeholders) {
            List<string> strings = FindAllFormats(str);

            List<string> gfList = placeholders.ToList();

            str = (from s in strings let st = s where !gfList.Exists(x => x == st) select s).Aggregate(str, (current, s) => current.Replace(s, ""));

            return str;

        }

        /// <summary>
        /// Replace name format strings ( {name1}{name2} ) with .net format strings ( {0}{1} )
        /// by the order they appear.
        /// If a format string is missing - compared to the placeholder - add it to the end of the
        /// resulting string.
        /// </summary>
        /// <param name="toSearch">The string to parse</param>
        /// <param name="placeholders">The items to compare to</param>
        /// <param name="missingNodes">Will hold the missing tokens</param>
        /// <returns>The parsed format string</returns>
        public string ReplaceByPlaceholders(string toSearch, string[] placeholders, List<MissingNode> missingNodes) {
            List<IndexNode> beginnings = new List<IndexNode>(placeholders.Length);
            return ReplaceByPlaceholders(toSearch, placeholders, missingNodes, beginnings);
        }

        /// <summary>
        /// Replace name format strings ( {name1}{name2} ) with .net format strings ( {0}{1} )
        /// by the order they appear.
        /// If a format string is missing - compared to the placeholder - add it to the end of the
        /// resulting string.
        /// </summary>
        /// <param name="toSearch">The string to parse</param>
        /// <param name="placeholders">The items to compare to</param>
        /// <param name="missingNodes">Will hold the missing tokens</param>
        /// <param name="beginnings">Will hold the found tokens</param>
        /// <returns>The parsed format string</returns>
        public string ReplaceByPlaceholders(string toSearch, string[] placeholders, List<MissingNode> missingNodes, List<IndexNode> beginnings) {
            toSearch = PrepareString(toSearch, placeholders);
            int found = 0;
            int placeholderLength = placeholders.Length;
            for (int i = 0; i < placeholderLength; i++) {
                if (toSearch.IndexOf(placeholders[i]) < 0) {
                    missingNodes.Add(new MissingNode { FormatItem = placeholders[i], Index = i });
                    continue;
                }
                IndexNode indexNode = new IndexNode {
                    StartIndex = toSearch.IndexOf(placeholders[i]),
                    PlaceHolder = placeholders[i],
                    PlaceHolderIndex = i
                };
                beginnings.Add(indexNode);
                found++;
            }
            string toReturn = "";
            for (int i = 0; i < beginnings.Count; i++) {
                toReturn = toSearch.Replace(beginnings[i].PlaceHolder, "{" + beginnings[i].PlaceHolderIndex + "}");
                toSearch = toReturn;
            }
            if (found < placeholderLength) {
                int missing = placeholderLength - found;
                int highValue = placeholderLength - missing;
                for (int i = highValue; i < placeholderLength; i++) {
                    toReturn += "{" + i + "}";
                }
            }
            return toReturn;
        }
    }
}
