﻿namespace EISCS.Routing.Concrete.RoutePatternFormatting {
    /// <summary>
    /// Holds format strings
    /// </summary>
    public class RouteUrlPatternsInfo {
        public string GroupPattern { get; set; }
        public string GroupProductPattern { get; set; }
        public string ProductPattern { get; set; }
    }
}
