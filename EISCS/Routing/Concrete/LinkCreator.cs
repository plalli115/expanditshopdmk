﻿using System;
using CmsPublic.ModelLayer.Models;
using EISCS.ExpandITFramework.Util;
using EISCS.Routing.Interfaces;

namespace EISCS.Routing.Concrete {
    public class LinkCreator {

        private readonly IRouteDataProvider<RouteDataNode> _provider;
        private readonly IHttpRuntimeWrapper _httpRuntime;

        public LinkCreator(IRouteDataProvider<RouteDataNode> routeDataProvider, IHttpRuntimeWrapper httpRuntime) {
            _provider = routeDataProvider;
            _httpRuntime = httpRuntime;
        }

        public IHttpRuntimeWrapper HttpRuntime {
            get { return _httpRuntime; }
        }

        public string CreateTemplateGroupLink(int groupGuid, string languageGuid) {
            RouteDataNode node = _provider.FillByGroupGuid(groupGuid, languageGuid);
            return node == null ? null : VirtualRoot() + "/" + node.RouteUrl;
        }

        public string CreateTemplateGroupLink(Group grp, string languageGuid) {
            return CreateTemplateGroupLink(grp.ID, languageGuid);
        }

        public string CreateTemplateProductLink(string productGuid, string languageGuid) {
            RouteDataNode dataNode = _provider.FillByProductGuid(productGuid, languageGuid);
            return dataNode != null ? VirtualRoot() + "/" + dataNode.RouteUrl : null;
        }

        public string CreateTemplateProductLink(object productguid, object groupguid, string languageGuid) {
            int iGroupGuid;
            try {
                iGroupGuid = (int)groupguid;
            }
            catch (InvalidCastException){
                var sGroupGuid = (string)groupguid;
                int.TryParse(sGroupGuid.Trim(), out iGroupGuid);
            }

            string sProductGuid = ((string)productguid).Trim();

            RouteDataNode node = _provider.FillByGroupProductGuid(iGroupGuid, sProductGuid, languageGuid);
            return node == null ? CreateTemplateProductLink(sProductGuid, languageGuid) : VirtualRoot() + "/" + node.RouteUrl;
        }

        public string CreateTemplateGroupProductLink(object productguid, object groupguid, string languageGuid) {
            int iGroupGuid;
            try {
                iGroupGuid = (int)groupguid;
            }
            catch (InvalidCastException){
                var sGroupGuid = (string)groupguid;
                int.TryParse(sGroupGuid.Trim(), out iGroupGuid);
            }

            string sProductGuid = ((string)productguid).Trim();

            if (iGroupGuid == 0) {
                iGroupGuid = _provider.GetGroupGuidByProductGuid(sProductGuid, languageGuid);
            }

            RouteDataNode node = _provider.FillByGroupProductGuid(iGroupGuid, sProductGuid, languageGuid);
            return node == null ? CreateTemplateProductLink(sProductGuid, languageGuid) : VirtualRoot() + "/" + node.RouteUrl;
        }


        private string VirtualRoot() {
            return _httpRuntime.AppDomainAppVirtualPath.Equals("/")
                                                 ? ""
                                                 : _httpRuntime.AppDomainAppVirtualPath;
        }

    }
}
