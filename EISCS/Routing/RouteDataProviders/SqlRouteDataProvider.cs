﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using CmsPublic.DataRepository;
using CmsPublic.ModelLayer.Models;
using EISCS.Routing.Interfaces;
using StackExchange.Profiling;

namespace EISCS.Routing.RouteDataProviders
{
    public class SqlRouteDataProvider<T> : BaseDataService,  IRouteDataProvider<T> where T : class, IRouteDataNode
    {
        private const string TableName = "RouteTable";
        private INodeListLookup<T> LookUp { get; set; }
        private IRouteCreationModule<T> RouteRecreator { get; set; }

        public SqlRouteDataProvider(INodeListLookup<T> nodeListLookup, IRouteCreationModule<T> recreator, IExpanditDbFactory expanditDbFactory)
            : base(expanditDbFactory)
        {
            LookUp = nodeListLookup;
            RouteRecreator = recreator;
        }

        #region Lookup

        public T FillByGroupGuid(int groupGuid, string languageGuid)
        {
            string sql =
                string.Format(
                    @"  SELECT  {0}.*, TemplateController AS Controller, TemplateFileName AS ViewFile 
                        FROM    {0} 
                                JOIN TemplateTable ON {0}.TargetUrl = TemplateTable.TemplateGuid
                        WHERE   GroupGuid = {1} AND ProductGuid IS NULL", TableName, groupGuid);
            List<T> result = GetResults<T>(sql).ToList();
            return LookUp.LookUpGroup(result, groupGuid.ToString(), languageGuid);
        }

        public T FillByGroupProductGuid(int groupGuid, string productGuid, string languageGuid)
        {
            string sql =
                string.Format(
                    @"  SELECT  {0}.*, TemplateController AS Controller, TemplateFileName AS ViewFile 
                        FROM    {0} 
                                JOIN TemplateTable ON {0}.TargetUrl = TemplateTable.TemplateGuid
                    WHERE GroupGuid = {1} AND ProductGuid = N'{2}'", TableName, groupGuid, productGuid.Replace("'", "''"));
            
            List<T> retList = GetResults<T>(sql).ToList();
            return LookUp.LookUpGroupProduct(retList, groupGuid.ToString(), productGuid, languageGuid);
        }

        public T FillByProductGuid(string productGuid, string languageGuid)
        {
            string sql =
                string.Format(
                @"  SELECT  {0}.*, TemplateController AS Controller, TemplateFileName AS ViewFile 
                    FROM    {0} 
                            JOIN TemplateTable ON {0}.TargetUrl = TemplateTable.TemplateGuid 
                    WHERE   ProductGuid = N'{1}'", TableName, productGuid.Replace("'", "''"));
            List<T> retList = GetResults<T>(sql).ToList();
            return LookUp.LookUpProduct(retList, 0, productGuid, languageGuid);
        }

        public T FillByRoute(string route, HttpContextBase context)
        {

            string sql =
                string.Format(
                    @"  SELECT  {0}.*, TemplateController AS Controller, TemplateFileName AS ViewFile 
                        FROM    {0} 
                                JOIN TemplateTable ON {0}.TargetUrl = TemplateTable.TemplateGuid 
                        WHERE   RouteUrl = N'{1}' ORDER BY LanguageGuid", TableName, route.Replace("'", "''").ToLower());
            List<T> retList = GetResults<T>(sql).ToList();

            if (!retList.Any())
            {
                route = FixEncoding(route);
                sql =
                    string.Format(
                        @"  SELECT  {0}.*, TemplateController AS Controller, TemplateFileName AS ViewFile 
                            FROM    {0} 
                                    JOIN TemplateTable ON {0}.TargetUrl = TemplateTable.TemplateGuid 
                            WHERE   RouteUrl = N'{1}' ORDER BY LanguageGuid", TableName, route.Replace("'", "''").ToLower());
                retList = GetResults<T>(sql).ToList();
            }

            return LookUp.LookUpRoute(retList, route, context);
        }

        public T FillByControllerName(string controllerName)
        {
            string sql =
                string.Format(@"SELECT  TOP 1 * 
                                FROM    {0} 
                                WHERE Controller = N'{1}'", TableName, controllerName.Replace("'", "''"));
            return GetResults<T>(sql).FirstOrDefault();
        }

        public int GetGroupGuidByProductGuid(string productGuid, string languageGuid)
        {
            string sql =
                string.Format(@"SELECT  TOP 1 GroupGuid 
                                FROM    {0} 
                                WHERE GroupGuid <> 0 AND ProductGuid = N'{1}'", TableName, productGuid.Replace("'", "''"));
            return GetSingle<int>(sql);
        }

        #endregion

        private string FixEncoding(string route)
        {
            byte[] utf8Bytes = Encoding.Default.GetBytes(route);
            string hex = BitConverter.ToString(utf8Bytes);
            hex = "%" + hex.Replace("-", "%");
            return HttpUtility.UrlDecode(hex);
        }

        #region CRUD

        public void Add(T t)
        {
            if (t != null)
            {
                string sqlQuery = string.Format("INSERT INTO {0} {1}", TableName, GenerateInsertParameters(t, false));
                Execute(sqlQuery, t);
            }
        }

        public int AddRange(ICollection<T> t)
        {
            if (t == null) return 0;

            foreach (var item in t)
            {
                Add(item);
            }
            return 1;
        }

        public int AddRangeFlushable(ICollection<T> t){
            return AddRange(t);
        }

        public int RecreateAll()
        {
            var profiler = MiniProfiler.Current;
            using (profiler.Step("Recreate Routes"))
            {
                bool recordsAffected = true;
                while (recordsAffected)
                {
                    recordsAffected = Execute(
                    string.Format(
                        @"DELETE TOP (10000)
                        FROM {0} WHERE TypeCode = '{1}' OR TypeCode = '{2}'", TableName, "$Auto", "$Alias"));
                }
            }
            IList<T> routeNodes = RouteRecreator.PrepareCreation(this);
            int result = 0;
            if (routeNodes != null)
            {
                result = AddRange(routeNodes);
            }
            return result;
        }

        public List<string> GetInternalLinkUrls(string languageGuid)
        {
            var sql = @"
                        SELECT  RouteUrl 
                        FROM    RouteTable
                        WHERE   ProductGuid IS NULL 
                        AND     LanguageGuid = '" + languageGuid + @"'
                        ORDER BY RouteUrl;";
            return GetResults<string>(sql).ToList();
        }

        public int Count(){
            var sql = @"SELECT COUNT(*) FROM RouteTable";
            return GetSingle<int>(sql);
        }

        public void RemoveByGroupGuid(ICollection<int> guids)
        {
            if (guids == null || guids.Count <= 0) return;
            StringBuilder builder = new StringBuilder(guids.Count);
            foreach (int guid in guids)
            {
                builder.Append(string.Format("DELETE FROM {0} WHERE GroupGuid = {1} ", TableName, guid));
            }

            Execute(builder.ToString());
        }

        public void RemoveByGroupGuidProductGuid(ICollection<GroupProduct> t)
        {
            StringBuilder builder = new StringBuilder();

            foreach (GroupProduct node in t)
            {
                builder.Append(string.Format("DELETE FROM {0} WHERE GroupGuid = {1} AND ProductGuid = '{2}'", TableName, node.GroupGuid, node.ProductGuid));
            }
            Execute(builder.ToString());
        }

        public void RemoveByProductGuid(ICollection<string> guids)
        {
            if (guids == null || guids.Count <= 0) return;
            StringBuilder builder = new StringBuilder(guids.Count);
            foreach (string guid in guids)
            {
                builder.Append(string.Format("DELETE FROM {0} WHERE ProductGuid = '{1}' ", TableName, guid));
            }
            Execute(builder.ToString());
        }

        public void RemoveByProductGuid(ICollection<string> guids, string languageGuid)
        {
            if (guids == null || guids.Count <= 0) return;
            StringBuilder builder = new StringBuilder(guids.Count);
            foreach (string guid in guids)
            {
                builder.Append(string.Format("DELETE FROM {0} WHERE ProductGuid = '{1}' AND LanguageGuid = '{2}' ", TableName, guid, languageGuid));
            }
            Execute(builder.ToString());
        }

        #endregion
    }
}