﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using CmsPublic.ModelLayer.Models;
using EISCS.Routing.Concrete;
using EISCS.Routing.Interfaces;
using EISCS.Routing.Synchronization;
using StackExchange.Profiling;

namespace EISCS.Routing.RouteDataProviders
{
    public class ListRouteDataProvider<T> : IRouteDataProvider<T> where T : class, IRouteDataNode
    {
        private readonly List<T> _myList;
        private readonly List<T> _routeCreationList; 
        private readonly Dictionary<string, Dictionary<int, T>> _fastGroupLookup;
        private readonly ReaderWriterLockSlim _sync = new ReaderWriterLockSlim(LockRecursionPolicy.SupportsRecursion);
        private const int TimeoutMilliseconds = 5000;
        private ILookup<string, T> _myLookup;

        public ListRouteDataProvider(List<T> nodeList, INodeListLookup<T> nodeListLookup, IRouteCreationModule<T> recreator, Dictionary<string, Dictionary<int, T>> fastGroupLookup)
        {
            _myList = nodeList;
            _routeCreationList = new List<T>();
            LookUp = nodeListLookup;
            RouteRecreator = recreator;
            _fastGroupLookup = fastGroupLookup;
        }

        private IRouteCreationModule<T> RouteRecreator { get; set; }
        private INodeListLookup<T> LookUp { get; set; }

        public int RecreateAll()
        {
            _routeCreationList.Clear();
            IList<T> routeNodes = RouteRecreator.PrepareCreation(this);
            if (routeNodes != null)
            {
                AddRangeFlushable(routeNodes);
            }

            int count = 0;
            _sync.Write(() =>
                count = RecreateAllWork(), TimeoutMilliseconds
                );
            return count;
        }

        private int RecreateAllWork(){
            _myList.Clear();

            foreach (var routeNode in _routeCreationList)
            {
                _myList.Add(new RouteDataNode(routeNode) as T);
            }
            
            _myLookup = _routeCreationList.ToLookup(x => x.RouteUrl);
            _routeCreationList.Clear();

            return _myList.Count;
        }
        
        public List<string> GetInternalLinkUrls(string languageGuid)
        {
            var strings = new List<string>();
            _sync.Read(() =>
                strings = GetInternalLinkUrlsWork(languageGuid), TimeoutMilliseconds, true);
            return strings;
        }

        private List<string> GetInternalLinkUrlsWork(string languageGuid)
        {
            var query = from s in _myList
                        where (s.LanguageGuid == languageGuid && String.IsNullOrWhiteSpace(s.ProductGuid))
                        select s.RouteUrl;
            return query.ToList();
        }

        public int Count()
        {
            int count = 0;
            _sync.Read(() => count = _myList.Count, TimeoutMilliseconds);
            return count;
        }

        public T FillByGroupGuid(int groupGuid, string languageGuid){
            T t = null;
            _sync.Read
                (
                    () => t = FillByGroupGuidWork(groupGuid, languageGuid), TimeoutMilliseconds
                );
            return t;
        }

        private T FillByGroupGuidWork(int groupGuid, string languageGuid)
        {
            if (!string.IsNullOrWhiteSpace(languageGuid) && _fastGroupLookup.ContainsKey(languageGuid))
            {
                var tmp = _fastGroupLookup[languageGuid];
                if (tmp.ContainsKey(groupGuid))
                {
                    var retval = tmp[groupGuid];
                    return retval;
                }
            }

            // Fallback if property language is null etc.
            List<T> result = _myList.Where(x => x.GroupGuid == groupGuid).ToList();
            return LookUp.LookUpGroup(result, groupGuid.ToString(), languageGuid);
        }

        public T FillByGroupProductGuid(int groupGuid, string productGuid, string languageGuid)
        {
            T t = null;
            _sync.Read
                (
                    () => t = FillByGroupProductGuidWork(groupGuid, productGuid, languageGuid), TimeoutMilliseconds
                );
            return t;
        }

        private T FillByGroupProductGuidWork(int groupGuid, string productGuid, string languageGuid)
        {
            List<T> retList = _myList.Where(x => x.GroupGuid == groupGuid && x.ProductGuid == productGuid).ToList();
            return LookUp.LookUpGroupProduct(retList, groupGuid.ToString(), productGuid, languageGuid);
        }

        public T FillByProductGuid(string productGuid, string languageGuid)
        {
            T t = null;
            _sync.Read
                (
                    () => t = FillByProductGuidWork(productGuid, languageGuid), TimeoutMilliseconds
                );
            return t;
        }

        private T FillByProductGuidWork(string productGuid, string languageGuid)
        {
            List<T> retList = _myList.Where(x => x.ProductGuid == productGuid).ToList();
            return LookUp.LookUpProduct(retList, 0, productGuid, languageGuid);
        }

        public T FillByRoute(string route, HttpContextBase context)
        {
            var profiler = MiniProfiler.Current;
            using (profiler.Step("Fill by Route"))
            {
                T t = null;
                _sync.Read(() => t = FillByRouteWork(route, context), TimeoutMilliseconds, true);
                return t;
            }
        }

        private T FillByRouteWork(string route, HttpContextBase context)
        {
            List<T> myList = _myLookup[route].ToList();
            return LookUp.LookUpRoute(myList, route, context);
        }

        public T FillByControllerName(string controllerName)
        {
            T t = null;
            _sync.Read
                (
                    () => t = FillByControllerNameWork(controllerName), TimeoutMilliseconds
                );
            return t;
        }

        private T FillByControllerNameWork(string controllerName)
        {
            IEnumerable<T> myList = _myList.Where(x => x.Controller.ToLower() == controllerName.ToLower());
            return myList.FirstOrDefault();
        }

        public int GetGroupGuidByProductGuid(string productGuid, string languageGuid)
        {
            throw new NotImplementedException("GetGroupGuidByProductGuid is not implemented in the ListRouteDataProvider");
        }

        public int NrOfItems(){
            int count = 0;
            _sync.Read
                (
                    () => count = _myList.Count, TimeoutMilliseconds
                );
            return count;
        }
        
        #region Add and Remove

        public void Add(T t)
        {
            if (t != null)
            {
                _sync.Write(
                    () =>
                    AddWork(t), TimeoutMilliseconds, true
                );
            }
        }

        private void AddWork(T t)
        {
            _myList.Add(t);
            _myLookup = _myList.ToLookup(x => x.RouteUrl);
        }

        public int AddRange(ICollection<T> t)
        {
            int result = -1;
            _sync.Write(
                () =>
                    result = AddRangeWork(t), TimeoutMilliseconds, true
                );
            return result;
        }

        public int AddRangeFlushable(ICollection<T> t){
            int result = -1;
            if (t != null)
            {
                result = t.Count;
               _routeCreationList.AddRange(t);
            }
            return result;
        }

        private int AddRangeWork(ICollection<T> t)
        {
            int result = -1;
            if (t != null)
            {
                result = t.Count;
                _myList.AddRange(t);
                _myLookup = _myList.ToLookup(x => x.RouteUrl);
            }
            return result;
        }

        public void RemoveByGroupGuid(ICollection<int> guids)
        {
            if (guids == null || guids.Count <= 0)
            {
                return;
            }
            _sync.Write(
                () =>
                    RemoveByGroupGuidWork(guids), TimeoutMilliseconds, true
                );
        }

        private void RemoveByGroupGuidWork(ICollection<int> guids)
        {
            if (guids == null || guids.Count <= 0)
            {
                return;
            }
            foreach (T t in
                guids.Select(temp => _myList.FindAll(x => x.GroupGuid == temp)).SelectMany(removeList => removeList))
            {
                _myList.Remove(t);
            }
            foreach (int guid in guids)
            {
                foreach (KeyValuePair<string, Dictionary<int, T>> pair in _fastGroupLookup)
                {
                    if (_fastGroupLookup[pair.Key].ContainsKey(guid))
                    {
                        _fastGroupLookup[pair.Key].Remove(guid);
                    }
                }
            }
            _myLookup = _myList.ToLookup(x => x.RouteUrl);
        }

        public void RemoveByProductGuid(ICollection<string> guids)
        {
            if (guids == null || guids.Count <= 0)
            {
                return;
            }
            _sync.Write(
                () =>
                    RemoveByProductGuidWork(guids), TimeoutMilliseconds, true
                );
        }

        private void RemoveByProductGuidWork(ICollection<string> guids)
        {
            if (guids == null || guids.Count <= 0)
            {
                return;
            }
            foreach (T t in
                guids.Select(temp => _myList.FindAll(x => x.ProductGuid == temp)).SelectMany(removeList => removeList))
            {
                _myList.Remove(t);
            }
            _myLookup = _myList.ToLookup(x => x.RouteUrl);
        }

        public void RemoveByProductGuid(ICollection<string> guids, string languageGuid)
        {
            if (guids == null || guids.Count <= 0)
            {
                return;
            }
            _sync.Write(
                () =>
                    RemoveByProductGuidWork(guids, languageGuid), TimeoutMilliseconds, true
                );
        }

        private void RemoveByProductGuidWork(ICollection<string> guids, string languageGuid)
        {
            if (guids == null || guids.Count <= 0)
            {
                return;
            }
            foreach (T t in
                guids.Select(temp => _myList.FindAll(x => x.ProductGuid == temp && x.LanguageGuid == languageGuid)).SelectMany(removeList => removeList))
            {
                _myList.Remove(t);
            }
            _myLookup = _myList.ToLookup(x => x.RouteUrl);
        }

        public void RemoveByGroupGuidProductGuid(ICollection<GroupProduct> t)
        {
            _sync.Write(
                () =>
                    RemoveByGroupGuidProductGuidWork(t), TimeoutMilliseconds, true
                );
        }

        private void RemoveByGroupGuidProductGuidWork(ICollection<GroupProduct> t)
        {
            foreach (T tr in
                t.Select(temp => _myList.FindAll(x => x.GroupGuid == temp.GroupGuid && x.ProductGuid == temp.ProductGuid)).SelectMany(removeList => removeList))
            {
                _myList.Remove(tr);
            }
            _myLookup = _myList.ToLookup(x => x.RouteUrl);
        }

        #endregion
    }
}