﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using CmsPublic.DataRepository;
using CmsPublic.ModelLayer.Models;
using EISCS.ExpandITFramework.Infrastructure;
using EISCS.Routing.Interfaces;
using EISCS.Shop.DO.Interface;
using EISCS.Shop.DO.Repository;

namespace EISCS.Routing.RouteDataProviders
{
    public class SqlCachedRouteDataProvider<T> : BaseRepository<T>,  IRouteDataProvider<T> where T : class, IRouteDataNode
    {

        private const string TableName = "RouteTable";
        private const string TemplateTableName = "TemplateTable";
        private INodeListLookup<T> LookUp { get; set; }
        private IRouteCreationModule<T> RouteRecreator { get; set; }
        private List<T> _routes;
        private List<TemplateQuerySet> _templates;
        private const string CacheKey = "CachedRouteTable";

        public SqlCachedRouteDataProvider(INodeListLookup<T> nodeListLookup, IRouteCreationModule<T> recreator, IExpanditDbFactory expanditDbFactory) : base(expanditDbFactory)
        {
            LookUp = nodeListLookup;
            RouteRecreator = recreator;
            Read();
        }

        private void CheckCache()
        {
            if (CacheManager.CacheGet(CacheKey) == null)
            {
                Read();
            }
        }

        private void Read()
        {
            string sql =
                string.Format("SELECT * FROM {0} ", TableName);
            _routes = GetResults<T>(sql).ToList();

            sql =
                string.Format("SELECT * FROM {0} ", TemplateTableName);
            _templates = GetResults<TemplateQuerySet>(sql).ToList();

            CacheManager.CacheSetAggregated(CacheKey, new object(), new[] { TableName, TemplateTableName });
        }

        #region Lookup

        public T FillByGroupGuid(int groupGuid, string languageGuid)
        {
            CheckCache();
            List<T> result = _routes.Where(x => x.GroupGuid == groupGuid).ToList();
            return result.Count > 0 ?
                AddTemplateData(LookUp.LookUpGroup(result, groupGuid.ToString(), languageGuid)) : null;
        }

        public T FillByGroupProductGuid(int groupGuid, string productGuid, string languageGuid)
        {
            CheckCache();
            List<T> retList =
                _routes.Where(x => x.GroupGuid == groupGuid && x.ProductGuid == productGuid).ToList();

            return retList.Count > 0 ?
                AddTemplateData(LookUp.LookUpGroupProduct(retList, groupGuid.ToString(), productGuid, languageGuid)) : null;
        }

        public T FillByProductGuid(string productGuid, string languageGuid)
        {
            CheckCache();
            List<T> retList =
                _routes.Where(x => x.ProductGuid == productGuid).ToList();

            return retList.Count > 0 ?
                AddTemplateData(LookUp.LookUpProduct(retList, 0, productGuid, languageGuid)) : null;
        }

        public T FillByRoute(string route, HttpContextBase context)
        {
            CheckCache();
            List<T> retList = _routes.Where(x => x.RouteUrl.ToLower() == route.ToLower()).ToList();
            return retList.Count > 0 ?
                AddTemplateData(LookUp.LookUpRoute(retList, route, context)) : null;
        }

        public T FillByControllerName(string controllerName)
        {
            throw new System.NotImplementedException();
        }

        public int GetGroupGuidByProductGuid(string productGuid, string languageGuid)
        {
            CheckCache();
            var firstOrDefault = _routes.FirstOrDefault(x => x.GroupGuid != 0 && x.ProductGuid == productGuid);

            int groupGuid = 0;
            if (firstOrDefault != null)
            {
                groupGuid = firstOrDefault.GroupGuid;
            }
            return groupGuid;
        }

        #endregion

        private T AddTemplateData(T node)
        {
            if (node != null)
            {
                string id = node.TargetUrl;
                var templateInfo = _templates.FirstOrDefault(t => t.TemplateGuid == id);
                if (templateInfo != null)
                {
                    node.Controller = templateInfo.TemplateController;
                    node.ViewFile = templateInfo.TemplateFileName;
                }
            }
            return node;
        }

        #region CRUD

        public void Add(T t)
        {
            if (t != null)
            {
                string sqlQuery = string.Format("INSERT INTO {0} {1}", GetTableName(), GenerateInsertParameters(t, false));
                Execute(sqlQuery);
            }
        }

        public int AddRange(ICollection<T> t)
        {
            if (t == null) return 0;
            foreach (var item in t)
            {
                Add(item);
            }
            return 1;
        }

        public int AddRangeFlushable(ICollection<T> t){
            return AddRange(t);
        }

        public int RecreateAll()
        {
            Execute(
                string.Format("DELETE FROM {0} WHERE TypeCode = '{1}' OR TypeCode = '{2}'", TableName, "$Auto", "$Alias"));
            IList<T> routeNodes = RouteRecreator.PrepareCreation(this);
            int result = 0;
            if (routeNodes != null)
            {
                result = AddRange(routeNodes);
            }
            return result;
        }

        public List<string> GetInternalLinkUrls(string languageGuid)
        {
            var sql = @"
                        SELECT RouteUrl FROM RouteTable
                        WHERE ProductGuid IS NULL 
                        AND LanguageGuid = '" + languageGuid + @"'
                        ORDER BY RouteUrl;";
            return GetResults<string>(sql).ToList();
        }

        public int Count(){
            var sql = @"SELECT COUNT(*) FROM RouteTable";
            return GetSingle<int>(sql);
        }

        public void RemoveByGroupGuid(ICollection<int> guids)
        {
            if (guids == null || guids.Count <= 0) return;
            StringBuilder builder = new StringBuilder(guids.Count);
            foreach (int guid in guids)
            {
                builder.Append(string.Format("DELETE FROM {0} WHERE GroupGuid = {1} ", TableName, guid));
            }

            Execute(builder.ToString());
        }

        public void RemoveByGroupGuidProductGuid(ICollection<GroupProduct> t)
        {
            StringBuilder builder = new StringBuilder();

            foreach (GroupProduct node in t)
            {
                builder.Append(string.Format("DELETE FROM {0} WHERE GroupGuid = {1} AND ProductGuid = '{2}'", TableName, node.GroupGuid, node.ProductGuid));
            }
            Execute(builder.ToString());
        }

        public void RemoveByProductGuid(ICollection<string> guids)
        {
            if (guids == null || guids.Count <= 0) return;
            StringBuilder builder = new StringBuilder(guids.Count);
            foreach (string guid in guids)
            {
                builder.Append(string.Format("DELETE FROM {0} WHERE ProductGuid = '{1}' ", TableName, guid));
            }
            Execute(builder.ToString());
        }

        public void RemoveByProductGuid(ICollection<string> guids, string languageGuid)
        {
            if (guids == null || guids.Count <= 0) return;
            StringBuilder builder = new StringBuilder(guids.Count);
            foreach (string guid in guids)
            {
                builder.Append(string.Format("DELETE FROM {0} WHERE ProductGuid = '{1}' AND LanguageGuid = '{2}' ", TableName, guid, languageGuid));
            }
            Execute(builder.ToString());
        }

        #endregion
    }
}
