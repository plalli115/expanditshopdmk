﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using EISCS.Routing.Providers;

namespace EISCS.Routing
{
    public class ExpanditMvcIHttpHandler : IRouteHandler
    {
        public IHttpHandler GetHttpHandler(RequestContext requestContext)
        {
            string controllerName = RouteManager.Route.GetControllerName(requestContext);

            return controllerName != null ? new ExpanditMvcRouteHandler(requestContext, controllerName) : new ExpanditRouteHandler().GetHttpHandler(requestContext);
        }
    }

    public class ExpanditMvcRouteHandler : MvcHandler
    {
        private readonly string _controllerName;

        public ExpanditMvcRouteHandler(RequestContext requestContext)
            : base(requestContext)
        {
        }

        public ExpanditMvcRouteHandler(RequestContext requestContext, string controllerName)
            : base(requestContext)
        {
            _controllerName = controllerName;
        }

        protected override void ProcessRequest(HttpContextBase httpContext)
        {
            if (RequestContext == null)
                throw new InvalidOperationException("No RequestContext");

            IControllerFactory controllerFactory = ControllerBuilder.Current.GetControllerFactory();
            IController controller = controllerFactory.CreateController(RequestContext, _controllerName);

            controller.Execute(RequestContext);
        }

    }

}
