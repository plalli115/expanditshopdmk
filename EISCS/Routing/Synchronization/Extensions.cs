﻿using System;
using System.Threading;

namespace EISCS.Routing.Synchronization
{
    public static class Extensions
    {
        /// <summary>
        /// ReaderWriterLockSlim extension for synchronizing read access.
        /// </summary>
        /// <param name="target">ReaderWriterLockSlim to execute on.</param>
        /// <param name="closure">Action to execute once a lock is acquired.</param>
        /// <param name="millisecondsTimeout">Indicates if and for how long a timeout is used to acquire a lock.</param>
        /// <param name="throwsOnTimeout">If this parameter is true,
        /// then if a timeout value is reached, an exception is thrown.</param>
        /// <returns>Returns false if a timeout is reached.</returns>
        public static bool Read(this ReaderWriterLockSlim target,
          Action closure, int? millisecondsTimeout = null, bool throwsOnTimeout = false)
        {
            bool lockHeld = false;
            try
            {
                if (millisecondsTimeout == null)
                    target.EnterReadLock();
                else if (!target.TryEnterReadLock(millisecondsTimeout.Value))
                {
                    if (throwsOnTimeout)
                    {
                        throw new TimeoutException(
                            "Could not gain a read lock within the timeout specified. " +
                            "(millisecondsTimeout=" + millisecondsTimeout.Value + ") ");
                    }

                    return false;
                }
                lockHeld = true;
                closure();
            }
            finally
            {
                if (lockHeld)
                    target.ExitReadLock();
            }

            return lockHeld;
        }

        /// <summary>
        /// ReaderWriterLockSlim extension for synchronizing write access.
        /// </summary>
        /// <param name="target">ReaderWriterLockSlim to execute on.</param>
        /// <param name="closure">Action to execute once a lock is acquired.</param>
        /// <param name="millisecondsTimeout">Indicates if and for how long a timeout is used to acquire a lock.</param>
        /// <param name="throwsOnTimeout">If this parameter is true,
        /// then if a timeout value is reached, an exception is thrown.</param>
        /// <returns>Returns false if a timeout is reached.</returns>
        public static bool Write(this ReaderWriterLockSlim target,
          Action closure, int? millisecondsTimeout = null, bool throwsOnTimeout = false)
        {
            bool lockHeld = false;
            try
            {
                if (millisecondsTimeout == null)
                    target.EnterWriteLock();
                else if (!target.TryEnterWriteLock(millisecondsTimeout.Value))
                {
                    if (throwsOnTimeout)
                    {
                        throw new TimeoutException(
                            "Could not gain a read lock within the timeout specified. " +
                            "(millisecondsTimeout=" + millisecondsTimeout.Value + ") ");
                    }

                    return false;
                }
                lockHeld = true;
                closure();
            }
            finally
            {
                if (lockHeld)
                    target.ExitWriteLock();
            }

            return lockHeld;
        }
    }
}
