﻿using System.Web;
using System.Web.Compilation;
using System.Web.Routing;
using System.Web.SessionState;
using System.Web.UI;
using EISCS.Routing.Interfaces;
using EISCS.Routing.Providers;

namespace EISCS.Routing {
    public class ExpanditRouteHandler : RouteHandlerBase, IRouteHandler, IRequiresSessionState {

        public ExpanditRouteHandler()
            : base(true) {
        }

        public IHttpHandler GetHttpHandler(RequestContext requestContext) {

            VirtualPath = RouteManager.Route.GetVirtualGroupProductPath(requestContext);

            IHttpHandler page = (IHttpHandler)BuildManager.CreateInstanceFromVirtualPath(VirtualPath, typeof(Page));

            if (page != null) {
                IRoutablePage routePage = page as IRoutablePage;
                if (routePage != null) {
                    routePage.RouteContext = requestContext;
                }
            }
            return page;
        }
    }
}