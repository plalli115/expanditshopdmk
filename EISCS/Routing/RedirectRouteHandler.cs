﻿using System.Web;
using System.Web.Routing;

namespace EISCS.Routing
{
    public class RedirectRouteHandler : IRouteHandler
    {
        private readonly string _newUrl;

        public RedirectRouteHandler(string newUrl)
        {
            _newUrl = newUrl;
        }

        public IHttpHandler GetHttpHandler(RequestContext requestContext)
        {
            return new RedirectHandler(_newUrl);
        }
    }

    public class RedirectHandler : IHttpHandler
    {
        private readonly string _newUrl;

        public RedirectHandler(string newUrl)
        {
            _newUrl = newUrl;
        }

        public bool IsReusable
        {
            get { return true; }
        }

        public void ProcessRequest(HttpContext httpContext)
        {
            httpContext.Response.Status = "301 Moved Permanently";
            httpContext.Response.StatusCode = 301;
            httpContext.Response.AppendHeader("Location", _newUrl);
        }
    }
}
