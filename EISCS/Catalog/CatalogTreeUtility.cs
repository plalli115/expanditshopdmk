﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.Mvc;
using CmsPublic.ModelLayer.Models;
using EISCS.Shop.DO.Interface;

namespace EISCS.Catalog
{
    ///<summary>
    /// Help building Group Tree Structures
    ///</summary>
    public class CatalogTreeUtility
    {
        private readonly IGroupRepository _gr;

        ///<summary>
        /// CTOR
        ///</summary>
        ///<param name="groupRepository"></param>
        public CatalogTreeUtility(IGroupRepository groupRepository)
        {
            _gr = groupRepository ?? DependencyResolver.Current.GetService<IGroupRepository>();
        }

        /// <summary>
        /// Get single parent trail to root from current Group
        /// </summary>
        /// <param name="grp"></param>
        /// <param name="languageGuid"></param>
        /// <param name="topGroup"></param>
        public Group TrickleUp(Group grp, string languageGuid, Group topGroup)
        {
            if (grp == null || grp.ParentGuid == 0) return topGroup;
            Group parent = _gr.GetGroup(grp.ParentGuid, languageGuid);
            topGroup = parent;
            parent.Children.Add(grp);
            grp.Parent = parent;
            if (grp.ParentGuid != grp.ID)
                topGroup = TrickleUp(grp.Parent, languageGuid, topGroup);
            return topGroup;
        }

        /// <summary>
        /// Load groups from the 2 highest levels
        /// </summary>
        /// <param name="languageGuid"></param>
        /// <returns></returns>
        public Group Top2LevelGroups(string languageGuid)
        {
            Dictionary<int, Group> groupDict = _gr.GetAllGroupsDictionary(languageGuid, true);
            Group g = TreeBuilder(groupDict, 0); // Works as long as 0 is the topmost group guid

            // This makes the top 2 levels rendered
            foreach (Group level in g.Children)
            {
                foreach (Group secondLevel in level.Children)
                {
                    if (secondLevel.HasChildren())
                    {
                        secondLevel.RemoveChildren();
                        // Mark this level with plus sign
                    }
                }
            }
            return g;
        }

        /// <summary>
        /// Load groups from the top level only
        /// </summary>
        /// <param name="languageGuid"></param>
        /// <returns></returns>
        public Group Top1LevelGroups(string languageGuid)
        {
            Dictionary<int, Group> groupDict = _gr.GetAllGroupsDictionary(languageGuid,  true);
            Group g = TreeBuilder(groupDict, 0); // Works as long as 0 is the topmost group guid
            // This makes top 1 levels rendered
            foreach (Group level in g.Children)
            {
                if (level.HasChildren())
                {
                    level.RemoveChildren();
                    // Mark this level with plus sign
                }
            }
            return g;
        }

        /// <summary>
        /// Load subgroups at the level beneeth the group with the given groupGuid
        /// </summary>
        /// <param name="languageGuid"></param>
        /// <param name="groupGuid"></param>
        /// <returns></returns>
        public Group SubGroups(string languageGuid, int groupGuid)
        {
            Group parentGroup = _gr.GetGroup(groupGuid, languageGuid);
            Dictionary<int, Group> groupDict = _gr.GetSubGroupsDictionary(languageGuid, groupGuid, true);

            Group g = TreeBuilder(groupDict, parentGroup.ID);

            foreach (Group level in g.Children)
            {
                if (level.HasChildren())
                {
                    level.RemoveChildren();
                    // Mark this level with plus sign
                }
            }
            return g;
        }

        /// <summary>
        /// Load All subgroups at the level beneeth the group with the given groupGuid
        /// </summary>
        /// <param name="languageGuid"></param>
        /// <param name="groupGuid"></param>
        /// <returns></returns>
        public Group AllSubGroups(string languageGuid, int groupGuid)
        {
            Group parentGroup = _gr.GetGroup(groupGuid, languageGuid);
            Dictionary<int, Group> groupDict = _gr.GetSubGroupsDictionary(languageGuid, groupGuid, true);

            return TreeBuilder(groupDict, parentGroup.ID);
        }

        ///<summary>
        /// Get All Subgroups at ALL sub levels
        ///</summary>
        ///<param name="groupGuid"></param>
        ///<returns></returns>
        public Group AllSubGroups(int groupGuid)
        {
            Group parentGroup = _gr.GetGroup(groupGuid, null);
            Dictionary<int, Group> groupDict = _gr.GetSubGroupsDictionary(null, groupGuid,  false);

            return TreeBuilder(groupDict, parentGroup.ID);
        }

        // Working good
        ///<summary>
        ///</summary>
        ///<param name="theDict"></param>
        ///<param name="group"></param>
        public void CompositeHelper(Dictionary<int, Group> theDict, Group group)
        {
            theDict[group.GroupGuid] = group;
        }

        // Working good
        ///<summary>
        ///</summary>
        ///<param name="theDict"></param>
        ///<param name="theParentGuid"></param>
        ///<returns></returns>
        public Group TreeBuilder(Dictionary<int, Group> theDict, int theParentGuid)
        {
            var root = new Group { GroupGuid = theParentGuid, Parent = null };

            foreach (int key in theDict.Keys)
            {
                try
                {
                    int parentGuid = theDict[key].ParentGuid;
                    if (parentGuid == theParentGuid)
                    {
                        root.Children.Add(theDict[key]);
                        theDict[key].Parent = root;
                    }
                    else
                    {
                        theDict[parentGuid].Children.Add(theDict[key]);
                        theDict[key].Parent = theDict[parentGuid];
                    }
                }
                catch (Exception)
                {
                    DoSomething();
                }
            }
            theDict.Add(theParentGuid, root);
            return theDict[theParentGuid];
        }

        private void DoSomething()
        {
        }


        ///<summary>
        ///</summary>
        ///<param name="languageGuid"></param>
        ///<param name="filterArray"></param>
        ///<param name="isPosList"></param>
        ///<returns></returns>
        public Group GetFilteredGroupTree(string languageGuid, string[] filterArray, bool isPosList)
        {
            Dictionary<int, Group> groupDict;
            if (filterArray != null)
            {
                List<Group> rootList = _gr.GetRootGroupChildren(languageGuid, false);
                int iParsed;
                var parameterBuilder = new StringBuilder();
                if (isPosList)
                {
                    //We have a positive list of menu items. Remove and make an exclude list parameter.
                    int filterArrayLen = filterArray.Length;
                    for (int i = 0; i < filterArrayLen - 1; i++)
                    {
                        int.TryParse(filterArray[i].Trim(), out iParsed);
                        if (iParsed >= 0)
                        {
                            parameterBuilder.Append(rootList[iParsed].ID + ",");
                        }
                    }
                    int.TryParse(filterArray[filterArrayLen - 1].Trim(), out iParsed);
                    if (iParsed >= 0)
                    {
                        parameterBuilder.Append(rootList[iParsed].ID.ToString());
                    }
                    string includeParams = parameterBuilder.ToString(); // should look like "1,2,3"
                    groupDict = _gr.GetSubGroupsWithInclude(languageGuid, 0, true, includeParams);
                }
                else
                {
                    //We have a negative list of menu items. 
                    for (int i = 0; i < filterArray.Length - 1; i++)
                    {
                        int.TryParse(filterArray[i].Trim(), out iParsed);
                        if (iParsed >= 0)
                        {
                            parameterBuilder.Append(rootList[iParsed].ID + ",");
                        }
                        else
                        {
                            parameterBuilder.Append(rootList[rootList.Count + iParsed].ID + ",");
                        }
                    }
                    int.TryParse(filterArray[filterArray.Length - 1].Trim(), out iParsed);
                    parameterBuilder.Append(iParsed < 0
                                                ? rootList[rootList.Count + iParsed].ID.ToString()
                                                : rootList[iParsed].ID.ToString());
                    string excludeParams = parameterBuilder.ToString(); // should look like "1,2,3"
                    groupDict = _gr.GetSubGroupsWithExclude(languageGuid, 0,  true, excludeParams);
                }
            }
            else
            {
                groupDict = _gr.GetSubGroupsDictionary(languageGuid, 0,  true);
            }
            return TreeBuilder(groupDict, 0);
        }

        /* OVERLOAD WHICH TAKES ROOT GUID AS PARAMETER */

        ///<summary>
        ///</summary>
        ///<param name="languageGuid"></param>
        ///<param name="filterArray"></param>
        ///<param name="isPosList"></param>
        ///<param name="rootGuid"></param>
        ///<returns></returns>
        public Group GetFilteredGroupTree(string languageGuid, string[] filterArray, bool isPosList, int rootGuid)
        {
            Dictionary<int, Group> groupDict;
            if (filterArray != null)
            {
                List<Group> rootList = SubGroups(languageGuid, rootGuid).Children;
                if (rootList.Count == 0)
                {
                    groupDict = _gr.GetSubGroupsDictionary(languageGuid, rootGuid, true);
                    return TreeBuilder(groupDict, rootGuid);
                }
                int iParsed;
                var parameterBuilder = new StringBuilder();
                if (isPosList)
                {
                    //We have a positive list of menu items. Remove and make an exclude list parameter.
                    int filterArrayLen = filterArray.Length;
                    for (int i = 0; i < filterArrayLen - 1; i++)
                    {
                        int.TryParse(filterArray[i].Trim(), out iParsed);
                        if (iParsed >= 0)
                        {
                            parameterBuilder.Append(rootList[iParsed].ID + ",");
                        }
                    }
                    int.TryParse(filterArray[filterArrayLen - 1].Trim(), out iParsed);
                    if (iParsed >= 0)
                    {
                        parameterBuilder.Append(rootList[iParsed].ID.ToString());
                    }
                    string includeParams = parameterBuilder.ToString(); // should look like "1,2,3"
                    groupDict = _gr.GetSubGroupsWithInclude(languageGuid, rootGuid, true, includeParams);
                }
                else
                {
                    //We have a negative list of menu items. 
                    for (int i = 0; i < filterArray.Length - 1; i++)
                    {
                        int.TryParse(filterArray[i].Trim(), out iParsed);
                        if (iParsed >= 0)
                        {
                            parameterBuilder.Append(rootList[iParsed].ID + ",");
                        }
                        else
                        {
                            parameterBuilder.Append(rootList[rootList.Count + iParsed].ID + ",");
                        }
                    }
                    int.TryParse(filterArray[filterArray.Length - 1].Trim(), out iParsed);
                    parameterBuilder.Append(iParsed < 0
                                                ? rootList[rootList.Count + iParsed].ID.ToString()
                                                : rootList[iParsed].ID.ToString());
                    string excludeParams = parameterBuilder.ToString(); // should look like "1,2,3"
                    /* DEBUG WRITE */
                    Console.WriteLine(excludeParams);
                    groupDict = _gr.GetSubGroupsWithExclude(languageGuid, rootGuid, true, excludeParams);
                }
            }
            else
            {
                groupDict = _gr.GetSubGroupsDictionary(languageGuid, rootGuid, true);
            }
            return TreeBuilder(groupDict, rootGuid);
        }
    }
}