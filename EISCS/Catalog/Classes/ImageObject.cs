﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EISCS.Catalog.Classes {
    public class ImageObject {
        public string Img { get; set; }
        public string Alt { get; set; }
        public string Target { get; set; }
        public string Text { get; set; }
        public string Link { get; set; }
        public string Time { get; set; }

        public ImageObject(string img, string alt, string target, string text, string link, string time){
            Img = img;
            Alt = alt;
            Target = target;
            Text = text;
            Link = link;
            Time = time;
        }
    }
}
