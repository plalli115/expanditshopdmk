﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Xml;
using CmsPublic.ModelLayer.Models;
using CmsPublic.ModelLayer.Interfaces;
using EISCS.Catalog.Logic;
using EISCS.ExpandITFramework.Util;

namespace EISCS.Catalog.Render {
    /// <summary>
    /// Summary description for XMLcompositeWriter
    /// </summary>
    public class Catalog2Xml {
        private readonly XmlWriterSettings _wSettings;
        private readonly MemoryStream _ms;
        private readonly XmlWriter _xw;
        private int _rootNodeGuid;
        private readonly string _textKeyValue; // "NAME" or possibly "PICTURE1"
        private readonly string _urlKeyValue; // "REDIRECTURL"
        private readonly string _languageGuid;

        public Catalog2Xml(string textKeyValue, string urlKeyValue, string languageGuid) {
            _textKeyValue = textKeyValue;
            _urlKeyValue = urlKeyValue;
            _languageGuid = languageGuid;
            _wSettings = new XmlWriterSettings {Indent = true};
            _ms = new MemoryStream();
            _xw = XmlWriter.Create(_ms, _wSettings);// Get new instance and write declaration
            _xw.WriteStartDocument();
        }

        public string RenderXml(Group root) {
            _rootNodeGuid = root.GroupGuid;
            // Write the root node
            _xw.WriteStartElement("MenuRoot");
            Render(root.Children);
            _xw.WriteEndElement();
            // Close the document
            _xw.WriteEndDocument();
            // Flush the write
            _xw.Flush();
            byte[] buffer = _ms.ToArray();
            string xmlOutput = System.Text.Encoding.UTF8.GetString(buffer, 3, buffer.Length -3);
            return xmlOutput;
        }

        private void Render(IEnumerable<Group> locations) {
            foreach (Group grp in locations) {
                if (!IsStateVisible(grp)) continue;
                RenderParent(grp);
                if (grp.HasChildren()) {
                    RenderChildren(grp.Children);
                }
                _xw.WriteEndElement();
            }
        }

        private void RenderParent(Group grp){
            RenderElements(grp, grp.Parent.GroupGuid == _rootNodeGuid ? "Menu" : "SubMenu");
        }

        private static DateTime? GetDateValue(Group grp, string value) {
            if (!grp.Properties.PropDict.ContainsKey(value)) {
                return null;
            }

            if (string.IsNullOrEmpty(grp.Properties.PropDict[value])) {
                return null;
            }

            string dateString = grp.Properties.PropDict[value];
            DateTime dateTime;
            if (!DateTime.TryParse(dateString, out dateTime))
                return null;

            return dateTime;
        }

        public bool IsStateVisible(Group grp){
            DateTime? startDate = GetDateValue(grp, "STARTDATE"); 
            DateTime? endDate = GetDateValue(grp, "ENDDATE");  
            if (startDate == null || endDate == null){
                return true;
            }
            DateTime currentDate = DateTime.Now;
            return currentDate >= startDate.Value && currentDate <= endDate.Value;
        }

        private void RenderChildren(IEnumerable<Group> grpList) {
            foreach (Group grp in grpList) {
                if (!IsStateVisible(grp)) continue;
                RenderElements(grp, "SubMenu");
                if (grp.HasChildren()){
                    Render(grp.Children);
                }
                _xw.WriteEndElement();
            }
        }

        private void RenderElements(Group grp, string elementName) {
            string text = grp.Properties.PropDict[_textKeyValue];
            string urlValue = grp.Properties.PropDict[_urlKeyValue];
            string url = NavigationUtil.NavigationLink(grp, urlValue, "{0}", _languageGuid); // Get navigation
            string target = NavigationUtil.Target(urlValue); // Get target

            // Handle a different property type than the expected "NAME" property
            string propType = null;
            if (grp.Properties.PropTypeDict.ContainsKey(_textKeyValue)) {
                propType = grp.Properties.PropTypeDict[_textKeyValue];
            }
            if (!string.IsNullOrEmpty(propType) && !string.IsNullOrEmpty(text)) {
                PropTypes p = (PropTypes)Enum.Parse(typeof(PropTypes), propType);
                if (p == PropTypes.PICTURE) {
                    string altText = grp.Properties.PropDict["NAME"];
                    text = "<img src=\"" + AppUtil.ShopFullPath() + text + "\" alt=\"" + altText + "\" title=\"" + altText + "\">";
                }
            }
            else {
                // Default to the NAME property
                text = grp.Properties.PropDict["NAME"];
            }

            _xw.WriteStartElement(elementName);
            _xw.WriteStartAttribute("text");
            _xw.WriteString(text);
            _xw.WriteEndAttribute();
            _xw.WriteStartAttribute("url");
            _xw.WriteString(url);
            _xw.WriteEndAttribute();
            _xw.WriteStartAttribute("target");
            _xw.WriteString(target);
            _xw.WriteEndAttribute();
        }
    }
}