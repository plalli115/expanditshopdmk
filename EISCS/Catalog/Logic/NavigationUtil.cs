﻿using System;
using CmsPublic.ModelLayer.Models;
using EISCS.ExpandITFramework.Util;
using EISCS.Routing.Providers;

namespace EISCS.Catalog.Logic {
    /// <summary>
    /// Summary description for NavigationUtil
    /// </summary>
    public class NavigationUtil {
        /// <summary>
        /// Creates a navigation link from the REDIRECTURL property.
        /// </summary>
        /// <param name="grp"></param>
        /// <param name="urlValue">Input string to be converted to url</param>
        /// <param name="navPart">A format string like "javascript:window.location.href='{0}';"</param>
        /// <param name="languageGuid"></param>
        /// <returns>
        /// A full navigation link like "javascript:window.location.href='http://localhost/shop/link.aspx?GroupGuid=1';"
        /// or a full url string like "http://localhost/shop/link.aspx?GroupGuid=1" if the format string is "{0}".
        /// </returns>
        public static string NavigationLink(Group grp, string urlValue, string navPart, string languageGuid) {
            string navLink;
            
            if (string.IsNullOrEmpty(urlValue)) {
                navLink =
                    //AppUtil.ShopFullPath() + "/" + grp.RouteUrl;
                    RouteManager.Route.CreateTemplateGroupLink(grp, languageGuid);
            }
            else if (urlValue.StartsWith("http:") || urlValue.StartsWith("https:")) {
                navLink = urlValue;
            }
            else if (urlValue.StartsWith("www.")) {
                navLink = "http://" + urlValue;
            }
            else if (urlValue.Contains("?")) {
                if (urlValue.Contains("GroupGuid=")) {
                    navLink = AppUtil.ShopFullPath() + "/" + urlValue;
                }
                else {
                    navLink = AppUtil.ShopFullPath() + "/" + urlValue + "&GroupGuid=" + grp.ID;
                }
            }
            else {
                navLink = AppUtil.ShopFullPath() + "/" + urlValue;
            }
            return String.Format(navPart, navLink);
        }

        public static string Target(string urlValue)
        {
            const string defaultTarget = "_self";
            if (string.IsNullOrEmpty(urlValue))
            {
                return defaultTarget;
            }
            if (urlValue.StartsWith("http:") || urlValue.StartsWith("https:"))
            {
                return "_blank";
            }
            return urlValue.StartsWith("www.") ? "_blank" : defaultTarget;
        }
    }
}