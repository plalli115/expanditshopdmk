﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EISCS.Catalog.Classes;

namespace EISCS.Catalog.Logic {
    public class MultiImageHelper {

        public string SliderTime { get; set; }
        public List<ImageObject> ImageObjects { get; private set; }

        public MultiImageHelper() {
            ImageObjects = new List<ImageObject>();
        }

        public void PropToObjects(string propertyString) {
            if(string.IsNullOrEmpty(propertyString)){return;}

            var splittedProperties = propertyString.Split('|');

            if (splittedProperties.Length < 7) {return;}

            if(splittedProperties.Length % 2 == 0){return;}

            SliderTime = splittedProperties[0];

            for (int i = 7; i < splittedProperties.Length + 6; i += 6) {
                var imgObj = new ImageObject(splittedProperties[i - 6], splittedProperties[i - 5],
                                             splittedProperties[i - 4], splittedProperties[i - 3],
                                             splittedProperties[i - 2], splittedProperties[i - 1]);
                Push(imgObj);
            }
        }

        public void Push(ImageObject imageObject) {
            ImageObjects.Add(imageObject);
        }

    }
}
