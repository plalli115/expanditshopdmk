﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CmsPublic.ModelLayer.Models;
using EISCS.ExpandITFramework.Infrastructure;
using EISCS.ExpandITFramework.Infrastructure.Configuration;
using EISCS.ExpandITFramework.Infrastructure.Configuration.Interfaces;
using EISCS.ExpandITFramework.Util;
using EISCS.Shop.DO.Interface;
using StackExchange.Profiling;

namespace EISCS.Catalog.Logic
{
    public interface ICatalogLogic
    {
        /// <summary>
        /// Returns a menu sub tree from the Catalog.
        /// </summary>
        /// <param name="index">
        /// Can be a negative or a positive value. Starting at zero, a positive value gets the item in that position 
        /// in the list of Groups. A negative value gets the item from the end of the list, for example -1 gets the 
        /// last item, -2 gets the second last item.
        /// </param>
        /// <param name="languageGuid"></param>
        /// <returns></returns>
        Group GetMenuGroupTree(int index, string languageGuid);

        Group GetMenuGroupTree(int index, string languageGuid, int rootGuid);

        /// <summary>
        /// Get the entire tree structure
        /// </summary>
        /// <param name="languageGuid"></param>
        /// <returns></returns>
        Group GetAllGroupTree(string languageGuid);

        /// <summary>
        /// Loads a filtered catalog structure as a Composite Group object
        /// </summary>
        /// <param name="languageGuid">
        /// If the languageGuid parameter is null or empty the languageGuid
        /// will be retrieved from the Session
        /// </param>
        /// <returns></returns>
        /// <remarks>Loads the Catalog based on the filter applied in the web.config</remarks>
        Group GetCatalogGroupTree(string languageGuid);

        /// <summary>
        /// Get a list representation of the filtered top level groups in the catalog
        /// </summary>
        /// <param name="languageGuid"></param>
        /// <returns></returns>
        List<Group> GetCatalogTopGroups(string languageGuid);

        List<Group> GetCatalogNextLevel(int groupGuid, string languageGuid);
        int CatalogTotalDepth();
        Group GetGroup(int groupGuid, string languageGuid);
    }

    public class CatalogLogic : ICatalogLogic
    {

        private readonly IGroupRepository _gr;
        private readonly CatalogTreeUtility _ch;
        private readonly IConfigurationValue _configurationReader;
        private readonly bool _cacheCatalog;

        private string[] GetNegList() { return Utilities.Split(Utilities.CStrEx(_configurationReader.AppSettings("CATALOG_HIDE_NEGLIST")), ","); }
        private string[] GetPosList() { return Utilities.Split(Utilities.CStrEx(_configurationReader.AppSettings("CATALOG_SHOW_POSLIST")), ","); }
        private static string GetSessionLanguageGuid() { return Utilities.CStrEx(HttpContext.Current.Session["LanguageGuid"]); }
        private static string GetlanguageGuid(string languageGuid) { return string.IsNullOrEmpty(languageGuid) ? GetSessionLanguageGuid() : languageGuid; }

        public CatalogLogic()
            : this(new ConfigurationValue(), null)
        {
        }

        public CatalogLogic(IConfigurationValue configReader, IGroupRepository groupRepository)
        {
            if (groupRepository != null)
            {
                _gr = groupRepository;
            }
            else
            {
                _gr = DependencyResolver.Current.GetService<IGroupRepository>();
            }
            _ch = new CatalogTreeUtility(_gr);
            _configurationReader = configReader;
            _cacheCatalog = ExpanditLib2.CBoolEx(_configurationReader.AppSettings("CACHE_CATALOG_TREE"));
        }

        /// <summary>
        /// Returns a menu sub tree from the Catalog.
        /// </summary>
        /// <param name="index">
        /// Can be a negative or a positive value. Starting at zero, a positive value gets the item in that position 
        /// in the list of Groups. A negative value gets the item from the end of the list, for example -1 gets the 
        /// last item, -2 gets the second last item.
        /// </param>
        /// <param name="languageGuid"></param>
        /// <returns></returns>
        public Group GetMenuGroupTree(int index, string languageGuid)
        {
            string strLanguageGuid = GetlanguageGuid(languageGuid);

            // Check if already in cache 
            string key = "MenuGroupTree-" + index + "-" + strLanguageGuid;

            object oGroup = CacheManager.CacheGet(key);

            // Return object from cache if set 
            if (oGroup != null)
            {
                return oGroup as Group;
            }

            int groupGuid;
            // Get all top level groups without properties
            List<Group> topList = _gr.GetTopGroups(strLanguageGuid, false);

            // Try to retrieve a Group ID from the List at the given index. 0 = first group. -1 = last group.            
            try
            {
                groupGuid = index < 0 ? topList.ElementAt(topList.Count + index).ID : topList.ElementAt(index).ID;
            }
            catch
            {
                // index out of range, bad input.
                groupGuid = 0;
            }

            // Get the group with the correct ID. This will be the root of the menu sub tree.
            Group compositeGroup = _ch.AllSubGroups(strLanguageGuid, groupGuid);

            // Cache for better performance
            String[] tableNames = Utilities.Split(_configurationReader.AppSettings("GroupProductRelatedTablesForCaching"), "|");
            CacheManager.CacheSetAggregated(key, compositeGroup, tableNames);

            return compositeGroup;
        }

        //Overload test
        public Group GetMenuGroupTree(int index, string languageGuid, int rootGuid)
        {
            string strLanguageGuid = GetlanguageGuid(languageGuid);
            List<Group> topList = GetCatalogNextLevel(rootGuid, strLanguageGuid);
            int groupGuid;
            // Try to retrieve a Group ID from the List at the given index. 0 = first group. -1 = last group.            
            try
            {
                groupGuid = index < 0 ? topList.ElementAt(topList.Count + index).ID : topList.ElementAt(index).ID;
            }
            catch
            {
                // index out of range, bad input.
                groupGuid = 0;
            }
            // Get the group with the correct ID. This will be the root of the menu sub tree.
            Group compositeGroup = _ch.AllSubGroups(strLanguageGuid, groupGuid);
            return compositeGroup;
        }

        /// <summary>
        /// Get the entire tree structure
        /// </summary>
        /// <param name="languageGuid"></param>
        /// <returns></returns>
        public Group GetAllGroupTree(string languageGuid)
        {
            string strLanguageGuid = GetlanguageGuid(languageGuid);
            return _ch.GetFilteredGroupTree(strLanguageGuid, null, false);
        }

        /// <summary>
        /// Loads a filtered catalog structure as a Composite Group object
        /// </summary>
        /// <param name="languageGuid">
        /// If the languageGuid parameter is null or empty the languageGuid
        /// will be retrieved from the Session
        /// </param>
        /// <returns></returns>
        /// <remarks>Loads the Catalog based on the filter applied in the web.config</remarks>
        public Group GetCatalogGroupTree(string languageGuid)
        {
            string strLanguageGuid = GetlanguageGuid(languageGuid);
            
            string key = "CatalogGroupTree-" + strLanguageGuid;

            if (_cacheCatalog)
            {
                // Return object from cache if set 
                object oGroup = CacheManager.CacheGet(key);
                if (oGroup != null)
                {
                    return oGroup as Group;
                }
            }

            Group compositeGroup;

            string[] arrShowIndexes = GetPosList();
            string[] arrHiddenIndexes = GetNegList();

            var profiler = MiniProfiler.Current;
            using (profiler.Step("GetFilteredGroupTree Method"))
            {
                if (arrShowIndexes.Length > 0 && !string.IsNullOrEmpty(arrShowIndexes[0]))
                {
                    //We have a positive list, ignore negative list
                    compositeGroup = _ch.GetFilteredGroupTree(strLanguageGuid, arrShowIndexes, true);
                }
                else if (arrHiddenIndexes.Length > 0 && !string.IsNullOrEmpty(arrHiddenIndexes[0]))
                {
                    //We have a negative list, use it
                    compositeGroup = _ch.GetFilteredGroupTree(strLanguageGuid, arrHiddenIndexes, false);
                }
                else
                {
                    //No lists exist - get unfiltered tree
                    compositeGroup = _ch.GetFilteredGroupTree(strLanguageGuid, null, false);
                }
            }

            if (_cacheCatalog)
            {
                // Insert into cache if caching is preferred
                String[] tableNames =
                    Utilities.Split(_configurationReader.AppSettings("GroupProductRelatedTablesForCaching"), "|");
                CacheManager.CacheSetAggregated(key, compositeGroup, tableNames);
            }
            return compositeGroup;

        }

        /// <summary>
        /// Get a list representation of the filtered top level groups in the catalog
        /// </summary>
        /// <param name="languageGuid"></param>
        /// <returns></returns>
        public List<Group> GetCatalogTopGroups(string languageGuid)
        {
            return GetCatalogGroupTree(languageGuid).Children;
        }

        public List<Group> GetCatalogNextLevel(int groupGuid, string languageGuid)
        {
            return _ch.SubGroups(GetlanguageGuid(languageGuid), groupGuid).Children;
        }

        public int CatalogTotalDepth()
        {
            int levels = 0;
            object oInt = _gr.CatalogLevels();
            if (Utilities.SafeDbNull(oInt) != null)
            {
                levels = (int)oInt;
            }
            return levels;
        }

        public Group GetGroup(int groupGuid, string languageGuid)
        {
            return _gr.GetGroup(groupGuid, GetlanguageGuid(languageGuid));
        }
    }
}