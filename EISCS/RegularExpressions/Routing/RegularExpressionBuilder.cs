﻿using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Text;

namespace EISCS.RegularExpressions.Routing {
    /// <summary>
    /// RegularExpressionBuilder
    /// </summary>
    public static class RegularExpressionBuilder {
        public static string Build(ReadOnlyCollection<string> segments) {
            if (segments == null) {
                throw new ArgumentNullException("segments", "Segment List is Null");
            }

            StringBuilder expression =
                new StringBuilder();

            for (int index = 0; index < segments.Count; index++) {
                string current =
                    segments[index];

                if (current.IsParameterized()) {
                    string placeHolder =
                        string.Concat("{", current.GetParameterName(), "}");

                    string replacement =
                        string.Format(CultureInfo.InvariantCulture, @"(?<{0}>[/A-Ö0-9_-]*)", current.GetParameterName());

                    current = current.Replace(placeHolder, replacement);
                }

                expression.Append(current);

                if (index < segments.Count - 1) {
                    expression.Append("/");
                }
            }

            return expression.ToString();
        }
    }
}