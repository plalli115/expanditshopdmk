﻿using System.Configuration;
using CmsPublic.DataRepository;
using EISCS.Shop.DO;

namespace EISCS.Shop
{
    public class PortalDatabaseConnectionFactory : ExpanditDbFactory, IPortalDatabaseConnectionFactory
    {
        public PortalDatabaseConnectionFactory(ConnectionStringSettings connectionSettings)
            : base(connectionSettings.ConnectionString, connectionSettings.ProviderName)
        {
        }
    }
}