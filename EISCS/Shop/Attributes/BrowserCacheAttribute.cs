﻿using System;
using System.Web;
using System.Web.Mvc;

namespace EISCS.Shop.Attributes
{
    public class BrowserCacheAttribute : ActionFilterAttribute
    {
        ///<summary>
        ///</summary>
        public BrowserCacheAttribute()
        {
            Duration = 10;
        }

        /// <SUMMARY>  
        /// Gets or sets the cache duration in seconds.   
        /// The default is 10 seconds.  
        /// </SUMMARY>  
        /// <VALUE>The cache duration in seconds.</VALUE>  
        public int Duration { get; set; }

        ///<summary>
        ///</summary>
        public bool PreventBrowserCaching { get; set; }

        /// <summary>
        /// Called by the MVC framework after the action method executes.
        /// </summary>
        /// <param name="filterContext">The filter context.</param>
        public override void OnActionExecuted(
            ActionExecutedContext filterContext)
        {
            if (Duration < 0) return;

            HttpCachePolicyBase cache = filterContext.HttpContext
                .Response.Cache;

            if (PreventBrowserCaching)
            {
                cache.SetCacheability(HttpCacheability.NoCache);
                cache.SetExpires(DateTime.UtcNow.AddHours(-1));
                cache.SetNoStore();
                Duration = 0;
                return;
            }
            
            cache.SetCacheability(HttpCacheability.Public);

            TimeSpan cacheDuration = TimeSpan.FromSeconds(Duration);
            cache.SetExpires(DateTime.Now.Add(cacheDuration));
            cache.SetMaxAge(cacheDuration);
            cache.AppendCacheExtension("must-revalidate,"
                                       + "proxy-revalidate");
        }
    }
}
