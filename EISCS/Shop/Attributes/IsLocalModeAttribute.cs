﻿using System.Web.Mvc;
using System.Web.Routing;
using EISCS.Wrappers.Configuration;

namespace EISCS.Shop.Attributes {
	public class IsLocalModeAttribute : ActionFilterAttribute {
		public override void OnActionExecuting(ActionExecutingContext filterContext)
		{
			var configuration = new ConfigurationObject();
			var isLocalMode = (bool)filterContext.HttpContext.Application["IsLocalMode"];
			if (configuration.Read("IGNORE_LOCALMODE") == "0" && isLocalMode == true) {
				filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new {action="Index", controller="LocalMode"}));
			}
		}
	}
}
