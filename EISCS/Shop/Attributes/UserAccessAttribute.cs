﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using EISCS.ExpandITFramework.Util;
using EISCS.Shop.DO.Interface;

namespace EISCS.Shop.Attributes
{
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class)]
    public class UserAccessAttribute : AuthorizeAttribute
    {
        public string AccessClass { get; set; }
        private IExpanditUserService _expanditUserService;
        private IHttpRuntimeWrapper _httpRuntimeWrapper;

        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            _expanditUserService = DependencyResolver.Current.GetService<IExpanditUserService>();
            _httpRuntimeWrapper = DependencyResolver.Current.GetService<IHttpRuntimeWrapper>();
			
            string location = string.Format("~/Accounts/Login?returl={0}", Uri.EscapeUriString(filterContext.HttpContext.Request.Path));

            if (!_expanditUserService.UserAccess(AccessClass))
            {
                filterContext.Result = new RedirectResult(location);
            }
        }

        private string VirtualRoot()
        {
            return _httpRuntimeWrapper.AppDomainAppVirtualPath.Equals("/")
                                                 ? "/"
                                                 : _httpRuntimeWrapper.AppDomainAppVirtualPath + "/";
        }
    }
}
