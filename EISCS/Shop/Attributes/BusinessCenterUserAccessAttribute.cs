﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using EISCS.CMS.Account;
using EISCS.ExpandITFramework.Util;
using EISCS.Shop.DO.Interface;

namespace EISCS.Shop.Attributes
{
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class)]
    public class BusinessCenterUserAccessAttribute : AuthorizeAttribute
    {
        public string AccessClass { get; set; }
        private IBusinessCenterAccountManager _accountManager;

        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            _accountManager = DependencyResolver.Current.GetService<IBusinessCenterAccountManager>();

            string location = string.Format("~/cms/Account/Login?returl={0}", Uri.EscapeUriString(filterContext.HttpContext.Request.Path));

            if (!(bool) filterContext.HttpContext.Application["IsLocalMode"])
            {
                filterContext.Result = new RedirectResult("~/");
                return;
            }

            if (!_accountManager.UserAccess(AccessClass))
            {
                filterContext.Result = new RedirectResult(location);
            }
        }
    }
}
