﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Configuration;

namespace EISCS.Shop.Attributes
{
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property | AttributeTargets.Parameter, AllowMultiple = false, Inherited = true)]
    public class PasswordStringLengthAttribute : StringLengthAttribute
    {
        private readonly int _minimumLength = Convert.ToInt32(ConfigurationManager.AppSettings["PASSWORD_LEN"]);
        private readonly int _maximumLength;

        public PasswordStringLengthAttribute(int maximumLength)
            : base(maximumLength)
        {
            _maximumLength = maximumLength;
        }

        public override string FormatErrorMessage(string name)
        {
            var error = ErrorMessageString;
            return error.Replace("{0}", name).Replace("{1}", _maximumLength.ToString()).Replace("{2}", _minimumLength.ToString());
        }

        public override bool IsValid(object value)
        {
            if (value != null && base.IsValid(value))
            {
                var val = value.ToString();
                return val.Length >= _minimumLength;
            }
            
            return false;            
        }
    }
}