﻿using System.Configuration;
using CmsPublic.DataRepository;
using EISCS.Shop.DO;

namespace EISCS.Shop
{
    public class ShopDatabaseConnectionFactory : ExpanditDbFactory, IShopDatabaseConnectionFactory
    {
        public ShopDatabaseConnectionFactory(ConnectionStringSettings connectionSettings)
            : base(connectionSettings.ConnectionString, connectionSettings.ProviderName)
        {
        }
    }
}