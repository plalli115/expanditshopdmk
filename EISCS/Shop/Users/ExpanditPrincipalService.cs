﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Web;
using System.Web.Security;

namespace EISCS.Shop.Users
{
    public class ExpanditPrincipalService : IPrincipalService
    {
        private readonly HttpContextBase _context;

        public ExpanditPrincipalService(HttpContextBase context)
        {
            _context = context;
        }

        #region IPrincipalService Members

        public IPrincipal GetCurrent()
        {
            IPrincipal user = _context.User;
            // User is signed in and conversion is done
            if (user is ExpanditUserPrincipal)
                return user;

            // user is signed in, but conversion is not yet done
            if (user != null && user.Identity.IsAuthenticated && user.Identity is FormsIdentity)
            {
                var id = (FormsIdentity)_context.User.Identity;

                var ticket = id.Ticket;
                if (FormsAuthentication.SlidingExpiration)
                    ticket = FormsAuthentication.RenewTicketIfOld(ticket);

                var fid = new ExpanditUserIdentity(ticket);
                return new ExpanditUserPrincipal(fid);
            }

            // Default
            return new ExpanditUserPrincipal(new ExpanditUserIdentity(null));
        }

        #endregion
    }

    public interface IPrincipalService
    {
        IPrincipal GetCurrent();
    }
}
