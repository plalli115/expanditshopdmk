﻿using System;
using System.Web;
using System.Web.Security;
using EISCS.Shop.DO.Dto;
using Newtonsoft.Json;

namespace EISCS.Shop.Users
{
    public class ExpanditFormsAuthCookie
    {
        public string Id { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
        public string Role { get; set; }
        public bool RememberMe { get; set; }
    }
    
    public interface IFormsAuthenticationService
    {
        void SignIn(UserTable user, bool createPersistentCookie);
        void SignOut();
    }

    public class FormsAuthenticationService : IFormsAuthenticationService
    {
        private readonly HttpContextBase _httpContext;

        public FormsAuthenticationService(HttpContextBase httpContext)
        {
            _httpContext = httpContext;
        }

        #region IFormsAuthenticationService Members

        public void SignIn(UserTable user, bool createPersistentCookie)
        {
            if (user == null)
                throw new ArgumentNullException("user");

            var cookie = new ExpanditFormsAuthCookie
            {
                Id = user.UserGuid,
                Email = user.EmailAddress,
                UserName = user.UserLogin,
                RememberMe = createPersistentCookie,
                Role = user.RoleId
            };

            string userData = JsonConvert.SerializeObject(cookie);
            var ticket = new FormsAuthenticationTicket(1, cookie.Email, DateTime.Now,
                                                       DateTime.Now.Add(FormsAuthentication.Timeout),
                                                       createPersistentCookie, userData);
            string encTicket = FormsAuthentication.Encrypt(ticket);
            var httpCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encTicket) { Expires = DateTime.Now.Add(FormsAuthentication.Timeout) };

            _httpContext.Response.Cookies.Add(httpCookie);
            
        }

        public void SignOut()
        {
            FormsAuthentication.SignOut();
        }

        #endregion
    }
}
