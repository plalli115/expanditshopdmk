﻿using System;
using System.Reflection;
using System.Runtime.Serialization;
using System.Security.Principal;
using System.Web.Security;
using Newtonsoft.Json;

namespace EISCS.Shop.Users
{
    [Serializable]
    public class ExpanditUserIdentity : MarshalByRefObject, IIdentity, ISerializable
    {
        private readonly string _name;

        public string Id { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
        public bool RememberMe { get; set; }
        public string Role { get; set; }

        public ExpanditUserIdentity(FormsAuthenticationTicket ticket)
        {
            if (ticket == null)
            {
                _name = "Guest";
                Role = "Guest";
                return;
            }

            var data = JsonConvert.DeserializeObject<ExpanditFormsAuthCookie>(ticket.UserData);

            if (data == null)
            {
                return;
            }

            Id = data.Id;
            UserName = data.UserName;
            _name = UserName;
            Email = data.Email;
            Role = data.Role;
            RememberMe = data.RememberMe;
        }

        public string Name
        {
            get { return _name; }
        }

        public string AuthenticationType
        {
            get { return "forms"; }
        }

        public bool IsAuthenticated
        {
            get { return !string.IsNullOrEmpty(_name); }
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            if (context.State == StreamingContextStates.CrossAppDomain)
            {
                GenericIdentity gIdent = new GenericIdentity(Name, AuthenticationType);
                info.SetType(gIdent.GetType());

                MemberInfo[] serializableMembers = FormatterServices.GetSerializableMembers(gIdent.GetType());
                object[] serializableValues = FormatterServices.GetObjectData(gIdent, serializableMembers);

                for (int i = 0; i < serializableMembers.Length; i++)
                {
                    info.AddValue(serializableMembers[i].Name, serializableValues[i]);
                }
            }
            else
            {
                throw new InvalidOperationException("Serialization not supported");
            }
        }
    }
}
