﻿using System;
using System.Security.Principal;

namespace EISCS.Shop.Users
{
    public class ExpanditUserPrincipal : IPrincipal
    {
        private readonly ExpanditUserIdentity _identity;

        #region IPrincipal

        public IIdentity Identity { get { return _identity; } }

        public bool IsInRole(string role)
        {
            return String.CompareOrdinal(role, _identity.Role) == 0;
        }

        #endregion

        public ExpanditUserPrincipal(ExpanditUserIdentity identity)
        {
            _identity = identity;
        }

        public int Id { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public TimeZoneInfo TimeZone { get; set; }
        public bool RememberMe { get; set; }
        public string Role { get; set; }
    }

    public class UserPrincipalSerializeModel
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public TimeZoneInfo TimeZone { get; set; }
        public bool RememberMe { get; set; }
        public string Role { get; set; }

    }
}
