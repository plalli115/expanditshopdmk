﻿using CmsPublic.DataRepository;
using EISCS.Shop.DO;
using EISCS.Shop.DO.Interface;

namespace EISCS.Shop
{
    public interface IShopDatabaseConnectionFactory : IExpanditDbFactory
    {
    }
}