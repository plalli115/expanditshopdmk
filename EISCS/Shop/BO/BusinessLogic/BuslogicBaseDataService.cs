﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CmsPublic.DataRepository;
using EISCS.Shop.DO;
using EISCS.Shop.DO.Dto.Ledger;
using EISCS.Shop.DO.Interface;

namespace EISCS.Shop.BO.BusinessLogic
{
    public class BuslogicBaseDataService : BaseDataService
    {
        public BuslogicBaseDataService(IExpanditDbFactory dbFactory)
            : base(dbFactory)
        {
        }

        public virtual LedgerEntryContainer GetCustomerLedgerEntries(string customerGuid)
        {
            string query = "SELECT * FROM CustomerLedgerEntry WHERE CustomerGuid = @customerGuid";
            var entries = GetResults<CustomerLedgerEntryItem>(query, new { customerGuid }).ToList();
            if (entries.Count == 0)
            {
                return null;
            }
            var currency = entries[0].CurrencyGuid;
            return new LedgerEntryContainer { LedgerEntries = entries, BalanceAmountLCY = entries.Sum(item => item.AmountLCY), Currency = currency};
        }
    }
}
