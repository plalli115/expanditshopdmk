﻿using System;
using System.Collections.Generic;
using System.Linq;
using EISCS.ExpandITFramework.Infrastructure;
using EISCS.ExpandITFramework.Util;
using EISCS.Shop.DO.Dto.Backend.Navision;
using EISCS.Shop.DO.Dto.Backend.Navision.Nav;
using EISCS.Shop.DO.Dto.Cart;
using EISCS.Shop.DO.Interface;
using EISCS.Shop.DO.Service;
using EISCS.Wrappers.Configuration;
using ExpandIT;
using EISCS.Shop.DO.Dto.Promotions;

namespace EISCS.Shop.BO.BusinessLogic.Navision.Nav
{
    /// <summary>
    /// Business logic
    /// This code depend on these tables. Please make sure that these tables are extracted.
    /// The c:\program files\ExpandIT\Internet Shop\Attain_CP.eic file contains the necessary extract information.
    /// 
    /// Tableno  Table name in web database.
    ///    19: Attain_CustomerInvoiceDiscount
    ///  7002: Attain_ProductPrice
    ///  7004: Attain_SalesLineDiscount
    ///   325: Attain_VATPostingSetup
    /// </summary>
    /// <remarks></remarks>
    public class BuslogicNav : BuslogicBase
    {
        private string _productInListParameter;
        private string _itemCustomerDiscountGrouplist;
        private readonly ICurrencyConverter _currencyConverter;
        private readonly IConfigurationObject _configurationObject;
        private List<VatPctInfo> _vatPctInfos;
        private readonly bool _isCollectQuantitiesForLineDiscount;
        private readonly INavDataService _dataService;
        private readonly IShippingHandlingPriceRepository _shippingHandlingPriceRepository;
        private readonly IExpanditUserService _expanditUserService;
        private readonly PromotionsServices _promotionsServices;

        public BuslogicNav(IConfigurationObject configuration, ICurrencyConverter currencyConverter, INavDataService dataService, IShippingHandlingPriceRepository shippingHandlingPriceRepository, IExpanditUserService expanditUserService, PromotionsServices promotionsServices)
        {
            _currencyConverter = currencyConverter;
            _configurationObject = configuration;
            _dataService = dataService;
            _shippingHandlingPriceRepository = shippingHandlingPriceRepository;
            _expanditUserService = expanditUserService;
            _promotionsServices = promotionsServices;
            _isCollectQuantitiesForLineDiscount =
                ExpanditLib2.CBoolEx(_configurationObject.Read("NAV_COLLECT_QUANTITES_FOR_LINE_DISCOUNTS"));
        }


        public override PageMessages CalculateOrder<T>(BaseOrderHeader<T> order, string currency, string defaultCurrency)
        {
            try
            {
                // 1.1 Set current currency
                order.CurrencyGuid = currency;

                // 1.2 Get Quantities
                AccumulateProductQuantities(order);

                // 2. Initialization - get customer specific data from CustomerTable
                string customerGuid = GetCustomerGuid(order.UserGuid);
                InitOrder(order, customerGuid, currency);

                // 3. Line calculations
                StandardPrice(order, defaultCurrency);
                if (_configurationObject.Read("USE_USPROMOS") == "TRUE")
                {
                    ListPrice(order, defaultCurrency);
                    PromoLineDiscounts(order, defaultCurrency);
                }
                else
                {
                    LineDiscount(order, defaultCurrency);
                    ListPrice(order, defaultCurrency);
                }
                ItemPriceFixCurrency(order, defaultCurrency);
                OrderLineTotal(order, defaultCurrency);

                // Precalculate shipping
                // This is needed because the ServiceCharge lookup in InvoiceDiscounts needs the Shipping and Handling values to find the correct entry
                PreCalculateShipping(order, currency, defaultCurrency);

                // 4. Header calculations
                SubTotal(order, customerGuid, defaultCurrency); 
                InvoiceDiscounts(order, customerGuid, defaultCurrency);
                CalculateVat(order, customerGuid, defaultCurrency);

                // Mark order as calculated
                order.IsCalculated = true;
            }
            catch (Exception ex)
            {
                order.Total = 0;
                order.TotalInclTax = 0;
                order.InvoiceDiscount = 0;
                order.ServiceCharge = 0;
                order.ServiceChargeInclTax = 0;
                order.IsCalculated = false;

                var messages = new PageMessages();
                messages.Errors.Add(ex.Message);
                return messages;
            }
            return null;
        }

        double PromoInvoiceDiscounts<T>(BaseOrderHeader<T> order, string defaultCurrency) where T : BaseOrderLine
        {
            if (String.IsNullOrEmpty(order.PromotionCode))
            {
                return 0;
            }
            double totalPromoDiscount = 0;
            Promotion p = _promotionsServices.getPromoByCode(order.PromotionCode);
            List<int> InvoiceTypes = new List<int> { 5, 6, 7, 8 };
            foreach (PromotionEntry promoEntry in p.PromotionEntries)
            {
                if (!InvoiceTypes.Contains(promoEntry.PromotionType))
                {
                    continue;
                }
                else if (_promotionsServices.PromotionEntryMet(promoEntry, order))
                {
                    double pctDiscount;
                    if (promoEntry.DiscountAmount > 0)
                    {
                        pctDiscount = (promoEntry.DiscountAmount / order.SubTotal) * 100;
                    }
                    else if (promoEntry.DiscountPercentage > 0)
                    {
                        pctDiscount = promoEntry.DiscountPercentage;
                    }
                    else
                    {
                        pctDiscount = 0;
                    }
                    if (promoEntry.MaxDiscountAmount > 0)
                    {
                        pctDiscount = Math.Min(pctDiscount, (promoEntry.MaxDiscountAmount / order.SubTotal) * 100);
                    }
                    totalPromoDiscount += pctDiscount;
                }
            }
            return totalPromoDiscount;
        }

        public override void PromoShippingDiscounts<T>(BaseOrderHeader<T> order, string defaultCurrency)
        {
            if (String.IsNullOrEmpty(order.PromotionCode))
            {
                return;
            }
            Promotion p = _promotionsServices.getPromoByCode(order.PromotionCode);
            List<int> ShippingTypes = new List<int> { 3, 4 };
            foreach (PromotionEntry promoEntry in p.PromotionEntries)
            {
                if (!ShippingTypes.Contains(promoEntry.PromotionType))
                {
                    continue;
                }
                else if (_promotionsServices.PromotionEntryMet(promoEntry, order))
                {
                    double amtDiscount = 0;
                    if (promoEntry.DiscountAmount > 0)
                    {
                        if (promoEntry.PromotionType == 3)
                        {
                            //For ShippingToItem promos, discount is multiplied by the quantity of the Linked Item purchased
                            int num_purchased = (int)order.Lines.Find(l => l.ProductGuid == promoEntry.LinkedCode).Quantity;
                            amtDiscount = promoEntry.DiscountAmount * num_purchased;
                        }
                        else
                        {
                            amtDiscount = promoEntry.DiscountAmount;
                        }
                    }
                    else if (promoEntry.DiscountPercentage > 0)
                    {
                        amtDiscount = Math.Round((promoEntry.DiscountPercentage / 100) * order.ShippingAmount, 2);
                    }
                    else
                    {
                        amtDiscount = 0;
                    }
                    if (promoEntry.MaxDiscountAmount > 0)
                    {
                        amtDiscount = Math.Min(amtDiscount, promoEntry.MaxDiscountAmount);
                    }
                    amtDiscount = Math.Min(amtDiscount, order.ShippingAmount);
                    order.ShippingAmount -= amtDiscount;
                }
            }


        }
        private void PromoLineDiscounts<T>(BaseOrderHeader<T> order, string defaultCurrency) where T : BaseOrderLine
        {
            if (String.IsNullOrEmpty(order.PromotionCode))
            {
                return;
            }
            Promotion p = _promotionsServices.getPromoByCode(order.PromotionCode);
            List<int> LineTypes = new List<int> { 1, 2 };
            foreach (PromotionEntry promoEntry in p.PromotionEntries)
            {
                if (!LineTypes.Contains(promoEntry.PromotionType))
                {
                    continue;
                }
                else if (_promotionsServices.PromotionEntryMet(promoEntry, order))
                {
                    BaseOrderLine orderLine = order.Lines.Find(l => l.ProductGuid == promoEntry.Code);
                    double amtDiscount;
                    if (promoEntry.DiscountAmount > 0)
                    {
                        amtDiscount = promoEntry.DiscountAmount * orderLine.Quantity;
                    }
                    else if (promoEntry.DiscountPercentage > 0)
                    {
                        amtDiscount = (promoEntry.DiscountPercentage / 100) * orderLine.ListPrice * orderLine.Quantity;
                    }
                    else
                    {
                        amtDiscount = 0;
                    }
                    if (promoEntry.MaxDiscountAmount > 0) // don't exceed the maximum discount
                    {
                        amtDiscount = Math.Min(promoEntry.MaxDiscountAmount, amtDiscount);
                    }
                    //don't exceed the total cost of the item
                    double flatAmt = Math.Min(amtDiscount, orderLine.ListPrice * orderLine.Quantity);
                    //orderLine.LineDiscountAmount = Math.Min(amtDiscount, orderLine.ListPrice * orderLine.Quantity);
                    ((LineParameter)orderLine.LineParameter).LineDiscountPct = flatAmt / (orderLine.ListPrice * orderLine.Quantity) * 100;
                }
            }
            //Check for any Lines that are added as Promotional Gifts, and set their discounts to be 100% of their list price
            foreach (T Line in order.Lines)
            {
                if (Line.PromotionalGift)
                {
                    //Line.LineDiscountAmount = Line.ListPrice * Line.Quantity;
                    ((LineParameter)Line.LineParameter).LineDiscountPct = 100;
                }
            }
        }


        private void PreCalculateShipping<T>(BaseOrderHeader<T> order, string currency, string defaultCurrency) where T: BaseOrderLine
        {
            var shippingHandlingPrices = _shippingHandlingPriceRepository.GetShippingHandlingPrice(order.ShippingHandlingProviderGuid);
            double sum = order.Lines.Sum(x => x.TotalInclTax) - order.InvoiceDiscount;

            if (shippingHandlingPrices == null || shippingHandlingPrices.Count == 0)
            {
                order.ShippingAmount = 0;
                order.HandlingAmount = 0;
            }
            else
            {
                int i = -1;
                do
                {
                    i++;
                    order.HandlingAmount = Math.Round(_currencyConverter.ConvertCurrency(shippingHandlingPrices[i].HandlingAmount, defaultCurrency, currency), 2);
                    order.ShippingAmount = Math.Round(_currencyConverter.ConvertCurrency(shippingHandlingPrices[i].ShippingAmount, defaultCurrency, currency), 2);
                    if (_configurationObject.Read("USE_USPROMOS") == "TRUE")
                    {
                        PromoShippingDiscounts(order, defaultCurrency);
                    }
                    order.ShippingAmountInclTax = Math.Round(order.ShippingAmount * (1 + (ExpanditLib2.ConvertToDbl(_configurationObject.Read("SHIPPING_TAX_PCT"))) / 100), 2);
                    order.HandlingAmountInclTax = Math.Round(order.HandlingAmount * (1 + (ExpanditLib2.ConvertToDbl(_configurationObject.Read("TAX_PCT"))) / 100), 2);
                } while (i <= shippingHandlingPrices.Count - 1 && sum > Math.Round(_currencyConverter.ConvertCurrency(ExpanditLib2.ConvertToDbl(shippingHandlingPrices[i].CalculatedValue), defaultCurrency, currency), 2));
            }
        }

        private string GetCustomerGuid(string userGuid)
        {
            var userTable = _expanditUserService.GetUser(userGuid);

            if (userTable == null)
            {
                return _configurationObject.Read("ANONYMOUS_CUSTOMERGUID");
            }

            return string.IsNullOrEmpty(userTable.CustomerGuid) ? _configurationObject.Read("ANONYMOUS_CUSTOMERGUID") : userTable.CustomerGuid;
        }

        /*
         * Header Methods
         */

        /// <summary>
        /// Read information from CustomerTable
        /// </summary>
        /// <param name="order"> </param>
        /// <param name="customerGuid"></param>
        /// <param name="currencyGuid"> </param>
        /// <returns></returns>
        public void InitOrder<T>(BaseOrderHeader<T> order, string customerGuid, string currencyGuid) where T : BaseOrderLine
        {
            CustomerTableNav customerTable = _dataService.SelectFromCustomerTable(customerGuid);

            if (customerTable == null)
            {
                throw new Exception(string.Format("CUSTOMER {0} DOES NOT EXIST", customerGuid));
            }

            HeaderParameter headerParameter = new HeaderParameter();

            headerParameter.CustomerGuid = customerTable.CustomerGuid;
            headerParameter.CustomerDiscountGroup = customerTable.CustomerDiscountGroup;
            headerParameter.AllowLineDisc = customerTable.AllowLineDisc;
            headerParameter.PriceGroupCode = customerTable.PriceGroupCode;
            headerParameter.InvoiceDiscountCode = customerTable.InvoiceDiscountCode;
            headerParameter.VATBusinessPostingGroup = customerTable.VATBusinessPostingGroup;
            headerParameter.CurrencyGuid =
                !ExpanditLib2.CBoolEx(_configurationObject.Read("ATTAIN_USE_CURRENCY_FROM_BILLTO_CUSTOMER")) ? currencyGuid : customerTable.CurrencyGuid;

            // Set the additional order header parameter with initial values here
            order.HeaderParameter = headerParameter;

            order.PricesIncludingVat = customerTable.PricesIncludingVat;
        }


        /* 
         *  2 (header)
         */

        public void SubTotal<T>(BaseOrderHeader<T> order, string customerGuid, string currencyGuid) where T : BaseOrderLine
        {
            double amountSum = 0;
            double invoiceDiscountSum = 0;

            foreach (var line in order.Lines)
            {
                if (((LineParameter)line.LineParameter).ListPriceIsInclTax)
                {
                    amountSum += line.LineTotal / (1 + Convert.ToDouble(line.TaxPct) / 100);
                    if (((LineParameter)line.LineParameter).AllowInvoiceDiscount)
                    {
                        invoiceDiscountSum += line.LineTotal / (1 + Convert.ToDouble(line.TaxPct) / 100);
                    }
                }
                else
                {
                    amountSum += line.LineTotal;
                    if (((LineParameter)line.LineParameter).AllowInvoiceDiscount)
                    {
                        invoiceDiscountSum += line.LineTotal;
                    }
                }
            }

            order.SubTotal = amountSum;
            ((HeaderParameter)order.HeaderParameter).SubTotal_InvoiceDiscount = invoiceDiscountSum;
            ((HeaderParameter)order.HeaderParameter).SubTotal_ServiceCharge = amountSum + order.ShippingAmount +
                                                                               order.HandlingAmount;
            order.Total = amountSum;
        }

        /*
         *  3 (Header)
         */

        public void InvoiceDiscounts<T>(BaseOrderHeader<T> order, string customerGuid, string currencyGuid) where T : BaseOrderLine
        {
            //  Calculation of Service chage and invoice discount.
            //  Invoice discount should be calculated on SubTotal_InvoiceDiscount (Found in Function NFSubTotal)
            //  Service Charge should be calculated on SubTotal - not considering if invoice dicounts are allowed or not.

            double discountPct = 0;
            double serviceCharge = 0;
            bool isDiscountFound = false;
            bool isServiceChargeFound = false;
            bool isSameCurrency = false;

            List<Attain_CustomerInvoiceDiscount> customerInvoiceDiscounts =
                _dataService.SelectFromAttainCustomerInvoiceDiscount(
                    ((HeaderParameter)order.HeaderParameter).InvoiceDiscountCode, order.CurrencyGuid);

            if (customerInvoiceDiscounts != null)
            {
                // Try with the same currency
                List<Attain_CustomerInvoiceDiscount> discounts = customerInvoiceDiscounts.FindAll(disc => disc.CurrencyCode == order.CurrencyGuid);

                if (discounts.Count > 0)
                {
                    isSameCurrency = true;
                }

                foreach (var discount in discounts)
                {
                    if (!isDiscountFound && ((HeaderParameter)order.HeaderParameter).SubTotal_InvoiceDiscount >= discount.MinimumAmount)
                    {
                        isDiscountFound = true;
                        discountPct = discount.DiscountPct;
                    }

                    if (!isServiceChargeFound && ((HeaderParameter)order.HeaderParameter).SubTotal_ServiceCharge >= discount.MinimumAmount)
                    {
                        isServiceChargeFound = true;
                        serviceCharge = discount.ServiceCharge;
                    }

                    if (isDiscountFound && isServiceChargeFound)
                    {
                        break;
                    }
                }

                if (!isSameCurrency)
                {
                    // LOOK FOR THE EMPTY CURRENCY
                    discounts = customerInvoiceDiscounts.FindAll(disc => disc.CurrencyCode == "");
                    foreach (var discount in discounts)
                    {
                        double tmpInvSubTotal =
                            _currencyConverter.ConvertCurrency(
                                ((HeaderParameter)order.HeaderParameter).SubTotal_InvoiceDiscount, order.CurrencyGuid, "");
                        double tmpSubTotal =
                            _currencyConverter.ConvertCurrency(
                                ((HeaderParameter)order.HeaderParameter).SubTotal_ServiceCharge, order.CurrencyGuid, "");

                        if (!isDiscountFound && tmpInvSubTotal >= discount.MinimumAmount)
                        {
                            isDiscountFound = true;
                            discountPct = discount.DiscountPct;
                        }

                        if (!isServiceChargeFound && tmpSubTotal >= discount.MinimumAmount)
                        {
                            isServiceChargeFound = true;
                            serviceCharge = _currencyConverter.ConvertCurrency(discount.ServiceCharge, "",
                                                                               order.CurrencyGuid);
                        }
                        if (isDiscountFound && isServiceChargeFound)
                        {
                            break;
                        }
                    }
                }
            }

            double discPct = ExpanditLib2.RoundEx(discountPct, 5);
            if (_configurationObject.Read("USE_USPROMOS") == "TRUE")
            {
                discPct += PromoInvoiceDiscounts(order, currencyGuid);
            }

            order.InvoiceDiscountPct = discPct.ToString();
            order.ServiceCharge = ExpanditLib2.RoundEx(serviceCharge, 2);
            order.InvoiceDiscount = Math.Min(order.SubTotal, Math.Round(((HeaderParameter)order.HeaderParameter).SubTotal_InvoiceDiscount * discPct / 100, 2));
            order.Total = order.SubTotal + order.ServiceCharge - order.InvoiceDiscount;
        }

        /*
         *  4 (Header)
         */

        public void CalculateVat<T>(BaseOrderHeader<T> order, string customerGuid, string currencyGuid) where T : BaseOrderLine
        {
            double vatPct;
            decimal invoiceDiscountAmountInclTaxSum = 0;
            decimal totalInclTaxSum = 0;
            decimal totalSum = 0;
            decimal invoiceDiscountAmountSum = 0;
            Dictionary<double, VatNode> vatSums = new Dictionary<double, VatNode>();
            decimal subTotalSum = 0;
            decimal subTotalInclVatSum = 0;
            decimal lineDiscountAmountExclTaxSum = 0;
            decimal lineDiscountAmountInclTaxSum = 0;

            foreach (var line in order.Lines)
            {
                decimal orderLineListPriceInclTax;
                decimal orderLineListPriceExclTax;
                decimal orderLineSubTotalInclTax;
                decimal orderLineSubTotalExclTax;
                decimal orderLineTotalInclTax;
                decimal orderLineTotalExclTax;
                decimal orderLineLineDiscountAmountInclTax;
                decimal orderLineLineDiscountAmountExclTax;
                decimal orderLineInvoiceDiscountAmountInclTax = 0;
                decimal orderLineInvoiceDiscountAmountExclTax = 0;
                vatPct = 0;

                // Reusing the sql result collected in the OrderLineTotal method
                if (_vatPctInfos != null)
                {
                    if (_vatPctInfos.Exists(vat => vat.ProductGuid == line.ProductGuid))
                    {
                        var firstOrDefault = _vatPctInfos.FirstOrDefault(x => x.ProductGuid == line.ProductGuid);
                        if (firstOrDefault != null)
                        {
                            vatPct = firstOrDefault.VATPct;
                        }
                    }
                }

                if (((LineParameter)line.LineParameter).ListPriceIsInclTax)
                {
                    orderLineListPriceInclTax = (decimal)line.ListPrice;
                    orderLineListPriceExclTax = (decimal)(line.ListPrice * (1 / (1 + vatPct / 100)));
                    orderLineSubTotalInclTax = (decimal)((LineParameter)line.LineParameter).SubTotal;
                    orderLineSubTotalExclTax = (decimal)(((LineParameter)line.LineParameter).SubTotal * (1 / (1 + vatPct / 100)));
                    orderLineTotalInclTax = (decimal)line.LineTotal;
                    orderLineTotalExclTax = (decimal)(line.LineTotal * (1 / (1 + vatPct / 100)));
                    orderLineLineDiscountAmountInclTax = (decimal)line.LineDiscountAmount;
                    orderLineLineDiscountAmountExclTax = (decimal)(line.LineDiscountAmount * (1 / (1 + vatPct / 100)));
                }
                else
                {
                    orderLineListPriceInclTax = (decimal)(line.ListPrice * (1 + vatPct / 100));
                    orderLineListPriceExclTax = (decimal)line.ListPrice;
                    orderLineSubTotalInclTax = (decimal)(((LineParameter)line.LineParameter).SubTotal * (1 + vatPct / 100));
                    orderLineSubTotalExclTax = (decimal)((LineParameter)line.LineParameter).SubTotal;
                    orderLineTotalInclTax = (decimal)(line.LineTotal * (1 + vatPct / 100));
                    orderLineTotalExclTax = (decimal)line.LineTotal;
                    orderLineLineDiscountAmountInclTax = (decimal)(line.LineDiscountAmount * (1 + vatPct / 100));
                    orderLineLineDiscountAmountExclTax = (decimal)line.LineDiscountAmount;
                }

                //Special case when customer prices is incl VAT - needs different calculation of line discounts
                if (!((LineParameter)line.LineParameter).ListPriceIsInclTax)
                {
                    if (ExpanditLib2.CBoolEx(order.PricesIncludingVat))
                    {
                        orderLineLineDiscountAmountInclTax = (decimal)line.LineDiscountAmountInclTax;
                        orderLineTotalInclTax = (decimal)line.TotalInclTax;
                    }
                }

                if (((LineParameter)line.LineParameter).AllowInvoiceDiscount)
                {
                    orderLineInvoiceDiscountAmountInclTax = orderLineTotalInclTax * (ExpanditLib2.CDblEx(order.InvoiceDiscountPct) / 100);
                    orderLineInvoiceDiscountAmountExclTax = orderLineTotalExclTax * (ExpanditLib2.CDblEx(order.InvoiceDiscountPct) / 100);
                }

                decimal orderLineTaxAmount = orderLineTotalExclTax * (decimal)(vatPct / 100);

                // Add the correct values to the dictionary

                line.ListPrice = (double)ExpanditLib2.RoundEx(orderLineListPriceExclTax, 5);
                line.ListPriceInclTax = (double)ExpanditLib2.RoundEx(orderLineListPriceInclTax, 5);
                ((LineParameter)line.LineParameter).SubTotal = (double)ExpanditLib2.RoundEx(orderLineSubTotalExclTax, 5);
                ((LineParameter)line.LineParameter).SubTotalInclTax = (double)ExpanditLib2.RoundEx(orderLineSubTotalInclTax, 5);
                line.LineTotal = (double)ExpanditLib2.RoundEx(orderLineTotalExclTax, 5);
                line.TotalInclTax = (double)ExpanditLib2.RoundEx(orderLineTotalInclTax, 5);
                line.LineDiscountAmount = (double)ExpanditLib2.RoundEx(orderLineLineDiscountAmountExclTax, 5);
                line.LineDiscountAmountInclTax = (double)ExpanditLib2.RoundEx(orderLineLineDiscountAmountInclTax, 5);
                ((LineParameter)line.LineParameter).InvoiceDiscountAmount = ExpanditLib2.RoundEx(orderLineInvoiceDiscountAmountExclTax, 5);
                ((LineParameter)line.LineParameter).InvoiceDiscountAmountInclTax = ExpanditLib2.RoundEx(orderLineInvoiceDiscountAmountInclTax, 5);
                line.TaxAmount = (double)ExpanditLib2.RoundEx(orderLineTaxAmount, 5);

                //_______________________________________________TRY 5 instead of 2 decimals from here on__________________________________________________________________

                if (order.PricesIncludingVat)
                {
                    if (vatSums.ContainsKey(vatPct))
                    {
                        vatSums[vatPct].VatSum = vatSums[vatPct].VatSum + ExpanditLib2.RoundEx(orderLineTotalInclTax, 5);
                        vatSums[vatPct].InvoiceDiscountAmountSumExclVAT = vatSums[vatPct].InvoiceDiscountAmountSumExclVAT + orderLineInvoiceDiscountAmountExclTax;
                        vatSums[vatPct].InvoiceDiscountAmountSumInclVAT = vatSums[vatPct].InvoiceDiscountAmountSumInclVAT + orderLineInvoiceDiscountAmountInclTax;
                    }
                    else
                    {
                        VatNode node = new VatNode();
                        node.VatSum = ExpanditLib2.RoundEx(orderLineTotalInclTax, 5);
                        node.InvoiceDiscountAmountSumExclVAT = orderLineInvoiceDiscountAmountExclTax;
                        node.InvoiceDiscountAmountSumInclVAT = orderLineInvoiceDiscountAmountInclTax;
                        vatSums.Add(vatPct, node);
                    }
                }
                else
                {
                    if (vatSums.ContainsKey(vatPct))
                    {
                        vatSums[vatPct].VatSum = vatSums[vatPct].VatSum + ExpanditLib2.RoundEx(orderLineTotalExclTax, 5);
                        vatSums[vatPct].InvoiceDiscountAmountSumExclVAT = vatSums[vatPct].InvoiceDiscountAmountSumExclVAT + orderLineInvoiceDiscountAmountExclTax;
                        vatSums[vatPct].InvoiceDiscountAmountSumInclVAT = vatSums[vatPct].InvoiceDiscountAmountSumInclVAT + orderLineInvoiceDiscountAmountInclTax;
                    }
                    else
                    {
                        VatNode node = new VatNode();
                        node.VatSum = ExpanditLib2.RoundEx(orderLineTotalExclTax, 5);
                        node.InvoiceDiscountAmountSumExclVAT = orderLineInvoiceDiscountAmountExclTax;
                        node.InvoiceDiscountAmountSumInclVAT = orderLineInvoiceDiscountAmountInclTax;
                        vatSums.Add(vatPct, node);
                    }
                }

                invoiceDiscountAmountSum = invoiceDiscountAmountSum + orderLineInvoiceDiscountAmountExclTax;
                invoiceDiscountAmountInclTaxSum = invoiceDiscountAmountInclTaxSum + orderLineInvoiceDiscountAmountInclTax;
                lineDiscountAmountInclTaxSum = lineDiscountAmountInclTaxSum + ExpanditLib2.RoundEx(orderLineLineDiscountAmountInclTax, 5);
                lineDiscountAmountExclTaxSum = lineDiscountAmountExclTaxSum + ExpanditLib2.RoundEx(orderLineLineDiscountAmountExclTax, 5);
                subTotalSum = subTotalSum + ExpanditLib2.RoundEx(orderLineSubTotalExclTax, 5);
                subTotalInclVatSum = subTotalInclVatSum + ExpanditLib2.RoundEx(orderLineSubTotalInclTax, 5);
                totalSum = totalSum + ExpanditLib2.RoundEx(orderLineTotalExclTax, 5);
                totalInclTaxSum = totalInclTaxSum + ExpanditLib2.RoundEx(orderLineTotalInclTax, 5);
                line.TaxPct = ExpanditLib2.CStrEx(vatPct);
            } // End foreach

            invoiceDiscountAmountSum = ExpanditLib2.RoundEx(invoiceDiscountAmountSum, 5);
            invoiceDiscountAmountInclTaxSum = ExpanditLib2.RoundEx(invoiceDiscountAmountInclTaxSum, 5);

            decimal vaTexDiscountSum = 0;
            decimal VATSum = 0;

            ExpDictionary calculatedVatValuesDict = new ExpDictionary();
            foreach (double vatType in vatSums.Keys)
            {
                decimal vaTxSum;
                decimal vaTexDiscount;
                if (order.PricesIncludingVat)
                {
                    vaTxSum = ExpanditLib2.RoundEx(vatSums[vatType].VatSum, 5);
                    vaTexDiscount = ExpanditLib2.RoundEx(vaTxSum * (decimal)(1 - (1 / (1 + vatType / 100))), 5);
                    vaTxSum = ExpanditLib2.RoundEx((vaTxSum - ExpanditLib2.RoundEx(vatSums[vatType].InvoiceDiscountAmountSumInclVAT, 5)) * (decimal)(1 - (1 / (1 + vatType / 100))), 5);
                    VATSum = VATSum + vaTxSum;
                }
                else
                {
                    vaTxSum = ExpanditLib2.RoundEx(vatSums[vatType].VatSum, 5);
                    vaTexDiscount = vaTxSum * (decimal)(vatType / 100);
                    vaTxSum = ExpanditLib2.RoundEx((vaTxSum - ExpanditLib2.RoundEx(vatSums[vatType].InvoiceDiscountAmountSumExclVAT, 5)) * (decimal)(vatType / 100), 5);
                    VATSum = VATSum + vaTxSum;
                }
                if (!string.IsNullOrEmpty(order.HeaderGuid))
                {
                    if (!calculatedVatValuesDict.ContainsKey("VatTypes"))
                    {
                        calculatedVatValuesDict["VatTypes"] = new ExpDictionary();
                    }
                    ExpDictionary x = (ExpDictionary)calculatedVatValuesDict["VatTypes"];
                    if (x.ContainsKey(vatType))
                    {
                        x[vatType] = vaTxSum;

                    }
                    else
                    {
                        x.Add(vatType, vaTxSum);
                    }
                }
                vaTexDiscountSum = vaTexDiscountSum + vaTexDiscount;
            }

            if (order.PricesIncludingVat)
            {
                totalInclTaxSum = (totalInclTaxSum - invoiceDiscountAmountInclTaxSum);
            }
            else
            {
                totalInclTaxSum = (totalSum - invoiceDiscountAmountSum) + VATSum;
                subTotalInclVatSum = ExpanditLib2.RoundEx(subTotalSum + vaTexDiscountSum, 5);
            }

            // --
            // Service Charge calculations. Lookup the tax pct for service charge according to cfg.
            // --

            var postingSetup =
                _dataService.GetVatPctFromAttainVatPostingSetup(
                    ((HeaderParameter)order.HeaderParameter).VATBusinessPostingGroup,
                    _configurationObject.Read("ATTAIN_SERVICECHARGE_PRODUCTPOSTINGGROUP"));

            vatPct = postingSetup != null ? ExpanditLib2.ConvertToDbl(postingSetup.VATPct) : 0;

            // Set the VATPct in the orderobject for the Shipping and Handling calculations.
            order.TaxPct = ExpanditLib2.CStrEx(vatPct);

            // Calculate the VAT on the Service Charge, if any
            order.ServiceChargeInclTax = order.ServiceCharge * (1 + vatPct / 100);

            VATSum = VATSum + (decimal)(order.ServiceCharge * vatPct / 100);

            // Tax fields
            order.TaxAmount = (double)VATSum;

            order.InvoiceDiscount = (double)invoiceDiscountAmountSum;

            order.InvoiceDiscountInclTax = (double)invoiceDiscountAmountInclTaxSum;
            order.SubTotalInclTax = (double)(subTotalInclVatSum - lineDiscountAmountInclTaxSum);

            if (order.PricesIncludingVat)
            {
                order.SubTotal = (double)(subTotalInclVatSum - vaTexDiscountSum - lineDiscountAmountInclTaxSum);
                order.Total = (double)(totalInclTaxSum + ExpanditLib2.CDblEx(order.ServiceChargeInclTax)) - (double)VATSum;
            }
            else
            {
                order.SubTotal = (double)(subTotalSum - lineDiscountAmountExclTaxSum);
                order.Total = (double)(totalSum + ExpanditLib2.CDblEx(order.ServiceCharge) - invoiceDiscountAmountSum);
            }

            order.TotalInclTax = (double)(totalInclTaxSum + ExpanditLib2.CDblEx(order.ServiceChargeInclTax));

            if (!string.IsNullOrEmpty(order.HeaderGuid))
            {
                order.VatTypes = ExpandIT.Serialization.Marshall.MarshallDictionary((ExpDictionary)calculatedVatValuesDict["VatTypes"]);
            }

        }

        /*
         * Line Methods
         */


        /// <summary>
        /// Get information from the productTable
        /// Store result in _orderLineParameters
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="order"></param>
        /// <param name="defaultCurrency"></param>
        public void StandardPrice<T>(BaseOrderHeader<T> order, string defaultCurrency) where T : BaseOrderLine
        {
            List<string> guids = order.Lines.Select(line => "'" + line.ProductGuid + "'").ToList();
            string productGuids = string.Join(",", guids);

            _productInListParameter = productGuids;

            List<NavProductTable> navProduct = _dataService.SelectStandardPrice(productGuids);

            foreach (var line in order.Lines)
            {
                var productTable = navProduct.Find(x => x.ProductGuid == line.ProductGuid);
                if (productTable == null)
                {
                    order.Lines.Remove(line);
                    continue;
                }
                line.ListPrice = productTable.ListPrice;
                line.CurrencyGuid = defaultCurrency;

                var lineParameters = new LineParameter();
                // AllowLineDisc follows the customer, but this value might be overruled by AllowLineDiscount from Attain_ProductPrice found in the ListPrice method.
                lineParameters.AllowLineDisc = ((HeaderParameter)order.HeaderParameter).AllowLineDisc;
                lineParameters.AllowInvoiceDiscount = productTable.AllowInvoiceDiscount;
                lineParameters.ItemCustomerDiscountGroup = productTable.ItemCustomerDiscountGroup;
                lineParameters.VATProductPostingGroup = productTable.VATProductPostingGroup;
                lineParameters.ListPriceIsInclTax = productTable.PriceIncludesVAT;

                line.LineParameter = lineParameters; // Set the parameter object with initial values here
            }

            List<string> discountGroupList = order.Lines.Select(line => "'" + ((LineParameter)line.LineParameter).ItemCustomerDiscountGroup + "'").ToList();

            _itemCustomerDiscountGrouplist = string.Join(",", discountGroupList);

            if (_itemCustomerDiscountGrouplist.Replace("'", "").Length == 0)
            {
                _itemCustomerDiscountGrouplist = string.Empty;
            }
        }

        public void ListPrice<T>(BaseOrderHeader<T> order, string defaultCurrency) where T : BaseOrderLine
        {
            // Header conditions: IN ProductGuidList, CustomerRelType, CustomerRelGuid, StartingDate, EndingDate, CurrencyGuid
            // Line conditions: ProductGuid, UOMGuid, VariantCode, MinimumQuantity

            // ProdcutGuid      IN ProductList
            // CustomerRetType  Customer, Group, All
            // CustomerRetGuid  (CustomerRelType == Customer AND CustomerRelGuid == User("CustomerGuid")) OR
            //                  (CustomerRelType == CustomerGrp AND CustomerRelGuid == User("CustomerGuid")) OR
            //                  (CustomerRelType == All)
            // StartingDate        (NOW > StartingDate AND Now < EndingDate)
            // EndingDate
            // CurrencyGuid        CurrencyGuid == User("CurrencyGuid") (If Blank CurrencyGuid == Default Currency guid - Take into consideration
            // VariantCode      VarinatCode = Item("VarinatCode") or not used
            // UOMGuid          UOMGuid = Item(UnitOfMeasureGuid)
            // MinimumQuantity  OrderLine("Qunatity") >= MinimumQuantity

            // If one or more prices is found in the customers currency (different from default currency) then the best currency price is always used.

            List<string> guids = order.Lines.Select(line => "'" + line.ProductGuid + "'").ToList();
            string productGuids = string.Join(",", guids);

            List<Attain_ProductPrice> attainProductPrices = _dataService.SelectListPrice(productGuids, ((HeaderParameter)order.HeaderParameter).CustomerGuid,
                                                                                         ((HeaderParameter)order.HeaderParameter).PriceGroupCode,
                                                                                         ((HeaderParameter)order.HeaderParameter).CurrencyGuid, defaultCurrency);

            if (attainProductPrices == null)
            {
                return;
            }

            foreach (var line in order.Lines)
            {
                var attainProductPrice = attainProductPrices.FindAll(x => x.ProductGuid == line.ProductGuid && (string.IsNullOrEmpty(x.VariantCode) || x.VariantCode == line.VariantCode));
                if (attainProductPrice.IsNullOrEmpty()) { continue; }

                Attain_ProductPrice BestSalesPrice = new Attain_ProductPrice();

                foreach (Attain_ProductPrice SalesPrice in attainProductPrice)
                {
                    double productTotalQuantity;
                    double variantTotalQuantity;
                    
                    if (_isCollectQuantitiesForLineDiscount)
                    {
                        productTotalQuantity = ProductQuantities.GetProductQuantities(line.ProductGuid);
                        if (string.IsNullOrEmpty(line.VariantCode))
                        {
                            variantTotalQuantity = line.Quantity;
                        }
                        else
                        {
                            variantTotalQuantity = ProductQuantities.GetVariantQuantities(line.ProductGuid, line.VariantCode);
                        }
                    }
                    else
                    {
                        productTotalQuantity = line.Quantity;
                        variantTotalQuantity = line.Quantity;
                    }

                    // Check for MinimumQuantity
                    if ((productTotalQuantity >= SalesPrice.MinimumQuantity) && string.IsNullOrEmpty(SalesPrice.VariantCode) ||
                        (!string.IsNullOrEmpty(line.VariantCode) && (variantTotalQuantity >= SalesPrice.MinimumQuantity) && (SalesPrice.VariantCode == line.VariantCode)))
                    {
                        if ((string.IsNullOrEmpty(BestSalesPrice.CurrencyGuid) && !string.IsNullOrEmpty(SalesPrice.CurrencyGuid)) ||
                            (string.IsNullOrEmpty(BestSalesPrice.VariantCode) && !string.IsNullOrEmpty(SalesPrice.VariantCode)))
                        {
                            // More specific price
                            BestSalesPrice = SalesPrice;
                        } 
                        else if ((string.IsNullOrEmpty(BestSalesPrice.CurrencyGuid) || !string.IsNullOrEmpty(SalesPrice.CurrencyGuid)) &&
                            (string.IsNullOrEmpty(BestSalesPrice.VariantCode) || !string.IsNullOrEmpty(SalesPrice.VariantCode)))
                        {
                            // Equally specific price - compare prices
                            if ((BestSalesPrice.UnitPrice == 0) || (CalcSalesPriceWithLineDisc(BestSalesPrice, ((LineParameter)line.LineParameter).LineDiscountPct) > CalcSalesPriceWithLineDisc(SalesPrice, ((LineParameter)line.LineParameter).LineDiscountPct)))
                            {
                                BestSalesPrice = SalesPrice;
                            }
                        }
                    }
                }

                if (BestSalesPrice.UnitPrice == 0) continue;

                line.ListPrice = BestSalesPrice.UnitPrice;
                ((LineParameter)line.LineParameter).ListPriceIsInclTax = BestSalesPrice.PriceInclTax;
                ((LineParameter)line.LineParameter).AllowLineDisc = BestSalesPrice.AllowLineDiscount;
                ((LineParameter)line.LineParameter).AllowInvoiceDiscount = BestSalesPrice.AllowInvoiceDiscount;
                ((LineParameter)line.LineParameter).Attain_VATBusPostingGrPrice = BestSalesPrice.Attain_VATBusPostingGrPrice;
                line.CurrencyGuid = BestSalesPrice.CurrencyGuid;
            } // End foreach
        }

        private double CalcSalesPriceWithLineDisc(Attain_ProductPrice SalesPrice, double LineDiscountPct)
        {
            if (SalesPrice.AllowLineDiscount)
            {
                return SalesPrice.UnitPrice * (1 - LineDiscountPct / 100);
            }
            else
            {
                return SalesPrice.UnitPrice;
            }
        }

        public void ItemPriceFixCurrency<T>(BaseOrderHeader<T> order, string defaultCurrency) where T : BaseOrderLine
        {
            foreach (var line in order.Lines.Where(line => line.CurrencyGuid != order.CurrencyGuid))
            {
                line.ListPrice = Convert.ToDouble(_currencyConverter.ConvertCurrency(line.ListPrice, line.CurrencyGuid, order.CurrencyGuid));
                line.CurrencyGuid = order.CurrencyGuid;
            }
        }
        
        public void LineDiscount<T>(BaseOrderHeader<T> order, string defaultCurrency) where T : BaseOrderLine
        {
            // Line Discount
            // Header conditions: CurrencyCode, StartingDate, EndingDate, SalesType, SalesCode
            // Line conditions: (Type = 0)ItemNo OR (Type=1)ItemDiscGroup OR (Type=2), MinimumQuantity, VariantCode

            List<Attain_SalesLineDiscount> salesLineDiscounts =
                _dataService.SelectFromAttainSalesLineDiscount(_productInListParameter, _itemCustomerDiscountGrouplist,
                                                               ((HeaderParameter)order.HeaderParameter).CustomerGuid,
                                                               ((HeaderParameter)order.HeaderParameter).CustomerDiscountGroup,
                                                               order.CurrencyGuid,
                                                               defaultCurrency);

            if (salesLineDiscounts == null)
            {
                return;
            }

            foreach (var line in order.Lines)
            {

                double discPct = 0;
                double discPctC = 0;
                bool isCurrencyDiscount = false;
                double productTotalQuantity;
                double variantTotalQuantity;
                double itemMinimumQuantity;
                double itemLineDiscountPct;
                string itemVariantCode;
                string productVariantCode;

                // Look for products
                var salesLineDiscountProducts = salesLineDiscounts.FindAll(x => x.Code == line.ProductGuid && x.Type == 0);

                if (!salesLineDiscountProducts.IsNullOrEmpty())
                {
                    foreach (var salesLineDiscount in salesLineDiscountProducts)
                    {
                        if (salesLineDiscount != null)
                        {
                            if (_isCollectQuantitiesForLineDiscount)
                            {
                                productTotalQuantity = ProductQuantities.GetProductQuantities(line.ProductGuid);
                            }
                            else
                            {
                                productTotalQuantity = line.Quantity;
                            }

                            itemMinimumQuantity = salesLineDiscount.MinimumQuantity;
                            itemVariantCode = salesLineDiscount.VariantCode;
                            itemLineDiscountPct = salesLineDiscount.LineDiscountPct;
                            productVariantCode = line.VariantCode;

                            isCurrencyDiscount = salesLineDiscount.CurrencyCode == order.CurrencyGuid &&
                                                 salesLineDiscount.CurrencyCode != defaultCurrency;

                            if (string.IsNullOrEmpty(productVariantCode))
                            {
                                if ((productTotalQuantity >= itemMinimumQuantity) && string.IsNullOrEmpty(itemVariantCode))
                                {
                                    if (isCurrencyDiscount)
                                    {
                                        if (itemLineDiscountPct > discPctC)
                                        {
                                            discPctC = salesLineDiscount.LineDiscountPct;
                                        }
                                    }
                                    else
                                    {
                                        if (itemLineDiscountPct > discPct)
                                        {
                                            discPct = salesLineDiscount.LineDiscountPct;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if (_isCollectQuantitiesForLineDiscount)
                                {
                                    variantTotalQuantity = ProductQuantities.GetVariantQuantities(line.ProductGuid, line.VariantCode);
                                }
                                else
                                {
                                    variantTotalQuantity = line.Quantity;
                                }
                                if (((productTotalQuantity >= itemMinimumQuantity) && itemVariantCode == "") ||
                                    ((variantTotalQuantity >= itemMinimumQuantity) && (itemVariantCode == productVariantCode)))
                                {
                                    if (isCurrencyDiscount)
                                    {
                                        if (itemLineDiscountPct > discPctC)
                                        {
                                            discPctC = salesLineDiscount.LineDiscountPct;
                                        }
                                    }
                                    else
                                    {
                                        if (itemLineDiscountPct > discPct)
                                        {
                                            discPct = salesLineDiscount.LineDiscountPct;
                                        }
                                    }
                                }
                            }

                        }
                    }
                }

                // Then look for groups
                var salesLineDiscountGroups = salesLineDiscounts.FindAll(x => x.Code == ((LineParameter)line.LineParameter).ItemCustomerDiscountGroup && x.Type == 1);

                if (!salesLineDiscountGroups.IsNullOrEmpty())
                {
                    foreach (var salesLineDiscountGroup in salesLineDiscountGroups)
                    {
                        if (salesLineDiscountGroup != null)
                        {
                            if (_isCollectQuantitiesForLineDiscount)
                            {
                                productTotalQuantity = ProductQuantities.GetProductQuantities(line.ProductGuid);
                            }
                            else
                            {
                                productTotalQuantity = line.Quantity;
                            }

                            itemMinimumQuantity = salesLineDiscountGroup.MinimumQuantity;
                            itemVariantCode = salesLineDiscountGroup.VariantCode;
                            itemLineDiscountPct = salesLineDiscountGroup.LineDiscountPct;
                            productVariantCode = line.VariantCode;

                            isCurrencyDiscount = salesLineDiscountGroup.CurrencyCode == order.CurrencyGuid &&
                                                 salesLineDiscountGroup.CurrencyCode != defaultCurrency;

                            if (string.IsNullOrEmpty(productVariantCode))
                            {
                                if ((productTotalQuantity >= itemMinimumQuantity) && string.IsNullOrEmpty(itemVariantCode))
                                {
                                    if (isCurrencyDiscount)
                                    {
                                        if (itemLineDiscountPct > discPctC)
                                        {
                                            discPctC = salesLineDiscountGroup.LineDiscountPct;
                                        }
                                    }
                                    else
                                    {
                                        if (itemLineDiscountPct > discPct)
                                        {
                                            discPct = salesLineDiscountGroup.LineDiscountPct;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if (_isCollectQuantitiesForLineDiscount)
                                {
                                    variantTotalQuantity = ProductQuantities.GetVariantQuantities(line.ProductGuid, line.VariantCode);
                                }
                                else
                                {
                                    variantTotalQuantity = line.Quantity;
                                }
                                if (((productTotalQuantity >= itemMinimumQuantity) && itemVariantCode == "") ||
                                    ((variantTotalQuantity >= itemMinimumQuantity) && (itemVariantCode == productVariantCode)))
                                {
                                    if (isCurrencyDiscount)
                                    {
                                        if (itemLineDiscountPct > discPctC)
                                        {
                                            discPctC = salesLineDiscountGroup.LineDiscountPct;
                                        }
                                    }
                                    else
                                    {
                                        if (itemLineDiscountPct > discPct)
                                        {
                                            discPct = salesLineDiscountGroup.LineDiscountPct;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                ((LineParameter)line.LineParameter).LineDiscountPct = discPctC > 0 ? discPctC : discPct;
            }
        }

        public void OrderLineTotal<T>(BaseOrderHeader<T> order, string defaultCurrency) where T : BaseOrderLine
        {
            //Needed for the customer prices incl VAT calculation          
            _vatPctInfos =
                _dataService.SelectFromProductTableJoinAttainVatPostingSetup(
                    ((HeaderParameter)order.HeaderParameter).VATBusinessPostingGroup, _productInListParameter);

            foreach (var line in order.Lines)
            {
                if (!((LineParameter)line.LineParameter).AllowLineDisc)
                {
                    ((LineParameter)line.LineParameter).LineDiscountPct = 0;
                }

                if (((LineParameter)line.LineParameter).LineDiscountPct > 100)
                {
                    ((LineParameter)line.LineParameter).LineDiscountPct = 100;
                }

                ((LineParameter)line.LineParameter).SubTotal = line.Quantity * line.ListPrice;

                line.LineDiscountAmount = ((LineParameter)line.LineParameter).SubTotal *
                                          ((LineParameter)line.LineParameter).LineDiscountPct / 100;

                line.LineDiscount = ((LineParameter)line.LineParameter).LineDiscountPct;

                double dec1 = ((LineParameter)line.LineParameter).SubTotal;
                double dec2 = line.LineDiscountAmount;
                line.LineTotal = ExpanditLib2.RoundEx(dec1, 2) - ExpanditLib2.RoundEx(dec2, 2);

                // -- Get VAT % - This is done again in when calculating the header, but we may need it when calulating Invoice Discount and Service Charge
                double vatPct = 0;

                if (_vatPctInfos != null)
                {
                    if (_vatPctInfos.Exists(vat => vat.ProductGuid == line.ProductGuid))
                    {
                        var firstOrDefault = _vatPctInfos.FirstOrDefault(vat => vat.ProductGuid == line.ProductGuid);
                        if (firstOrDefault != null)
                            vatPct = firstOrDefault.VATPct;
                    }
                }
                line.TaxPct = ExpanditLib2.CStrEx(vatPct);

                // -- TotalInclTax is needed for Shipping and Handling calculation, which may be called before the NFCalculateVat function that would
                // -- set the value. But this should be the correct value.
                // -- The value may be overridden below in special cases

                if (((LineParameter)line.LineParameter).ListPriceIsInclTax)
                {
                    line.TotalInclTax = line.LineTotal;
                }
                else
                {
                    line.TotalInclTax = line.LineTotal * (1 + vatPct / 100);
                }

                // -- If the listprice is not including VAT
                if (!((LineParameter)line.LineParameter).ListPriceIsInclTax)
                {
                    // -- If the customers prices is including VAT
                    if (order.PricesIncludingVat)
                    {
                        // -- Do a Navision style line amount and line discount calculation by recalculating the list price and line discount to include VAT, then
                        // do a rounding to two decimals and subtract the discount from the price.
                        ((LineParameter)line.LineParameter).SubTotalInclTax = line.Quantity * line.ListPrice *
                                                                               (1 + vatPct / 100);
                        line.LineDiscountAmountInclTax = ((LineParameter)line.LineParameter).SubTotalInclTax *
                                                         ((LineParameter)line.LineParameter).LineDiscountPct / 100;
                        double param1 = ((LineParameter)line.LineParameter).SubTotalInclTax;
                        double param2 = line.LineDiscountAmountInclTax;
                        line.TotalInclTax = ExpanditLib2.RoundEx(param1, 2) - ExpanditLib2.RoundEx(param2, 2);
                    }
                }
            }
        }
    }


    /// <summary>
    /// Internal class to hold parameters used in price calculations
    /// </summary>
    public class LineParameter : ILineParameter
    {
        // Additional parameters set in StandardPrice
        public bool AllowLineDisc { get; set; }
        public bool AllowInvoiceDiscount { get; set; }
        public string ItemCustomerDiscountGroup { get; set; }
        public string VATProductPostingGroup { get; set; }
        public bool ListPriceIsInclTax { get; set; }
        // Additional parameter set in ListPrice
        public string Attain_VATBusPostingGrPrice { get; set; }
        // Additional parameter set in LineDiscount
        public double LineDiscountPct { get; set; }
        // Additional parameters set in OrderLineTotal
        public double SubTotal { get; set; }
        public double SubTotalInclTax { get; set; }
        public double OrderLineTotal { get; set; }
        // Additional parameters set in CalculateVat
        public decimal InvoiceDiscountAmount { get; set; }
        public decimal InvoiceDiscountAmountInclTax { get; set; }

        // A list of identifiers used in "IN" SQL statements
        public string ItemCustomerDiscountGroupList { get; set; }
    }

    /// <summary>
    /// Internal class to hold parameters used in price calculations
    /// </summary>
    public class HeaderParameter : IHeaderParameter
    {
        public bool AllowLineDisc { get; set; }
        public string CustomerGuid { get; set; }
        public string CustomerDiscountGroup { get; set; }
        public string PriceGroupCode { get; set; }
        public string InvoiceDiscountCode { get; set; }
        public string VATBusinessPostingGroup { get; set; }
        public string CurrencyGuid { get; set; }
        public double SubTotal_InvoiceDiscount { get; set; }
        public double SubTotal_ServiceCharge { get; set; }
    }

    public class VatNode
    {
        public decimal VatSum { get; set; }
        public decimal InvoiceDiscountAmountSumExclVAT { get; set; }
        public decimal InvoiceDiscountAmountSumInclVAT { get; set; }
    }
}
