﻿using System;
using System.Collections.Generic;
using System.Linq;
using CmsPublic.DataRepository;
using EISCS.Shop.DO.Dto.Backend.Navision;
using EISCS.Shop.DO.Dto.Backend.Navision.Nav;
using EISCS.Shop.DO.Interface;

namespace EISCS.Shop.BO.BusinessLogic.Navision.Nav
{
    public interface INavDataService
    {
        CustomerTableNav SelectFromCustomerTable(string customerGuid);

        List<Attain_CustomerInvoiceDiscount> SelectFromAttainCustomerInvoiceDiscount(string invoiceDiscountCode,
            string currencyGuid);

        Attain_VATPostingSetup GetVatPctFromAttainVatPostingSetup(string businessPostingGroup, string productPostingGroup);
        List<NavProductTable> SelectStandardPrice(string productGuids);

        List<Attain_ProductPrice> SelectListPrice(string productGuids, string customerGuid, string priceGroupCode,
            string customerCurrencyGuid, string defaultCurrency);

        List<Attain_SalesLineDiscount> SelectFromAttainSalesLineDiscount(string productInListParameter,
            string itemCustomerDiscountGrouplist,
            string customerGuid,
            string customerDiscountGroup,
            string orderCurrencyGuid,
            string defaultCurrency);

        List<VatPctInfo> SelectFromProductTableJoinAttainVatPostingSetup(string vatBusinessPostingGroup, string productGuids);

        LedgerEntryContainer GetCustomerLedgerEntries(string customerGuid);
    }

    public class NavDataService : BuslogicBaseDataService, INavDataService
    {
        public NavDataService(IExpanditDbFactory dbFactory)
            : base(dbFactory)
        {
        }

        public CustomerTableNav SelectFromCustomerTable(string customerGuid)
        {
            string sql =
                    "IF ((ISNULL((SELECT BillToCustomerGuid FROM CustomerTable WHERE CustomerGuid = @customerGuid),''))='') " +
                    " SELECT * FROM CustomerTable WHERE CustomerGuid = @customerGuid" +
                    " ELSE" +
                    " SELECT * FROM CustomerTable WHERE CustomerGuid = (SELECT BillToCustomerGuid FROM CustomerTable WHERE CustomerGuid = @customerGuid)";
            return GetResults<CustomerTableNav>(sql, new { customerGuid }).SingleOrDefault();
        }

        public List<Attain_CustomerInvoiceDiscount> SelectFromAttainCustomerInvoiceDiscount(string invoiceDiscountCode, string currencyGuid)
        {
            string sql =
                "SELECT DiscountPct, CurrencyCode, ServiceCharge, MinimumAmount FROM Attain_CustomerInvoiceDiscount WHERE" +
                " InvoiceDiscountCode = @invoiceDiscountCode AND" +
                " ((CurrencyCode='') Or (CurrencyCode IS NULL) OR (CurrencyCode = @currencyGuid))" +
                " ORDER BY CurrencyCode DESC, MinimumAmount DESC";

            return GetResults<Attain_CustomerInvoiceDiscount>(sql, new { invoiceDiscountCode, currencyGuid }).ToList();
        }

        public Attain_VATPostingSetup GetVatPctFromAttainVatPostingSetup(string businessPostingGroup, string productPostingGroup)
        {
            string sql =
                    "SELECT TOP 1 VATPct FROM Attain_VATPostingSetup WHERE (BusinessPostingGroup=@businessPostingGroup) AND (ProductPostingGroup=@productPostingGroup)";

            return GetResults<Attain_VATPostingSetup>(sql, new { businessPostingGroup, productPostingGroup }).SingleOrDefault();
        }

        public List<NavProductTable> SelectStandardPrice(string productGuids)
        {
            string sql =
                "SELECT ProductGuid, ListPrice, AllowInvoiceDiscount, VATProductPostingGroup, ItemCustomerDiscountGroup, PriceIncludesVAT FROM ProductTable WHERE ProductGuid IN(" +
                (string.IsNullOrEmpty(productGuids) ? "''" : productGuids) + ")";
            return GetResults<NavProductTable>(sql).ToList();
        }

        public List<Attain_ProductPrice> SelectListPrice(string productGuids, string customerGuid, string priceGroupCode, string customerCurrencyGuid, string defaultCurrency)
        {
            productGuids = string.IsNullOrEmpty(productGuids) ? "''" : productGuids;
            string sql =
                string.Format("SELECT ProductGuid, AllowInvoiceDiscount, Attain_VATBusPostingGrPrice, CustomerRelType, " +
                              "MinimumQuantity, EndingDate, CustomerRelGuid, CurrencyGuid, StartingDate, UnitPrice, " +
                              "UOMGuid, VariantCode, PriceInclTax, AllowLineDiscount FROM Attain_ProductPrice " +
                              "WHERE ProductGuid IN (" + productGuids + ") AND " +
                              "((CustomerRelType = 0 AND CustomerRelGuid = '{0}') OR (CustomerRelType = 1 AND CustomerRelGuid = '{1}' " +
                              ") OR " + "(CustomerRelType = 2)) AND " +
                              "(StartingDate <= {4} OR StartingDate IS NULL) AND " +
                              "(EndingDate >= {4} OR EndingDate IS NULL) AND " +
                              "(CurrencyGuid = '{2}' OR CurrencyGuid IS NULL OR CurrencyGuid = '' OR CurrencyGuid = '{3}') " +
                              " ORDER BY ProductGuid", customerGuid, priceGroupCode, customerCurrencyGuid, defaultCurrency, ExpanditLib2.SafeDatetime(DateTime.Now));

            return GetResults<Attain_ProductPrice>(sql).ToList();
        }

        public List<Attain_SalesLineDiscount> SelectFromAttainSalesLineDiscount(string productInListParameter, string itemCustomerDiscountGrouplist, string customerGuid,
            string customerDiscountGroup, string orderCurrencyGuid, string defaultCurrency)
        {
            productInListParameter = string.IsNullOrEmpty(productInListParameter) ? "''" : productInListParameter;
            string sql =
                "SELECT Code, SalesType, MinimumQuantity, EndingDate, SalesCode, Type, CurrencyCode, StartingDate, LineDiscountPct, VariantCode FROM Attain_SalesLineDiscount WHERE " +
                "((Type = 0 AND Code IN (" + productInListParameter + ")) OR ";

            if (!string.IsNullOrEmpty(itemCustomerDiscountGrouplist))
            {
                sql += " (Type = 1 AND Code IN (" + itemCustomerDiscountGrouplist + ")) OR ";
            }

            sql += " (Type = 2))" +
                   " AND((SalesType = 0 AND SalesCode " + ExpanditLib2.SafeEqualString(customerGuid) + ") OR";

            if (!string.IsNullOrEmpty(customerDiscountGroup))
            {
                sql += " (SalesType = 1 AND SalesCode " +
                       ExpanditLib2.SafeEqualString(customerDiscountGroup) + ") OR";
            }

            sql += " (SalesType = 2))" +
                   " AND ((StartingDate <= " + ExpanditLib2.SafeDatetime(DateTime.Now) + ") OR (StartingDate IS NULL)) AND ((EndingDate >= " +
                   ExpanditLib2.SafeDatetime(DateTime.Now) + ") OR (EndingDate IS NULL)) AND ";

            sql += "(CurrencyCode " + ExpanditLib2.SafeEqualString(orderCurrencyGuid) +
                   " OR CurrencyCode IS NULL OR CurrencyCode = '' OR CurrencyCode = " +
                   ExpanditLib2.SafeString(defaultCurrency) + ")";

            sql += " ORDER BY Type, Code";

            return GetResults<Attain_SalesLineDiscount>(sql).ToList();
        }

        public List<VatPctInfo> SelectFromProductTableJoinAttainVatPostingSetup(string vatBusinessPostingGroup, string productGuids)
        {
            string sql = string.Format("SELECT ProductGuid, VATPct FROM ProductTable LEFT JOIN Attain_VATPostingSetup ON " +
                                       "ProductTable.VATProductPostingGroup = Attain_VATPostingSetup.ProductPostingGroup " +
                                       "WHERE (BusinessPostingGroup='{0}') AND " +
                                       "ProductGuid IN ({1})", vatBusinessPostingGroup, string.IsNullOrEmpty(productGuids) ? "''" : productGuids);

            return GetResults<VatPctInfo>(sql).ToList();
        }
    }
}