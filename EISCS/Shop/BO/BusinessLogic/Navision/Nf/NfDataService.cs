﻿using System;
using System.Collections.Generic;
using System.Linq;
using CmsPublic.DataRepository;
using EISCS.Shop.DO.Dto.Backend.Navision;
using EISCS.Shop.DO.Dto.Backend.Navision.Nf;
using EISCS.Shop.DO.Interface;


namespace EISCS.Shop.BO.BusinessLogic.Navision.Nf
{
    public interface INfDataService
    {

        bool GetBoolFromLineDiscountCalculation();
        CustomerTableNf SelectFromCustomerTable(string customerGuid);
        List<NfProductTable> SelectStandardPrice(string productGuids);
        List<NF_ItemPrice> SelectListPrice(string productGuids, string customerGuid, string priceGroupCode, string currencyGuid);
        List<NF_Item_Qty_Disc> SelectFromNfItemQtyDiscJoinProductTable(string productGuids);
        List<NF_CustomerItemDiscount> SelectFromNfCustomerItemDiscountJoinProductTable(string productGuids, string customerItemDiscountGroup);
        List<NF_CustomerInvoiceDiscount> SelectFromNfCustomerInvoiceDiscount(string invoiceDiscountCode, string currencyGuid);
        List<VatPctInfo> SelectFromProductTableJoinNfVatPostingSetup(string vatBusinessPostingGroup, string productGuids);
        NF_VATPostingSetup GetVatPctFromNfVatPostingSetup(string businessPostingGroup, string productPostingGroup);
        LedgerEntryContainer GetCustomerLedgerEntries(string customerGuid);
    }

    public class NfDataService : BuslogicBaseDataService, INfDataService
    {

        public NfDataService(IExpanditDbFactory dbFactory)
            : base(dbFactory)
        {
        }

        public bool GetBoolFromLineDiscountCalculation()
        {
            const string sql = "SELECT LineDiscountCalculation FROM NF_SalesReceivablesSetup WHERE LineDiscountCalculation <> 0";
            var list = GetResults<double>(sql).ToList();
            return list.Any();
        }

        public CustomerTableNf SelectFromCustomerTable(string customerGuid)
        {
            string sql =
                string.Format(
                "IF ((ISNULL((SELECT BillToCustomerGuid FROM CustomerTable WHERE CustomerGuid = '{0}'),''))='') " +
                    " SELECT * FROM CustomerTable WHERE CustomerGuid = '{0}'" +
                    " ELSE" +
                    " SELECT * FROM CustomerTable WHERE CustomerGuid = (SELECT BillToCustomerGuid FROM CustomerTable WHERE CustomerGuid = '{0}')", customerGuid);
            return GetSingle<CustomerTableNf>(sql);
        }

        public List<NfProductTable> SelectStandardPrice(string productGuids)
        {
            string sql =
                "SELECT SELECT ProductGuid, ListPrice, AllowInvoiceDiscount, VATProductPostingGroup, PriceIncludesVAT FROM ProductTable WHERE ProductGuid IN(" + productGuids + ")";
            return GetResults<NfProductTable>(sql).ToList();

        }

        public List<NF_ItemPrice> SelectListPrice(string productGuids, string customerGuid, string priceGroupCode, string currencyGuid)
        {
            string sql =
                string.Format("SELECT ItemNo, UnitPrice, CurrencyCode, AllowInvoiceDiscount, AllowQuantityDiscount, AllowCustomerItemDiscount, " +
                         "EndingDate, StartingDate, PriceInclTax FROM NF_ItemPrice " +
                         "WHERE ItemNo IN (" + productGuids + ") AND " +
                         "((PriceGroupCode = {0}) OR (PriceGroupCode = '') OR (PriceGroupCode IS NULL)) AND " +
                         "((CurrencyCode = {1}) OR (CurrencyCode = '') OR (CurrencyCode IS NULL)) AND " +
                         "((StartingDate <= '" + DateTime.Now + "') OR (StartingDate IS NULL)) " +
                         "ORDER BY PriceGroupCode, CurrencyCode, StartingDate", priceGroupCode, currencyGuid);

            return GetResults<NF_ItemPrice>(sql).ToList();
        }

        public List<NF_Item_Qty_Disc> SelectFromNfItemQtyDiscJoinProductTable(string productGuids)
        {
            string sql =
                "SELECT ProductGuid, DiscountPct, MinimumQuantity, NF_Item_Qty_Disc.QtyDiscountGroup FROM NF_Item_Qty_Disc " +
                "LEFT JOIN ProductTable ON NF_Item_Qty_Disc.QtyDiscountGroup = ProductTable.QtyDiscountGroup " +
                "WHERE ProductTable.ProductGuid IN (" + productGuids + ") " +
                "ORDER BY ProductGuid, MinimumQuantity DESC";

            return GetResults<NF_Item_Qty_Disc>(sql).ToList();
        }

        public List<NF_CustomerItemDiscount> SelectFromNfCustomerItemDiscountJoinProductTable(string productGuids, string customerItemDiscountGroup)
        {
            string sql =
                "SELECT ProductTable.ProductGuid, ProductTable.ItemCustomerDiscountGroup, NF_CustomerItemDiscount.CustomerDiscountGroup, DiscountPct " +
                "FROM ProductTable LEFT JOIN NF_CustomerItemDiscount ON ProductTable.ItemCustomerDiscountGroup = NF_CustomerItemDiscount.ItemDiscountGroup " +
                "WHERE (CustomerDiscountGroup=" + customerItemDiscountGroup + ") AND ProductTable.ProductGuid IN (" + productGuids + ")";

            return GetResults<NF_CustomerItemDiscount>(sql).ToList();
        }

        public List<NF_CustomerInvoiceDiscount> SelectFromNfCustomerInvoiceDiscount(string invoiceDiscountCode, string currencyGuid)
        {
            string sql = string.Format(
                "SELECT DiscountPct, CurrencyCode, ServiceCharge, MinimumAmount FROM NF_CustomerInvoiceDiscount WHERE" +
                " InvoiceDiscountCode='{0}' AND" +
                " ((CurrencyCode='') Or (CurrencyCode IS NULL) OR (CurrencyCode = '{1}'))" +
                " ORDER BY CurrencyCode DESC, MinimumAmount DESC", invoiceDiscountCode, currencyGuid);

            return GetResults<NF_CustomerInvoiceDiscount>(sql).ToList();
        }

        public List<VatPctInfo> SelectFromProductTableJoinNfVatPostingSetup(string vatBusinessPostingGroup, string productGuids)
        {
            string sql = string.Format("SELECT ProductGuid, VATPct FROM ProductTable LEFT JOIN Attain_VATPostingSetup ON " +
                         "ProductTable.VATProductPostingGroup = NF_VATPostingSetup.ProductPostingGroup " +
                         "WHERE (BusinessPostingGroup='{0}') AND " +
                         "ProductGuid IN ({1})", vatBusinessPostingGroup, productGuids);

            return GetResults<VatPctInfo>(sql).ToList();
        }

        public NF_VATPostingSetup GetVatPctFromNfVatPostingSetup(string businessPostingGroup, string productPostingGroup)
        {
            string sql =
                string.Format(
                    "SELECT TOP 1 VATPct FROM NF_VATPostingSetup WHERE (BusinessPostingGroup='{0}') AND (ProductPostingGroup='{1}')",
                    businessPostingGroup, productPostingGroup);

            return GetSingle<NF_VATPostingSetup>(sql);
        }
    }
}
