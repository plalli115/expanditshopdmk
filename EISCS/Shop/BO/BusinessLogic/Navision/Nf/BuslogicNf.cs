﻿using System;
using System.Collections.Generic;
using System.Linq;
using EISCS.ExpandITFramework.Infrastructure;
using EISCS.Shop.DO.Dto.Backend.Navision;
using EISCS.Shop.DO.Dto.Backend.Navision.Nf;
using EISCS.Shop.DO.Dto.Cart;
using EISCS.Shop.DO.Interface;
using EISCS.Wrappers.Configuration;
using ExpandIT;

namespace EISCS.Shop.BO.BusinessLogic.Navision.Nf
{
    public class BuslogicNf : BuslogicBase
    {
        private readonly ICurrencyConverter _currencyConverter;
        private readonly IConfigurationObject _configurationObject;
        private readonly bool _isCollectQuantitiesForLineDiscount;
        private readonly INfDataService _dataService;
        private string _productInListParameter;
        private bool _multiplyDiscounts;
        private readonly IExpanditUserService _expanditUserService;

        public BuslogicNf(IConfigurationObject configuration, ICurrencyConverter currencyConverter, INfDataService dataService, IExpanditUserService expanditUserService)
        {
            _currencyConverter = currencyConverter;
            _configurationObject = configuration;
            _dataService = dataService;
            _expanditUserService = expanditUserService;
            _isCollectQuantitiesForLineDiscount =
                ExpanditLib2.CBoolEx(_configurationObject.Read("NF_COLLECT_QUANTITES_FOR_LINE_DISCOUNTS"));
        }

        public override PageMessages CalculateOrder<T>(BaseOrderHeader<T> order, string currency, string defaultCurrency)
        {
            try
            {
                _multiplyDiscounts = _dataService.GetBoolFromLineDiscountCalculation();

                // 1. Get Quantities
                AccumulateProductQuantities(order);

                // 2. Initialization - get customer specific data from CustomerTable
                string customerGuid = GetCustomerGuid(order.UserGuid);
                InitOrder(order, customerGuid, currency);

                // 3. Line calculations
                StandardPrice(order, defaultCurrency);
                ListPrice(order, defaultCurrency);
                ItemPriceFixCurrency(order, defaultCurrency);
                QuantityDiscount(order);
                NfCustomerItemDiscount(order);
                OrderLineTotal(order);

                // 4. Header calculations
                SubTotal(order, customerGuid, defaultCurrency); // TODO: Check if it's the defaultCurrency or the customerCurrency that should be used here
                InvoiceDiscounts(order, customerGuid, defaultCurrency); // TODO: Check if it's the defaultCurrency or the customerCurrency that should be used here
                CalculateVat(order, customerGuid, defaultCurrency); // TODO: Check if it's the defaultCurrency or the customerCurrency that should be used here

                // Mark order as calculated
                order.IsCalculated = true;
            }
            catch (Exception ex)
            {
                var messages = new PageMessages();
                messages.Errors.Add(ex.Message);
                return messages;
            }
            return null;
        }

        private string GetCustomerGuid(string userGuid)
        {
            var userTable = _expanditUserService.GetUser(userGuid);

            if (userTable == null)
            {
                return _configurationObject.Read("ANONYMOUS_CUSTOMERGUID");
            }

            return string.IsNullOrEmpty(userTable.CustomerGuid) ? _configurationObject.Read("ANONYMOUS_CUSTOMERGUID") : userTable.CustomerGuid;
        }

        /*
         * Header Methods
         */

        /// <summary>
        /// Read information from CustomerTable
        /// </summary>
        /// <param name="order"> </param>
        /// <param name="customerGuid"></param>
        /// <param name="currencyGuid"> </param>
        /// <returns></returns>
        public void InitOrder<T>(BaseOrderHeader<T> order, string customerGuid, string currencyGuid) where T : BaseOrderLine
        {
            CustomerTableNf customerTable = _dataService.SelectFromCustomerTable(customerGuid);

            if (customerTable == null)
            {
                throw new Exception(string.Format("CUSTOMER {0} DOES NOT EXIST", customerGuid));
            }

            HeaderParameter headerParameter = new HeaderParameter();

            headerParameter.CustomerGuid = customerTable.CustomerGuid;
            headerParameter.CustomerItemDiscountGroup = customerTable.CustomerItemDiscountGroup;
            headerParameter.AllowQuantityDiscount = customerTable.AllowQuantityDiscount;
            headerParameter.PriceGroupCode = customerTable.PriceGroupCode;
            headerParameter.InvoiceDiscountCode = customerTable.InvoiceDiscountCode;
            headerParameter.VATBusinessPostingGroup = customerTable.VATBusinessPostingGroup;
            headerParameter.CurrencyGuid = currencyGuid;

            // Set the additional order header parameter with initial values here
            order.HeaderParameter = headerParameter;
        }

        /* 
         *  2 (header)
         */

        public void SubTotal<T>(BaseOrderHeader<T> order, string customerGuid, string currencyGuid) where T : BaseOrderLine
        {
            double amountSum = 0;
            double invoiceDiscountSum = 0;

            foreach (var line in order.Lines)
            {
                if (((LineParameter)line.LineParameter).ListPriceIsInclTax)
                {
                    amountSum += line.LineTotal / (1 + Convert.ToDouble(line.TaxPct) / 100);
                    if (((LineParameter)line.LineParameter).AllowInvoiceDiscount)
                    {
                        invoiceDiscountSum += line.LineTotal / (1 + Convert.ToDouble(line.TaxPct) / 100);
                    }
                }
                else
                {
                    amountSum += line.LineTotal;
                    if (((LineParameter)line.LineParameter).AllowInvoiceDiscount)
                    {
                        invoiceDiscountSum += line.LineTotal;
                    }
                }
            }

            order.SubTotal = amountSum;
            ((HeaderParameter)order.HeaderParameter).SubTotal_InvoiceDiscount = invoiceDiscountSum;
            order.Total = amountSum;
        }

        /*
         *  3 (Header)
         */

        public void InvoiceDiscounts<T>(BaseOrderHeader<T> order, string customerGuid, string currencyGuid) where T : BaseOrderLine
        {
            //  Calculation of Service chage and invoice discount.
            //  Invoice discount should be calculated on SubTotal_InvoiceDiscount (Found in Function NFSubTotal)
            //  Service Charge should be calculated on SubTotal - not considering if invoice dicounts are allowed or not.

            double discountPct = 0;
            double serviceCharge = 0;
            bool isDiscountFound = false;
            bool isServiceChargeFound = false;
            bool isSameCurrency = false;

            List<NF_CustomerInvoiceDiscount> customerInvoiceDiscounts =
                _dataService.SelectFromNfCustomerInvoiceDiscount(
                    ((HeaderParameter)order.HeaderParameter).InvoiceDiscountCode, order.CurrencyGuid);

            if (customerInvoiceDiscounts != null)
            {
                // Try with the same currency
                List<NF_CustomerInvoiceDiscount> discounts =
                    customerInvoiceDiscounts.FindAll(disc => disc.CurrencyCode == order.CurrencyGuid);

                if (discounts.Count > 0)
                {
                    isSameCurrency = true;
                }

                foreach (var discount in discounts)
                {
                    if (!isDiscountFound &&
                        ((HeaderParameter)order.HeaderParameter).SubTotal_InvoiceDiscount >= discount.MinimumAmount)
                    {
                        isDiscountFound = true;
                        discountPct = discount.DiscountPct;
                    }

                    if (!isServiceChargeFound &&
                        order.SubTotal >= discount.MinimumAmount)
                    {
                        isServiceChargeFound = true;
                        serviceCharge = discount.ServiceCharge;
                    }

                    if (isDiscountFound && isServiceChargeFound)
                    {
                        break;
                    }
                }

                if (!isSameCurrency)
                {
                    // LOOK FOR THE EMPTY CURRENCY
                    discounts = customerInvoiceDiscounts.FindAll(disc => disc.CurrencyCode == "");
                    foreach (var discount in discounts)
                    {
                        double tmpInvSubTotal =
                            _currencyConverter.ConvertCurrency(
                                ((HeaderParameter)order.HeaderParameter).SubTotal_InvoiceDiscount, order.CurrencyGuid,
                                "");
                        double tmpSubTotal =
                            _currencyConverter.ConvertCurrency(
                                order.SubTotal, order.CurrencyGuid, "");

                        if (!isDiscountFound && tmpInvSubTotal >= discount.MinimumAmount)
                        {
                            isDiscountFound = true;
                            discountPct = discount.DiscountPct;
                        }

                        if (!isServiceChargeFound && tmpSubTotal >= discount.MinimumAmount)
                        {
                            isServiceChargeFound = true;
                            serviceCharge = _currencyConverter.ConvertCurrency(discount.ServiceCharge, "",
                                                                               order.CurrencyGuid);
                        }
                        if (isDiscountFound && isServiceChargeFound)
                        {
                            break;
                        }
                    }
                }
            }

            double discPct = ExpanditLib2.RoundEx(discountPct, 5);

            order.InvoiceDiscountPct = discPct.ToString();
            order.ServiceCharge = ExpanditLib2.RoundEx(serviceCharge, 2);
            order.InvoiceDiscount = ((HeaderParameter)order.HeaderParameter).SubTotal_InvoiceDiscount * discPct / 100;
            order.Total = order.SubTotal + order.ServiceCharge - order.InvoiceDiscount;
        }

        /*
         *  4 (Header)
         */

        public void CalculateVat<T>(BaseOrderHeader<T> order, string customerGuid, string currencyGuid) where T : BaseOrderLine
        {
            double vatPct;
            decimal invoiceDiscountAmountInclTaxSum = 0;
            decimal totalInclTaxSum = 0;
            decimal totalSum = 0;
            decimal invoiceDiscountAmountSum = 0;
            Dictionary<double, VatNode> vatSums = new Dictionary<double, VatNode>();
            decimal subTotalSum = 0;
            decimal subTotalInclVatSum = 0;
            decimal lineDiscountAmountExclTaxSum = 0;
            decimal lineDiscountAmountInclTaxSum = 0;
            
            var vatPctInfos =
                _dataService.SelectFromProductTableJoinNfVatPostingSetup(
                    ((HeaderParameter)order.HeaderParameter).VATBusinessPostingGroup, _productInListParameter);

            foreach (var line in order.Lines)
            {
                decimal orderLineListPriceInclTax;
                decimal orderLineListPriceExclTax;
                decimal orderLineSubTotalInclTax;
                decimal orderLineSubTotalExclTax;
                decimal orderLineTotalInclTax;
                decimal orderLineTotalExclTax;
                decimal orderLineLineDiscountAmountInclTax;
                decimal orderLineLineDiscountAmountExclTax;
                decimal orderLineInvoiceDiscountAmountInclTax = 0;
                decimal orderLineInvoiceDiscountAmountExclTax = 0;
                vatPct = 0;

                if (vatPctInfos != null)
                {
                    if (vatPctInfos.Exists(vat => vat.ProductGuid == line.ProductGuid))
                    {
                        var firstOrDefault = vatPctInfos.FirstOrDefault(x => x.ProductGuid == line.ProductGuid);
                        if (firstOrDefault != null)
                        {
                            vatPct = firstOrDefault.VATPct;
                        }
                    }
                }

                line.TaxPct = ExpanditLib2.CStrEx(vatPct);

                if (((LineParameter)line.LineParameter).ListPriceIsInclTax)
                {
                    orderLineListPriceInclTax = (decimal)line.ListPrice;
                    orderLineListPriceExclTax = (decimal)(line.ListPrice * (1 / (1 + vatPct / 100)));
                    orderLineSubTotalInclTax = (decimal)((LineParameter)line.LineParameter).SubTotal;
                    orderLineSubTotalExclTax = (decimal)(((LineParameter)line.LineParameter).SubTotal * (1 / (1 + vatPct / 100)));
                    orderLineTotalInclTax = (decimal)line.LineTotal;
                    orderLineTotalExclTax = (decimal)(line.LineTotal * (1 / (1 + vatPct / 100)));
                    orderLineLineDiscountAmountInclTax = (decimal)line.LineDiscountAmount;
                    orderLineLineDiscountAmountExclTax = (decimal)(line.LineDiscountAmount * (1 / (1 + vatPct / 100)));
                }
                else
                {
                    orderLineListPriceInclTax = (decimal)(line.ListPrice * (1 + vatPct / 100));
                    orderLineListPriceExclTax = (decimal)line.ListPrice;
                    orderLineSubTotalInclTax = (decimal)(((LineParameter)line.LineParameter).SubTotal * (1 + vatPct / 100));
                    orderLineSubTotalExclTax = (decimal)((LineParameter)line.LineParameter).SubTotal;
                    orderLineTotalInclTax = (decimal)(line.LineTotal * (1 + vatPct / 100));
                    orderLineTotalExclTax = (decimal)line.LineTotal;
                    orderLineLineDiscountAmountInclTax = (decimal)(line.LineDiscountAmount * (1 + vatPct / 100));
                    orderLineLineDiscountAmountExclTax = (decimal)line.LineDiscountAmount;
                }

                //Special case when customer prices is incl VAT - needs different calculation of line discounts
                if (!((LineParameter)line.LineParameter).ListPriceIsInclTax)
                {
                    if (ExpanditLib2.CBoolEx(order.PricesIncludingVat))
                    {
                        orderLineLineDiscountAmountInclTax = (decimal)line.LineDiscountAmountInclTax;
                        orderLineTotalInclTax = (decimal)line.TotalInclTax;
                    }
                }

                if (((LineParameter)line.LineParameter).AllowInvoiceDiscount)
                {
                    orderLineInvoiceDiscountAmountInclTax = orderLineTotalInclTax * (ExpanditLib2.CDblEx(order.InvoiceDiscountPct) / 100);
                    orderLineInvoiceDiscountAmountExclTax = orderLineTotalExclTax * (ExpanditLib2.CDblEx(order.InvoiceDiscountPct) / 100);
                }

                decimal orderLineTaxAmount = orderLineTotalExclTax * (decimal)(vatPct / 100);

                // Add the correct values to the dictionary

                line.ListPrice = (double)ExpanditLib2.RoundEx(orderLineListPriceExclTax, 5);
                line.ListPriceInclTax = (double)ExpanditLib2.RoundEx(orderLineListPriceInclTax, 5);
                ((LineParameter)line.LineParameter).SubTotal = (double)ExpanditLib2.RoundEx(orderLineSubTotalExclTax, 5);
                ((LineParameter)line.LineParameter).SubTotalInclTax = (double)ExpanditLib2.RoundEx(orderLineSubTotalInclTax, 5);
                line.LineTotal = (double)ExpanditLib2.RoundEx(orderLineTotalExclTax, 5);
                line.TotalInclTax = (double)ExpanditLib2.RoundEx(orderLineTotalInclTax, 5);
                line.LineDiscountAmount = (double)ExpanditLib2.RoundEx(orderLineLineDiscountAmountExclTax, 5);
                line.LineDiscountAmountInclTax = (double)ExpanditLib2.RoundEx(orderLineLineDiscountAmountInclTax, 5);
                ((LineParameter)line.LineParameter).InvoiceDiscountAmount = ExpanditLib2.RoundEx(orderLineInvoiceDiscountAmountExclTax, 5);
                ((LineParameter)line.LineParameter).InvoiceDiscountAmountInclTax = ExpanditLib2.RoundEx(orderLineInvoiceDiscountAmountInclTax, 5);
                line.TaxAmount = (double)ExpanditLib2.RoundEx(orderLineTaxAmount, 5);

                if (order.PricesIncludingVat)
                {
                    if (vatSums.ContainsKey(vatPct))
                    {
                        vatSums[vatPct].VatSum = vatSums[vatPct].VatSum + ExpanditLib2.RoundEx(orderLineTotalInclTax, 5);
                        vatSums[vatPct].InvoiceDiscountAmountSumExclVAT = vatSums[vatPct].InvoiceDiscountAmountSumExclVAT + orderLineInvoiceDiscountAmountExclTax;
                        vatSums[vatPct].InvoiceDiscountAmountSumInclVAT = vatSums[vatPct].InvoiceDiscountAmountSumInclVAT + orderLineInvoiceDiscountAmountInclTax;
                    }
                    else
                    {
                        VatNode node = new VatNode();
                        node.VatSum = ExpanditLib2.RoundEx(orderLineTotalInclTax, 5);
                        node.InvoiceDiscountAmountSumExclVAT = orderLineInvoiceDiscountAmountExclTax;
                        node.InvoiceDiscountAmountSumInclVAT = orderLineInvoiceDiscountAmountInclTax;
                        vatSums.Add(vatPct, node);
                    }
                }
                else
                {
                    if (vatSums.ContainsKey(vatPct))
                    {
                        vatSums[vatPct].VatSum = vatSums[vatPct].VatSum + ExpanditLib2.RoundEx(orderLineTotalExclTax, 5);
                        vatSums[vatPct].InvoiceDiscountAmountSumExclVAT = vatSums[vatPct].InvoiceDiscountAmountSumExclVAT + orderLineInvoiceDiscountAmountExclTax;
                        vatSums[vatPct].InvoiceDiscountAmountSumInclVAT = vatSums[vatPct].InvoiceDiscountAmountSumInclVAT + orderLineInvoiceDiscountAmountInclTax;
                    }
                    else
                    {
                        VatNode node = new VatNode();
                        node.VatSum = ExpanditLib2.RoundEx(orderLineTotalExclTax, 5);
                        node.InvoiceDiscountAmountSumExclVAT = orderLineInvoiceDiscountAmountExclTax;
                        node.InvoiceDiscountAmountSumInclVAT = orderLineInvoiceDiscountAmountInclTax;
                        vatSums.Add(vatPct, node);
                    }
                }

                invoiceDiscountAmountSum = invoiceDiscountAmountSum + orderLineInvoiceDiscountAmountExclTax;
                invoiceDiscountAmountInclTaxSum = invoiceDiscountAmountInclTaxSum + orderLineInvoiceDiscountAmountInclTax;
                lineDiscountAmountInclTaxSum = lineDiscountAmountInclTaxSum + ExpanditLib2.RoundEx(orderLineLineDiscountAmountInclTax, 5);
                lineDiscountAmountExclTaxSum = lineDiscountAmountExclTaxSum + ExpanditLib2.RoundEx(orderLineLineDiscountAmountExclTax, 5);
                subTotalSum = subTotalSum + ExpanditLib2.RoundEx(orderLineSubTotalExclTax, 5);
                subTotalInclVatSum = subTotalInclVatSum + ExpanditLib2.RoundEx(orderLineSubTotalInclTax, 5);
                totalSum = totalSum + ExpanditLib2.RoundEx(orderLineTotalExclTax, 5);
                totalInclTaxSum = totalInclTaxSum + ExpanditLib2.RoundEx(orderLineTotalInclTax, 5);
                line.TaxPct = ExpanditLib2.CStrEx(vatPct);
            } // End foreach

            invoiceDiscountAmountSum = ExpanditLib2.RoundEx(invoiceDiscountAmountSum, 5);
            invoiceDiscountAmountInclTaxSum = ExpanditLib2.RoundEx(invoiceDiscountAmountInclTaxSum, 5);

            decimal vaTexDiscountSum = 0;
            decimal VATSum = 0;

            ExpDictionary calculatedVatValuesDict = new ExpDictionary();
            foreach (double vatType in vatSums.Keys)
            {
                decimal vaTxSum;
                decimal vaTexDiscount;
                if (order.PricesIncludingVat)
                {
                    vaTxSum = ExpanditLib2.RoundEx(vatSums[vatType].VatSum, 5);
                    vaTexDiscount = ExpanditLib2.RoundEx(vaTxSum * (decimal)(1 - (1 / (1 + vatType / 100))), 5);
                    vaTxSum = ExpanditLib2.RoundEx((vaTxSum - ExpanditLib2.RoundEx(vatSums[vatType].InvoiceDiscountAmountSumInclVAT, 5)) * (decimal)(1 - (1 / (1 + vatType / 100))), 5);
                    VATSum = VATSum + vaTxSum;
                }
                else
                {
                    vaTxSum = ExpanditLib2.RoundEx(vatSums[vatType].VatSum, 5);
                    vaTexDiscount = vaTxSum * (decimal)(vatType / 100);
                    vaTxSum = ExpanditLib2.RoundEx((vaTxSum - ExpanditLib2.RoundEx(vatSums[vatType].InvoiceDiscountAmountSumExclVAT, 5)) * (decimal)(vatType / 100), 5);
                    VATSum = VATSum + vaTxSum;
                }
                if (!string.IsNullOrEmpty(order.HeaderGuid))
                {
                    if (!calculatedVatValuesDict.ContainsKey("VatTypes"))
                    {
                        calculatedVatValuesDict["VatTypes"] = new ExpDictionary();
                    }
                    ExpDictionary x = (ExpDictionary)calculatedVatValuesDict["VatTypes"];
                    if (x.ContainsKey(vatType)) { x[vatType] = vaTxSum; }
                }
                vaTexDiscountSum = vaTexDiscountSum + vaTexDiscount;
            }

            if (order.PricesIncludingVat)
            {
                totalInclTaxSum = (totalInclTaxSum - invoiceDiscountAmountInclTaxSum);
            }
            else
            {
                totalInclTaxSum = (totalSum - invoiceDiscountAmountSum) + VATSum;
                subTotalInclVatSum = ExpanditLib2.RoundEx(subTotalSum + vaTexDiscountSum, 5);
            }

            //// --
            //// Service Charge calculations. Lookup the tax pct for service charge according to cfg.
            //// --

            var postingSetup =
                _dataService.GetVatPctFromNfVatPostingSetup(
                    ((HeaderParameter)order.HeaderParameter).VATBusinessPostingGroup,
                    _configurationObject.Read("NF_SERVICECHARGE_PRODUCTPOSTINGGROUP"));

            vatPct = postingSetup != null ? ExpanditLib2.ConvertToDbl(postingSetup.VATPct) : 0;

            // Set the VATPct in the orderobject for the Shipping and Handling calculations.
            order.TaxPct = ExpanditLib2.CStrEx(vatPct);

            // Calculate the VAT on the Service Charge, if any
            order.ServiceChargeInclTax = order.ServiceCharge * (1 + vatPct / 100);

            VATSum = VATSum + (decimal)(order.ServiceCharge * vatPct / 100);

            // Tax fields
            order.TaxAmount = (double)VATSum;

            order.InvoiceDiscount = (double)invoiceDiscountAmountSum;

            order.InvoiceDiscountInclTax = (double)invoiceDiscountAmountInclTaxSum;
            order.SubTotalInclTax = (double)(subTotalInclVatSum - lineDiscountAmountInclTaxSum);

            if (order.PricesIncludingVat)
            {
                order.SubTotal = (double)(subTotalInclVatSum - vaTexDiscountSum - lineDiscountAmountInclTaxSum);
                order.Total = (double)(totalInclTaxSum + ExpanditLib2.CDblEx(order.ServiceChargeInclTax)) - (double)VATSum;
            }
            else
            {
                order.SubTotal = (double)(subTotalSum - lineDiscountAmountExclTaxSum);
                order.Total = (double)(totalSum + ExpanditLib2.CDblEx(order.ServiceCharge) - invoiceDiscountAmountSum);
            }

            order.TotalInclTax = (double)(totalInclTaxSum + ExpanditLib2.CDblEx(order.ServiceChargeInclTax));

            if (!string.IsNullOrEmpty(order.HeaderGuid))
            {
                order.VatTypes = ExpandIT.Serialization.Marshall.MarshallDictionary((ExpDictionary)calculatedVatValuesDict["VatTypes"]);
            }
        }

        /*
         * Line calculations
         */

        public void StandardPrice<T>(BaseOrderHeader<T> order, string defaultCurrency) where T : BaseOrderLine
        {
            List<string> guids = order.Lines.Select(line => "'" + line.ProductGuid + "'").ToList();
            _productInListParameter = string.Join(",", guids);

            List<NfProductTable> navProduct = _dataService.SelectStandardPrice(_productInListParameter);

            foreach (var line in order.Lines)
            {
                var productTable = navProduct.Find(x => x.ProductGuid == line.ProductGuid);
                line.ListPrice = productTable.ListPrice;
                line.CurrencyGuid = defaultCurrency; // TODO: check if this is right.

                var lineParameters = new LineParameter
                {
                    AllowInvoiceDiscount = productTable.AllowInvoiceDiscount,
                    VATProductPostingGroup = productTable.VATProductPostingGroup,
                    ListPriceIsInclTax = productTable.PriceIncludesVAT
                };

                line.LineParameter = lineParameters; // Set the parameter object with initial values here
            }
        }

        public void ListPrice<T>(BaseOrderHeader<T> order, string defaultCurrency) where T : BaseOrderLine
        {
            List<NF_ItemPrice> attainProductPrices = _dataService.SelectListPrice(_productInListParameter, ((HeaderParameter)order.HeaderParameter).CustomerGuid,
                                                                                         ((HeaderParameter)order.HeaderParameter).PriceGroupCode,
                                                                                         ((HeaderParameter)order.HeaderParameter).CurrencyGuid);
            if (attainProductPrices == null)
            {
                return;
            }

            foreach (var line in order.Lines)
            {
                var nfItem = attainProductPrices.FirstOrDefault(x => x.ItemNo == line.ProductGuid);

                if (nfItem == null) { continue; }

                line.ListPrice = nfItem.UnitPrice;
                ((LineParameter)line.LineParameter).ListPriceIsInclTax = nfItem.PriceInclTax;
                line.CurrencyGuid = nfItem.CurrencyCode;
                ((LineParameter)line.LineParameter).AllowInvoiceDiscount = nfItem.AllowInvoiceDiscount;
                ((LineParameter)line.LineParameter).AllowQuantityDiscount = nfItem.AllowQuantityDiscount;
                ((LineParameter)line.LineParameter).AllowCustomerItemDiscount = nfItem.AllowCustomerItemDiscount;
                if (string.IsNullOrEmpty(line.CurrencyGuid))
                {
                    line.CurrencyGuid = defaultCurrency; // TODO: Check that currency is correct.
                }
            }

        }

        public void ItemPriceFixCurrency<T>(BaseOrderHeader<T> order, string defaultCurrency) where T : BaseOrderLine
        {
            foreach (var line in order.Lines.Where(line => line.CurrencyGuid != order.CurrencyGuid))
            {
                line.ListPrice = Convert.ToDouble(_currencyConverter.ConvertCurrency(line.ListPrice, line.CurrencyGuid, order.CurrencyGuid));
                line.CurrencyGuid = order.CurrencyGuid;
            }
        }

        public void QuantityDiscount<T>(BaseOrderHeader<T> order) where T : BaseOrderLine
        {
            List<NF_Item_Qty_Disc> discs = _dataService.SelectFromNfItemQtyDiscJoinProductTable(_productInListParameter);

            if (discs == null) { return; }

            foreach (var line in order.Lines)
            {
                var nfDiscount = discs.FirstOrDefault(x => x.ProductGuid == line.ProductGuid);

                if (nfDiscount == null) { continue; }

                double discPct = 0;

                double productTotalQuantity = _isCollectQuantitiesForLineDiscount ? ProductQuantities.GetProductQuantities(line.ProductGuid) : line.Quantity;

                if (productTotalQuantity >= nfDiscount.MinimumQuantity)
                {
                    discPct = nfDiscount.DiscountPct;
                }

                ((LineParameter)line.LineParameter).QuantityDiscountPct = discPct;

            }
        }

        private void NfCustomerItemDiscount<T>(BaseOrderHeader<T> order) where T : BaseOrderLine
        {
            var discs = _dataService.SelectFromNfCustomerItemDiscountJoinProductTable(_productInListParameter, ((HeaderParameter)order.HeaderParameter).CustomerItemDiscountGroup);

            if (discs == null) { return; }

            foreach (var line in order.Lines)
            {
                var nfDiscount = discs.FirstOrDefault(x => x.ProductGuid == line.ProductGuid);
                ((LineParameter)line.LineParameter).CustomerItemDiscountPct = nfDiscount == null ? 0 : nfDiscount.DiscountPct;
            }
        }

        public void OrderLineTotal<T>(BaseOrderHeader<T> order) where T : BaseOrderLine
        {
            foreach (var line in order.Lines)
            {
                if (!((LineParameter)line.LineParameter).AllowQuantityDiscount && !((HeaderParameter)order.HeaderParameter).AllowQuantityDiscount)
                {
                    ((LineParameter)line.LineParameter).QuantityDiscountPct = 0;
                }

                if (!((LineParameter)line.LineParameter).AllowCustomerItemDiscount)
                {
                    ((LineParameter)line.LineParameter).CustomerItemDiscountPct = 0;
                }

                if (_multiplyDiscounts)
                {
                    line.LineDiscount = 100 - 100 * ((1 - ((LineParameter)line.LineParameter).QuantityDiscountPct / 100) * (1 - ((LineParameter)line.LineParameter).CustomerItemDiscountPct / 100));
                }
                else
                {
                    line.LineDiscount = ((LineParameter)line.LineParameter).QuantityDiscountPct + ((LineParameter)line.LineParameter).CustomerItemDiscountPct;
                }

                if (line.LineDiscount > 100)
                {
                    line.LineDiscount = 100;
                }

                ((LineParameter)line.LineParameter).SubTotal = line.Quantity * line.ListPrice;
                line.LineDiscountAmount = ((LineParameter)line.LineParameter).SubTotal * line.LineDiscount / 100;
                line.LineTotal = ((LineParameter)line.LineParameter).SubTotal - line.LineDiscountAmount;

            }
        }

        public override void PromoShippingDiscounts<T>(BaseOrderHeader<T> order, string defaultCurrency)
        {
            throw new NotImplementedException();
        }
    }

    /// <summary>
    /// Internal class to hold parameters used in price calculations
    /// </summary>
    public class HeaderParameter : IHeaderParameter
    {
        public bool AllowQuantityDiscount { get; set; }
        public string CustomerGuid { get; set; }
        public string CustomerItemDiscountGroup { get; set; }
        public string PriceGroupCode { get; set; }
        public string InvoiceDiscountCode { get; set; }
        public string VATBusinessPostingGroup { get; set; }
        public string CurrencyGuid { get; set; }
        public double SubTotal_InvoiceDiscount { get; set; }
    }

    /// <summary>
    /// Internal class to hold parameters used in price calculations
    /// </summary>
    public class LineParameter : ILineParameter
    {
        public double CustomerItemDiscountPct { get; set; }
        public double QuantityDiscountPct { get; set; }
        public bool AllowQuantityDiscount { get; set; }
        public bool AllowCustomerItemDiscount { get; set; }
        // Additional parameters set in StandardPrice
        public bool AllowLineDisc { get; set; }
        public bool AllowInvoiceDiscount { get; set; }
        //public string ItemCustomerDiscountGroup { get; set; } // IS USED?
        public string VATProductPostingGroup { get; set; }
        public bool ListPriceIsInclTax { get; set; }
        // Additional parameter set in ListPrice
        public string Attain_VATBusPostingGrPrice { get; set; }
        // Additional parameter set in LineDiscount
        public double LineDiscountPct { get; set; }
        // Additional parameters set in OrderLineTotal
        public double SubTotal { get; set; }
        public double SubTotalInclTax { get; set; }
        public double OrderLineTotal { get; set; }
        // Additional parameters set in CalculateVat
        public decimal InvoiceDiscountAmount { get; set; }
        public decimal InvoiceDiscountAmountInclTax { get; set; }

        // A list of identifiers used in "IN" SQL statements
        public string ItemCustomerDiscountGroupList { get; set; }
    }

    public class VatNode
    {
        public decimal VatSum { get; set; }
        public decimal InvoiceDiscountAmountSumExclVAT { get; set; }
        public decimal InvoiceDiscountAmountSumInclVAT { get; set; }
    }
}
