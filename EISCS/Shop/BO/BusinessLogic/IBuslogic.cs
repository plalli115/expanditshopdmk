﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CmsPublic.ModelLayer.Models;
using EISCS.ExpandITFramework.Infrastructure;
using EISCS.Shop.DO.Dto.Cart;

namespace EISCS.Shop.BO.BusinessLogic
{
    public interface IBuslogic
    {
        PageMessages CalculateOrder<T>(BaseOrderHeader<T> order, string currency, string defaultCurrency) where T : BaseOrderLine;
        void PromoShippingDiscounts<T>(BaseOrderHeader<T> order, string defaultCurrency) where T : BaseOrderLine;
    }
}
