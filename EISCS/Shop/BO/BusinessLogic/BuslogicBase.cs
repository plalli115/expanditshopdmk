﻿using System;
using System.Collections.Generic;
using EISCS.ExpandITFramework.Infrastructure;
using EISCS.Shop.DO.Dto.Cart;

namespace EISCS.Shop.BO.BusinessLogic
{
    public abstract class BuslogicBase : IBuslogic
    {
        protected ProductQuantity ProductQuantities;
        public abstract PageMessages CalculateOrder<T>(BaseOrderHeader<T> order, string currency, string defaultCurrency) where T : BaseOrderLine;
        public abstract void PromoShippingDiscounts<T>(BaseOrderHeader<T> order, string defaultCurrency) where T : BaseOrderLine;
        /// <summary>
        /// Internal class to hold information about product quantities
        /// </summary>
        public class ProductQuantity
        {
            public ProductQuantity()
            {
                QuantityDict = new Dictionary<string, QuantityNode>();
            }

            private class QuantityNode
            {
                public QuantityNode()
                {
                    Variants = new Dictionary<string, double>();
                }
                public double Quantity { get; set; }
                public Dictionary<string, double> Variants { get; set; }
            }

            private Dictionary<string, QuantityNode> QuantityDict { get; set; }

            public void Add(string productGuid, string variantCode, double quantity)
            {
                if (!QuantityDict.ContainsKey(productGuid))
                {
                    var node = new QuantityNode { Quantity = quantity };
                    if (!string.IsNullOrEmpty(variantCode))
                    {
                        node.Variants.Add(variantCode, quantity);
                    }
                    QuantityDict.Add(productGuid, node);
                }
                else
                {
                    var node = QuantityDict[productGuid];
                    node.Quantity += quantity;
                    if (!string.IsNullOrEmpty(variantCode))
                    {
                        if (!node.Variants.ContainsKey(variantCode))
                        {
                            node.Variants.Add(variantCode, quantity);
                        }
                        else
                        {
                            node.Variants[variantCode] += quantity;
                        }
                    }
                }
            }

            public double GetProductQuantities(string productGuid)
            {
                if (!QuantityDict.ContainsKey(productGuid))
                {
                    return 0;
                }

                return QuantityDict[productGuid].Quantity;
            }

            public double GetVariantQuantities(string productGuid, string variantCode)
            {
                if (!QuantityDict.ContainsKey(productGuid))
                {
                    return 0;
                }

                return !QuantityDict[productGuid].Variants.ContainsKey(variantCode) ? 0 : QuantityDict[productGuid].Variants[variantCode];
            }

            public bool VariantsExist(string productGuid)
            {
                if (!QuantityDict.ContainsKey(productGuid))
                {
                    return false;
                }

                return QuantityDict[productGuid].Variants.Count > 0;
            }
        }

        protected void AccumulateProductQuantities<T>(BaseOrderHeader<T> order) where T : BaseOrderLine
        {
            ProductQuantities = new ProductQuantity();

            foreach (var line in order.Lines)
            {
                ProductQuantities.Add(line.ProductGuid, line.VariantCode, line.Quantity);
            }
        }

        
    }
}
