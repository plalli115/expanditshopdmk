﻿using System;
using System.Collections.Generic;
using System.Linq;
using CmsPublic.DataRepository;
using EISCS.Shop.DO.Dto.Backend.XAL;
using EISCS.Shop.DO.Interface;

namespace EISCS.Shop.BO.BusinessLogic.Xal
{
    public interface IXalDataService
    {
        CustomerTableXal SelectFromCustomerTable(string customerGuid);
        List<XalProductTable> SelectFromProductTable(IEnumerable<string> productGuids);
        XAL_VatCodeRate GetVatCodeRate(string vatCode, string systemDate);
        IEnumerable<XAL_Parameters> GetXalPriceParameters();

        IEnumerable<XAL_PriceDisc> GetFromXalPriceDiscs(string fAccountcode, string fItemCode, string fRelation, string fAccountrelation, string fItemrelation, string systemDate,
            string currencyGuid, bool useNullExchange);

        IEnumerable<XAL_PriceDisc> GetAnyFromXalPriceDiscs(string sql);
        LedgerEntryContainer GetCustomerLedgerEntries(string customerGuid);
        IEnumerable<XAL_PriceDisc> GetTradeAgreements(CustomerTableXal customer, DateTime systemDate, string orderCurrency, bool useNullExchange);
    }

    public class XalDataService : BuslogicBaseDataService, IXalDataService
    {
        public XalDataService(IExpanditDbFactory dbFactory)
            : base(dbFactory)
        {
        }

        public CustomerTableXal SelectFromCustomerTable(string customerGuid)
        {
            string sql =
                "SELECT TOP 1 * FROM CustomerTable WHERE CustomerGuid = @customerGuid";
            return GetResults<CustomerTableXal>(sql, new { customerGuid }).SingleOrDefault();
        }

        public List<XalProductTable> SelectFromProductTable(IEnumerable<string> productGuids)
        {
            string[] enumerable = productGuids as string[] ?? productGuids.ToArray();
            return !enumerable.Any() ? null : GetResults<XalProductTable>("SELECT * FROM ProductTable WHERE ProductGuid IN @productGuids", new {productGuids = enumerable}).ToList();
        }

        public XAL_VatCodeRate GetVatCodeRate(string vatCode, string systemDate)
        {
            string sql =
                "SELECT VatPct FROM XAL_VatCodeRate" +
                " WHERE VatCode = " + ExpanditLib2.SafeString(vatCode) +
                " AND (" + ExpanditLib2.SafeDatetime(systemDate) + " >= FromDate OR FromDate = " + ExpanditLib2.GetZeroDate() + ")" +
                " AND (" + ExpanditLib2.SafeDatetime(systemDate) + " <= ToDate OR ToDate = " + ExpanditLib2.GetZeroDate() + ")";
            return GetResults<XAL_VatCodeRate>(sql).SingleOrDefault();
        }

        public IEnumerable<XAL_Parameters> GetXalPriceParameters()
        {
            return GetResults<XAL_Parameters>("SELECT * FROM XAL_Parameters WHERE Name='SYSTEMPARAMETERS' OR Name='SYSTEMPARAMETRE' ORDER BY UserID, [Name]");
        }

        public IEnumerable<XAL_PriceDisc> GetFromXalPriceDiscs(string fAccountcode, string fItemCode, string fRelation, string fAccountrelation, string fItemrelation, string systemDate,
            string currencyGuid, bool useNullExchange)
        {
            string sql =
                "SELECT QuantityAmount, PriceUnit, Amount, Deliverytime, ExternalItemNo, Percent1, Percent2, LookforForward FROM XAL_PriceDisc WHERE " +
                "AccountCode = " + fAccountcode +
                " AND ItemCode = " + fItemCode +
                " AND Relation = " + fRelation +
                " AND AccountRelation " + fAccountrelation +
                " AND ItemRelation " + fItemrelation +
                " AND (" + ExpanditLib2.SafeDatetime(systemDate) + " >= FromDate OR FromDate = " + ExpanditLib2.GetZeroDate() + ")" +
                " AND (" + ExpanditLib2.SafeDatetime(systemDate) + " <= ToDate OR ToDate = " + ExpanditLib2.GetZeroDate() + ")" +
                " AND (Exchange = " + ExpanditLib2.SafeString(currencyGuid);

            if (useNullExchange)
            {
                sql += " OR Exchange is NULL";
            }

            sql += ") ORDER BY Relation, ItemCode, ItemRelation, AccountCode, AccountRelation, Exchange, QuantityAmount";

            return GetResults<XAL_PriceDisc>(sql);
        }

        public IEnumerable<XAL_PriceDisc> GetAnyFromXalPriceDiscs(string sql)
        {
            return GetResults<XAL_PriceDisc>(sql);
        }

        public IEnumerable<XAL_PriceDisc> GetTradeAgreements(CustomerTableXal customer, DateTime systemDate, string orderCurrency, bool useNullExchange)
        {
            string currencyCondition = "";
            if (useNullExchange)
            {
                currencyCondition += " OR Exchange is NULL";
            }
            currencyCondition = "(Exchange = @currencyGuid " + currencyCondition + ")";
            
            string sql = @" SELECT      [Amount],[AccountCode],[AccountRelation],[QuantityAmount],[PriceUnit],[Percent1],[Percent2],[Relation],[LookforForward],[ItemCode],[ItemRelation],[FromDate],[ToDate] 
                            FROM        XAL_PriceDisc
                            WHERE       Relation IN (4, 5, 6, 7) 
                                        AND (
                                                (AccountCode = 0 AND AccountRelation = @customerGuid) 
                                                OR (AccountCode = 1 AND (   AccountRelation = @priceGroup OR 
                                                                            AccountRelation = @lineDisc OR 
                                                                            AccountRelation = @endDisc) ) 
                                                OR (AccountCode = 2) 
                                            ) 
                                        AND (@systemDatePlusOneSecond >= FromDate OR FromDate = @xalMinDate) 
                                        AND (@systemDate <= ToDate OR ToDate = @xalMinDate)
                                        AND ExternalItemNo IS NULL
                                        AND " + currencyCondition + @" 
                            ORDER BY    Relation, ItemCode, ItemRelation, AccountCode, AccountRelation, Exchange, QuantityAmount";

            var param = new
                {
                    customerGuid = customer.CustomerGuid,
                    priceGroup = customer.Pricegroup,
                    lineDisc = customer.Linedisc,
                    endDisc = customer.Enddisc,
                    currencyGuid = customer.CurrencyGuid,
                    systemDatePlusOneSecond = systemDate.AddSeconds(1),
                    systemDate,
                    xalMinDate = ExpanditLib2.GetZeroDate()
                };

            return GetResults<XAL_PriceDisc>(sql, param).ToList();
        }
        
    }
}