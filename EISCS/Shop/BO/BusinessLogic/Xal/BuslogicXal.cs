﻿using System;
using System.Collections.Generic;
using System.Linq;
using EISCS.ExpandITFramework.Infrastructure;
using EISCS.Shop.DO.Dto.Backend.XAL;
using EISCS.Shop.DO.Dto.Cart;
using EISCS.Shop.DO.Interface;
using EISCS.Wrappers.Configuration;
using ExpandIT;

namespace EISCS.Shop.BO.BusinessLogic.Xal
{
    public class BuslogicXal : BuslogicBase
    {
        private readonly IConfigurationObject _configurationObject;
        private readonly ICurrencyConverter _currencyConverter;
        private readonly IXalDataService _dataService;
        private readonly InfoObject _infoObject;
        private readonly IExpanditUserService _expanditUserService;

        public BuslogicXal(IConfigurationObject configuration, ICurrencyConverter currencyConverter, IXalDataService dataService, IExpanditUserService expanditUserService)
        {
            _configurationObject = configuration;
            _currencyConverter = currencyConverter;
            _dataService = dataService;
            _expanditUserService = expanditUserService;
            _infoObject = new InfoObject();
        }

        public override PageMessages CalculateOrder<T>(BaseOrderHeader<T> order, string currency, string defaultCurrency)
        {
            try
            {
                string customerGuid = GetCustomerGuid(order.UserGuid);
                InitOrder(order, customerGuid, currency);
                XalInitTradeAgreements();
                foreach (T line in order.Lines)
                {
                    CalcLine(order, line, defaultCurrency);
                }
                XalTotalOrderDisc(order);
                XalOrderTotal(order);
                order.IsCalculated = true;
            }
            catch (Exception ex)
            {
                var messages = new PageMessages();
                messages.Errors.Add(ex.Message);
                return messages;
            }
            return null;
        }

        private void CalcLine<T>(BaseOrderHeader<T> order, BaseOrderLine line, string defaultCurrency) where T : BaseOrderLine
        {
            XalInitLine(order, line, defaultCurrency);
            XalLineDiscount(order, line, defaultCurrency);
            XalLineAmount(line);
            XalLineDiscountAmount(line);
        }

        public void InitOrder<T>(BaseOrderHeader<T> order, string customerGuid, string currencyGuid) where T : BaseOrderLine
        {
            _infoObject.SystemDate = DateTime.Today;
            _infoObject.ProductList = GetProductGuidList(order);
            _infoObject.ProductsDict = _dataService.SelectFromProductTable(_infoObject.ProductList);
            _infoObject.XALVatOnTotalDiscount = ExpanditLib2.CBoolEx(_configurationObject.Read("XAL_VAT_ON_TOTAL_DISCOUNT"));
            _infoObject.XALResetLineDiscounts = ExpanditLib2.CBoolEx(_configurationObject.Read("XAL_RESET_LINE_DISCOUNTS"));
            _infoObject.XALNegDiscAsFee = ExpanditLib2.CBoolEx(_configurationObject.Read("XAL_NEG_DISC_AS_FEE"));
            _infoObject.XALUseInvoiceToAccount = ExpanditLib2.CBoolEx(_configurationObject.Read("XAL_USE_INVOICE_TO_ACCOUNT"));

            CustomerTableXal customerTable = _dataService.SelectFromCustomerTable(customerGuid);

            if (customerTable == null)
            {
                throw new Exception(string.Format("Error initializing order - Customer Account [{0}] was not found.", customerGuid));
            }

            string custNo = customerTable.BillToCustomerGuid;

            if (_infoObject.XALUseInvoiceToAccount && !string.IsNullOrEmpty(ExpanditLib2.CStrEx(custNo).Trim()))
            {
                customerTable = _dataService.SelectFromCustomerTable(custNo);
                if (customerTable == null)
                {
                    throw new Exception(string.Format("Error initializing order - Invoice to CustomerAccount [{0}] not found. ", custNo));
                }
            }

            _infoObject.CustomerRecord = customerTable;
            
            var headerParameter = new HeaderParameter
                {
                    CustomerGuid = customerGuid,
                    MultilnDisc = customerTable.Multilndisc,
                    EndDisc = customerTable.Enddisc,
                    Vatdutiable = ExpanditLib2.CBoolEx(customerTable.Vatdutiable),
                    VatCode = customerTable.VatCode,
                };

            // Set the additional order header parameter with initial values here
            order.HeaderParameter = headerParameter;

            order.TaxPct = XalGetTaxPct(customerTable.VatCode);
            order.ServiceCharge = 0;
            order.InvoiceDiscount = 0;

            order.CurrencyGuid = ExpanditLib2.CBoolEx(_configurationObject.Read("XAL_USE_CURRENCY_FROM_BILLTO_CUSTOMER")) ? customerTable.CurrencyGuid : currencyGuid;
            // Make sure to use the correct curency
            string multiCurrencySiteCurrency = _configurationObject.Read("MULTICURRENCY_SITE_CURRENCY");
            bool useNullExchange = multiCurrencySiteCurrency.Trim() == order.CurrencyGuid.Trim();
            _infoObject.TradeAgreement = _dataService.GetTradeAgreements(customerTable, _infoObject.SystemDate, order.CurrencyGuid, useNullExchange);
        }

        private void XalTotalOrderDisc<T>(BaseOrderHeader<T> order) where T : BaseOrderLine
        {
            // Calculates total discount, multiline discounts and fees

            double pdmDiscAmount;
            double pdmPercent1;
            double pdmPercent2;
            bool pdmAllItem = false;
            bool pdmEndfound = false;
            double pdmBalance = 0;
            double pdmBalanceTotal = 0;
            double totalDiscPct = 0;

            string sql;

            object[] items = _infoObject.PriceParameters.DictItems;

            string systemDate = _infoObject.SystemDate.ToShortDateString(); //Date

            ExpDictionary multiLineSum = new ExpDictionary();

            //
            // Multiline Discount
            //

            // Find all MultiLine discount groups in use on lines.

            foreach (var line in order.Lines)
            {
                if (!string.IsNullOrEmpty(ExpanditLib2.CStrEx(((LineParameter)line.LineParameter).Multilndisc)))
                {
                    if (_infoObject.XALResetLineDiscounts)
                    {
                        line.LineDiscount = 0;
                        line.LineDiscountAmount = 0;
                    }

                    if (!multiLineSum.ContainsKey(((LineParameter)line.LineParameter).Multilndisc))
                    {
                        multiLineSum[((LineParameter)line.LineParameter).Multilndisc] = 0;
                    }

                    double multiLineDiscValue = (double)multiLineSum[((LineParameter)line.LineParameter).Multilndisc];
                    multiLineSum[((LineParameter)line.LineParameter).Multilndisc] = multiLineDiscValue + line.Quantity;

                }
            }


            if (multiLineSum.Count > 0)
            {
                object[] mulkeys = multiLineSum.DictKeys;
                object[] mulitems = multiLineSum.DictItems;

                for (int i = 0; i <= multiLineSum.Count - 1; i++)
                {
                    pdmDiscAmount = 0;
                    pdmPercent1 = 0;
                    pdmPercent2 = 0;

                    // for each Multiline Discount group get the discount


                    for (int j = 5; j <= 19; j += 7)
                    {
                        for (int k = 0; k <= 1; k++)
                        {
                            if (ExpanditLib2.CBoolEx(items[j + k - 1]))
                            {
                                string fItemCode;
                                switch (k)
                                {
                                    case 0:
                                        // -- "0" = Item Grp 1
                                        fItemCode = "ItemCode = 1 AND ItemRelation = " + ExpanditLib2.SafeString(mulkeys[i]);
                                        break;
                                    case 1:
                                        // -- "1" = All Items 2
                                        fItemCode = "ItemCode = 2 AND ItemRelation is null";
                                        break;
                                    default:
                                        fItemCode = "";
                                        break;
                                }

                                string fAccountcode;
                                switch (j)
                                {
                                    case 5:
                                        // -- CustomerAccount = 0
                                        fAccountcode = "AccountCode = 0 AND AccountRelation = " + ExpanditLib2.SafeString(((HeaderParameter)order.HeaderParameter).CustomerGuid);
                                        break;
                                    case 12:
                                        // -- MultiLineDisc = 1
                                        fAccountcode = "AccountCode = 1 AND AccountRelation = " + ExpanditLib2.SafeString(((HeaderParameter)order.HeaderParameter).MultilnDisc);
                                        break;
                                    case 19:
                                        // -- All = 2
                                        fAccountcode = "AccountCode = 2 AND AccountRelation is null ";
                                        break;
                                    default:
                                        fAccountcode = "";
                                        break;
                                }

                                sql = "SELECT Amount, Percent1, Percent2, LookforForward FROM XAL_PriceDisc" + " WHERE Relation = 6" + " AND " + fItemCode + " AND " + fAccountcode + " AND QuantityAmount <= " + ExpanditLib2.CDblEx(mulitems[i]) + " AND (" + ExpanditLib2.SafeDatetime(systemDate) + " >= FromDate OR FromDate = " + ExpanditLib2.GetZeroDate() + ")" + " AND (" + ExpanditLib2.SafeDatetime(systemDate) + " <= ToDate OR ToDate = " + ExpanditLib2.GetZeroDate() + ")" + " AND (Exchange = " + ExpanditLib2.SafeString(order.CurrencyGuid.Trim());

                                if (order.CurrencyGuid.Trim() == _configurationObject.Read("MULTICURRENCY_SITE_CURRENCY").Trim())
                                {
                                    sql = sql + " OR Exchange is NULL";
                                }

                                sql = sql + ") ORDER BY Relation, ItemCode, ItemRelation, AccountCode, AccountRelation, Exchange, QuantityAmount";

                                IEnumerable<XAL_PriceDisc> rsPriceDisc = _dataService.GetAnyFromXalPriceDiscs(sql);

                                foreach (XAL_PriceDisc priceDisc in rsPriceDisc)
                                {
                                    pdmDiscAmount = pdmDiscAmount + priceDisc.Amount;
                                    pdmPercent1 = pdmPercent1 + priceDisc.Percent1;
                                    pdmPercent2 = pdmPercent2 + priceDisc.Percent2;

                                    // -- All Items ItemCode = 2
                                    if (k == 1)
                                    {
                                        pdmAllItem = true;
                                    }

                                    if (!ExpanditLib2.CBoolEx(priceDisc.LookforForward))
                                    {
                                        // -- Stop search
                                        j = 26;
                                        k = 2;
                                    }
                                }

                            }
                            // -- Next k
                        }
                        // -- Next j
                    }

                    pdmPercent1 = 100 * (1 - (1 - pdmPercent1 / 100) * (1 - pdmPercent2 / 100));

                    // For each line in order - add the discount.
                    foreach (var line in order.Lines)
                    {
                        if (((LineParameter)line.LineParameter).Multilndisc == (string)mulkeys[i] || pdmAllItem)
                        {
                            if (pdmPercent1 > line.LineDiscount)
                            {
                                line.LineDiscount = pdmPercent1;
                            }

                            if (pdmDiscAmount > line.LineDiscountAmount)
                            {
                                line.LineDiscountAmount = pdmDiscAmount;
                            }

                            line.LineTotal = _currencyConverter.ConvertCurrency(line.Quantity * (line.ListPrice - line.LineDiscountAmount) * (100 - line.LineDiscount) / 100,
                                order.CurrencyGuid, line.CurrencyGuid);
                        }
                    }

                }
            }

            //
            // Balance Total for use in Total discount
            //

            foreach (var line in order.Lines)
            {
                if (((LineParameter)line.LineParameter).Enddisc == 1)
                {
                    pdmBalance += _currencyConverter.ConvertCurrency(line.LineTotal, line.CurrencyGuid, order.CurrencyGuid);
                    pdmEndfound = true;
                }
                pdmBalanceTotal += _currencyConverter.ConvertCurrency(line.LineTotal, line.CurrencyGuid, order.CurrencyGuid);
            }

            //
            // Total Order Discount
            //

            pdmDiscAmount = 0;
            pdmPercent1 = 0;
            pdmPercent2 = 0;

            if (pdmEndfound)
            {

                for (int i = 7; i <= 21; i += 7)
                {
                    string pdmArel;
                    switch (i)
                    {
                        case 7:
                            // -- CustomerAccount = 0
                            pdmArel = "AccountCode = 0 AND AccountRelation = " + ExpanditLib2.SafeString(((HeaderParameter)order.HeaderParameter).CustomerGuid);
                            break;
                        case 14:
                            // -- EndDisc = 1
                            pdmArel = "AccountCode = 1 AND AccountRelation = " + ExpanditLib2.SafeString(((HeaderParameter)order.HeaderParameter).EndDisc);
                            break;
                        case 21:
                            // -- All = 2
                            pdmArel = "AccountCode = 2 AND AccountRelation is null ";
                            break;
                        default:
                            pdmArel = "";
                            break;
                    }


                    if (ExpanditLib2.CBoolEx(items[i - 1]))
                    {
                        sql = "SELECT Amount, Percent1, Percent2, LookforForward FROM XAL_PriceDisc" + " WHERE Relation = 7" + " AND ItemCode = 2 AND ItemRelation is null" + " AND " + pdmArel + " AND QuantityAmount <= " + ExpanditLib2.SafeFloat(ExpanditLib2.Abs(pdmBalance)) + " AND (" + ExpanditLib2.SafeDatetime(systemDate) + " >= FromDate OR FromDate = " + ExpanditLib2.GetZeroDate() + ")" + " AND (" + ExpanditLib2.SafeDatetime(systemDate) + " <= ToDate OR ToDate = " + ExpanditLib2.GetZeroDate() + ")" + " AND (Exchange = " + ExpanditLib2.SafeString(order.CurrencyGuid);

                        if (order.CurrencyGuid == _configurationObject.Read("MULTICURRENCY_SITE_CURRENCY").Trim())
                        {
                            sql = sql + " OR Exchange is NULL";
                        }

                        sql = sql + ") ORDER BY Relation, ItemCode, ItemRelation, AccountCode, AccountRelation, Exchange, QuantityAmount";

                        var rsPriceDisc = _dataService.GetAnyFromXalPriceDiscs(sql);

                        foreach (var disc in rsPriceDisc)
                        {
                            if (ExpanditLib2.CBoolEx(disc.Amount))
                            {
                                pdmDiscAmount = pdmDiscAmount + disc.Amount;
                            }
                            if (ExpanditLib2.CBoolEx(disc.Percent1))
                            {
                                pdmPercent1 = pdmPercent1 + disc.Percent1;
                            }
                            if (ExpanditLib2.CBoolEx(disc.Percent2))
                            {
                                pdmPercent2 = pdmPercent2 + disc.Percent2;
                            }

                            //
                            if (!ExpanditLib2.CBoolEx(disc.LookforForward))
                            {
                                break;
                            }
                        }

                    }
                }
            }

            // -- calculate total Discount Pct, round to one decimal, and calculate DiscountAmount. This is the way XAL does it.
            if (pdmPercent1 != 0 || pdmPercent2 != 0)
            {
                if (pdmBalance > 0)
                {
                    totalDiscPct = ExpanditLib2.RoundEx(((pdmDiscAmount / pdmBalance) + (1 - (1 - pdmPercent1 / 100) * (1 - pdmPercent2 / 100))) * 100, 1);
                }
                pdmDiscAmount = pdmBalance * (totalDiscPct / 100);
            }

            //
            // Total Fee or not fee
            //

            if (pdmDiscAmount * pdmBalanceTotal > 0 || !_infoObject.XALNegDiscAsFee)
            {
                if (_infoObject.XALNegDiscAsFee)
                {
                    order.ServiceCharge = 0;
                }
                if (pdmBalanceTotal > 0)
                {
                    order.InvoiceDiscountPct = (100 * pdmDiscAmount / pdmBalanceTotal).ToString();
                    order.InvoiceDiscount = pdmDiscAmount;
                }
                else
                {
                    order.InvoiceDiscountPct = "0";
                    order.InvoiceDiscount = 0;
                }
            }
            else
            {
                order.InvoiceDiscountPct = "0";
                order.InvoiceDiscount = 0;
                order.ServiceCharge = -pdmDiscAmount;
            }
        }

        private string XalGetTaxPct(string vatCode)
        {
            XAL_VatCodeRate vatCodeRate = _dataService.GetVatCodeRate(vatCode, _infoObject.SystemDate.ToShortDateString());
            return vatCodeRate != null ? vatCodeRate.VatPct.ToString() : "0";
        }

        public void XalOrderTotal<T>(BaseOrderHeader<T> order) where T : BaseOrderLine
        {
            double amountSumVat = 0;
            double amountSum = 0;
            foreach (T line in order.Lines)
            {
                double taxPct = 0;
                if (((HeaderParameter)order.HeaderParameter).Vatdutiable)
                {
                    if (!string.IsNullOrEmpty(ExpanditLib2.CStrEx(((LineParameter)line.LineParameter).VatCode)))
                    {
                        if (!string.IsNullOrEmpty(ExpanditLib2.CStrEx(((HeaderParameter)order.HeaderParameter).VatCode)))
                        {
                            // -- If vatcode on order - use that
                            ((LineParameter)line.LineParameter).VatCode = ((HeaderParameter)order.HeaderParameter).VatCode;
                        }

                        taxPct = (ExpanditLib2.ConvertToDbl(XalGetTaxPct(((LineParameter)line.LineParameter).VatCode)));

                        line.TaxAmount = line.LineTotal * (taxPct / 100);
                    }
                    else
                    {
                        // Calculate no VAT on order if not VATCode specified
                        line.TaxAmount = 0;
                    }
                }
                else
                {
                    line.TaxAmount = 0;
                }

                line.ListPriceInclTax = line.ListPrice * (1 + taxPct / 100);
                line.LineDiscountAmountInclTax = line.LineDiscountAmount * (1 + (taxPct / 100));
                line.TotalInclTax = line.LineTotal + line.TaxAmount;

                // Write TaxPct to line
                line.TaxPct = taxPct.ToString();

                amountSumVat = amountSumVat + line.TotalInclTax;
                amountSum = amountSum + line.LineTotal;
            }

            // -- Sum of lines

            {
                order.SubTotal = amountSum;
                order.SubTotalInclTax = amountSumVat;
                double taxPct = 0;

                if (_infoObject.XALVatOnTotalDiscount)
                {
                    taxPct = (ExpanditLib2.ConvertToDbl(XalGetTaxPct(((HeaderParameter)order.HeaderParameter).VatCode)));

                    order.InvoiceDiscountInclTax = order.InvoiceDiscount * (1 + (taxPct / 100));
                }
                else
                {
                    order.InvoiceDiscountInclTax = order.InvoiceDiscount;
                }

                order.ServiceChargeInclTax = order.ServiceCharge * (1 + (taxPct / 100));
            }

            order.Total = amountSum - order.InvoiceDiscount + order.ServiceCharge;
            order.TotalInclTax = amountSumVat - order.InvoiceDiscountInclTax + order.ServiceChargeInclTax;
            order.TaxAmount = order.TotalInclTax - order.Total;
        }

        public void XalInitTradeAgreements()
        {
            IEnumerable<XAL_Parameters> x = _dataService.GetXalPriceParameters();
            XAL_Parameters dataItem = x.FirstOrDefault();

            if (dataItem == null)
            {
                throw new Exception("Problem calculating order : XAL_Parameters table not found - please make sure that all tables are extracted.");
            }

            var priceParameters = new ExpDictionary();
            var priceMatrix = new ExpDictionary();

            int pdSalesPrLi = ExpanditLib2.CIntEx(dataItem.Real1);
            int pdSalesMuEn = ExpanditLib2.CIntEx(dataItem.Real2);

            // --
            // -- Generate Trade agreement parameters from XAL
            // --

            int codeSaPr = pdSalesPrLi & 65535;
            int codeSaLi = (pdSalesPrLi / 65536) & 65535;
            int codeSaMu = pdSalesMuEn & 65535;
            int codeSaEn = (pdSalesMuEn / 65536) & 65535;

            priceParameters.Add("SALESORDERPRICEACCOUNTITEM", (codeSaPr & 1) != 0);
            priceParameters.Add("SALESORDERLINEACCOUNTITEM", (codeSaLi & 1) != 0);
            priceParameters.Add("SALESORDERLINEACCOUNTGROUP", (codeSaLi & 2) != 0);
            priceParameters.Add("SALESORDERLINEACCOUNTALL", (codeSaLi & 4) != 0);
            priceParameters.Add("SALESORDERMULTILNACCOUNTGROUP", (codeSaMu & 2) != 0);
            priceParameters.Add("SALESORDERMULTILNACCOUNTALL", (codeSaMu & 4) != 0);
            priceParameters.Add("SALESORDERENDACCOUNTALL", (codeSaEn & 4) != 0);

            priceParameters.Add("SALESORDERPRICEGROUPITEM", (codeSaPr & 8) != 0);
            priceParameters.Add("SALESORDERLINEGROUPITEM", (codeSaLi & 8) != 0);
            priceParameters.Add("SALESORDERLINEGROUPGROUP", (codeSaLi & 16) != 0);
            priceParameters.Add("SALESORDERLINEGROUPALL", (codeSaLi & 32) != 0);
            priceParameters.Add("SALESORDERMULTILNGROUPGROUP", (codeSaMu & 16) != 0);
            priceParameters.Add("SALESORDERMULTILNGROUPALL", (codeSaMu & 32) != 0);
            priceParameters.Add("SALESORDERENDGROUPALL", (codeSaEn & 32) != 0);

            priceParameters.Add("SALESORDERPRICEALLITEM", (codeSaPr & 64) != 0);
            priceParameters.Add("SALESORDERLINEALLITEM", (codeSaLi & 64) != 0);
            priceParameters.Add("SALESORDERLINEALLGROUP", (codeSaLi & 128) != 0);
            priceParameters.Add("SALESORDERLINEALLALL", (codeSaLi & 256) != 0);
            priceParameters.Add("SALESORDERMULTILNALLGROUP", (codeSaMu & 128) != 0);
            priceParameters.Add("SALESORDERMULTILNALLALL", (codeSaMu & 256) != 0);
            priceParameters.Add("SALESORDERENDALLALL", (codeSaEn & 256) != 0);

            // --
            // -- Trade Agreement search order...
            // --

            // Customer Account
            priceMatrix["SALESORDERPRICEACCOUNTITEM"] = "004"; // -- Price 1
            priceMatrix["SALESORDERLINEACCOUNTITEM"] = "005"; // -- Line  2
            priceMatrix["SALESORDERLINEACCOUNTGROUP"] = "015"; // -- Line  3
            priceMatrix["SALESORDERLINEACCOUNTALL"] = "025"; // -- Line  4
            priceMatrix["SALESORDERMULTILNACCOUNTGROUP"] = "016"; // -- Multi 5
            priceMatrix["SALESORDERMULTILNACCOUNTALL"] = "026"; // -- Multi 6
            priceMatrix["SALESORDERENDACCOUNTALL"] = "027"; // -- All [Invoice] 7

            // Customer Group
            priceMatrix["SALESORDERPRICEGROUPITEM"] = "104"; // -- Price 8
            priceMatrix["SALESORDERLINEGROUPITEM"] = "105"; // -- Line 9
            priceMatrix["SALESORDERLINEGROUPGROUP"] = "115"; // -- Line 10
            priceMatrix["SALESORDERLINEGROUPALL"] = "125"; // -- Line 11
            priceMatrix["SALESORDERMULTILNGROUPGROUP"] = "116"; // -- Multi 12
            priceMatrix["SALESORDERMULTILNGROUPALL"] = "126"; // -- Multi 13
            priceMatrix["SALESORDERENDGROUPALL"] = "127"; // -- All [Invoice] 14

            // All Customers
            priceMatrix["SALESORDERPRICEALLITEM"] = "204"; // -- Price 15
            priceMatrix["SALESORDERLINEALLITEM"] = "205"; // -- Line 16
            priceMatrix["SALESORDERLINEALLGROUP"] = "215"; // -- Line 17
            priceMatrix["SALESORDERLINEALLALL"] = "225"; // -- Line 18
            priceMatrix["SALESORDERMULTILNALLGROUP"] = "216"; // -- Multi 19
            priceMatrix["SALESORDERMULTILNALLALL"] = "226"; // -- Multi 20
            priceMatrix["SALESORDERENDALLALL"] = "227"; // -- All [Invoice] 21

            _infoObject.PriceParameters = priceParameters;
            _infoObject.PriceMatrix = priceMatrix;

            _infoObject.BackEnd = "XAL270";
        }

        private void XalInitLine<T>(BaseOrderHeader<T> order, BaseOrderLine line, string defaultCurrency) where T : BaseOrderLine
        {
            string productGuid = line.ProductGuid;
            XalProductTable productDict = _infoObject.ProductsDict.FirstOrDefault(x => x.ProductGuid == productGuid);

            if (productDict == null)
            {
                order.Lines.RemoveAll(x => x.ProductGuid == productGuid);
                string errorMessage = string.Format("{0} [{1}] {2}", Resource.GetLabel("MESSAGE_INTERNAL_PRODUCTGUID"), productGuid, Resource.GetLabel("MESSAGE_IS_NOT_AVAILABLE"));
                throw new Exception(errorMessage);
            }

            // Make sure that currency is the same on all lines on order.
            line.CurrencyGuid = order.CurrencyGuid;
            line.ListPrice = _currencyConverter.ConvertCurrency(productDict.ListPrice, defaultCurrency, line.CurrencyGuid);

            line.LineParameter = new LineParameter();
            ((LineParameter)line.LineParameter).LineDiscGrp = productDict.Linedisc;
            ((LineParameter)line.LineParameter).Multilndisc = productDict.Multilndisc;
            ((LineParameter)line.LineParameter).Enddisc = productDict.Enddisc;
            ((LineParameter)line.LineParameter).VatCode = productDict.SalesVatCode;

            line.LineDiscount = 0;
            line.LineDiscountAmount = 0;
            line.LineTotal = 0;
        }

        private void XalLineDiscount<T>(BaseOrderHeader<T> order, BaseOrderLine line, string defaultCurrency) where T : BaseOrderLine
        {
            // XAL/Axapta tradeagreement description
            //
            //                     ACCOUNT NO.        ACCOUNT GROUP       ALL ACCOUNTS
            //                   Item Grp  All       Item Grp   All      Item Grp   All
            // SALES
            // +------------------------------------------------------------------------+
            // +Price........:  0( )                7( )               14( )            +
            // +Line disc....:  1( ) 2( ) 3( )      8( ) 9( )10( )     15( )16( )17( )  +
            // +Multi disc...:       4( ) 5( )          11( )12( )          18( )19( )  +
            // +Total disc...:            6( )               13( )               20( )  +
            // +------------------------------------------------------------------------+
            //                   Search direction ---->
            //

            string productGuid = line.ProductGuid;
            XalProductTable rsProduct = _infoObject.ProductsDict.FirstOrDefault(x => x.ProductGuid == productGuid);

            if (rsProduct == null)
            {
                string errorMessage = string.Format("{0} [{1}] {2}", Resource.GetLabel("MESSAGE_INTERNAL_PRODUCTGUID"), productGuid, Resource.GetLabel("MESSAGE_IS_NOT_AVAILABLE"));
                throw new Exception(errorMessage);
            }

            double pdPercent1 = 0;
            double pdPercent2 = 0;
            bool pLook = true;
            bool lLook = true;

            bool pDel = false; // TODO: Find if this is actually used anywhere. Comes from old VB code.

            var searchKeys = new int[2][];
            searchKeys[0] = new[] { 0, 7, 14 };
            searchKeys[1] = new[] { 1, 2, 3, 8, 9, 10, 15, 16, 17 };

            object[] items = _infoObject.PriceParameters.DictItems;
            object[] keys = _infoObject.PriceParameters.DictKeys;

            for (int i = 0; i <= 1; i++)
            {
                for (int j = 0; j < searchKeys[i].Length; j++)
                {
                    string faccountRelation = "";
                    string fItemRelation = "";
                    var tradeArgName = keys[searchKeys[i][j]] as string;
                    var tradeAgrValue = (bool)items[searchKeys[i][j]];

                    if (tradeAgrValue)
                    {
                        string fAccountcode = ((string)_infoObject.PriceMatrix[tradeArgName]).Substring(0, 1);
                        string fItemCode = ((string)_infoObject.PriceMatrix[tradeArgName]).Substring(1, 1);
                        string fRelation = ((string)_infoObject.PriceMatrix[tradeArgName]).Substring(2, 1);

                        switch (fAccountcode)
                        {
                            case "0":
                                // CustomerGuid
                                faccountRelation += string.Format("{0}", _infoObject.CustomerRecord.CustomerGuid);
                                break;
                            case "1":
                                // CustomerGroup
                                switch (fRelation)
                                {
                                    case "4":
                                        // Price
                                        faccountRelation += string.Format("{0}", _infoObject.CustomerRecord.Pricegroup);
                                        break;
                                    case "5":
                                        // Line
                                        faccountRelation += string.Format("{0}", _infoObject.CustomerRecord.Linedisc);
                                        break;
                                }
                                break;
                            case "2":
                                // All
                                faccountRelation = " IS NULL";
                                break;
                            default:
                                faccountRelation = "";
                                break;
                        }

                        switch (fItemCode)
                        {
                            case "0":
                                // itemNumber
                                fItemRelation += string.Format("{0}", line.ProductGuid);
                                break;
                            case "1":
                                // ItemGroup
                                XalProductTable firstOrDefault = _infoObject.ProductsDict.FirstOrDefault(x => x.ProductGuid == line.ProductGuid);
                                if (firstOrDefault != null)
                                {
                                    fItemRelation += string.Format("{0}", firstOrDefault.Linedisc);
                                }
                                break;
                            case "2":
                                // All
                                fItemRelation = " IS NULL ";
                                break;
                            default:
                                fItemRelation = "";
                                break;
                        }

                        //string multiCurrencySiteCurrency = _configurationObject.Read("MULTICURRENCY_SITE_CURRENCY");

                        //bool useNullExchange = multiCurrencySiteCurrency.Trim() == order.CurrencyGuid.Trim();

                        //var rsTradeAgr = _dataService.GetFromXalPriceDiscs(fAccountcode, fItemCode, fRelation, faccountRelation, fItemRelation,
                        //    _infoObject.SystemDate.ToShortDateString(), order.CurrencyGuid, useNullExchange);

                        var rsTradeAgr = _infoObject.TradeAgreement.Where(t => "" + t.AccountCode == fAccountcode &&
                                                                                                            "" + t.ItemCode == fItemCode &&
                                                                                                            "" + t.Relation == fRelation &&
                                                                                                            t.AccountRelation == faccountRelation &&
                                                                                                            t.ItemRelation == fItemRelation);

                        foreach (var tradeAgrItem in rsTradeAgr)
                        {
                            if (pDel)
                            {
                                pDel = false;
                                if (fRelation == "4")
                                {
                                    line.ListPrice = 0;
                                }
                                else if (fRelation == "5")
                                {
                                    line.LineDiscountAmount = 0;
                                }
                            }

                            if (tradeAgrItem.QuantityAmount <= line.Quantity)
                            {
                                if (fRelation == "4" && pLook)
                                {
                                    double tmpAmount = 0;
                                    if (ExpanditLib2.CBoolEx(tradeAgrItem.PriceUnit))
                                    {
                                        tmpAmount = tradeAgrItem.Amount / tradeAgrItem.PriceUnit;
                                    }
                                    else
                                    {
                                        tmpAmount = tradeAgrItem.Amount;
                                    }

                                    if ((ExpanditLib2.CBoolEx(tradeAgrItem.Amount) && tmpAmount < line.ListPrice) || !ExpanditLib2.CBoolEx(line.ListPrice))
                                    {
                                        if (!ExpanditLib2.CBoolEx(tradeAgrItem.PriceUnit))
                                        {
                                            ((LineParameter)line.LineParameter).PriceUnit = 1.0;
                                        }
                                        else
                                        {
                                            ((LineParameter)line.LineParameter).PriceUnit = tradeAgrItem.PriceUnit;
                                        }

                                        line.ListPrice = tradeAgrItem.Amount / ((LineParameter)line.LineParameter).PriceUnit;

                                        if (ExpanditLib2.CBoolEx(tradeAgrItem.Deliverytime))
                                        {
                                            DateTime tmp1 = _infoObject.SystemDate.AddHours(tradeAgrItem.Deliverytime);

                                            if (tmp1 > ((LineParameter)line.LineParameter).ConfirmedDel)
                                            {
                                                ((LineParameter)line.LineParameter).ConfirmedDel = tmp1;
                                            }
                                        }
                                    }

                                    if (!ExpanditLib2.CBoolEx(((LineParameter)line.LineParameter).ExternalItemNo))
                                    {
                                        ((LineParameter)line.LineParameter).ExternalItemNo = tradeAgrItem.ExternalItemNo;
                                    }
                                }
                                else if (fRelation == "5" && lLook)
                                {
                                    line.LineDiscountAmount += tradeAgrItem.Amount;
                                    pdPercent1 += tradeAgrItem.Percent1;
                                    pdPercent2 += tradeAgrItem.Percent2;
                                }
                                if (!ExpanditLib2.CBoolEx(tradeAgrItem.LookforForward))
                                {
                                    pLook = pLook && (fRelation != "4");
                                    lLook = lLook && (fRelation != "5");
                                }
                            }
                        }
                    }
                }
            }

            if (!pDel)
            {
                line.LineDiscount = 100 * (1 - (1 - pdPercent1 / 100) * (1 - pdPercent2 / 100));
            }

            if (line.ListPrice <= 0)
            {
                line.ListPrice = rsProduct.ListPrice;
            }
        }

        private void XalLineAmount(BaseOrderLine line)
        {
            if (line.ListPrice > 0)
            {
                line.LineTotal = line.Quantity * (line.ListPrice - line.LineDiscountAmount) * (100 - line.LineDiscount) / 100;
            }
        }

        private void XalLineDiscountAmount(BaseOrderLine line)
        {
            // what to do here ? The original VB code: orderLine("LineDiscountAmount") = orderLine("LineDiscountAmount")
        }

        private List<string> GetProductGuidList<T>(BaseOrderHeader<T> order) where T : BaseOrderLine
        {
            return order.Lines.Select(line => line.ProductGuid).ToList();
        }

        private string GetCustomerGuid(string userGuid)
        {
            var userTable = _expanditUserService.GetUser(userGuid);

            if (userTable == null)
            {
                return _configurationObject.Read("ANONYMOUS_CUSTOMERGUID");
            }

            return string.IsNullOrEmpty(userTable.CustomerGuid) ? _configurationObject.Read("ANONYMOUS_CUSTOMERGUID") : userTable.CustomerGuid;
        }

        public override void PromoShippingDiscounts<T>(BaseOrderHeader<T> order, string defaultCurrency)
        {
            throw new NotImplementedException();
        }
    }

    internal class InfoObject
    {
        public DateTime SystemDate { get; set; }
        public List<string> ProductList { get; set; }
        public IEnumerable<XalProductTable> ProductsDict { get; set; }
        public bool XALVatOnTotalDiscount { get; set; }
        public bool XALResetLineDiscounts { get; set; }
        public bool XALNegDiscAsFee { get; set; }
        public bool XALUseInvoiceToAccount { get; set; }
        public CustomerTableXal CustomerRecord { get; set; }
        public ExpDictionary PriceParameters { get; set; }
        public ExpDictionary PriceMatrix { get; set; }
        public string BackEnd { get; set; }
        public IEnumerable<XAL_PriceDisc> TradeAgreement { get; set; }
    }

    public class LineParameter : ILineParameter
    {
        public string LineDiscGrp { get; set; }
        public string Multilndisc { get; set; }
        public int Enddisc { get; set; }
        public string VatCode { get; set; }
        public double PriceUnit { get; set; }
        public DateTime ConfirmedDel { get; set; }
        public string ExternalItemNo { get; set; }
    }

    /// <summary>
    ///     Internal class to hold parameters used in price calculations
    /// </summary>
    public class HeaderParameter : IHeaderParameter
    {
        public string CustomerGuid { get; set; }
        public string MultilnDisc { get; set; }
        public string EndDisc { get; set; }
        public string VatCode { get; set; }
        public bool Vatdutiable { get; set; }
        public bool HasDiscount { get; set; }
    }
}