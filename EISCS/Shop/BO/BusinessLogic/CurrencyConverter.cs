﻿using System;
using System.Configuration;
using System.Linq;
using CmsPublic.DataRepository;
using EISCS.ExpandITFramework.Infrastructure;
using EISCS.Shop.DO;
using EISCS.Shop.DO.Dto.Currency;
using EISCS.Shop.DO.Interface;
using EISCS.Wrappers.Configuration;

namespace EISCS.Shop.BO.BusinessLogic
{
    public interface ICurrencyConverter
    {
        double ConvertCurrency(double inputAmount, string inputCurrency, string outputCurrency);
        decimal GetExchangeFactor(string fromCurrency, string toCurrency);
        string GetIso4217Code(string aCurrency);
    }

    public class CurrencyConverter : BaseDataService, ICurrencyConverter
    {
        private readonly IConfigurationObject _configuration;

        public CurrencyConverter(IExpanditDbFactory dbFactory, IConfigurationObject configuration)
            : base(dbFactory)
        {
            _configuration = configuration;
        }

        public double ConvertCurrency(double inputAmount, string inputCurrency, string outputCurrency)
        {
            if (string.IsNullOrEmpty(inputCurrency))
            {
                inputCurrency = _configuration.Read("MULTICURRENCY_SITE_CURRENCY");
            }

            if (string.IsNullOrEmpty(outputCurrency))
            {
                outputCurrency = _configuration.Read("MULTICURRENCY_SITE_CURRENCY");
            }

            if (inputCurrency == outputCurrency)
            {
                return inputAmount;
            }

            decimal exchangeFactor = GetExchangeFactor(inputCurrency, outputCurrency);

            if (exchangeFactor <= 0)
            {
                // In this case some kind of error happend, or the exchange factor was not calculated due to missing data.
                // Set bUseMultiCurrency = false to prevent displaying of Secondary Currency in the shop. Set exchFactor = 1
                // to complete the calculation.
                exchangeFactor = 1;
            }

            return inputAmount * (double)exchangeFactor;
        }

        public decimal GetExchangeFactor(string fromCurrency, string toCurrency)
        {
            // The easy case
            if (fromCurrency == toCurrency)
            {
                return 1;
            }

            // Try lookup
            decimal retval = LookupExchangeRate(fromCurrency, toCurrency);

            // Try triangulation
            if (retval == 0)
            {
                if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["MULTICURRENCY_TRIANG_CURRENCY"]))
                {
                    decimal triang1 = LookupExchangeRate(fromCurrency, ConfigurationManager.AppSettings["MULTICURRENCY_TRIANG_CURRENCY"]);
                    decimal triang2 = LookupExchangeRate(ConfigurationManager.AppSettings["MULTICURRENCY_TRIANG_CURRENCY"], toCurrency);
                    retval = triang1 * triang2;
                }
            }

            if (retval == 0)
            {
                //TODO: FIX Messages
                //globals.messages.Errors.Add("Unable to convert currency from " & fromCurrency & " to " & ToCurrency)
            }

            return retval;
        }

        protected decimal LookupExchangeRate(string fromCurrency, string toCurrency)
        {
            decimal retv;

            string cachekey = "Exchage-" + fromCurrency + "-" + toCurrency;
            object cachevalue = CacheManager.CacheGet(cachekey);
            if (cachevalue != null)
            {
                retv = ExpanditLib2.CDblEx(cachevalue);
            }
            else
            {
                string sqlFrom = "";
                string sqlTo = "";
                if (fromCurrency == ConfigurationManager.AppSettings["MULTICURRENCY_SITE_CURRENCY"])
                    sqlFrom = " OR (LTrim(RTrim(FromCurrency))='') OR (FromCurrency IS NULL)";
                if (toCurrency == ConfigurationManager.AppSettings["MULTICURRENCY_SITE_CURRENCY"])
                    sqlTo = " OR (LTrim(RTrim(ToCurrency))='') OR (ToCurrency IS NULL)";
                string sql = "SELECT ExchangeRate FROM CurrencyExchange WHERE ((FromCurrency=" + ExpanditLib2.SafeString(fromCurrency) + ")" + sqlFrom + ") AND ((ToCurrency=" + ExpanditLib2.SafeString(toCurrency) + ")" + sqlTo + ")";

                CurrencyExchange currencyExchange = GetResults<CurrencyExchange>(sql).SingleOrDefault();
                if (currencyExchange == null)
                {
                    sqlFrom = "";
                    sqlTo = "";
                    if (fromCurrency == ConfigurationManager.AppSettings["MULTICURRENCY_SITE_CURRENCY"])
                        sqlTo = " OR (LTrim(RTrim(ToCurrency))='') OR (ToCurrency IS NULL)";
                    if (toCurrency == ConfigurationManager.AppSettings["MULTICURRENCY_SITE_CURRENCY"])
                        sqlFrom = " OR (LTrim(RTrim(FromCurrency))='') OR (FromCurrency IS NULL)";
                    sql = "SELECT ExchangeRate FROM CurrencyExchange WHERE ((FromCurrency=" + ExpanditLib2.SafeString(toCurrency) + ")" + sqlFrom + ") AND ((ToCurrency=" + ExpanditLib2.SafeString(fromCurrency) + ")" + sqlTo + ")";

                    currencyExchange = GetResults<CurrencyExchange>(sql).SingleOrDefault();
                    if (currencyExchange != null)
                    {
                        retv = (decimal)(currencyExchange.ExchangeRate / 100);
                    }
                    else
                    {
                        retv = 0;
                    }
                }
                else
                {
                    retv = 100 / (decimal)(currencyExchange.ExchangeRate);
                }

                CacheManager.CacheSetAggregated(cachekey, retv, new[] { "CurrencyTable", "CurrencyExchange" });
            }

            return retv;
        }

        public virtual string GetIso4217Code(string aCurrency)
        {
            string retv;

            if (ExpanditLib2.IsNull(aCurrency))
                aCurrency = ConfigurationManager.AppSettings["MULTICURRENCY_SITE_CURRENCY"];
            if (string.IsNullOrEmpty(aCurrency))
                aCurrency = ConfigurationManager.AppSettings["MULTICURRENCY_SITE_CURRENCY"];
            string sql = "SELECT Iso4217 FROM CurrencyIso4217 WHERE CurrencyGuid=" + ExpanditLib2.SafeString(aCurrency);
            CurrencyIso4217 currencyIso4217 = GetResults<CurrencyIso4217>(sql).SingleOrDefault();

            if (currencyIso4217 != null)
            {
                retv = currencyIso4217.Iso4217;
            }
            else
            {
                // Unable to retrieve the Iso4217 code for the currency - use the default, as this is being displyed and is expected on the page.
                aCurrency = ConfigurationManager.AppSettings["MULTICURRENCY_SITE_CURRENCY"];
                sql = "SELECT Iso4217 FROM CurrencyIso4217 WHERE CurrencyGuid=" + ExpanditLib2.SafeString(aCurrency);
                currencyIso4217 = GetResults<CurrencyIso4217>(sql).SingleOrDefault();

                if (currencyIso4217 != null)
                {
                    retv = currencyIso4217.Iso4217;
                }
                else
                {
                    throw new Exception("ExpandIT Multicurrency Module, Unable to retrieve the Iso4217 code for currency [" + aCurrency + "]");
                }
            }
            return retv;
        }
    }
}
