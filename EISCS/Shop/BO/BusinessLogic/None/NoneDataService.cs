﻿using System.Collections.Generic;
using System.Linq;
using CmsPublic.DataRepository;
using DataFieldDefinition;
using EISCS.Shop.DO;
using EISCS.Shop.DO.Dto.Ledger;
using EISCS.Shop.DO.Interface;

namespace EISCS.Shop.BO.BusinessLogic.None
{
    public interface INoneDataService
    {
        List<ProductTableColumns> SelectListPrices(IEnumerable<string> inParameter);
        LedgerEntryContainer GetCustomerLedgerEntries(string customerGuid);
    }

    public class NoneDataService : BuslogicBaseDataService, INoneDataService
    {
        public NoneDataService(IExpanditDbFactory dbFactory)
            : base(dbFactory)
        {
        }

        public List<ProductTableColumns> SelectListPrices(IEnumerable<string> inParameter)
        {
            string[] enumerable = inParameter as string[] ?? inParameter.ToArray();
            if (!enumerable.Any())
            {
                return null;
            }

            return GetResults<ProductTableColumns>("SELECT ProductGuid, ListPrice FROM ProductTable WHERE ProductGuid IN @inParameter", new {inParameter = enumerable}).ToList();
        }
    }
}