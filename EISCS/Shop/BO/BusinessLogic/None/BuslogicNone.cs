﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataFieldDefinition;
using EISCS.ExpandITFramework.Infrastructure;
using EISCS.Shop.DO.Dto.Cart;
using EISCS.Wrappers.Configuration;

namespace EISCS.Shop.BO.BusinessLogic.None
{
    public class BuslogicNone : IBuslogic
    {
        private ICurrencyConverter _currency;
        private readonly IConfigurationObject _configurationObject;
        private INoneDataService _dataService;

        public BuslogicNone(IConfigurationObject configuration, ICurrencyConverter currency, INoneDataService dataService)
        {
            _currency = currency;
            _configurationObject = configuration;
            _dataService = dataService;
        }

        // TODO: Check if RecalculateLines when currency on header is different from user curreny should be implemented.
        public PageMessages CalculateOrder<T>(BaseOrderHeader<T> order, string userCurrency, string siteDefaultCurrency) where T : BaseOrderLine
        {
            if (order == null || order.IsCalculated)
            {
                return null;
            }

            if (string.IsNullOrEmpty(userCurrency))
            {
                userCurrency = siteDefaultCurrency;
            }

            order.CurrencyGuid = userCurrency;

            bool listPriceIsIncludingTax = ExpanditLib2.CBoolEx(_configurationObject.Read("LISTPRICE_INCLUDE_TAX"));

            List<string> guids = order.Lines.Select(line => line.ProductGuid).ToList();

            List<ProductTableColumns> products = _dataService.SelectListPrices(guids);

            if (products == null)
            {
                return null;
            }

            List<string> toRemove = new List<string>();

            double sumSubTotal = 0;

            try
            {
                foreach (var line in order.Lines)
                {
                    var product = products.FirstOrDefault(prod => prod.ProductGuid == line.ProductGuid);

                    if (product == null)
                    {
                        toRemove.Add(line.ProductGuid);
                        continue;
                    }

                    line.TaxPct = order.TaxPct;
                    line.CurrencyGuid = order.CurrencyGuid;
                    line.ListPrice = (double)_currency.ConvertCurrency(product.ListPrice, siteDefaultCurrency, userCurrency);

                    double taxAmount;

                    if (listPriceIsIncludingTax)
                    {
                        taxAmount = line.ListPrice * (1 - 1 / (1 + Convert.ToDouble(line.TaxPct) / 100));
                        line.ListPriceInclTax = line.ListPrice;
                        line.ListPrice = line.ListPrice - taxAmount;
                    }
                    else
                    {
                        taxAmount = line.ListPrice * (Convert.ToDouble(line.TaxPct) / 100);
                        line.ListPriceInclTax = line.ListPrice + taxAmount;
                    }

                    line.LineTotal = line.ListPrice * line.Quantity;
                    line.TotalInclTax = line.LineTotal * (1 + Convert.ToDouble(line.TaxPct) / 100);
                    line.TaxAmount = line.LineTotal * (Convert.ToDouble(line.TaxPct) / 100);
                    //  No discounts calculated in ExpandIT Basic Business Logic. Modify to calculate discounts.
                    line.LineDiscountAmount = 0;
                    line.LineDiscountAmountInclTax = line.LineDiscountAmount * (1 + Convert.ToDouble(line.TaxPct) / 100);
                    line.LineDiscount = 0;
                    line.IsCalculated = true;

                    sumSubTotal += line.LineTotal;
                }

                order.ServiceCharge = 0;
                order.ServiceChargeInclTax = 0;
                order.InvoiceDiscount = 0;
                order.InvoiceDiscountInclTax = 0;
                order.SubTotal = sumSubTotal;
                order.SubTotalInclTax = sumSubTotal * (1 + Convert.ToDouble(order.TaxPct) / 100);
                order.TaxAmount = sumSubTotal * Convert.ToDouble(order.TaxPct) / 100;
                order.Total = sumSubTotal;
                order.TotalInclTax = sumSubTotal + order.TaxAmount;
                order.IsCalculated = true;

                if (toRemove.Count > 0)
                {
                    foreach (string s in toRemove)
                    {
                        order.Lines.RemoveAll(x => x.ProductGuid == s);
                    }
                }

            }
            catch (Exception ex)
            {
                var messages = new PageMessages();
                messages.Errors.Add(ex.Message);
                return messages;
            }
            return null;
        }

        public void PromoShippingDiscounts<T>(BaseOrderHeader<T> order, string defaultCurrency) where T : BaseOrderLine
        {
            throw new NotImplementedException();
        }
    }
}
