﻿using System.Collections.Generic;
using System.Linq;
using CmsPublic.DataRepository;
using EISCS.Shop.DO.Dto.Backend.Axapta;
using EISCS.Shop.DO.Dto.Ledger;
using EISCS.Shop.DO.Interface;

namespace EISCS.Shop.BO.BusinessLogic.Axapta
{
    public interface IAxDataService
    {
        CustomerTableAx2009 SelectFromCustomerTable(string customerGuid);
        List<AxProductTable> SelectFromProductTable(IEnumerable<string> productGuids);
        IEnumerable<AXAPTA_PriceDisc> GetPriceDiscount(string systemDate, IEnumerable<string> productGuidEnumerable, IEnumerable<string> productDiscountEnumerable, string currencyGuid);
        IEnumerable<AXAPTA_PriceParameters> GetAxaptaPriceParameters();
        IEnumerable<AXAPTA_TaxData> GetProductTaxCodes(string customerGuid, string systemDate, IEnumerable<string> productGuidEnumerable);
        LedgerEntryContainer GetCustomerLedgerEntries(string customerGuid);
    }

    public class AxDataService : BuslogicBaseDataService, IAxDataService
    {
        public AxDataService(IExpanditDbFactory dbFactory)
            : base(dbFactory)
        {
        }
        
        public CustomerTableAx2009 SelectFromCustomerTable(string customerGuid)
        {
            string sql =
                "SELECT * FROM CustomerTable WHERE CustomerGuid = @customerGuid";
            return GetResults<CustomerTableAx2009>(sql, new { customerGuid }).SingleOrDefault();
        }

        public List<AxProductTable> SelectFromProductTable(IEnumerable<string> productGuids)
        {
            string[] enumerable = productGuids as string[] ?? productGuids.ToArray();
            return !enumerable.Any() ? null : GetResults<AxProductTable>("SELECT * FROM ProductTable WHERE ProductGuid IN @productGuids", new { productGuids = enumerable }).ToList();
        }

        public IEnumerable<AXAPTA_PriceDisc> GetPriceDiscount(string systemDate, IEnumerable<string> productGuidEnumerable, IEnumerable<string> productDiscountEnumerable,
            string currencyGuid)
        {
            string[] productGuidArray = productGuidEnumerable as string[] ?? productGuidEnumerable.ToArray();
            string[] productDiscountArray = productGuidEnumerable as string[] ?? productDiscountEnumerable.ToArray();

            string sql = "SELECT *, TranslatedVariantCode = CASE VariantCode WHEN 'AllBlank2' THEN '' ELSE VariantCode END FROM AXAPTA_PriceDisc WHERE " +
                         "((ItemCode = 0 AND ItemRelation IN @productGuidParameter) OR " +
                         "(ItemCode = 1 AND ItemRelation IN @discountGuidParameter) OR " +
                         "(ItemCode = 2)) AND " +
                         "(" + ExpanditLib2.SafeDatetime(systemDate) + " >= FromDate OR FromDate IS NULL)" +
                         " AND (" + ExpanditLib2.SafeDatetime(systemDate) + " <= ToDate OR ToDate IS NULL)" +
                         " AND Exchange = " + ExpanditLib2.SafeString(currencyGuid) +
                         " ORDER BY Relation, ItemCode, ItemRelation, AccountCode, AccountRelation, Exchange, QuantityAmount";

            return GetResults<AXAPTA_PriceDisc>(sql, new { productGuidParameter = productGuidArray, discountGuidParameter = productDiscountArray });
        }

        public IEnumerable<AXAPTA_PriceParameters> GetAxaptaPriceParameters()
        {
            return GetResults<AXAPTA_PriceParameters>("SELECT * FROM AXAPTA_PriceParameters");
        }

        public IEnumerable<AXAPTA_TaxData> GetProductTaxCodes(string customerGuid, string systemDate, IEnumerable<string> productGuidEnumerable)
        {
            // DateTime.Today.ToShortDateString()

            string[] productGuidArray = productGuidEnumerable as string[] ?? productGuidEnumerable.ToArray();

            string sql = "SELECT ProductTable.ProductGuid, AXAPTA_TaxOnItem.TaxCode, AXAPTA_TaxData.TaxLimitMax, AXAPTA_TaxData.TaxLimitMin, AXAPTA_TaxData.TaxValue " +
                         "FROM ProductTable INNER JOIN AXAPTA_TaxOnItem ON (ProductTable.TaxItemGroupId = AXAPTA_TaxOnItem.TaxItemGroup) " +
                         "INNER JOIN AXAPTA_TaxGroupData ON (AXAPTA_TaxGroupData.TaxCode = AXAPTA_TaxOnItem.TaxCode) " +
                         "INNER JOIN CustomerTable ON (AXAPTA_TaxGroupData.TaxGroup = CustomerTable.TaxGroup) " +
                         "INNER JOIN AXAPTA_TaxData ON (AXAPTA_TaxData.TaxCode = AXAPTA_TaxOnItem.TaxCode) " +
                         "WHERE ProductTable.ProductGuid IN @productGuidParameter " +
                         "AND CustomerTable.CustomerGuid = " + ExpanditLib2.SafeString(customerGuid) + " " +
                         "AND (" + ExpanditLib2.SafeDatetime(systemDate) +
                         " >= AXAPTA_TaxData.TaxFromDate OR AXAPTA_TaxData.TaxFromDate IS NULL) " +
                         "AND (" + ExpanditLib2.SafeDatetime(systemDate) +
                         " <= AXAPTA_TaxData.TaxToDate OR AXAPTA_TaxData.TaxToDate IS NULL) " +
                         "ORDER BY AXAPTA_TaxOnItem.TaxCode, ProductTable.ProductGuid ";

            return GetResults<AXAPTA_TaxData>(sql, new { productGuidParameter = productGuidArray });
        }

        public override LedgerEntryContainer GetCustomerLedgerEntries(string customerGuid)
        {
            string query = "SELECT * FROM AXAPTA_CustomerLedgerEntryOpen WHERE CustomerGuid = @customerGuid";
            var axOpenLedgers = GetResults<AXAPTA_CustomerLedgerEntryOpen>(query, new { customerGuid }).ToList();

            query = "SELECT * FROM CustomerLedgerEntry WHERE CustomerGuid = @customerGuid";
            var entries = GetResults<CustomerLedgerEntryItem>(query, new { customerGuid }).ToList();
            if (entries.Count == 0)
            {
                return null;
            }
            var currency = entries[0].CurrencyGuid;
            double balanceAmountLcy = axOpenLedgers.Sum(item => item.Amount);

            return new LedgerEntryContainer { LedgerEntries = entries, BalanceAmountLCY = balanceAmountLcy, Currency = currency };
        }
    }
}