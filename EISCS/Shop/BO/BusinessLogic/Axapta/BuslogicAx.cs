﻿using System;
using System.Collections.Generic;
using System.Linq;
using EISCS.ExpandITFramework.Infrastructure;
using EISCS.Shop.DO.Dto.Backend.Axapta;
using EISCS.Shop.DO.Dto.Cart;
using EISCS.Shop.DO.Interface;
using EISCS.Wrappers.Configuration;
using ExpandIT;

namespace EISCS.Shop.BO.BusinessLogic.Axapta
{
    public class BuslogicAx : BuslogicBase
    {
        private readonly IConfigurationObject _configurationObject;
        private readonly ICurrencyConverter _currencyConverter;
        private readonly IAxDataService _dataService;

        // Use an object to resemble the old Info dictionary.
        private readonly InfoObject _infoObject;
        private readonly IExpanditUserService _expanditUserService;

        public BuslogicAx(IConfigurationObject configuration, ICurrencyConverter currencyConverter, IAxDataService dataService, IExpanditUserService expanditUserService)
        {
            _configurationObject = configuration;
            _currencyConverter = currencyConverter;
            _dataService = dataService;
            _expanditUserService = expanditUserService;
            _infoObject = new InfoObject();
        }

        public override PageMessages CalculateOrder<T>(BaseOrderHeader<T> order, string currency, string defaultCurrency)
        {
            try
            {
                _infoObject.SystemDate = DateTime.Today.ToShortDateString();
                _infoObject.ProductList = GetProductGuidList(order);
                _infoObject.ProductsDict = _dataService.SelectFromProductTable(_infoObject.ProductList);
                _infoObject.ProductGroupList = InitProductGroupList(_infoObject.ProductsDict);

                AccumulateProductQuantities(order);
                _infoObject.ProductQuantities = ProductQuantities;

                _infoObject.ProductDiscountList = new List<string>();

                foreach (AxProductTable axProductTable in _infoObject.ProductsDict)
                {
                    string mlnd = axProductTable.MultilnDisc;
                    if (!_infoObject.ProductDiscountList.Contains(mlnd))
                    {
                        _infoObject.ProductDiscountList.Add(mlnd);
                    }
                    string lnd = axProductTable.LineDisc;
                    if (!_infoObject.ProductDiscountList.Contains(lnd))
                    {
                        _infoObject.ProductDiscountList.Add(lnd);
                    }
                }

                // 1. Initialization - get customer specific data from CustomerTable
                string customerGuid = GetCustomerGuid(order.UserGuid);
                // 1.1
                InitOrder(order, customerGuid, currency);
                // 1.2
                GetDictAxaptaPriceDisc(currency);

                // 1.3
                AxaptaInitTradeAgreements();

                // 2. Calculate orderlines
                foreach (T line in order.Lines)
                {
                    CalcLine(order, line, defaultCurrency);
                }

                // 3. Calculate Order discounts
                AxaptaTotalOrderDisc(order);

                // 4.
                AxaptaOrderTotal(order);

                // Mark order as calculated
                order.IsCalculated = true;
            }
            catch (Exception ex)
            {
                var messages = new PageMessages();
                messages.Errors.Add(ex.Message);
                return messages;
            }
            return null;
        }

        private void CalcLine<T>(BaseOrderHeader<T> order, BaseOrderLine line, string defaultCurrency) where T : BaseOrderLine
        {
            AxaptaInitLine(order, line, defaultCurrency); // TODO: Error checking and handling
            AxaptaLineDisc(line);
            AxaptaLineAmount(line);
            AxaptaLineDiscountAmount(line);

            line.IsCalculated = true;
        }

        private void GetDictAxaptaPriceDisc(string currency)
        {
            IEnumerable<AXAPTA_PriceDisc> discs = _dataService.GetPriceDiscount(_infoObject.SystemDate, _infoObject.ProductList, _infoObject.ProductDiscountList, currency);

            string lastKey = "";
            int keyCounter = 0;

            foreach (AXAPTA_PriceDisc disc in discs)
            {
                string currentKey = disc.AccountCode + "," + disc.ItemCode + "," + disc.Relation + "," + disc.AccountRelation + "," + disc.ItemRelation + "," +
                                    disc.TranslatedVariantCode;

                if (currentKey != lastKey)
                {
                    keyCounter = 0;
                    if (!_infoObject.DictAxaptaPriceDisc.ContainsKey(currentKey))
                    {
                        _infoObject.DictAxaptaPriceDisc.Add(currentKey, new Dictionary<string, AXAPTA_PriceDisc>());
                    }
                    lastKey = currentKey;
                }

                keyCounter += 1;

                if (!_infoObject.DictAxaptaPriceDisc[currentKey].ContainsKey(keyCounter.ToString()))
                {
                    _infoObject.DictAxaptaPriceDisc[currentKey].Add(keyCounter.ToString(), disc);
                }
            }
        }

        public void InitOrder<T>(BaseOrderHeader<T> order, string customerGuid, string currencyGuid) where T : BaseOrderLine
        {
            CustomerTableAx2009 customerTable = _dataService.SelectFromCustomerTable(customerGuid);

            if (customerTable == null)
            {
                throw new Exception(string.Format("Customer with customerGuid {0} was not found.", customerGuid));
            }

            _infoObject.CustomerRecord = customerTable;

            var headerParameter = new HeaderParameter
                {
                    CustomerGuid = customerGuid,
                    MultilnDisc = customerTable.MultilnDisc,
                    EndDisc = customerTable.EndDisc,
                    TaxGroup = customerTable.TaxGroup,
                    Vatdutiable = ExpanditLib2.CBoolEx(customerTable.TaxGroup)
                };


            // Set the additional order header parameter with initial values here
            order.HeaderParameter = headerParameter;

            order.TaxPct = _configurationObject.Read("AXAPTA_VAT_PCT");
            order.ServiceCharge = 0;
            order.InvoiceDiscount = 0;

            order.CurrencyGuid =
                ExpanditLib2.CBoolEx(_configurationObject.Read("AXAPTA_USE_CURRENCY_FROM_CUSTOMER")) || string.IsNullOrEmpty(currencyGuid)
                    ? customerTable.CurrencyGuid
                    : currencyGuid;
        }

        public void AxaptaTotalOrderDisc<T>(BaseOrderHeader<T> order) where T : BaseOrderLine
        {
            // Calculates total discount, multiline discounts and fees

            double pdmDiscAmount;
            double pdmPercent1;
            double pdmPercent2;
            double pdmBalance;
            double pdmBalanceTotal = 0;
            string currentkey;
            AXAPTA_PriceDisc priceDiscItem;
            string fAccountcode = "0";
            string fItemCode = "0";
            string pdmAcode = "0";
            bool pdmFee = true;
            bool pdmEndfound = false;
            string fItemrelation = "";
            string fAccountrelation = "";
            string pdmArel = "";

            object[] items = _infoObject.PriceParameters.DictItems;

            Dictionary<string, Dictionary<string, AXAPTA_PriceDisc>> dictAxaptaPriceDisc = _infoObject.DictAxaptaPriceDisc;

            //
            // Multiline Discount
            //

            // Find all MultiLine discount groups in use on lines.
            // Sum the quantaties for these groups.
            // Find the total quantity of all items.

            var multiLineSum = new ExpDictionary();
            multiLineSum["Groups"] = new ExpDictionary();

            foreach (T line in order.Lines)
            {
                object currMultilnDisc = ExpanditLib2.CStrEx(((LineParameter)line.LineParameter).MultilnDisc);

                ((ExpDictionary)multiLineSum["Groups"])[currMultilnDisc] = ExpanditLib2.ConvertToDbl(((ExpDictionary)multiLineSum["Groups"])[currMultilnDisc]) + line.Quantity;
            }

            if (((ExpDictionary)multiLineSum["Groups"]).Count > 0)
            {
                object[] mulkeys = ((ExpDictionary)multiLineSum["Groups"]).DictKeys;
                object[] mulitems = ((ExpDictionary)multiLineSum["Groups"]).DictItems;

                // for each Multiline Discount group get the discount
                for (int i = 0; i <= ((ExpDictionary)multiLineSum["Groups"]).Count - 1; i++)
                {
                    pdmBalance = ExpanditLib2.ConvertToDbl(mulitems[i]);
                    pdmDiscAmount = 0;
                    pdmPercent1 = 0;
                    pdmPercent2 = 0;
                    for (int j = 5; j <= 19; j += 7)
                    {
                        for (int k = 0; k <= 1; k++)
                        {
                            if ((bool)items[j + k - 1])
                            {
                                switch (k)
                                {
                                    case 0:
                                        // Item Grp 1
                                        fItemCode = "1";
                                        fItemrelation = (string)mulkeys[i];
                                        break;
                                    case 1:
                                        // All Items 2
                                        fItemCode = "2";
                                        fItemrelation = "";
                                        break;
                                }

                                switch (j)
                                {
                                    case 5:
                                        // CustomerAccount = 0
                                        fAccountcode = "0";
                                        fAccountrelation = ((HeaderParameter)order.HeaderParameter).CustomerGuid;
                                        break;
                                    case 12:
                                        // MultiLineDisc = 1
                                        fAccountcode = "1";
                                        fAccountrelation = ((HeaderParameter)order.HeaderParameter).MultilnDisc;
                                        break;
                                    case 19:
                                        // All = 2
                                        fAccountcode = "2";
                                        fAccountrelation = "";
                                        break;
                                }

                                currentkey =
                                    fAccountcode + "," + fItemCode + "," + "6" + "," + fAccountrelation + "," + fItemrelation + "," + "";

                                if (dictAxaptaPriceDisc.ContainsKey(currentkey) && dictAxaptaPriceDisc[currentkey] != null)
                                {
                                    foreach (var key in dictAxaptaPriceDisc[currentkey])
                                    {
                                        priceDiscItem = dictAxaptaPriceDisc[currentkey][key.Key];

                                        if (CompareQuantity(pdmBalance, priceDiscItem))
                                        {
                                            pdmDiscAmount = pdmDiscAmount + priceDiscItem.Amount;
                                            pdmPercent1 = pdmPercent1 + priceDiscItem.Percent1;
                                            pdmPercent2 = pdmPercent2 + priceDiscItem.Percent2;

                                            if (!ExpanditLib2.CBoolEx(priceDiscItem.LookforForward))
                                            {
                                                // Stop search
                                                j = 26;
                                                k = 2;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    pdmPercent1 = 100 * (1 - (1 - pdmPercent1 / 100) * (1 - pdmPercent2 / 100));

                    // For each line in order - add the discount.
                    foreach (T line in order.Lines)
                    {
                        bool asd = ExpanditLib2.CStrEx(((LineParameter)line.LineParameter).MultilnDisc) == ExpanditLib2.CStrEx(mulkeys[i]);
                        if (asd)
                        {
                            line.LineDiscount += pdmPercent1;
                            line.LineDiscountAmount += pdmDiscAmount;
                            line.LineTotal = line.Quantity * (line.ListPrice - line.LineDiscountAmount) * (100 - line.LineDiscount) / 100;
                        }
                    }
                }
            }

            //
            // Balance Total for use in Total discount
            //

            pdmBalance = 0;

            foreach (T line in order.Lines)
            {
                if (ExpanditLib2.CStrEx(((LineParameter)line.LineParameter).EndDisc) == "1")
                {
                    pdmBalance += line.LineTotal;
                    pdmEndfound = true;
                }
                pdmBalanceTotal += line.LineTotal;
            }

            //
            // Total Order Discount
            //

            pdmDiscAmount = 0;
            pdmPercent1 = 0;
            pdmPercent2 = 0;

            if (pdmEndfound)
            {
                for (int i = 7; i <= 21; i += 7)
                {
                    switch (i)
                    {
                        case 7:
                            // CustomerAccount 0
                            pdmAcode = "0";
                            pdmArel = ((HeaderParameter)order.HeaderParameter).CustomerGuid;
                            break;
                        case 14:
                            // EndDisc = 1
                            pdmAcode = "1";
                            pdmArel = ((HeaderParameter)order.HeaderParameter).EndDisc;
                            break;
                        case 21:
                            // All = 2
                            pdmAcode = "2";
                            pdmArel = "";
                            break;
                    }

                    if ((bool)items[i - 1])
                    {
                        currentkey =
                            (pdmAcode) + "," + "2" + "," + "7" + "," + pdmArel + "," + "" + "," + "";

                        //LRunning = true;

                        if (dictAxaptaPriceDisc.ContainsKey(currentkey) && dictAxaptaPriceDisc[currentkey] != null)
                        {
                            foreach (var key in dictAxaptaPriceDisc[currentkey])
                            {
                                priceDiscItem = dictAxaptaPriceDisc[currentkey][key.Key];
                                if (CompareQuantity(pdmBalance, priceDiscItem))
                                {
                                    if (priceDiscItem.Amount != 0)
                                    {
                                        pdmDiscAmount += priceDiscItem.Amount;
                                    }
                                    if (priceDiscItem.Percent1 != 0)
                                    {
                                        pdmPercent1 += priceDiscItem.Percent1;
                                    }
                                    if (priceDiscItem.Percent2 != 0)
                                    {
                                        priceDiscItem.Percent2 += priceDiscItem.Percent2;
                                    }

                                    if (!ExpanditLib2.CBoolEx(priceDiscItem.LookforForward))
                                    {
                                        //pdmAc = 28;
                                        //LRUnning = false;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            pdmDiscAmount = pdmDiscAmount + (pdmBalance * (1 - (1 - pdmPercent1 / 100) * (1 - pdmPercent2 / 100)));

            //
            // Total Fee or not fee
            //

            if (pdmDiscAmount * pdmBalanceTotal > 0 || !pdmFee)
            {
                if (pdmFee)
                {
                    order.ServiceCharge = 0;
                }
                if (pdmBalanceTotal > 0)
                {
                    order.InvoiceDiscountPct = (100 * pdmDiscAmount / pdmBalanceTotal).ToString();
                    order.InvoiceDiscount = pdmDiscAmount;
                }
                else
                {
                    order.InvoiceDiscountPct = "0";
                    order.InvoiceDiscount = 0;
                }
            }
            else
            {
                order.InvoiceDiscountPct = "0";
                order.InvoiceDiscount = 0;
                order.ServiceCharge = -pdmDiscAmount;
            }
        }

        private void AxaptaOrderTotal<T>(BaseOrderHeader<T> order) where T : BaseOrderLine
        {
            double amountSum = 0;
            double amountSumVat = 0;

            SetOrderLineVatPct(order);

            foreach (T line in order.Lines)
            {
                double taxPct = ExpanditLib2.ConvertToDbl(line.TaxPct);
                line.TaxAmount = (line.LineTotal * (taxPct / 100));
                amountSumVat += (line.LineTotal + line.TaxAmount);
                line.ListPriceInclTax = line.ListPrice * (1 + taxPct / 100);
                line.LineDiscountAmountInclTax = line.LineDiscountAmount * (1 + (taxPct / 100));
                if (line.LineDiscountAmountInclTax > 0)
                {
                    ((HeaderParameter)order.HeaderParameter).HasDiscount = true;
                }
                line.TotalInclTax = line.LineTotal + line.TaxAmount;
                amountSum += line.LineTotal;
            }

            order.SubTotal = amountSum;
            order.SubTotalInclTax = amountSumVat;

            bool calcVatOnDisc = ExpanditLib2.CBoolEx(_configurationObject.Read("AXAPTA_VAT_ON_TOTAL_DISCOUNT"));
            double discTaxPct = ExpanditLib2.ConvertToDbl(_configurationObject.Read("AXAPTA_VAT_PCT"));

            if (calcVatOnDisc)
            {
                order.InvoiceDiscountInclTax = order.InvoiceDiscount * (1 + (discTaxPct / 100));
            }
            else
            {
                order.InvoiceDiscountInclTax = order.InvoiceDiscount;
            }

            order.ServiceChargeInclTax = order.ServiceCharge * (1 + (discTaxPct / 100));
            order.Total = amountSum - order.InvoiceDiscount + order.ServiceCharge;
            order.TotalInclTax = amountSumVat - order.InvoiceDiscountInclTax + order.ServiceChargeInclTax;
            order.TaxAmount = order.TotalInclTax - order.Total;
        }

        private void AxaptaInitTradeAgreements()
        {
            IEnumerable<AXAPTA_PriceParameters> x = _dataService.GetAxaptaPriceParameters();

            AXAPTA_PriceParameters dataItem = x.FirstOrDefault();

            if (dataItem == null)
            {
                return;
            }

            var priceParameters = new ExpDictionary();

            priceParameters.Add("SALESORDERPRICEACCOUNTITEM", ExpanditLib2.CBoolEx(dataItem.SALESORDERPRICEACCOUNTITEM));
            priceParameters.Add("SALESORDERLINEACCOUNTITEM", ExpanditLib2.CBoolEx(dataItem.SALESORDERLINEACCOUNTITEM));
            priceParameters.Add("SALESORDERLINEACCOUNTGROUP", ExpanditLib2.CBoolEx(dataItem.SALESORDERLINEACCOUNTGROUP));
            priceParameters.Add("SALESORDERLINEACCOUNTALL", ExpanditLib2.CBoolEx(dataItem.SALESORDERLINEACCOUNTALL));
            priceParameters.Add("SALESORDERMULTILNACCOUNTGROUP", ExpanditLib2.CBoolEx(dataItem.SALESORDERMULTILNACCOUNTGROUP));
            priceParameters.Add("SALESORDERMULTILNACCOUNTALL", ExpanditLib2.CBoolEx(dataItem.SALESORDERMULTILNACCOUNTALL));
            priceParameters.Add("SALESORDERENDACCOUNTALL", ExpanditLib2.CBoolEx(dataItem.SALESORDERENDACCOUNTALL));

            priceParameters.Add("SALESORDERPRICEGROUPITEM", ExpanditLib2.CBoolEx(dataItem.SALESORDERPRICEGROUPITEM));
            priceParameters.Add("SALESORDERLINEGROUPITEM", ExpanditLib2.CBoolEx(dataItem.SALESORDERLINEGROUPITEM));
            priceParameters.Add("SALESORDERLINEGROUPGROUP", ExpanditLib2.CBoolEx(dataItem.SALESORDERLINEGROUPGROUP));
            priceParameters.Add("SALESORDERLINEGROUPALL", ExpanditLib2.CBoolEx(dataItem.SALESORDERLINEGROUPALL));
            priceParameters.Add("SALESORDERMULTILNGROUPGROUP", ExpanditLib2.CBoolEx(dataItem.SALESORDERMULTILNGROUPGROUP));
            priceParameters.Add("SALESORDERMULTILNGROUPALL", ExpanditLib2.CBoolEx(dataItem.SALESORDERMULTILNGROUPALL));
            priceParameters.Add("SALESORDERENDGROUPALL", ExpanditLib2.CBoolEx(dataItem.SALESORDERENDGROUPALL));

            priceParameters.Add("SALESORDERPRICEALLITEM", ExpanditLib2.CBoolEx(dataItem.SALESORDERPRICEALLITEM));
            priceParameters.Add("SALESORDERLINEALLITEM", ExpanditLib2.CBoolEx(dataItem.SALESORDERLINEALLITEM));
            priceParameters.Add("SALESORDERLINEALLGROUP", ExpanditLib2.CBoolEx(dataItem.SALESORDERLINEALLGROUP));
            priceParameters.Add("SALESORDERLINEALLALL", ExpanditLib2.CBoolEx(dataItem.SALESORDERLINEALLALL));
            priceParameters.Add("SALESORDERMULTILNALLGROUP", ExpanditLib2.CBoolEx(dataItem.SALESORDERMULTILNALLGROUP));
            priceParameters.Add("SALESORDERMULTILNALLALL", ExpanditLib2.CBoolEx(dataItem.SALESORDERMULTILNALLALL));
            priceParameters.Add("SALESORDERENDALLALL", ExpanditLib2.CBoolEx(dataItem.SALESORDERENDALLALL));

            var priceMatrix = new ExpDictionary();

            // Customer Account
            priceMatrix.Add("SALESORDERPRICEACCOUNTITEM", "004");       // Price 1
            priceMatrix.Add("SALESORDERLINEACCOUNTITEM", "005");        // Line 2
            priceMatrix.Add("SALESORDERLINEACCOUNTGROUP", "015");       // Line 3
            priceMatrix.Add("SALESORDERLINEACCOUNTALL", "025");         // Line 4
            priceMatrix.Add("SALESORDERMULTILNACCOUNTGROUP", "016");    // Multi 5
            priceMatrix.Add("SALESORDERMULTILNACCOUNTALL", "026");      // Multi 6
            priceMatrix.Add("SALESORDERENDACCOUNTALL", "027");          // All (Invoice) 7

            // Customer Group
            priceMatrix.Add("SALESORDERPRICEGROUPITEM", "104");         // Price 8
            priceMatrix.Add("SALESORDERLINEGROUPITEM", "105");          // Line 9
            priceMatrix.Add("SALESORDERLINEGROUPGROUP", "115");         // Line 10
            priceMatrix.Add("SALESORDERLINEGROUPALL", "125");           // Line 11
            priceMatrix.Add("SALESORDERMULTILNGROUPGROUP", "116");      // Multi 12
            priceMatrix.Add("SALESORDERMULTILNGROUPALL", "126");        // Multi 13
            priceMatrix.Add("SALESORDERENDGROUPALL", "127");            // All (Invoice) 14

            // All Customers
            priceMatrix.Add("SALESORDERPRICEALLITEM", "204");           // Price 15
            priceMatrix.Add("SALESORDERLINEALLITEM", "205");            // Line 16
            priceMatrix.Add("SALESORDERLINEALLGROUP", "215");           // Line 17
            priceMatrix.Add("SALESORDERLINEALLALL", "225");             // Line 18
            priceMatrix.Add("SALESORDERMULTILNALLGROUP", "216");        // Multi 19
            priceMatrix.Add("SALESORDERMULTILNALLALL", "226");          // Multi 20
            priceMatrix.Add("SALESORDERENDALLALL", "227");              // All (Invoice) 21

            _infoObject.PriceParameters = priceParameters;
            _infoObject.PriceMatrix = priceMatrix;

            _infoObject.BackEnd = "AXAPTA";
        }

        private void AxaptaInitLine<T>(BaseOrderHeader<T> order, BaseOrderLine line, string defaultCurrency) where T : BaseOrderLine
        {
            string productGuid = line.ProductGuid;
            AxProductTable productDict = _infoObject.ProductsDict.FirstOrDefault(x => x.ProductGuid == productGuid);

            if (productDict == null)
            {
                order.Lines.RemoveAll(x => x.ProductGuid == productGuid);
                string errorMessage = string.Format("{0} [{1}] {2}", Resource.GetLabel("MESSAGE_INTERNAL_PRODUCTGUID"), productGuid, Resource.GetLabel("MESSAGE_IS_NOT_AVAILABLE"));
                throw new Exception(errorMessage);
            }

            // Make sure that currency is the same on all lines on order.
            line.CurrencyGuid = order.CurrencyGuid;
            line.ListPrice = AxaptaAmount(_currencyConverter.ConvertCurrency(productDict.ListPrice, defaultCurrency, line.CurrencyGuid));

            line.LineParameter = new LineParameter();
            ((LineParameter)line.LineParameter).LineDiscGrp = productDict.LineDisc;
            ((LineParameter)line.LineParameter).MultilnDisc = productDict.MultilnDisc;
            ((LineParameter)line.LineParameter).EndDisc = productDict.EndDisc;

            ((LineParameter)line.LineParameter).TaxItemGroupId = productDict.TaxItemGroupId;

            line.LineDiscount = 0;
            line.LineDiscountAmount = 0;
            line.LineTotal = 0;
        }

        private void AxaptaLineDisc(BaseOrderLine line)
        {
            // AXAPTA/Axapta tradeagreement description
            // 
            //                      ACCOUNT NO.        ACCOUNT GROUP       ALL ACCOUNTS
            //                    Item Grp  All       Item Grp   All      Item Grp   All
            //  SALES
            //  +------------------------------------------------------------------------+
            //  +Price........:  1( )                8( )               15( )            +
            //  +Line disc....:  2( ) 3( ) 4( )      9( )10( )11( )     16( )17( )18( )  +
            //  +Multi disc...:       5( ) 6( )          12( )13( )          19( )20( )  +
            //  +Total disc...:            7( )               14( )               21( )  +
            //  +------------------------------------------------------------------------+
            //                    Search direction ---->

            string productGuid = line.ProductGuid;
            AxProductTable productDict = _infoObject.ProductsDict.FirstOrDefault(x => x.ProductGuid == productGuid);

            if (productDict == null)
            {
                string errorMessage = string.Format("{0} [{1}] {2}", Resource.GetLabel("MESSAGE_INTERNAL_PRODUCTGUID"), productGuid, Resource.GetLabel("MESSAGE_IS_NOT_AVAILABLE"));
                throw new Exception(errorMessage);
            }

            Dictionary<string, Dictionary<string, AXAPTA_PriceDisc>> dictAxaptaPriceDisc = _infoObject.DictAxaptaPriceDisc;

            bool pDel = false; // TODO: Find if this is actually used anywhere. Comes from old VB code.

            string faccountRelation = "";
            string fItemRelation = "";
            string fVariantCode = "";

            double pdPercent1 = 0;
            double pdPercent2 = 0;
            bool pLook = true;
            bool lLook = true;

            object[] items = _infoObject.PriceParameters.DictItems;
            object[] keys = _infoObject.PriceParameters.DictKeys;

            // Loop
            for (int i = 1; i <= 15; i += 7)
            {
                for (int j = 0; j <= 3; j++)
                {
                    var tradeArgName = keys[(i + j) - 1] as string;
                    var tradeAgrValue = (bool)items[(i + j) - 1];

                    if (tradeAgrValue)
                    {
                        string fAccountcode = ((string)_infoObject.PriceMatrix[tradeArgName]).Substring(0, 1);
                        string fItemCode = ((string)_infoObject.PriceMatrix[tradeArgName]).Substring(1, 1);
                        string fRelation = ((string)_infoObject.PriceMatrix[tradeArgName]).Substring(2, 1);

                        switch (fAccountcode)
                        {
                            case "0":
                                // CustomerGuid
                                faccountRelation = _infoObject.CustomerRecord.CustomerGuid;
                                break;
                            case "1":
                                // CustomerGroup
                                switch (fRelation)
                                {
                                    case "4":
                                        // Price
                                        faccountRelation = _infoObject.CustomerRecord.PriceGroup;
                                        break;
                                    case "5":
                                        // Line
                                        faccountRelation = _infoObject.CustomerRecord.LineDisc;
                                        break;
                                }
                                break;
                            case "2":
                                // All
                                faccountRelation = "";
                                break;
                        }

                        switch (fItemCode)
                        {
                            case "0":
                                // ItemNumber
                                fItemRelation = line.ProductGuid;
                                fVariantCode = line.VariantCode;
                                break;
                            case "1":
                                // ItemGroup
                                AxProductTable firstOrDefault = _infoObject.ProductsDict.FirstOrDefault(x => x.ProductGuid == line.ProductGuid);
                                if (firstOrDefault != null)
                                {
                                    fItemRelation = firstOrDefault.LineDisc;
                                }
                                fVariantCode = "";
                                break;
                            case "2":
                                // All
                                fItemRelation = "";
                                fVariantCode = "";
                                break;
                        }

                        string currentKey = fAccountcode + "," + fItemCode + "," + fRelation + "," + faccountRelation + "," + fItemRelation + "," + fVariantCode;

                        if (dictAxaptaPriceDisc.ContainsKey(currentKey) && dictAxaptaPriceDisc[currentKey] != null)
                        {
                            foreach (var key in dictAxaptaPriceDisc[currentKey])
                            {
                                AXAPTA_PriceDisc tradeAgrItem = dictAxaptaPriceDisc[currentKey][key.Key];
                                if (pDel)
                                {
                                    pDel = false;
                                    if (fRelation == "4")
                                    {
                                        line.ListPrice = 0;
                                    }
                                    else if (fRelation == "5")
                                    {
                                        line.LineDiscountAmount = 0;
                                    }
                                }

                                double productTotalQuantity;
                                if (ExpanditLib2.CBoolEx(_configurationObject.Read("AX_COLLECT_QUANTITES_FOR_LINE_DISCOUNTS")))
                                {
                                    productTotalQuantity = _infoObject.ProductQuantities.GetProductQuantities(line.ProductGuid);
                                }
                                else
                                {
                                    productTotalQuantity = line.Quantity;
                                }

                                if (CompareQuantity(productTotalQuantity, tradeAgrItem))
                                {
                                    if (fRelation == "4" && pLook)
                                    {
                                        double tmpAmount;
                                        if (ExpanditLib2.CBoolEx(tradeAgrItem.PriceUnit))
                                        {
                                            tmpAmount = tradeAgrItem.Amount / tradeAgrItem.PriceUnit;
                                        }
                                        else
                                        {
                                            tmpAmount = tradeAgrItem.Amount;
                                        }

                                        if (ExpanditLib2.CBoolEx(tradeAgrItem.Amount) && (tmpAmount < line.ListPrice || !ExpanditLib2.CBoolEx(line.ListPrice)))
                                        {
                                            if (!ExpanditLib2.CBoolEx(tradeAgrItem.PriceUnit))
                                            {
                                                ((LineParameter)line.LineParameter).PriceUnit = 1.0;
                                            }
                                            else
                                            {
                                                ((LineParameter)line.LineParameter).PriceUnit = tradeAgrItem.PriceUnit;
                                            }

                                            line.ListPrice = tradeAgrItem.Amount / ((LineParameter)line.LineParameter).PriceUnit;

                                            if (ExpanditLib2.CBoolEx(tradeAgrItem.DeliveryTime))
                                            {
                                                DateTime tmp1 = DateTime.Parse(_infoObject.SystemDate).AddHours(tradeAgrItem.DeliveryTime);

                                                if (tmp1 > ((LineParameter)line.LineParameter).ConfirmedDel)
                                                {
                                                    ((LineParameter)line.LineParameter).ConfirmedDel = tmp1;
                                                }
                                            }
                                        }
                                    }
                                    else if (fRelation == "5" && lLook)
                                    {
                                        line.LineDiscountAmount += tradeAgrItem.Amount;
                                        pdPercent1 += tradeAgrItem.Percent1;
                                        pdPercent2 += tradeAgrItem.Percent2;
                                    }

                                    if (tradeAgrItem.LookforForward != 1)
                                    {
                                        pLook = pLook && (fRelation != "4");
                                        lLook = lLook && (fRelation != "5");
                                    }
                                }
                            }
                        }
                    }
                }
            }

            if (!pDel)
            {
                line.LineDiscount = 100 * (1 - (1 - pdPercent1 / 100) * (1 - pdPercent2 / 100));
            }

            if (line.ListPrice <= 0)
            {
                line.ListPrice = productDict.ListPrice;
            }
        }

        /// <summary>
        /// SalesLineAmount
        /// </summary>
        /// <param name="line"></param>
        private static void AxaptaLineAmount(BaseOrderLine line)
        {
            if (line.ListPrice > 0)
            {
                line.LineTotal = line.Quantity * (line.ListPrice - line.LineDiscountAmount) * (100 - line.LineDiscount) / 100;
            }
        }

        private static void AxaptaLineDiscountAmount(BaseOrderLine line)
        {
            line.LineDiscountAmount = line.LineDiscountAmount;
        }

        private static double AxaptaAmount(double amount)
        {
            return ExpanditLib2.RoundEx(amount, 2);
        }

        // -- Axapta VAT Calculations --

        /// <summary>
        ///     Sets the vat percent on each orderline
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="order"></param>
        private void SetOrderLineVatPct<T>(BaseOrderHeader<T> order) where T : BaseOrderLine
        {
            string customerGuid = ((HeaderParameter)order.HeaderParameter).CustomerGuid;
            if (string.IsNullOrEmpty(customerGuid))
            {
                customerGuid = GetCustomerGuid(order.UserGuid);
            }
            if (string.IsNullOrEmpty(customerGuid))
            {
                throw new Exception("Customer is not specified");
            }

            // 1. Get table with all taxcodes, productguids, taxpct and limits
            IEnumerable<AXAPTA_TaxData> taxData = _dataService.GetProductTaxCodes(customerGuid, _infoObject.SystemDate, _infoObject.ProductList);

            // 2. Summarized LineTotal for each productguid in order dictionary 
            // Get new dictionary with amount summarized for each product 

            {
                Dictionary<string, double> dictProductAmount = SumProductAmount(order);

                // 3. Creates a dictionary with taxcodes and taxpct 
                var dictTaxCodePct = new Dictionary<string, double>();

                string lastTaxCode = "";
                double totalProductTaxCodeAmount = 0;
                double lastMinTaxAmount = 0;
                double lastMaxTaxAmount = 0;
                double lastTaxValue = 0;
                AXAPTA_TaxData[] axaptaTaxDatas = taxData as AXAPTA_TaxData[] ?? taxData.ToArray();
                for (int i = 0; i < axaptaTaxDatas.Count(); i++)
                {
                    AXAPTA_TaxData dr = axaptaTaxDatas[i];

                    // Checks if the vat code has changed and has been set 
                    if (!dr.TaxCode.Equals(lastTaxCode))
                    {
                        // Adds vat percent for the current vat code to the dictionary holding taxcodes and taxpct 
                        // This is not nessesary for the first item, because we want the summarized totalProductTaxCodeAmount 
                        if (i != 0)
                        {
                            dictTaxCodePct.Add(lastTaxCode, CalcTaxPct(totalProductTaxCodeAmount, lastMinTaxAmount, lastMaxTaxAmount, lastTaxValue));
                        }

                        // Stores old values 
                        lastMinTaxAmount = dr.TaxLimitMin;
                        lastMaxTaxAmount = dr.TaxLimitMax;
                        lastTaxValue = dr.TaxValue;
                        lastTaxCode = dr.TaxCode;
                    }

                    // Adds the total amount for the current product 
                    if (dictProductAmount.ContainsKey(dr.ProductGuid))
                    {
                        totalProductTaxCodeAmount += dictProductAmount[dr.ProductGuid];
                    }

                    // Adds vatpct for the last vat code to the dictionary 
                    if (i == (axaptaTaxDatas.Length - 1))
                    {
                        //totalTaxAmount += CalcTaxAmount(totalProductTaxCodeAmount, lastMinTaxAmount, lastMaxTaxAmount, lastTaxValue); 
                        dictTaxCodePct.Add(lastTaxCode, CalcTaxPct(totalProductTaxCodeAmount, lastMinTaxAmount, lastMaxTaxAmount, lastTaxValue));
                    }
                }

                // 4. Set the tax pct for each order line i orderDict 
                foreach (AXAPTA_TaxData dr in axaptaTaxDatas)
                {
                    SetOrderLineTaxPct(dictTaxCodePct[dr.TaxCode], dr.ProductGuid, order);
                }
            }
        }

        /// <summary>
        ///     Summarizes the LineTotal for each productguid in the order dictionary
        /// </summary>
        /// <param name="order"></param>
        /// <returns>Dictionary with productguid as key and total amount as value</returns>
        private Dictionary<string, double> SumProductAmount<T>(BaseOrderHeader<T> order) where T : BaseOrderLine
        {
            var dictProductAmount = new Dictionary<string, double>();

            foreach (T line in order.Lines)
            {
                // Clear previous vat amount and vat pct
                line.TaxPct = "0";
                line.TaxAmount = 0;

                // Checks if the productguid allready is present in the dictionary 
                if (dictProductAmount.ContainsKey(line.ProductGuid))
                {
                    dictProductAmount[line.ProductGuid] += line.LineTotal;
                }
                else
                {
                    dictProductAmount.Add(line.ProductGuid, line.LineTotal);
                }
            }
            return dictProductAmount;
        }

        /// <summary>
        ///     Calculates the vat percent for a tax code
        /// </summary>
        /// <param name="totalProductTaxCodeAmount"></param>
        /// <param name="lastMinTaxAmount"></param>
        /// <param name="lastMaxTaxAmount"></param>
        /// <param name="lastTaxValue"></param>
        /// <returns></returns>
        private double CalcTaxPct(double totalProductTaxCodeAmount, double lastMinTaxAmount, double lastMaxTaxAmount, double lastTaxValue)
        {
            double taxPct = 0;
            // Checks if the vat amount is in the range for adding vat to the order 
            if (totalProductTaxCodeAmount > lastMinTaxAmount && (totalProductTaxCodeAmount < lastMaxTaxAmount || lastMaxTaxAmount <= 0))
            {
                taxPct = lastTaxValue;
            }
            return taxPct;
        }

        /// <summary>
        ///     Sets the vat percent for each line in the order dictionary
        /// </summary>
        /// <param name="taxPct">vat percent for the tax code</param>
        /// <param name="productGuid"></param>
        /// <param name="order">the full order object</param>
        private void SetOrderLineTaxPct<T>(double taxPct, string productGuid, BaseOrderHeader<T> order) where T : BaseOrderLine
        {
            foreach (T line in order.Lines)
            {
                if (line.ProductGuid == productGuid)
                {
                    double oldTaxPct;
                    if (double.TryParse(line.TaxPct, out oldTaxPct))
                    {
                        line.TaxPct = (oldTaxPct + taxPct).ToString();
                    }
                }
            }
        }

        private bool CompareQuantity(double productTotalQuantity, AXAPTA_PriceDisc tradeAgrItem)
        {
            if (_configurationObject.Read("BackendType") == "AX2012")
            {
                bool retVal = (productTotalQuantity >= tradeAgrItem.QuantityAmount);
                // TODO: Check logic which uses QuantityAmountTo here. Might not be correct.
                if (retVal && Math.Abs(tradeAgrItem.QuantityAmountTo) > 0)
                {
                    retVal = (productTotalQuantity <= tradeAgrItem.QuantityAmountTo);
                }
                return retVal;
            }
            return (productTotalQuantity >= tradeAgrItem.QuantityAmount);
        }

        private List<string> GetProductGuidList<T>(BaseOrderHeader<T> order) where T : BaseOrderLine
        {
            return order.Lines.Select(line => line.ProductGuid).ToList();
        }

        private List<string> InitProductGroupList(IEnumerable<AxProductTable> products)
        {
            return products == null ? null : products.Select(line => line.ProductGrp).ToList();
        }

        private string GetCustomerGuid(string userGuid)
        {
            var userTable = _expanditUserService.GetUser(userGuid);

            if (userTable == null)
            {
                return _configurationObject.Read("ANONYMOUS_CUSTOMERGUID");
            }

            return string.IsNullOrEmpty(userTable.CustomerGuid) ? _configurationObject.Read("ANONYMOUS_CUSTOMERGUID") : userTable.CustomerGuid;
        }

        public override void PromoShippingDiscounts<T>(BaseOrderHeader<T> order, string defaultCurrency)
        {
            throw new NotImplementedException();
        }
    }

    internal class InfoObject
    {
        public InfoObject()
        {
            DictAxaptaPriceDisc = new Dictionary<string, Dictionary<string, AXAPTA_PriceDisc>>();
            PriceParameters = new ExpDictionary();
            PriceMatrix = new ExpDictionary();
        }

        public string SystemDate { get; set; }
        public List<string> ProductList { get; set; }
        public IEnumerable<AxProductTable> ProductsDict { get; set; }
        public List<string> ProductGroupList { get; set; }
        public BuslogicBase.ProductQuantity ProductQuantities { get; set; }
        public List<string> ProductDiscountList { get; set; }
        public CustomerTableAx2009 CustomerRecord { get; set; }
        public Dictionary<string, Dictionary<string, AXAPTA_PriceDisc>> DictAxaptaPriceDisc { get; set; }
        public ExpDictionary PriceParameters { get; set; }
        public ExpDictionary PriceMatrix { get; set; }
        public string BackEnd { get; set; }
    }

    /*
    ///     Internal classes to hold parameters used in price calculations
    */

    public class LineParameter : ILineParameter
    {
        public string LineDiscGrp { get; set; }
        public string MultilnDisc { get; set; }
        public int EndDisc { get; set; }
        public string TaxItemGroupId { get; set; }
        public double PriceUnit { get; set; }
        public DateTime ConfirmedDel { get; set; }
    }


    public class HeaderParameter : IHeaderParameter
    {
        public string CustomerGuid { get; set; }
        public string MultilnDisc { get; set; }
        public string EndDisc { get; set; }
        public string TaxGroup { get; set; }
        public bool Vatdutiable { get; set; }
        public bool HasDiscount { get; set; }
    }
}