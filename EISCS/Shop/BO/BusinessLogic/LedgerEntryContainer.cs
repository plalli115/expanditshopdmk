﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EISCS.Shop.DO.Dto.Ledger;

namespace EISCS.Shop.BO.BusinessLogic
{
    public class LedgerEntryContainer
    {
        public double BalanceAmountLCY { get; set; }
        public string Currency { get; set; }
        public List<CustomerLedgerEntryItem> LedgerEntries { get; set; }
    }
}
