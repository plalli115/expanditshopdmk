﻿using System.Collections.Generic;
using System.Linq;
using CmsPublic.DataRepository;
using EISCS.Shop.DO;
using EISCS.Shop.DO.Dto.Backend.C5;
using EISCS.Shop.DO.Dto.Ledger;
using EISCS.Shop.DO.Interface;

namespace EISCS.Shop.BO.BusinessLogic.C5
{
    public interface IC5DataService
    {
        IEnumerable<C5ProductTable> SelectFromProductTable(IEnumerable<string> productGuids);
        CustomerTableC5 SelectFromCustomerTable(string customerGuid);
        C5_VatCodeRate GetVatCodeRate(string vatCode);
        IEnumerable<C5_PriceDisc> GetFromC5PriceDiscs(string systemDate);
        IEnumerable<C5PriceData> GetC5StockPrice(IEnumerable<string> productGuids);
        LedgerEntryContainer GetCustomerLedgerEntries(string customerGuid);
    }

    public class C5DataService : BuslogicBaseDataService, IC5DataService
    {
        public C5DataService(IExpanditDbFactory dbFactory) : base(dbFactory)
        {
        }

        public IEnumerable<C5ProductTable> SelectFromProductTable(IEnumerable<string> productGuids)
        {
            string[] enumerable = productGuids as string[] ?? productGuids.ToArray();
            return !enumerable.Any() ? null : GetResults<C5ProductTable>("SELECT * FROM ProductTable WHERE ProductGuid IN @productGuids", new {productGuids = enumerable}).ToList();
        }

        public CustomerTableC5 SelectFromCustomerTable(string customerGuid)
        {
            string sql =
                "SELECT * FROM CustomerTable WHERE CustomerGuid = @customerGuid";
            return GetResults<CustomerTableC5>(sql, new { customerGuid }).SingleOrDefault();
        }

        public C5_VatCodeRate GetVatCodeRate(string vatCode)
        {
            string sql =
                "SELECT VatPct FROM C5_VatCodeRate WHERE LTRIM(RTRIM(VatCode)) = @vatCode";
            return GetResults<C5_VatCodeRate>(sql, new { vatCode }).SingleOrDefault();
        }

        public IEnumerable<C5_PriceDisc> GetFromC5PriceDiscs(string systemDate)
        {
            object sysDate = ExpanditLib2.SafeDatetime(systemDate);
            object zeroDate = ExpanditLib2.GetZeroDate();

            string sql =
                string.Format(
                    "SELECT * FROM C5_PriceDisc WHERE ({0} >= FromDate OR (FromDate = {1} OR FromDate IS NULL)) AND ({0} <= ToDate OR (ToDate = {1} OR ToDate IS NULL)) ORDER BY ItemCode,ItemRelation,AccountCode,AccountRelation,Type,QuantityAmount",
                    sysDate, zeroDate);

            return GetResults<C5_PriceDisc>(sql);
        }

        public IEnumerable<C5PriceData> GetC5StockPrice(IEnumerable<string> productGuids)
        {
            string sql = "SELECT ProductGuid, C5_StockPrice.C5PriceGroup, ListPrice, CurrencyGuid, InclVat " +
                         "FROM C5_StockPrice LEFT JOIN C5_StockPriceGroup ON C5_StockPrice.C5PriceGroup = C5_StockPriceGroup.C5PriceGroup " +
                         "WHERE ltrim(rtrim(ProductGuid)) IN @productGuids " +
                         "ORDER BY C5_StockPrice.ProductGuid, C5_StockPrice.C5PriceGroup";

            string[] enumerable = productGuids as string[] ?? productGuids.ToArray();
            return !enumerable.Any() ? null : GetResults<C5PriceData>(sql, new { productGuids = enumerable }).ToList();
        }
    }
}