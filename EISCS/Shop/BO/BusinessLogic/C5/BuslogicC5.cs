﻿using System;
using System.Collections.Generic;
using System.Linq;
using EISCS.ExpandITFramework.Infrastructure;
using EISCS.Shop.DO.Dto.Backend.C5;
using EISCS.Shop.DO.Dto.Cart;
using EISCS.Shop.DO.Interface;
using EISCS.Wrappers.Configuration;
using ExpandIT;

namespace EISCS.Shop.BO.BusinessLogic.C5
{
    public class BuslogicC5 : IBuslogic
    {
        private readonly IConfigurationObject _configurationObject;
        private readonly ICurrencyConverter _currencyConverter;
        private readonly IC5DataService _dataService;
        private readonly InfoObject _infoObject;
        private readonly IExpanditUserService _expanditUserService;

        public BuslogicC5(IConfigurationObject configuration, ICurrencyConverter currencyConverter, IC5DataService dataService, IExpanditUserService expanditUserService)
        {
            _configurationObject = configuration;
            _currencyConverter = currencyConverter;
            _dataService = dataService;
            _expanditUserService = expanditUserService;
            _infoObject = new InfoObject();
        }

        public PageMessages CalculateOrder<T>(BaseOrderHeader<T> order, string currency, string defaultCurrency) where T : BaseOrderLine
        {
            try
            {
                string customerGuid = GetCustomerGuid(order.UserGuid);
                InitOrder(order, customerGuid, currency);
                C5InitTradeAgreements();

                IEnumerable<C5_PriceDisc> discs = _dataService.GetFromC5PriceDiscs(_infoObject.SystemDate);

                string lastKey = "";
                int keyCounter = 0;

                foreach (C5_PriceDisc disc in discs)
                {
                    string currentKey = disc.ItemCode + "," + disc.ItemRelation + "," + disc.AccountCode + "," + disc.AccountRelation;

                    if (currentKey != lastKey)
                    {
                        keyCounter = 0;
                        if (!_infoObject.DictC5PriceDisc.ContainsKey(currentKey))
                        {
                            _infoObject.DictC5PriceDisc.Add(currentKey, new Dictionary<string, C5_PriceDisc>());
                        }
                        lastKey = currentKey;
                    }

                    keyCounter += 1;

                    if (!_infoObject.DictC5PriceDisc[currentKey].ContainsKey(keyCounter.ToString()))
                    {
                        _infoObject.DictC5PriceDisc[currentKey].Add(keyCounter.ToString(), disc);
                    }
                }

                // Get Stock Prices
                IEnumerable<C5PriceData> stockPrices = _dataService.GetC5StockPrice(_infoObject.ProductList);

                if (stockPrices != null)
                {
                    foreach (C5PriceData c5PriceData in stockPrices)
                    {
                        string currentKey = c5PriceData.ProductGuid + "," + c5PriceData.C5PriceGroup;
                        if (!_infoObject.DictC5StockPrice.ContainsKey(currentKey))
                        {
                            _infoObject.DictC5StockPrice.Add(currentKey, c5PriceData);
                        }
                    }
                }

                foreach (T line in order.Lines)
                {
                    CalcLine(order, line, defaultCurrency);
                }

                C5OrderTotal(order);

                order.IsCalculated = true;
            }
            catch (Exception ex)
            {
                var messages = new PageMessages();
                messages.Errors.Add(ex.Message);
                return messages;
            }
            return null;
        }

        private void CalcLine<T>(BaseOrderHeader<T> order, BaseOrderLine line, string defaultCurrency) where T : BaseOrderLine
        {
            C5InitLine(order, line, defaultCurrency); // OK
            C5LinePrice(order, line); // OK
            C5LineDisc(order, line, defaultCurrency); // OK
            C5LineAmount(line); // OK
            line.IsCalculated = true;
        }

        private void C5InitLine<T>(BaseOrderHeader<T> order, BaseOrderLine line, string defaultCurrency) where T : BaseOrderLine
        {
            string productGuid = line.ProductGuid;
            C5ProductTable productDict = _infoObject.ProductsDict.FirstOrDefault(x => x.ProductGuid == productGuid);

            if (productDict == null)
            {
                order.Lines.RemoveAll(x => x.ProductGuid == productGuid);
                string errorMessage = string.Format("{0} [{1}] {2}", Resource.GetLabel("MESSAGE_INTERNAL_PRODUCTGUID"), productGuid, Resource.GetLabel("MESSAGE_IS_NOT_AVAILABLE"));
                throw new Exception(errorMessage);
            }

            // Make sure that currency is the same on all lines on order.
            line.CurrencyGuid = order.CurrencyGuid;
            line.ListPrice = _currencyConverter.ConvertCurrency(productDict.ListPrice, defaultCurrency, line.CurrencyGuid);

            line.LineParameter = new LineParameter();
            ((LineParameter)line.LineParameter).LineDiscGrp = productDict.Linedisc;
            ((LineParameter)line.LineParameter).TaxCode = productDict.SalesVatCode;
            ((LineParameter)line.LineParameter).Vatdutiable = !string.IsNullOrEmpty(productDict.SalesVatCode);

            line.LineDiscount = 0;
            line.LineDiscountAmount = 0;
            line.LineTotal = 0;
            line.TaxAmount = 0;
            line.TotalInclTax = 0;
        }

        private void C5LinePrice<T>(BaseOrderHeader<T> order, BaseOrderLine line) where T : BaseOrderLine
        {
            if (string.IsNullOrEmpty(((HeaderParameter)order.HeaderParameter).C5PriceGroup))
            {
                throw new Exception("ExpandIT C5 Business Logic - Customer Pricegroup not defined for Customer.");
            }

            string currentKey = line.ProductGuid + "," + ((HeaderParameter)order.HeaderParameter).C5PriceGroup;

            if (_infoObject.DictC5StockPrice[currentKey] == null)
            {
                // Use default price if Customer Price not found
                string defaultPriceGroup = _configurationObject.Read("C5_PRICEGROUP");
                currentKey = line.ProductGuid + "," + defaultPriceGroup;
                if (_infoObject.DictC5StockPrice[currentKey] == null)
                {
                    throw new Exception(string.Format("ExpandIT C5 Business Logic - Price was not found - default group [{0}] not found", defaultPriceGroup));
                }
            }

            C5PriceData priceData = _infoObject.DictC5StockPrice[currentKey];

            line.ListPrice = _currencyConverter.ConvertCurrency(priceData.ListPrice, priceData.CurrencyGuid, line.CurrencyGuid);
            ((LineParameter)line.LineParameter).ListPriceIsIncludingTax = ExpanditLib2.CBoolEx(priceData.InclVat);
        }

        private string GetCustomerGuid(string userGuid)
        {
            var userTable = _expanditUserService.GetUser(userGuid);

            if (userTable == null)
            {
                return _configurationObject.Read("ANONYMOUS_CUSTOMERGUID");
            }

            return string.IsNullOrEmpty(userTable.CustomerGuid) ? _configurationObject.Read("ANONYMOUS_CUSTOMERGUID") : userTable.CustomerGuid;
        }


        private void C5InitTradeAgreements()
        {
            var priceParameters = new ExpDictionary();
            var priceMatrix = new ExpDictionary();

            // --
            // Generate Trade agreement parameters from C5 -
            // This gives the search order
            // --

            priceParameters["SALESORDERPRICEACCOUNTITEM"] = true;
            priceParameters["SALESORDERLINEACCOUNTITEM"] = true;
            priceParameters["SALESORDERLINEACCOUNTGROUP"] = true;
            priceParameters["SALESORDERLINEACCOUNTALL"] = true;
            priceParameters["SALESORDERMULTILNACCOUNTGROUP"] = false;
            priceParameters["SALESORDERMULTILNACCOUNTALL"] = false;
            priceParameters["SALESORDERENDACCOUNTALL"] = false;

            priceParameters["SALESORDERPRICEGROUPITEM"] = true;
            priceParameters["SALESORDERLINEGROUPITEM"] = true;
            priceParameters["SALESORDERLINEGROUPGROUP"] = true;
            priceParameters["SALESORDERLINEGROUPALL"] = true;
            priceParameters["SALESORDERMULTILNGROUPGROUP"] = false;
            priceParameters["SALESORDERMULTILNGROUPALL"] = false;
            priceParameters["SALESORDERENDGROUPALL"] = false;

            priceParameters["SALESORDERPRICEALLITEM"] = true;
            priceParameters["SALESORDERLINEALLITEM"] = true;
            priceParameters["SALESORDERLINEALLGROUP"] = true;
            priceParameters["SALESORDERLINEALLALL"] = true;
            priceParameters["SALESORDERMULTILNALLGROUP"] = false;
            priceParameters["SALESORDERMULTILNALLALL"] = false;
            priceParameters["SALESORDERENDALLALL"] = false;

            // --
            // Trade Agreement search order...
            // --

            priceMatrix["SALESORDERPRICEACCOUNTITEM"] = "004"; //Price 1
            priceMatrix["SALESORDERLINEACCOUNTITEM"] = "005"; //Line  2
            priceMatrix["SALESORDERLINEACCOUNTGROUP"] = "015"; //Line  3
            priceMatrix["SALESORDERLINEACCOUNTALL"] = "025"; //Line  4
            priceMatrix["SALESORDERMULTILNACCOUNTGROUP"] = "016"; //Multi 5
            priceMatrix["SALESORDERMULTILNACCOUNTALL"] = "026"; //Multi 6
            priceMatrix["SALESORDERENDACCOUNTALL"] = "027"; //All [Invoice] 7
            priceMatrix["SALESORDERPRICEGROUPITEM"] = "104"; //Price 8
            priceMatrix["SALESORDERLINEGROUPITEM"] = "105"; //Line 9
            priceMatrix["SALESORDERLINEGROUPGROUP"] = "115"; //Line 10
            priceMatrix["SALESORDERLINEGROUPALL"] = "125"; //Line 11
            priceMatrix["SALESORDERMULTILNGROUPGROUP"] = "116"; //Multi 12
            priceMatrix["SALESORDERMULTILNGROUPALL"] = "126"; //Multi 13
            priceMatrix["SALESORDERENDGROUPALL"] = "127"; //All [Invoice] 14
            priceMatrix["SALESORDERPRICEALLITEM"] = "204"; //Price 15
            priceMatrix["SALESORDERLINEALLITEM"] = "205"; //Line 16
            priceMatrix["SALESORDERLINEALLGROUP"] = "215"; //Line 17
            priceMatrix["SALESORDERLINEALLALL"] = "225"; //Line 18
            priceMatrix["SALESORDERMULTILNALLGROUP"] = "216"; //Multi 19
            priceMatrix["SALESORDERMULTILNALLALL"] = "226"; //Multi 20
            priceMatrix["SALESORDERENDALLALL"] = "227"; //All [Invoice] 21

            _infoObject.PriceParameters = priceParameters;
            _infoObject.PriceMatrix = priceMatrix;

            _infoObject.BackEnd = "C5180";
        }

        public void InitOrder<T>(BaseOrderHeader<T> order, string customerGuid, string currencyGuid) where T : BaseOrderLine
        {
            _infoObject.SystemDate = DateTime.Today.ToShortDateString();

            _infoObject.C5UsePrimaryAccount = ExpanditLib2.CBoolEx(_configurationObject.Read("C5_USE_PRIMARY_ACCOUNT"));
            _infoObject.C5UseInvoiceToAccount = ExpanditLib2.CBoolEx(_configurationObject.Read("C5_USE_INVOICETO_ACCOUNT"));

            _infoObject.ProductList = GetProductGuidList(order);

            _infoObject.ProductsDict = _dataService.SelectFromProductTable(_infoObject.ProductList);


            CustomerTableC5 customerTable = _dataService.SelectFromCustomerTable(customerGuid);

            if (customerTable == null)
            {
                throw new Exception(string.Format("Error initializing order - Customer Account [{0}] was not found.", customerGuid));
            }

            string custNo = customerTable.BillToCustomerGuid;

            if (_infoObject.C5UseInvoiceToAccount && !string.IsNullOrEmpty(ExpanditLib2.CStrEx(custNo).Trim()))
            {
                customerTable = _dataService.SelectFromCustomerTable(custNo);
                if (customerTable == null)
                {
                    throw new Exception(string.Format("Error initializing order - Invoice to CustomerAccount [{0}] not found. ", custNo));
                }
            }

            _infoObject.CustomerRecord = customerTable;

            var headerParameter = new HeaderParameter
                {
                    CustomerGuid = customerGuid,
                    Vatdutiable = ExpanditLib2.CBoolEx(customerTable.VatCode),
                    VatCode = customerTable.VatCode,
                    C5PriceGroup = customerTable.C5Pricegroup,
                    DiscountGroup = customerTable.PriceGroup
                };

            // Set the additional order header parameter with initial values here
            order.HeaderParameter = headerParameter;

            order.TaxCode = customerTable.VatCode;
            order.TaxPct = C5GetTaxPct(customerTable.VatCode);
            order.ServiceCharge = 0;
            order.InvoiceDiscount = 0;

            order.CurrencyGuid = ExpanditLib2.CBoolEx(_configurationObject.Read("C5_USE_CURRENCY_FROM_BILLTO_CUSTOMER")) ? customerTable.CurrencyGuid : currencyGuid;
        }

        private string C5GetTaxPct(string vatCode)
        {
            C5_VatCodeRate vatCodeRate = _dataService.GetVatCodeRate(vatCode);
            return vatCodeRate != null ? vatCodeRate.VatPct.ToString() : "0";
        }

        private List<string> GetProductGuidList<T>(BaseOrderHeader<T> order) where T : BaseOrderLine
        {
            return order.Lines.Select(line => line.ProductGuid).ToList();
        }

        public void C5OrderTotal<T>(BaseOrderHeader<T> order) where T : BaseOrderLine
        {
            double amountSumVat = 0;
            double amountSum = 0;

            foreach (T line in order.Lines)
            {
                double taxPct;

                if (((HeaderParameter)order.HeaderParameter).Vatdutiable)
                {
                    // Use tax pct from line (Product) if set
                    taxPct =
                        ExpanditLib2.ConvertToDbl(((LineParameter)line.LineParameter).Vatdutiable
                            ? C5GetTaxPct(((LineParameter)line.LineParameter).TaxCode)
                            : C5GetTaxPct(order.TaxCode));

                    if (((LineParameter)line.LineParameter).ListPriceIsIncludingTax)
                    {
                        // Substract the Tax from the ListPrice and Line Total
                        line.LineTotal = line.LineTotal * 1 / (1 + taxPct / 100);
                        line.ListPriceInclTax = line.ListPrice;
                        line.ListPrice = line.ListPrice * 1 / (1 + taxPct / 100);

                        line.LineDiscountAmountInclTax = line.LineDiscountAmount;
                        line.LineDiscountAmount = line.LineDiscountAmount * 1 / (1 + (taxPct / 100));
                    }
                    else
                    {
                        line.ListPriceInclTax = line.ListPrice * (1 + taxPct / 100);
                        line.LineDiscountAmountInclTax = line.LineDiscountAmount * (1 + (taxPct / 100));
                    }

                    line.TaxAmount = line.LineTotal * (taxPct / 100);
                }
                else
                {
                    taxPct = ExpanditLib2.ConvertToDbl(C5GetTaxPct(((LineParameter)line.LineParameter).TaxCode));

                    if (((LineParameter)line.LineParameter).Vatdutiable && ((LineParameter)line.LineParameter).ListPriceIsIncludingTax)
                    {
                        // Substract the Tax from the ListPrice and Line Total
                        line.LineTotal = line.LineTotal * 1 / (1 + taxPct / 100);
                        line.ListPriceInclTax = line.ListPrice;
                        line.ListPrice = line.ListPrice * 1 / (1 + taxPct / 100);
                    }
                    else
                    {
                        line.ListPriceInclTax = line.ListPrice * (1 + taxPct / 100);
                    }

                    line.TaxAmount = 0;
                }

                line.TaxPct = taxPct.ToString();

                line.TotalInclTax = line.LineTotal + line.TaxAmount;

                amountSumVat = amountSumVat + line.TotalInclTax;
                amountSum = amountSum + line.LineTotal;
            }

            // Sum of lines
            order.SubTotal = amountSum;
            // Sum of lines
            order.SubTotalInclTax = amountSumVat;

            order.InvoiceDiscountInclTax = order.InvoiceDiscount * (1 + (ExpanditLib2.ConvertToDbl(order.TaxPct) / 100));
            order.ServiceChargeInclTax = order.ServiceCharge * (1 + (ExpanditLib2.ConvertToDbl(order.TaxPct) / 100));

            order.TotalInclTax = amountSumVat - order.InvoiceDiscountInclTax + order.ServiceChargeInclTax;
            order.Total = amountSum - order.InvoiceDiscount + order.ServiceCharge;
            order.TaxAmount = order.TotalInclTax - order.Total;
        }


        public void C5LineDisc<T>(BaseOrderHeader<T> order, BaseOrderLine line, string defaultCurrency) where T : BaseOrderLine
        {
            // C5 tradeagreement description

            //  Hvis du vaelger 'Ja', soeges efter varesalgsrabatter i denne raekkefoelge:
            //                        cust               cust                 cust
            //                     ACCOUNT NO.        ACCOUNT GROUP       ALL ACCOUNTS
            //                   Item Grp  All       Item Grp   All      Item Grp   All
            // SALES
            // +------------------------------------------------------------------------+
            // +Price........:  1( )                5( )                9( )            +
            // +Line disc....:  2( ) 3( ) 4( )      6( ) 7( ) 8( )     10( )11( )12( )  +
            // +------------------------------------------------------------------------+
            //                   Search direction ---->

            //
            //  1. Varenummer      / Debitorkonto
            //  2. Varerabatgruppe / Debitorkonto
            //  3. Alle varer      / Debitorkonto
            //  4. Varenummer      / Debitorrabatgruppe
            //  5. Varerabatgruppe / Debitorrabatgruppe
            //  6. Alle varer      / Debitorrabatgruppe
            //  7. Varenummer      / Alle debitorkonti
            //  8. Varerabatgruppe / Alle debitorkonti
            //  9. Alle varer      / Alle debitorkonti
            //
            //  Hvis du vaelger 'Nej', soeges efter varesalgsrabatter i denne raekkefoelge:

            //                        cust               cust                 cust
            //                     ACCOUNT NO.        ACCOUNT GROUP       ALL ACCOUNTS
            //                   Item Grp  All       Item Grp   All      Item Grp   All
            // SALES
            // +------------------------------------------------------------------------+
            // +Price........:  1( )                2( )                3( )            +
            // +Line disc....:  4( ) 7( )10( )      5( )  8( )11( )     6( ) 9( )12( )  +
            // +------------------------------------------------------------------------+

            //  1. Varenummer      / Debitorkonto
            //  2. Varenummer      / Debitorrabatgruppe
            //  3. Varenummer      / Alle debitorkonti
            //  4. Varerabatgruppe / Debitorkonto
            //  5. Varerabatgruppe / Debitorrabatgruppe
            //  6. Varerabatgruppe / Alle debitorkonti
            //  7. Alle varer      / Debitorkonto
            //  8. Alle varer      / Debitorrabatgruppe
            //  9. Alle varer      / Alle debitorkonti

            //

            string fAccountrelation = "";
            string fItemrelation = "";
            int fromb;
            int tob;
            int stepb;
            int froma;
            int toa;
            int stepa;

            Dictionary<string, Dictionary<string, C5_PriceDisc>> dictC5PriceDisc = _infoObject.DictC5PriceDisc;

            double pdPercent1 = 0;
            double pdPercent2 = 0;

            object[] items = _infoObject.PriceParameters.DictItems;
            object[] keys = _infoObject.PriceParameters.DictKeys;


            double orgListPrice = line.ListPrice;
            double orgListPriceInclTax = line.ListPriceInclTax;

            // Customer Account No.

            //  1  2  3  4
            //  8  9 10 11
            // 15 16 17 18

            //  1  8  15
            //  2  9  16
            //  3 10  17
            //  4 11  18

            if (_infoObject.C5UsePrimaryAccount)
            {
                froma = 1;
                toa = 15;
                stepa = 7;
                fromb = 0;
                tob = 3;
                stepb = 1;
            }
            else
            {
                froma = 1;
                toa = 4;
                stepa = 1;
                fromb = 0;
                tob = 14;
                stepb = 7;
            }

            bool lLook = true;
            bool pDel = true;

            for (int i = froma; i <= toa; i += stepa)
            {
                if (!lLook)
                {
                    break;
                }
                for (int j = fromb; j <= tob; j += stepb)
                {
                    string tradeAgrName = ExpanditLib2.CStrEx(keys[i + j]);
                    bool tradeAgrValue = ExpanditLib2.CBoolEx(items[i + j]);


                    if (tradeAgrValue)
                    {
                        string fAccountcode = ((string)_infoObject.PriceMatrix[tradeAgrName]).Substring(0, 1);
                        string fItemocode = ((string)_infoObject.PriceMatrix[tradeAgrName]).Substring(1, 1);

                        switch (fAccountcode)
                        {
                            case "0":
                                // CustomerGuid
                                fAccountrelation = string.Format("='{0}'", ((HeaderParameter)order.HeaderParameter).CustomerGuid);
                                break;
                            case "1":
                                // CustomerGroup
                                fAccountrelation = string.Format("='{0}'", ((HeaderParameter)order.HeaderParameter).DiscountGroup);
                                break;
                            case "2":
                                // All
                                fAccountrelation = "";
                                break;
                        }

                        switch (fItemocode)
                        {
                            case "0":
                                // ItemNumber
                                fItemrelation = string.Format("='{0}'", line.ProductGuid);
                                break;
                            case "1":
                                // ItemGroup
                                fItemrelation = string.Format("='{0}'", ((LineParameter)line.LineParameter).LineDiscGrp);
                                break;
                            case "2":
                                // All
                                fItemrelation = "";
                                break;
                        }

                        string currentKey = fItemocode + "," + fItemrelation + "," + fAccountcode + "," + fAccountrelation;

                        bool sameRunning = true;

                        if (dictC5PriceDisc.ContainsKey(currentKey) && dictC5PriceDisc[currentKey] != null)
                        {
                            foreach (string keyLoopVariable in (dictC5PriceDisc[currentKey]).Keys)
                            {
                                string key = keyLoopVariable;
                                if (!sameRunning)
                                {
                                    break;
                                }
                                C5_PriceDisc item = dictC5PriceDisc[currentKey][key];

                                int fRelation = item.Type;

                                if (pDel)
                                {
                                    pDel = false;
                                    // Percent
                                    if (fRelation == 0)
                                    {
                                        line.LineDiscount = 0;
                                        // DiscAmount
                                    }
                                    else if (fRelation == 1)
                                    {
                                        line.LineDiscountAmount = 0;
                                        // Price
                                    }
                                    else if (fRelation == 2)
                                    {
                                        line.ListPrice = 0;
                                    }
                                }

                                if (item.QuantityAmount <= line.Quantity)
                                {
                                    switch (fRelation)
                                    {
                                        case 2:
                                            // relation = Sales(Price)
                                            if (ExpanditLib2.CBoolEx(item.Amount) && !ExpanditLib2.CBoolEx(line.ListPrice))
                                            {
                                                line.ListPrice = item.Amount;
                                            }
                                            break;
                                        case 0:
                                            // DiscPercent
                                            pdPercent1 += item.Amount;
                                            pdPercent2 = 0;
                                            break;
                                        case 1:
                                            // Discamount
                                            line.LineDiscountAmount += item.Amount;
                                            break;
                                    }
                                }

                                if (item.LookforForwardSame != 1)
                                {
                                    sameRunning = false;
                                }

                                if (item.LookforForward != 1)
                                {
                                    lLook = false;
                                    break;
                                }
                            }
                        }
                    }
                    if (!lLook)
                    {
                        break;
                    }
                }
            }

            if (!pDel)
            {
                line.LineDiscount = 100 * (1 - (1 - pdPercent1 / 100) * (1 - pdPercent2 / 100));
            }

            if (line.ListPrice <= 0)
            {
                line.ListPriceInclTax = orgListPriceInclTax;
                line.ListPrice = orgListPrice;
            }
        }

        private void C5LineAmount(BaseOrderLine line)
        {
            if (line.ListPrice > 0)
            {
                line.LineTotal = line.Quantity * (line.ListPrice - line.LineDiscountAmount) * (100 - line.LineDiscount) / 100;
            }
        }

        public void PromoShippingDiscounts<T>(BaseOrderHeader<T> order, string defaultCurrency) where T : BaseOrderLine
        {
            throw new NotImplementedException();
        }
    }

    internal class InfoObject
    {
        public InfoObject()
        {
            DictC5PriceDisc = new Dictionary<string, Dictionary<string, C5_PriceDisc>>();
            DictC5StockPrice = new Dictionary<string, C5PriceData>();
        }

        public string SystemDate { get; set; }
        public List<string> ProductList { get; set; }
        public IEnumerable<C5ProductTable> ProductsDict { get; set; }
        public CustomerTableC5 CustomerRecord { get; set; }
        public ExpDictionary PriceParameters { get; set; }
        public ExpDictionary PriceMatrix { get; set; }
        public string BackEnd { get; set; }
        public bool C5UsePrimaryAccount { get; set; }
        public bool C5UseInvoiceToAccount { get; set; }
        public Dictionary<string, Dictionary<string, C5_PriceDisc>> DictC5PriceDisc { get; set; }
        public Dictionary<string, C5PriceData> DictC5StockPrice { get; set; }
    }

    public class LineParameter : ILineParameter
    {
        public bool Vatdutiable;
        public string LineDiscGrp { get; set; }
        public string Multilndisc { get; set; }
        public int Enddisc { get; set; }
        public string VatCode { get; set; }
        public double PriceUnit { get; set; }
        public DateTime ConfirmedDel { get; set; }
        public string ExternalItemNo { get; set; }
        public string TaxCode { get; set; }
        public bool ListPriceIsIncludingTax { get; set; }
    }

    public class HeaderParameter : IHeaderParameter
    {
        public string CustomerGuid { get; set; }
        public string VatCode { get; set; }
        public bool Vatdutiable { get; set; }
        public bool HasDiscount { get; set; }
        public string C5PriceGroup { get; set; }
        public string DiscountGroup { get; set; }
    }
}