﻿using EISCS.Shop.DO.BAS.Dto;

namespace EISCS.Shop.BO.BAS
{
    public class UserAccessItem
    {
        public string UserId { get; set; }
        public string AccessGroupId { get; set; }
        public UserAccessLevel AccessTypeId { get; set; }

        public enum UserAccessLevel
        {
            UnSet = 0,
            Project = 1,
            Customer = 2,
            CustomerGroup = 3,
            Administrator = 4
        }
    }
}