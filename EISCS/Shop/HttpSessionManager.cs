﻿using System.Configuration;
using System.Web;

namespace EISCS.Shop
{
    public class HttpSessionManager : ISessionManager
    {
        public string GetLanguageGuid()
        {
            return HttpContext.Current.Session["LanguageGuid"] as string ??
                   ConfigurationManager.AppSettings["LANGUAGE_DEFAULT"];
        }

	    public string GetCurrencyGuid()
	    {
		    return HttpContext.Current.Session["CurrencyGuid"] as string ??
		           ConfigurationManager.AppSettings["CURRENCY_DEFAULT"];
	    }
    }
}