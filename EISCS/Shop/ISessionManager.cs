﻿namespace EISCS.Shop
{
    public interface ISessionManager
    {
        string GetLanguageGuid();
	    string GetCurrencyGuid();
    }
}