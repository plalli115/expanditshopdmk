﻿﻿using System;

namespace EISCS.Shop.DO.PostData
 {
     public class UserItemPostData
     {
         public string ContactName { get; set; }
         public string EmailAddress { get; set; }
         public string CustomerGuid { get; set; }
         public string CompanyName { get; set; }
         public string CountryGuid { get; set; }
         public string CurrencyGuid { get; set; }
         public string SecondaryCurrencyGuid { get; set; }
         public string Address1 { get; set; }
         public string Address2 { get; set; }
         public string CityName { get; set; }
         public string PhoneNo { get; set; }
         public string ZipCode { get; set; }
         public string CountryName { get; set; }
         public string LanguageGuid { get; set; }
         public string UserPassword { get; set; }
         public string ConfirmedUserPassword { get; set; }
         public DateTime UserCreated { get; set; }
         public DateTime UserModified { get; set; }
     }
 }