﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EISCS.Shop.DO.PostData
{
    public class UserPasswordPostData
    {
        public string CurrentUserPassword { get; set; }
        public string NewUserPassword { get; set; }
        public string ConfirmedNewUserPassword { get; set; }
    }
}
