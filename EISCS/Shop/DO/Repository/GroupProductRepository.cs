﻿using System;
using System.Collections.Generic;
using System.Linq;
using CmsPublic.DataRepository;
using CmsPublic.ModelLayer.Models;
using EISCS.Shop.DO.Interface;

namespace EISCS.Shop.DO.Repository
{
    [TableProperties(TableName = "GroupProduct" , PrimaryKeyName = "GroupProductGuid")]
    public class GroupProductRepository : BaseRepository<GroupProduct>, IGroupProductRepository
    {
        private readonly IPropertiesDataService _propertiesDataService;
        public GroupProductRepository(IExpanditDbFactory dbFactory, IPropertiesDataService propertiesDataService) : base(dbFactory)
        {
            _propertiesDataService = propertiesDataService;
        }



        /* NEW METHOD "=20111213 */

        public string LoadSingleProperty(string propOwner, string propertyOwnerType, string propName,
                                         string languageGuid)
        {

            string sql =
                @"  SELECT  PropTransText 
                    FROM    PropVal 
                            INNER JOIN PropTrans ON PropVal.PropValGuid=PropTrans.PropValGuid 
                    WHERE   PropOwnerTypeGuid=@propertyOwnerType 
                    AND     (PropLangGuid = @languageGuid) 
                    AND     PropVal.PropGuid = @propName 
                    AND     PropOwnerRefGuid = @propOwner 
                    ORDER BY PropOwnerRefGuid, PropVal.PropGuid 
                    OPTION(HASH JOIN); ";

            return GetSingle<string>(sql, new { propOwner, propName, languageGuid, propertyOwnerType });
            
        }

        /* NEW METHOD 20110224 
         *
         * Load the GroupProducts for ALL the sub groups (including children and grand children etc)
         */

        public List<GroupProduct> GetAllGroupProducts(int groupGuid, string languageGuid, bool isLoadProperties)
        {

            string variantSql = "WHERE EXISTS(SELECT * FROM temp1 WHERE temp1.ProductGuid = trans.ProductGuid AND trans.LanguageGuid = @languageGuid) ";
            if (HasVariantCode())
            {
                variantSql =
                    "WHERE EXISTS(SELECT * FROM temp1 WHERE temp1.ProductGuid = trans.ProductGuid AND trans.LanguageGuid = @languageGuid AND (trans.VariantCode = '' OR trans.VariantCode IS NULL)) ";
            }
    

            string sql =    @"WITH temp_GroupTable(GroupGuid, iteration) AS 
                              ( 
                                SELECT  GroupGuid, 0 
                                FROM    GroupTable 
                                WHERE   ParentGuid = @groupGuid 
                                UNION ALL 
                                SELECT  b.GroupGuid, a.iteration + 1 
                                FROM    temp_GroupTable AS a, GroupTable AS b 
                                WHERE   a.GroupGuid = b.ParentGuid 
                            ), 
                            temp_GroupProduct(GroupProductGuid, GroupGuid, SortIndex, ProductGuid) AS 
                            ( 
                                SELECT  GroupProductGuid, GroupGuid, SortIndex, ProductGuid 
                                FROM    GroupProduct 
                                WHERE   EXISTS(SELECT ProductGuid FROM temp_GroupTable WHERE GroupProduct.GroupGuid = temp_GroupTable.GroupGuid) 
                            ) , 
                            temp1(GroupProductGuid, GroupGuid, SortIndex, ProductName, ProductGuid) AS 
                            ( 
                                SELECT  gp.GroupProductGuid,gp.GroupGuid, gp.SortIndex, p.ProductName, p.ProductGuid  
                                FROM    temp_GroupProduct gp 
                                        INNER JOIN ProductTable p ON gp.ProductGuid = p.ProductGuid 
                            ) ,
                            temp2(ProductName, ProductGuid) AS 
                            ( 
                                SELECT  trans.ProductName, trans.ProductGuid 
                                FROM    ProductTranslation trans " +
                                variantSql +
                            @") 
                            ,temp3(ProductGuid) AS 
                            ( 
                                SELECT ProductGuid FROM temp1 
                                EXCEPT 
                                SELECT ProductGuid FROM temp2 
                            ) ,
                            temp4(ProductName, ProductGuid) AS 
                            (  
                                SELECT  ProductName, ProductGuid 
                                FROM    temp1 
                                WHERE   ProductGuid IN(SELECT * FROM temp3) 
                            ),
                            temp5(ProductName, ProductGuid) AS
                            (  
                                SELECT  CAST(ProductName AS NVARCHAR(50)), ProductGuid 
                                FROM    temp2 
                                UNION 
                                SELECT  ProductName, ProductGuid 
                                FROM    temp4
                            ) 
                            SELECT  temp1.GroupProductGuid, GroupGuid, SortIndex, temp5.ProductName, temp1.ProductGuid 
                            FROM    temp1 
                            INNER JOIN temp5 ON temp1.ProductGuid = temp5.ProductGuid 
                            OPTION(HASH JOIN) ";


            List<GroupProduct> groupProductList = GetResults<GroupProduct>(sql, new { languageGuid = GetExternalLanguageGuid(languageGuid), groupGuid }).ToList();
            
            //
            Dictionary<string, List<PropValPropTransQuerySet>> propTransDict = LoadSubGroupProductProperties(groupGuid, "PRD", languageGuid );

            var toRemove = new List<GroupProduct>(); // Should not be removed - instead props should be added!
            foreach (GroupProduct groupProduct in groupProductList)
            {
                if (!propTransDict.ContainsKey(groupProduct.ProductGuid))
                {
                    toRemove.Add(groupProduct);
                    continue;
                }
                List<PropValPropTransQuerySet> tempSet = propTransDict[groupProduct.ProductGuid];
                var propValSet = new PrdPropValSet();
                //Dictionary<string, string> propDict = tempSet.ToDictionary(set => set.PropGuid, set => set.PropTransText);
                var propDict = new Dictionary<string, string>();
                foreach (PropValPropTransQuerySet set in tempSet)
                {
                    if (!propDict.ContainsKey(set.PropGuid))
                        propDict.Add(set.PropGuid, set.PropTransText);
                }
                propValSet.PropDict = propDict;
                groupProduct.Properties = propValSet;
            }
            foreach (GroupProduct groupProduct in toRemove)
            {
                // Test to add empty props instead of removing GroupProduct
                var propValSet = new PrdPropValSet();
                var propDict = new Dictionary<string, string>();
                propValSet.PropDict = propDict;
                groupProduct.Properties = propValSet;

                // Original - remove
                //groupProductList.Remove(groupProduct);
            }

            return groupProductList;
        }

        /* NEW METHOD 20110224 
         *
         * Load the properies for the sub groups
         */

        private Dictionary<string, List<PropValPropTransQuerySet>> LoadSubGroupProductProperties(int groupGuid, string propOwnerTypeGuid, string languageGuid)
        {

            string sql = @"WITH temp_GroupTable(GroupGuid, iteration) AS 
                            ( 
                                SELECT  GroupGuid, 0 
                                FROM    GroupTable 
                                WHERE   ParentGuid = @groupGuid 
                                UNION ALL 
                                SELECT  b.GroupGuid, a.iteration + 1 
                                FROM    temp_GroupTable AS a, GroupTable AS b 
                                WHERE   a.GroupGuid = b.ParentGuid
                            ),
                            temp_GroupProduct(ProductGuid) AS 
                            ( 
                                SELECT  ProductGuid 
                                FROM    GroupProduct 
                                WHERE   EXISTS(SELECT ProductGuid FROM temp_GroupTable WHERE GroupProduct.GroupGuid = temp_GroupTable.GroupGuid) 
                            ), 
                            temp(PropOwnerRefGuid, PropValGuid, PropGuid, PropOwnerTypeGuid) AS 
                            ( 
                                SELECT  PropOwnerRefGuid, PropValGuid, PropGuid, PropOwnerTypeGuid 
                                FROM    PropVal 
                                WHERE   EXISTS(SELECT * FROM temp_GroupProduct WHERE temp_GroupProduct.ProductGuid = PropOwnerRefGuid) 
                            ) 
                            SELECT  PropTransText, PropOwnerRefGuid, PropGuid, PropTrans.PropLangGuid 
                            FROM    PropTrans 
                                    INNER JOIN temp ON PropTrans.PropValGuid = temp.PropValGuid 
                            WHERE   PropOwnerTypeGuid=@propOwnerTypeGuid 
                            AND     (PropLangGuid IS NULL OR PropLangGuid= @languageGuid) 
                            OPTION(HASH JOIN) ";


            var propTransList = GetResults<PropValPropTransQuerySet>(sql, new { propOwnerTypeGuid, languageGuid, groupGuid }).ToList();
            Console.WriteLine(propTransList.Count);

            return  EISCS.ExpandITFramework.Util.Utilities.ConvertToDictionary(propTransList, x => x.PropOwnerRefGuid);
        }

        /* NEW METHOD 20110222 */

        public List<GroupProduct> GetAllGroupProducts(string languageGuid, bool isLoadProperties)
        {
            string transText = "WHERE EXISTS(SELECT * FROM temp1 WHERE temp1.ProductGuid = trans.ProductGuid AND trans.LanguageGuid = @languageGuid) ";
            if (HasVariantCode() )
            {
                transText =
                    "WHERE EXISTS(SELECT * FROM temp1 WHERE temp1.ProductGuid = trans.ProductGuid AND trans.LanguageGuid = @languageGuid AND (trans.VariantCode = '' OR trans.VariantCode IS NULL)) ";
            }

            string sql =
                @"WITH temp1(GroupProductGuid, GroupGuid, SortIndex, ProductName, ProductGuid) AS 
                ( 
                    SELECT  gp.GroupProductGuid,gp.GroupGuid, gp.SortIndex, p.ProductName, p.ProductGuid  
                    FROM    GroupProduct gp 
                    INNER JOIN ProductTable p  ON gp.ProductGuid = p.ProductGuid 
                ),
                temp2(ProductName, ProductGuid) AS 
                ( 
                    SELECT  trans.ProductName, trans.ProductGuid 
                    FROM    ProductTranslation trans "
                     +transText +
                @"),
                temp3(ProductGuid) AS 
                ( 
                    SELECT   ProductGuid 
                    FROM    temp1 
                    EXCEPT 
                    SELECT  ProductGuid 
                    FROM    temp2 
                ),
                temp4(ProductName, ProductGuid) AS 
                (  
                    SELECT  ProductName, ProductGuid 
                    FROM    temp1 
                    WHERE   ProductGuid IN(SELECT * FROM temp3) 
                ),
                temp5(ProductName, ProductGuid) AS 
                ( 
                    SELECT  CAST(ProductName AS NVARCHAR(50)), ProductGuid 
                    FROM    temp2 
                    UNION 
                    SELECT  ProductName, ProductGuid 
                    FROM    temp4 
                ) 
                SELECT  temp1.GroupProductGuid, GroupGuid, SortIndex, temp5.ProductName, temp1.ProductGuid 
                FROM    temp1 
                        INNER JOIN temp5 ON temp1.ProductGuid = temp5.ProductGuid 
                OPTION(HASH JOIN) ";

            List<GroupProduct> groupProductList = GetResults(sql, new { languageGuid = GetExternalLanguageGuid(languageGuid) }).ToList();

            Dictionary<string, List<PropValPropTransQuerySet>> propTransDict = LoadProperties("PRD", languageGuid);

            var toRemove = new List<GroupProduct>();
            foreach (GroupProduct groupProduct in groupProductList)
            {
                if (!propTransDict.ContainsKey(groupProduct.ProductGuid))
                {
                    toRemove.Add(groupProduct);
                    continue;
                }
                List<PropValPropTransQuerySet> tempSet = propTransDict[groupProduct.ProductGuid];
                var propValSet = new PrdPropValSet();
                //Dictionary<string, string> propDict = tempSet.ToDictionary(set => set.PropGuid, set => set.PropTransText);

                var propDict = new Dictionary<string, string>();
                foreach (PropValPropTransQuerySet set in tempSet)
                {
                    if (!propDict.ContainsKey(set.PropGuid))
                        propDict.Add(set.PropGuid, set.PropTransText);
                }

                propValSet.PropDict = propDict;
                groupProduct.Properties = propValSet;
            }
            foreach (GroupProduct groupProduct in toRemove)
            {
                // Test to add empty props instead of removing GroupProduct
                var propValSet = new PrdPropValSet();
                var propDict = new Dictionary<string, string>();
                propValSet.PropDict = propDict;
                groupProduct.Properties = propValSet;

                // Original - remove
                //groupProductList.Remove(groupProduct);
            }

            return groupProductList;
        }


        // TEST METHOD 
        public Dictionary<string, List<PropValPropTransQuerySet>> LoadProperties(string propOwnerTypeGuid, string languageGuid)
        {
            string sql = @" WITH temp(PropOwnerRefGuid, PropValGuid, PropGuid, PropOwnerTypeGuid) AS 
                            ( 
                                SELECT  PropOwnerRefGuid, PropValGuid, PropGuid, PropOwnerTypeGuid 
                                FROM    PropVal 
                                WHERE   EXISTS(SELECT * FROM GroupProduct WHERE GroupProduct.ProductGuid = PropOwnerRefGuid) 
                            ) 
                            SELECT  PropTransText, PropOwnerRefGuid, PropGuid, PropTrans.PropLangGuid 
                            FROM    PropTrans 
                                    INNER JOIN temp ON PropTrans.PropValGuid = temp.PropValGuid
                            WHERE   PropOwnerTypeGuid=@propOwnerTypeGuid 
                            AND     (PropLangGuid IS NULL OR PropLangGuid= @languageGuid) 
                            OPTION(HASH JOIN) ";

            var propTransList = GetResults<PropValPropTransQuerySet>(sql, new { languageGuid, propOwnerTypeGuid }).ToList();
            Console.WriteLine(propTransList.Count);

            return EISCS.ExpandITFramework.Util.Utilities.ConvertToDictionary(propTransList, x => x.PropOwnerRefGuid);
        }


        public List<GroupProduct> GetGroupProducts(string productGuid)
        {
            string sql = @" SELECT  ProductGuid 
                            FROM    GroupProduct 
                            WHERE   ProductGuid = @productGuid ";
            return GetResults(sql, new {productGuid}).ToList();
        }

        public List<GroupProduct> GetGroupProducts(List<string> groupProductGuids)
        {
            string groupProductGuidInclause = EISCS.ExpandITFramework.Util.Utilities.ToInClauseString(groupProductGuids, "GroupProductGuid");
            string sql = @" SELECT   *
                            FROM    GroupProduct
                            WHERE   " + groupProductGuidInclause;
            
            return GetResults(sql).ToList();
        }

        //

        /*
         * CHANGED 2011-02-04
         * */

        /// <summary>
        /// Load all GroupProducts from a specific groupGuid
        /// </summary>
        /// <param name="groupGuid"></param>
        /// <param name="languageGuid"></param>
        /// <param name="isLoadProperties"></param>
        /// <returns></returns>
        public List<GroupProduct> GetGroupProducts(string groupGuid, string languageGuid, bool isLoadProperties)
        {

            int groupId = int.Parse(groupGuid);

            string transtext = "WHERE EXISTS(SELECT * FROM temp1 WHERE temp1.ProductGuid = trans.ProductGuid AND trans.LanguageGuid = @languageGuid) ";
            if (HasVariantCode())
            {
                transtext =
                    "WHERE EXISTS(SELECT * FROM temp1 WHERE temp1.ProductGuid = trans.ProductGuid AND trans.LanguageGuid = @languageGuid AND (trans.VariantCode = '' OR trans.VariantCode IS NULL)) ";
            }

            string sql =
                        @"  WITH temp1(GroupProductGuid, GroupGuid, SortIndex, ProductName, ProductGuid) AS 
                            ( 
                                SELECT  gp.GroupProductGuid,gp.GroupGuid, gp.SortIndex, p.ProductName, p.ProductGuid  
                                FROM    GroupProduct gp 
                                        INNER JOIN ProductTable p ON gp.ProductGuid = p.ProductGuid 
                                WHERE   gp.GroupGuid IN (@groupGuid)	 
                            ) ,
                            temp2(ProductName, ProductGuid) AS 
                            (
                                SELECT  trans.ProductName, trans.ProductGuid 
                                FROM    ProductTranslation trans "
                                +transtext +
                            @") ,
                            temp3(ProductGuid) AS 
                            ( 
                                SELECT  ProductGuid 
                                FROM    temp1 
                                EXCEPT      
                                SELECT  ProductGuid 
                                FROM temp2 
                            ),
                            temp4(ProductName, ProductGuid) AS 
                            (  
                                SELECT  ProductName, ProductGuid 
                                FROM    temp1 
                                WHERE ProductGuid IN(SELECT * FROM temp3) 
                            ),
                            temp5(ProductName, ProductGuid) AS 
                            (  
                                SELECT  CAST(ProductName AS NVARCHAR(50)), ProductGuid 
                                FROM    temp2 
                                UNION 
                                SELECT  ProductName, ProductGuid 
                                FROM    temp4
                            )
                            SELECT  temp1.GroupProductGuid, GroupGuid, SortIndex, temp5.ProductName, temp1.ProductGuid 
                            FROM    temp1 
                                    INNER JOIN temp5 ON temp1.ProductGuid = temp5.ProductGuid 
                            OPTION(HASH JOIN) ";

            List<GroupProduct> groupProductList = GetResults(sql, new { languageGuid = GetExternalLanguageGuid(languageGuid), groupId }).ToList();
            if (isLoadProperties && groupProductList.Count > 0)
            {
                _propertiesDataService.LoadGpProperties(groupProductList, languageGuid, groupGuid);
                
            }
            return groupProductList;
        }

        // TODO: Move this method to route provider implementation
        public List<string> GetInternalLinkUrls(string languageGuid)
        {
            string sql = @" SELECT  RouteUrl 
                            FROM    RouteTable 
                            WHERE   ProductGuid IS NULL
                            AND     LanguageGuid = @languageGuid 
                            ORDER BY RouteUrl";

            List<string> urls = GetResults<string>(sql, new { languageGuid }).ToList();

            return urls;
        }

        #region Create, Update, Move, Delete GroupProducts


        public void DeleteGroupProduct(int groupGuid)
        {
            string sql = @"DELETE FROM GroupProduct WHERE GroupGuid = @groupGuid";
            Execute(sql, new { groupGuid });
        }

        public void AddProductsToGroup(string[] productGuids, string groupGuid)
        {
            int groupId = int.Parse(groupGuid);
            int highestGroupProductGuid = GetHighestGroupProductGuid() + 1;
            int highestSortIndex = GetHighestSortIndexFromGroupProduct(groupId) + 1;


            for (int i = 0; i < productGuids.Length; i++)
            {
                var gp = new GroupProduct()
                    {
                        GroupGuid = groupId,
                        GroupProductGuid =highestGroupProductGuid + i,
                        ProductGuid =  productGuids[i],
                        SortIndex = highestSortIndex + i,
                    };
                Add(gp, false);
            }
        }


        public void DeleteProductsFromGroup(string[] groupProductGuid, int groupGuid)
        {
            string inClause = ExpandITFramework.Util.Utilities.ToInClauseString(groupProductGuid.ToList(), "GroupProductGuid");

            string sql  = @"DELETE FROM GroupProduct 
                            WHERE groupGuid= @groupGuid and " + inClause;

            Execute(sql, new {groupGuid});
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="currentItem"></param>
        /// <param name="nextItem"></param>
        /// <param name="prevItem"></param>
        /// <param name="groupGuid"></param>
        public void ChangeGroupProductSortOrder(string currentItem, string nextItem, string prevItem, string groupGuid)
        {
            string sql = "";
            if (nextItem != "NULL")
            {
                sql =
                    @"  BEGIN 
                            DECLARE @oldVal INT 
                        
                            SELECT  DISTINCT @oldVal = SortIndex 
                            FROM    GroupProduct 
                            WHERE   GroupGuid = @groupGuid 
                            AND     ProductGuid = @nextGuid 
                        
                            UPDATE  GroupProduct 
                            SET     SortIndex = SortIndex + 1 
                            WHERE    GroupGuid = @groupGuid 
                            AND     SortIndex >= @oldVal

                            UPDATE  GroupProduct 
                            SET     SortIndex = @oldVal 
                            WHERE   ProductGuid = @currentGuid 
                            AND     GroupGuid = @groupGuid 
                        END ";
            }
            else
            {
                sql =
                    @" BEGIN 
                             DECLARE @oldVal INT 
                             SELECT DISTINCT @oldVal = SortIndex 
                             FROM GroupProduct
                             WHERE GroupGuid = @groupGuid 
                             AND ProductGuid = @prevGuid 
                    
                             UPDATE GroupProduct 
                             SET SortIndex = @oldVal + 1 
                             WHERE ProductGuid = @currentGuid 
                             AND GroupGuid = @groupGuid 
                        END ";
            }
            
            Execute(sql, new { currentGuid = currentItem, groupGuid, nextGuid = nextItem, prevGuid= prevItem });
        }

        // duplicates 
        public string GetExternalLanguageGuid(string languageGuid)
        {
            var sql = @"SELECT ExternalLanguageGuid FROM LanguageTable WHERE LanguageGuid = @languageGuid";
            return GetSingle<string>(sql, new { languageGuid });
        }


        private bool HasVariantCode()
        {
            var sql = @"IF COL_LENGTH('ProductTranslation','VariantCode') IS NULL BEGIN SELECT '0' END ELSE SELECT '1'";
            var result = GetSingle<string>(sql);
            return result == "1";
        }
        
        private int GetHighestSortIndexFromGroupProduct(int groupGuid)
        {

            string sql = @" SELECT  MAX(SortIndex) 
                            FROM    GroupProduct 
                            WHERE   GroupGuid = @groupGuid";
            int? test = GetSingle<int?>(sql, new {groupGuid});
            if (test.HasValue)
            {
                return test.Value;
            }
            return 1;
            
        }

        private int GetHighestGroupProductGuid()
        {
            string sql = @" SELECT  MAX(GroupProductGuid) 
                            FROM    GroupProduct";

            var test = GetSingle<int?>(sql);
            if (test.HasValue)
            {
                return test.Value;
            }
            return 1;
        }

        #endregion
    }
}