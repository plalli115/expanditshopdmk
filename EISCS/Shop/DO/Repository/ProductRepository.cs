﻿using System;
using System.Collections.Generic;
using System.Linq;
using CmsPublic.DataRepository;
using CmsPublic.ModelLayer.Interfaces;
using CmsPublic.ModelLayer.Models;
using Dapper;
using EISCS.Shop.DO.Interface;

namespace EISCS.Shop.DO.Repository
{
    [TableProperties(TableName = "ProductTable", PrimaryKeyName = "ProductGuid")]
    public class ProductRepository : BaseRepository<Product>, IProductRepository
    {
        private IPropertiesDataService _propertiesDataService;
        public ProductRepository(IExpanditDbFactory dbFactory, IPropertiesDataService propertiesDataService) : base(dbFactory)
        {
            _propertiesDataService = propertiesDataService;
        }


        public int GetProductsCount(string filter)
        {

            string filterText = "";
            if (!string.IsNullOrEmpty(filter))
            {
                filter = filter.Replace("*", "%");
                filterText += " WHERE (LOWER(ProductName) LIKE(LOWER('" + filter + "%')) OR LOWER(ProductGuid) LIKE(LOWER('" + filter + "%')))";
            }
            var sql = @"SELECT  COUNT(*) 
                        FROM    ProductTable "
                        + filterText ;

            var result = GetSingle<int>(sql);
            return result;
        }

        public int GetProductsCount(string type, string filter)
        {
            string filterText = string.Empty;
            if (!string.IsNullOrEmpty(type) && !string.IsNullOrEmpty(filter))
            {
                filter = filter.Replace("*", "%");
                filterText = " WHERE " + type + " LIKE('" + filter + "%')";
            }

            string sql = @" SELECT  COUNT(*) 
                            FROM    ProductTable " 
                            + filterText;

            return GetSingle<int>(sql);
        }

        public int GetGroupProductsCount(int groupGuid)
        {
            string sql = @" SELECT COUNT(*) 
                            FROM GroupProduct gp INNER JOIN ProductTable pt
                            ON gp.ProductGuid = pt.ProductGuid
                            WHERE GroupGuid = @groupGuid";

            return GetSingle<int>(sql, new { groupGuid });
        }

        public List<Product> GetProductRange(int startIndex, int endIndex, bool isLoadProperties, string filter, string languageGuid, string columnSort, string sortDir)
        {
            return GetProductsRange(startIndex, endIndex, isLoadProperties, languageGuid, filter, columnSort, sortDir);
        }

        public List<Product> GetProductsRange(int startIndex, int endIndex, bool isLoadProperties, string languageGuid, string filter, string columnSort, string sortDir)
        {
            string filterText = string.Empty;

            if (!string.IsNullOrEmpty(filter))
            {
                filter = filter.Replace("*", "%");
                filterText = " WHERE (LOWER(ProductName) LIKE(LOWER('" + filter + "%')) OR LOWER(ProductGuid) LIKE(LOWER('" + filter + "%')))";
            }

            string sql = ProductProductTranslationPagingQuery() +
                                  @"SELECT * FROM 
                                   (SELECT ROW_NUMBER() OVER (ORDER BY " + columnSort + @" " + sortDir + @", ProductGuid) AS RowID, 
                                   ProductTable.*, IsNull(TExists.IsUsedInAnyGroup, 0) AS IsUsedInAnyGroup FROM ProductTable LEFT JOIN (
                                    SELECT ProductGuid AS ExistsProductGuid, CASE WHEN COUNT(*)>0 THEN 1 ELSE 0 END
                                    AS IsUsedInAnyGroup FROM GroupProduct GROUP BY ProductGuid) TExists ON (TExists.ExistsProductGuid = ProductTable.ProductGuid)
                                    " + filterText +
                                  @") temp6 
                                   WHERE RowID BETWEEN @startIndex AND @endIndex ORDER BY RowID";

            languageGuid = _propertiesDataService.GetExternalLanguageGuid(languageGuid);

            var productList = GetResults<Product>(sql, new {languageGuid, startIndex, endIndex}).ToList();

            if (isLoadProperties && productList.Count > 0)
            {
                _propertiesDataService.LoadProperties(productList.Cast<IPropertyOwner>().ToList(), languageGuid);
            }

            return productList;
        }
        
        private string ProductProductTranslationPagingQuery()
        {
            string transText = HasVariantCode()
                ? "AND (trans.VariantCode = '' OR trans.VariantCode IS NULL)"
                : "";

            string sql = @"
WITH temp1(ProductName, ProductGuid) AS 
( 
    SELECT ProductName, ProductGuid FROM ProductTable  
),
temp2(ProductName, ProductGuid) AS
( 
    SELECT DISTINCT CAST(trans.ProductName AS NVARCHAR(50)), trans.ProductGuid  
    FROM temp1 prod 
    INNER JOIN ProductTranslation trans 
    ON
        prod.ProductGuid = trans.ProductGuid AND trans.LanguageGuid = @languageGuid " + transText + @"
),
temp3(ProductGuid) AS 
( 
    SELECT ProductGuid FROM temp1 
    EXCEPT 
    SELECT ProductGuid FROM temp2 
),
temp4(ProductName, ProductGuid) AS 
(  
    SELECT ProductName, ProductGuid FROM temp1 WHERE ProductGuid IN(SELECT * FROM temp3) 
),
temp5(ProductName, ProductGuid) AS 
(  
    SELECT ProductName, ProductGuid 
    FROM temp2 
    UNION
    SELECT ProductName, ProductGuid 
    FROM temp4 
),
temp6(ProductName, ProductGuid) AS 
(
    SELECT temp5.ProductName, temp1.ProductGuid 
    FROM temp1 INNER JOIN temp5 
    ON temp1.ProductGuid = temp5.ProductGuid 
)
";
            return sql;
        }




        public List<Product> GetGroupProductsRange(int startIndex, int endIndex, bool isLoadProperties, string languageGuid, string filter, int groupGuid, string sort = null,string direction = null)
        {
            return GetGroupProductsRange(startIndex, endIndex, isLoadProperties, languageGuid,null, filter, groupGuid, sort, direction);
        }

        public List<Product> GetGroupProductsRange(int startIndex, int endIndex, bool isLoadProperties, string languageGuid,
                                      string type, string filter, int groupGuid, string sort, string direction)
        {

            var sqlPar = new DynamicParameters();
            sqlPar.Add("groupGuid", groupGuid);
            sort = string.IsNullOrEmpty(sort) ? "SortIndex" : sort;

            var productList = LoadProducts(startIndex, endIndex, isLoadProperties, languageGuid,  filter, sort, direction, true, "GroupGuid=@groupGuid", sqlPar);
            return productList;
        }

        public List<Product> GetProductsRange(ICollection<string> guids, string languageGuid,  string filter, string sort, string direction, int startIndex, int endIndex)
        {
            if (guids.Count == 0) return new List<Product>();

            guids = guids.Select(s => "'" + s + "'").ToList();
            var builder = string.Join(",", guids);
            sort = string.IsNullOrEmpty(sort) ? "ProductName" : sort;

            List<Product> result = LoadProducts(startIndex, endIndex, true, languageGuid,  filter, sort, direction, false, "ProductTable.ProductGuid in (" + builder + ")");
            return result;
        }

        private List<Product> LoadProducts(int startIndex, int endIndex, bool isLoadProperties, string languageGuid, string filter, string sort, string direction, Boolean productInGroup,  string preWhereClause = "", DynamicParameters preWhereParameters = null)
        {
            string filterText = "";
            string filterParameter = "";
            if (!string.IsNullOrEmpty(filter))
            {
                filter = filter.Replace("*", "%");
                filterText = " WHERE (ProductTable.ProductName LIKE('" + filter + "%') OR ProductTable.ProductGuid LIKE('" + filter + "%'))";
            }

            if (!string.IsNullOrEmpty(preWhereClause))
            {
                filterText = filterText + (filterText.Length > 0 ? " AND " : " WHERE ") + preWhereClause;
                
            }

            filterText = filterText + (HasVariantCode()
                                        ? " AND (ProductTranslation.VariantCode = '' OR ProductTranslation.VariantCode IS NULL) "
                                        : "");


            string orderByClause = "ORDER BY Fup " + direction;
            string indexClause = "";
            string rowColumn = "";
            string sortText = (sort == "Price") ? "" : sort + " " + direction;
            if (!string.IsNullOrEmpty(sortText))
            {
                if (sort.Equals("Productname", StringComparison.InvariantCultureIgnoreCase))
                {
                    orderByClause = "ORDER BY CAST(ProductNameTranslate AS NVARCHAR(50)) " + direction;
                }
                indexClause = startIndex == -1 && endIndex == -1 ? "" : " WHERE Row BETWEEN  @startIndex AND @endIndex ";
                rowColumn = " ROW_NUMBER() OVER (" + orderByClause + ") AS Row, ";
            }

            string sql = "";
            if (productInGroup)
            {
                sql = BuildGroupProductsSQL(rowColumn, indexClause, filterText, sort);
            }
            else
            {
                sql = BuildProductsSQL(rowColumn, indexClause, filterText, sort);
            }


            string extLanguageGuid = _propertiesDataService.GetExternalLanguageGuid(languageGuid);
            if (preWhereParameters == null)
            {
                var param = new { languageGuid = extLanguageGuid, filter = filterParameter, startIndex, endIndex };
                preWhereParameters = new DynamicParameters(param);
            }
            else
            {
                preWhereParameters.Add("languageGuid", extLanguageGuid);
                preWhereParameters.Add("filter", filterParameter);
                preWhereParameters.Add("startIndex", startIndex);
                preWhereParameters.Add("endIndex", endIndex);
            }

            List<Product> productList = GetResults(sql, preWhereParameters).ToList();
            

            if (isLoadProperties && productList.Count > 0)
            {
                _propertiesDataService.LoadProperties(productList.Cast<IPropertyOwner>().ToList(), languageGuid);
            }

            productList.ForEach(p => p.ProductName = p.ProductNameTranslate ?? p.ProductName);
            return productList;
        }


        private string BuildGroupProductsSQL(string rowColumn, string indexClause, string filterText, string orderByClause)
        {

            string sql = @"
                ;WITH Combo AS
                (
	                SELECT ProductTable.*, GroupProduct.GroupProductGuid, 
	                ISNULL(ProductTranslation.ProductName, ProductTable.ProductName ) AS ProductNameTranslate, GroupProduct.SortIndex AS Fup
	                FROM ProductTable 
		            JOIN GroupProduct ON ProductTable.ProductGuid=GroupProduct.ProductGuid
		            LEFT JOIN ProductTranslation ON (ProductTranslation.ProductGuid=ProductTable.ProductGuid AND LanguageGuid = @languageGuid)
	                " + filterText + @" 
                ),
                Tmp AS( SELECT "
                + rowColumn + @" Combo.* FROM Combo)
                SELECT Tmp.*, Fup AS SortIndex FROM Tmp " + indexClause;
            return sql;
        }

        private string BuildProductsSQL(string rowColumn, string indexClause, string filterText, string orderByClause)
        {
            string sql = @"
                ;WITH Combo AS
                (
	                SELECT ProductTable.*, 
	                ISNULL(ProductTranslation.ProductName, ProductTable.ProductName ) AS ProductNameTranslate
	                FROM ProductTable 		            
		            LEFT JOIN ProductTranslation ON (ProductTranslation.ProductGuid=ProductTable.ProductGuid AND LanguageGuid = @languageGuid)
	                " + filterText + @" 
                ),
                Tmp AS( SELECT "
                + rowColumn + @" Combo.* FROM Combo)
                SELECT * FROM Tmp " + indexClause;
            return sql;
        }

        public List<GroupProduct> GroupProducts(string groupId, string languageGuid, bool isLoadProperties)
        {
            var groupGuid = int.Parse(groupId);

            string transtext = "WHERE EXISTS(SELECT * FROM temp1 WHERE temp1.ProductGuid = trans.ProductGuid AND trans.LanguageGuid = @languageGuid) ";
            if (HasVariantCode())
            {
                transtext =
                    "WHERE EXISTS(SELECT * FROM temp1 WHERE temp1.ProductGuid = trans.ProductGuid AND trans.LanguageGuid = @languageGuid AND (trans.VariantCode = '' OR trans.VariantCode IS NULL)) ";
            }
            

            string sql  =
                @"WITH temp1(GroupProductGuid, GroupGuid, SortIndex, ProductName, ProductGuid) AS 
                ( 
                    SELECT  gp.GroupProductGuid,gp.GroupGuid, gp.SortIndex, p.ProductName, p.ProductGuid  
                    FROM    GroupProduct gp 
                            INNER JOIN ProductTable p  ON gp.ProductGuid = p.ProductGuid 
                    WHERE   gp.GroupGuid IN (@groupGuid)	 
                ), 
                temp2(ProductName, ProductGuid) AS 
                (
                    SELECT  trans.ProductName, trans.ProductGuid
                    FROM    ProductTranslation trans " +
                    transtext +
                @"), 
                temp3(ProductGuid) AS 
                ( 
                    SELECT ProductGuid FROM temp1 
                    EXCEPT 
                    SELECT ProductGuid FROM temp2 
                ), 
                temp4(ProductName, ProductGuid) AS 
                (  
                    SELECT  ProductName, ProductGuid 
                    FROM    temp1 
                    WHERE   ProductGuid IN(SELECT * FROM temp3) 
                ),
                temp5(ProductName, ProductGuid) AS 
                (  
                    SELECT  CAST(ProductName AS NVARCHAR(50)), ProductGuid 
                    FROM    temp2 
                    UNION 
                    SELECT  ProductName, ProductGuid 
                    FROM    temp4 
                ) 
                SELECT  temp1.GroupProductGuid, GroupGuid, SortIndex, temp5.ProductName, temp1.ProductGuid 
                FROM    temp1 
                        INNER JOIN temp5 ON temp1.ProductGuid = temp5.ProductGuid 
                OPTION(HASH JOIN) ";

            List<GroupProduct> groupProductList = GetResults<GroupProduct>(sql, new {languageGuid, groupGuid}).ToList();
            

            if (isLoadProperties && groupProductList.Any())
            {
                _propertiesDataService.LoadGpProperties(groupProductList, languageGuid, groupId);
            }
            return groupProductList;
        }

        public Product SingleProduct(string productGuid, string languageGuid, bool isLoadProperties = true)
        {
            string sql = ProductProductTranslationBaseQuery();
            Product result = GetSingle(sql, new { productGuid, languageGuid = _propertiesDataService.GetExternalLanguageGuid(languageGuid), });

            // The translated value is placed in ProductNameTranslate in the sql data, so it must be copied to the actual Product name
            if (result != null)
            {
                result.ProductName = result.ProductNameTranslate;
                if (isLoadProperties)
                {
                    _propertiesDataService.LoadProperties(new List<IPropertyOwner>() { result }, languageGuid);
                }
            }
            return result;

        }

        public Product SingleProductAnonymousCollection(string guid, string languageGuid)
        {
            Product p = SingleProduct(guid, languageGuid);
            if (p != null)
            {
                _propertiesDataService.LoadPropertiesForEdit(new List<IPropertyOwner> { p }, languageGuid);
            }
            else
            {
                throw new Exception("Could not load Product");
            }
            return p;
        }

        public List<Product> GetAllProducts(string languageGuid, bool isLoadProperties)
        {

            string transText = "";
            if (HasVariantCode())
            {
                transText = "AND (ProductTranslation.VariantCode = '' OR ProductTranslation.VariantCode IS NULL) ";
            }
                

            string sql =
                @"  SELECT  ProductTable.*, IsNull(ProductTranslation.ProductName, ProductTable.ProductName) AS ProductNameTranslate 
                    FROM    ProductTable 
                            LEFT JOIN ProductTranslation ON (ProductTable.ProductGuid=ProductTranslation.ProductGuid and ProductTranslation.LanguageGuid = @languageGuid " + transText + @")";

            List<Product> productList = GetResults<Product>(sql, new { languageGuid= _propertiesDataService.GetExternalLanguageGuid(languageGuid)}).ToList();

            // Get correct translation
            productList.ForEach(p => p.ProductName = p.ProductNameTranslate ?? p.ProductName);

            // LOAD PROPERTIES GOES HERE
            Dictionary<string, List<PropValPropTransQuerySet>> propTransDict =  _propertiesDataService.LoadProperties("PRD", languageGuid);
            //
            var returnList = new List<Product>();
            foreach (Product product in productList)
            {
                if (!propTransDict.ContainsKey(product.ProductGuid))
                {
                    var newPropValSet = new PrdPropValSet {PropDict = new Dictionary<string, string>()};
                    product.Properties = newPropValSet;
                    returnList.Add(product);
                    continue;
                }
                List<PropValPropTransQuerySet> tempSet = propTransDict[product.ProductGuid];
                var propValSet = new PrdPropValSet();

                var propDict = new Dictionary<string, string>();
                foreach (PropValPropTransQuerySet set in tempSet)
                {
                    if (!propDict.ContainsKey(set.PropGuid))
                        propDict.Add(set.PropGuid, set.PropTransText);
                }

                propValSet.PropDict = propDict;
                product.Properties = propValSet;
                returnList.Add(product);
            }
            return returnList;
        }

        private bool HasVariantCode()
        {
            var sql = @"IF COL_LENGTH('ProductTranslation','VariantCode') IS NULL BEGIN SELECT '0' END ELSE SELECT '1'";
            var result = GetSingle<string>(sql);
            return result == "1";
        }

        private string ProductProductTranslationBaseQuery()
        {
            string transText = HasVariantCode()
                ? "AND (ProductTranslation.VariantCode = '' OR ProductTranslation.VariantCode IS NULL)"
                : "";

            string sql = @"SELECT TOP 1 ProductTable.*, IsNull(ProductTranslation.ProductName, ProductTable.ProductName) AS ProductNameTranslate FROM 
                           ProductTable LEFT JOIN
                           ProductTranslation ON (ProductTable.ProductGuid=ProductTranslation.ProductGuid and ProductTranslation.LanguageGuid = @languageGuid " + transText + @")
                           WHERE ProductTable.ProductGuid=@productGuid";

            return sql;
        }

 

        public List<GroupProduct> GetAllGroupProducts(string languageGuid, bool isLoadProperties)
        {
            string transText = "WHERE EXISTS(SELECT * FROM temp1 WHERE temp1.ProductGuid = trans.ProductGuid AND trans.LanguageGuid = @languageGuid) ";
            if (HasVariantCode())
            {
                transText =
                    "WHERE EXISTS(SELECT * FROM temp1 WHERE temp1.ProductGuid = trans.ProductGuid AND trans.LanguageGuid = @languageGuid AND (trans.VariantCode = '' OR trans.VariantCode IS NULL)) ";
            }

            var sql =
                @"WITH temp1(GroupProductGuid, GroupGuid, SortIndex, ProductName, ProductGuid) AS 
                ( 
                    SELECT gp.GroupProductGuid,gp.GroupGuid, gp.SortIndex, p.ProductName, p.ProductGuid  
                    FROM GroupProduct gp INNER JOIN ProductTable p 
                    ON gp.ProductGuid = p.ProductGuid 
                ) 
                ,temp2(ProductName, ProductGuid) AS 
                ( 
                    SELECT trans.ProductName, trans.ProductGuid 
                    FROM ProductTranslation trans 
                    " +transText +
                @") 
                ,temp3(ProductGuid) AS 
                ( 
                    SELECT ProductGuid FROM temp1 
                    EXCEPT 
                    SELECT ProductGuid FROM temp2 
                ) 
                ,temp4(ProductName, ProductGuid) AS 
                (  
                    SELECT ProductName, ProductGuid FROM temp1 WHERE ProductGuid IN(SELECT * FROM temp3) 
                ) 
                ,temp5(ProductName, ProductGuid) AS 
                (  
                    SELECT CAST(ProductName AS NVARCHAR(50)), ProductGuid 
                    FROM temp2 
                    UNION 
                    SELECT ProductName, ProductGuid 
                    FROM temp4 
                ) 
                SELECT temp1.GroupProductGuid, GroupGuid, SortIndex, temp5.ProductName, temp1.ProductGuid 
                FROM temp1 INNER JOIN temp5 
                ON temp1.ProductGuid = temp5.ProductGuid 
                OPTION(HASH JOIN) ";

            List<GroupProduct> groupProductList = GetResults<GroupProduct>(sql, new { languageGuid = _propertiesDataService.GetExternalLanguageGuid(languageGuid) }).ToList();

            Dictionary<string, List<PropValPropTransQuerySet>> propTransDict = _propertiesDataService.LoadProperties("PRD", languageGuid);
            
            foreach (GroupProduct groupProduct in groupProductList)
            {
                var propValSet = new PrdPropValSet();
                var propDict = new Dictionary<string, string>();

                if (!propTransDict.ContainsKey(groupProduct.ProductGuid))
                {
                    propValSet = new PrdPropValSet();
                    propDict = new Dictionary<string, string>();
                    propValSet.PropDict = propDict;
                    groupProduct.Properties = propValSet;
                    continue;
                }

                List<PropValPropTransQuerySet> tempSet = propTransDict[groupProduct.ProductGuid];

                foreach (PropValPropTransQuerySet set in tempSet)
                {
                    if (!propDict.ContainsKey(set.PropGuid))
                        propDict.Add(set.PropGuid, set.PropTransText);
                }

                propValSet.PropDict = propDict;
                groupProduct.Properties = propValSet;
            }

            return groupProductList;
        }

        public List<GroupProduct> GetAllGroupProducts(int groupGuid, string languageGuid, bool isLoadProperties)
        {
            string variant = "WHERE EXISTS(SELECT * FROM temp1 WHERE temp1.ProductGuid = trans.ProductGuid AND trans.LanguageGuid = @languageGuid) ";
            if (HasVariantCode())
            {
                variant =
                    "WHERE EXISTS(SELECT * FROM temp1 WHERE temp1.ProductGuid = trans.ProductGuid AND trans.LanguageGuid = @languageGuid AND (trans.VariantCode = '' OR trans.VariantCode IS NULL)) ";
            }


            string sql = @"WITH temp_GroupTable(GroupGuid, iteration) AS 
            ( 
                SELECT GroupGuid, 0 
                FROM GroupTable WHERE ParentGuid = @groupGuid 
                UNION ALL 
                SELECT b.GroupGuid, a.iteration + 1 
                FROM temp_GroupTable AS a, GroupTable AS b 
                WHERE a.GroupGuid = b.ParentGuid 
            )
            temp_GroupProduct(GroupProductGuid, GroupGuid, SortIndex, ProductGuid) AS 
            ( 
                SELECT  GroupProductGuid, GroupGuid, SortIndex, ProductGuid 
                FROM    GroupProduct
                WHERE   EXISTS(SELECT ProductGuid FROM temp_GroupTable WHERE GroupProduct.GroupGuid = temp_GroupTable.GroupGuid) 
            ) , 
            temp1(GroupProductGuid, GroupGuid, SortIndex, ProductName, ProductGuid) AS 
            ( 
                SELECT  gp.GroupProductGuid,gp.GroupGuid, gp.SortIndex, p.ProductName, p.ProductGuid  
                FROM    temp_GroupProduct gp 
                        INNER JOIN ProductTable p ON gp.ProductGuid = p.ProductGuid 
            ),
            temp2(ProductName, ProductGuid) AS 
            ( 
                SELECT  trans.ProductName, trans.ProductGuid 
                FROM    ProductTranslation trans 
                " + variant + @"
            ) ,
            temp3(ProductGuid) AS
            ( 
                SELECT ProductGuid FROM temp1 
                EXCEPT 
                SELECT ProductGuid FROM temp2 
            ),
            temp4(ProductName, ProductGuid) AS 
            (  
                SELECT  ProductName, ProductGuid 
                FROM    temp1 
                WHERE   ProductGuid IN(SELECT * FROM temp3) 
            ),
            temp5(ProductName, ProductGuid) AS
            (  
                SELECT  CAST(ProductName AS NVARCHAR(50)), ProductGuid 
                FROM    temp2 
                UNION 
                SELECT ProductName, ProductGuid
                FROM temp4 
            ) 
            SELECT  temp1.GroupProductGuid, GroupGuid, SortIndex, temp5.ProductName, temp1.ProductGuid 
            FROM    temp1 
                    INNER JOIN temp5 ON temp1.ProductGuid = temp5.ProductGuid 
            OPTION(HASH JOIN)";


            List<GroupProduct> groupProductList = GetResults<GroupProduct>(sql, new { languageGuid = _propertiesDataService.GetExternalLanguageGuid(languageGuid), groupGuid}).ToList();
            //
            Dictionary<string, List<PropValPropTransQuerySet>> propTransDict = _propertiesDataService.LoadSubGroupProductProperties(groupGuid, "PRD", languageGuid);
            
            foreach (GroupProduct groupProduct in groupProductList)
            {
                var propValSet = new PrdPropValSet();
                var propDict = new Dictionary<string, string>();

                if (!propTransDict.ContainsKey(groupProduct.ProductGuid))
                {
                    // Add empty props instead of removing GroupProduct
                    propValSet.PropDict = propDict;
                    groupProduct.Properties = propValSet;
                    continue;
                }
                List<PropValPropTransQuerySet> tempSet = propTransDict[groupProduct.ProductGuid];
                propValSet = new PrdPropValSet();
                propDict = new Dictionary<string, string>();
                foreach (PropValPropTransQuerySet set in tempSet)
                {
                    if (!propDict.ContainsKey(set.PropGuid))
                        propDict.Add(set.PropGuid, set.PropTransText);
                }
                propValSet.PropDict = propDict;
                groupProduct.Properties = propValSet;
            }
            
            return groupProductList;
        }

        public List<Product> GetProducts(ICollection<string> guids, string languageGuid)
        {
            if (guids.Count == 0) return new List<Product>();

            guids = guids.Select(s => "'" + s + "'").ToList();
            var builder = string.Join(",", guids);

            string transText = HasVariantCode()
                ? "AND (ProductTranslation.VariantCode = '' OR ProductTranslation.VariantCode IS NULL)) "
                : ")";

            string sql =
                @"SELECT ProductTable.*, IsNull(ProductTranslation.ProductName, ProductTable.ProductName) AS ProductNameTranslate FROM 
                           ProductTable LEFT JOIN
                           ProductTranslation ON (ProductTable.ProductGuid=ProductTranslation.ProductGuid and ProductTranslation.LanguageGuid = @languageGuid " + transText + @"
                           WHERE ProductTable.ProductGuid IN(" + builder + @")";


            var productList = GetResults(sql, new { languageGuid= _propertiesDataService.GetExternalLanguageGuid(languageGuid) }).ToList();
            
            // Get correct translation
            productList.ForEach(p => p.ProductName = p.ProductNameTranslate ?? p.ProductName);

            // LOAD PROPERTIES GOES HERE
            Dictionary<string, List<PropValPropTransQuerySet>> propTransDict = _propertiesDataService.LoadProperties2("PRD", languageGuid, builder);
            
            foreach (Product product in productList)
            {
                var propValSet = new PrdPropValSet();
                var propDict = new Dictionary<string, string>();

                // Look for products with no properties and add an empty property set to each of them
                if (!propTransDict.ContainsKey(product.ProductGuid))
                {
                    propValSet = new PrdPropValSet();
                    propDict = new Dictionary<string, string>();
                    propValSet.PropDict = propDict;
                    product.Properties = propValSet;
                    continue;
                }

                List<PropValPropTransQuerySet> tempSet = propTransDict[product.ProductGuid];
                
                foreach (PropValPropTransQuerySet set in tempSet)
                {
                    if (!propDict.ContainsKey(set.PropGuid))
                        propDict.Add(set.PropGuid, set.PropTransText);
                }

                propValSet.PropDict = propDict;
                product.Properties = propValSet;
            }

            return productList;
        }


        public List<Product> FindProducts(string searchQuery)
        {
            var query = string.Format("SELECT * FROM {0} WHERE ProductGuid LIKE '%' + @searchQuery + '%' OR ProductName LIKE '%' + @searchQuery + '%'", GetTableName());
            var param = new { searchQuery };

            return GetResults<Product>(query, param).ToList();
        }
        

        public int GetTotalRecords()
        {
            return GetTotalRecordsFiltered("");
        }

        public int GetTotalRecordsFiltered(string filter)
        {
            return GetProductsCount(filter);
        }
    }
}