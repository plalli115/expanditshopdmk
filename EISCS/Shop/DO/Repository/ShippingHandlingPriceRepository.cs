﻿using System.Collections.Generic;
using System.Linq;
using CmsPublic.DataRepository;
using EISCS.Shop.DO.Dto;
using EISCS.Shop.DO.Interface;

namespace EISCS.Shop.DO.Repository
{
	[TableProperties(TableName = "ShippingHandlingPrice")]
	public class ShippingHandlingPriceRepository : BaseRepository<ShippingHandlingPrice>, IShippingHandlingPriceRepository
	{
		public ShippingHandlingPriceRepository(IExpanditDbFactory dbFactory) : base(dbFactory)
		{
		}


		public List<ShippingHandlingPrice> GetShippingHandlingPrice(string providerGuid)
		{
			string query = string.Format("SELECT * FROM {0} WHERE ShippingHandlingProviderGuid = @providerGuid", GetTableName());
			var param = new {providerGuid};

			return GetResults(query, param).ToList();
		}
	}
}