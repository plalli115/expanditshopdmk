﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using CmsPublic.DataRepository;
using Dapper;
using EISCS.Shop.DO.Dto;
using EISCS.Shop.DO.Dto.Cart;
using EISCS.Shop.DO.Interface;

namespace EISCS.Shop.DO.Repository
{
    [TableProperties(TableName = "ShopSalesHeader", PrimaryKeyName = "HeaderGuid")]
    public class ShopSalesHeaderRepository : BaseRepository<ShopSalesHeader>, IShopSalesHeaderRepository
    {	    
        public ShopSalesHeaderRepository(IExpanditDbFactory dbFactory): base(dbFactory)
	    {
		   
	    }

        public List<ShopSalesHeader> GetShopSalesHeadersByUser(string userGuid)        
        {
            string query = string.Format(@"SELECT * FROM {0} WHERE UserGuid = @userGuid Order By HeaderDate Desc", GetTableName());
            var param = new { userGuid };
            return GetResults(query, param).ToList();
        }

        public List<ShopSalesHeader> GetShopSalesHeadersByCustRef(string custRef)
        {
            string query = string.Format(@"SELECT * FROM {0} WHERE LOWER(CustomerReference) like LOWER(@custRef) Order By HeaderDate Desc", GetTableName());
            var param = new { custRef = string.Format("%{0}%", custRef) };
            return GetResults(query, param).ToList();
        }

        public List<ShopSalesHeader> GetShopSalesHeadersByCustRef(string userGuid, string custRef)
        {
            string query = string.Format(@"SELECT * FROM {0} WHERE UserGuid = @userGuid AND LOWER(CustomerReference) like LOWER(@custRef) Order By HeaderDate Desc", GetTableName());
            var param = new { userGuid , custRef = string.Format("%{0}%", custRef)};
            return GetResults(query, param).ToList();
        }

        public IEnumerable<ShopSalesHeader> GetShopSalesHeadersByPaymentTransactionId(string paymentTransactionId)
        {
            string query = string.Format("SELECT * FROM {0} WHERE PaymentTransactionID = @paymentTransactionId", GetTableName());
            return GetResults(query, new {paymentTransactionId});
        }

        public ShopSalesHeader Add(ShopSalesHeader item)
	    {
		    var sqlQuery = string.Format("INSERT INTO {0} ("+
				"HeaderGuid, " +
				"Address1, " +
				"Address2, " +
				"CityName, " +
				"CompanyName, " +
				"ContactName, " +
				"CountryGuid, " +
				"CountyName, " +
				"CurrencyGuid, " +
				"CustomerPONumber, " +
				"CustomerReference, " +
				"EmailAddress, " +
				"HeaderComment, " +
				"HeaderDate, " +
				"InvoiceDiscount, " +
				"ServiceCharge, " +
				"PaymentType, " +
                "PromotionCode, " +
				"ShipToAddress1, " +
				"ShipToAddress2, " +
				"ShipToCityName, " +
				"ShipToCompanyName, " +
				"ShipToContactName, " +
				"ShipToCountryGuid, " +
				"ShipToCountyName, " +
				"ShipToEmailAddress, " +
				"ShipToStateName, " +
				"ShipToZipCode, " +
				"StateName, " +
				"SubTotal, " +
				"SubTotalInclTax, " +
				"TaxCode, " +
				"TotalInclTax, " +
				"UserGuid," +
				"ZipCode, " +
				"Total, " +
				"TaxAmount, " +
				"ShippingHandlingProviderGuid, " +
				"ShippingAmount, " +
				"ShippingAmountInclTax, " +
				"HandlingAmount, " +
				"HandlingAmountInclTax, " +
				"ServiceChargeInclTax, " +
				"InvoiceDiscountInclTax, " +
				"PaymentFeeAmount, " +
				"PaymentFeeAmountInclTax, " +
				"PaymentStatus, " +
				"InvoiceDiscountPct, " +
				"TaxPct, " +
				"PricesIncludingVat, " +
				"VatTypes) " +
				"OUTPUT INSERTED.* VALUES (" +
				"@headerGuid, "+
				"@address1, " +
				"@address2, " +
				"@cityName, " +
				"@companyName, " +
				"@contactName, " +
				"@countryGuid, " +
				"@countyName, " +
				"@currencyGuid, " +
				"@customerPONumber, " +
				"@customerReference, " +
				"@emailAddress, " +
				"@headerComment, " +
				"@headerDate, " +
				"@InvoiceDiscount, " +
				"@serviceCharge, " +
				"@paymentType, "+
                "@promotionCode, " +
				"@shipToAddress1, " +
				"@shipToAddress2, " +
				"@shipToCityName, " +
				"@shipToCompanyName, " +
				"@shipToContactName, " +
				"@shipToCountryGuid, " +
				"@shipToCountyName, " +
				"@shipToEmailAddress, " +
				"@shipToStateName, " +
				"@shipToZipCode, " +
				"@stateName, " +
				"@subTotal, " +
				"@subTotalInclTax, " +
				"@taxCode, " +
				"@totalInclTax, " +
				"@userGuid, "+
				"@zipCode, " +
				"@total, " +
				"@taxAmount, " +
				"@shippingHandlingProviderGuid, " +
				"@shippingAmount, " +
				"@shippingAmountInclTax, " +
				"@handlingAmount, " +
				"@handlingAmountInclTax, " +
				"@serviceChargeInclTax, " +
				"@invoiceDiscountInclTax, " +
				"@paymentFeeAmount, " +
				"@paymentFeeAmountInclTax, " +
				"@paymentStatus, " +
				"@invoiceDiscountPct, " +
				"@taxPct, " +
				"@pricesIncludingVat, " +
				"@vatTypes)", GetTableName());

		    var param = new
			    {
					item.HeaderGuid,
					item.Address1,
					item.Address2,
					item.CityName,
					item.CompanyName,
					item.ContactName,
					item.CountryGuid,
					item.CountyName,
					item.CurrencyGuid,
					item.CustomerPONumber,
					item.CustomerReference,
					item.EmailAddress,
					item.HeaderComment,
					item.HeaderDate,
					item.InvoiceDiscount,
					item.ServiceCharge,
					paymentType = item.PaymentMethod,
                    item.PromotionCode,
					item.ShipToAddress1,
					item.ShipToAddress2,
					item.ShipToCityName,
					item.ShipToCompanyName,
					item.ShipToContactName,
					item.ShipToCountryGuid,
					item.ShipToCountyName,
					item.ShipToEmailAddress,
					item.ShipToStateName,
					item.ShipToZipCode,
					item.StateName,
					item.SubTotal,
					item.SubTotalInclTax,
					item.TaxCode,
					item.TotalInclTax,
					item.UserGuid,
					item.ZipCode,
					item.Total,
					item.TaxAmount,
					item.ShippingHandlingProviderGuid,
					item.ShippingAmount,
					item.ShippingAmountInclTax,
					item.HandlingAmount,
					item.HandlingAmountInclTax,
					item.ServiceChargeInclTax,
					item.InvoiceDiscountInclTax,
					item.PaymentFeeAmount,
					item.PaymentFeeAmountInclTax,
					item.PaymentStatus,
					item.InvoiceDiscountPct,
					item.TaxPct,
					item.PricesIncludingVat,
					item.VatTypes,
				};

			IDbConnection connection = GetConnection();

		    try
		    {
			    return connection.Query<ShopSalesHeader>(sqlQuery, param).SingleOrDefault();
		    }
		    finally
		    {
			    DbFactory.Finalize(connection);
		    }
	    }

        public void MailUpdate(ShopSalesHeader shopSalesHeader)
        {
            var mailSent = shopSalesHeader.MailSent;
            var customerRef = shopSalesHeader.CustomerReference;
            using (var connection = GetConnection())
            {
                var query = string.Format(@"UPDATE {0} SET " +
                                          "MailSent = @mailsent " +
                                          "WHERE CustomerReference = @customerRef", GetTableName());

                connection.Execute(query, new
                {
                    mailSent,
                    customerRef
                });
            }
        }

        public List<ShopSalesHeader> GetLastShopSales()
        {
            var query = String.Format("SELECT TOP 10 * FROM {0} ORDER BY HeaderDate DESC", GetTableName());
            return GetResults(query).ToList();
        }

        public List<ShopSalesHeader> GetLatestShopSalesInXDays(int days)
        {
            var query = String.Format("SELECT * FROM {0} WHERE HeaderDate >= DATEADD(day, -@days, GETDATE()) ORDER BY HeaderDate DESC", GetTableName());
            var param = new {days};
            return GetResults(query, param).ToList();
        }

        public List<ShopSalesHeader> GetSalesHeadersBetweenDates(DateTime initDate, DateTime endDate)
        {
            var query = String.Format("SELECT Sum(TotalInclTax) AS TotalInclTax, Sum(Total) AS Total, CurrencyGuid FROM {0} WHERE HeaderDate BETWEEN @initDate AND @endDate GROUP BY CurrencyGuid", GetTableName());
            var param = new {initDate, endDate};
            return GetResults(query, param).ToList();
        }

        public List<ShopSalesHeader> GetShopSalesRange(string userGuid, int startIndex, int endIndex, string searchValue, string sort = null, string direction = null)
        {
            var query = String.Format(
                @"SELECT x.* FROM (" +
                @"SELECT ROW_NUMBER() OVER (ORDER BY " + sort + @" " + direction + @", HeaderGuid) AS RowID," +
                @" * FROM {0} WHERE UserGuid = @userGuid) x " + 
                @"WHERE x.RowID BETWEEN @startIndex AND @endIndex ORDER BY x.RowID", GetTableName()); 
            var param = new
                {
                    userGuid,
                    startIndex,
                    endIndex
                };
            return GetResults(query, param).ToList();
        }
    }
}