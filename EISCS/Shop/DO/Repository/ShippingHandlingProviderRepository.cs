﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.Versioning;
using System.Web;
using CmsPublic.DataRepository;
using EISCS.Shop.DO.Dto;
using EISCS.Shop.DO.Interface;


namespace EISCS.Shop.DO.Repository
{
	[TableProperties(TableName = "ShippingHandlingProvider", PrimaryKeyName = "ShippingHandlingProviderGuid")]
	public class ShippingHandlingProviderRepository : BaseRepository<ShippingHandlingProvider>, IShippingHandlingProviderRepository
	{
		private readonly HttpContextBase _httpContext;

		public ShippingHandlingProviderRepository(IExpanditDbFactory dbFactory, HttpContextBase httpContext) : base(dbFactory)
		{
			_httpContext = httpContext;
		}

		public List<ShippingHandlingProvider> GetShippingHandlingProviders()
		{
			string query = string.Format("SELECT * FROM {0}", GetTableName());

			List<ShippingHandlingProvider> shippingHandlingProviders = GetResults(query).ToList();

			for (int i = 0; i < shippingHandlingProviders.Count; i++) {

				shippingHandlingProviders[i] = Translate(shippingHandlingProviders[i]);
			}

			return shippingHandlingProviders;
		}

		public ShippingHandlingProvider GetShippingHandlingProvider(string shippingHandlingProviderGuid)
		{
			if (string.IsNullOrEmpty(shippingHandlingProviderGuid))
				return null;

			string query = string.Format("SELECT * FROM {0} WHERE ShippingHandlingProviderGuid=@shippingHandlingProviderGuid", GetTableName());
			var param = new { shippingHandlingProviderGuid };

			ShippingHandlingProvider shippingHandlingProvider = GetResults(query, param).SingleOrDefault();

			if (shippingHandlingProvider != null)
				shippingHandlingProvider = Translate(shippingHandlingProvider);

			return shippingHandlingProvider;
		}

		private ShippingHandlingProvider Translate(ShippingHandlingProvider provider)
		{
			var globalResourceObject = _httpContext.GetGlobalResourceObject("Language", provider.ProviderName);
			if (globalResourceObject != null) {
				provider.ProviderName = globalResourceObject.ToString();
			}

			globalResourceObject = _httpContext.GetGlobalResourceObject("Language", provider.ProviderDescription);
			if (globalResourceObject != null) {
				provider.ProviderDescription = globalResourceObject.ToString();
			}

			return provider;
		}
	}
}