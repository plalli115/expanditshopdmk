﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using AutoMapper;
using CmsPublic.DataRepository;
using Dapper;
using EISCS.Shop.DO.Dto;
using EISCS.Shop.DO.Interface;
using EISCS.WebUserControlLogic.Country;

namespace EISCS.Shop.DO.Repository
{
    [TableProperties(TableName = "UserTable", PrimaryKeyName = "UserGuid")]
    public class UserItemRepository : BaseRepository<UserTable>, IUserItemRepository
    {
        private readonly CountryControlLogic _countryControlLogic;

        public UserItemRepository(IExpanditDbFactory dbFactory, CountryControlLogic countryControlLogic)
            : base(dbFactory)
        {
            _countryControlLogic = countryControlLogic;
        }

        public override UserTable Add(UserTable user, bool useOutputClause = true)
        {
            user.UserGuid = Guid.NewGuid().ToString();
            string query = string.Format("INSERT INTO {0} {1}", GetTableName(), GenerateInsertParameters(user));
            query += " INSERT INTO UserRole VALUES (@UserGuid, @RoleId)";
            return GetResults(query, user).SingleOrDefault();
        }

        public override bool Remove(string id)
        {
            string query = string.Format(@"DELETE FROM {0} WHERE {1} = @id", GetTableName(), GetIdColumnName());
            query += " DELETE FROM UserRole WHERE UserGuid = @id";
            return Execute(query, new { id });
        }

        public UserTable GetUserByUserName(string userName)
        {
            string sql = string.Format("SELECT TOP 1 * FROM {0} WHERE LOWER(UserLogin)=@userName", GetTableName());

            var param = new { userName };

            UserTable userTable = GetResults(sql, param).FirstOrDefault();
            return userTable;
        }

        public UserTable GetUserByEmailAndUserLogin(string email, string userLogin)
        {
            string sql = string.Format("SELECT TOP 1 * FROM {0} WHERE LOWER(EmailAddress)=@email AND LOWER(UserLogin)=@userLogin", GetTableName());

            var param = new
            {
                email,
                userLogin
            };

            UserTable userTable = GetResults(sql, param).FirstOrDefault();
            return userTable;
        }

        public UserTable GetUserByRequestResetPasswordToken(string requestToken)
        {
            string sql = string.Format("SELECT * FROM {0} WHERE PasswordResetRequestKey=@requestToken", GetTableName());
            return GetResults(sql, new { requestToken }).FirstOrDefault();
        }

        public bool UpdateLanguage(UserTable user, string lang)
        {
            string query = string.Format(@"UPDATE {0}
										 SET LanguageGuid = @languageGuid
										 WHERE UserGuid = @userGuid", GetTableName());
            var param = new
                {
                    languageGuid = lang,
                    userGuid = user.UserGuid
                };

            return Execute(query, param);
        }

        public bool UpdateCurrency(UserTable user, string newCurrencyGuid)
        {
            string query = string.Format(@"UPDATE {0}
										 SET CurrencyGuid = @currencyGuid
										 WHERE UserGuid = @userGuid", GetTableName());
            var param = new
                {
                    currencyGuid = newCurrencyGuid,
                    userGuid = user.UserGuid
                };
            return Execute(query, param);
        }

        public bool UpdatePassword(string userGuid, string newPassword, string newEncryptionType, string newEncryptionSalt)
        {
            var now = DateTime.Now;
            var query = string.Format(@"UPDATE {0}
										 SET UserPassword = @newPassword, EncryptionType = @newEncryptionType, EncryptionSalt = @newEncryptionSalt, LastPasswordChangeDate = @now
										 WHERE UserGuid = @userGuid", GetTableName());
            var param = new
                {
                    newPassword,
                    newEncryptionType,
                    newEncryptionSalt,
                    userGuid,
                    now
                };
            return Execute(query, param);
        }

        public bool UpdatePasswordWithVersion(string userGuid, string newPassword, string newEncryptionType, string newEncryptionSalt, string newPasswordVersion)
        {
            var now = DateTime.Now;
            var query = string.Format(@"UPDATE {0}
										 SET UserPassword = @newPassword, EncryptionType = @newEncryptionType, EncryptionSalt = @newEncryptionSalt, PasswordVersion = @newPasswordVersion, LastPasswordChangeDate = @now
										 WHERE UserGuid = @userGuid", GetTableName());
            var param = new
            {
                newPassword,
                newEncryptionType,
                newEncryptionSalt,
                userGuid,
                newPasswordVersion,
                now
            };
            return Execute(query, param);
        }

        public ShippingAddress GetBillingAddress(string userGuid)
        {
            var user = GetById(userGuid);
            var shippingAddress = Mapper.Map<UserTable, ShippingAddress>(user);
            shippingAddress.AddressType = user.IsB2B ? "B2B" : "B2C";
            shippingAddress.IsDefault = true;

            var countryRow = _countryControlLogic.LoadCountries().Select().SingleOrDefault(c => (string)c["CountryGuid"] == shippingAddress.CountryGuid);
            shippingAddress.CountryName = (countryRow == default(DataRow)) ? string.Empty : (string)countryRow["CountryName"];

            return shippingAddress;
        }

        public int GetB2CUsersCount()
        {
            return GetB2CUsersCount(null);
        }

        public int GetB2CUsersCount(string searchValue)
        {
            IDbConnection connection = GetConnection();
            var filter = String.Empty;
            if (searchValue != null)
            {
                filter = @"AND (ContactName LIKE('%" + searchValue + "%') " +
                         "OR Address1 LIKE('%" + searchValue + "%') " +
                         "OR Address2 LIKE('%" + searchValue + "%') " +
                         "OR CityName LIKE('%" + searchValue + "%') " +
                         "OR ZipCode LIKE('%" + searchValue + "%') " +
                         "OR CountryGuid LIKE('%" + searchValue + "%') " +
                         "OR EmailAddress LIKE('%" + searchValue + "%') " +
                         ")";
                filter = filter.Replace("*", "%");
            }
            var query = string.Format(@"SELECT COUNT(*) FROM {0} WHERE IsB2B=0 {1}", GetTableName(), filter);
            var result = 0;
            try
            {
                result = connection.Query<int>(query).FirstOrDefault();
            }
            finally
            {
                DbFactory.Finalize(connection);
            }
            return result;
        }

        public List<UserTable> GetB2CUsersList(int startIndex, int endIndex, string columnSort, string direction, string searchValue)
        {
            var filter = String.Empty;
            if (searchValue != null)
            {
                filter = @"AND (ContactName LIKE('%" + searchValue + "%') " +
                         "OR Address1 LIKE('%" + searchValue + "%') " +
                         "OR Address2 LIKE('%" + searchValue + "%') " +
                         "OR CityName LIKE('%" + searchValue + "%') " +
                         "OR ZipCode LIKE('%" + searchValue + "%') " +
                         "OR CountryGuid LIKE('%" + searchValue + "%') " +
                         "OR EmailAddress LIKE('%" + searchValue + "%') " +
                         ")";
                filter = filter.Replace("*", "%");
            }
            var query = String.Format(@"SELECT * FROM
            (SELECT ROW_NUMBER() OVER (ORDER BY {0} {1}, UserGuid) as RowID, *,
            (SELECT TOP 1 HeaderDate FROM ShopSalesHeader WHERE UserGuid LIKE(UserTable.UserGuid) ORDER BY HeaderDate desc) AS LastOrder
            FROM {2} WHERE IsB2B=0 {3}) temp
            WHERE RowID BETWEEN @startIndex AND @endIndex ORDER BY RowID", columnSort, direction, GetTableName(), filter);
            var param = new
                {
                    columnSort,
                    startIndex,
                    endIndex
                };
            return GetResults(query, param).ToList();
        }

        public int CountTotalUsers()
        {
            var connection = GetConnection();
            var filter = String.Empty;
            var query = string.Format(@"SELECT COUNT(*) FROM {0}", GetTableName());
            int result;
            try
            {
                result = connection.Query<int>(query).FirstOrDefault();
            }
            finally
            {
                DbFactory.Finalize(connection);
            }
            return result;
        }

        public bool UpdateRequestPasswordKeyAndTimestamp(UserTable user)
        {
            string query = string.Format(@"UPDATE {0}
										 SET PasswordResetRequestKey = @passwordResetKey, PasswordResetRequestTime = @requestPasswordTime
										 WHERE UserGuid = @userGuid", GetTableName());
            var param = new
            {
                passwordResetKey = user.PasswordResetRequestKey,
                requestPasswordTime = user.PasswordResetRequestTime,
                userGuid = user.UserGuid
            };
            return Execute(query, param);
        }
    }
}