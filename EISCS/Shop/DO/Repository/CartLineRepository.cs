﻿using System.Collections.Generic;
using System.Linq;
using CmsPublic.DataRepository;
using EISCS.Shop.DO.Dto.Cart;
using EISCS.Shop.DO.Interface;

namespace EISCS.Shop.DO.Repository
{
    [TableProperties(TableName = "CartLine", PrimaryKeyName = "LineGuid")]
    public class CartLineRepository : BaseRepository<CartLine>, ICartLineRepository
    {
        public CartLineRepository(IExpanditDbFactory dbFactory)
            : base(dbFactory)
        {
        }

        public List<CartLine> GetCartLinesById(string headerGuid)
        {
            string query = string.Format("SELECT * FROM {0} WHERE HeaderGuid = @headerGuid", GetTableName());

            return GetResults(query, new {headerGuid}).ToList();
        }

        public bool Delete(IEnumerable<string> inParameter)
        {
            const string sqlQuery = "DELETE FROM CartLine WHERE LineGuid IN @inParameter";
            var param = new {inParameter};
            return Execute(sqlQuery, param);
        }
    }
}