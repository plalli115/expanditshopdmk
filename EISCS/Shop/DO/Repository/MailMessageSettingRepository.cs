﻿using System.Data;
using System.Linq;
using System.Reflection;
using CmsPublic.DataRepository;
using Dapper;
using EISCS.Shop.DO.Dto;
using EISCS.Shop.DO.Interface;

namespace EISCS.Shop.DO.Repository
{
    [TableProperties(TableName = "MailMessageSettings", PrimaryKeyName = "Id")]
    public class MailMessageSettingRepository : BaseRepository<MailMessageSettings>, IMailMessageSettingRepository
    {
        public MailMessageSettingRepository(IExpanditDbFactory dbFactory) : base(dbFactory)
        {
        }


        public int UpdateOrInsert(BaseMailMessageSettings settings)
        {
            var tableName = GetTableName();
            string query = string.Format(
                    @"IF EXISTS (SELECT * FROM MailMessageSettings)
                        BEGIN 
                            UPDATE {0} SET {1}
                        END
                        ELSE
                        BEGIN
                            INSERT INTO {2} {3}
                        END", tableName, GenerateUpdateParameters(settings), tableName, GenerateInsertParameters(settings));

            int rowsAffected;

            IDbConnection connection = GetConnection();

            try
            {
                rowsAffected = connection.Execute(query, settings);
            }
            finally
            {
                DbFactory.Finalize(connection);
            }
            return rowsAffected;
        }
    }

    public interface IMailMessageSettingRepository : IBaseRepository<MailMessageSettings>
    {
        int UpdateOrInsert(BaseMailMessageSettings settings);
    }
}
