﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Mvc.Html;
using CmsPublic.DataRepository;
using Dapper;
using EISCS.Shop.BO;
using EISCS.Shop.DO.Dto;
using EISCS.Shop.DO.Interface;

namespace EISCS.Shop.DO.Repository
{
    [TableProperties(TableName = "ShippingAddress", PrimaryKeyName = "ShippingAddressGuid")]
    public class ShippingAddressRepository : BaseRepository<ShippingAddress>, IShippingAddressRepository
    {
        public ShippingAddressRepository(IExpanditDbFactory dbFactory)
            : base(dbFactory)
        {
        }

        public ShippingAddress GetAddress(string addressGuid)
        {
            if (string.IsNullOrEmpty(addressGuid))
            {
                return null;
            }

            var sqlQuery = string.Format("SELECT * FROM {0} WHERE ShippingAddressGuid = @addressGuid", GetTableName());
            var param = new { addressGuid };

            return GetResults(sqlQuery, param).FirstOrDefault();
        }

	    public ShippingAddress GetB2BAddress(string addressGuid, string customerGuid)
	    {
			var sql = string.Format("SELECT *, ShippingAddressCode AS ShippingAddressGuid FROM ShippingAddressB2B WHERE CustomerGuid = @customerGuid AND ShippingAddressCode = @addressGuid");
			var param = new { customerGuid, addressGuid };
			return GetResults(sql, param).SingleOrDefault();
	    }

	    public List<ShippingAddress> GetB2BAddresses(string customerGuid)
	    {
		    var sql = string.Format("SELECT 'B2B' AS AddressType, ShippingAddressCode AS ShippingAddressGuid, * FROM ShippingAddressB2B WHERE CustomerGuid = @customerGuid");
		    var param = new {customerGuid};
		    return GetResults(sql, param).ToList();
	    }

	    public List<ShippingAddress> GetAddresses(string userGuid)
        {
            /*var sqlQuery = string.Format("SELECT 'B2C' AS AddressType, ss.ShippingAddressGuid, ss.Address1, ss.Address2, ss.CityName, ss.CompanyName, ss.ContactName, ss.CountryGuid, " +
                                            "ss.UserGuid, ss.IsDefault, ss.ZipCode, ss.StateName, ss.EmailAddress, ss.CountryName  " +
                                            "FROM {0} AS ss INNER JOIN  " +
                                            "UserTable AS ut ON ss.UserGuid = ut.UserGuid " +
                                            "WHERE (ss.UserGuid = @userGuid) " +
                                            "UNION " +
                                            "SELECT 'B2B' AS AddressType, NULL AS ShippingAddressGuid, c.Address1, c.Address2, c.CityName, c.CompanyName, c.ContactName, c.CountryGuid,  " +
                                            "c.UserGuid, NULL AS IsDefault, c.ZipCode, c.StateName, c.EmailAddress, c.CountryName " +
                                            "FROM {0} RIGHT OUTER JOIN " +
											"(SELECT s.Address1, s.Address2, s.CityName, s.CompanyName, IsNull(s.ContactName, u.ContactName) AS ContactName, s.CountryGuid, u.UserGuid, s.ZipCode, u.StateName, '' as CountryName, " +
                                            "IsNull(s.EmailAddress, u.EmailAddress) AS EmailAddress " +
                                            "FROM ShippingAddressB2B AS s INNER JOIN " +
                                            "UserTable AS u ON s.CustomerGuid = u.CustomerGuid " +
                                            "WHERE      (u.UserGuid = @userGuid)) AS c ON ShippingAddress.UserGuid = c.UserGuid " +
                                            "ORDER BY ss.ShippingAddressGuid;", GetTableName());
			*/

		    var sqlQuery = string.Format("SELECT 'B2C' AS AddressType, * FROM {0} WHERE UserGuid = @userGuid", GetTableName());
            var param = new { userGuid };

            return GetResults(sqlQuery, param).ToList();
        }

        public override ShippingAddress Add(ShippingAddress item, bool useOutputClause = true)
		{
			var query = string.Format(@"INSERT INTO {0} (Address1, Address2, CityName, CompanyName, ContactName, CountryGuid, CountyName, EmailAddress, ShippingAddressGuid, StateName, UserGuid, ZipCode, IsDefault, AddressType, CountryName) OUTPUT INSERTED.* VALUES (@address1, @address2, @cityName, @companyName, @contactName, @countryGuid, @countyName, @emailAddress, newId(), @stateName, @userGuid, @zipCode, 1, @addressType, @countryName)", GetTableName());

			var param = new
				{
					item.Address1,
					item.Address2,
					item.CityName,
					item.CompanyName,
					item.ContactName,
					item.CountryGuid,
					item.CountyName,
					item.EmailAddress,
					item.StateName,
					item.UserGuid,
					item.ZipCode,
					item.IsDefault,
					item.AddressType,
					item.CountryName
				};

			SetAllAddressToNotDefault(item.UserGuid);

			return GetResults(query, param).SingleOrDefault();
		}

	    private void SetAllAddressToNotDefault(string userGuid)
	    {
		    using (var connection = GetConnection())
		    {
			    var query = string.Format(@"UPDATE {0} SET " +
			                              "IsDefault = 0 " +
			                              "WHERE UserGuid = @userGuid", GetTableName());

			    connection.Execute(query, new
				    {
					    userGuid
				    });
		    }
	    }

	    public override bool Update(ShippingAddress item)
	    {
		    int affectedRecords;
		    using (var connection = GetConnection())
		    {
			    var query = string.Format(@"UPDATE {0} SET " +
											"Address1 = @address1, " +
											"Address2 = @address2, " +
											"CityName = @cityName, " +
											"CompanyName = @companyName, " +
											"ContactName = @contactName, " +
											"CountryGuid = @countryGuid, " +
											"CountyName = @countyName, " +
											"EmailAddress = @emailAddress, " +
											"StateName = @stateName, " +
											"ZipCode = @zipCode, " +
											"IsDefault = @isDefault, " +
											"AddressType = @addressType, " +
											"CountryName = @countryName " +
											"WHERE ShippingAddressGuid = @shippingAddressGuid", GetTableName());

				affectedRecords = connection.Execute(query, new {
					item.Address1,
					item.Address2,
					item.CityName,
					item.CompanyName,
					item.ContactName,
					item.CountryGuid,
					item.CountyName,
					item.EmailAddress,
					item.StateName,
					item.ZipCode,
					item.IsDefault,
					item.AddressType,
					item.CountryName,
					item.ShippingAddressGuid
				});
		    }

		    return (affectedRecords > 0);
	    }

	    public void NewDefaultShippingAddressForUser(string shippingAddressGuid, string userGuid)
	    {
		    using (var connection = GetConnection())
		    {
			    var query = string.Format(@"UPDATE {0} SET " +
			                              "IsDefault = " +
										  "(CASE WHEN ShippingAddressGuid <> @shippingAddressGuid " +
										  "THEN 0 " + 
										  "ELSE 1 " +
										  "END) " +
			                              "WHERE UserGuid = @userGuid", GetTableName());

			    connection.Execute(query, new
				    {
					    shippingAddressGuid,
					    userGuid
				    });
		    }
	    }
    }
}