﻿using System;
using System.Collections.Generic;
using System.Linq;
using CmsPublic.DataRepository;
using CmsPublic.Exceptions;
using CmsPublic.ModelLayer.Interfaces;
using CmsPublic.ModelLayer.Models;
using EISCS.Shop.DO.Interface;

namespace EISCS.Shop.DO.Repository
{
    [TableProperties(TableName = "MailMessage", PrimaryKeyName = "MailMessageGuid")]
    public class MailMessageRepository : BaseRepository<ExpanditMailMessage>, IMailMessageRepository
    {
        private IPropertiesDataService _prodPropertiesDataService;

        public MailMessageRepository(IExpanditDbFactory dbFactory, IPropertiesDataService propertiesDataService)
            : base(dbFactory)
        {
            _prodPropertiesDataService = propertiesDataService;
        }

        public List<Product> GetMailProducts(int messageGuid, string languageGuid)
        {

            string sql = @" SELECT  MailMessageProduct.MailMessageGuid, ProductTable.*, IsNull(ProductTranslation.ProductName, ProductTable.ProductName) AS ProductNameTranslate 
                            FROM    ProductTable 
                                    LEFT JOIN ProductTranslation ON (ProductTable.ProductGuid=ProductTranslation.ProductGuid and ProductTranslation.LanguageGuid = @languageGuid)
						            INNER JOIN MailMessageProduct ON (MailMessageProduct.ProductGuid = ProductTable.ProductGuid AND MailMessageProduct.MailMessageGuid = @messageGuid)
            ";

            List<Product> productList = GetResults<Product>(sql, new { languageGuid = _prodPropertiesDataService.GetExternalLanguageGuid(languageGuid), messageGuid }).ToList();

            // Get correct translation
            productList.ForEach(p => p.ProductName = p.ProductNameTranslate ?? p.ProductName);

            return productList;

        }

        public IEnumerable<string> GetMailProducts(int id)
        {
            string sql = "SELECT ProductGuid FROM MailMessageProduct WHERE MailMessageGuid = @MailMessageGuid";
            return GetResults<string>(sql, new { MailMessageGuid = id });
        }

        public void SetMailProducts(string mailmessageGuid, IEnumerable<string> guids)
        {
            if (string.IsNullOrWhiteSpace(mailmessageGuid))
            {
                return;
            }

            int messageGuid;

            bool isAnumber = int.TryParse(mailmessageGuid, out messageGuid);
            if (!isAnumber)
            {
                return;
            }

            var existing = GetMailProducts(messageGuid);
            var enumerable = guids.Except(existing);
            List<MailMessageProduct> mailMessageProducts = enumerable.Select(guid => new MailMessageProduct { MailMessageGuid = mailmessageGuid, ProductGuid = guid }).ToList();

            string sql = @"INSERT INTO MailMessageProduct VALUES (@MailMessageGuid, @ProductGuid)";

            Execute(sql, mailMessageProducts);
        }

        public void RemoveMailProducts(int id, IEnumerable<string> productGuids)
        {
            var enumerable = productGuids as string[] ?? productGuids.ToArray();

            string sql = "DELETE FROM MailMessageProduct WHERE MailMessageGuid = @MailMessageGuid AND ProductGuid IN @Products";

            Execute(sql, new { MailMessageGuid = id, Products = enumerable });
        }

        public ExpanditMailMessage GetMessageByName(string messageName, string languageGuid, bool isLoadProperties)
        {
            string sql = @" SELECT  * 
                            FROM    MailMessage 
                            WHERE   MailMessageName = @messageName";

            var mailmessage = GetSingle(sql, new { messageName });
            if (isLoadProperties && mailmessage != null)
            {
                var list = new List<IPropertyOwner>() { mailmessage };
                _prodPropertiesDataService.LoadProperties(list, languageGuid);
            }
            return mailmessage;
        }

        public List<ExpanditMailMessage> GetAll(string languageGuid, bool isLoadProperties)
        {
            var mailMessageList = All().ToList();

            if (isLoadProperties && mailMessageList.Any())
            {
                _prodPropertiesDataService.LoadProperties(mailMessageList.Cast<IPropertyOwner>().ToList(), languageGuid);
            }
            return mailMessageList;
        }

        /// <summary>
        /// Get a specific Mail Message, with or without Properties
        /// </summary>
        /// <param name="messageGuid"></param>
        /// <param name="languageGuid"></param>
        /// <param name="isLoadProperties"></param>
        /// <returns></returns>
        public ExpanditMailMessage GetMessage(int messageGuid, string languageGuid, bool isLoadProperties)
        {
            var mailmessage = GetById(messageGuid);
            if (isLoadProperties && mailmessage != null)
            {
                var list = new List<IPropertyOwner>() { mailmessage };
                _prodPropertiesDataService.LoadProperties(list, languageGuid);
            }
            return mailmessage;
        }

        public void SaveProperties(ExpanditMailMessage product, string languageGuid)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Get a specific Mail Message with both Editor and PropVal Properties
        /// </summary>
        /// <param name="messageGuid"></param>
        /// <param name="languageGuid"></param>
        /// <returns></returns>
        public ExpanditMailMessage GetMessageForEditor(int messageGuid, string languageGuid)
        {
            ExpanditMailMessage m = GetMessage(messageGuid, languageGuid, true);
            if (m != null)
            {
                var mailMessageList = new List<IPropertyOwner> { m };
                _prodPropertiesDataService.LoadPropertiesForEdit(mailMessageList, languageGuid);
            }
            else
            {
                throw new MailProviderException("Could not load Mail");
            }
            return m;
        }

        public ExpanditMailMessage GetDummyMessageForEditor(string languageGuid)
        {
            var dummy = new ExpanditMailMessage { MailMessageGuid = 0 };
            var mailMessageList = new List<IPropertyOwner>(1) { dummy };
            _prodPropertiesDataService.LoadPropertiesForEdit(mailMessageList, languageGuid);

            return dummy;
        }

        private int GetHighestMessageGuid()
        {
            return GetSingle<int>("SELECT MAX(MailMessageGuid) FROM MailMessage");
        }

        public ExpanditMailMessage CreateMailMessage(string languageGuid, string messageName)
        {
            int newMessageGuid = GetHighestMessageGuid() + 1;
            var message = new ExpanditMailMessage()
                {
                    MailMessageGuid = newMessageGuid,
                    DateCreated = DateTime.Now.ToUniversalTime(),
                    Active = true,
                    MailMessageName = messageName
                };

            Add(message);

            return GetMessage(newMessageGuid, languageGuid, true);
        }

        /// <summary>
        /// Delete a ExpanditMailMessage and all its properties
        /// </summary>
        /// <param name="messageGuid"></param>
        /// <param name="languageGuid"></param>
        /// <returns></returns>
        public void DeleteMailMessage(int messageGuid, string languageGuid)
        {
            // Get Message to delete all properties
            ExpanditMailMessage mailMessage = GetMessage(messageGuid, languageGuid, true);

            _prodPropertiesDataService.DeleteAllProperties(mailMessage.Properties);

            Remove(messageGuid);
        }
    }
}