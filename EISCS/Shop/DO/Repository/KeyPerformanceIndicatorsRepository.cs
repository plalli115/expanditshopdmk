﻿using System;
using System.Collections.Generic;
using System.Linq;
using CmsPublic.DataRepository;
using EISCS.Shop.BO.BusinessLogic;
using EISCS.Shop.DO.Dto;
using EISCS.Shop.DO.Dto.Cart;
using EISCS.Shop.DO.Interface;
using EISCS.Wrappers.Configuration;

namespace EISCS.Shop.DO.Repository
{
    [TableProperties(TableName = "KeyPerformanceIndicators", PrimaryKeyName = "EntryDate")]
    public class KeyPerformanceIndicatorsRepository : BaseRepository<KPI>, IKeyPerformanceIndicatorsRepository
    {
        private readonly  IShopSalesLineRepository _shopSalesLineRepository;
        private readonly ICurrencyConverter _currencyConverter;
        private readonly IConfigurationObject _config;

        public KeyPerformanceIndicatorsRepository(IExpanditDbFactory dbFactory, IShopSalesLineRepository shopSalesLineRepository, ICurrencyConverter currencyConverter, IConfigurationObject config) : base(dbFactory)
        {
            _shopSalesLineRepository = shopSalesLineRepository;
            _currencyConverter = currencyConverter;
            _config = config;
        }

        public List<KPI> GetLastMonthReport()
        {
            var lastMonth = GetReportFromXMonthsBack(1);
            var twoMonthsAgo = GetReportFromXMonthsBack(2);

            var list = new List<KPI>
                {
                    lastMonth, 
                    twoMonthsAgo
                };
            
            return list;
        }

        private KPI GetReportFromXMonthsBack(int months)
        {
            var lastMonth = DateTime.Today.AddMonths(-months).Month;
            var theYear = lastMonth == 12 ? DateTime.Today.Year - 1 : DateTime.Today.Year;
            var initDate = new DateTime(theYear, lastMonth, 1);
            var endDate = new DateTime(theYear, lastMonth, DateTime.DaysInMonth(theYear, lastMonth));
            var reportsBetweenDates = GetReportBetweenDates(initDate, endDate);

            return GetTotals(reportsBetweenDates, initDate, endDate);
        }

        public KPI GetTotals(List<KPI> report, DateTime initDate, DateTime endDate)
        {
            var totals = new KPI();
            if (report.Any())
            {
                foreach (var dayReport in report)
                {
                    totals.TotalLogins += dayReport.TotalLogins;
                    totals.TotalItems += dayReport.TotalItems;
                    totals.TotalOrders += dayReport.TotalOrders;
                    totals.TotalRevenueWithoutVAT += dayReport.TotalRevenueWithoutVAT;
                    totals.NewUsers += dayReport.NewUsers;
                }
                totals.EntryDate = report.First().EntryDate;
                totals.AvgItemPrize = totals.TotalRevenueWithoutVAT/totals.TotalItems;
                totals.AvgOrderSize = totals.TotalItems/(double) totals.TotalOrders;
                if (Double.IsNaN(totals.AvgOrderSize))
                    totals.AvgOrderSize = 0;
            }
            else
            {
                totals.EntryDate = initDate;
            }

            return totals;
        }

        public bool NewLogin()
        {
            var today = DateTime.Today;
            var sql = String.Format(@"SELECT * FROM {0} WHERE EntryDate = @today", GetTableName());
            var param = new {today};
            var result = GetResults(sql, param).SingleOrDefault();
            if (result == null)
            {
                result = new KPI {TotalLogins = 1, EntryDate = today};
                Add(result);
                return true;
            }
            result.TotalLogins++;
            return Update(result);
        }

        public bool NewUser()
        {
            var today = DateTime.Today;
            var sql = String.Format(@"SELECT * FROM {0} WHERE EntryDate = @today", GetTableName());
            var param = new { today };
            var result = GetResults(sql, param).SingleOrDefault();
            if (result == null)
            {
                result = new KPI{NewUsers = 1, EntryDate = today};
                Add(result);
                return true;
            }
            result.NewUsers++;
            return Update(result);
        }

        public List<KPI> GetReportBetweenDates(DateTime initDate, DateTime endDate)
        {
            var sql = String.Format(@"SELECT * FROM {0} WHERE EntryDate BETWEEN @initDate AND @endDate", GetTableName());
            var param = new
                {
                    initDate,
                    endDate
                };

            return GetResults(sql, param).ToList();
        }

        public bool NewSale(ShopSalesHeader salesHeader)
        {
            if (GetOrderSize(salesHeader) == 0)
                return false;

            var theDate = salesHeader.HeaderDate;
            var dateString = theDate.ToString("yyyy-MM-dd");
            var sql = String.Format(@"SELECT * FROM {0} WHERE EntryDate = @dateString", GetTableName());
            var param = new { dateString }; 
            var result = GetResults(sql, param).SingleOrDefault();

            if (result == null)
            {
                result = new KPI
                    {
                        TotalOrders = 1,
                        EntryDate = theDate,
                        TotalRevenueWithoutVAT = _currencyConverter.ConvertCurrency(salesHeader.Total, salesHeader.CurrencyGuid, _config.Read("MULTICURRENCY_SITE_CURRENCY")),
                        AvgOrderSize = GetOrderSize(salesHeader),
                        AvgItemPrize = AvgItemPrizeOfOrder(salesHeader),
                        TotalItems = GetOrderSize(salesHeader),
                        NewUsers = 0,
                        TotalLogins = 0
                    };
                Add(result);
                return true;
            }

            result.TotalOrders++;
            result.TotalItems += GetOrderSize(salesHeader);
            result.TotalRevenueWithoutVAT += _currencyConverter.ConvertCurrency(salesHeader.Total, salesHeader.CurrencyGuid, _config.Read("MULTICURRENCY_SITE_CURRENCY"));
            result.AvgOrderSize = result.TotalItems/(double)result.TotalOrders;
            result.AvgItemPrize = result.TotalRevenueWithoutVAT/result.TotalItems;
            
            return Update(result);
        }

        public KPI GetLastDayReport()
        {
            var sql = String.Format(@"SELECT TOP 1 * FROM {0} ORDER BY EntryDate DESC", GetTableName());
            return GetResults(sql).SingleOrDefault();
        }

        private int GetOrderSize(ShopSalesHeader salesHeader)
        {
            var lines = _shopSalesLineRepository.GetShopSalesLinesByHeaderGuid(salesHeader.HeaderGuid);
            return Convert.ToInt32(lines.Sum(line => line.Quantity));
        }

        private double AvgItemPrizeOfOrder(ShopSalesHeader salesHeader)
        {
            var orderSize = GetOrderSize(salesHeader);
            return orderSize / _currencyConverter.ConvertCurrency(salesHeader.Total, salesHeader.CurrencyGuid, _config.Read("MULTICURRENCY_SITE_CURRENCY"));
        }
    }
}