﻿using System;
using System.Data;
using System.Linq;
using CmsPublic.DataRepository;
using EISCS.Shop.DO.Dto;
using EISCS.Shop.DO.Dto.Cart;
using EISCS.Shop.DO.Interface;
using ExpandIT;

namespace EISCS.Shop.DO.Repository
{
    [TableProperties(TableName = "CartHeader", PrimaryKeyName = "HeaderGuid")]
    public class CartHeaderRepository : BaseRepository<CartHeader>, ICartHeaderRepository
    {
	    private readonly IExpanditUserService _userService;

		public CartHeaderRepository(IExpanditDbFactory dbFactory, IExpanditUserService userService)
            : base(dbFactory)
	    {
		    _userService = userService;
	    }

	    public CartHeader GetCartHeaderByUser(string userGuid)
        {
            string query = string.Format(@"SELECT * FROM {0} WHERE UserGuid = @userGuid", GetTableName());

            var param = new {userGuid};

            return GetResults(query, param).FirstOrDefault();
        }

	    public CartHeader UpdateShippingAddress(ShippingAddress shippingAddress)
	    {
		    var cartHeader = GetCartHeaderByUser(_userService.UserGuid);

		    cartHeader.ShipToAddress1				= shippingAddress.Address1;
		    cartHeader.ShipToAddress2				= shippingAddress.Address2;
			cartHeader.ShipToCityName				= shippingAddress.CityName;
			cartHeader.ShipToCompanyName			= shippingAddress.CompanyName;
			cartHeader.ShipToContactName			= shippingAddress.ContactName;
			cartHeader.ShipToCountryGuid			= shippingAddress.CountryGuid;
			cartHeader.ShipToCountryName			= shippingAddress.CountryName;
			cartHeader.ShipToCountyName				= shippingAddress.CountyName;
			cartHeader.ShipToEmailAddress			= shippingAddress.EmailAddress;
			cartHeader.ShipToShippingAddressGuid	= shippingAddress.ShippingAddressGuid;
		    cartHeader.ShippingAddressGuid			= shippingAddress.ShippingAddressGuid;
			cartHeader.ShipToStateName				= shippingAddress.StateName;
		    cartHeader.ShipToZipCode				= shippingAddress.ZipCode;

		    return cartHeader;
	    }

	    public CartHeader MapBillingAddress(UserTable user)
	    {
		    var cartHeader = GetCartHeaderByUser(_userService.UserGuid);

		    cartHeader.Address1 = user.Address1;
		    cartHeader.Address2 = user.Address2;
		    cartHeader.CityName = user.CityName;
		    cartHeader.CompanyName = user.CompanyName;
		    cartHeader.ContactName = user.ContactName;
		    cartHeader.CountryGuid = user.CountryGuid;
		    cartHeader.CountyName = user.CountyName;
		    cartHeader.CountryName = _userService.GetCountryName(user.CountryGuid);
		    cartHeader.EmailAddress = user.EmailAddress;
		    cartHeader.StateName = user.StateName;
		    cartHeader.ZipCode = user.ZipCode;

		    if (Update(cartHeader))
		    {
			    return cartHeader;
		    }
		    throw new Exception("Unknown error updating user values on the cart");
		}
    }
}