﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using CmsPublic.DataRepository;
using Dapper;
using EISCS.Shop.Attributes;
using EISCS.Shop.DO.Interface;

namespace EISCS.Shop.DO.Repository
{
    public abstract class BaseRepository<T> : BaseDataService, IBaseRepository<T> where T : class
    {
        protected new readonly IExpanditDbFactory DbFactory;
        private readonly TablePropertiesAttribute _tableProperties;


        protected BaseRepository(IExpanditDbFactory dbFactory)
            : base(dbFactory)
        {
            DbFactory = dbFactory;
            _tableProperties = TablePropertiesAttribute.GetTableProperties(this);
        }

        public virtual string GetTableName()
        {
            if (string.IsNullOrWhiteSpace(_tableProperties.TableName))
            {
                throw new NullReferenceException("TableName must be set on the TableProperties attribute or override GetTableName method.");
            }

            return _tableProperties.TableName;
        }

        public virtual string GetIdColumnName()
        {
            if (string.IsNullOrWhiteSpace(_tableProperties.PrimaryKeyName))
            {
                throw new NullReferenceException("PrimaryKeyName must be set on the TableProperties attribute or override GetIdColumnName method.");
            }

            return _tableProperties.PrimaryKeyName;
        }

        public virtual string AllColumns()
        {
            return "*";
        }

        public IEnumerable<T> All()
        {
            string query = GetDefaultSql();

            return GetResults(query);
        }

        public virtual T GetById(int id)
        {
            return GetById(new { id });
        }

        public virtual T GetById(string id)
        {
            return GetById(new { id });
        }

        public virtual T Add(T item, bool useOutputClause = true)
        {
            string sqlQuery = string.Format("INSERT INTO {0} {1}", GetTableName(), GenerateInsertParameters(item, useOutputClause));

            IDbConnection connection = GetConnection();

            try
            {
                if (useOutputClause)
                {
                    return connection.Query<T>(sqlQuery, item).SingleOrDefault();
                }
                else
                {
                    connection.Execute(sqlQuery, item);
                    return null;
                }
            }
            finally
            {
                DbFactory.Finalize(connection);
            }
        }

        public virtual bool Update(T item)
        {
            PropertyInfo[] properties = item.GetType().GetProperties();
            PropertyInfo idProperty = properties.Single(p => p.Name == GetIdColumnName());

            string sqlQuery = string.Format("UPDATE {0} SET {1} WHERE {2} = @" + idProperty.Name, GetTableName(), GenerateUpdateParameters(item), GetIdColumnName());

            int rowsAffected;

            IDbConnection connection = GetConnection();

            try
            {
                rowsAffected = connection.Execute(sqlQuery, item);
            }
            finally
            {
                DbFactory.Finalize(connection);
            }

            return rowsAffected > 0;
        }

        public virtual bool Remove(int id)
        {
            return Delete(new { id });
        }

        public virtual bool Remove(string id)
        {
            return Delete(new { id });
        }

        public string GetDefaultSql()
        {
            return "select " + AllColumns() + " from " + GetTableName();
        }

        public IDbConnection GetConnection()
        {
            return DbFactory.GetConnection();
        }

        private T GetById(object param)
        {
            string query = string.Format("{0} where {1} = @id", GetDefaultSql(), GetIdColumnName());

            return GetResults(query, param).SingleOrDefault();
        }

        private bool Delete(object param)
        {
            string query = string.Format(@"DELETE FROM {0} WHERE {1} = @id", GetTableName(), GetIdColumnName());
            return Execute(query, param);
        }

        protected virtual IEnumerable<T> GetResults(string query, object param = null)
        {
            IEnumerable<T> result;
            IDbConnection connection = GetConnection();

            try
            {
                result = connection.Query<T>(query, param);
            }
            finally
            {
                DbFactory.Finalize(connection);
            }
            return result;
        }

        protected virtual T GetSingle(string query, object param = null)
        {
            T result;
            IDbConnection connection = GetConnection();

            try
            {
                result = connection.Query<T>(query, param).SingleOrDefault();
            }
            finally
            {
                DbFactory.Finalize(connection);
            }
            return result;
        }

        protected virtual bool Execute(string query, object param = null)
        {
            IDbConnection connection = DbFactory.GetConnection();
            try
            {
                int affectedRows = connection.Execute(query, param);

                return affectedRows > 0;
            }
            finally
            {
                DbFactory.Finalize(connection);
            }
        }
    }
}