﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using CmsPublic.DataRepository;
using Dapper;
using EISCS.Shop.DO.Dto;
using EISCS.Shop.DO.Interface;

namespace EISCS.Shop.DO.Repository
{
    [TableProperties(TableName = "CompanyTable")]
    public class CompanyRepository : BaseRepository<Company>, ICompanyRepository
    {
        public CompanyRepository(IExpanditDbFactory dbFactory) : base(dbFactory)
        {
        }

        public Company GetCompanyInfo()
        {
            var sql = String.Format(@"SELECT TOP 1 * FROM {0}", GetTableName());
            return GetResults(sql).FirstOrDefault();
        }

        public override bool Update(Company item)
        {
            DeleteRecords();
            if (item.CompanyGuid == null)
                item.CompanyGuid = "1";
            var dataReturned = Add(item);
            return dataReturned != null;
        }

        public override Company Add(Company item, bool useOutputClause = true)
        {
            var sql = String.Format("INSERT INTO {0} {1}", GetTableName(), GenerateInsertParameters(item, useOutputClause));

            var connection = GetConnection();
            int rowsAffected;
            try
            {
                rowsAffected = connection.Execute(sql, item);
            }
            finally
            {
                DbFactory.Finalize(connection);
            }

            return rowsAffected == 1 ? GetCompanyInfo() : null;
            
        }

        private void DeleteRecords()
        {
            var sql = String.Format(@"DELETE FROM {0}", GetTableName());
            Execute(sql);
        }

        private string GetColumnName(PropertyInfo propertyInfo)
        {
            var attributes = (TableColumnAttribute[])propertyInfo.GetCustomAttributes(typeof(TableColumnAttribute), false);
            var attribute = attributes.FirstOrDefault(a => !string.IsNullOrWhiteSpace(a.ColumnName));

            return (attribute != null) ? attribute.ColumnName : propertyInfo.Name;
        }

        public override string GenerateInsertParameters(object obj, bool useOutputClause = true)
        {
            var properties = obj.GetType().GetProperties();
            var columnNames = new List<string>();
            var parameters = new List<string>();
            foreach (var propInfo in properties)
            {
                var infos = (TableColumnAttribute[])propInfo.GetCustomAttributes(typeof(TableColumnAttribute), false);
                if (infos.Length == 0 || infos.All(i => i.MapForInsert))
                {
                    columnNames.Add("[" + GetColumnName(propInfo) + "]");
                    parameters.Add("@" + propInfo.Name);
                }
            }

            var firstPart = "(" + string.Join(",", columnNames) + ")";
            var secondPart = " VALUES (" + string.Join(",", parameters) + ")";

            return firstPart + secondPart;
        }
    }
}