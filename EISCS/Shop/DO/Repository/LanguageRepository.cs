﻿using System.Collections.Generic;
using System.Linq;
using CmsPublic.DataRepository;
using EISCS.Shop.DO.Dto;
using EISCS.Shop.DO.Interface;

namespace EISCS.Shop.DO.Repository
{
    [TableProperties(TableName = "LanguageTable", PrimaryKeyName = "LanguageGuid")]
    public class LanguageRepository : BaseRepository<LanguageTable>, ILanguageRepository
    {

        public LanguageRepository(IExpanditDbFactory dbFactory)
            : base(dbFactory)
        {
        }

        public List<LanguageTable> LoadLanguages()
        {
            var sql = string.Format(@"SELECT LanguageGuid, LanguageNameNative as LanguageName
                                         FROM {0} WHERE LanguageEnabled <> 0
                                         AND LanguageNameNative IS NOT NULL 
                                         UNION
                                         SELECT LanguageGuid, LanguageName FROM {0}
                                         WHERE LanguageEnabled <> 0
                                         AND LanguageNameNative IS NULL 
                                         ORDER BY LanguageName", GetTableName());

            return GetResults(sql).ToList();
        }


    }
}