﻿using System.Collections.Generic;
using System.Linq;
using CmsPublic.DataRepository;
using EISCS.Shop.DO.Dto;
using EISCS.Shop.DO.Interface;

namespace EISCS.Shop.DO.Repository {
	[TableProperties(TableName = "PaymentType")]
	public class PaymentTypeRepository : BaseRepository<PaymentTypeTable>, IPaymentTypeRepository
	{
	    private readonly IExpanditUserService _expanditUserService;

		public PaymentTypeRepository(IExpanditDbFactory dbFactory, IExpanditUserService expanditUserService) : base(dbFactory)
		{
		    _expanditUserService = expanditUserService;
		}

		public List<PaymentTypeTable> GetPaymentTypeByUserId(string userGuid)
		{
		    string userType = _expanditUserService.UserType.ToString();
			
			var sql = string.Format("SELECT * FROM {0} WHERE UserType = @userType", GetTableName());
			var param = new {userType};

			return GetResults(sql, param).ToList();
		}
	}
}
