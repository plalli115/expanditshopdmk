﻿using System.Collections.Generic;
using System.Linq;
using CmsPublic.DataRepository;
using EISCS.Shop.DO.Dto;
using EISCS.Shop.DO.Dto.Cart;
using EISCS.Shop.DO.Interface;

namespace EISCS.Shop.DO.Repository
{
    [TableProperties(TableName = "Favorites")]
    public class FavoritesRepository : BaseRepository<FavoriteItem>, IFavoritesRepository
    {
        public FavoritesRepository(IExpanditDbFactory dbFactory)
            : base(dbFactory)
        {
        }

        public IEnumerable<FavoriteItem> GetFavoritesForUser(string userGuid)
        {
            string query = string.Format("SELECT * FROM {0} WHERE UserGuid = @userGuid", GetTableName());

            return GetResults(query, new { userGuid });
        }

        public void DeleteFavorite(string userGuid, string productGuid)
        {
            string query = string.Format("DELETE FROM {0} WHERE UserGuid = @userGuid and ProductGuid=@productGuid", GetTableName());
            
            Execute(query, new { userGuid , productGuid});
        }

        public void AddFavorite(FavoriteItem item)
        {
            string query = string.Format("INSERT INTO {0} {1}", GetTableName(), GenerateInsertParameters(item));
            
            GetResults(query, item );
        }

        public IEnumerable<FavoriteItem> GetFavoritesForUser(string userGuid, List<string> productGuids)
        {
            string query = string.Format("SELECT * FROM {0} WHERE UserGuid = @userGuid and ProductGuid in @productGuids", GetTableName());

            return GetResults(query, new { userGuid, productGuids });
        } 
    }


}