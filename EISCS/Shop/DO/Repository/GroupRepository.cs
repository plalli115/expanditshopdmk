﻿using System;
using System.Collections.Generic;
using System.Linq;
using CmsPublic.DataProviders.Logging;
using CmsPublic.DataRepository;
using CmsPublic.ModelLayer.Interfaces;
using CmsPublic.ModelLayer.Models;
using EISCS.ExpandITFramework.Util;
using EISCS.Shop.DO.Dto;
using EISCS.Shop.DO.Interface;

namespace EISCS.Shop.DO.Repository
{
    [TableProperties(TableName = "GroupTable", PrimaryKeyName = "GroupGuid")]
    public class GroupRepository : BaseRepository<Group>, IGroupRepository
    {
        private IPropertiesDataService _propertiesDataService;
        public GroupRepository(IExpanditDbFactory dbFactory, IPropertiesDataService propertiesDataService) : base(dbFactory)
        {
            _propertiesDataService = propertiesDataService;
        }

        /// <summary>
        /// Get a specific Group with both Editor and PropVal Properties
        /// </summary>
        /// <param name="groupGuid"></param>
        /// <param name="languageGuid"></param>
        /// <returns>Group</returns>
        public Group GetGroupForEditor(int groupGuid, string languageGuid)
        {
            Group g = GetGroup(groupGuid, languageGuid, true);
            var groupList = new List<IPropertyOwner>();
            if (g != null)
            {
                groupList.Add(g);
                _propertiesDataService.LoadPropertiesForEdit(groupList, languageGuid);
            }
            else
            {
                throw new Exception("Could not load Group");
            }
            return g;
        }


        public List<Group> GetRootGroupChildren(string languageGuid, bool isLoadProperties)
        {
            string sql =
                @"  SELECT  GroupTable.*, tmpl.TemplateFileName, tmpl.TemplateController 
                    FROM    GroupTable 
                            LEFT JOIN (
                                        SELECT  TemplateTable.*, tmp.PropOwnerRefGuid, tmp.PropTransText 
                                        FROM    TemplateTable 
                                                RIGHT JOIN  (
                                                                SELECT  PropTransText, PropOwnerRefGuid 
                                                                FROM    PropVal pv 
                                                                        INNER JOIN PropTrans pt ON pv.PropValGuid = pt.PropValGuid
                                                                WHERE   pv.PropGuid = 'TEMPLATE' 
                                                                AND     PropOwnerTypeGuid = 'GRP' 
                                                                AND     (PropLangGuid = @languageGuid OR PropLangGuid IS NULL)
                                                            ) AS tmp  ON TemplateTable.TemplateGuid = tmp.PropTransText
                                    ) AS tmpl ON tmpl.PropOwnerRefGuid = GroupTable.GroupGuid
                    WHERE GroupTable.ParentGuid = 0
                    ORDER BY ParentGuid, SortIndex";

            return GetGroups(sql,new {languageGuid}, languageGuid, isLoadProperties);
        }

        object IGroupRepository.CatalogLevels()
        {
            return CatalogLevels();
        }


        private List<Group> GetGroups(string sql, Object param, string languageGuid, bool isLoadProperties)
        {
            List<Group> groupList = GetResults(sql, param).ToList();
            if (isLoadProperties && groupList.Count > 0)
            {

                _propertiesDataService.LoadProperties(groupList.Cast<IPropertyOwner>().ToList(), languageGuid);
 
            }
            return groupList;
        }

        /// <summary>
        /// Get a list of all Groups, with or without Properties
        /// </summary>
        /// <param name="languageGuid">string</param>
        /// <param name="isLoadProperties">bool, determines if Properties will be loaded</param>
        /// <returns>List of Groups</returns>
        public List<Group> GetAllGroups(string languageGuid, bool isLoadProperties)
        {
            return GetGroups("SELECT * FROM GroupTable ORDER BY ParentGuid, SortIndex", new{},  languageGuid, isLoadProperties);
        }

        public List<Group> GetSubGroups(string languageGuid, int groupGuid, bool isLoadProperties)
        {
            string sql =
                @" ;WITH temp_GroupTable (ParentGuid, GroupGuid, SortIndex, iteration) AS
                ( 
                    SELECT  ParentGuid, GroupTable.GroupGuid, SortIndex, 0 
                    FROM    GroupTable
                    WHERE   ParentGuid = @groupGuid 
                    UNION ALL 
                    SELECT  b.ParentGuid, b.GroupGuid, b.SortIndex, a.iteration + 1
                    FROM    temp_GroupTable AS a, GroupTable AS b 
                    WHERE   a.GroupGuid = b.ParentGuid 
                ) 
                SELECT  temp_GroupTable.*, tmpl.TemplateGuid, TemplateFileName, TemplateController 
                FROM    temp_GroupTable 
                        LEFT JOIN (
                                    SELECT  TemplateTable.*, tmp.PropOwnerRefGuid, tmp.PropTransText 
                                    FROM    TemplateTable 
                                            RIGHT JOIN (
                                                        SELECT  PropTransText, PropOwnerRefGuid 
                                                        FROM    PropVal pv 
                                                                INNER JOIN PropTrans pt ON pv.PropValGuid = pt.PropValGuid
                                                        WHERE   pv.PropGuid = 'TEMPLATE' 
                                                        AND     PropOwnerTypeGuid = 'GRP' 
                                                        AND     (PropLangGuid = @languageGuid OR PropLangGuid IS NULL)
                                                        ) AS tmp    ON TemplateTable.TemplateGuid = tmp.PropTransText
                                    ) AS tmpl   ON tmpl.PropOwnerRefGuid = temp_GroupTable.GroupGuid
                ORDER BY ParentGuid, SortIndex ";


            return GetGroups(sql, new { groupGuid , languageGuid}, languageGuid, isLoadProperties);
        }

        public List<Group> GetTopGroups(string languageGuid, bool isLoadProperties)
        {
            
            string sql =
                  @"SELECT  GroupGuid, SortIndex 
                    FROM    GroupTable 
                    WHERE   ParentGuid IN ( 
                                            SELECT  ParentGuid 
                                            FROM    GroupTable 
                                            EXCEPT 
                                            SELECT  GroupGuid 
                                            FROM    GroupTable
                                        ) 
                    ORDER BY SortIndex";

            return GetGroups(sql, new {}, languageGuid, isLoadProperties);
        }

        /// <summary>
        /// Get a specific Group, with or without Properties
        /// </summary>
        /// <param name="groupGuid">int</param>
        /// <param name="languageGuid">string</param>
        /// <param name="isLoadProperties">bool, determines if Properties will be loaded</param>
        /// <returns></returns>
        public Group GetGroup(int groupGuid, string languageGuid, bool isLoadProperties = true)
        {
            string sql =
                @"
                SELECT  GroupTable.*, tmpl.TemplateGuid, TemplateFileName, TemplateController 
                FROM    GroupTable 
                        LEFT JOIN (
                                    SELECT  TemplateTable.*, tmp.PropOwnerRefGuid, tmp.PropTransText 
                                    FROM    TemplateTable 
                                            RIGHT JOIN  (
                                                            SELECT  PropTransText, PropOwnerRefGuid 
                                                            FROM    PropVal pv 
                                                                    INNER JOIN PropTrans pt ON pv.PropValGuid = pt.PropValGuid
                                                            WHERE   pv.PropGuid = 'TEMPLATE' 
                                                            AND     PropOwnerTypeGuid = 'GRP' 
                                                            AND     (PropLangGuid = @languageGuid OR PropLangGuid IS NULL)
                                                        ) AS tmp  ON TemplateTable.TemplateGuid = tmp.PropTransText
                                ) AS tmpl   ON tmpl.PropOwnerRefGuid = GroupTable.GroupGuid
                WHERE   GroupTable.GroupGuid = @groupGuid
                ORDER BY ParentGuid, SortIndex
                ";
            List<Group> groupList = GetResults(sql, new { groupGuid, languageGuid }).ToList();
            if (isLoadProperties && groupList.Count > 0)
            {
                _propertiesDataService.LoadProperties(groupList.Cast<IPropertyOwner>().ToList(), languageGuid);
            }
            return groupList.Count > 0 ? groupList[0] : null;
        }


        public List<Group> GetGroupsFromGroupGuids(List<int> groupGuids, string languageGuid, bool isLoadProperties)
        {
            string inGroupGuidClause = Utilities.ToInClauseString(groupGuids, "GroupGuid");
            string sql = @" SELECT  * 
                            FROM    GroupTable 
                            WHERE   " + inGroupGuidClause;
            List<Group> groupList = GetResults(sql, new {}).ToList();
            
            if (isLoadProperties && groupList.Count > 0)
            {
                _propertiesDataService.LoadProperties(groupList.Cast<IPropertyOwner>().ToList(), languageGuid);
            }
            return groupList;
        }

        public int CatalogLevels()
        {
            
            string sql = @" WITH temp_GroupTable (ParentGuid, GroupGuid, iteration) AS  
                            ( 
                                SELECT  ParentGuid, GroupGuid, 0 
                                FROM    GroupTable 
                                WHERE   ParentGuid = 0 
                                UNION ALL 
                                SELECT a.ParentGuid, b.GroupGuid, a.iteration + 1 
                                FROM temp_GroupTable AS a, GroupTable AS b 
                                WHERE a.GroupGuid = b.ParentGuid 
                             ) 
                              SELECT    MAX(iteration) 
                              FROM      temp_GroupTable ";

            return GetSingle<int>(sql);
        }

        public List<string> GetPropertyGuidsIncludedInTemplate(string templateGuid)
        {
            string sql =
                        @"  SELECT  tp.PropGuid 
                            FROM    TemplateProperty tp 
                                    INNER JOIN TemplateTable tt ON tp.TemplateGuid = tt.TemplateGuid AND tt.TemplateGuid = @templateGuid";

            List<string> returnList = GetResults<string>(sql, new {templateGuid}).ToList();
            
            return returnList;
        }

        public List<TemplateProperty> GetTemplateProperties(string templateGuid)
        {
            string sql =
                    @"  SELECT  tp.TemplateGuid, PropGuid, PropGroupGuid, PropHeader 
                        FROM    TemplateProperty tp 
                                INNER JOIN TemplateTable tt ON tp.TemplateGuid = tt.TemplateGuid AND tt.TemplateGuid = @templateGuid";

            List<TemplateProperty> templateProperties = GetResults<TemplateProperty>(sql ,new {templateGuid} ).ToList();

            return templateProperties;
        }

        public void ConvertFromMultiLanguage(string propOwnerRefGuid, string propGuid, string languageGuid)
        {
            _propertiesDataService.ML2NULL(propOwnerRefGuid, propGuid, languageGuid);
        }

        public void ConvertToMultiLanguage(string propOwnerRefGuid, string propGuid, string languageGuid,
                                           List<LanguageTable> allLanguages)
        {
            _propertiesDataService.NULL2ML(propOwnerRefGuid, propGuid, languageGuid, allLanguages);
        }

        public List<TemplateQuerySet> TemplateTable(string filter)
        {
            string sql = @"SELECT * FROM TemplateTable WHERE " + filter + " <> 0";
            return GetResults<TemplateQuerySet>(sql).ToList();
        }

        public void SaveProperties(Group group, string languageGuid)
        {
            _propertiesDataService.SaveProperties(group.IProperties, languageGuid);
        }

        public Group GetGroupByPropGuidAndPropTransText(string propGuid, string propTransText, string languageGuid)
        {
            string sql = @"SELECT GroupGuid FROM GroupTable 
                                    WHERE GroupGuid IN (SELECT PropOwnerRefGuid FROM PropTrans pt JOIN propval pv ON pt.PropValGuid = pv.PropValGuid
                                    WHERE PropGuid = @propGuid AND PropTransText = @propTransText)";


            int groupGuid = GetResults<int>(sql, new { propGuid, propTransText }).SingleOrDefault();
            
            Group group = GetGroup(groupGuid, languageGuid, true);

            return group;
        }


        #region Dictionaries

        /// <summary>
        /// Get all Groups as a Dictionary of(int, Group), with or without Properties
        /// </summary>
        /// <param name="languageGuid"></param>
        /// <param name="isLoadProperties"></param>
        /// <returns></returns>
        public Dictionary<int, Group> GetAllGroupsDictionary(string languageGuid,
                                                   bool isLoadProperties)
        {
            string sql  = "SELECT * FROM GroupTable ORDER BY ParentGuid, SortIndex";
            var result = GetGroups(sql, new {languageGuid}, languageGuid, isLoadProperties);

            return result.ToDictionary(x => x.GroupGuid);
        }

        public Dictionary<int, Group> GetTopGroupsDictionary(string languageGuid, bool isLoadProperties)
        {
            var result = GetTopGroups(languageGuid, isLoadProperties);
            return result.ToDictionary(x => x.GroupGuid);
        }

        public Dictionary<int, Group> GetSubGroupsDictionary(string languageGuid, int groupGuid, bool isLoadProperties)
        {
            var result = GetSubGroups(languageGuid, groupGuid, isLoadProperties);
            return result.ToDictionary(x => x.GroupGuid);
        }

        public Dictionary<int, Group> GetSubGroupsWithExclude(string languageGuid, int groupGuid, bool isLoadProperties, string excludeParams)
        {


            string sql =
                @" ;WITH temp_GroupTable (ParentGuid, GroupGuid, SortIndex, iteration) AS
                    ( 
                        SELECT  ParentGuid, GroupTable.GroupGuid, SortIndex, 0
                        FROM    GroupTable                 
                        WHERE   ParentGuid = @groupGuid 
                        AND     GroupTable.GroupGuid NOT IN (" + excludeParams + @")
                        UNION ALL 
                        SELECT  b.ParentGuid, b.GroupGuid, b.SortIndex, a.iteration + 1
                        FROM    temp_GroupTable AS a, GroupTable AS b 
                        WHERE   a.GroupGuid = b.ParentGuid 
                    ) 
                    SELECT  temp_GroupTable.*, tmpl.TemplateGuid, TemplateFileName, TemplateController 
                    FROM    temp_GroupTable 
                    LEFT JOIN   (
                                    SELECT  TemplateTable.*, tmp.PropOwnerRefGuid, tmp.PropTransText 
                                    FROM    TemplateTable 
                                    RIGHT JOIN  (
                                                    SELECT  PropTransText, PropOwnerRefGuid 
                                                    FROM    PropVal pv 
                                                            INNER JOIN PropTrans pt ON pv.PropValGuid = pt.PropValGuid
                                                    WHERE   pv.PropGuid = 'TEMPLATE' 
                                                    AND     PropOwnerTypeGuid = 'GRP' 
                                                    AND (PropLangGuid = @languageGuid OR PropLangGuid IS NULL)
                                                ) AS tmp  ON TemplateTable.TemplateGuid = tmp.PropTransText
                                ) AS tmpl   ON tmpl.PropOwnerRefGuid = temp_GroupTable.GroupGuid
                    ORDER BY ParentGuid, SortIndex ";

            var result = GetGroups(sql, new {groupGuid, languageGuid}, languageGuid, isLoadProperties);

            return result.ToDictionary(x => x.GroupGuid);
        }

        public Dictionary<int, Group> GetSubGroupsWithInclude(string languageGuid, int groupGuid,
                                                              bool isLoadProperties, string includeParams)
        {
            string sql =
                @" ;WITH temp_GroupTable (ParentGuid, GroupGuid, SortIndex, iteration) AS
                    ( 
                        SELECT  ParentGuid, GroupTable.GroupGuid, SortIndex, 0
                        FROM    GroupTable                 
                        WHERE   ParentGuid = @groupGuid 
                        AND     GroupTable.GroupGuid IN (" + includeParams + @")
                        UNION ALL 
                        SELECT  b.ParentGuid, b.GroupGuid, b.SortIndex, a.iteration + 1
                        FROM    temp_GroupTable AS a, GroupTable AS b 
                        WHERE   a.GroupGuid = b.ParentGuid 
                    ) 
                    SELECT  temp_GroupTable.*, tmpl.TemplateGuid, TemplateFileName, TemplateController 
                    FROM    temp_GroupTable 
                            LEFT JOIN (
                                        SELECT  TemplateTable.*, tmp.PropOwnerRefGuid, tmp.PropTransText 
                                        FROM    TemplateTable 
                                                RIGHT JOIN  (
                                                                SELECT  PropTransText, PropOwnerRefGuid 
                                                                FROM    PropVal pv 
                                                                        INNER JOIN PropTrans pt ON pv.PropValGuid = pt.PropValGuid
                                                                WHERE   pv.PropGuid = 'TEMPLATE' 
                                                                AND     PropOwnerTypeGuid = 'GRP' 
                                                                AND     (PropLangGuid = @languageGuid OR PropLangGuid IS NULL)
                                                            ) AS tmp  ON TemplateTable.TemplateGuid = tmp.PropTransText
                                    ) AS tmpl   ON tmpl.PropOwnerRefGuid = temp_GroupTable.GroupGuid
                    ORDER BY ParentGuid, SortIndex ";

            var result = GetGroups(sql, new {groupGuid, languageGuid}, languageGuid, isLoadProperties);
            return result.ToDictionary(x=>x.GroupGuid);
        }


        #endregion

        #region Create, Update, Move, Delete Groups

        /// <summary>
        /// Move the sortIndex one step up starting at the sortIndex of groupGuid
        /// </summary>
        /// <param name="parentGuid"></param>
        /// <param name="groupGuid"></param>
        /// <returns></returns>
        private void PrivateUpdateSortOrder(int parentGuid, int groupGuid)
        {
            string sql = @" UPDATE GroupTable 
                            SET SortIndex = SortIndex + 1 
                            WHERE ParentGuid = @parentGuid
                            AND SortIndex >= ( 
                                                SELECT SortIndex 
                                                FROM GroupTable 
                                                WHERE GroupGuid = @groupGuid 
                                            )";
            Execute(sql, new { parentGuid , groupGuid});
        }

        public int GetHighestRootGroupGuid()
        {
            string sql= @"  SELECT  MAX(GroupGuid) 
                            FROM    GroupTable WHERE ParentGuid = 0";

            return GetSingle<int>(sql);
        }

        public int GetLowestRootGroupGuidBySortOrder()
        {
            int rootGroupGuid = -1;
            int lowestSortIndex = GetLowestSortIndex(0);
            
            string sql = @" SELECT  MAX(GroupGuid) 
                            FROM    GroupTable WHERE ParentGuid = 0 
                            AND     SortIndex = @lowestSortIndex";
            int objRootGroupGuid = GetSingle<int>(sql, new { lowestSortIndex });

            try
            {
                rootGroupGuid = (int)objRootGroupGuid;
            }
            catch
            {
                rootGroupGuid = GetHighestRootGroupGuid();
            }

            return rootGroupGuid;
        }

        public int EnsureParentGroup(string languageGuid)
        {
            int highestRootGroupGuid = GetLowestRootGroupGuidBySortOrder(); //GetHighestRootGroupGuid();
            if (highestRootGroupGuid == -1)
            {
                
                var group = new Group() { GroupGuid = 1, ParentGuid = 0, SortIndex = 1};
                Add(group, false); 
                
                try
                {
                    Group initGroup = GetGroup(1, languageGuid, true);
                    GrpPropValSet propVal = initGroup.Properties;
                    if (propVal.PropDict != null)
                    {
                        if (propVal.PropDict.ContainsKey("NAME"))
                        {
                            propVal.PropDict["NAME"] = "Catalog";
                        }
                        if (propVal.PropDict.ContainsKey("_NAME"))
                        {
                            propVal.PropDict["_NAME"] = "false";
                        }
                    }
                    _propertiesDataService.SaveProperties(initGroup.IProperties, languageGuid);
                }
                catch (Exception)
                {
                    FileLogger.Log("EXCEPTION");
                }
                //

                return 1;
            }
            return highestRootGroupGuid;
        }

        /// <summary>
        /// Return the current highest GroupGuid value in GroupTable
        /// </summary>
        /// <returns></returns>
        private int GetHighestGroupGuid()
        {

            string sql = @" SELECT  MAX(GroupGuid) 
                            FROM    GroupTable";

            return GetSingle<int>(sql);
        }

        private int GetSortIndex(int groupGuid)
        {
            string sql = @" SELECT  SortIndex 
                            FROM    GroupTable 
                            WHERE   GroupGuid = @groupGuid ";
            return GetSingle<int>(sql, new {groupGuid});
        }

        private int GetHighestSortIndex(int parentGuid)
        {
            string sql =
                        @"  SELECT  MAX(SortIndex) 
                            FROM    GroupTable 
                            WHERE   ParentGuid = @parentGuid ";
            return GetSingle<int>(sql , new {parentGuid});
        }

        private int GetLowestSortIndex(int parentGuid)
        {
            string sql =
                        @"  SELECT  MIN(SortIndex) 
                            FROM    GroupTable 
                            WHERE   ParentGuid = @parentGuid ";
            return GetSingle<int>(sql, new {parentGuid});
           
        }

        /// <summary>
        /// Give the Group a new ParentGuid and a new SortIndex
        /// </summary>
        /// <param name="groupGuid"></param>
        /// <param name="parentGuid"></param>
        /// <param name="siblingGroupGuid"></param>
        public void RearrangeGroupOrder(int groupGuid, int parentGuid, int siblingGroupGuid)
        {
            // get the current sortIndex. This will be sortIndex of the moved group
            int sortIndex = GetSortIndex(siblingGroupGuid);
            // move sortIndexes
            PrivateUpdateSortOrder(parentGuid, siblingGroupGuid);
            var group = new Group(){GroupGuid = groupGuid,ParentGuid = parentGuid, SortIndex = sortIndex};
            Update(group);
        }

        public void RegroupGroupTable(int groupGuid, int parentGuid)
        {
            int sortIndex = -1;
            try
            {
                sortIndex = GetHighestSortIndex(parentGuid);
            } catch (Exception ex)
            {
                //ignore - this is the first time this group has become a prent
            }
            var group = new Group() { GroupGuid = groupGuid, ParentGuid = parentGuid, SortIndex = sortIndex+1 };
            Update(group);
        }

        /// <summary>
        /// Create a new Group in the GroupTable. The new GroupGuid will be the current hihgest GroupGuid + 1
        /// </summary>
        /// <param name="parentGuid"></param>
        /// <param name="siblingGroupGuid">The group that will appear after the new group</param>
        /// <returns></returns>
        /// 

        public int CreateNewGroup(int parentGuid, int siblingGroupGuid)
        {
            // get the current sortIndex. This will be sortIndex of the new group
            int sortIndex = GetSortIndex(siblingGroupGuid);
            // move sortIndexes one step up starting at current sortIndex
            PrivateUpdateSortOrder(parentGuid, siblingGroupGuid);

            // get current highest groupGuid
            int currentHighGroupGuid = GetHighestGroupGuid();
            if (currentHighGroupGuid > 0)
            {
                var group = new Group() { GroupGuid = currentHighGroupGuid + 1 , ParentGuid = parentGuid, SortIndex = sortIndex};
                Add(group, false);
                return group.GroupGuid;
            }
            return -1;
        }

        public int InsertGroup(int groupGuid, int parentGuid, int siblingGroupGuid)
        {
            int sortIndex = GetSortIndex(siblingGroupGuid);
            if (sortIndex != -1)
            {
                // move sortIndexes one step up starting at current sortIndex
                PrivateUpdateSortOrder(parentGuid, siblingGroupGuid);
            }
            else
            {
                sortIndex = GetHighestSortIndex(parentGuid) + 1;
            }

            int actualGroupGuid = groupGuid;
            // check if GroupGuid exist
            var groupExists = GetById(groupGuid);
            if (groupExists!= null)
            {
                // get current highest groupGuid
                int currentHighGroupGuid = GetHighestGroupGuid();
                actualGroupGuid = currentHighGroupGuid + 1;
            }
            var group = new Group { GroupGuid = actualGroupGuid, ParentGuid = parentGuid, SortIndex = sortIndex };
            Add(group, false);
            return group.GroupGuid;
        }


        public int DeleteGroup(int groupGuid, string languageGuid)
        {
            
            bool isRootGroup = false;

            // First find out if the parentGuid = 0
            // If it is 0, this is a top level group. It's NOT good to delete the only top group.
            // 3. Find the ParentGuid of the current group to be deleted
            var group = GetById(groupGuid);
            
            //Should be placed on Group class
            if (group.ParentGuid == 0)
            {
                isRootGroup = true;
            }

            // 1. Load the subgroups of the current group and delete all properties of each group        
            List<Group> gList = GetSubGroups(languageGuid, groupGuid, true);
            foreach (Group g in gList)
            {
                _propertiesDataService.DeleteAllProperties(g.Properties);
            }

            // 1.1 Delete all the subgroups from the GroupProductTable
            //var gpp = new GroupProductProvider(DbExecutor);
            foreach (Group g in gList)
            {
              //  gpp.DeleteGroupProduct(g.GroupGuid);
                Execute(@"DELETE FROM GroupProduct WHERE GroupGuid = @groupGuid", new { g.GroupGuid });
            }

            // 1.2 Delete the Group from the GroupProductTable
            //gpp.DeleteGroupProduct(groupGuid);
            Execute(@"DELETE FROM GroupProduct WHERE GroupGuid = @groupGuid", new { groupGuid });

            // 2. Delete all properties belonging to the current group            
            Group grp = GetGroup(groupGuid, languageGuid, true);
            _propertiesDataService.DeleteAllProperties(grp.Properties);


            // 4. Delete all subgroups from the GroupTable
            if (gList.Count > 0)
            {
                string inGroupGuidClause = Utilities.ToInClauseString(gList.Select(x => x.GroupGuid).ToList(), "GroupGuid");
                string sqlSubDelete = "DELETE FROM GroupTable WHERE (" + inGroupGuidClause + ")";
                Execute(sqlSubDelete);
            }

            // 5. Delete the current group from the GroupTable
            string sqlDelete = "DELETE FROM GroupTable WHERE GroupGuid = @groupGuid";
            Execute(sqlDelete, new {groupGuid});

            return isRootGroup ? -100 : group.ParentGuid;
        }

        #endregion
    }
}