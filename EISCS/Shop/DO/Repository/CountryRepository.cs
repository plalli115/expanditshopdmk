﻿using CmsPublic.DataRepository;
using EISCS.Shop.DO.Dto.Country;
using EISCS.Shop.DO.Interface;

namespace EISCS.Shop.DO.Repository
{
    [TableProperties(TableName = "CountryTable", PrimaryKeyName = "CountryGuid")]
    public class CountryRepository : BaseRepository<CountryItem>, ICountryRepository
    {
        public CountryRepository(IExpanditDbFactory dbFactory): base(dbFactory){}
    }
}