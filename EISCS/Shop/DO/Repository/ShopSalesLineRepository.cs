﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using AutoMapper;
using CmsPublic.DataRepository;
using Dapper;
using EISCS.Shop.DO.Dto.Cart;
using EISCS.Shop.DO.Interface;

namespace EISCS.Shop.DO.Repository
{
    [TableProperties(TableName = "ShopSalesLine", PrimaryKeyName = "LineGuid")]
    public class ShopSalesLineRepository : BaseRepository<ShopSalesLine>, IShopSalesLineRepository
    {
        public ShopSalesLineRepository(IExpanditDbFactory dbFactory) : base(dbFactory)
        {
        }

        public List<ShopSalesLine> GetShopSalesLinesByHeaderGuid(string headerGuid)
        {
            string query = string.Format("SELECT * FROM {0} WHERE HeaderGuid = @headerGuid", GetTableName());

            return GetResults(query, new {headerGuid}).ToList();
        }

        public bool Delete(IEnumerable<string> inParameter)
        {
            string sqlQuery = string.Format("DELETE FROM {0} WHERE LineGuid IN @inParameter", GetTableName());
            var param = new {inParameter};
            return Execute(sqlQuery, param);
        }

	    public ShopSalesLine Add(ShopSalesLine item)
	    {
		    var sql = String.Format("INSERT INTO {0} (" +
				"CurrencyGuid, " +
				"HeaderGuid, " +
				"LineComment, " +
				"LineDiscount, " +
				"LineDiscountAmount, " +
				"LineGuid, " +
				"LineNumber, " +
				"LineTotal, " +
				"ListPrice, " +
				"ListPriceInclTax, " +
				"ProductGuid, " +
				"ProductName, " +
				"Quantity, " +
				"TotalInclTax, " +
				"VariantCode, " +
				"LineDiscountAmountInclTax, " +
				"VariantName, " +
				"TaxPct, " +
				"TaxAmount) " +
				"OUTPUT INSERTED.* VALUES (" +
				"@currencyGuid, " +
				"@headerGuid, " +
				"@lineComment, " +
				"@lineDiscount, " +
				"@lineDiscountAmount, " +
				"@lineGuid, " +
				"@lineNumber, " +
				"@lineTotal, " +
				"@listPrice, " +
				"@listPriceInclTax, " +
				"@productGuid, " +
				"@productName, " +
				"@quantity, " +
				"@totalInclTax, " +
				"@variantCode, " +
				"@lineDiscountAmountInclTax, " +
				"@variantName, " +
				"@taxPct, " +
				"@taxAmount)", GetTableName());

		    IDbConnection connection = GetConnection();

		    try
		    {
			    return connection.Query<ShopSalesLine>(sql, item).SingleOrDefault();
		    }
		    finally
		    {
			    DbFactory.Finalize(connection);
		    }
	    }
    }
}