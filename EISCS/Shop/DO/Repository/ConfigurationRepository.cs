﻿using System.Collections.Generic;
using System.Linq;
using CmsPublic.DataRepository;
using EISCS.Shop.DO.Interface;

namespace EISCS.Shop.DO.Repository
{
    [TableProperties(TableName = "Configuration", PrimaryKeyName = "Profile")]
    public class ConfigurationRepository : BaseRepository<string>, IConfigurationRepository
    {
        public ConfigurationRepository(IExpanditDbFactory dbFactory)
            : base(dbFactory)
        {
        }

        public string GetRemoteUrl()
        {
            return GetResults(string.Format("SELECT DISTINCT Value FROM {0} WHERE Configuration='Global.Remote Web Root - URL'", GetTableName())).SingleOrDefault();
        }

        public string GetRemoteUrl(string profile)
        {
            return string.IsNullOrWhiteSpace(profile) ? GetRemoteUrl() : GetResults(string.Format("SELECT Value FROM {0} WHERE Configuration='Global.Remote Web Root - URL' AND Profile = @profile", GetTableName()), new { profile }).SingleOrDefault();
        }

        public string ReadFirstRunValue()
        {

            var sql = string.Format(@"SELECT [Value] 
                                     FROM {0} 
                                     WHERE Configuration = 'Global.FirstRun'", GetTableName());

            var configList = GetResults(sql).ToList();

            return configList.Count > 0 ? configList[0] : "";
        }
    }
}
