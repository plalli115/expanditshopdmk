﻿using System.Collections.Generic;
using CmsPublic.DataRepository;
using EISCS.Shop.DO.Dto.Currency;
using EISCS.Shop.DO.Interface;

namespace EISCS.Shop.DO.Repository
{
    [TableProperties(TableName = "CurrencyTable", PrimaryKeyName = "CurrencyCode")]
    public class CurrencyRepository : BaseRepository<CurrencyItem>, ICurrencyRepository
    {
        public CurrencyRepository(IExpanditDbFactory dbFactory)
            : base(dbFactory)
        {
        }

        public override string AllColumns()
        {
            return "CurrencyCode as Id, CurrencyName as Name, CurrencyEnabled";
        }

	    public IEnumerable<CurrencyItem> GetValid()
	    {
		    string query = "SELECT CurrencyCode as Id, CurrencyName as Name FROM " + GetTableName() + " WHERE CurrencyEnabled <> 0";
		    return GetResults(query);
	    }
    }
}