﻿using System.Collections.Generic;
using System.Linq;
using CmsPublic.DataRepository;
using EISCS.Shop.DO.Dto;
using EISCS.Shop.DO.Interface;

namespace EISCS.Shop.DO.Repository {
	[TableProperties(TableName = "PaymentType_ShippingHandlingProvider")]
	class PaymentTypeShippingHandlingProviderRepository : BaseRepository<PaymentTypeShippingHandlingProviderTable>, IPaymentTypeShippingHandlingProviderRepository
	{

		public PaymentTypeShippingHandlingProviderRepository(IExpanditDbFactory dbFactory)
			: base(dbFactory)
		{
		}

		public List<PaymentTypeShippingHandlingProviderTable> GetPaymentTypeByProvider(string providerGuid)
		{
			var sql = string.Format("SELECT * FROM {0} WHERE ShippingProviderGuid = @providerGuid", GetTableName());
			var param = new {providerGuid};

			return GetResults(sql, param).ToList();
		}
	}
}
