﻿using System.Collections.Generic;
using CmsPublic.ModelLayer.Models;

namespace EISCS.Shop.DO.Interface
{
    public interface IMailMessageRepository : IBaseRepository<ExpanditMailMessage>
    {
        ExpanditMailMessage GetMessageByName(string messageName, string languageGuid, bool b);
        List<ExpanditMailMessage> GetAll(string language, bool b);
        ExpanditMailMessage GetMessageForEditor(int id, string language);
        List<Product> GetMailProducts(int id, string language);
        IEnumerable<string> GetMailProducts(int id);
        void RemoveMailProducts(int id, IEnumerable<string> productGuids);
        void SetMailProducts(string mailmessageGuid, IEnumerable<string> guids);
        ExpanditMailMessage GetMessage(int messageGuid, string languageGuid, bool b);
        ExpanditMailMessage CreateMailMessage(string languageGuid, string messageName);
    }
}
