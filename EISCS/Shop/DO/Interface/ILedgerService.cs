﻿using System.Collections.Generic;
using EISCS.Shop.BO.BusinessLogic;
using EISCS.Shop.DO.Dto.Ledger;

namespace EISCS.Shop.DO.Interface
{
    public interface ILedgerService
    {
        LedgerEntryContainer GetLedgerEntriesByUser();
    }
}