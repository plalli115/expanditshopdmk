using System.Collections.Generic;
using EISCS.Shop.DO.Dto;

namespace EISCS.Shop.DO.Interface
{
    public interface IUserStorageService
    {
        IEnumerable<UserTable> GetAllB2BUsers(); // TODO: Should be removed
        string FindCustomerGuidByUser(string userGuid);
        UserTable GetUserByCustomerAndUserGuid(string customerGuid, string userGuid);
        UserTable GetUserByUserGuid(string userGuid);
        string GetCountryName(string countryGuid);
        IEnumerable<AccessRoles> GetUserAccessByRole(string accessClass, string userRole);
        string GetCustomerGroup(string customerGuid);
        IEnumerable<UserTable> GetAllUsers();
        int GetAllUsersCount();
        bool NewLogin(string userGuid);
    }
}