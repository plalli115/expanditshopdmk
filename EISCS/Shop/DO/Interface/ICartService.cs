using System.Collections.Generic;
using EISCS.ExpandITFramework.Infrastructure;
using EISCS.Shop.DO.Dto.Cart;
using EISCS.Shop.DO.Service;

namespace EISCS.Shop.DO.Interface
{
    public interface ICartService
    {
        CartHeader Add(string userGuid, string productGuid, string productName, double quantity, string comment, string variantCode, bool updateQuantity, bool isGift = false);
        CartHeader Add(string userGuid, string productGuid, string productName, double quantity, string comment, string variantCode, bool updateQuantity, PageMessages messages, bool isGift = false);
        CartHeader GetCartById(string userGuid);
        PageMessages Calculate(CartHeader cartHeader);
        PageMessages Calculate(CartHeader cartHeader, string currency, string defaultCurrency);
        List<ProductInfoNode> GetProducts(CartHeader cartHeader);
        Result MergeUserCarts(string loggedInUserGuid, string offlineUserGuid);
        CartHeader Update(string userGuid, List<CartUpdateInfo> cartInfo);
        CartHeader Delete(string userGuid, string productGuid, string variantCode, bool isGift = false);
        MiniCartItem UpdateMiniCart(CartHeader header);
        string ShippingHandlingName(CartHeader header);
	    bool SaveCartHeader(CartHeader header);
	    CartHeader GetShippingHandlingPrice(string userGuid);
	    CartHeader GetShippingHandlingPrice(string userGuid, string shippingProviderGuid);
        void ClearCartLines(CartHeader cartHeader);
        List<CartHeader> GetStaleUserCarts(int maxAge);
        List<CartHeader> ClearStaleUserCarts(int maxAge);
        string GetCurrencyIso4217(string currencyGuid);
        CartHeader PromoGiftDiscounts(CartHeader cartHeader);
    }
}