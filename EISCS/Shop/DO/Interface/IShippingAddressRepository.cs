﻿using System.Collections.Generic;
using EISCS.Shop.DO.Dto;

namespace EISCS.Shop.DO.Interface
{
    public interface IShippingAddressRepository : IBaseRepository<ShippingAddress>
    {
        ShippingAddress GetAddress(string addressGuid);
	    ShippingAddress GetB2BAddress(string addressGuid, string customerGuid);
	    List<ShippingAddress> GetB2BAddresses(string customerGuid);
        List<ShippingAddress> GetAddresses(string userGuid);
	    new bool Update(ShippingAddress shippingAddress);
	    void NewDefaultShippingAddressForUser(string shippingAddressGuid, string userGuid);
    }
}