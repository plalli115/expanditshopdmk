using System.Collections.Generic;
using EISCS.Shop.BO.BAS;
using EISCS.Shop.DO.Dto;
using EISCS.Shop.DO.Service;

namespace EISCS.Shop.DO.Interface
{
    public interface IExpanditUserService
    {
        string UserGuid { get; set; }
        string CustomerGuid { get; set; }
        string LanguageGuid { get; set; }
        string CurrencyGuid { get; set; }
        string UserEmail { get; set; }
        MiniCartItem MiniCart { get; set; }
        string GetDefaultCurrency { get; }
        string GetCountryName(string countryGuid);
        UserClass UserType { get; }
        bool UserAccess(string accessClass);
        UserTable GetUser();
        UserTable GetUser(string userGuid);
        // TEST
        string UniqueIdentity { get; }
        // Portal releated
        UserTable GetPortalUser();
        UserTable GetPortalUser(string id);
        IEnumerable<UserTable> GetAllB2BUsers();
        IEnumerable<UserTable> GetAllUsers();
        int GetAllUsersCount();
        bool NewLogin(string userGuid);
    }
}