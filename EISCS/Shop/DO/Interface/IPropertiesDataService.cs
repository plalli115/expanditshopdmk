﻿using System.Collections.Generic;
using CmsPublic.ModelLayer.Interfaces;
using CmsPublic.ModelLayer.Models;
using EISCS.Shop.DO.Dto;

namespace EISCS.Shop.DO.Interface
{
    public interface IPropertiesDataService
    {
        /// <summary>
        /// </summary>
        /// <param name="propValSet"></param>
        /// <param name="languageGuid"></param>
        void SaveProperties(IPropValSet propValSet, string languageGuid);
        void ML2NULL(string propOwnerRefGuid, string propGuid, string languageGuid);

        void LoadProperties(ICollection<IPropertyOwner> propOwner, string languageGuid);
        Dictionary<string, List<PropValPropTransQuerySet>> LoadProperties(string propOwner, string languageGuid);
        Dictionary<string, List<PropValPropTransQuerySet>> LoadProperties2(string propOwner, string languageGuid, string productGuids);
        Dictionary<string, List<PropValPropTransQuerySet>> LoadSubGroupProductProperties(int groupGuid, string propOwner, string languageGuid);

        void LoadPropertiesForEdit(ICollection<IPropertyOwner> propOwner, string languageGuid);
        ///<summary>
        ///</summary>
        ///<param name="propertyOwner"></param>
        ///<param name="propertyOwnerType"></param>
        ///<param name="propName"></param>
        ///<param name="languageGuid"></param>
        ///<returns></returns>
        string GetSingleProperty(string propertyOwner, string propertyOwnerType, string propName, string languageGuid);
        List<PropValPropTransQuerySet> LoadGpPropQuerySet(IEnumerable<IPropertyOwner> propOwnerIdentifier, string languageGuid, string groupGuid);
        IEnumerable<PropertyNode> SetPropertyNodeList(IPropValSet propValSet);
        void LoadGpProperties(ICollection<GroupProduct> propOwner, string languageGuid, string groupGuid);

        string GetExternalLanguageGuid(string languageGuid);


        void NULL2ML(string propOwnerRefGuid, string propGuid, string languageGuid, List<LanguageTable> allLanguages);
        void DeleteAllProperties(IPropValSet properties);
        TemplateQuerySet GetTemplateInfo(string id);
        List<TemplateQuerySet> GetAllTemplateInfo();
        string GetTemplateGuid(string templateName);
        void InitTemplateTables();
        bool UpdateTemplates(List<TemplateQuerySet> oldData, List<TemplateQuerySet> newData);
        void InsertTemplates(List<TemplateQuerySet> propData);
    }
}