﻿using EISCS.Shop.DO.Dto;

namespace EISCS.Shop.DO.Interface
{
    public interface ILoginService
    {
        UserTable Impersonate(string userGuid);
        ExpanditLoginStatus SecondFactorLogin(string userName, string password, string pin);
        ExpanditLoginStatus Login(string userName, string password);
        ExpanditLoginStatus Login(string userName, string password, out string loggedInUserGuid);
        void Logout();

        bool ValidatePasswordAndEncryptionType(string userGuid, string password, string chosenEncryptionType, string passwordInTable, string encryptionTypeInTable,
            string encryptionSaltInTable, string passwordVersion = null);
    }

    public enum ExpanditLoginStatus
    {
        LoggedIn, AuthorizedNeedPin, Unauthorized, Authorized, NeedApproval
    }
}