﻿using System;
using System.Collections.Generic;
using EISCS.Shop.DO.Dto.Cart;

namespace EISCS.Shop.DO.Interface {
	public interface IShopSalesService
	{
		ShopSalesHeader CartHeader2ShopSalesHeader(CartHeader cartHeader, string userGuid);
	    ShopSalesHeader GetShopSalesHeader(string customerReference);
		ShopSalesHeader GetLatestShopSalesHeaderByUser(string userGuid);
        void MailUpdate(ShopSalesHeader shopSalesHeader);
	    List<ShopSalesHeader> GetLastShopSales();
        List<ShopSalesHeader> GetLatestShopSalesInXDays(int days);
	    string GetRevenueBetweenDates(DateTime initDate, DateTime endDate);
        List<ShopSalesHeader> GetShopSalesRange(string userGuid, int startIndex, int endIndex, string searchValue, string sort = null, string direction = null);
	    List<ShopSalesHeader> GetShopSalesHeaderByUser(string userGuid);
	}
}
