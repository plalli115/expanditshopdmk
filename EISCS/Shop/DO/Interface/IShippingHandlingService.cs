﻿using System.Collections.Generic;
using EISCS.Shop.DO.Dto;

namespace EISCS.Shop.DO.Interface
{
	public interface IShippingHandlingService
	{
		List<ShippingHandlingProvider> GetShippingHandlingProviders();
		ShippingHandlingProvider GetShippingHandlingProvider(string shippingProviderGuid);
	}
}