﻿using EISCS.Shop.DO.Dto;
using EISCS.Shop.DO.Dto.Cart;

namespace EISCS.Shop.DO.Interface
{
    public interface ICartHeaderRepository : IBaseRepository<CartHeader>
    {
        CartHeader GetCartHeaderByUser(string userGuid);
	    CartHeader UpdateShippingAddress(ShippingAddress shippingAddress);
	    CartHeader MapBillingAddress(UserTable user);
    }
}
