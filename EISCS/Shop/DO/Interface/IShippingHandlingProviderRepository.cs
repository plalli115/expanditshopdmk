﻿using System.Collections.Generic;
using EISCS.Shop.DO.Dto;

namespace EISCS.Shop.DO.Interface
{
	public interface IShippingHandlingProviderRepository
	{
		List<ShippingHandlingProvider> GetShippingHandlingProviders();
		ShippingHandlingProvider GetShippingHandlingProvider(string shippingHandlingProviderGuid);
	}
}