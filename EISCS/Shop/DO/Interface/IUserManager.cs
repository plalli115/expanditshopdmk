﻿using System.Collections.Generic;
using EISCS.Shop.DO.Dto;
using EISCS.Shop.DO.PostData;

namespace EISCS.Shop.DO.Interface
{
    public interface IUserManager
    {
        // Manage Users
        UserTable GetUser(string userGuid);
        bool AddUser(UserTable user);
        bool DeleteUser(UserTable user);
        bool UpdateUser(UserTable user, bool noPasswordCheck);
        bool UpdateUser(UserTable postData);
        bool ManagerUpdateUser(UserTable postData);
        bool UpdatePassword(UserPasswordPostData postData);
        string UpdateLostPassword(string email, string userName, int length, out string contactName);

        // Manage Roles
        bool AddRole(string roleId, string roleDescription, bool readOnly);
        bool AddRoleAccess(string roleId, string accessClass);
        bool RemoveRoleAccess(string roleId, string accessClass);
        bool DeleteRole(string roleId, string substituteRoleId);
        int RoleCount();
        int RoleCount(string searchParameter);
        IEnumerable<RoleTable> Roles(int start, int end, string column, string direction, string searchParameter);
        RoleTable GetRole(string roleId);

        // Manage User Roles
        bool SetUserRole(string userGuid, string roleId);
        IEnumerable<RoleTable> GetAllRoles();
        IEnumerable<AccessTable> GetAllAccesses();
        IEnumerable<AccessRoles> GetAllAccessRoles();
        bool UpdateAccess(Dictionary<string, List<string>> keyValues);
        bool UpdateUserRole(string userGuid, string roleId);

        // List Users
        int UserCount();
        int UserCount(string searchParameter);
        IEnumerable<UserTable> Users(int start, int end, string column, string direction, string searchParameter);

        // List Customers
        int CustomerCount();
        int CustomerCount(string searchParameter);
        IEnumerable<CustomerTableBase> Customers(int start, int end, string column, string direction, string searchParameter);
        string CustomerCompanyName(string customerGuid);

        // Set Customer
        bool UpdateUserCustomer(string userGuid, string customerGuid);

        // Get shop customer detail
        CustomerTableBase GetCustomer(string id);
        bool UpdateRole(RoleTable role);
    }
}
