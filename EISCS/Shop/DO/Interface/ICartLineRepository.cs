﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EISCS.Shop.DO.Dto.Cart;

namespace EISCS.Shop.DO.Interface
{
    public interface ICartLineRepository : IBaseRepository<CartLine>
    {
        List<CartLine> GetCartLinesById(string headerGuid);
        bool Delete(IEnumerable<string> inParameter);
    }
}
