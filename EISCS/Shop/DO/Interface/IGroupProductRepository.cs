﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CmsPublic.ModelLayer.Models;
using EISCS.Shop.DO.Dto;

namespace EISCS.Shop.DO.Interface
{
    public interface IGroupProductRepository
    {
        void AddProductsToGroup(string[] productGuids, string groupGuid);
        void DeleteProductsFromGroup(string[] groupProductGuids, int groupGuid);
        void ChangeGroupProductSortOrder(string currentItem, string nextItem, string prevItem, string groupGuid);
        List<GroupProduct> GetGroupProducts(string productGuid);
        List<GroupProduct> GetGroupProducts(List<string> groupProductGuids);
    }
}
