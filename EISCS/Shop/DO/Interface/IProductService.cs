using System.Collections.Generic;
using EISCS.Shop.DO.Dto.Cart;
using EISCS.Shop.DO.Dto.Product;
using EISCS.Shop.DO.Service;

namespace EISCS.Shop.DO.Interface
{
    public class ProductServiceConstants
    {
        public static string PRICE_SORT = "Price";
    }

    public interface IProductService
    {
     
        ProductInfoNodeContainer GetProduct(string productGuid, string variantCode);
        ProductInfoNodeContainer GetProducts(CartHeader info, ICollection<string> guids, string sort = "", string direction = "", int startIndex = -1, int endIndex = -1, bool oneProductPerVariant = false);
        ProductInfoNodeContainer GetProducts(int groupGuid, int startIndex, int endIndex, string sort = null, string direction = null);
        ProductInfoNodeContainer GetMostPopularProducts(int pageSize);
        int GetGroupProductsCount(int groupGuid);
        ProductInfoNodeContainer GetRelatedProducts(string productGuid);
    }
}