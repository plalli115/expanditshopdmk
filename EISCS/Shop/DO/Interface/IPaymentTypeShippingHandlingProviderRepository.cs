﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EISCS.Shop.DO.Dto;
using EISCS.Shop.DO.Repository;

namespace EISCS.Shop.DO.Interface {
	public interface IPaymentTypeShippingHandlingProviderRepository : IBaseRepository<PaymentTypeShippingHandlingProviderTable>
	{
		List<PaymentTypeShippingHandlingProviderTable> GetPaymentTypeByProvider(string providerGuid);
	}
}
