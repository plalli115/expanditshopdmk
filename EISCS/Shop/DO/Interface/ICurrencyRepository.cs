﻿using System.Collections.Generic;
using EISCS.Shop.DO.Dto;
using EISCS.Shop.DO.Dto.Currency;

namespace EISCS.Shop.DO.Interface
{
    public interface ICurrencyRepository : IBaseRepository<CurrencyItem>
    {
	    IEnumerable<CurrencyItem> GetValid();
    }
}
