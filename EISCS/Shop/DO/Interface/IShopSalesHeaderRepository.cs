﻿using System;
using System.Collections.Generic;
using EISCS.Shop.DO.Dto.Cart;

namespace EISCS.Shop.DO.Interface
{
    public interface IShopSalesHeaderRepository : IBaseRepository<ShopSalesHeader>
    {
        List<ShopSalesHeader> GetShopSalesHeadersByUser(string userGuid);
        List<ShopSalesHeader> GetShopSalesHeadersByCustRef(string custRef);
        List<ShopSalesHeader> GetShopSalesHeadersByCustRef(string userGuid, string custRef);
        IEnumerable<ShopSalesHeader> GetShopSalesHeadersByPaymentTransactionId(string paymentTransactionId);
		ShopSalesHeader Add(ShopSalesHeader item);
        void MailUpdate(ShopSalesHeader shopSalesHeader);
        List<ShopSalesHeader> GetLastShopSales();
        List<ShopSalesHeader> GetLatestShopSalesInXDays(int days);
        List<ShopSalesHeader> GetSalesHeadersBetweenDates(DateTime initDate, DateTime endDate);
        List<ShopSalesHeader> GetShopSalesRange(string userGuid, int startIndex, int endIndex, string searchValue, string sort = null, string direction = null);
    }
}