﻿using System.Collections.Generic;
using EISCS.Shop.DO.Dto.Cart;

namespace EISCS.Shop.DO.Interface
{
    public interface IShopSalesLineRepository : IBaseRepository<ShopSalesLine>
    {
        List<ShopSalesLine> GetShopSalesLinesByHeaderGuid(string headerGuid);
        bool Delete(IEnumerable<string> inParameter);
	    ShopSalesLine Add(ShopSalesLine line);
    }
}