using System;
using System.Collections.Generic;
using EISCS.Shop.BO;
using EISCS.Shop.DO.Dto;

namespace EISCS.Shop.DO.Interface{
    public interface IUserItemRepository : IBaseRepository<UserTable>
    {
        bool UpdateLanguage(UserTable user, string lang);
        bool UpdateCurrency(UserTable user, string currencyGuid);
        bool UpdatePassword(string userGuid, string password, string newEncryptionType, string newEncryptionSalt);
        bool UpdatePasswordWithVersion(string userGuid, string newPassword, string newEncryptionType, string newEncryptionSalt, string newPasswordVersion);
        UserTable GetUserByUserName(string userName);
        UserTable GetUserByEmailAndUserLogin(string email, string userLogin);
        // TODO: Check if this declaration of GetBillingAddress is really used
        ShippingAddress GetBillingAddress(string userGuid);
        int GetB2CUsersCount();
        int GetB2CUsersCount(string searchValue);
        List<UserTable> GetB2CUsersList(int startIndex, int endIndex, string columnSort, string direction, string searchValue);
        int CountTotalUsers();
        bool UpdateRequestPasswordKeyAndTimestamp(UserTable user);
        UserTable GetUserByRequestResetPasswordToken(string requestToken);
    }
}