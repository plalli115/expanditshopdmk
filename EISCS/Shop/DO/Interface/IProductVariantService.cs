﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EISCS.Shop.DO.Dto;
using EISCS.Shop.DO.Dto.Product;

namespace EISCS.Shop.DO.Interface
{
    public interface IProductVariantService{
        ProductVariantContainer GetVariants(IEnumerable<string> productGuids, string languageGuid);
    }
}
