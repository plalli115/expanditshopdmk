﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EISCS.ExpandITFramework.Infrastructure;
using EISCS.Shop.DO.Dto.Cart;

namespace EISCS.Shop.DO.Interface
{
    public interface IOrderService
    {
        List<ShopSalesHeader> GetOrdersByUser(string userGuid);
        ShopSalesHeader GetOrder(string headerGuid);
        List<ShopSalesHeader> SearchByCustRef(string userGuid, string custRef);
        void Reorder(string headerGuid, PageMessages messages);
    }
}
