﻿using System.Collections.Generic;
using EISCS.Shop.DO.Dto;

namespace EISCS.Shop.DO.Interface {
	public interface IPaymentService
	{
		List<string> GetProvidersByUser(string userType);
		List<PaymentTypeTable> GetAllPaymentTypes();
		List<PaymentTypeTable> GetPaymentTypeByProvider(string providerGuid, string userType);
		string GetPaymentMethodName(string paymentMethod);
	}
}
