﻿using System.Collections.Generic;
using CmsPublic.ModelLayer.Models;

namespace EISCS.Shop.DO.Interface
{
    public interface IProductRepository : IBaseRepository<Product>
    {
        /// <summary>
        /// Get number of products by filter type
        /// </summary>
        /// <param name="type"></param>
        /// <param name="filter"></param>
        /// <returns></returns>
        int GetProductsCount(string type, string filter);

        int GetGroupProductsCount(int groupGuid);

        /// <summary>
        /// Get all products within a defined range
        /// </summary>
        /// <param name="startIndex"></param>
        /// <param name="endIndex"></param>
        /// <param name="isLoadProperties"></param>
        /// <param name="filter"></param>
        /// <param name="languageGuid"></param>
        /// <param name="columnSort"></param>
        /// <param name="sortDir"></param>
        /// <returns></returns>
        List<Product> GetProductRange(int startIndex, int endIndex, bool isLoadProperties, string filter, string languageGuid, string columnSort, string sortDir);
        List<Product> GetProductsRange(ICollection<string> guids, string languageGuid, string filter, string sort, string direction, int startIndex, int endIndex);

        List<Product> GetGroupProductsRange(int startIndex, int endIndex, bool isLoadProperties, string languageGuid,
                                            string filter, int groupGuid, string sort = null, string direction = null);

        /// <summary>
        /// Get all Products from the specified Group
        /// </summary>
        /// <param name="groupGuid"></param>
        /// <param name="languageGuid"></param>
        /// <param name="isLoadProperties"></param>
        /// <returns></returns>
        List<GroupProduct> GroupProducts(string groupGuid, string languageGuid, bool isLoadProperties);

        /// <summary>
        /// Load properties for a single product
        /// </summary>
        /// <param name="guid"></param>
        /// <param name="languageGuid"></param>
        /// <param name="isLoadProperties"></param>
        /// <returns></returns>
        Product SingleProduct(string guid, string languageGuid, bool isLoadProperties = true);

        ///<summary>
        ///</summary>
        ///<param name="guid"></param>
        ///<param name="languageGuid"></param>
        ///<returns></returns>
        Product SingleProductAnonymousCollection(string guid, string languageGuid);

        ///<summary>
        ///</summary>
        ///<param name="languageGuid"></param>
        ///<param name="isLoadProperties"></param>
        ///<returns></returns>
        List<Product> GetAllProducts(string languageGuid, bool isLoadProperties);

        ///<summary>
        ///</summary>
        ///<param name="languageGuid"></param>
        ///<param name="isLoadProperties"></param>
        ///<returns></returns>
        List<GroupProduct> GetAllGroupProducts(string languageGuid, bool isLoadProperties);

        ///<summary>
        ///</summary>
        ///<param name="groupGuid"></param>
        ///<param name="languageGuid"></param>
        ///<param name="isLoadProperties"></param>
        ///<returns></returns>
        List<GroupProduct> GetAllGroupProducts(int groupGuid, string languageGuid, bool isLoadProperties);

        ///<summary>
        ///</summary>
        ///<param name="guids"></param>
        ///<param name="languageGuid"></param>
        ///<returns></returns>
        List<Product> GetProducts(ICollection<string> guids, string languageGuid);

        int GetTotalRecords();
        int GetTotalRecordsFiltered(string filter);
        List<Product> FindProducts(string searchQuery);

    }
}
