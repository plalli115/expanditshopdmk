﻿namespace EISCS.Shop.DO.Interface {
	public interface IConfigurationRepository : IBaseRepository<string>
	{
		string GetRemoteUrl();
	    string GetRemoteUrl(string profile);
	    string ReadFirstRunValue();
	}
}
