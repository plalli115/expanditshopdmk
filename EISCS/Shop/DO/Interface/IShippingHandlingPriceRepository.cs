﻿using System.Collections.Generic;
using EISCS.Shop.DO.Dto;

namespace EISCS.Shop.DO.Interface
{
	public interface IShippingHandlingPriceRepository
	{
		List<ShippingHandlingPrice> GetShippingHandlingPrice(string providerGuid);
	}
}