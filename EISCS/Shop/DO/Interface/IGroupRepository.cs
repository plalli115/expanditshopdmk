﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CmsPublic.ModelLayer.Models;
using EISCS.Shop.DO.Dto;

namespace EISCS.Shop.DO.Interface
{
    public interface IGroupRepository : IBaseRepository<Group>
    {


        /// <summary>
        /// Get a list of Group Objects based on a collection of GroupGuid's
        /// </summary>
        /// <param name="groupGuids"></param>
        /// <param name="languageGuid"></param>
        /// <param name="isLoadProperties"></param>
        /// <returns></returns>
        List<Group> GetGroupsFromGroupGuids(List<int> groupGuids, string languageGuid, bool isLoadProperties);

        /// <summary>
        /// Get a list of all Group Objects 
        /// </summary>
        /// <param name="languageGuid"></param>
        /// <param name="isLoadProperties"></param>
        /// <returns></returns>
        List<Group> GetAllGroups(string languageGuid, bool isLoadProperties);
        Dictionary<int, Group> GetAllGroupsDictionary(string languageGuid,bool isLoadProperties);

        /// <summary>
        /// Get a list of Group Objects descending from a parent Group Object
        /// </summary>
        /// <param name="languageGuid"></param>
        /// <param name="groupGuid"></param>
        /// <param name="isLoadProperties"></param>
        /// <returns></returns>
        List<Group> GetSubGroups(string languageGuid, int groupGuid, bool isLoadProperties);

        /// <summary>
        /// Get a dictionary of Group Guid's and Group Objects descending from a parent Group Object
        /// </summary>
        /// <param name="languageGuid"></param>
        /// <param name="groupGuid"></param>
        /// <param name="isLoadProperties"></param>
        /// <returns></returns>
        Dictionary<int, Group> GetSubGroupsDictionary(string languageGuid, int groupGuid, bool isLoadProperties);

        /// <summary>
        /// Get a list of Group Objects at the top level
        /// </summary>
        /// <param name="languageGuid"></param>
        /// <param name="isLoadProperties"></param>
        /// <returns></returns>
        List<Group> GetTopGroups(string languageGuid, bool isLoadProperties);

        /// <summary>
        /// Get a dictionary of Group Guid's and Group Objects at the top level
        /// </summary>
        /// <param name="languageGuid"></param>
        /// <param name="isLoadProperties"></param>
        /// <returns></returns>
        Dictionary<int, Group> GetTopGroupsDictionary(string languageGuid, bool isLoadProperties);

        ///<summary>
        ///</summary>
        ///<param name="groupId"></param>
        ///<param name="languageGuid"></param>
        ///<returns></returns>
        Group GetGroupForEditor(int groupId, string languageGuid);


        /// <summary>
        /// 
        /// </summary>
        /// <param name="guid"></param>
        /// <param name="languageGuid"></param>
        /// <param name="isLoadProperties"></param>
        /// <returns></returns>
        Group GetGroup(int guid, string languageGuid, bool isLoadProperties = true);


 
        int InsertGroup(int groupGuid, int parentGuid, int siblingGroupGuid);

        ///<summary>
        ///</summary>
        ///<param name="groupGuid"></param>
        ///<param name="languageGuid"></param>
        ///<returns></returns>
        int DeleteGroup(int groupGuid, string languageGuid);



        ///<summary>
        ///</summary>
        ///<returns></returns>
        int GetHighestRootGroupGuid();

        ///<summary>
        ///</summary>
        ///<param name="languageGuid"></param>
        ///<returns></returns>
        int EnsureParentGroup(string languageGuid);

        ///<summary>
        ///</summary>
        ///<param name="languageGuid"></param>
        ///<param name="isLoadProperties"></param>
        ///<returns></returns>
        List<Group> GetRootGroupChildren(string languageGuid, bool isLoadProperties);

        ///<summary>
        ///</summary>
        ///<param name="languageGuid"></param>
        ///<param name="groupGuid"></param>
        ///<param name="isLoadProperties"></param>
        ///<param name="excludeParams"></param>
        ///<returns></returns>
        Dictionary<int, Group> GetSubGroupsWithExclude(string languageGuid, int groupGuid,
                                                       bool isLoadProperties, string excludeParams);

        ///<summary>
        ///</summary>
        ///<param name="languageGuid"></param>
        ///<param name="groupGuid"></param>
        ///<param name="isLoadProperties"></param>
        ///<param name="includeParams"></param>
        ///<returns></returns>
        Dictionary<int, Group> GetSubGroupsWithInclude(string languageGuid, int groupGuid,
                                                       bool isLoadProperties, string includeParams);

        ///<summary>
        ///</summary>
        ///<returns></returns>
        object CatalogLevels();


        ///<summary>
        ///</summary>
        ///<param name="templateName"></param>
        ///<returns></returns>
        List<string> GetPropertyGuidsIncludedInTemplate(string templateName);

        ///<summary>
        ///</summary>
        ///<param name="templateFileName"></param>
        ///<returns></returns>
        List<TemplateProperty> GetTemplateProperties(string templateFileName);


        void ConvertFromMultiLanguage(string propOwnerRefGuid, string propGuid, string languageGuid);
        void ConvertToMultiLanguage(string propOwnerRefGuid, string propGuid, string languageGuid, List<LanguageTable> allLanguages);
        void SaveProperties(Group @group, string languageGuid);
        void RegroupGroupTable(int groupGuid, int parentGuid);
        void RearrangeGroupOrder(int groupGuid, int parentGuid, int siblingGuid);
        int CreateNewGroup(int parentGuid, int siblingGuid);
        List<TemplateQuerySet> TemplateTable(string filter);
        Group GetGroupByPropGuidAndPropTransText(string propGuid, string propTransText, string languageGuid);
    }
}
