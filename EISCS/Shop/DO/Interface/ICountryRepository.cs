﻿using EISCS.Shop.DO.Dto.Country;

namespace EISCS.Shop.DO.Interface
{
    public interface ICountryRepository : IBaseRepository<CountryItem>
    {

    }
}
