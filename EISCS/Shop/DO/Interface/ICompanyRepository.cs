﻿using EISCS.Shop.DO.Dto;

namespace EISCS.Shop.DO.Interface
{
    public interface ICompanyRepository : IBaseRepository<Company>
    {
        Company GetCompanyInfo();
    }
}
