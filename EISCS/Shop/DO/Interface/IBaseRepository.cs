﻿using System.Collections.Generic;

namespace EISCS.Shop.DO.Interface
{
    public interface IBaseRepository<T> where T : class 
    {
        string GetTableName();
        string AllColumns();
        string GetIdColumnName();
        string GetDefaultSql();
        IEnumerable<T> All();
        T GetById(int id);
        T GetById(string id);
        T Add(T item, bool useOutputClause = true);
        bool Update(T item);
        bool Remove(int id);
        bool Remove(string id);
    }
}
