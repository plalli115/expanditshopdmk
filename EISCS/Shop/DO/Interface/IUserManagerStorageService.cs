﻿using System.Collections.Generic;
using EISCS.Shop.DO.Dto;

namespace EISCS.Shop.DO.Interface
{
    public interface IUserManagerStorageService
    {
        // Manage Roles
        bool AddRole(string roleId, string roleDescription, bool readOnly);
        bool AddRoleAccess(string roleId, string accessClass);
        bool RemoveRoleAccess(string roleId, string accessClass);
        bool DeleteRole(string roleId);
        int RoleCount();
        int RoleCount(string searchParameter);
        IEnumerable<RoleTable> Roles(int start, int end, string column, string direction, string searchParameter);
        RoleTable GetRole(string roleId);
        // Manage User Roles
        bool SetUserRole(string userGuid, string roleId);
        // Manage Users
        bool AddUser(UserTable user);
        bool DeleteUser(UserTable user);
        IEnumerable<AccessTable> GetAllAccesses();
        IEnumerable<RoleTable> GetAllRoles();
        IEnumerable<AccessRoles> GetAllAccessRoles();
        // Set Access
        bool UpdateAccess(Dictionary<string, List<string>> keyValues);
        bool UpdateUserRole(string userGuid, string roleId);
        bool UpdateUserRoles(string roleId, string substituteRoleId);
        // List Customers
        int GetCustomersCount();
        int GetCustomersCount(string searchValue);
        string GetCustomerCompanyName(string customerGuid);
        IEnumerable<CustomerTableBase> Customers(int start, int end, string column, string direction, string searchParameter);
        int UsersCount(string searchParameter);
        IEnumerable<UserTable> Users(int start, int end, string column, string direction, string searchParameter);
        bool UpdateUserCustomer(string userGuid, string customerGuid);
        // Get shop customer detail
        CustomerTableBase GetCustomer(string id);
        bool UpdateRole(RoleTable role);
    }
}
