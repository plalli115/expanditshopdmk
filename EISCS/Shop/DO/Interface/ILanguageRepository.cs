﻿using System.Collections.Generic;
using EISCS.Shop.DO.Dto;

namespace EISCS.Shop.DO.Interface
{
    public interface ILanguageRepository : IBaseRepository<LanguageTable>
    {
        List<LanguageTable> LoadLanguages();
    }
}