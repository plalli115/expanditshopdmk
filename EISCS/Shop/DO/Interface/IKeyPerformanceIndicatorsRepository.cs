﻿using System;
using System.Collections.Generic;
using EISCS.Shop.DO.Dto;
using EISCS.Shop.DO.Dto.Cart;

namespace EISCS.Shop.DO.Interface
{
    public interface IKeyPerformanceIndicatorsRepository : IBaseRepository<KPI>
    {
        List<KPI> GetLastMonthReport();
        bool NewLogin();
        bool NewSale(ShopSalesHeader salesHeader);
        KPI GetLastDayReport();
        List<KPI> GetReportBetweenDates(DateTime initDate, DateTime endDate);
        bool NewUser();
        KPI GetTotals(List<KPI> report, DateTime initDate, DateTime endDate);
    }
}