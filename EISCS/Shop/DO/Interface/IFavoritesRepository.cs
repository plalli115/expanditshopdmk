﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EISCS.Shop.DO.Dto;

namespace EISCS.Shop.DO.Interface
{
    public interface IFavoritesRepository
    {
        IEnumerable<FavoriteItem> GetFavoritesForUser(string userGuid, List<string> guids);
        IEnumerable<FavoriteItem> GetFavoritesForUser(string userGuid);
        
        void DeleteFavorite(string userGuid, string productGuid);
        void AddFavorite(FavoriteItem item);
    }
}
