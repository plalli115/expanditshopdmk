﻿using System;

namespace EISCS.Shop.DO.BAS.Dto
{
    public class ReportHeader
    {
        // ESMReport
        public string ReportGuid { get; set; }
        public string ReportTypeGuid { get; set; }
        public string UserGuid { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime CreatedTime { get; set; }
        public DateTime CompletedDate { get; set; }
        public DateTime CompletedTime { get; set; }
        public string JobPlanningGuid { get; set; }
        public string CustomerGuid { get; set; }
        public string ServiceOrderGuid { get; set; }
        public string ServiceItemNo { get; set; }
        public string ServiceHeaderGuid { get; set; }
        public string ItemNo { get; set; }
        public string SerialNo { get; set; }
        public string ProjectGuid { get; set; }
        public string ReportData { get; set; }
        public byte[] PdfFileData { get; set; }
        public DateTime PdfCreatedDate { get; set; }
        public int ReadOnly { get; set; }
        public string Client_Guid { get; set; }
        public DateTime BASReceivedTime { get; set; }
        public int BASClient { get; set; }
        public int BASSyncCommand { get; set; }
        public int BASSyncState { get; set; }
        public int BASGuid { get; set; }
        public int BASVersion { get; set; }
        public string RecordAction { get; set; }

        // ESMReportType
        public string ReportName { get; set; }
        public string Description { get; set; }
        public string TemplateFile { get; set; }
        public string IconFile { get; set; }
        public string IconFileCompleted { get; set; }
        public int SortIndex { get; set; }
    }
}