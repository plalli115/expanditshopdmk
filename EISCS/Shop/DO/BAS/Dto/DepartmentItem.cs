﻿namespace EISCS.Shop.DO.BAS.Dto
{
    public class DepartmentItem
    {
        public string DepartmentGuid { get; set; }
        public string Description { get; set; }
    }
}