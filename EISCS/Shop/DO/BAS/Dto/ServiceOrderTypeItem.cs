﻿namespace EISCS.Shop.DO.BAS.Dto
{
    public class ServiceOrderTypeItem
    {
        public string Code { get; set; }
        public string Description { get; set; }
    }
}
