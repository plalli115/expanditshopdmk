﻿using System;

namespace EISCS.Shop.DO.BAS.Dto
{
    public class ProjectItem
    {
        public string ProjectGuid { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime StartingDate { get; set; }
        public DateTime EndingDate { get; set; }
        public int ProjectStatusGuid { get; set; }
        public string ProjectManagerGuid { get; set; }
        public string DepartmentGuid { get; set; }
        public string ProjectDimensionGuid { get; set; }
        public string JobPostingGroup { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string ProjectName { get; set; }
        public string CustomerGuid { get; set; }
    }
}