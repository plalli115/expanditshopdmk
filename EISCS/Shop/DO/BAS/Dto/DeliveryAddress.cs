﻿namespace EISCS.Shop.DO.BAS.Dto
{
    public class CustomerAddressItem
    {
        public bool IsMainAddress { get; set; }
        public string CustomerGuid { get; set; }
        public string EmailAddress { get; set; }
        public string AddressGuid { get; set; }
        public string CompanyName { get; set; }
        public string CountryGuid { get; set; }
        public string Address1 { get; set; }
        public string ServiceZoneCode { get; set; }
        public string Address2 { get; set; }
        public string CityName { get; set; }
        public string ContactPerson { get; set; }
        public string LocationGuid { get; set; }
        public string PhoneNo { get; set; }
        public string ZipCode { get; set; }
        public string CountyGuid { get; set; }
    }
}