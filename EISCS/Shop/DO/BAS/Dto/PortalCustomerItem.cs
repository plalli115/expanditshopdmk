﻿namespace EISCS.Shop.DO.BAS.Dto
{
    public class PortalCustomerItem
    {
        public string CustomerGuid { get; set; }
        public string EmailAddress { get; set; }
        public string CompanyName { get; set; }
        public string CurrencyGuid { get; set; }
        public string Address { get; set; }
        public string Address2 { get; set; }
        public string CityName { get; set; }
        public string ContactName { get; set; }
        public string PhoneNo { get; set; }
        public string ZipCode { get; set; }
        public string CountyName { get; set; }
    }
}
