﻿namespace EISCS.Shop.DO.BAS.Dto
{
    public class ServiceOrderReport : ReportHeader
    {
        public ServiceOrder ServiceOrder;
        public ServiceItem ServiceItem;
    }
}
