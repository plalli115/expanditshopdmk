﻿using System;

namespace EISCS.Shop.DO.BAS.Dto
{
    public class ServiceOrder
    {
        // Standard BAS
        public string ServiceOrderGuid { get; set; }
        public string ServiceHeaderGuid { get; set; }
        public string ProjectGuid { get; set; }
        public int Priority { get; set; }
        public string ServiceOrderType { get; set; }
        public float ResponseTimeHours { get; set; }
        public DateTime? ResponseDate { get; set; }
        public DateTime? ResponseTime { get; set; }
        public DateTime? StartingDate { get; set; }
        public DateTime? StartingTime { get; set; }
        public DateTime? FinishingDate { get; set; }
        public DateTime? FinishingTime { get; set; }
        public int LineNumber { get; set; }
        public bool Warranty { get; set; }
        public float WarrantyPartsPct { get; set; }
        public float WarrantyLaborPct { get; set; }
        public string ContractNo { get; set; }
        public string LocationofServiceItem { get; set; }
        public string ServiceItemNo { get; set; }
        public string FaultReasonCode { get; set; }
        public string FaultAreaCode { get; set; }
        public string SymptomCode { get; set; }
        public string FaultCode { get; set; }
        public string ResolutionCode { get; set; }
        public string ServiceItemGroupCode { get; set; }
        public int DocumentType { get; set; }
        public string ItemNo { get; set; }
        public int ItemType { get; set; }
        public string SerialNo { get; set; }
        public int NoofPreviousServices { get; set; }
        public string JobDescription { get; set; }
        public string Description { get; set; }
        public string RepairStatusCode { get; set; }
        public string CustomerGuid { get; set; }
        public string AddressGuid { get; set; }
        public int AddressType { get; set; }
        public string CompanyName { get; set; }
        public string Address { get; set; }
        public string Address2 { get; set; }
        public string ZipCode { get; set; }
        public string EmailAddress { get; set; }
        public string CityName { get; set; }
        public string YourReference { get; set; }
        public string UserGuid { get; set; }
        public int ContactCustomer { get; set; }
        public string ContactRegarding { get; set; }
        public bool ReadOnly { get; set; }
        public string client_guid { get; set; }
        public DateTime? BASReceivedTime { get; set; }
        public DateTime? BASInjected { get; set; }
        public int BASClient { get; set; }
        public int BASSyncCommand { get; set; }
        public int BASSyncState { get; set; }
        public int BASGuid { get; set; }
        public string BillToCustomerGuid { get; set; }
        public string BilltoCompanyName { get; set; }
        public string BilltoAddress { get; set; }
        public string BilltoAddress2 { get; set; }
        public string BilltoZipCode { get; set; }
        public string BilltoCityName { get; set; }
        public string BilltoContactName { get; set; }
        public string BilltoCountyName { get; set; }
        public string BilltoCountryGuid { get; set; }
        public string CountryGuid { get; set; }
        public string CurrencyGuid { get; set; }
        public string RecordAction { get; set; }
        public string JobPlanningGuid { get; set; }
        public string CountyName { get; set; }
        public string BilltoCompanyName2 { get; set; }
        public string ContactName { get; set; }
        public string CompanyName2 { get; set; }
        public string FaxNo { get; set; }
        public string PhoneNo { get; set; }
        public string PhoneNo2 { get; set; }
        public string ShiptoContactName { get; set; }
        public string ShiptoCountryGuid { get; set; }
        public string ShiptoCountyName { get; set; }
        public string ShiptoAddress { get; set; }
        public string ShiptoAddress2 { get; set; }
        public string ShiptoCityName { get; set; }
        public string ShiptoEmailAddress { get; set; }
        public string ShiptoCompanyName { get; set; }
        public string ShiptoCompanyName2 { get; set; }
        public string ShiptoFaxNo { get; set; }
        public string ShiptoPhoneNo { get; set; }
        public string ShiptoPhoneNo2 { get; set; }
        public string ShiptoZipCode { get; set; }
        public int BASVersion { get; set; }
        public string DepartmentGuid { get; set; }
        public DateTime? ApprovedDate { get; set; }
        public bool Approved { get; set; }
        public string ApprovedBy { get; set; }  
        public string OrderedByContactName { get; set; }
        public bool IsUrgent { get; set; }
        public bool ShiptoDoCallBeforeVisit { get; set; }
        public bool IsLockedInTime { get; set; }
        public bool IsLockedToUser { get; set; }
        public string CompleteAction { get; set; }
        public string ResponsibleUserGuid { get; set; }
        public string SalesPersonGuid { get; set; }
        public bool SKSReportIsRequired { get; set; }
        
        // CUSTOM PROPERTIES
        public string StatusCode { get; set; }
        public string StatusLabel { get; set; }
    }
}