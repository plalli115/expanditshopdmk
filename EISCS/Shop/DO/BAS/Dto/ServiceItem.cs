﻿using System;

namespace EISCS.Shop.DO.BAS.Dto
{
    public class ServiceItem
    {
        public string ServiceItemNo { get; set; }
        public string ItemNo { get; set; }
        public string LocationofServiceItem { get; set; }
        public DateTime WarrentyStartingDateLabor{ get; set; }
        public DateTime WarrentyEndingDateLabor { get; set; }
        public DateTime WarrentyStartingdateParts { get; set; }
        public DateTime WarrentyEndingDateParts { get; set; }
        public float WarrentyPartsPct { get; set; }
        public string SerialNo { get; set; }
        public float WarrentyLaborPct { get; set; }
        public float ResponseTimeHours { get; set; }
        public string ServiceItemGroupCode { get; set; }
        public string Description { get; set; }
        public int Priority { get; set; }
        public string CustomerGuid { get; set; }
        public string AddressGuid { get; set; }
    }
}   