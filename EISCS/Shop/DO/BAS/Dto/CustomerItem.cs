﻿namespace EISCS.Shop.DO.BAS.Dto
{
    public class CustomerItem
    {
        public string CustomerGuid { get; set; }
        public string EmailAddress { get; set; }
        public string CompanyName { get; set; }
        public string BillToCustomerGuid { get; set; }
        public string BillToCompanyName2 { get; set; }
        public string ShipToCompanyName { get; set; }
        public string ShipToCompanyName2 { get; set; }
        public string ContactName { get; set; }
        public string Address { get; set; }
        public string Address2 { get; set; }
        public string ZipCode { get; set; }
        public string CityName { get; set; }
        public string CountyName { get; set; }
        public string CountryGuid { get; set; }
        public bool EnableLogin { get; set; }
    }
}