﻿namespace EISCS.Shop.DO.BAS.Dto
{
    public class BASUser
    {
        private string m_FullName = "";

        public string UserGuid { get; set; }
        public string DepartmentGuid { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string FullName { get { return (m_FullName != "" ? m_FullName : FirstName + " " + MiddleName + " " + LastName); } set { m_FullName = value; } }

        public string EMailAddress { get; set; }
        public string PhoneNo { get; set; }
        public string MobilePhoneNo { get; set; }
        
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string ZipCode { get; set; }
        public string CityName { get; set; }
        public string CountyName { get; set; }
        public string CountryGuid { get; set; }

        public string UserTitle { get; set; }
        public string ResourceGuid { get; set; }
        public string SalesPersonGuid { get; set; }

    }
}
