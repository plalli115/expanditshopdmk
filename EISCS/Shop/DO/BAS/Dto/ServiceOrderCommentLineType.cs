﻿namespace EISCS.Shop.DO.BAS.Dto
{
    /*******************************************************
     *                                                     *
     * THIS CAN CHANGE DEPENDING ON THE CUSTOMER SETTINGS  *
     *                                                     *
     *******************************************************/
    public enum ServiceOrderCommentLineType
    {
        MessageToFromOffice = 0,
        MessageToInvoice = 10,
        PurchaseFromVendorComment = 11,
        MessageFromPortal = 12
    }
}