﻿namespace EISCS.Shop.DO.BAS.Dto
{
    public class ServiceOrderPortalStatusItem
    {
        public string ServiceOrderId { get; set; }
        public string PortalStatusId { get; set; }
        public string StatusCode { get; set; }
        public string StatusLabel { get; set; }
    }

    public class ServiceOrderPlanningtatusItem
    {
        public string ServiceOrderId { get; set; }
        public int JobPlanningType { get; set; }
        public string StatusCode { get; set; }
    }

}