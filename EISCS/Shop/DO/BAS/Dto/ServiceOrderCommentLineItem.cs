﻿using System;

namespace EISCS.Shop.DO.BAS.Dto
{
    public class ServiceOrderCommentLineItem
    {
        public ServiceOrderCommentLineItem()
        {
            CommentType = ServiceOrderCommentLineType.MessageFromPortal;
            CommentDate = DateTime.Now;
        }

        public ServiceOrderCommentLineItem(string commentString)
        {
            CommentType = ServiceOrderCommentLineType.MessageFromPortal;
            CommentDate = DateTime.Now;
        }

        public string ServiceOrderGuid { get; set; }
        public string CommentTypeName { get; set; }
        public string ServiceOrderNumber { get; set; }
        public int ServiceItemLineNumber { get; set; }
        public int CommentLineNumber { get; set; }
        public string CommentLineGuid { get; set; }
        public string CommentText { get; set; }
        public DateTime? CommentDate { get; set; }
        public string CommentTypeText { get; set; }
        public string RecordAction { get; set; }
        public string JobPlanningGuid { get; set; }
        public DateTime? BASReceivedTime { get; set; }
        public DateTime? BASInjected { get; set; }
        public int BASClient { get; set; }
        public int BASSyncCommand { get; set; }
        public int BASSyncState { get; set; }
        public int BASGuid { get; set; }
        public bool ReadOnly { get; set; }
        public string client_guid { get; set; }
        public string BASGuidLocal { get; set; }
        public int BASVersion { get; set; }
        public bool Approved { get; set; }
        public string ApprovedBy { get; set; }
        public DateTime? ApprovedDate { get; set; }
        public string VendorGuid { get; set; }

        public ServiceOrderCommentLineType CommentType { get; set; }
        public string PortalContactName { get; set; }
        
        public string UserType { get; set; }
        public string UserGuid { get; set; }

    }
}