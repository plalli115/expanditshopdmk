﻿namespace EISCS.Shop.DO.BAS.Dto
{
    public class CustomerPropertyItem
    {
        public int Id { get; set; }
        public string CustomerId { get; set; }
        public string PropertyId { get; set; }
        public TableType RelationTableType { get; set; }

        public enum TableType
        {
            Invalid = 0,
            Tasks = 1,
            ServiceManager = 2,
            Technician = 3
        }
    }
}