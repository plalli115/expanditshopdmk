﻿using System;

namespace EISCS.Shop.DO.BAS.Dto
{
    public class ServiceAttachmentItem
    {
        public string AttachmentGuid { get; set; }
        public string ServiceOrderGuid { get; set; }
        public string ServiceHeaderGuid { get; set; }
        public int LineNumber { get; set; }
        public DateTime SavedDate { get; set; }
        public DateTime SavedTime { get; set; }
        public string FileName { get; set; }
        public string FileExtension { get; set; }
        public string ContentType { get; set; }
        public long FileSize { get; set; }
        public byte[] FileData { get; set; }
        public string Description { get; set; }
        public string AttachmentRelation { get; set; }
        public string RecordAction { get; set; }
        public string JobPlanningGuid { get; set; }
        public int Readonly { get; set; }
        public string client_guid { get; set; }
        public DateTime BASReceivedTime { get; set; }
        public DateTime BASInjected { get; set; }
        public int BASClient { get; set; }
        public int BASSyncCommand { get; set; }
        public int BASSyncState { get; set; }
        public int BASGuid { get; set; }
        public bool Approved { get; set; }
        public DateTime ApprovedDate { get; set; }
        public string ApprovedBy { get; set; }
        public int ExpConversionStatus { get; set; }
        public string BackendAreaId { get; set; }
    }
}