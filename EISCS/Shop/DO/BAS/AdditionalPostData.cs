﻿namespace EISCS.Shop.DO.BAS.Postdata
{
    public class AdditionalPostData
    {
        public string DepartmentGuid { get; set; }
        public string ResponsibleUserGuid { get; set; }
    }
}