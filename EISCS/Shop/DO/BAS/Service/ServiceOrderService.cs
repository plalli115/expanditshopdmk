﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using AutoMapper;
using CmsPublic.DataRepository;
using EISCS.ExpandITFramework.Util;
using EISCS.Shop.BO.BAS;
using EISCS.Shop.DO.BAS.Dto;
using EISCS.Shop.DO.BAS.Interface;
using EISCS.Shop.DO.BAS.PostData;
using EISCS.Shop.DO.Dto;
using EISCS.Shop.DO.Interface;
using Microsoft.VisualBasic.ApplicationServices;

namespace EISCS.Shop.DO.BAS.Service
{
    public class ServiceOrderService : BaseDataService, IServiceOrderService
    {
        private readonly bool _serviceItemsEnabled;
        private readonly IServiceOrderCommentLineRepository _commentLineRepository;
        private readonly ICustomerService _customerService;
        private readonly IServiceAttachmentService _serviceAttachmentService;
        private readonly IServiceOrderPortalStatusRepository _serviceOrderPortalStatusRepository;
        private readonly IServiceOrderRepository _serviceOrderRepository;
        private readonly IUniqueGuidService _uniqueGuidService;
        private readonly IExpanditUserService _userService;

        public ServiceOrderService(IPortalDatabaseConnectionFactory connectionFactory, IServiceOrderRepository serviceOrderRepository,
                                   IServiceOrderCommentLineRepository commentLineRepository, IUniqueGuidService uniqueGuidService, ICustomerService customerService,
                                   IServiceOrderPortalStatusRepository serviceOrderPortalStatusRepository, IServiceAttachmentService serviceAttachmentService, IExpanditUserService userService) : base(connectionFactory)
        {
            _serviceOrderRepository = serviceOrderRepository;
            _commentLineRepository = commentLineRepository;
            _uniqueGuidService = uniqueGuidService;
            _customerService = customerService;
            _serviceOrderPortalStatusRepository = serviceOrderPortalStatusRepository;
            _serviceAttachmentService = serviceAttachmentService;
            _userService = userService;
            _serviceItemsEnabled = GetConfigurationAsBool("ENABLE_SERVICE_ITEM");
        }

        // TODO: Finish it!
        public IEnumerable<ServiceOrder> GetAllItemsByUserRole(int start, int end, string columnBeingSort, string sortDir, string searchValue, UserTable user, ServiceOrderSearchType st = ServiceOrderSearchType.Active)
        {
            var serviceOrders = Enumerable.Empty<ServiceOrder>();

            if (user != null)
            {
                switch (user.RoleId)
                {
                    case "Admin":
                        serviceOrders = _serviceOrderRepository.GetAllServiceOrders(start, end, columnBeingSort, sortDir, searchValue, st);
                        break;
                    case "CustomerGroup":
                        var customerGuidList = _customerService.GetCustomersForCustomerGroup(user.CustomerGroupId).Select(cg => cg.CustomerGuid);
                        serviceOrders = _serviceOrderRepository.GetAllByCustomerGuidList(customerGuidList, st);
                        break;
                    default:
                        serviceOrders = _serviceOrderRepository.GetAllByCustomerGuid(user.CustomerGuid, start, end, columnBeingSort, sortDir, searchValue, st);
                        break;
                }
            }

            return serviceOrders;
        }

        public int CountAllItemsByUserRole(string searchValue, UserTable user, ServiceOrderSearchType st)
        {
            var count = 0;
            if (user != null)
            {
                switch (user.RoleId)
                {
                    case "Admin":
                        count = CountAllServiceOrders(searchValue, st);
                        break;
                    case "CustomerGroup":
                        var customerGuidList = _customerService.GetCustomersForCustomerGroup(user.CustomerGroupId);
                        count = CountAllByCustomerGuidList(searchValue, customerGuidList, st);
                        break;
                    default:
                        count = CountAllByCustomerGuid(searchValue, user.CustomerGuid, st);
                        break;
                }
            }

            return count;
        }

        public bool CanEditServiceOrder(string serviceOrderGuid)
        {
            var sql = String.Format(@"SELECT COUNT(*) FROM ESMServiceOrderReg WHERE ServiceOrderGuid = @serviceOrderGuid");
            var param = new {serviceOrderGuid};
            return GetResults<int>(sql, param).SingleOrDefault() != 0;
        }

        public IEnumerable<ServiceOrder> GetAllArchivedItemsByUserRole(UserTable user)
        {
            IEnumerable<ServiceOrder> serviceOrders = Enumerable.Empty<ServiceOrder>();

            switch (user.RoleId)
            {
                case "Admin":
                    serviceOrders = _serviceOrderRepository.GetAllArchivedServiceOrders();
                    break;
                case "CustomerGroup":
                    var customerGuidList = _customerService.GetCustomersForCustomerGroup(user.CustomerGroupId).Select(cg => cg.CustomerGuid);
                    serviceOrders = _serviceOrderRepository.GetAllArchivedByCustomerGuidList(customerGuidList);
                    break;
                default:
                    serviceOrders = _serviceOrderRepository.GetAllArchivedByCustomerGuid(user.CustomerGuid);
                    break;
            }

            return serviceOrders;
        }

        public PortalStatusItem GetServiceOrderPortalStatus(string statusGuid)
        {
            // If not found, default to appointed
            return _serviceOrderPortalStatusRepository.GetById(statusGuid) ?? _serviceOrderPortalStatusRepository.GetById("APPOINTED");
        }

        public ServiceOrder GetById(string id)
        {
            var soItem = _serviceOrderRepository.GetById(id);
            if (soItem != null)
                soItem.StatusCode = _serviceOrderRepository.GetServiceOrderStatus(soItem.ServiceOrderGuid);
            return soItem;
        }

        public void Remove(string id)
        {
            string serviceOrderGuid = GetById(id).ServiceOrderGuid;

            _serviceAttachmentService.DeleteAllByServiceOrderGuid(serviceOrderGuid);
            _commentLineRepository.DeleteAllByServiceOrderGuid(serviceOrderGuid);
            _serviceOrderPortalStatusRepository.Remove(serviceOrderGuid);

            _serviceOrderRepository.Remove(id);
        }

        public string CreateServiceOrder(UserTable user, ServiceOrderPostData serviceOrderPostData, ServiceOrderJobPostData jobPostData, ServiceOrderAdditionalPostData serviceOrderAdditionalPostData = null, ServiceItemPostData serviceItemPostData = null)
        {
            string uniqueGuid;
            
            try
            {
                uniqueGuid = _uniqueGuidService.GenerateUniqueGuid();
            }
            catch (Exception)
            {
                var random = new Random(Guid.NewGuid().GetHashCode());
                uniqueGuid = random.Next().ToString();
            }
            

            ServiceOrder serviceOrder = PopulateServiceOrderIdsAndDates(serviceOrderPostData, jobPostData, uniqueGuid);

            if (user.UserAccess("EditServiceOrder") && user.UserAccess("EditServiceOrderDepartment"))
            {
                PopulateAdministratorRoleFieldData(serviceOrder, serviceOrderAdditionalPostData);
            }

            if (_serviceItemsEnabled && user.UserAccess("CreateServiceItem"))
            {
                PopulateServiceItemData(serviceOrder, serviceItemPostData);
            }
            
            ServiceOrder created = _serviceOrderRepository.Add(serviceOrder);

            ServiceOrderCommentLineItem commentItem = BuildServiceOrderCommentLine(serviceOrderPostData, uniqueGuid);
            _commentLineRepository.Add(commentItem);

            return created.ServiceOrderGuid;
        }
        
        public bool UpdateServiceOrder(ServiceOrderEditData serviceOrderEditData)
        {
            var serviceOrderItem = Mapper.Map<ServiceOrder>(serviceOrderEditData);
            PopulateServiceItemData(serviceOrderItem, Mapper.Map<ServiceItemPostData>(serviceOrderEditData));
            // Map Repair status code from the JobPlanningStatus table:
            serviceOrderItem.RepairStatusCode = GetServiceOrderPortalStatus(serviceOrderItem.RepairStatusCode).ExternalStatusGuid;
            return _serviceOrderRepository.Update(serviceOrderItem);
        }

        public IEnumerable<ServiceOrderTypeItem> GetAllServiceOrderTypes()
        {
            return _customerService.GetAllServiceOrderTypes().Select(t => new ServiceOrderTypeItem {Code = t.Key, Description = t.Value});
        }

        private ServiceOrder PopulateServiceOrderIdsAndDates(ServiceOrderPostData serviceOrderPostData, ServiceOrderJobPostData jobPostData, string newServiceOrderGuid)
        {
            var serviceOrder = new ServiceOrder
                {
                    ServiceOrderGuid = newServiceOrderGuid,
                    ServiceHeaderGuid = newServiceOrderGuid,
                    CustomerGuid = serviceOrderPostData.CustomerGuid,
                    StartingDate = serviceOrderPostData.SelectedStartDate,
                    FinishingDate = serviceOrderPostData.SelectedFinishDate,
                    RepairStatusCode = GetServiceOrderPortalStatus(serviceOrderPostData.Status).ExternalStatusGuid,
                    ProjectGuid = serviceOrderPostData.ProjectGuid,
                    BilltoContactName = serviceOrderPostData.BillToContactName,
                    ShiptoContactName = serviceOrderPostData.ShiptoContactName,
                    ShiptoCompanyName = serviceOrderPostData.ShipToCompanyName,
                    ShiptoCompanyName2 = serviceOrderPostData.ShipToCompanyName2,
                    ShiptoAddress = serviceOrderPostData.ShipToAddress,
                    ShiptoZipCode = serviceOrderPostData.ShipToZipCode,
                    ShiptoCityName = serviceOrderPostData.ShipToCityName,
                    ShiptoPhoneNo = serviceOrderPostData.ShipToPhoneNo,
                    ShiptoPhoneNo2 = serviceOrderPostData.ShipToPhoneNo2,
                    ShiptoEmailAddress = serviceOrderPostData.ShipToEmailAddress,
                    JobDescription = serviceOrderPostData.ShortDescription,
                    Description = serviceOrderPostData.ServiceItemDescription,
                    ItemType = 2,
                    ServiceOrderType = jobPostData.JobTypeId,
                    StatusCode = jobPostData.JobStatusId,
                    DepartmentGuid = jobPostData.DepartmentId,
                    ResponsibleUserGuid = jobPostData.ServiceManagerId,
                    RecordAction = "NEW",
                    BASReceivedTime = DateTime.Now,
                    ResponseDate = serviceOrderPostData.SelectedStartDate,
                    ResponseTime = serviceOrderPostData.SelectedStartDate,
                    DocumentType = 1,
                    BASVersion = 1,
                    LineNumber = 10000
                };

            // Fill in customer data
            CustomerItem cust = _customerService.GetCustomer(serviceOrder.CustomerGuid);
            serviceOrder.CompanyName     = cust.CompanyName;
            //ServiceOrder.CompanyName2    = cust.CompanyName2;
            serviceOrder.ContactName     = cust.ContactName;
            serviceOrder.Address         = cust.Address;
            serviceOrder.Address2        = cust.Address2;   
            serviceOrder.ZipCode         = cust.ZipCode;   
            serviceOrder.CityName        = cust.CityName;    
            serviceOrder.CountryGuid     = cust.CountryGuid;
            serviceOrder.CountyName      = cust.CountyName;
            serviceOrder.EmailAddress    = cust.EmailAddress;
            //ServiceOrder.FaxNo           = cust.FaxNo;
            //ServiceOrder.PhoneNo         = cust.PhoneNo;
            //ServiceOrder.PhoneNo2        = cust.PhoneNo2;

            if(!string.IsNullOrEmpty(cust.BillToCustomerGuid))
                cust = _customerService.GetCustomer(cust.BillToCustomerGuid);
            // Fill in bill to customer data
            serviceOrder.BillToCustomerGuid = cust.CustomerGuid;
            serviceOrder.BilltoCompanyName = cust.CompanyName;
            //ServiceOrder.BilltoCompanyName2    = cust.CompanyName2;
            serviceOrder.BilltoContactName = cust.ContactName;
            serviceOrder.BilltoAddress = cust.Address;
            serviceOrder.BilltoAddress2 = cust.Address2;
            serviceOrder.BilltoZipCode = cust.ZipCode;
            serviceOrder.BilltoCityName = cust.CityName;
            serviceOrder.BilltoCountryGuid = cust.CountryGuid;
            serviceOrder.BilltoCountyName = cust.CountyName;
            //ServiceOrder.BilltoEmailAddress = cust.EmailAddress;
            //ServiceOrder.BilltoFaxNo           = cust.FaxNo;
            //ServiceOrder.BilltoPhoneNo         = cust.PhoneNo;
            //ServiceOrder.BilltoPhoneNo2        = cust.PhoneNo2;

            
            return serviceOrder;
        }

        private void PopulateServiceItemData(ServiceOrder serviceOrder, ServiceItemPostData serviceItemPostData)
        {
            serviceOrder.ServiceItemNo = serviceItemPostData.ServiceItemNo;
            serviceOrder.ItemNo = serviceItemPostData.ItemNo;
            serviceOrder.Description = serviceItemPostData.Description;
            serviceOrder.ItemType = 0;
            // ServiceItemNo 0=Service item, 1=Product, 2=Just text...
            if (string.IsNullOrEmpty(serviceOrder.ServiceItemNo))
            {
                serviceOrder.ItemType = 1;
                if (string.IsNullOrEmpty(serviceOrder.ItemNo)) serviceOrder.ItemType = 2;
            }
            
        }

        private ServiceOrderCommentLineItem BuildServiceOrderCommentLine(ServiceOrderPostData serviceOrderPostData, string uniqueGuid)
        {
            var commentItem = Mapper.Map<ServiceOrderCommentLineItem>(serviceOrderPostData);
            commentItem.CommentLineGuid = Guid.NewGuid().ToString();
            commentItem.ApprovedDate = serviceOrderPostData.SelectedStartDate;
            commentItem.ServiceOrderGuid = uniqueGuid;
            commentItem.ServiceOrderNumber = uniqueGuid;
            var user = _userService.GetUser(serviceOrderPostData.UserId);
            commentItem.PortalContactName = user.ContactName;
            commentItem.UserGuid = user.UserGuid;
            commentItem.UserType = "PORTAL";

            return commentItem;
        }

        private void PopulateAdministratorRoleFieldData(ServiceOrder item, ServiceOrderAdditionalPostData serviceOrderAdditionalPostData)
        {
            if (serviceOrderAdditionalPostData != null)
            {
                item.DepartmentGuid = serviceOrderAdditionalPostData.DepartmentGuid;
                item.ResponsibleUserGuid = serviceOrderAdditionalPostData.ResponsibleUserGuid;
            }
        }

        private bool GetConfigurationAsBool(string key)
        {
            string result = ConfigurationManager.AppSettings.Get(key);

            return Utilities.CBoolEx(result);
        }

        private int CountAllServiceOrders(string searchValue, ServiceOrderSearchType st = ServiceOrderSearchType.Active)
        {
            return GetResults<int>(GetCountSql(searchValue, st)).FirstOrDefault();
        }

        private int CountAllByCustomerGuidList(string searchValue, IEnumerable<CustomerItem> customerGuidList, ServiceOrderSearchType st)
        {
            return customerGuidList.Sum(customer => CountAllByCustomerGuid(searchValue, customer.CustomerGuid, st));
        }

        private int CountAllByCustomerGuid(string searchValue, string customerGuid, ServiceOrderSearchType st)
        {
            var sql = string.Format("{0} AND (CustomerGuid = @customerGuid)", GetCountSql(searchValue, st));
            var param = new { customerGuid };
            return GetResults<int>(sql, param).SingleOrDefault();
        }

        private int CountAllByProjectIds(string searchValue, IEnumerable<string> projectIds, ServiceOrderSearchType st)
        {
            throw new NotImplementedException();
        }

        private string GetCountSql(string searchValue, ServiceOrderSearchType st)
        {
            string statusFilter;
            switch (st)
            {
                case ServiceOrderSearchType.Active:
                    statusFilter = "AND (IsNull(StatusCode,'') != 'COMPLETED')";
                    break;
                case ServiceOrderSearchType.Closed:
                    statusFilter = "AND (IsNull(StatusCode,'') = 'COMPLETED')";
                    break;
                default:
                    statusFilter = "";
                    break;
            }

            var searchText = string.Empty;
            if (!string.IsNullOrEmpty(searchValue))
            {
                searchValue = searchValue.Replace("*", "%");
                searchText = "AND ((LOWER(ServiceOrderGuid) LIKE LOWER('" + searchValue +
                    "%')) OR (LOWER(JobDescription) LIKE LOWER('" + searchValue +
                    "%')) OR (LOWER(Description) LIKE LOWER('" + searchValue +
                    "%')) OR (LOWER(ShiptoCompanyName) LIKE LOWER('" + searchValue +
                    "%')) OR (LOWER(ShiptoContactName ) LIKE LOWER('" + searchValue +
                    "%')) OR (LOWER(ShiptoAddress) LIKE LOWER('" + searchValue +
                    "%')) OR (LOWER(ShiptoCityName) LIKE LOWER('" + searchValue +
                    "%')) OR (LOWER(ShiptoZipCode) LIKE LOWER('" + searchValue +
                    "%')) OR (LOWER(CompanyName) LIKE LOWER('" + searchValue +
                    "%')) OR (LOWER(Address) LIKE LOWER('" + searchValue +
                    "%')) OR (LOWER(ZipCode) LIKE LOWER('" + searchValue +
                    "%')) OR (LOWER(CityName) LIKE LOWER('" + searchValue +
                    "%')))";
            }

            return String.Format(@"SELECT COUNT(*) FROM (
	                                    SELECT {0}, ESMServiceOrder.ResponseDate AS BASReceivedTime, 0 AS BASGuid FROM ESMServiceOrder
	                                      LEFT JOIN ESMServiceOrderReg ON (ESMServiceOrder.ServiceOrderGuid=ESMServiceOrderReg.ServiceOrderGuid)
	                                      WHERE ESMServiceOrderReg.ServiceOrderGuid IS NULL
	                                    UNION ALL
	                                    SELECT {1}, ESMServiceOrderReg.BASReceivedTime, ESMServiceOrderReg.BASGuid FROM ESMServiceOrderReg
	                                      INNER JOIN (
		                                      SELECT Max(BASGuid) AS BASGuid FROM ESMServiceOrderReg 
			                                    GROUP BY ESMServiceOrderReg.ServiceOrderGuid
	                                      ) T1 ON (ESMServiceOrderReg.BASGuid = T1.BASGuid)
                                    ) AS ServiceOrder

                                    LEFT JOIN
	                                    (SELECT StatusGuid AS StatusCode, Guid_X AS ServiceOrderId, JobPlanningType FROM 
		                                    (select Guid_X, JobPlanningType, MAX(Precedence) as Precedence from JobPlanning WITH (NOLOCK) inner join JobPlanningStatus WITH (NOLOCK) on (JobPlanning.StatusGuid=JobPlanningStatus.StatusGuid)
		                                    group by JobPlanning.Guid_X, JobPlanningType) T1
		                                    inner join JobPlanningStatus ON (T1.Precedence = JobPlanningStatus.Precedence)
	                                    ) AS Status
                                    ON (ServiceOrder.ServiceOrderGuid=Status.ServiceOrderId)
                                    WHERE (IsNull(Status.JobPlanningType,3) IN (1,3)) {2} {3}",
                                _serviceOrderRepository.GetServiceOrderCommonFields().Replace("@", "ESMServiceOrder."),
                                _serviceOrderRepository.GetServiceOrderCommonFields().Replace("@", "ESMServiceOrderReg."),
                                statusFilter,
                                searchText);
        }
    }
}