﻿using System.Linq;
using CmsPublic.DataRepository;
using EISCS.ExpandITFramework.Util;
using EISCS.Shop.DO.BAS.Dto;
using EISCS.Shop.DO.BAS.Interface;
using EISCS.Shop.DO.BAS.Repository;
using EISCS.Shop.DO.Dto;
using EISCS.Shop.DO.Interface;

namespace EISCS.Shop.DO.BAS.Service
{
    public class ServiceItemService : BaseDataService, IServiceItemService
    {
        private readonly IPortalDatabaseConnectionFactory _connectionFactory;
        private readonly ICustomerService _customerService;
        private readonly IServiceItemRepository _serviceItemRepository;

        public ServiceItemService(IPortalDatabaseConnectionFactory connectionFactory, ICustomerService customerService, IServiceItemRepository serviceItemRepository) : base(connectionFactory)
        {
            _connectionFactory = connectionFactory;
            _customerService = customerService;
            _serviceItemRepository = serviceItemRepository;
        }

        public int CountServiceItemsByUser(string searchValue, UserTable user, string customerGuid, string addressGuid)
        {
            var count = 0;
            if (customerGuid.IsNullOrEmpty())
            {
                switch (user.RoleId)
                {
                    case "Admin":
                        count = CountServiceItems(searchValue);
                        break;
                    case "CustomerGroup":
                        var customerGuidList = _customerService.GetCustomersForCustomerGroup(user.CustomerGroupId).Select(cg => cg.CustomerGuid);
                        count += customerGuidList.Sum(customer => CountServiceItems(searchValue, customer));
                        break;
                    default:
                        count = CountServiceItems(searchValue, user.CustomerGuid);
                        break;
                }
            }
            else
                count = CountServiceItems(searchValue, customerGuid, addressGuid);

            return count;
        }

        public ServiceItem GetById(string serviceItemNo)
        {
            return _serviceItemRepository.GetById(serviceItemNo);
        }

        private int CountServiceItems(string searchValue = null, string customerGuid = null, string addressGuid = null)
        {
            var searchFilter = "";
            if (!string.IsNullOrEmpty(searchValue))
            {
                searchFilter = string.Format(@" WHERE lower(ServiceItemNo) LIKE lower('%" + searchValue + "%')" +
                                        "OR ItemNo LIKE '%" + searchValue + "%'" +
                                        "OR lower(SerialNo) LIKE lower('%" + searchValue + "%')" +
                                        "OR lower(Description) LIKE lower('%" + searchValue + "%')");
                searchFilter = searchFilter.Replace("*", "%");
            }

            if (!string.IsNullOrEmpty(customerGuid))
            {
                if (string.IsNullOrEmpty(searchFilter))
                    searchFilter = @" WHERE ";
                else
                    searchFilter += @" AND ";
                searchFilter += string.Format(@"CustomerGuid = @customerGuid");
            }

            if (addressGuid != null)
            {
                if (string.IsNullOrEmpty(searchFilter))
                    searchFilter = @" WHERE ";
                else
                    searchFilter += @" AND ";
                searchFilter += string.Format(@"AddressGuid = @addressGuid");
            }

            var param = new { customerGuid, addressGuid };

            var query = string.Format(@"SELECT COUNT(*) FROM ESMServiceItem {0}", searchFilter);

            return GetResults<int>(query, param).FirstOrDefault();
        }
    }
}