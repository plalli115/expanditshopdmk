﻿using System.Data;
using System.Linq;
using CmsPublic.DataRepository;
using EISCS.Shop.DO.BAS.Interface;

namespace EISCS.Shop.DO.BAS.Service
{
    public class UniqueGuidService : BaseDataService, IUniqueGuidService
    {
        public UniqueGuidService(IPortalDatabaseConnectionFactory connectionFactory)
            : base(connectionFactory)
        {
        }

        public string GenerateUniqueGuid()
        {
            return GetResults<string>("GetNextServiceHeaderGuid", commandType: CommandType.StoredProcedure).Single();
        }
    }
}