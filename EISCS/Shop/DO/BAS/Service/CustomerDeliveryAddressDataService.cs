﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlTypes;
using System.Linq;
using CmsPublic.DataRepository;
using EISCS.ExpandITFramework.Util;
using EISCS.Shop.DO.BAS.Dto;
using EISCS.Shop.DO.BAS.Interface;

namespace EISCS.Shop.DO.BAS.Service
{
    public class CustomerAddressDataService : BaseDataService, ICustomerAddressDataService
    {
        public CustomerAddressDataService(IPortalDatabaseConnectionFactory dbFactory) : base(dbFactory)
        {
        }

        public IEnumerable<CustomerAddressItem> All()
        {
            const string query = "SELECT * FROM CustomerAddress";

            return GetResults<CustomerAddressItem>(query);
        }

        public IEnumerable<CustomerAddressItem> AllByCustomerId(string id)
        {
            const string query = "SELECT * FROM CustomerAddress WHERE CustomerGuid = @id";
            var param = new {id};

            return GetResults<CustomerAddressItem>(query, param);
        }

		public CustomerAddressItem CustomerDeliveryAddressById(string customerId, string addressGuid)
        {
			const string query = "SELECT * FROM CustomerAddress WHERE CustomerGuid = @customerId AND AddressGuid = @addressGuid";
            var param = new {customerId, addressGuid};

            return GetResults<CustomerAddressItem>(query, param).SingleOrDefault();
        }

        public List<CustomerAddressItem> GetAddressesForCustomer(int start, int end, string columnBeingSort, string sortDir, string searchValue, string customerGuid)
        {
            var filter = string.Empty;
            if (!searchValue.IsNullOrEmpty())
            {
                filter = string.Format(@" AND (Lower(Address1) LIKE(Lower({0}) OR " + 
                                       "Lower(Address2) LIKE(Lower({0}) OR " +
                                       "Lower(CityName) LIKE(Lower({0}) OR " +
                                       "Lower(ContactPerson) LIKE(Lower({0}) OR " +
                                       "Lower(ZipCode) LIKE(Lower({0})) ", 
                                       searchValue);
            }

            var query = string.Format(@"SELECT * FROM (" +
                                      "SELECT ROW_NUMBER() OVER (ORDER BY {0} {1}, AddressGuid) AS RowID, * " +
                                      "FROM CustomerAddress " +
                                      "WHERE CustomerGuid = @customerGuid {2}) temp " +
                                      "WHERE RowID BETWEEN {3} AND {4} " +
                                      "ORDER BY AddressGuid",
                                      columnBeingSort,
                                      sortDir,
                                      filter,
                                      start,
                                      end);

            var param = new {customerGuid};
            var addresses = GetResults<CustomerAddressItem>(query, param).ToList();

            return addresses;
        }

        public int CountAdressesForCustomer(string searchValue, string customerGuid)
        {
            var filter = string.Empty;
            if (!searchValue.IsNullOrEmpty())
            {
                filter =
                    string.Format(
                        @" AND (Lower(Address1) LIKE(Lower({0}) OR Lower(Address2) LIKE(Lower({0}) OR Lower(CityName) LIKE(Lower({0}) OR Lower(ContactPerson) LIKE(Lower({0}) OR Lower(ZipCode) LIKE(Lower({0}))",
                        searchValue);
            }

            var query = string.Format(@"SELECT COUNT(*) " +
                                        "FROM CustomerAddress " + 
                                        "WHERE CustomerGuid = @customerGuid {0}",
                                        filter);
            var param = new {customerGuid};
            return GetResults<int>(query, param).SingleOrDefault();
        }
    }
}