﻿using System;
using System.Collections.Generic;
using System.Web;
using EISCS.Shop.DO.BAS.Dto;
using EISCS.Shop.DO.BAS.Interface;

namespace EISCS.Shop.DO.BAS.Service
{
    public class ServiceAttachmentService : IServiceAttachmentService
    {
        private readonly IServiceAttachmentRepository _serviceAttachmentRepository;

        public ServiceAttachmentService(IServiceAttachmentRepository serviceAttachmentRepository)
        {
            _serviceAttachmentRepository = serviceAttachmentRepository;
        }

        public bool SaveAttachment(HttpPostedFileBase file, string serviceOrderGuid, string attachmentComment)
        {
            string strFileExtension = "";
            string[] strFileParts = file.FileName.Split('.');
            if(strFileParts.Length>0)
                strFileExtension = strFileParts[strFileParts.Length-1];
            
            if (file == null) throw new ArgumentNullException("file");
            if (serviceOrderGuid == null) throw new ArgumentNullException("serviceOrderGuid");

            var bytes = new Byte[file.InputStream.Length];
            file.InputStream.Read(bytes, 0, bytes.Length);

            DateTime created = DateTime.Now;
            var item = new ServiceAttachmentItem
                {
                    ServiceOrderGuid = serviceOrderGuid,
                    ServiceHeaderGuid = serviceOrderGuid,
                    FileName = file.FileName,
                    ContentType = file.ContentType,
                    FileExtension = strFileExtension,
                    FileData = bytes,
                    FileSize = file.InputStream.Length,
                    Description = attachmentComment,
                    SavedDate = created,
                    SavedTime = created,
                    BASReceivedTime = created,
                    BASInjected = created,
                    ApprovedDate = created
                };

            var savedItem = _serviceAttachmentRepository.Add(item);

            return savedItem != null;
        }

        public bool DeleteAttachment(int uniqueId)
        {
            return _serviceAttachmentRepository.Remove(uniqueId);
        }

        public void DeleteAllByServiceOrderGuid(string orderGuid)
        {
            _serviceAttachmentRepository.DeleteAllByServiceOrderGuid(orderGuid);
        }

        public IEnumerable<ServiceAttachmentItem> GetAllAttachmentsByServiceOrderGuid(string serviceOrderGuid)
        {
            return _serviceAttachmentRepository.GetAllByServiceOrderGuid(serviceOrderGuid);
        }
    }
}