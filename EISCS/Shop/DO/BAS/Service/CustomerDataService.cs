﻿using System.Collections.Generic;
using System.Linq;
using CmsPublic.DataRepository;
using EISCS.Shop.DO.BAS.Dto;
using EISCS.Shop.DO.BAS.Interface;
using EISCS.Shop.DO.Interface;

namespace EISCS.Shop.DO.BAS.Service
{
    public class CustomerDataService : BaseDataService, ICustomerDataService
    {
        private readonly IPortalDatabaseConnectionFactory _portalDatabaseConnectionFactory;

        public CustomerDataService(IExpanditDbFactory dbFactory, IPortalDatabaseConnectionFactory portalDatabaseConnectionFactory) : base(dbFactory)
        {
            _portalDatabaseConnectionFactory = portalDatabaseConnectionFactory;
        }

        public IEnumerable<PortalCustomerItem> AjaxFindCustomer(string searchQuery)
        {
            var query = string.Format("{0} WHERE CustomerGuid LIKE '%' + @searchQuery + '%' OR CompanyName LIKE '%' + @searchQuery + '%'", GetDefaultSql());
            var param = new {searchQuery};

            return GetResults<PortalCustomerItem>(query, param, _portalDatabaseConnectionFactory);
        }

        public PortalCustomerItem CustomerById(string customerGuid)
        {
            var query = string.Format("{0} WHERE CustomerGuid = @customerGuid", GetDefaultSql());
            var param = new {@customerGuid};

            return GetResults<PortalCustomerItem>(query, param, _portalDatabaseConnectionFactory).SingleOrDefault();
        }

        private string GetDefaultSql()
        {
            return "SELECT * FROM CustomerTable";
        }
    }
}