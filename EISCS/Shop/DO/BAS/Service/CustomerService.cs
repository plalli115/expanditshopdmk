﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using AutoMapper;
using CmsPublic.DataRepository;
using EISCS.ExpandITFramework.Util;
using EISCS.Shop.DO.BAS.Dto;
using EISCS.Shop.DO.BAS.Interface;
using EISCS.Shop.DO.Dto;
using EISCS.WebUserControlLogic.Country;

namespace EISCS.Shop.DO.BAS.Service
{
    public class CustomerService : BaseDataService, ICustomerService
    {
        private readonly ICustomerPropertiesRepository _customerPropertiesRepository;
        private readonly IShopDatabaseConnectionFactory _shopConnectionFactory;
        private readonly ICountryControlLogic _countryControlLogic;
        private IPortalDatabaseConnectionFactory _connectionFactory;

        public CustomerService(IPortalDatabaseConnectionFactory connectionFactory,
        IShopDatabaseConnectionFactory shopConnectionFactory,
                               ICustomerPropertiesRepository customerPropertiesRepository,
                               ICountryControlLogic countryControlLogic)
            : base(connectionFactory)
        {
            _connectionFactory = connectionFactory;
            _shopConnectionFactory = shopConnectionFactory;
            _customerPropertiesRepository = customerPropertiesRepository;
            _countryControlLogic = countryControlLogic;
        }

        public IEnumerable<CustomerItem> FindCustomers(string searchString, string customerGroupId = null)
        {
            IEnumerable<CustomerItem> customers;

            if (customerGroupId == null)
            {
                string query = @"SELECT * 
                             FROM CustomerTable 
                             WHERE (CompanyName LIKE '%' + @searchString + '%' 
                                    OR CustomerGuid LIKE '%' + @searchString + '%')";

                customers = GetResults<CustomerItem>(query, new { searchString }, _connectionFactory);
            }
            else
            {
                customers = GetCustomersForCustomerGroup(customerGroupId);
            }

            return customers ?? Enumerable.Empty<CustomerItem>();
        }

        public CustomerItem GetCustomer(string id)
        {
            string query = @"SELECT * FROM CustomerTable WHERE CustomerGuid = @customerGuid";
            return GetResults<CustomerItem>(query, new { customerGuid = id }, _connectionFactory).FirstOrDefault();
        }

        public Dictionary<string, string> GetAllTechnicians()
        {
            return GetAllFrom<TechnicianItem>("UserTable").ToDictionary(key => key.UserGuid, value => value.FullName);
        }

        public Dictionary<string, string> GetAllServiceManagers()
        {
            return GetAllFrom<ServiceManagerItem>("UserTable")
                .ToDictionary(key => key.UserGuid, value => value.FullName);
        }

        public Dictionary<string, string> GetAllServiceOrderTypes()
        {
            return GetAllFrom<ServiceOrderTypeItem>("ESMServiceOrderType")
                .ToDictionary(key => key.Code, value => value.Description);
        }

        public IEnumerable<CustomerPropertyItem> GetPropertiesByCustomerId(string customerId)
        {
            return _customerPropertiesRepository.All().Where(p => p.CustomerId == customerId);
        }

        public void SetPropertiesForCustomer(string customerId, IEnumerable<string> taskIds,
                                             IEnumerable<string> technicianIds,
                                             IEnumerable<string> serviceManagerIds)
        {
            Execute("delete from CustomerProperties where CustomerId = @customerId", new { customerId },
                    _shopConnectionFactory);

            AddCustomerProperties(taskIds, customerId, CustomerPropertyItem.TableType.Tasks);
            AddCustomerProperties(technicianIds, customerId, CustomerPropertyItem.TableType.Technician);
            AddCustomerProperties(serviceManagerIds, customerId, CustomerPropertyItem.TableType.ServiceManager);
        }

        private void AddCustomerProperties(IEnumerable<string> ids, string customerId,
                                           CustomerPropertyItem.TableType tableType)
        {
            var item = new CustomerPropertyItem { CustomerId = customerId, RelationTableType = tableType };
            foreach (string taskId in ids)
            {
                item.PropertyId = taskId;
                _customerPropertiesRepository.Add(item);
            }
        }

        public ShippingAddress GetBillingAddress(string customerGuid)
        {
            var customer = GetCustomer(customerGuid);
            var shippingAddress = Mapper.Map<CustomerItem, ShippingAddress>(customer);
            shippingAddress.AddressType = "B2B";
            shippingAddress.IsDefault = true;

            var countryRow = _countryControlLogic.LoadCountries().Select().SingleOrDefault(c => (string)c["CountryGuid"] == shippingAddress.CountryGuid);
            shippingAddress.CountryName = (countryRow == default(DataRow)) ? string.Empty : (string)countryRow["CountryName"];

            return shippingAddress;
        }

        public List<CustomerItem> GetCustomers(int start, int end, string columnBeingSort, string sortDir, string searchValue, string customerGuid = null)
        {
            var filter = "";
            if (!searchValue.IsNullOrEmpty())
            {
                filter = string.Format(@"WHERE CustomerGuid LIKE '%" + searchValue +
                    "%' OR lower(CompanyName) LIKE lower('%" + searchValue +
                    "%') OR lower(CityName) LIKE lower('%" + searchValue +
                    "%') OR lower(Address) LIKE lower('%" + searchValue + "%')");
            }

            if (!customerGuid.IsNullOrEmpty())
            {
                if (filter.IsNullOrEmpty())
                    filter = string.Format(@" WHERE ");
                filter += string.Format(@" CustomerGuid LIKE @customerGuid");
            }

            var sql = string.Format(@"SELECT * FROM (" +
                                        "SELECT ROW_NUMBER() OVER (ORDER BY " + columnBeingSort + " " + sortDir + ", CustomerGuid) AS RowID, * " +
                                        "FROM CustomerTable " + filter + ") temp " +
                                        "WHERE RowID BETWEEN " + start + " AND " + end + " ORDER BY RowID");

            var param = new { customerGuid };

            return GetResults<CustomerItem>(sql, param).ToList();
        }

        public int CountCustomers(string searchValue, UserTable user)
        {
            var sql = "";
            var count = 0;

            var filter = string.Empty;
            if (!searchValue.IsNullOrEmpty())
                filter = string.Format(@" CustomerGuid LIKE '%" + searchValue +
                    "%' OR lower(CompanyName) LIKE lower('%" + searchValue +
                    "%') OR lower(CityName) LIKE lower('%" + searchValue +
                    "%') OR lower(Address) LIKE lower('%" + searchValue + "%')");

            if (user != null)
            {
                switch (user.RoleId)
                {
                    case "Admin":
                        if (!filter.IsNullOrEmpty())
                            filter = string.Format(@" WHERE {0}", filter);
                        sql = string.Format(@"SELECT COUNT(*) FROM CustomerTable {0}", filter);
                        count = GetResults<int>(sql).SingleOrDefault();
                        break;
                    default:
                        if (!filter.IsNullOrEmpty())
                            filter = string.Format(@" AND {0}", filter);
                        count = GetSingleCustomerCount(filter, sql);
                        break;
                    case "CustomerGroup":
                        if (!filter.IsNullOrEmpty())
                            filter = string.Format(@" AND {0}", filter);
                        count = GetCountCustomersResults(filter, user.CustomerGroupId);
                        break;
                }
            }

            return count;
        }

        private int GetSingleCustomerCount(string filter, string customerGuid)
        {
            var sql = string.Format(@"SELECT COUNT(*) FROM CustomerTable WHERE CustomerGuid = '{0}' {1}", customerGuid, filter);
            return GetResults<int>(sql).SingleOrDefault();
        }

        private int GetCountCustomersResults(string filter, string customerGroupId)
        {
            var customersGroup = GetCustomersForCustomerGroup(customerGroupId);
            var count = customersGroup.Sum(customer => GetSingleCustomerCount(filter, customer.CustomerGuid));
            return count;
        }

        public IEnumerable<CustomerItem> GetCustomersForCustomerGroup(string id)
        {
            string query = @"SELECT * 
                             FROM CustomerTable 
                             WHERE BillToCustomerGuid = @id
                             OR CustomerGuid = @id";

            return GetResults<CustomerItem>(query, new { id }, _shopConnectionFactory);
        }

        public List<CustomerItem> GetCustomersByCustomerGroup(IEnumerable<string> customerGuidList, int start, int end, string columnBeingSort, string sortDir, string searchValue)
        {
            var searchString = string.Empty;
            if (!string.IsNullOrEmpty(searchValue))
            {
                searchString = string.Format(@" WHERE lower(ServiceItemNo) LIKE lower('%" + searchValue + "%')" +
                                        "OR ItemNo LIKE '%" + searchValue + "%'" +
                                        "OR lower(SerialNo) LIKE lower('%" + searchValue + "%')" +
                                        "OR lower(Description) LIKE lower('%" + searchValue + "%')");
                searchString = searchString.Replace("*", "%");
            }

            if (string.IsNullOrEmpty(searchString))
                searchString = @" WHERE (";
            else
                searchString += @" AND (";

            searchString = customerGuidList.Aggregate(searchString, (current, customer) => current + string.Format(@" CustomerGuid = '{0}' OR ", customer));

            searchString = searchString.Remove(searchString.Length - 3);
            searchString += ")";

            var query = string.Format(@"SELECT * FROM (" +
                                        "SELECT ROW_NUMBER() OVER (ORDER BY {0} " + sortDir + ", CustomerGuid) AS RowID, * " +
                                        "FROM CustomerTable " +
                                        "{2}) temp " +
                                        "WHERE RowID BETWEEN {3} AND {4} ORDER BY RowID",
                                        columnBeingSort,
                                        sortDir,
                                        searchString,
                                        start,
                                        end);

            return GetResults<CustomerItem>(query).ToList();
        }
    }
}