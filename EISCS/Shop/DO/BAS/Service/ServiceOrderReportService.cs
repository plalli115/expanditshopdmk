﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using AutoMapper;
using CmsPublic.DataRepository;
using EISCS.Shop.DO.BAS.Dto;
using EISCS.Shop.DO.BAS.Interface;

namespace EISCS.Shop.DO.BAS.Service
{
    public class ServiceOrderReportService : BaseDataService, IServiceOrderReportService
    {
        private readonly IServiceOrderService _serviceOrderService;
        private readonly IServiceItemService _serviceItemService;

        public ServiceOrderReportService(IPortalDatabaseConnectionFactory connectionFactory, IServiceOrderService serviceOrderService, IServiceItemService serviceItemService) : base(connectionFactory)
        {
            _serviceOrderService = serviceOrderService;
            _serviceItemService = serviceItemService;
        }

        public IEnumerable<ReportHeader> GetReports(int start, int end, string columnBeingSort, string sortDir, string searchValue)
        {
            var searchText = string.Empty;
            if (!string.IsNullOrEmpty(searchValue))
            {
                searchValue = searchValue.Replace("*", "%");
                searchText = "AND (LOWER(ServiceOrderGuid) LIKE LOWER('" + searchValue + "%') OR (LOWER(UserGuid) LIKE LOWER('" + searchValue + "%')))";
            }

            var sql = String.Format(@"SELECT * FROM (
                                        SELECT ROW_NUMBER() OVER (
                                                ORDER BY {3} {4}, CompletedDate DESC, CompletedTime DESC, ReportGuid
                                            ) AS RowID
                                            , ESMReport.*, 
                                            ESMReportType.Description
                                        FROM ESMReport
                                        JOIN ESMReportType
                                        ON ESMReport.ReportTypeGuid = ESMReportType.ReportTypeGuid
                                        WHERE PdfFileData IS NOT NULL {0}
                                    ) temp
                                    WHERE RowID BETWEEN {1} AND {2} ORDER BY RowID",
                searchText,
                start,
                end,
                columnBeingSort,
                sortDir);

            return GetResults<ReportHeader>(sql);
        }

        public int CountReports(string searchValue)
        {
            var searchText = string.Empty;
            if (!string.IsNullOrEmpty(searchValue))
            {
                searchValue = searchValue.Replace("*", "%");
                searchText = "AND (LOWER(ServiceOrderGuid) LIKE LOWER('" + searchValue + "%') OR (LOWER(UserGuid) LIKE LOWER('" + searchValue + "%')))";
            }

            var sql = String.Format(@"SELECT COUNT(*) FROM (
                                        SELECT ESMReport.*, ESMReportType.Description 
                                        FROM ESMReport
                                        JOIN ESMReportType
                                        ON ESMReport.ReportTypeGuid = ESMReportType.ReportTypeGuid
                                        WHERE PdfFileData IS NOT NULL {0}
                                    ) temp",
                searchText);

            return GetResults<int>(sql).SingleOrDefault();
        }

        public ReportHeader GetReport(string reportGuid)
        {
            var sql = String.Format(@"SELECT TOP 1 * FROM ESMReport WHERE ReportGuid = @reportGuid ORDER BY BASGuid DESC");
            var param = new {reportGuid};
            return GetResults<ReportHeader>(sql, param).FirstOrDefault();
        }

        public bool ServiceOrderHasReport(string serviceOrderGuid)
        {
            return GetReportsForOrder(serviceOrderGuid).Any();
        }

        public IEnumerable<ReportHeader> GetReportsForOrder(string serviceOrderGuid)
        {
            var sql = String.Format(@"SELECT * 
                                        FROM ESMReport
                                        LEFT JOIN ESMReportType
                                        ON ESMReport.ReportTypeGuid = ESMReportType.ReportTypeGuid
                                        WHERE PdfFileData IS NOT NULL
                                        AND ServiceOrderGuid=@serviceOrderGuid");

            return GetResults<ReportHeader>(sql, new {serviceOrderGuid}).ToList();
        }


        public IEnumerable<ServiceOrderReport> CompleteReports(IEnumerable<ReportHeader> reports)
        {
            return reports.Select(CompleteReport).Where(completedReport => completedReport != null).ToList();
        }

        private ServiceOrderReport CompleteReport(ReportHeader report)
        {
            var completeReport = Mapper.Map<ServiceOrderReport>(report);
            completeReport.ServiceOrder = _serviceOrderService.GetById(report.ServiceOrderGuid);
            
            if (completeReport.ServiceOrder == null)
                return null;

            completeReport.ServiceItem = _serviceItemService.GetById(report.ServiceItemNo);
            return completeReport;
        }
    }
}