﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using CmsPublic.DataRepository;
using Dapper;
using EISCS.Shop.DO.BAS.Dto;
using EISCS.Shop.DO.BAS.Interface;
using EISCS.Shop.DO.Repository;

namespace EISCS.Shop.DO.BAS.Repository
{
    [TableProperties(TableName = "ESMServiceAttachmentReg", PrimaryKeyName = "BASGuid")]
    public class ServiceAttachmentRepository : BaseRepository<ServiceAttachmentItem>, IServiceAttachmentRepository
    {
        private const string COLUMN_NAMES =
            "AttachmentGuid, ServiceOrderGuid, ServiceHeaderGuid, LineNumber, SavedDate, SavedTime, FileName, FileExtension, ContentType, FileSize, FileData, Description, AttachmentRelation, RecordAction, JobPlanningGuid, readonly, client_guid, BASReceivedTime, BASInjected, BASClient, BASSyncCommand, BASSyncState, Approved";

        private const string COLUMN_VALUES =
            "newid(), @serviceOrderGuid, @serviceHeaderGuid, @lineNumber, @savedDate, @savedTime, @fileName, @fileExtension, @contentType, @fileSize, @fileData, @description, @attachmentRelation, @recordAction, @jobPlanningGuid, @Readonly, @client_guid, @basReceivedTime, @basInjected, @basClient, @basSyncCommand, @basSyncState, @approved";

        public ServiceAttachmentRepository(IPortalDatabaseConnectionFactory connectionFactory)
            : base(connectionFactory)
        {
        }

        public override ServiceAttachmentItem Add(ServiceAttachmentItem item, bool useOutputClause = true)
        {
            using (IDbConnection connection = GetConnection())
            {
                string query = string.Format(@"INSERT INTO {0}({1}) VALUES({2}); SELECT CAST(SCOPE_IDENTITY() AS INT)", GetTableName(), COLUMN_NAMES,
                    COLUMN_VALUES);
                if (String.IsNullOrEmpty(item.client_guid))
                    item.client_guid = Guid.NewGuid().ToString();
                IEnumerable<int> insertedItem =
                    connection.Query<int>(query,
                        new
                            {
                                attachmentGuid = item.AttachmentGuid,
                                serviceOrderGuid = item.ServiceOrderGuid,
                                serviceHeaderGuid = item.ServiceHeaderGuid,
                                lineNumber = GenerateUniqueId(item.ServiceOrderGuid),
                                savedDate = item.SavedDate,
                                savedTime = item.SavedTime,
                                fileName = item.FileName,
                                fileExtension = item.FileExtension,
                                contentType = item.ContentType,
                                fileSize = item.FileSize,
                                fileData = item.FileData,
                                description = item.Description,
                                attachmentRelation = item.AttachmentRelation,
                                recordAction = item.RecordAction,
                                jobPlanningGuid = item.JobPlanningGuid,
                                item.Readonly,
                                item.client_guid,
                                basReceivedTime = item.BASReceivedTime,
                                basInjected = item.BASInjected,
                                basClient = item.BASClient,
                                basSyncCommand = item.BASSyncCommand,
                                basSyncState = item.BASSyncState,
                                approved = item.Approved
                            });
                item.BASGuid = insertedItem.Single();
                return item;
            }
        }

        public override bool Update(ServiceAttachmentItem item)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<ServiceAttachmentItem> GetAllByServiceOrderGuid(string serviceOrderGuid)
        {
            using (IDbConnection connection = GetConnection())
            {
                string query = string.Format(@"{0} WHERE ServiceOrderGuid = @serviceOrderGuid", GetDefaultSql());
                var param = new {serviceOrderGuid};

                return connection.Query<ServiceAttachmentItem>(query, param);
            }
        }

        public void DeleteAllByServiceOrderGuid(string serviceOrderGuid)
        {
            using (IDbConnection connection = GetConnection())
            {
                string query = string.Format(@"DELETE FROM {0} WHERE ServiceOrderGuid = @serviceOrderGuid",
                    GetTableName());
                var param = new {serviceOrderGuid};

                connection.Execute(query, param);
            }
        }

        protected int GenerateUniqueId(string serviceOrderGuid)
        {
            int highestUniqueId = GetHighestUniqueId(serviceOrderGuid);
            var rand = new Random();
            int randomNumber = rand.Next(0, 9999);
            return (highestUniqueId + ((randomNumber*9999) + 10000));
        }

        private int GetHighestUniqueId(string serviceOrderGuid)
        {
            using (IDbConnection connection = GetConnection())
            {
                string query =
                    string.Format(
                        @"SELECT isnull(MAX(LineNumber),1) AS MaxLineNo FROM {0} WHERE ServiceOrderGuid = @serviceOrderGuid",
                        GetTableName());
                var param = new {serviceOrderGuid};

                return connection.Query<int>(query, param).SingleOrDefault();
            }
        }
    }
}