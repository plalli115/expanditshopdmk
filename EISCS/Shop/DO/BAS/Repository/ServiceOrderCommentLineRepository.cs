﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using CmsPublic.DataRepository;
using Dapper;
using EISCS.Shop.DO.BAS.Dto;
using EISCS.Shop.DO.BAS.Interface;
using EISCS.Shop.DO.Repository;

namespace EISCS.Shop.DO.BAS.Repository
{
    [TableProperties(TableName = "ESMServiceCommentLineReg", PrimaryKeyName = "BASGuid")]
    public class ServiceOrderCommentLineRepository : BaseRepository<ServiceOrderCommentLineItem>,
        IServiceOrderCommentLineRepository
    {
        private const string COLUMN_VALUES = @"@UserGuid, @UserType, @ServiceOrderGuid, @CommentType, @CommentTypeName, @ServiceOrderNumber, @ServiceItemLineNumber, @CommentLineNumber, @CommentLineGuid, @CommentText, @CommentDate, @CommentTypeText, @RecordAction, @JobPlanningGuid, @BASReceivedTime, @BASInjected, @BASClient, @BASSyncCommand, @BASSyncState, @readOnly, newId(), @BASGuidLocal, @BASVersion, @Approved, @ApprovedBy, @ApprovedDate, @VendorGuid";

        public ServiceOrderCommentLineRepository(IPortalDatabaseConnectionFactory connectionFactory)
            : base(connectionFactory)
        {
        }

        public override ServiceOrderCommentLineItem Add(ServiceOrderCommentLineItem item, bool useOutputClause = true)
        {
            using (IDbConnection connection = GetConnection())
            {
                string query = string.Format("INSERT INTO {0}(UserGuid, UserType, ServiceOrderGuid, CommentType, CommentTypeName, ServiceOrderNumber, ServiceItemLineNumber, CommentLineNumber, CommentLineGuid, CommentText, CommentDate, CommentTypeText, RecordAction, JobPlanningGuid, BASReceivedTime, BASInjected, BASClient, BASSyncCommand, BASSyncState, readonly, client_guid, BASGuidLocal, BASVersion, Approved, ApprovedBy, ApprovedDate, VendorGuid) VALUES({1}); SELECT CAST(SCOPE_IDENTITY() AS INT)", GetTableName(), COLUMN_VALUES);

                IEnumerable<int> insertedItem =
                    connection.Query<int>(query, new
                        {
                            item.UserGuid,
                            item.UserType,
                            item.ServiceOrderGuid,
                            item.CommentType,
                            item.CommentTypeName,
                            item.ServiceOrderNumber,
                            item.ServiceItemLineNumber,
                            item.CommentLineNumber,
                            item.CommentLineGuid,
                            item.CommentText,
                            item.CommentDate,
                            item.CommentTypeText,
                            item.RecordAction,
                            item.JobPlanningGuid,
                            item.BASReceivedTime,
                            item.BASInjected,
                            item.BASClient,
                            item.BASSyncCommand,
                            item.BASSyncState,
                            readOnly = item.ReadOnly,
                            item.BASGuidLocal,
                            item.BASVersion,
                            item.Approved,
                            item.ApprovedBy,
                            item.ApprovedDate,
                            item.VendorGuid
                        });

                item.BASGuid = insertedItem.SingleOrDefault();

                return item;
            }
        }

        public override bool Update(ServiceOrderCommentLineItem item)
        {
            int affectedRows;
            using (IDbConnection connection = GetConnection())
            {
                string query = string.Format(@"UPDATE {0} 
                                            SET     CommentText = @commentText
                                            WHERE   {1} = @guid", GetTableName(), GetIdColumnName());

                affectedRows = connection.Execute(query, new {commentText = item.CommentText, guid = item.BASGuid});
            }

            return (affectedRows > 0);
        }

        public IEnumerable<ServiceOrderCommentLineItem> GetAllCommentLinesByServiceOrderGuid(string serviceOrderGuid)
        {
            string query = string.Format(@"{0} WHERE ServiceOrderGuid = @serviceOrderGuid", GetDefaultSql());
            var param = new {serviceOrderGuid};

            return GetResults(query, param);
        }

        public ServiceOrderCommentLineItem GetCommentLineByServiceOrderGuid(string serviceOrderGuid)
        {
            string query = string.Format(@"{0} WHERE ServiceOrderGuid = @serviceOrderGuid AND CommentType = 1",
                GetDefaultSql());
            var param = new {serviceOrderGuid};

            return GetResults(query, param).SingleOrDefault();
        }

        public int GetHighestUniqueId(string serviceOrderGuid)
        {
            using (IDbConnection connection = GetConnection())
            {
                string query =
                    string.Format(
                        @"SELECT ISNULL(MAX(CommentLineNumber), 1) AS MaxLineNo FROM {0} WHERE ServiceOrderGuid = @serviceOrderGuid",
                        GetTableName());
                var param = new {serviceOrderGuid};

                return connection.Query<int>(query, param).Single();
            }
        }

        public void DeleteAllByServiceOrderGuid(string serviceOrderGuid)
        {
            using (IDbConnection connection = GetConnection())
            {
                string query = string.Format("DELETE FROM {0} WHERE ServiceOrderGuid = @serviceOrderGuid",
                    GetTableName());
                connection.Execute(query, new {serviceOrderGuid});
            }
        }
    }
}