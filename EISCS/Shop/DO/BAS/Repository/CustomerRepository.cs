﻿using System.Collections.Generic;
using CmsPublic.DataRepository;
using EISCS.Shop.DO.BAS.Dto;
using EISCS.Shop.DO.BAS.Interface;
using EISCS.Shop.DO.Repository;

namespace EISCS.Shop.DO.BAS.Repository
{
    [TableProperties(TableName = "CustomerTable", PrimaryKeyName = "CustomerGuid")]
    public class CustomerRepository : BaseRepository<CustomerItem>, ICustomerRepository
    {
        private const string FIND_QUERY =
            "CompanyName LIKE '%' + @companyName + '%' OR CustomerGuid LIKE '%' + @customerGuid + '%'";

        public CustomerRepository(IPortalDatabaseConnectionFactory connectionFactory)
            : base(connectionFactory)
        {
        }

        public IEnumerable<CustomerItem> FindCustomers(string searchQuery)
        {
            string query = string.Format("{0} WHERE {1}", GetDefaultSql(), FIND_QUERY);
                
            var parameters = new
                {
                    companyName = searchQuery,
                    customerGuid = searchQuery
                };

            return GetResults(query, parameters);
        }

        public IEnumerable<CustomerItem> GetCustomersByCustomerBranch(string customerBranchGuid)
        {
            string query = string.Format(@"{0} WHERE CustomerBranch = @customerBranch", GetDefaultSql());
            var param = new {customerBranch = customerBranchGuid};

            return GetResults(query, param);
        }
    }
}