﻿using System.Linq;
using CmsPublic.DataRepository;
using EISCS.Shop.DO.BAS.Dto;
using EISCS.Shop.DO.BAS.Interface;
using EISCS.Shop.DO.Repository;

namespace EISCS.Shop.DO.BAS.Repository
{
    [TableProperties(TableName = "JobPlanningStatus", PrimaryKeyName = "StatusGuid")]
    public class ServiceOrderPortalStatusRepository : BaseRepository<PortalStatusItem>,
        IServiceOrderPortalStatusRepository
    {
        public ServiceOrderPortalStatusRepository(IPortalDatabaseConnectionFactory connectionFactory)
            : base(connectionFactory)
        {
        }

        // The following code does not make sense. Status codes are maintained in BAS and should never be modified from the service portal.
        /*
        public override PortalStatusItem Add(PortalStatusItem item)
        {
            string query =
                string.Format(@"INSERT INTO {0} ({1}, PortalStatusId) output inserted.*
                      VALUES(@serviceOrderId, @portalStatusId)", GetTableName(), GetIdColumnName());

            var param = new {@portalStatusId = item.StatusGuid, serviceOrderId = item.StatusGuid};

            return GetResults(query, param).Single();
        }

        public override bool Update(PortalStatusItem item)
        {
            string query = string.Format(@"UPDATE {0} SET PortalStatusId = @status WHERE {1} = @id", GetTableName(),
                GetIdColumnName());
            var param = new {status = item.StatusGuid, id = @item.StatusGuid};

            return Execute(query, param);
        }
        */
    }
}