﻿using CmsPublic.DataRepository;
using EISCS.Shop.DO.BAS.Dto;
using EISCS.Shop.DO.BAS.Interface;
using EISCS.Shop.DO.Repository;

namespace EISCS.Shop.DO.BAS.Repository
{
    [TableProperties(TableName = "JobPlanningStatus", PrimaryKeyName = "StatusGuid")]
    public class PortalStatusRepository : BaseRepository<PortalStatusItem>, IPortalStatusRespository
    {
        public PortalStatusRepository(IPortalDatabaseConnectionFactory connectionFactory)
            : base(connectionFactory)
        {
        }
    }
}