﻿using System.Collections.Generic;
using CmsPublic.DataRepository;
using EISCS.Shop.DO.BAS.Dto;
using EISCS.Shop.DO.BAS.Interface;
using EISCS.Shop.DO.Repository;

namespace EISCS.Shop.DO.BAS.Repository
{
    [TableProperties(TableName = "ProjectTable", PrimaryKeyName = "ProjectGuid")]
    public class ProjectRepository : BaseRepository<ProjectItem>, IProjectRepository
    {
        public ProjectRepository(IPortalDatabaseConnectionFactory connectionFactory) : base(connectionFactory)
        {
        }

        public IEnumerable<ProjectItem> GetProjectsByProjectIds(IEnumerable<string> projectIds)
        {
            string query = string.Format("{0} WHERE ProjectGuid IN @projectIds", GetDefaultSql());

            return GetResults(query, new {projectIds});
        }

        public IEnumerable<ProjectItem> GetProjectsByCustomerId(string customerId)
        {
            string query = string.Format("{0} WHERE CustomerGuid = @customerId", GetDefaultSql());

            return GetResults(query, new {customerId});
        }
    }
}