﻿using System.Collections.Generic;
using EISCS.Shop.DO.BAS.Dto;
using EISCS.Shop.DO.Interface;

namespace EISCS.Shop.DO.BAS.Repository
{
    public interface IServiceItemRepository : IBaseRepository<ServiceItem>
    {
        IEnumerable<ServiceItem> AjaxFindServiceItem(string searchQuery);
        IEnumerable<ServiceItem> AjaxFindServiceItemByCustomer(string searchQuery, string customerId, string customerAddressId = null);
        IEnumerable<ServiceItem> GetServiceItemsByCustomer(string customerId, string customerAddressId = null);
        IEnumerable<ServiceItem> GetServiceItems(int start, int end, string columnBeingSort, string sortDir, string searchValue, string customerGuid = null, string customerAddressId = null);
        IEnumerable<ServiceItem> GetServiceItemsByCustomerGroup(IEnumerable<string> customerGuidList, int start, int end, string columnBeingSort, string sortDir, string searchValue);
    }
}