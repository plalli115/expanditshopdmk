﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using CmsPublic.DataRepository;
using Dapper;
using System.Configuration;
using EISCS.Shop.DO.BAS.Dto;
using EISCS.Shop.DO.BAS.Interface;
using EISCS.Shop.DO.Dto;
using EISCS.Shop.DO.Repository;

namespace EISCS.Shop.DO.BAS.Repository
{
    [TableProperties(TableName = "ESMServiceOrderReg", PrimaryKeyName = "ServiceOrderGuid" /*"BASGuid"*/)]
    public class ServiceOrderRepository : BaseRepository<ServiceOrder>, IServiceOrderRepository
    {
        private const string SERVICE_ORDER_VALUES = @"@ServiceOrderGuid, @ServiceHeaderGuid, @ProjectGuid, @Priority, @ServiceOrderType, @ResponseTimeHours, @ResponseDate, @ResponseTime, @StartingDate, @StartingTime, @FinishingDate, @FinishingTime, @LineNumber, @Warranty, @WarrantyPartsPct, @WarrantyLaborPct, @ContractNo, @LocationofServiceItem, @ServiceItemNo, @FaultReasonCode, @FaultAreaCode, @SymptomCode, @FaultCode, @ResolutionCode, @ServiceItemGroupCode, @DocumentType, @ItemNo, @ItemType, @SerialNo, @NoofPreviousServices, @JobDescription, @Description, @RepairStatusCode, @CustomerGuid, @AddressGuid, @AddressType, @CompanyName, @Address, @Address2, @ZipCode, @EmailAddress, @CityName, @YourReference, @UserGuid, @ContactCustomer, @ContactRegarding, @readOnly, newid(), @BASReceivedTime, @BASInjected, 0, @BASSyncCommand, @BASSyncState, @BillToCustomerGuid, @BilltoCompanyName, @BilltoAddress, @BilltoAddress2, @BilltoZipCode, @BilltoCityName, @BilltoContactName, @BilltoCountyName, @BilltoCountryGuid, @CountryGuid, @CurrencyGuid, @RecordAction, @JobPlanningGuid, @CountyName, @BilltoCompanyName2, @ContactName, @CompanyName2, @FaxNo, @PhoneNo, @PhoneNo2, @ShiptoContactName, @ShiptoCountryGuid, @ShiptoCountyName, @ShiptoAddress, @ShiptoAddress2, @ShiptoCityName, @ShiptoEmailAddress, @ShiptoCompanyName, @ShiptoCompanyName2, @ShiptoFaxNo, @ShiptoPhoneNo, @ShiptoPhoneNo2, @ShiptoZipCode, @BASVersion, @DepartmentGuid, @ApprovedDate, @Approved, @ApprovedBy, @OrderedByContactName, @IsUrgent, @ShiptoDoCallBeforeVisit, @IsLockedInTime, @IsLockedToUser, @CompleteAction, @ResponsibleUserGuid, @SalesPersonGuid, @SKSReportIsRequired";
        private const string SERVICE_ORDER_COMMON_FIELDS_GENERIC = @"@ServiceOrderGuid, @ServiceHeaderGuid, @Priority, @ServiceOrderType, @ResponseTimeHours, @ResponseDate, @ResponseTime, @StartingDate, @StartingTime, @FinishingDate, @FinishingTime, @LineNumber, @Warranty, @WarrantyPartsPct, @WarrantyLaborPct, @ContractNo, @LocationofServiceItem, @ServiceItemNo, @FaultReasonCode, @FaultAreaCode, @SymptomCode, @FaultCode, @ResolutionCode, @ServiceItemGroupCode, @DocumentType, @ItemNo, @SerialNo, @NoofPreviousServices, @JobDescription, @Description, @RepairStatusCode, @CustomerGuid, @CompanyName, @Address, @Address2, @ZipCode, @EmailAddress, @CityName, @YourReference, @BillToCustomerGuid, @BilltoCompanyName, @BilltoAddress, @BilltoAddress2, @BilltoZipCode, @BilltoCityName, @BilltoContactName, @BilltoCountyName, @BilltoCountryGuid, @CountryGuid, @CountyName, @BilltoCompanyName2, @ContactName, @CompanyName2, @FaxNo, @PhoneNo, @PhoneNo2, @ShiptoContactName, @ShiptoCountryGuid, @ShiptoCountyName, @ShiptoAddress, @ShiptoAddress2, @ShiptoCityName, @ShiptoEmailAddress, @ShiptoCompanyName, @ShiptoCompanyName2, @ShiptoFaxNo, @ShiptoPhoneNo, @ShiptoPhoneNo2, @ShiptoZipCode, @DepartmentGuid, @OrderedByContactName, @IsUrgent, @ShiptoDoCallBeforeVisit, @IsLockedInTime, @IsLockedToUser";
        private const string SERVICE_ORDER_COMMON_FIELDS_NAV = @"@ServiceOrderGuid, @ServiceHeaderGuid, @Priority, @ServiceOrderType, @ResponseTimeHours, @ResponseDate, @ResponseTime, @StartingDate, @StartingTime, @FinishingDate, @FinishingTime, @LineNumber, @Warranty, @WarrantyPartsPct, @WarrantyLaborPct, @ContractNo, @LocationofServiceItem, @ServiceItemNo, @FaultReasonCode, @FaultAreaCode, @SymptomCode, @FaultCode, @ResolutionCode, @ServiceItemGroupCode, @DocumentType, @ItemNo, @SerialNo, @NoofPreviousServices, @JobDescription, @Description, @RepairStatusCode, @CustomerGuid, @CompanyName, @Address, @Address2, @ZipCode, @EmailAddress, @CityName, @YourReference, @BillToCustomerGuid, @BilltoCompanyName, @BilltoAddress, @BilltoAddress2, @BilltoZipCode, @BilltoCityName, @BilltoContactName, @BilltoCountyName, @BilltoCountryGuid, @CountryGuid, @CountyName, @BilltoCompanyName2, @ContactName, @CompanyName2, @FaxNo, @PhoneNo, @PhoneNo2, @ShiptoContactName, @ShiptoCountryGuid, @ShiptoCountyName, @ShiptoAddress, @ShiptoAddress2, @ShiptoCityName, @ShiptoEmailAddress, @ShiptoCompanyName, @ShiptoCompanyName2, @ShiptoFaxNo, @ShiptoPhoneNo, @ShiptoPhoneNo2, @ShiptoZipCode, @DepartmentGuid, @IsUrgent";
        private const string SERVICE_ORDER_COMMON_FIELDS_AX = @"@ServiceOrderGuid, @ServiceHeaderGuid, @ProjectGuid, @Priority, @ServiceOrderType, @ResponseDate, @ResponseTime, @LineNumber, @ContractNo, @ItemNo, @JobDescription, @Description, @RepairStatusCode, @CustomerGuid, @CompanyName, @Address, @Address2, @ZipCode, @CityName, @CountryGuid, @CountyName, @ShiptoContactName, @ShiptoCountryGuid, @ShiptoCountyName, @ShiptoAddress, @ShiptoCityName, @ShiptoCompanyName, @ShiptoEmailAddress, @ShiptoPhoneNo, @ShiptoZipCode, @DepartmentGuid, @IsUrgent, @FinishingDate, @FinishingTime";

        private string SERVICE_ORDER_COMMON_FIELDS = "";

        private const bool SELECT_ARCHIVED_ITEMS = true;
        private const bool SELECT_ACTIVE_ITEMS = false;


        private IEnumerable<ServiceOrder> LoadOrderPlanningStatuses(IEnumerable<ServiceOrder> serviceOrderList)
        {
            /*
            foreach (ServiceOrder serviceOrderItem in serviceOrderList)
            {
                serviceOrderItem.StatusCode = GetServiceOrderStatus(serviceOrderItem.ServiceOrderGuid);
            }
            */
            return serviceOrderList;
        }

        public ServiceOrderRepository(IPortalDatabaseConnectionFactory connectionFactory)
            : base(connectionFactory)
        {
            switch (ConfigurationManager.AppSettings["BackendType"])
            {
                case "NAV":
                            SERVICE_ORDER_COMMON_FIELDS = SERVICE_ORDER_COMMON_FIELDS_NAV;
                            break;
                case "AX":
                case "AX2012":
                            SERVICE_ORDER_COMMON_FIELDS = SERVICE_ORDER_COMMON_FIELDS_AX;
                            break;
                default:
                            SERVICE_ORDER_COMMON_FIELDS = SERVICE_ORDER_COMMON_FIELDS_GENERIC;
                            break;
                        
            }
        }

        public override ServiceOrder GetById(string serviceOrderGuid)
        {
            string query = string.Format("{0} AND ServiceOrderGuid = @orderId", GetServiceOrderSql(ServiceOrderSearchType.All));
            var param = new { orderId = serviceOrderGuid };
            return LoadOrderPlanningStatuses(GetResults(query, param)).FirstOrDefault();
        }

        // TODO: Remove this
        public IEnumerable<ServiceOrder> GetAllServiceOrders(ServiceOrderSearchType st = ServiceOrderSearchType.Active)
        {
            string query = GetServiceOrderSql(st);
            return LoadOrderPlanningStatuses(GetResults(query));
        }

        public IEnumerable<ServiceOrder> GetAllServiceOrders(int start, int end, string columnBeingSort, string sortDir, string searchValue, ServiceOrderSearchType st = ServiceOrderSearchType.Active)
        {
            var query = GetServiceOrderSql(start, end, columnBeingSort, sortDir, searchValue, st);
            return LoadOrderPlanningStatuses(GetResults(query));
        }

        public IEnumerable<ServiceOrder> GetAllByCustomerGuid(string customerId, int start, int end, string columnBeingSort, string sortDir, string searchValue, ServiceOrderSearchType st = ServiceOrderSearchType.Active)
        {
            var query = GetServiceOrderSql(start, end, columnBeingSort, sortDir, searchValue, st).Replace(") temp", "AND (CustomerGuid = @customerGuid)) temp");
            var param = new { customerGuid = customerId };
            return LoadOrderPlanningStatuses(GetResults(query, param));
        }

        // TODO: Remove this
        public IEnumerable<ServiceOrder> GetAllByCustomerGuidList(IEnumerable<string> customerGuidList, ServiceOrderSearchType st = ServiceOrderSearchType.Active)
        {
            string query = string.Format("{0} AND CustomerGuid IN @customerGuidList ORDER BY BASReceivedTime DESC", GetServiceOrderSql(st));
            var param = new {customerGuidList};

            return LoadOrderPlanningStatuses(GetResults(query, param));
        }

        // TODO: Remove this
        public IEnumerable<ServiceOrder> GetAllByCustomerGuid(string customerId, ServiceOrderSearchType st = ServiceOrderSearchType.Active)
        {
            string query = string.Format("{0} AND (CustomerGuid = @customerGuid) ORDER BY BASReceivedTime DESC", GetServiceOrderSql(st));
            var param = new {customerGuid = customerId};
            return LoadOrderPlanningStatuses(GetResults(query, param));
        }

        // Todo: Remove this
        public IEnumerable<ServiceOrder> GetAllByProjectIds(IEnumerable<string> projectIds, ServiceOrderSearchType st = ServiceOrderSearchType.Active)
        {
            string query = string.Format("{0} WHERE ProjectGuid IN @projectIds", GetDefaultSql());
            var param = new {projectIds};

            return LoadOrderPlanningStatuses(GetResults(query, param));
        }

        public string GetServiceOrderGuidByBasGuid(string basGuid)
        {
            using (IDbConnection connetion = GetConnection())
            {
                string query = string.Format("SELECT ServiceOrderGuid FROM {0} WITH (NOLOCK) WHERE {1} = @basGuid", GetTableName(), GetIdColumnName());
                IEnumerable<string> id = connetion.Query<string>(query, new {basGuid});
                return id.FirstOrDefault();
            }
        }

        // TODO: Remove this
        public IEnumerable<ServiceOrder> GetAllByUserGuid(string userGuid, ServiceOrderSearchType st = ServiceOrderSearchType.Active)
        {
            string query = string.Format("{0} AND UserGuid = @userGuid ORDER BY BASReceivedTime DESC", GetServiceOrderSql(st));
            var param = new {userGuid};

            return LoadOrderPlanningStatuses(GetResults(query, param));
        }

        public IEnumerable<ServiceOrder> GetAllArchivedServiceOrders()
        {
            string query = GetServiceOrderSql(ServiceOrderSearchType.Closed);

            return LoadOrderPlanningStatuses(GetResults(query));
        }

        public IEnumerable<ServiceOrder> GetAllArchivedByCustomerGuidList(IEnumerable<string> customerGuidList)
        {
            string query = string.Format("{0} AND CustomerGuid IN @customerGuidList", GetServiceOrderSql(ServiceOrderSearchType.Closed));
            var param = new {customerGuidList};

            return LoadOrderPlanningStatuses(GetResults(query, param));
        }

        public IEnumerable<ServiceOrder> GetAllArchivedByCustomerGuid(string customerId)
        {
            string query = string.Format("{0} AND CustomerGuid = @customerGuid", GetServiceOrderSql(ServiceOrderSearchType.Closed));
            var param = new {customerGuid = customerId};

            return LoadOrderPlanningStatuses(GetResults(query, param));
        }

        public IEnumerable<ServiceOrder> GetAllArchivedByProjectIds(IEnumerable<string> projectIds)
        {
            string query = string.Format("{0}  AND ProjectGuid IN @projectIds", GetServiceOrderSql(ServiceOrderSearchType.Closed));
            var param = new {projectIds};

            return LoadOrderPlanningStatuses(GetResults(query, param));
        }

        public override ServiceOrder Add(ServiceOrder item, bool useOutputClause = true)
        {
            using (IDbConnection connection = GetConnection())
            {
                string sql =
                    string.Format(@"INSERT INTO {0} (ServiceOrderGuid, ServiceHeaderGuid, ProjectGuid, Priority, ServiceOrderType, ResponseTimeHours, ResponseDate, ResponseTime, StartingDate, StartingTime, FinishingDate, FinishingTime, LineNumber, Warranty, WarrantyPartsPct, WarrantyLaborPct, ContractNo, LocationofServiceItem, ServiceItemNo, FaultReasonCode, FaultAreaCode, SymptomCode, FaultCode, ResolutionCode, ServiceItemGroupCode, DocumentType, ItemNo, ItemType, SerialNo, NoofPreviousServices, JobDescription, Description, RepairStatusCode, CustomerGuid, AddressGuid, AddressType, CompanyName, Address, Address2, ZipCode, EmailAddress, CityName, YourReference, UserGuid, ContactCustomer, ContactRegarding, readonly, client_guid, BASReceivedTime, BASInjected, BASClient, BASSyncCommand, BASSyncState, BillToCustomerGuid, BilltoCompanyName, BilltoAddress, BilltoAddress2, BilltoZipCode, BilltoCityName, BilltoContactName, BilltoCountyName, BilltoCountryGuid, CountryGuid, CurrencyGuid, RecordAction, JobPlanningGuid, CountyName, BilltoCompanyName2, ContactName, CompanyName2, FaxNo, PhoneNo, PhoneNo2, ShiptoContactName, ShiptoCountryGuid, ShiptoCountyName, ShiptoAddress, ShiptoAddress2, ShiptoCityName, ShiptoEmailAddress, ShiptoCompanyName, ShiptoCompanyName2, ShiptoFaxNo, ShiptoPhoneNo, ShiptoPhoneNo2, ShiptoZipCode, BASVersion, DepartmentGuid, ApprovedDate, Approved, ApprovedBy, OrderedByContactName, IsUrgent, ShiptoDoCallBeforeVisit, IsLockedInTime, IsLockedToUser, CompleteAction, ResponsibleUserGuid, SalesPersonGuid, SKSReportIsRequired) VALUES({1}); SELECT CAST(SCOPE_IDENTITY() AS INT)", GetTableName(), SERVICE_ORDER_VALUES);
                IEnumerable<int> id = connection.Query<int>(sql, new
                    {
                        item.ServiceOrderGuid,
                        item.ServiceHeaderGuid,
                        item.ProjectGuid,
                        item.Priority,
                        item.ServiceOrderType,
                        item.ResponseTimeHours,
                        item.ResponseDate,
                        item.ResponseTime,
                        item.StartingDate,
                        item.StartingTime,
                        item.FinishingDate,
                        item.FinishingTime,
                        item.LineNumber,
                        item.Warranty,
                        item.WarrantyPartsPct,
                        item.WarrantyLaborPct,
                        item.ContractNo,
                        item.LocationofServiceItem,
                        item.ServiceItemNo,
                        item.FaultReasonCode,
                        item.FaultAreaCode,
                        item.SymptomCode,
                        item.FaultCode,
                        item.ResolutionCode,
                        item.ServiceItemGroupCode,
                        item.DocumentType,
                        item.ItemNo,
                        item.ItemType,
                        item.SerialNo,
                        item.NoofPreviousServices,
                        item.JobDescription,
                        item.Description,
                        item.RepairStatusCode,
                        item.CustomerGuid,
                        item.AddressGuid,
                        item.AddressType,
                        item.CompanyName,
                        item.Address,
                        item.Address2,
                        item.ZipCode,
                        item.EmailAddress,
                        item.CityName,
                        item.YourReference,
                        item.UserGuid,
                        item.ContactCustomer,
                        item.ContactRegarding,
                        readOnly = item.ReadOnly,
                        item.BASReceivedTime,
                        item.BASInjected,
                        item.BASSyncCommand,
                        item.BASSyncState,
                        item.BillToCustomerGuid,
                        item.BilltoCompanyName,
                        item.BilltoAddress,
                        item.BilltoAddress2,
                        item.BilltoZipCode,
                        item.BilltoCityName,
                        item.BilltoContactName,
                        item.BilltoCountyName,
                        item.BilltoCountryGuid,
                        item.CountryGuid,
                        item.CurrencyGuid,
                        item.RecordAction,
                        item.JobPlanningGuid,
                        item.CountyName,
                        item.BilltoCompanyName2,
                        item.ContactName,
                        item.CompanyName2,
                        item.FaxNo,
                        item.PhoneNo,
                        item.PhoneNo2,
                        item.ShiptoContactName,
                        item.ShiptoCountryGuid,
                        item.ShiptoCountyName,
                        item.ShiptoAddress,
                        item.ShiptoAddress2,
                        item.ShiptoCityName,
                        item.ShiptoEmailAddress,
                        item.ShiptoCompanyName,
                        item.ShiptoCompanyName2,
                        item.ShiptoFaxNo,
                        item.ShiptoPhoneNo,
                        item.ShiptoPhoneNo2,
                        item.ShiptoZipCode,
                        item.BASVersion,
                        item.DepartmentGuid,
                        item.ApprovedDate,
                        item.Approved,
                        item.ApprovedBy,
                        item.OrderedByContactName,
                        item.IsUrgent,
                        item.ShiptoDoCallBeforeVisit,
                        item.IsLockedInTime,
                        item.IsLockedToUser,
                        item.CompleteAction,
                        item.ResponsibleUserGuid,
                        item.SalesPersonGuid,
                        item.SKSReportIsRequired
                    });

                item.BASGuid = id.SingleOrDefault();

                return item;
            }
        }

        public override bool Update(ServiceOrder item)
        {
            int affectedRows;
            using (IDbConnection connection = GetConnection())
            {
                string query =
                    string.Format(
                        @"UPDATE  {0} SET ItemType=@itemType, ServiceItemNo=@serviceItemNo, ItemNo=@itemNo, Description=@description, ProjectGuid=@projectGuid, ServiceOrderType=@serviceOrderType, DepartmentGuid=@departmentGuid, RepairStatusCode = @repairStatusCode, BilltoCompanyName2 = @billToCompanyName2, ShiptoCompanyName2 = @shipToCompanyName2, ShiptoContactName = @shiptoContactName, ShiptoAddress = @shiptoAddress, ShiptoCityName = @shiptoCityName, ShiptoCompanyName = @shiptoCompanyName, ShiptoZipCode = @shiptoZipCode, BilltoContactName = @billToContactName, JobDescription = @jobDescription, ShiptoEmailAddress = @shiptoEmailAddress, ResponseDate = @responseDate, StartingDate = @startingDate, FinishingDate = @finishingDate, ShiptoPhoneNo=@shiptoPhoneNo, ShiptoPhoneNo2=@shiptoPhoneNo2, BASVersion=BASVersion+1 WHERE ServiceOrderGuid = @serviceOrderGuid AND RecordAction='NEW'",
                        GetTableName());
                // If values are not specified - possibly due to access restrictions - leave them out
                if (item.ServiceItemNo == null)
                    query = query.Replace("ServiceItemNo=@serviceItemNo,", "");
                if (item.ProjectGuid == null)
                    query = query.Replace("ProjectGuid=@projectGuid,", "");
                if (item.ItemNo == null)
                    query = query.Replace("ItemNo=@itemNo,", "");
                if (item.ServiceOrderType == null)
                    query = query.Replace("ServiceOrderType=@serviceOrderType,", "");
                if (item.DepartmentGuid == null)
                    query = query.Replace("DepartmentGuid=@departmentGuid,", "");


                affectedRows = connection.Execute(query, new
                    {
                        itemType = item.ItemType,
                        serviceItemNo = item.ServiceItemNo,
                        itemNo = item.ItemNo,
                        description = item.Description,
                        projectGuid = item.ProjectGuid,
                        serviceOrderType = item.ServiceOrderType,
                        departmentGuid = item.DepartmentGuid,
                        repairStatusCode = item.RepairStatusCode,
                        billToCompanyName2 = item.BilltoCompanyName2,
                        shipToCompanyName2 = item.ShiptoCompanyName2,
                        shiptoContactName = item.ShiptoContactName,
                        shiptoAddress = item.ShiptoAddress,
                        shiptoCityName = item.ShiptoCityName,
                        shiptoCompanyName = item.ShiptoCompanyName,
                        shiptoZipCode = item.ShiptoZipCode,
                        billToContactName = item.BilltoContactName,
                        jobDescription = item.JobDescription,
                        shiptoEmailAddress = item.ShiptoEmailAddress,
                        shiptoPhoneNo = item.ShiptoPhoneNo,
                        shiptoPhoneNo2 = item.ShiptoPhoneNo2,
                        serviceOrderGuid = item.ServiceOrderGuid,
                        responseDate = item.ResponseDate,
                        startingDate = item.StartingDate,
                        finishingDate = item.FinishingDate
                    });
            }
            return (affectedRows > 0);
        }

        private string GetServiceOrderSql(ServiceOrderSearchType st = ServiceOrderSearchType.Active)
        {
//            return string.Format(@"SELECT *
//                                   FROM {0} so                                   
//                                   WHERE {1} = (
//			                                    SELECT max ({1})
//			                                    FROM {0}
//			                                    WHERE ServiceHeaderGuid = so.ServiceHeaderGuid
//                                                )", GetTableName(), GetIdColumnName());
/*
            var repairStatusCode = (requestArchivedServiceOrders) ? "= 'COMPLETE'" : "!= 'COMPLETE'";
            return string.Format(@"SELECT * 
                                   FROM {0} WITH (NOLOCK)
                                   WHERE {1} IN
                                   (
                                   SELECT MAX({1}) AS Id
                                   FROM {0} WITH (NOLOCK)
                                   WHERE RepairStatusCode {2}
                                   GROUP BY ServiceHeaderGuid
                                   )", GetTableName(), GetIdColumnName(), repairStatusCode);
*/
            
            string statusFilter;
            switch(st) 
            {
                case ServiceOrderSearchType.Active:
                    statusFilter = "AND (IsNull(StatusCode,'') != 'COMPLETED')";
                    break;
                case ServiceOrderSearchType.Closed:
                    statusFilter = "AND (IsNull(StatusCode,'') = 'COMPLETED')";
                    break;
                default:
                    statusFilter = "";
                    break;
            }
            return string.Format(@"
                                    SELECT * FROM (
	                                    SELECT {0}, ESMServiceOrder.ResponseDate AS BASReceivedTime, 0 AS BASGuid, 0 AS ItemType FROM ESMServiceOrder
	                                      LEFT JOIN ESMServiceOrderReg ON (ESMServiceOrder.ServiceOrderGuid=ESMServiceOrderReg.ServiceOrderGuid)
	                                      WHERE ESMServiceOrderReg.ServiceOrderGuid IS NULL
	                                    UNION ALL
	                                    SELECT {1}, ESMServiceOrderReg.BASReceivedTime, ESMServiceOrderReg.BASGuid, ESMServiceOrderReg.ItemType FROM ESMServiceOrderReg
	                                      INNER JOIN (
		                                      SELECT Max(BASGuid) AS BASGuid FROM ESMServiceOrderReg 
			                                    GROUP BY ESMServiceOrderReg.ServiceOrderGuid
	                                      ) T1 ON (ESMServiceOrderReg.BASGuid = T1.BASGuid)
                                    ) AS ServiceOrder

                                    LEFT JOIN
	                                    (SELECT StatusGuid AS StatusCode, Guid_X AS ServiceOrderId, JobPlanningType FROM 
		                                    (select Guid_X, JobPlanningType, MAX(Precedence) as Precedence from JobPlanning WITH (NOLOCK) inner join JobPlanningStatus WITH (NOLOCK) on (JobPlanning.StatusGuid=JobPlanningStatus.StatusGuid)
		                                    group by JobPlanning.Guid_X, JobPlanningType) T1
		                                    inner join JobPlanningStatus ON (T1.Precedence = JobPlanningStatus.Precedence)
	                                    ) AS Status
                                    ON (ServiceOrder.ServiceOrderGuid=Status.ServiceOrderId)
                                    WHERE (IsNull(Status.JobPlanningType,3) IN (1,3)) {2}
                                  ", SERVICE_ORDER_COMMON_FIELDS.Replace("@", "ESMServiceOrder."),
                                     SERVICE_ORDER_COMMON_FIELDS.Replace("@", "ESMServiceOrderReg."),
                                     statusFilter);

        }

        private string GetServiceOrderSql(int start, int end, string columnBeingSort, string sortDir, string searchValue, ServiceOrderSearchType st)
        {
            string statusFilter;
            switch (st)
            {
                case ServiceOrderSearchType.Active:
                    statusFilter = "AND (IsNull(StatusCode,'') != 'COMPLETED')";
                    break;
                case ServiceOrderSearchType.Closed:
                    statusFilter = "AND (IsNull(StatusCode,'') = 'COMPLETED')";
                    break;
                default:
                    statusFilter = "";
                    break;
            }

            var searchText = string.Empty;
            if (!string.IsNullOrEmpty(searchValue))
            {
                searchValue = searchValue.Replace("*", "%");
                searchText = "AND ((LOWER(ServiceOrderGuid) LIKE LOWER('" + searchValue +
                    "%')) OR (LOWER(JobDescription) LIKE LOWER('" + searchValue +
                    "%')) OR (LOWER(Description) LIKE LOWER('" + searchValue +
                    "%')) OR (LOWER(ShiptoCompanyName) LIKE LOWER('" + searchValue +
                    "%')) OR (LOWER(ShiptoContactName ) LIKE LOWER('" + searchValue +
                    "%')) OR (LOWER(ShiptoAddress) LIKE LOWER('" + searchValue +
                    "%')) OR (LOWER(ShiptoCityName) LIKE LOWER('" + searchValue +
                    "%')) OR (LOWER(ShiptoZipCode) LIKE LOWER('" + searchValue +
                    "%')) OR (LOWER(CompanyName) LIKE LOWER('" + searchValue +
                    "%')) OR (LOWER(Address) LIKE LOWER('" + searchValue +
                    "%')) OR (LOWER(ZipCode) LIKE LOWER('" + searchValue +
                    "%')) OR (LOWER(CityName) LIKE LOWER('" + searchValue + 
                    "%')))";
            }

            return String.Format(@"
                                    SELECT * FROM (
	                                    SELECT ROW_NUMBER() OVER (ORDER BY {6} {7}, ServiceOrderGuid, BASGuid) AS RowID, * FROM (
		                                    SELECT {0}, ESMServiceOrder.ResponseDate AS BASReceivedTime, 0 AS BASGuid FROM ESMServiceOrder
			                                    LEFT JOIN ESMServiceOrderReg ON (ESMServiceOrder.ServiceOrderGuid=ESMServiceOrderReg.ServiceOrderGuid)
			                                    WHERE ESMServiceOrderReg.ServiceOrderGuid IS NULL
		                                    UNION ALL
		                                    SELECT {1}, ESMServiceOrderReg.BASReceivedTime, ESMServiceOrderReg.BASGuid FROM ESMServiceOrderReg
			                                    INNER JOIN (
				                                    SELECT Max(BASGuid) AS BASGuid FROM ESMServiceOrderReg 
				                                    GROUP BY ESMServiceOrderReg.ServiceOrderGuid
			                                    ) T1 ON (ESMServiceOrderReg.BASGuid = T1.BASGuid)
	                                    ) AS ServiceOrder LEFT JOIN
		                                    (SELECT StatusGuid AS StatusCode, Guid_X AS ServiceOrderId, JobPlanningType FROM 
			                                    (select Guid_X, JobPlanningType, MAX(Precedence) as Precedence from JobPlanning WITH (NOLOCK) inner join JobPlanningStatus WITH (NOLOCK) on (JobPlanning.StatusGuid=JobPlanningStatus.StatusGuid)
			                                    group by JobPlanning.Guid_X, JobPlanningType) T1
			                                    inner join JobPlanningStatus ON (T1.Precedence = JobPlanningStatus.Precedence)
		                                    ) AS Status
	                                    ON (ServiceOrder.ServiceOrderGuid=Status.ServiceOrderId)
	                                    WHERE (IsNull(Status.JobPlanningType,3) IN (1,3)) {2} {3}
                                    ) temp
                                    WHERE RowID BETWEEN {4} AND {5} ORDER BY RowID",
                                SERVICE_ORDER_COMMON_FIELDS.Replace("@", "ESMServiceOrder."),
                                SERVICE_ORDER_COMMON_FIELDS.Replace("@", "ESMServiceOrderReg."),
                                statusFilter,
                                searchText,
                                start,
                                end,
                                columnBeingSort,
                                sortDir);
        }

        public string GetServiceOrderStatus(string serviceOrderId, int jobPlanningType = 3)
        {
            using (IDbConnection connetion = GetConnection())
            {
                string query = GetPlanningStatusSql() + " WHERE Guid_X=@serviceOrderId AND JobPlanningType=@jobPlanningType";
                IEnumerable<string> id = connetion.Query<string>(query, new {serviceOrderId, jobPlanningType });
                return id.FirstOrDefault();
            }
        }

        public string GetServiceOrderCommonFields()
        {
            return SERVICE_ORDER_COMMON_FIELDS;
        }

        private string GetPlanningStatusSql()
        {
            return @"select StatusGuid AS StatusCode, Guid_X AS ServiceOrderId, JobPlanningType FROM 
                    (select Guid_X, JobPlanningType, MAX(Precedence) as Precedence from JobPlanning WITH (NOLOCK) inner join JobPlanningStatus WITH (NOLOCK) on (JobPlanning.StatusGuid=JobPlanningStatus.StatusGuid)
                    group by JobPlanning.Guid_X, JobPlanningType) T1
                    inner join JobPlanningStatus ON (T1.Precedence = JobPlanningStatus.Precedence)";
        }


    }
}