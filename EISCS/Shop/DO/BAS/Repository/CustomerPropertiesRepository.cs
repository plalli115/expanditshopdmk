﻿using System.Linq;
using CmsPublic.DataRepository;
using EISCS.Shop.DO.BAS.Dto;
using EISCS.Shop.DO.BAS.Interface;
using EISCS.Shop.DO.Repository;

namespace EISCS.Shop.DO.BAS.Repository
{
    [TableProperties(TableName = "CustomerProperties", PrimaryKeyName = "Id")]
    public class CustomerPropertiesRepository : BaseRepository<CustomerPropertyItem>, ICustomerPropertiesRepository
    {
        public CustomerPropertiesRepository(IShopDatabaseConnectionFactory connectionFactory)
            : base(connectionFactory)
        {
        }

        public override string AllColumns()
        {
            return "Id, CustomerId, RelationTableId as PropertyId, RelationTableTypeId as RelationTableType";
        }

        public override CustomerPropertyItem Add(CustomerPropertyItem item, bool useOutputClause = true)
        {
            string sql = string.Format("insert into {0} (CustomerId, RelationTableId, RelationTableTypeId) output Inserted.* values (@CustomerId, @PropertyId, @RelationTableType)",
                GetTableName());
            return GetResults(sql, item).Single();
        }
    }
}