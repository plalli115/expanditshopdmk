﻿using System.Collections.Generic;
using System.Linq;
using CmsPublic.DataRepository;
using EISCS.ExpandITFramework.Util;
using EISCS.Shop.DO.BAS.Dto;
using EISCS.Shop.DO.Repository;

namespace EISCS.Shop.DO.BAS.Repository
{
    [TableProperties(TableName = "ESMServiceItem", PrimaryKeyName = "ServiceItemNo")]
    public class ServiceItemRepository : BaseRepository<ServiceItem>, IServiceItemRepository
    {
        public ServiceItemRepository(IPortalDatabaseConnectionFactory connectionFactory)
            : base(connectionFactory)
        {
        }

        public override string AllColumns()
        {
            return
                "*, ServiceItemNo as ServiceItemId, CustomerGuid as CustomerId, AddressGuid as AddressId";
        }

        public IEnumerable<ServiceItem> AjaxFindServiceItem(string searchQuery)
        {
            var query = string.Format(@"{0} 
                                        WHERE ServiceItemNo LIKE '%' + @searchQuery + '%'
                                        OR ItemNo LIKE '%' + @searchQuery + '%'
                                        OR SerialNo LIKE '%' + @searchQuery + '%'
                                        OR lower(Description) LIKE lower('%' + @searchQuery + '%')", GetDefaultSql());
            var param = new {searchQuery};

            return GetResults(query, param);
        }

        public IEnumerable<ServiceItem> AjaxFindServiceItemByCustomer(string searchQuery, string customerId, string customerAddressId = null)
        {
            var query = string.Format(@"{0} 
                                        WHERE ServiceItemNo LIKE '%' + @searchQuery + '%'
                                        OR (ItemNo LIKE '%' + @searchQuery + '%'
                                        OR SerialNo LIKE '%' + @searchQuery + '%'
                                        OR lower(Description) LIKE lower('%' + @searchQuery + '%')) AND CustomerGuid = @customerId ", GetDefaultSql());
            if (!string.IsNullOrEmpty(customerAddressId))
            {
                query = string.Format("{0} AND AddressGuid = @customerAddressId", query);
            }
            var param = new {searchQuery, customerId, customerAddressId};

            return GetResults(query, param);
        }

        public IEnumerable<ServiceItem> GetByAddressId(string addressId)
        {
            string query = string.Format("{0} where AddressGuid = @addressId", GetDefaultSql());

            return GetResults(query, new {addressId});
        }

        public IEnumerable<ServiceItem> GetServiceItemsByCustomer(string customerId, string customerAddressId = null)
        {
            var query = string.Format(@"{0} WHERE  CustomerGuid = @customerId ", GetDefaultSql());
            if (!string.IsNullOrEmpty(customerAddressId))
            {
                query = string.Format("{0} AND AddressGuid = @customerAddressId", query);
            }
            var param = new { customerId, customerAddressId };

            return GetResults(query, param);
        }

        public IEnumerable<ServiceItem> GetServiceItems(int start, int end, string columnBeingSort, string sortDir, string searchValue, string customerGuid = null, string customerAddressId = null)
        {
            var searchString = string.Empty;
            if (!string.IsNullOrEmpty(searchValue))
            {
                searchString = string.Format(@" WHERE lower(ServiceItemNo) LIKE lower('%" + searchValue + "%')" +
                                        "OR ItemNo LIKE '%" + searchValue + "%'" +
                                        "OR lower(SerialNo) LIKE lower('%" + searchValue + "%')" +
                                        "OR lower(Description) LIKE lower('%" + searchValue + "%')");
                searchString = searchString.Replace("*", "%");
            }

            if (!string.IsNullOrEmpty(customerGuid))
            {
                if (string.IsNullOrEmpty(searchString))
                    searchString = @" WHERE ";
                else
                    searchString += @" AND ";
                searchString += string.Format(@"CustomerGuid = @customerGuid");
            }

            if (customerAddressId != null)
            {
                if (string.IsNullOrEmpty(searchString))
                    searchString = @" WHERE ";
                else
                    searchString += @" AND ";
                searchString += string.Format(@"AddressGuid = @customerAddressId");
            }
            
            var query = string.Format(@"SELECT * FROM (" +
                                        "SELECT ROW_NUMBER() OVER (ORDER BY {0} " + sortDir + ", ServiceItemNo) AS RowID, * " +
                                        "FROM {2} " +
                                        "{3}) temp " +
                                        "WHERE RowID BETWEEN {4} AND {5}",
                                        columnBeingSort,
                                        sortDir,
                                        GetTableName(),
                                        searchString,
                                        start,
                                        end);

            var param = new {customerGuid, customerAddressId};
            return GetResults(query, param);
        }

        public IEnumerable<ServiceItem> GetServiceItemsByCustomerGroup(IEnumerable<string> customerGuidList, int start, int end, string columnBeingSort, string sortDir, string searchValue)
        {
            var searchString = string.Empty;
            if (!string.IsNullOrEmpty(searchValue))
            {
                searchString = string.Format(@" WHERE lower(ServiceItemNo) LIKE lower('%" + searchValue + "%')" +
                                        "OR ItemNo LIKE '%" + searchValue + "%'" +
                                        "OR lower(SerialNo) LIKE lower('%" + searchValue + "%')" +
                                        "OR lower(Description) LIKE lower('%" + searchValue + "%')");
                searchString = searchString.Replace("*", "%");
            }

            if (string.IsNullOrEmpty(searchString))
                searchString = @" WHERE (";
            else
                searchString += @" AND (";
            
            searchString = customerGuidList.Aggregate(searchString, (current, customer) => current + string.Format(@" CustomerGuid = {0} OR ", customer));

            searchString = searchString.Remove(searchString.Length - 3);
            searchString += ")";

            var query = string.Format(@"SELECT * FROM (" +
                                        "SELECT ROW_NUMBER() OVER (ORDER BY {0} " + sortDir + ", ServiceItemNo) AS RowID, * " +
                                        "FROM {2} " +
                                        "{3}) temp " +
                                        "WHERE RowID BETWEEN {4} AND {5}",
                                        columnBeingSort,
                                        sortDir,
                                        GetTableName(),
                                        searchString,
                                        start,
                                        end);

            return GetResults(query);
        }
    }
}