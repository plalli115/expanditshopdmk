﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using EISCS.Shop.BO.BAS;

namespace EISCS.Shop.DO.BAS
{
    [TableProperties(TableName = "ESMServiceCommentLineReg", PrimaryKeyName = "BASGuid")]
    public class ServiceOrderCommentLineItemRepository : BaseRepository<ServiceOrderCommentLineItem>,
                                                         IServiceOrderCommentLineItemRepository
    {
        private const string COLUMN_NAMES =
            @"ServiceOrderGuid, CommentType, CommentTypeName, ServiceOrderNumber, ServiceItemLineNumber, CommentLineNumber, CommentLineGuid,
                                                CommentText, CommentDate, CommentTypeText, RecordAction, JobPlanningGuid, BASReceivedTime, BASInjected, BASClient, BASSyncCommand,
                                                BASSyncState, readonly, client_guid, CommentTextExternal, Approved, ApprovedDate, ApprovedBy, ExpConversionStatus, BackendAreaId,
                                                ServiceOrderGuid_Inject";

        private const string COLUMN_VALUES =
            @"@ServiceOrderGuid, @CommentType, @CommentTypeName, @ServiceOrderNumber, @ServiceItemLineNumber, @CommentLineNumber, newid(),
                                                @CommentText, @CommentDate, @CommentTypeText, @RecordAction, @JobPlanningGuid, @BASReceivedTime, @BASInjected, @BASClient, @BASSyncCommand,
                                                @BASSyncState, @readonly, @client_guid, @CommentTextExternal, @Approved, @ApprovedDate, @ApprovedBy, @ExpConversionStatus, 
                                                @BackendAreaId, @ServiceOrderGuid_Inject";

        public ServiceOrderCommentLineItemRepository(string connectionString) : base(connectionString)
        {
        }

        public override string Add(ServiceOrderCommentLineItem item)
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                string query = string.Format("INSERT INTO {0}({1}) Output Inserted.{2} VALUES({3})", GetTableName(),
                                             COLUMN_NAMES, GetIdColumnName(), COLUMN_VALUES);

                IEnumerable<int> id = connection.Query<int>(query, new
                    {
                        item.ServiceOrderGuid,
                        item.CommentType,
                        item.CommentTypeName,
                        item.ServiceOrderNumber,
                        item.ServiceItemLineNumber,
                        item.CommentLineNumber,
                        item.CommentLineGuid,
                        item.CommentText,
                        item.CommentDate,
                        item.CommentTypeText,
                        item.RecordAction,
                        item.JobPlanningGuid,
                        item.BASReceivedTime,
                        item.BASInjected,
                        item.BASClient,
                        item.BASSyncCommand,
                        item.BASSyncState,
                        item.ReadOnly,
                        item.client_guid,
                        item.CommentTextExternal,
                        item.Approved,
                        item.ApprovedDate,
                        item.ApprovedBy,
                        item.ExpConversionStatus,
                        item.BackendAreaId,
                        item.ServiceOrderGuid_Inject
                    });

                return id.SingleOrDefault().ToString();
            }
        }

        public override void Update(ServiceOrderCommentLineItem item)
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                string query = string.Format(@"UPDATE {0} 
                                            SET     CommentText = @commentText
                                            WHERE   {1} = @guid", GetTableName(), GetIdColumnName());

                connection.Execute(query, new {commentText = item.CommentText, guid = item.BASGuid});
            }
        }

        public IEnumerable<ServiceOrderCommentLineItem> GetAllCommentLinesByServiceOrderGuid(string serviceOrderGuid)
        {
            string query = string.Format(@"{0} WHERE ServiceOrderGuid = @serviceOrderGuid", GetDefaultSql());
            var param = new {serviceOrderGuid};

            return GetResults(query, param);
        }

        public ServiceOrderCommentLineItem GetCommentLineByServiceOrderGuid(string serviceOrderGuid)
        {
            string query = string.Format(@"{0} WHERE ServiceOrderGuid = @serviceOrderGuid AND CommentType = 1",
                                         GetDefaultSql());
            var param = new {serviceOrderGuid};

            return GetResults(query, param).SingleOrDefault();
        }

        public int GetHighestUniqueId(string serviceOrderGuid)
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                string query =
                    string.Format(
                        @"SELECT ISNULL(MAX(CommentLineNumber), 1) AS MaxLineNo FROM {0} WHERE ServiceOrderGuid = @serviceOrderGuid",
                        GetTableName());
                var param = new {serviceOrderGuid};

                return connection.Query<int>(query, param).Single();
            }
        }

        public void DeleteAllByServiceOrderGuid(string serviceOrderGuid)
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                string query = string.Format("DELETE FROM {0} WHERE ServiceOrderGuid = @serviceOrderGuid",
                                             GetTableName());
                connection.Execute(query, new {serviceOrderGuid});
            }
        }
    }
}