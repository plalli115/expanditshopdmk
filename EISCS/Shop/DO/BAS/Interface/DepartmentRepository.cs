﻿using CmsPublic.DataRepository;
using EISCS.Shop.DO.BAS.Dto;
using EISCS.Shop.DO.BAS.Interface;
using EISCS.Shop.DO.Repository;

namespace EISCS.Shop.DO.BAS.Repository
{
    [TableProperties(TableName = "DepartmentTable", PrimaryKeyName = "DepartmentGuid")]
    public class DepartmentRepository : BaseRepository<DepartmentItem>, IDepartmentRepository
    {
        public DepartmentRepository(IPortalDatabaseConnectionFactory connectionFactory)
            : base(connectionFactory)
        {
        }
    }
}