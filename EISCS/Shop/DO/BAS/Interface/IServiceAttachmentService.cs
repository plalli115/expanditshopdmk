﻿using System.Collections.Generic;
using System.Web;
using EISCS.Shop.DO.BAS.Dto;

namespace EISCS.Shop.DO.BAS.Interface
{
    public interface IServiceAttachmentService
    {
        bool SaveAttachment(HttpPostedFileBase file, string serviceOrderGuid, string comment);
        bool DeleteAttachment(int uniqueId);
        void DeleteAllByServiceOrderGuid(string orderGuid);
        IEnumerable<ServiceAttachmentItem> GetAllAttachmentsByServiceOrderGuid(string serviceOrderGuid);
    }
}