﻿using System.Collections.Generic;
using EISCS.Shop.DO.BAS.Dto;
using EISCS.Shop.DO.Dto;

namespace EISCS.Shop.DO.BAS.Interface
{
    public interface ICustomerService
    {
        IEnumerable<CustomerItem> FindCustomers(string searchString, string customerGroupId = null);
        CustomerItem GetCustomer(string id);

        Dictionary<string, string> GetAllTechnicians();
        Dictionary<string, string> GetAllServiceManagers();
        Dictionary<string, string> GetAllServiceOrderTypes();
        IEnumerable<CustomerPropertyItem> GetPropertiesByCustomerId(string customerId);
        void SetPropertiesForCustomer(string customerId, IEnumerable<string> taskId, IEnumerable<string> technicianId, IEnumerable<string> serviceManagerId);
		ShippingAddress GetBillingAddress(string customerGuid);
        List<CustomerItem> GetCustomers(int start, int end, string columnBeingSort, string sortDir, string searchValue, string customerGuid = null);
        int CountCustomers(string searchValue, UserTable user);
        IEnumerable<CustomerItem> GetCustomersForCustomerGroup(string id);
        List<CustomerItem> GetCustomersByCustomerGroup(IEnumerable<string> customerGuidList, int start, int end, string columnBeingSort, string sortDir, string searchValue);
    }

}