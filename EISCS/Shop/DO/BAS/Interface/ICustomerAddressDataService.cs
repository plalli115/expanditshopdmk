﻿using System.Collections.Generic;
using EISCS.Shop.DO.BAS.Dto;

namespace EISCS.Shop.DO.BAS.Interface
{
    public interface ICustomerAddressDataService
    {
        IEnumerable<CustomerAddressItem> All();
        IEnumerable<CustomerAddressItem> AllByCustomerId(string customerId);
        CustomerAddressItem CustomerDeliveryAddressById(string customerId, string deliveryAddressGuid);
        List<CustomerAddressItem> GetAddressesForCustomer(int start, int end, string columnBeingSort, string sortDir, string searchValue, string customerGuid);
        int CountAdressesForCustomer(string searchValue, string customerGuid);
    }
}