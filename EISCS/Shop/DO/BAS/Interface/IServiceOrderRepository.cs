﻿using System.Collections.Generic;
using EISCS.Shop.DO.BAS.Dto;
using EISCS.Shop.DO.Dto;
using EISCS.Shop.DO.Interface;

namespace EISCS.Shop.DO.BAS.Interface
{
    public enum ServiceOrderSearchType
    {
        All = 0,
        Active = 1,
        Closed = 2
    }

    public interface IServiceOrderRepository : IBaseRepository<ServiceOrder>
    {
        IEnumerable<ServiceOrder> GetAllServiceOrders(ServiceOrderSearchType st = ServiceOrderSearchType.Active);
        IEnumerable<ServiceOrder> GetAllByCustomerGuidList(IEnumerable<string> customerGuidList, ServiceOrderSearchType st = ServiceOrderSearchType.Active);
        IEnumerable<ServiceOrder> GetAllByCustomerGuid(string customerId, ServiceOrderSearchType st = ServiceOrderSearchType.Active);
        IEnumerable<ServiceOrder> GetAllByProjectIds(IEnumerable<string> projectIds, ServiceOrderSearchType st = ServiceOrderSearchType.Active);

        IEnumerable<ServiceOrder> GetAllByUserGuid(string userGuid, ServiceOrderSearchType st = ServiceOrderSearchType.Active);
        string GetServiceOrderGuidByBasGuid(string basGuid);

        IEnumerable<ServiceOrder> GetAllArchivedServiceOrders();
        IEnumerable<ServiceOrder> GetAllArchivedByCustomerGuidList(IEnumerable<string> customerGuidList);
        IEnumerable<ServiceOrder> GetAllArchivedByCustomerGuid(string customerId);
        IEnumerable<ServiceOrder> GetAllArchivedByProjectIds(IEnumerable<string> projectIds);
        string GetServiceOrderStatus(string serviceOrderId, int JobPlanningtype = 3);
        IEnumerable<ServiceOrder> GetAllServiceOrders(int start, int end, string columnBeingSort, string sortDir, string searchValue, ServiceOrderSearchType st = ServiceOrderSearchType.Active);
        IEnumerable<ServiceOrder> GetAllByCustomerGuid(string customerId, int start, int end, string columnBeingSort, string sortDir, string searchValue, ServiceOrderSearchType st = ServiceOrderSearchType.Active);
        string GetServiceOrderCommonFields();
    }
}