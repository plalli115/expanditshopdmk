﻿using System.Collections.Generic;
using EISCS.Shop.DO.BAS.Dto;

namespace EISCS.Shop.DO.BAS.Interface
{
    public interface ICustomerDataService
    {
        IEnumerable<PortalCustomerItem> AjaxFindCustomer(string searchQuery);
        PortalCustomerItem CustomerById(string customerGuid);
    }
}