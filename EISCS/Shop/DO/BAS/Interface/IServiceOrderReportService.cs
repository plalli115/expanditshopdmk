﻿using System.Collections.Generic;
using EISCS.Shop.DO.BAS.Dto;

namespace EISCS.Shop.DO.BAS.Interface
{
    public interface IServiceOrderReportService
    {
        IEnumerable<ReportHeader> GetReports(int start, int end, string columnBeingSort, string sortDir, string searchValue);
        int CountReports(string searchValue);
        ReportHeader GetReport(string reportGuid);
        bool ServiceOrderHasReport(string serviceOrderGuid);
        IEnumerable<ServiceOrderReport> CompleteReports(IEnumerable<ReportHeader> reports);
        IEnumerable<ReportHeader> GetReportsForOrder(string serviceOrderGuid);
    }
}