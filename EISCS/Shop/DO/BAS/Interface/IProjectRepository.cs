﻿using System.Collections.Generic;
using EISCS.Shop.DO.BAS.Dto;
using EISCS.Shop.DO.Interface;

namespace EISCS.Shop.DO.BAS.Interface
{
    public interface IProjectRepository : IBaseRepository<ProjectItem>
    {
        IEnumerable<ProjectItem> GetProjectsByCustomerId(string customerGuid);
        IEnumerable<ProjectItem> GetProjectsByProjectIds(IEnumerable<string> projectIds);
    }
}