﻿using EISCS.Shop.DO.BAS.Dto;
using EISCS.Shop.DO.Interface;

namespace EISCS.Shop.DO.BAS.Interface
{
    public interface IPortalStatusRespository : IBaseRepository<PortalStatusItem>
    {
    }
}