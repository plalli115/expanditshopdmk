﻿using System.Collections.Generic;
using EISCS.Shop.DO.BAS.Dto;
using EISCS.Shop.DO.Interface;

namespace EISCS.Shop.DO.BAS.Interface
{
    public interface IServiceOrderCommentLineRepository : IBaseRepository<ServiceOrderCommentLineItem>
    {
        IEnumerable<ServiceOrderCommentLineItem> GetAllCommentLinesByServiceOrderGuid(string serviceOrderGuid);
        ServiceOrderCommentLineItem GetCommentLineByServiceOrderGuid(string serviceOrderGuid);
        int GetHighestUniqueId(string serviceOrderGuid);
        void DeleteAllByServiceOrderGuid(string serviceOrderGuid);
    }
}