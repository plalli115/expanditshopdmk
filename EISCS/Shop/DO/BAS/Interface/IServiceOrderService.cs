﻿using System.Collections.Generic;
using EISCS.Shop.BO.BAS;
using EISCS.Shop.DO.BAS.Dto;
using EISCS.Shop.DO.BAS.PostData;
using EISCS.Shop.DO.Dto;

namespace EISCS.Shop.DO.BAS.Interface
{
    public interface IServiceOrderService
    {
        string CreateServiceOrder(UserTable user, ServiceOrderPostData serviceOrderPostData, ServiceOrderJobPostData jobPostData, ServiceOrderAdditionalPostData serviceOrderAdditionalPostData = null, ServiceItemPostData postdata = null);
        
        IEnumerable<ServiceOrderTypeItem> GetAllServiceOrderTypes();
        IEnumerable<ServiceOrder> GetAllArchivedItemsByUserRole(UserTable user);
        PortalStatusItem GetServiceOrderPortalStatus(string serviceOrderId);
        ServiceOrder GetById(string id);
        bool UpdateServiceOrder(ServiceOrderEditData serviceOrderEditData);
        void Remove(string id);
        IEnumerable<ServiceOrder> GetAllItemsByUserRole(int start, int end, string columnBeingSort, string sortDir, string searchValue, UserTable user, ServiceOrderSearchType st = ServiceOrderSearchType.Active);
        int CountAllItemsByUserRole(string searchValue, UserTable user, ServiceOrderSearchType st);
        bool CanEditServiceOrder(string serviceOrderGuid);
    }
}