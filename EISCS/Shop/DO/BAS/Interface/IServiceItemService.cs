﻿using EISCS.Shop.DO.BAS.Dto;
using EISCS.Shop.DO.Dto;

namespace EISCS.Shop.DO.BAS.Interface
{
    public interface IServiceItemService
    {
        int CountServiceItemsByUser(string searchValue, UserTable user, string customerGuid, string addressGuid);
        ServiceItem GetById(string serviceItemNo);
    }
}
