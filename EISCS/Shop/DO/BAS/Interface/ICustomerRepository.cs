﻿using System.Collections.Generic;
using EISCS.Shop.DO.BAS.Dto;
using EISCS.Shop.DO.Interface;

namespace EISCS.Shop.DO.BAS.Interface
{
    public interface ICustomerRepository : IBaseRepository<CustomerItem>
    {
        IEnumerable<CustomerItem> FindCustomers(string searchQuery);
        IEnumerable<CustomerItem> GetCustomersByCustomerBranch(string customerBranchGuid);
    }
}