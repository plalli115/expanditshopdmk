﻿using System.Collections.Generic;
using EISCS.Shop.DO.BAS.Dto;
using EISCS.Shop.DO.Interface;

namespace EISCS.Shop.DO.BAS.Interface
{
    public interface IServiceAttachmentRepository : IBaseRepository<ServiceAttachmentItem>
    {
        IEnumerable<ServiceAttachmentItem> GetAllByServiceOrderGuid(string serviceOrderGuid);
        void DeleteAllByServiceOrderGuid(string serviceOrderGuid);
    }
}