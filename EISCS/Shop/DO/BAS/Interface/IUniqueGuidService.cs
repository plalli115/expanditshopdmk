﻿namespace EISCS.Shop.DO.BAS.Interface
{
    public interface IUniqueGuidService
    {
        string GenerateUniqueGuid();
    }
}