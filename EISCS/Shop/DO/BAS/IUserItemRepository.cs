﻿using EISCS.Shop.BO.BAS;

namespace EISCS.Shop.DO.BAS
{
    public interface IUserItemRepository : IBaseRepository<UserItem>
    {
    }
}