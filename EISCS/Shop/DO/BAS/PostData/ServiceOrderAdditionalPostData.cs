﻿namespace EISCS.Shop.DO.BAS.PostData
{
    public class ServiceOrderAdditionalPostData
    {
        public string DepartmentGuid { get; set; }
        public string ResponsibleUserGuid { get; set; }
    }
}