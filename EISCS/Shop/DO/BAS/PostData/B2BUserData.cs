﻿namespace EISCS.Shop.DO.BAS.PostData
{
    public class B2BUserData
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
    }
}