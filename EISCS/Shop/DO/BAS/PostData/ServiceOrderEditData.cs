﻿using System;
using EISCS.Shop.DO.Dto;

namespace EISCS.Shop.DO.BAS.PostData
{
    public class ServiceOrderEditData
    {
        public DateTime? SelectedStartDate { get; set; }
        public DateTime? SelectedFinishDate { get; set; }
        public string BillToCompanyName2 { get; set; }
        public string ShipToCompanyName2 { get; set; }

        public string ServiceOrderGuid { get; set; }
        public string DepartmentGuid { get; set; }
        public string ResponsibleUserGuid { get; set; }

        public string UserId { get; set; }
        public string CustomerGuid { get; set; }
        public string ProjectGuid { get; set; }
        public string ServiceOrderType { get; set; }
        public string CreateServiceItemDescription { get; set; }
        public string ServiceItemNo { get; set; }
        public string ItemNo { get; set; }

        public string Status { get; set; }
        public string Type { get; set; }

        public string ContactPerson { get; set; }

        public string ShipToContactPerson { get; set; }
        public string ShipToCompanyName { get; set; }
        public string ShipToAddress { get; set; }
        public string ShipToZipCode { get; set; }
        public string ShipToCityName { get; set; }
        public string ShipToPhoneNo { get; set; }
        public string ShipToPhoneNo2 { get; set; }
        public string ShipToEmailAddress { get; set; }
        public string ShopDescription { get; set; }
        public string LongDescription { get; set; }
        public string ServiceItemDescription { get; set; }
        public string ServiceOrderLineBillable { get; set; }
    }
}