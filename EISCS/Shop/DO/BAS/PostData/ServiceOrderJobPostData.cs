﻿namespace EISCS.Shop.DO.BAS.PostData
{
    public  class ServiceOrderJobPostData
    {
        public string JobTypeId { get; set; }
        public string JobStatusId { get; set; }
        public string DepartmentId { get; set; }
        public string TechnicianId { get; set; }
        public string ServiceManagerId { get; set; }
    }
}