﻿using System;

namespace EISCS.Shop.DO.BAS.PostData
{
    public class ServiceOrderPostData
    {
        public string UserId { get; set; }
        public string CustomerGuid { get; set; }
        public string PortalContactName { get; set; }

        public DateTime SelectedStartDate { get; set; }
        public DateTime? SelectedFinishDate { get; set; }

        public string JobType { get; set; }
        public string Status { get; set; }

        public string BillToContactName { get; set; }
        public string ProjectGuid { get; set; }

        public string ShiptoContactName { get; set; }
        public string ShipToCompanyName { get; set; }
        public string ShipToCompanyName2 { get; set; }
        public string ShipToAddress { get; set; }
        public string ShipToZipCode { get; set; }
        public string ShipToCityName { get; set; }
        public string ShipToPhoneNo { get; set; }
        public string ShipToPhoneNo2 { get; set; }
        public string ShipToEmailAddress { get; set; }

        public string ShortDescription { get; set; }
        public string LongDescription { get; set; }
        public string ServiceItemDescription { get; set; }
        public string ServiceOrderLineBillable { get; set; }
    }
}