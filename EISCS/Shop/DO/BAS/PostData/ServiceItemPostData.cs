﻿namespace EISCS.Shop.DO.BAS.PostData
{
    public class ServiceItemPostData
    {
        public string CustomerGuid { get; set; }
        public string CustomerAddressGuid { get; set; }
        public string ServiceItemNo { get; set; }
        public string ItemNo { get; set; }
        public string Description { get; set; }
    }
}