using CmsPublic.DataRepository;

namespace EISCS.Shop.DO.Dto
{
    public class CustomerTableBase
    {
        public string CustomerGuid { get; set; } // Base
        public string CompanyName { get; set; } // Base
        public double CreditLimitLCY { get; set; } // Base
        public string CurrencyGuid { get; set; } // Base
        public string CountryGuid { get; set; } // Base
        public string Address1 { get; set; } // Base
        public string CityName { get; set; } // Base
        public string PhoneNo { get; set; } // Base
        public string ZipCode { get; set; } // Base
        public string CountyName { get; set; } // Base
        public string BillToCustomerGuid { get; set; }
        public string Contactname { get; set; }

        [TableColumn(Mapping = TableColumnMapping.NotMapForInserts | TableColumnMapping.NotMapForUpdates)]
        public string EmailAddress { get; set; }
    }
}