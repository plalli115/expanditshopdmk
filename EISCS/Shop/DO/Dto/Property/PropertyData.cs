﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EISCS.Shop.DO.Dto.Property
{
    public class PropertyData
    {
        public PropertyData()
        {
            TemplatePropertyEntries = new List<TemplatePropertyEntry>();
        }

        public string TemplateGuid { get; set; }
        public string TemplateName { get; set; }
        public string TemplateFileName { get; set; }
        public string Templatedescription { get; set; }
        public string TemplateController { get; set; }
        public bool IsGroupTemplate { get; set; }
        public bool IsProductTemplate { get; set; }
        public List<TemplatePropertyEntry> TemplatePropertyEntries { get; private set; }
    }

    public class TemplatePropertyEntry
    {
        public string TemplateGuid { get; set; }
        public string PropGuid { get; set; }
        public string PropGroupGuid { get; set; }
        public string Propheader { get; set; }
    }
}
