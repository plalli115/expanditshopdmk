﻿using System;

namespace EISCS.Shop.DO.Dto
{
    public class KPI
    {
        public DateTime EntryDate { get; set; }
        public int TotalLogins { get; set; }
        public int TotalOrders { get; set; }
        public double AvgOrderSize { get; set; }
        public double TotalRevenueWithoutVAT { get; set; }
        public double AvgItemPrize { get; set; }
        public int TotalItems { get; set; }
        public int NewUsers { get; set; }
    }
}