﻿namespace EISCS.Shop.DO.Dto.Currency
{
    public class CurrencyItem
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public bool CurrencyEnabled { get; set; }
    }
}
