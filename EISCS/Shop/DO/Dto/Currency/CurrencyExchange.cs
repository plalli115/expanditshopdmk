﻿namespace EISCS.Shop.DO.Dto.Currency
{
    public class CurrencyExchange
    {
        public string FromCurrency { get; set; }
        public string ToCurrency { get; set; }
        public double ExchangeRate { get; set; }
    }
}
