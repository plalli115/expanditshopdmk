﻿namespace EISCS.Shop.DO.Dto.Currency
{
    public class CurrencyIso4217
    {
        public string CurrencyGuid { get; set; }
        public string Iso4217 { get; set; }
    }
}
