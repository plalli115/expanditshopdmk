﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EISCS.Shop.DO.Dto.Product;

namespace EISCS.Shop.DO.Dto
{
    public class ProductVariantContainer
    {

        private Dictionary<string, Dictionary<string, ProductVariant>> AllVariantsForAllProducts ;

        public void SetVariants(IEnumerable<ProductVariant> variants)
        {
            string productGuid = "";
            AllVariantsForAllProducts = new Dictionary<string, Dictionary<string, ProductVariant>>();
            var productLevel = new Dictionary<string, ProductVariant>();

            foreach (var variant in variants)
            {
                if (productGuid != variant.ProductGuid && !AllVariantsForAllProducts.ContainsKey(productGuid))
                {
                    AllVariantsForAllProducts.Add(productGuid, productLevel);
                    productGuid = variant.ProductGuid;
                    productLevel = new Dictionary<string, ProductVariant>();
                }

                if (!productLevel.ContainsKey(variant.VariantCode))
                {
                    productLevel.Add(variant.VariantCode, variant); 
                }
            }
            if (!productGuid.Equals("") && !AllVariantsForAllProducts.ContainsKey(productGuid))
            {
                AllVariantsForAllProducts.Add(productGuid, productLevel);
            }
        }

        public ProductVariant GetVariantForProduct(string productGuid, string variantCode)
        {
            var variantsForProduct =  GetVariantsForProduct(productGuid);
            if (variantsForProduct == null || variantCode == null)
            {
                return null;
            }

            if (!variantsForProduct.ContainsKey(variantCode))
            {
                return null;
            }

            return variantsForProduct[variantCode];
        }

        public Dictionary<string, ProductVariant> GetVariantsForProduct(string productGuid)
        {
            if (!AllVariantsForAllProducts.ContainsKey(productGuid))
            {
                return null;
            }

            return AllVariantsForAllProducts[productGuid];
        }
    }
}
