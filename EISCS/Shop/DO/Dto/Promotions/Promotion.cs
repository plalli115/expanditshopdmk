﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EISCS.Shop.DO.Dto.Promotions
{
    public class Promotion
    {
        public Promotion()
        {

        }

        public string PromotionGuid { get; set; }
        public string PromotionCode { get; set; }
        public string PromotionName { get; set; }
        public string PromotionShortDescription { get; set; }
        public string PromotionDescription { get; set; }
        public DateTime PromotionStartDate { get; set; }
        public DateTime PromotionEndDate { get; set; }
        public int NoOfOpportunities { get; set; }
        public int NoOfOpportunitiesPerUser { get; set; }
        public string PromotionStatusCode { get; set; }

        public List<PromotionEntry> PromotionEntries { get; set; }
    }
}
