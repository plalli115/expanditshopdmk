﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EISCS.Shop.DO.Dto.Promotions
{
    public class PromotionItemEntry
    {
        public PromotionItemEntry()
        {

        }
        public int EntryNo { get; set; }
        public string PromotionItemListCode { get; set; }
        public string ItemNo { get; set; }
        public string ItemDescription { get; set; }
    }
}
