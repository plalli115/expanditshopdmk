﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EISCS.Shop.DO.Dto.Promotions
{
    public class PromotionEntry
    {
		public PromotionEntry() { 

		}

		public string PromotionGuid { get; set; }
		public int LineNo { get; set; }
		public int PromotionType { get; set; }
		public string Code { get; set; }
		public string SubCode { get; set; }
		public string LinkedCode { get; set; }
		public float DiscountPercentage { get; set; }
		public float DiscountAmount { get; set; }
		public float MaxDiscountAmount { get; set; }
		public float AmountOrQtyRequired { get; set; }

		public List<PromotionItemEntry> PromotionItemEntries { get; set; }

    }
}
