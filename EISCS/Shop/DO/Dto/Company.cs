﻿namespace EISCS.Shop.DO.Dto
{
    public class Company
    {
        public string CompanyGuid { get; set; }
        public string CompanyName { get; set; }
        //public string CompanyVATNo { get; set; }
        public string FaxNo { get; set; }
        public string ZipCode { get; set; }
        public string EmailAddress { get; set; }
        public string HomePage { get; set; }
        public string CountryGuid { get; set; }
        public string CountryName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string CityName { get; set; }
        public string PhoneNo { get; set; }
    }
}