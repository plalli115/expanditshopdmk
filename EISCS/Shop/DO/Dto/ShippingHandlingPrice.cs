﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EISCS.Shop.DO.Dto {
	public class ShippingHandlingPrice {
		public string ShippingHandlingProviderGuid { get; set; }
		public float CalculatedValue { get; set; }
		public float ShippingAmount { get; set; }
		public float HandlingAmount { get; set; }
	}
}
