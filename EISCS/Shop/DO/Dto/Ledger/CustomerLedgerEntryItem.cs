﻿using System;
using System.ComponentModel.DataAnnotations;

namespace EISCS.Shop.DO.Dto.Ledger
{
    public class CustomerLedgerEntryItem
    {
        public object EntryGuid { get; set; }
        public string CurrencyGuid { get; set; }
        public double Amount { get; set; }
        public double RemainingAmount { get; set; }
        public double OriginalAmountLCY { get; set; }
        public double RemainingAmountLCY { get; set; }
        public double AmountLCY { get; set; }
        public double SalesLCY { get; set; }
        public double ProfitLCY { get; set; }
        public double InvoiceDiscountLCY { get; set; }
        public string SellToCustomerGuid { get; set; }
        public string CustomerGuid { get; set; }
        public bool IsOpen { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", NullDisplayText = "-")]
        public DateTime DueDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", NullDisplayText = "-")]
        public DateTime PostingDate { get; set; }

        public int DocumentType { get; set; }
        public string DocumentGuid { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", NullDisplayText = "-")]
        public DateTime DocumentDate { get; set; }

        public string ExternalDocumentNo { get; set; }
        public string Description { get; set; }
    }
}