﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EISCS.Shop.DO.Dto
{
    public class BusinessCenterUser
    {
        public string UserGuid { get; set; }
        public string UserLogin { get; set; }
        public string UserPassword { get; set; }
        public string RoleId { get; set; }
        public string EncryptionType { get; set; }
        public string EncryptionSalt { get; set; }
        public DateTime LastLoginDate { get; set; }
        public bool IsSuperUser { get; set; }
    }
}
