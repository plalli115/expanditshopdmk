﻿namespace EISCS.Shop.DO.Dto
{
	public class PaymentTypeTable
	{
		public string PaymentType { get; set; }
		public string PaymentName { get; set; }
	}
}