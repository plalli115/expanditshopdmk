namespace EISCS.Shop.DO.Dto
{
    public class AccessRoles
    {
        public string RoleId { get; set; }
        public string AccessClass { get; set; }
        public string ClassDescription { get; set; }
        public string RoleDescription { get; set; }
    }
}