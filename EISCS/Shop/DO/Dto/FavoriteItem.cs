﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EISCS.Shop.DO.Dto
{
    public class FavoriteItem
    {
        public string UserGuid { get; set; }
        public string ProductGuid { get; set; }
    }
}
