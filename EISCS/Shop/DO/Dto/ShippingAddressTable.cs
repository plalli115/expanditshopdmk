﻿namespace EISCS.Shop.DO.Dto
{
    public class ShippingAddressTable
    {
        public string ShippingAddressGuid { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string CityName { get; set; }
        public string CompanyName { get; set; }
        public string ContactName { get; set; }
        public string CountryGuid { get; set; }
        public string CountryName { get; set; }
        public string CountyName { get; set; }
        public string EmailAddress { get; set; }
        public string StateName { get; set; }
        public string UserGuid { get; set; }
        public string ZipCode { get; set; }
        public bool IsDefault { get; set; }
    }
}
