﻿namespace EISCS.Shop.DO.Dto
{
	public class ShippingHandlingProviderTable
	{
		public string ShippingHandlingProviderGuid { get; set; }
		public string ProviderName { get; set; }
		public string ProviderDescription { get; set; }
		public int SortIndex { get; set; }
		public bool IsPickUp { get; set; }
	}
}