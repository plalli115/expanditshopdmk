﻿namespace EISCS.Shop.DO.Dto
{
	public class PaymentTypeShippingHandlingProviderTable
	{
		public string PaymentType { get; set; }
		public string ShippingProviderGuid { get; set; }
	}
}