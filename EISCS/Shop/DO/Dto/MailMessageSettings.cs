﻿using System;
using CmsPublic.DataRepository;

namespace EISCS.Shop.DO.Dto
{
    public class BaseMailMessageSettings
    {
        [TableColumn(Mapping = TableColumnMapping.NotMapForUpdates)]
        public int Id { get; set; }
    }

    public class MailServerSettings : BaseMailMessageSettings
    {
        public bool ForceSsl { get; set; }
        public string Host { get; set; }
        public string Port { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }

    public class GeneralMailMessageSettings: BaseMailMessageSettings
    {
        public string SendAsName { get; set; }
        public string SendAsAddress { get; set; }
        public string CC { get; set; }
        public string BCC { get; set; }
    }

    public class StaleCartMailMessageSettings : BaseMailMessageSettings
    {
        public bool SendReminderEmail { get; set; }
        public int SendAfterStalePeriod { get; set; }
        public int RepeatPeriod { get; set; }
        public TimeSpan SendTime { get; set; }
    }

    public class MailMessageSettings : BaseMailMessageSettings
    {
        public bool ForceSsl { get; set; }
        public string Host { get; set; }
        public string Port { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string SendAsName { get; set; }
        public string SendAsAddress { get; set; }
        public string CC { get; set; }
        public string BCC { get; set; }
        public bool SendReminderEmail { get; set; }
        public int SendAfterStalePeriod { get; set; }
        public int RepeatPeriod { get; set; }
        public TimeSpan SendTime { get; set; }
    }
}
