using System;

namespace EISCS.Shop.DO.Dto
{
    public class CountryTable
    {
        public String CountryGuid { get; set; }
        public String CountryName { get; set; }
    }
}