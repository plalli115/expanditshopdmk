﻿namespace EISCS.Shop.DO.Dto.Country
{
    public class CountryItem
    {
        public string CountryGuid { get; set; }
        public string CountryName { get; set; }
    }
}