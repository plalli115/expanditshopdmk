﻿using System;
using System.Collections.Generic;
using System.Linq;
using CmsPublic.DataRepository;
using EISCS.Shop.DO.Interface;

namespace EISCS.Shop.DO.Dto
{
    public class UserTable
    {
        public UserTable(){
            PasswordResetRequestTime = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;
            UserCreated = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;
            UserModified = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;
        }

        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string CityName { get; set; }
        public string CompanyName { get; set; }
        public string ContactName { get; set; }
        public string CountryGuid { get; set; }
        public string CountyName { get; set; }
        public string CurrencyGuid { get; set; }
        public string CustomerGuid { get; set; }
        public string EmailAddress { get; set; }
        public bool IsB2B { get; set; }
        public string LanguageGuid { get; set; }
        public string PasswordVersion { get; set; }
        public string PhoneNo { get; set; }
        public string SecondaryCurrencyGuid { get; set; }
        public string StateName { get; set; }
        public DateTime UserCreated { get; set; }
        public string UserGuid { get; set; }
        public string UserLogin { get; set; }
        public DateTime UserModified { get; set; }
        public string UserPassword { get; set; }
        public string ZipCode { get; set; }
        public string ShippingProvider { get; set; }
        public byte Status { get; set; }
        public bool IsApproved { get; set; }
        public bool IsLockedOut { get; set; }
        public string EncryptionType { get; set; }
        public string EncryptionSalt { get; set; }
        
        public string RoleId { get; set; }

        [TableColumn(Mapping = TableColumnMapping.NeverMap)]
        public DateTime LastLoginDate { get; set; }
        
        [TableColumn(Mapping = TableColumnMapping.NeverMap)]
        public DateTime LastPasswordChangeDate { get; set; }
        
        [TableColumn(Mapping = TableColumnMapping.NeverMap)]
        public DateTime LastLockoutDate { get; set; }

        [TableColumn(Mapping = TableColumnMapping.NeverMap)]
        public int FailedPasswordAttemptCount { get; set; }
        
        [TableColumn(Mapping = TableColumnMapping.NeverMap)]
        public DateTime FailedPasswordAttemptCountWindowStart { get; set; }

        [TableColumn(Mapping = TableColumnMapping.NeverMap)]
        public string CustomerGroupId { get; set; }
        
        [TableColumn(Mapping = TableColumnMapping.NeverMap)]
        public Dictionary<string, string> RoleList { get; set; }

        [TableColumn(Mapping = TableColumnMapping.NeverMap)]
        public Dictionary<string, string> CustomerList { get; set; }

        [TableColumn(Mapping = TableColumnMapping.NeverMap)]
        public DateTime LastOrder { get; set; }

        [TableColumn(Mapping = TableColumnMapping.NeverMap)]
        public string CustomerName { get; set; }

        [TableColumn(Mapping = TableColumnMapping.NeverMap)]
        public IUserStorageService UserStorage { get; set; }

        public string PasswordResetRequestKey { get; set; }
        
        public DateTime PasswordResetRequestTime { get; set; }
        public string Secret { get; set; }
        public bool MultiFactorAuthEnabled { get; set; }
        
        public bool UserAccess(string accessClass)
        {
            string userRole = string.IsNullOrWhiteSpace(RoleId) ? "B2C" : RoleId;
            var userAccessClass = UserStorage.GetUserAccessByRole(accessClass, userRole);
            return userAccessClass != null && userAccessClass.Any();
        }
    }
}
