﻿namespace EISCS.Shop.DO.Dto
{
    public class ShippingAddress : ShippingAddressTable
    {
        public string AddressType { get; set; }
		public string CustomerGuid { get; set; }
    }
}