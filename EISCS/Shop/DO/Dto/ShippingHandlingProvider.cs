﻿using System.Collections.Generic;

namespace EISCS.Shop.DO.Dto
{
	public class ShippingHandlingProvider : ShippingHandlingProviderTable
	{
		public List<ShippingHandlingPrice> ShippingHandlingPrices;
	}
}