﻿using CmsPublic.DataRepository;
using EISCS.Shop.Attributes;

namespace EISCS.Shop.DO.Dto.Cart
{
    public class BaseOrderLine
    {
        [TableColumn(Mapping = TableColumnMapping.NeverMap)]
        public ILineParameter LineParameter { get; set; }
        
        public string HeaderGuid { get; set; }
        
        public string CurrencyGuid { get; set; }
        
        public string LineComment { get; set; }
        
        public double LineDiscountAmount { get; set; }
        
        public double LineDiscount { get; set; }
        
        public string LineGuid { get; set; }
        
        public int LineNumber { get; set; }
        
        public double LineTotal { get; set; }
        
        public double ListPrice { get; set; }
        
        public string ProductGuid { get; set; }
        
        public string ProductName { get; set; }
        
        public double Quantity { get; set; }
        
        public double TaxAmount { get; set; }
        
        public double TotalInclTax { get; set; }
        
        public string VariantCode { get; set; }
        
        public bool IsCalculated { get; set; }
        
        public string VersionGuid { get; set; }
        
        public double ListPriceInclTax { get; set; }
        
        public double LineDiscountAmountInclTax { get; set; }
        
        public string VariantName { get; set; }
        
        public string TaxPct { get; set; }

        public bool PromotionalGift { get; set; }
    }

    public interface ILineParameter
    {
    }
}
