﻿namespace EISCS.Shop.DO.Dto.Cart{
    public class CartUpdateInfo
    {
        public string Comment { get; set; }
        public string LineGuid { get; set; }
        public string VersionGuid { get; set; }
        public string ProductGuid { get; set; }
		public string ProductName { get; set; }
        public string VariantCode { get; set; }
        public string VariantName { get; set; }
        public double Quantity { get; set; }
    }
}