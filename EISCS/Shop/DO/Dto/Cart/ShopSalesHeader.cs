﻿using System;

namespace EISCS.Shop.DO.Dto.Cart
{
    public class ShopSalesHeader : BaseOrderHeader<ShopSalesLine>
    {
        public double PaymentFeeAmount { get; set; }
        public double PaymentFeeAmountInclTax { get; set; }
        public DateTime HeaderDate { get; set; }
        public bool MailSent { get; set; }
    }
}
