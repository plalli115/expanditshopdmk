﻿using System;
using System.Collections.Generic;
using CmsPublic.DataRepository;
using EISCS.Shop.Attributes;

namespace EISCS.Shop.DO.Dto.Cart
{
    public class BaseOrderHeader<T> where T : class
    {
        [TableColumn(Mapping = TableColumnMapping.NeverMap)]
        public List<T> Lines { get; set; }
        [TableColumn(Mapping = TableColumnMapping.NeverMap)]
        public IHeaderParameter HeaderParameter { get; set; }

        public DateTime CreatedDate { get; set; }

        public string CurrencyGuid { get; set; }

        public string CustomerPONumber { get; set; }

        public string HeaderComment { get; set; }

        public string HeaderGuid { get; set; }

        public double InvoiceDiscount { get; set; }

        public DateTime ModifiedDate { get; set; }

        public string MultiCartDescription { get; set; }

        public string MultiCartStatus { get; set; }

        public string PromotionCode { get; set; }

        public double ServiceCharge { get; set; }

        public double SubTotal { get; set; }

        public double TaxAmount { get; set; }

        public string TaxCode { get; set; }

        public double TotalInclTax { get; set; }

        public string UserGuid { get; set; }

        public double Total { get; set; }

        public string CustomerReference { get; set; }

        public string ShippingHandlingProviderGuid { get; set; }

        public bool IsCalculated { get; set; }

        public string VersionGuid { get; set; }

        public double SubTotalInclTax { get; set; }

        public double ServiceChargeInclTax { get; set; }

        public double InvoiceDiscountInclTax { get; set; }

        public double ShippingAmount { get; set; }

        public double ShippingAmountInclTax { get; set; }

        public double HandlingAmount { get; set; }

        public double HandlingAmountInclTax { get; set; }

        public string PaymentStatus { get; set; }

        public string PaymentError { get; set; }

        public string ShipToAddress1 { get; set; }

        public string ShipToAddress2 { get; set; }

        public string ShipToCityName { get; set; }

        public string ShipToCompanyName { get; set; }

        public string ShipToContactName { get; set; }

        public string ShipToCountryGuid { get; set; }

        [TableColumn(Mapping = TableColumnMapping.NeverMap)]
        public string ShipToCountryName { get; set; }

        public string ShipToCountyName { get; set; }

        public string ShipToEmailAddress { get; set; }

        public string ShipToStateName { get; set; }

        public string ShipToZipCode { get; set; }

        public string ShipToShippingAddressGuid { get; set; }

        public string Address1 { get; set; }

        public string Address2 { get; set; }

        public string CityName { get; set; }

        public string CompanyName { get; set; }

        public string ContactName { get; set; }

        public string CountryGuid { get; set; }

        public string CountryName { get; set; }

        public string CountyName { get; set; }		

        public string EmailAddress { get; set; }

        public string StateName { get; set; }

        public string ZipCode { get; set; }

        public string ShippingAddressGuid { get; set; }

        public string PaymentMethod { get; set; }

        public string InvoiceDiscountPct { get; set; }

        public string TaxPct { get; set; }

        public bool PricesIncludingVat { get; set; }

        public string VatTypes { get; set; }
    }

    public interface IHeaderParameter
    {
    }
}
