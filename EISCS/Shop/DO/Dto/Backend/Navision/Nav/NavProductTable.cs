﻿using DataFieldDefinition;

namespace EISCS.Shop.DO.Dto.Backend.Navision.Nav
{
    public class NavProductTable : ProductTableColumns
    {
        public bool AllowInvoiceDiscount { get; set; }
        public string VATProductPostingGroup { get; set; }
        public string ItemCustomerDiscountGroup { get; set; }
        public bool PriceIncludesVAT { get; set; }
    }
}
