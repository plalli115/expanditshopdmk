﻿namespace EISCS.Shop.DO.Dto.Backend.Navision.Nav
{
    public class Attain_CustomerInvoiceDiscount
    {
        public string InvoiceDiscountCode { get; set; }
        public double MinimumAmount { get; set; }
        public double DiscountPct { get; set; }
        public double ServiceCharge { get; set; }
        public string CurrencyCode { get; set; }
    }
}
