﻿using System;

namespace EISCS.Shop.DO.Dto.Backend.Navision.Nav
{
    public class Attain_ProductPrice
    {
      public string ProductGuid {get;set;}
      public bool AllowInvoiceDiscount {get;set;}
      public string Attain_VATBusPostingGrPrice {get;set;}
      public int CustomerRelType {get;set;}
      public double MinimumQuantity {get;set;}
      public DateTime EndingDate {get;set;}
      public string CustomerRelGuid {get;set;}
      public string CurrencyGuid {get;set;}
      public DateTime StartingDate {get;set;}
      public double UnitPrice {get;set;}
      public string UOMGuid {get;set;}
      public string VariantCode {get;set;}
      public bool PriceInclTax {get;set;}
      public bool AllowLineDiscount {get;set;}
    }
}
