﻿namespace EISCS.Shop.DO.Dto.Backend.Navision.Nav
{
    public class Attain_VATPostingSetup
    {
        public string BusinessPostingGroup { get; set; }
        public string ProductPostingGroup { get; set; }
        public double VATPct { get; set; }
    }
}