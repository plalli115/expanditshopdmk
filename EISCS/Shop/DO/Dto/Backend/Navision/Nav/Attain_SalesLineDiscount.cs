﻿using System;

namespace EISCS.Shop.DO.Dto.Backend.Navision.Nav
{
    public class Attain_SalesLineDiscount
    {
        public string Code { get; set; }
        public int SalesType { get; set; }
        public double MinimumQuantity { get; set; }
        public DateTime EndingDate { get; set; }
        public string SalesCode { get; set; }
        public int Type { get; set; }
        public string CurrencyCode { get; set; }
        public DateTime StartingDate { get; set; }
        public double LineDiscountPct { get; set; }
        public string UnitOfMeasureCode { get; set; }
        public string VariantCode { get; set; }
    }
}
