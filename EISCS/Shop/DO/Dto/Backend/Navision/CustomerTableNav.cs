﻿namespace EISCS.Shop.DO.Dto.Backend.Navision
{
    public class CustomerTableNavBase : CustomerTableBase
    {
        public new string EmailAddress { get; set; }
        public double ReminderAmounts { get; set; }
        public double ReminderAmountsLCY { get; set; }
        public string VATBusinessPostingGroup { get; set; }
        public double OutstandingOrdersLCY { get; set; }
        public double ShippedNotInvoicedLCY { get; set; }
        public string PriceGroupCode { get; set; }
        public string InvoiceDiscountCode { get; set; }
        public double Amount { get; set; }
        public string BillToCustomerGuid { get; set; }
        public double BalanceAmount { get; set; }
        public double BalanceAmountLCY { get; set; }
        public string Address2 { get; set; }
        public double BalanceDue { get; set; }
        public double BalanceDueLCY { get; set; }
        public bool EnableLogin { get; set; }
        public double OutstandingOrders { get; set; }
        public double ShippedNotInvoiced { get; set; }
        public string ContactName { get; set; }
    }

    public class CustomerTableNav : CustomerTableNavBase
    {
        public string CustomerDiscountGroup { get; set; }
        public bool AllowLineDisc { get; set; }
        public bool PricesIncludingVat { get; set; }
    }

    public class CustomerTableNf : CustomerTableNavBase
    {
        public bool AllowQuantityDiscount { get; set; }
        public string CustomerItemDiscountGroup { get; set; }
    }
}
