﻿namespace EISCS.Shop.DO.Dto.Backend.Navision.Nf
{
    public class NF_VATPostingSetup
    {
        public string BusinessPostingGroup { get; set; }
        public string ProductPostingGroup { get; set; }
        public double VATPct { get; set; }
    }
}
