﻿namespace EISCS.Shop.DO.Dto.Backend.Navision.Nf
{
    public class NF_CustomerItemDiscount
    {
        public string CustomerDiscountGroup { get; set; }
        public string ItemDiscountGroup { get; set; }
        public double DiscountPct { get; set; }
        // Used in join with ProductTable
        public string ItemCustomerDiscountGroup { get; set; }
        public string ProductGuid { get; set; }
    }
}
