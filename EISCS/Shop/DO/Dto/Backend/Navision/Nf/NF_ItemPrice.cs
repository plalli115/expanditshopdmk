﻿using System;

namespace EISCS.Shop.DO.Dto.Backend.Navision.Nf
{
    public class NF_ItemPrice
    {
        public string ItemNo { get; set; }
        public bool AllowInvoiceDiscount { get; set; }
        public string PriceGroupCode { get; set; }
        public string CurrencyCode { get; set; }
        public DateTime StartingDate { get; set; }
        public double UnitPrice { get; set; }
        public bool AllowQuantityDiscount { get; set; }
        public bool AllowCustomerItemDiscount { get; set; }
        public bool PriceInclTax { get; set; }
    }
}
