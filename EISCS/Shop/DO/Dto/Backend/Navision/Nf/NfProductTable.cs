using EISCS.Shop.DO.Dto.Backend.Navision.Nav;

namespace EISCS.Shop.DO.Dto.Backend.Navision.Nf
{
    public class NfProductTable : NavProductTable
    {
        public string QtyDiscountGroup { get; set; }
    }
}