﻿namespace EISCS.Shop.DO.Dto.Backend.Navision.Nf
{
    public class NF_SalesReceivablesSetup
    {
        public int LineDiscountCalculation { get; set; }
    }
}
