﻿namespace EISCS.Shop.DO.Dto.Backend.Navision.Nf
{
    public class NF_Item_Qty_Disc
    {
        public string QtyDiscountGroup { get; set; }
        public double MinimumQuantity { get; set; }
        public double DiscountPct { get; set; }
        // Used in join with ProductTable
        public string ProductGuid { get; set; }
    }
}
