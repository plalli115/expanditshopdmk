﻿namespace EISCS.Shop.DO.Dto.Backend.Navision{
    public class VatPctInfo
    {
        public string ProductGuid { get; set; }
        public double VATPct { get; set; }
    }
}