﻿namespace EISCS.Shop.DO.Dto.Backend.Axapta
{
    public class AxProductTable
    {
        public int EndDisc { get; set; }
        public double Inventory { get; set; }
        public string LineDisc { get; set; }
        public int ModuleType { get; set; }
        public string MultilnDisc { get; set; }
        public double ListPrice { get; set; }
        public string ProductGrp { get; set; }
        public string ProductGuid { get; set; }
        public string ProductName { get; set; }
        public string TaxItemGroupId { get; set; }
    }
}
