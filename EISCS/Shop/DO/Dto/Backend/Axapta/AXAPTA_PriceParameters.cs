﻿namespace EISCS.Shop.DO.Dto.Backend.Axapta
{
    public class AXAPTA_PriceParameters
    {
        public int PURCHENDACCOUNTALL { get; set; }
        public int PURCHENDALLALL { get; set; }
        public int PURCHENDGROUPALL { get; set; }
        public int PURCHLINEACCOUNTALL { get; set; }
        public int PURCHLINEACCOUNTGROUP { get; set; }
        public int PURCHLINEACCOUNTITEM { get; set; }
        public int PURCHLINEALLALL { get; set; }
        public int PURCHLINEALLGROUP { get; set; }
        public int PURCHLINEALLITEM { get; set; }
        public int PURCHLINEGROUPALL { get; set; }
        public int PURCHLINEGROUPGROUP { get; set; }
        public int PURCHLINEGROUPITEM { get; set; }
        public int PURCHMULTILNACCOUNTALL { get; set; }
        public int PURCHMULTILNACCOUNTGROUP { get; set; }
        public int PURCHMULTILNALLALL { get; set; }
        public int PURCHMULTILNALLGROUP { get; set; }
        public int PURCHMULTILNGROUPALL { get; set; }
        public int PURCHMULTILNGROUPGROUP { get; set; }
        public int PURCHPRICEACCOUNTITEM { get; set; }
        public int PURCHPRICEALLITEM { get; set; }
        public int PURCHPRICEGROUPITEM { get; set; }
        public int SALESORDERENDACCOUNTALL { get; set; }
        public int SALESORDERENDALLALL { get; set; }
        public int SALESORDERENDGROUPALL { get; set; }
        public int SALESORDERLINEACCOUNTALL { get; set; }
        public int SALESORDERLINEACCOUNTGROUP { get; set; }
        public int SALESORDERLINEACCOUNTITEM { get; set; }
        public int SALESORDERLINEALLALL { get; set; }
        public int SALESORDERLINEALLGROUP { get; set; }
        public int SALESORDERLINEALLITEM { get; set; }
        public int SALESORDERLINEGROUPALL { get; set; }
        public int SALESORDERLINEGROUPGROUP { get; set; }
        public int SALESORDERLINEGROUPITEM { get; set; }
        public int SALESORDERMULTILNACCOUNTALL { get; set; }
        public int SALESORDERMULTILNACCOUNTGROUP { get; set; }
        public int SALESORDERMULTILNALLALL { get; set; }
        public int SALESORDERMULTILNALLGROUP { get; set; }
        public int SALESORDERMULTILNGROUPALL { get; set; }
        public int SALESORDERMULTILNGROUPGROUP { get; set; }
        public int SALESORDERPRICEACCOUNTITEM { get; set; }
        public int SALESORDERPRICEALLITEM { get; set; }
        public int SALESORDERPRICEGROUPITEM { get; set; }
    }
}
