﻿namespace EISCS.Shop.DO.Dto.Backend.Axapta
{
    public class AXAPTA_TaxOnItem
    {
        public string TaxCode { get; set; }
        public string TaxItemGroup { get; set; }
    }
}
