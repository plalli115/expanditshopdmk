﻿namespace EISCS.Shop.DO.Dto.Backend.Axapta
{
    public class AXAPTA_ProductPrice
    {
        public int EndDisc { get; set; }
        public string ProductGuid { get; set; }
        public string LineDisc { get; set; }
        public int ModuleType { get; set; }
        public string MultilnDisc { get; set; }
        public double ListPrice { get; set; }
        public double Quantity { get; set; }
        public string TaxItemGroupId { get; set; }
    }
}
