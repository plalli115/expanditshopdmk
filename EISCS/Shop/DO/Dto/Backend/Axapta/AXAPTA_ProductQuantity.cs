﻿namespace EISCS.Shop.DO.Dto.Backend.Axapta
{
    public class AXAPTA_ProductQuantity
    {
        public double Quantity { get; set; }
        public string ProductGuid { get; set; }
    }
}
