﻿namespace EISCS.Shop.DO.Dto.Backend.Axapta
{
    public class AXAPTA_ProductName
    {
        public string ProductGuid { get; set; }
        public string ProductName { get; set; }
        public string ProductGrp { get; set; }
    }
}
