namespace EISCS.Shop.DO.Dto.Backend.Axapta
{
    public class CustomerTableAx2009 : CustomerTableBase
    {
        public string CashDisc { get; set; }
        public string DebtorGroup { get; set; }
        public int EnableLogin { get; set; }
        public string EndDisc { get; set; }
        public string LineDisc { get; set; }
        public string MultilnDisc { get; set; }
        public string PriceGroup { get; set; }
        public string StateName { get; set; }
        public string TaxGroup { get; set; }
    }
}