﻿namespace EISCS.Shop.DO.Dto.Backend.Axapta
{
    public class AXAPTA_CompanyInfo
    {
        public string Address1 { get; set; }
        public string ExchangeCode { get; set; }
        public string Name { get; set; }
        public string ExchangeCode2 { get; set; }
    }
}
