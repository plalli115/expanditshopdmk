﻿using System;

namespace EISCS.Shop.DO.Dto.Backend.Axapta
{
    public class AXAPTA_CustomerLedgerEntryOpen
    {
        public string CustomerGuid { get; set; }
        public double Amount { get; set; }
        public DateTime DueDate { get; set; }
        public int RecId { get; set; }
        public DateTime DocumentDate { get; set; }
    }
}
