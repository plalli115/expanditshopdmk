﻿using System;
using EISCS.Shop.Attributes;

namespace EISCS.Shop.DO.Dto.Backend.Axapta
{
    public class AXAPTA_TaxData
    {
        public string TaxCode { get; set; }
        public DateTime TaxFromDate { get; set; }
        public double TaxLimitMax { get; set; }
        public double TaxLimitMin { get; set; }
        public DateTime TaxToDate { get; set; }
        public double TaxValue { get; set; }
        [NotMapped]
        public string ProductGuid { get; set; }
    }
}
