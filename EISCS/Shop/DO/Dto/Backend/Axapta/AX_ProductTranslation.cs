﻿namespace EISCS.Shop.DO.Dto.Backend.Axapta
{
    public class AX_ProductTranslation
    {
        public string ProductGuid { get; set; }
        public string LanguageGuid { get; set; }
        public string ProductName { get; set; }
    }
}
