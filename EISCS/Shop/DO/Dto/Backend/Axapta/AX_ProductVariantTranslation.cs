﻿namespace EISCS.Shop.DO.Dto.Backend.Axapta
{
    public class AX_ProductVariantTranslation
    {
        public string VariantCode { get; set; }
        public string ProductGuid { get; set; }
        public string LanguageGuid { get; set; }
        public string ProductName { get; set; }
    }
}
