﻿using System;
using EISCS.Shop.Attributes;

namespace EISCS.Shop.DO.Dto.Backend.Axapta
{
    public class AXAPTA_PriceDisc
    {
        public int AccountCode { get; set; }
        public string AccountRelation { get; set; }
        public string Agreement { get; set; }
        public double Amount { get; set; }
        public string Exchange { get; set; }
        public int DeliveryTime { get; set; }
        public DateTime FromDate { get; set; }
        public string VariantCode { get; set; }
        public int ItemCode { get; set; }
        public string ItemRelation { get; set; }
        public double Percent1 { get; set; }
        public double Percent2 { get; set; }
        public double PriceUnit { get; set; }
        public double QuantityAmount { get; set; }
        public double QuantityAmountTo { get; set; }
        public int Relation { get; set; }
        public int LookforForward { get; set; }
        public DateTime ToDate { get; set; }
        [NotMapped]
        public string TranslatedVariantCode { get; set; }
    }
}
