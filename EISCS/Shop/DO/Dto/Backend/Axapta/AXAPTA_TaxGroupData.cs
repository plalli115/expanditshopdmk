﻿namespace EISCS.Shop.DO.Dto.Backend.Axapta
{
    public class AXAPTA_TaxGroupData
    {
        public string TaxCode { get; set; }
        public string TaxGroup { get; set; }
        public int UseTax { get; set; }
    }
}
