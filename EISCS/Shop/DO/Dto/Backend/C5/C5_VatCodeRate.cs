﻿namespace EISCS.Shop.DO.Dto.Backend.C5
{
    public class C5_VatCodeRate
    {
        public double VatPct { get; set; }
        public string VatCode { get; set; }
    }
}