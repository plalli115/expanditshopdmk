﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EISCS.Shop.DO.Dto.Backend.C5
{
    public class C5PriceData : C5_StockPrice
    {
        public string InclVat { get; set; }
    }
}
