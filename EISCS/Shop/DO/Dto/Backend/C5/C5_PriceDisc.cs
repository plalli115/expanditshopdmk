﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EISCS.Shop.DO.Dto.Backend.C5
{
    public class C5_PriceDisc
    {
        public int AccountCode { get; set; }
        public string AccountRelation { get; set; }
        public DateTime FromDate { get; set; }
        public int ItemCode { get; set; }
        public string ItemRelation { get; set; }
        public double QuantityAmount { get; set; }
        public double Amount { get; set; }
        public byte LookforForward { get; set; }
        public byte LookforForwardSame { get; set; }
        public DateTime ToDate { get; set; }
        public byte Type { get; set; }
    }
}
