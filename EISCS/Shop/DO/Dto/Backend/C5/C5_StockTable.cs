﻿namespace EISCS.Shop.DO.Dto.Backend.C5
{
    public class C5_StockTable
    {
        public string LineDisc { get; set; }
        public string ProductGrp { get; set; }
        public double Inventory { get; set; }
        public string ProductName { get; set; }
        public string ProductGuid { get; set; }
        public byte ItemType { get; set; }
        public double NetWeight { get; set; }
    }
}