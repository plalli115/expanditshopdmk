﻿namespace EISCS.Shop.DO.Dto.Backend.C5
{
    public class C5_StockPrice
    {
        public string CurrencyGuid { get; set; }
        public string ProductGuid { get; set; }
        public double ListPrice { get; set; }
        public string C5PriceGroup { get; set; }
        public string SalesVatCode { get; set; }
    }
}