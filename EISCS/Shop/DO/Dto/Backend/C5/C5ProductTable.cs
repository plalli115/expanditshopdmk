﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EISCS.Shop.DO.Dto.Backend.C5
{
    public class C5ProductTable
    {
        public string ProductGuid { get; set; }
        public string Linedisc { get; set; }
        public string SalesVatCode { get; set; }
        public double ListPrice { get; set; }
    }
}
