﻿namespace EISCS.Shop.DO.Dto.Backend.C5
{
    public class C5_StockPriceGroup
    {
        public string C5PriceGroup { get; set; }
        public string InclVat { get; set; }
    }
}