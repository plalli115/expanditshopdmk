﻿namespace EISCS.Shop.DO.Dto.Backend.C5
{
    public class CustomerTableC5 : CustomerTableBase
    {
        public string Address2 { get; set; }
        public int AddrRecID { get; set; }
        public double BalanceAmountLCY { get; set; }
        public double BalanceDueLCY { get; set; }
        public string C5Pricegroup { get; set; }
        public string CashDisc { get; set; }
        public byte CashPayment { get; set; }
        public string ContactName { get; set; }
        public string DebitorGroup { get; set; }
        public new string EmailAddress { get; set; }
        public byte EnableLogin { get; set; }
        public string PriceGroup { get; set; }
        public string VatCode { get; set; }
    }
}