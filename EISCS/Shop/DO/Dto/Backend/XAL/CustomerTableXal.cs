﻿namespace EISCS.Shop.DO.Dto.Backend.XAL
{
    public class CustomerTableXal : CustomerTableBase
    {
        public string Address2 { get; set; }
        public string ContactName { get; set; }
        public double BalanceAmountLCY { get; set; }
        public string CashDisc { get; set; }
        public string DebtorGroup { get; set; }
        public byte EnableLogin { get; set; }
        public string Enddisc { get; set; }
        public string Linedisc { get; set; }
        public string Multilndisc { get; set; }
        public string Pricegroup { get; set; }
        public string VatCode { get; set; }
        public byte Vatdutiable { get; set; }
    }
}