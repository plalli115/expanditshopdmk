﻿using System;

namespace EISCS.Shop.DO.Dto.Backend.XAL
{
    public class XAL_PriceDisc
    {
        public int AccountCode { get; set; }
        public string AccountRelation { get; set; }
        public string Agreement { get; set; }
        public double Amount { get; set; }
        public int Deliverytime { get; set; }
        public string Exchange { get; set; }
        public string ExternalItemNo { get; set; }
        public DateTime FromDate { get; set; }
        public int ItemCode { get; set; }
        public string ItemRelation { get; set; }
        public byte LookforForward { get; set; }
        public double Percent1 { get; set; }
        public double Percent2 { get; set; }
        public double PriceUnit { get; set; }
        public double QuantityAmount { get; set; }
        public byte Relation { get; set; }
        public DateTime ToDate { get; set; }
    }
}