﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EISCS.Shop.DO.Dto.Backend.XAL
{
    public class XalProductTable
    {
        public int Enddisc { get; set; }
        public string ProductGrp { get; set; }
        public string ProductName { get; set; }
        public string ProductGuid { get; set; }
        public string Linedisc { get; set; }
        public string Multilndisc { get; set; }
        public double ListPrice { get; set; }
        public string SalesVatCode { get; set; }
        public string VatGroup { get; set; }
    }
}
