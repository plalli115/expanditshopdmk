﻿namespace EISCS.Shop.DO.Dto.Backend.XAL
{
    public class XAL_Parameters
    {
        public string Name { get; set; }
        public double Real1 { get; set; }
        public double Real2 { get; set; }
        public int UserID { get; set; }
    }
}