﻿using System;

namespace EISCS.Shop.DO.Dto.Backend.XAL
{
    public class XAL_VatCodeRate
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string VatCode { get; set; }
        public double VatPct { get; set; }
    }
}