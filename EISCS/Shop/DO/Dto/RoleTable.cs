﻿namespace EISCS.Shop.DO.Dto
{
    public class RoleTable
    {
        public string RoleId { get; set; }
        public string RoleDescription { get; set; }
        public bool ReadOnly { get; set; }
    }
}
