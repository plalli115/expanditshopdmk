﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EISCS.Shop.DO.Dto
{
    public class AccessTable
    {
        public string AccessClass { get; set; }
        public string ClassDescription { get; set; }
        public string AccessType { get; set; }
    }
}
