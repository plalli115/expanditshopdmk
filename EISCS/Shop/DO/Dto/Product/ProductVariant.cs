﻿namespace EISCS.Shop.DO.Dto.Product
{
    public class ProductVariant
    {
        public string ProductGuid { get; set; }
        public string VariantCode { get; set; }
        public string VariantName { get; set; }

        public string GetDisplayName()
        {
            if (string.IsNullOrEmpty(VariantName))
            {
                return VariantCode;
            }
            
            return VariantName;
            
        }
    }
}
