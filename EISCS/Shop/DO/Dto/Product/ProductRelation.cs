namespace EISCS.Shop.DO.Dto.Product
{
    public class ProductRelation
    {
        public string ProductGuid { get; set; }
        public int ProductRelationType { get; set; }
        public string RelatedToProductGuid { get; set; }
        public int SortIndex { get; set; }
		public bool IsFound { get; set; }
    }
}