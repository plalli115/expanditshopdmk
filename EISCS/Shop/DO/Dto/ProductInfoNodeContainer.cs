﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CmsPublic.ModelLayer.Models;
using EISCS.ExpandITFramework.Infrastructure;
using EISCS.Shop.DO.Dto;
using EISCS.Shop.DO.Dto.Cart;
using EISCS.Shop.DO.Dto.Product;
using EISCS.Shop.DO.Interface;

namespace EISCS.Shop.DO.Service
{
    public class ProductInfoNodeContainer
    {
        public List<ProductInfoNode> ProductInfoNodes { get; set; }
        public int TotalCountInDb { get; set; }
        public CartHeader Header { get; set; }

        public void Combine(CartHeader header, IEnumerable<Product> products, ProductVariantContainer productVariantContainer, PageMessages messages, bool oneProductPerVariant = false, List<FavoriteItem> favorites = null, List<ProductRelation> productRelations = null)
        {
            Header = header;
            var res = new List<ProductInfoNode>();

            if (oneProductPerVariant == false)
            {
                foreach (var product in products)
                {
                    string productGuid = product.ProductGuid;
                    var cartLine = header.Lines.FirstOrDefault(x => x.ProductGuid == productGuid);

                    ProductInfoNode productInfo = CreateProductInfoNode(productGuid, product, cartLine, productVariantContainer, favorites, messages, productRelations);
                    res.Add(productInfo);
                }
            }
            else
            {
                foreach (var cartLine in header.Lines)
                {
                    string productGuid = cartLine.ProductGuid;
                    var product = products.FirstOrDefault(x => x.ProductGuid == productGuid);
                    ProductInfoNode productInfo = CreateProductInfoNode(productGuid, product, cartLine, productVariantContainer, favorites, messages, productRelations);

                    res.Add(productInfo);
                }
            }

            ProductInfoNodes = res;
        }

        private ProductInfoNode CreateProductInfoNode(string productGuid, CmsPublic.ModelLayer.Models.Product product, CartLine cartLine, ProductVariantContainer productVariantContainer, IEnumerable<FavoriteItem> favorites, PageMessages messages, List<ProductRelation> productRelations = null)
        {
            if (favorites != null)
            {
                var favorit = favorites.Where(x => x.ProductGuid == productGuid);
                if (product != null)
                {
                    product.IsFavorite = favorit.Any();
                }
            }
            var productVariants = productVariantContainer.GetVariantsForProduct(productGuid);

            var productInfo = new ProductInfoNode
            {
                Line = cartLine,
                Prod = product,
                VariantDict = productVariants,
                Messages = messages,
                ProductRelations = productRelations
            };

            return productInfo;
        }

        public void PriceSortingAndPaging(string sort, string direction, int startIndex, int endIndex)
        {
            if (sort == ProductServiceConstants.PRICE_SORT && ProductInfoNodes.Any())
            {
                //the GetRange and db range is different based - one i zero based the other is 1 based
                startIndex = startIndex - 1;

                if (direction == "asc")
                {
                    ProductInfoNodes.Sort((a, b) => a.Line.ListPriceInclTax.CompareTo(b.Line.ListPriceInclTax));
                }
                else
                {
                    ProductInfoNodes.Sort((b, a) => a.Line.ListPriceInclTax.CompareTo(b.Line.ListPriceInclTax));
                }

                if (endIndex > 0)
                {
                    int pageSize = endIndex - startIndex;
                    if (endIndex > ProductInfoNodes.Count)
                    {
                        pageSize = ProductInfoNodes.Count - startIndex;
                    }
                    ProductInfoNodes = ProductInfoNodes.GetRange(startIndex, pageSize);
                }

                TotalCountInDb = ProductInfoNodes.Count;
            }
        }
    }

    public class ProductInfoNode
    {
        public CmsPublic.ModelLayer.Models.Product Prod { get; set; }
        public CartLine Line { get; set; }
        public Dictionary<string, ProductVariant> VariantDict { get; set; }
        public PageMessages Messages { get; set; }
        public List<ProductRelation> ProductRelations { get; set; }
    }

    public class ProductGuids
    {
        public string ProductGuid { get; set; }
        public double TotalQuantity { get; set; }
    }
}
