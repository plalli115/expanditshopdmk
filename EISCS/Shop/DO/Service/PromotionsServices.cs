﻿using System;
using System.Collections.Generic;
using System.Linq;
using CmsPublic.DataRepository;
using EISCS.ExpandITFramework.Infrastructure;
using EISCS.ExpandITFramework.Util;
using EISCS.Shop.BO.BusinessLogic;
using EISCS.Shop.DO.Dto;
using EISCS.Shop.DO.Dto.Cart;
using EISCS.Shop.DO.Dto.Product;
using EISCS.Shop.DO.Interface;
using EISCS.Wrappers.Configuration;
using EISCS.Shop.DO.Dto.Promotions;

namespace EISCS.Shop.DO.Service
{
    public class PromotionsServices : BaseDataService
    {

        private readonly IShopSalesService _shopSalesService;

        public PromotionsServices(IExpanditDbFactory dbFactory, IShopSalesService shopSalesService)
            : base(dbFactory)
        {
            _shopSalesService = shopSalesService;
        }

        public Promotion getPromoByCode(string PromoCode)
        {
            string sql = "SELECT * FROM Promotion WHERE PromotionCode = @PromoCode";
            Promotion p = GetSingle<Promotion>(sql, new { PromoCode });
            SetEntries(p);
            return p;
        }

        public Promotion getPromoById(string PromoGuid)
        {
            string sql = "SELECT * FROM Promotions WHERE PromotionGuid = @PromoGuid";
            Promotion p = GetSingle<Promotion>(sql, new { PromoGuid });
            SetEntries(p);
            return p;
        }

        public void SetEntries (Promotion p)
        {
            if (p != null)
            {
                string sql = "SELECT * FROM PromotionEntries WHERE PromotionGuid = @PromoGuid";
                p.PromotionEntries = GetResults<PromotionEntry>(sql, new { PromoGuid = p.PromotionGuid }).ToList<PromotionEntry>();
                SetEntryItems(p);
            }
        }

        public void SetEntryItems(Promotion p)
        {
            if (p != null)
            {
                foreach (PromotionEntry entry in p.PromotionEntries)
                {
                    string sql = "SELECT * FROM PromotionItemEntry WHERE PromotionItemListCode = @ItemCode";
                    entry.PromotionItemEntries = GetResults<PromotionItemEntry>(sql, new { ItemCode = entry.Code }).ToList<PromotionItemEntry>();
                }
            }
        }


        public bool PromotionEntryMet<T>(PromotionEntry promoEntry, BaseOrderHeader<T> cartHeader) where T : BaseOrderLine
        {
            if (promoEntry == null)
            {
                return false;
            }
            switch (promoEntry.PromotionType)
            {
                case 1: //Single item - buy the item, get the discount
                    if (cartHeader.Lines.Exists(u => u.ProductGuid == promoEntry.Code))
                    {
                        return true;
                    }
                    break;
                case 2: //Linked item - buy the linked item, get the discount off the first item
                    if (cartHeader.Lines.Exists(u => u.ProductGuid == promoEntry.Code) && cartHeader.Lines.Exists(u => u.ProductGuid == promoEntry.LinkedCode))
                    {
                        return true;
                    }
                    break;
                case 3: //Shipping to Item - buy the linked item, and match Shipping provider,  get discount off shipping
                    if (cartHeader.Lines.Exists(u => u.ProductGuid == promoEntry.LinkedCode))
                    {
                        if (String.IsNullOrEmpty(cartHeader.ShippingHandlingProviderGuid) || cartHeader.ShippingHandlingProviderGuid == promoEntry.Code)
                        {
                            return true;
                        }
                    }
                    break;
                case 4: //Shipping - Match Shipping provider, get discount off shipping
                    if (String.IsNullOrEmpty(cartHeader.ShippingHandlingProviderGuid) || cartHeader.ShippingHandlingProviderGuid == promoEntry.Code)
                    {
                        return true;
                    }
                    break;
                case 5: //Invoice - if amount spent satisified, add discount amt/% to CustInvDiscount
                case 9: //Gift($) - if amount spent statisifed, add gift product
                    if (cartHeader.SubTotal >= promoEntry.AmountOrQtyRequired)
                    {
                        return true;
                    }
                    break;
                case 6: //Product List($) - if amount spent on all products in PromoItemEntry satisfied, add discount amt/% to CustInvDiscount
                case 10: //Gift List($) - if amount spent on all products in PromoItemEntry satisfied, add gift product
                    double total_spent = 0;
                    foreach (PromotionItemEntry p in promoEntry.PromotionItemEntries)
                    {
                        BaseOrderLine prod = cartHeader.Lines.Find(l => l.ProductGuid == p.ItemNo);
                        if (prod != null)
                        {
                            total_spent += prod.ListPrice * prod.Quantity;
                        }
                    }
                    if (total_spent >= promoEntry.AmountOrQtyRequired)
                    {
                        return true;
                    }
                    break;
                case 7: //Product List (Qty.) - if # products (uniques) bought in matching PromoItemEntry group satisfied, add discount amt/% to CustInvDiscount
                case 11: //Gift List (Qty) - if # products (uniques) bought in matching PromoItemEntry group satisfied, add gift product
                    int total_bought = 0;
                    foreach (PromotionItemEntry p in promoEntry.PromotionItemEntries)
                    {
                        BaseOrderLine prod = cartHeader.Lines.Find(l => l.ProductGuid == p.ItemNo);
                        if (prod != null)
                        {
                            total_bought++;
                        }
                    }
                    if (total_bought >= promoEntry.AmountOrQtyRequired)
                    {
                        return true;
                    }
                    break;
                case 8: //Product List (All) - if purchased ALL products in PromoItemEntry, add discount amt/% to CustInvDiscount
                case 12://Gift List (All) - if purchased ALL products in PromoItemEntry, add gift product
                    bool all_bought = true;
                    foreach (PromotionItemEntry p in promoEntry.PromotionItemEntries)
                    {
                        BaseOrderLine prod = cartHeader.Lines.Find(l => l.ProductGuid == p.ItemNo);
                        if (prod == null)
                        {
                            all_bought = false;
                            break;
                        }
                    }
                    if (all_bought)
                    {
                        return true;
                    }
                    break;
                default:
                    throw new System.Exception("Unknown Promotion Type");
            }
            //if we get here, the case wasn't satisfied
            return false;
        }


        int PromotionUsedTimes(string promotionCode, DateTime startDate)
        {
            List<ShopSalesHeader> sales = _shopSalesService.GetLatestShopSalesInXDays((DateTime.Now - startDate).Days);
            int salesWithPromo = (from s in sales where s.PromotionCode == promotionCode select s.HeaderGuid).Count();
            return salesWithPromo;
        }
        int PromotionUsedByUserTimes(string promotionCode, DateTime startDate, string userGuid)
        {
            List<ShopSalesHeader> sales = _shopSalesService.GetLatestShopSalesInXDays((DateTime.Now - startDate).Days);
            int salesWithPromo = (from s in sales where s.PromotionCode == promotionCode && s.UserGuid == userGuid  select s.HeaderGuid).Count();
            return salesWithPromo;
        }

        public string isValid(Promotion promo, string userGuid)
        {
            if (DateTime.Compare(promo.PromotionEndDate, DateTime.Now) < 0)
            {
                return "Promotion '" + promo.PromotionCode + "' expired";
                
            }
            else if (DateTime.Compare(promo.PromotionStartDate, DateTime.Now) > 0)
            {
                return "Promotion '" + promo.PromotionCode + "' not yet valid";
            }
            else if (promo.NoOfOpportunities > 0 && PromotionUsedTimes(promo.PromotionCode, promo.PromotionStartDate) >= promo.NoOfOpportunities)
            {
                return "Promotion '" + promo.PromotionCode + "' uses surpassed limit"; 
            }
            else if (promo.NoOfOpportunities > 0 && PromotionUsedByUserTimes(promo.PromotionCode, promo.PromotionStartDate, userGuid) >= promo.NoOfOpportunitiesPerUser)
            {
                return "You have exceeded the limit of times  '" + promo.PromotionCode + "' can be used";
            }

            return String.Empty;

        }
    }
}
