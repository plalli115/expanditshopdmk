﻿using CmsPublic.DataRepository;

namespace EISCS.Shop.DO.Service
{
    public class BaseUserStorageService : BaseDataService
    {
        public BaseUserStorageService(IExpanditDbFactory dbFactory)
            : base(dbFactory)
        {
            InitializeData();
        }

        private void InitializeData()
        {
            const string createTables = @"
            IF NOT EXISTS (SELECT name FROM sysobjects WITH (NOLOCK) WHERE name = 'AccessTable' AND type = 'U')
            IF NOT EXISTS (SELECT name FROM sysobjects WITH (TABLOCKX) WHERE name = 'AccessTable' AND type = 'U') 
                CREATE TABLE AccessTable
                (
	                [AccessClass] [nvarchar] (50) NOT NULL,
	                [ClassDescription] [nvarchar] (100) NULL,
                    [AccessType] [nvarchar] (50) NULL,
	                CONSTRAINT [PK_AccessTable] PRIMARY KEY CLUSTERED
	                (
		                [AccessClass] ASC
	                )
                )
            
            IF NOT EXISTS (SELECT name FROM sysobjects WITH (NOLOCK) WHERE name = 'RoleTable' AND type = 'U')
            IF NOT EXISTS (SELECT name FROM sysobjects WITH (TABLOCKX) WHERE name = 'RoleTable' AND type = 'U') 
                CREATE TABLE RoleTable
                (
                    [RoleId] [nvarchar] (50) NOT NULL,
	                [RoleDescription] [nvarchar] (100) NULL,
	                [ReadOnly] [bit] NULL CONSTRAINT [DF_RoleTable_ReadOnly] DEFAULT ((0)),
	                CONSTRAINT [PK_RoleTable] PRIMARY KEY CLUSTERED
	                (
		                [RoleId] ASC
	                ) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY  = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
                ) ON [PRIMARY]

            IF NOT EXISTS (SELECT name FROM sysobjects WITH (NOLOCK) WHERE name = 'AccessRoles' AND type = 'U')
            IF NOT EXISTS (SELECT name FROM sysobjects WITH (TABLOCKX) WHERE name = 'AccessRoles' AND type = 'U') 
                CREATE TABLE AccessRoles
                (
	                [RoleId] [nvarchar] (50) NOT NULL,
	                [AccessClass] [nvarchar] (50) NOT NULL,
	                CONSTRAINT [PK_AccessRoles] PRIMARY KEY CLUSTERED
	                (
		                [RoleId] ASC,
		                [AccessClass] ASC
	                ) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY  = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
                ) ON [PRIMARY]           
            ";

            Execute(createTables);

            // Insert defaults
            const string insert = @"
                IF NOT EXISTS (SELECT TOP 1 AccessClass FROM AccessTable) 
                BEGIN
                INSERT INTO [AccessTable]
                ([AccessClass],[ClassDescription],[AccessType])
                VALUES	        
                    ('AccountManagement','Access to Account Management', 'BC'),
                    ('BusinessCenter','Access to the BC', 'BC'),
                    ('EditCatalog','Allow to edit catalog content', 'BC'),        
	                ('Cart', 'Access to use the order pad', 'SHOP'),
	                ('Catalog', 'Access to the catalog', 'SHOP'),
	                ('CreateServiceItem', 'Allow create service item', 'PORTAL'),
	                ('CreateServiceOrder', 'Access to creating service orders', 'PORTAL'),
	                ('CustomerSearch', 'Allow searching for customer', 'PORTAL'),                    
                    ('EditServiceItem', 'Allow edit service item', 'PORTAL'),
                    ('EditServiceOrder', 'Allow edit service order', 'PORTAL'),
                    ('EditServiceOrderCustomer', 'Allow edit service order customer', 'PORTAL'),
                    ('EditServiceOrderDepartment', 'Allow edit service order department', 'PORTAL'),
                    ('EditServiceOrderJobStatus', 'Allow edit service order job status', 'PORTAL'),
                    ('EditServiceOrderJobType', 'Allow edit service order job type', 'PORTAL'),
                    ('EditServiceOrderProject', 'Allow edit service order project', 'PORTAL'),
                    ('EditServiceOrderServiceManager', 'Allow edit service order service manager', 'PORTAL'),
                    ('EditServiceOrderTechnician', 'Allow edit service order technician', 'PORTAL'),
                    ('Favorites', 'Access to favorites', 'SHOP'),
                    ('HomePage', 'Access to the homepage', 'SHOP'),
                    ('ListAllCustomers', 'View all customers', 'PORTAL'),
                    ('ListCustomer', 'View users of same customer', 'PORTAL'),
                    ('ListCustomerGroup', 'View customers in a specific group', 'PORTAL'),
                    ('OpenSite', 'Access to create customer accounts', 'SHOP'),
                    ('Order', 'Access to order items', 'SHOP'),
                    ('Portal', 'Access to the service portal', 'PORTAL'),
                    ('Profile', 'Access to user profile', 'SHOP'),
                    ('ReadServiceOrderExternalComments', 'Allow read service order messages to invoice', 'PORTAL'),
                    ('ReadServiceOrderInternalComments', 'Allow read service order mesages to office', 'PORTAL')
                END

                IF NOT EXISTS (SELECT TOP 1 RoleId FROM RoleTable) 
                BEGIN
                INSERT INTO [RoleTable]
                    ([RoleId],[RoleDescription],[ReadOnly])
                VALUES
                    ('Admin', 'Administrator', 1),
                    ('Anonymous', 'Anonymous User', 1),
                    ('B2B', 'B2B User', 1),
                    ('B2C', 'B2C User', 1),
                    ('CatalogEditor', 'Access to the CMS', 0),                    
                    ('Customer', 'Access to certain customer data', 1),
                    ('CustomerGroup', 'Access to a certain group of customer data', 1),                    
                    ('Project', 'Access to certain projects', 0)
                END

                IF NOT EXISTS (SELECT TOP 1 AccessClass FROM AccessRoles) 
                BEGIN
                INSERT INTO [AccessRoles]
                    ([RoleId]
                    ,[AccessClass])
                VALUES
	                ('Admin', 'AccountManagement'),
                    ('Admin', 'BusinessCenter'),
                    ('Admin', 'Cart'),
                    ('Admin', 'Catalog'),
                    ('Admin', 'CreateServiceItem'),
                    ('Admin', 'CreateServiceOrder'),
                    ('Admin', 'CustomerSearch'),
                    ('Admin', 'EditCatalog'),
                    ('Admin', 'EditServiceItem'),
                    ('Admin', 'EditServiceOrder'),
                    ('Admin', 'EditServiceOrderCustomer'),
                    ('Admin', 'EditServiceOrderDepartment'),
                    ('Admin', 'EditServiceOrderJobStatus'),
                    ('Admin', 'EditServiceOrderJobType'),
                    ('Admin', 'EditServiceOrderProject'),
                    ('Admin', 'EditServiceOrderServiceManager'),
                    ('Admin', 'EditServiceOrderTechnician'),
                    ('Admin', 'Favorites'),
                    ('Admin', 'HomePage'),
                    ('Admin', 'ListAllCustomers'),
                    ('Admin', 'ListCustomer'),
                    ('Admin', 'ListCustomerGroup'),
                    ('Admin', 'OpenSite'),
                    ('Admin', 'Order'),
                    ('Admin', 'Portal'),
                    ('Admin', 'Profile'),
                    ('Anonymous', 'Cart'),
                    ('Anonymous', 'Catalog'),
                    ('Anonymous', 'EditCatalog'),
                    ('Anonymous', 'HomePage'),
                    ('Anonymous', 'OpenSite'),
                    ('B2B', 'AccountManagement'),
                    ('B2B', 'Cart'),
                    ('B2B', 'Catalog'),
                    ('B2B', 'CreateServiceOrder'),
                    ('B2B', 'EditCatalog'),
                    ('B2B', 'Favorites'),
                    ('B2B', 'HomePage'),
                    ('B2B', 'ListAllCustomers'),
                    ('B2B', 'ListCustomer'),
                    ('B2B', 'ListCustomerGroup'),
                    ('B2B', 'OpenSite'),
                    ('B2B', 'Order'),
                    ('B2B', 'Portal'),
                    ('B2B', 'Profile'),
                    ('B2C', 'Cart'),
                    ('B2C', 'Catalog'),
                    ('B2C', 'EditCatalog'),
                    ('B2C', 'Favorites'),
                    ('B2C', 'HomePage'),
                    ('B2C', 'OpenSite'),
                    ('B2C', 'Order'),
                    ('B2C', 'Profile'),
                    ('CatalogEditor', 'EditCatalog'),                    
                    ('Customer', 'AccountManagement'),
                    ('Customer', 'Cart'),
                    ('Customer', 'Catalog'),
                    ('Customer', 'CreateServiceOrder'),
                    ('Customer', 'EditCatalog'),
                    ('Customer', 'EditServiceItem'),
                    ('Customer', 'EditServiceOrder'),
                    ('Customer', 'EditServiceOrderCustomer'),
                    ('Customer', 'EditServiceOrderDepartment'),
                    ('Customer', 'EditServiceOrderJobStatus'),
                    ('Customer', 'EditServiceOrderJobType'),
                    ('Customer', 'EditServiceOrderProject'),
                    ('Customer', 'EditServiceOrderServiceManager'),
                    ('Customer', 'EditServiceOrderTechnician'),
                    ('Customer', 'Favorites'),
                    ('Customer', 'HomePage'),
                    ('Customer', 'OpenSite'),
                    ('Customer', 'Order'),
                    ('Customer', 'Portal'),
                    ('Customer', 'Profile'),
                    ('CustomerGroup', 'Cart'),
                    ('CustomerGroup', 'Catalog'),
                    ('CustomerGroup', 'CreateServiceOrder'),
                    ('CustomerGroup', 'EditCatalog'),
                    ('CustomerGroup', 'EditServiceItem'),
                    ('CustomerGroup', 'EditServiceOrder'),
                    ('CustomerGroup', 'EditServiceOrderCustomer'),
                    ('CustomerGroup', 'EditServiceOrderDepartment'),
                    ('CustomerGroup', 'EditServiceOrderJobStatus'),
                    ('CustomerGroup', 'EditServiceOrderJobType'),
                    ('CustomerGroup', 'EditServiceOrderProject'),
                    ('CustomerGroup', 'EditServiceOrderServiceManager'),
                    ('CustomerGroup', 'EditServiceOrderTechnician'),
                    ('CustomerGroup', 'Favorites'),
                    ('CustomerGroup', 'HomePage'),
                    ('CustomerGroup', 'OpenSite'),
                    ('CustomerGroup', 'Order'),
                    ('CustomerGroup', 'Portal'),
                    ('CustomerGroup', 'Profile'),                    
                    ('Project', 'EditCatalog')
                END			        
                ";

            Execute(insert);
        }
    }
}
