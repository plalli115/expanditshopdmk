﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using CmsPublic.DataRepository;
using EISCS.Shop.DO.Dto.Cart;
using EISCS.Shop.DO.Interface;
using EISCS.Wrappers.Configuration;

namespace EISCS.Shop.DO.Service
{
    public interface IProductSearchService
    {
        ProductInfoNodeContainer GetResults(string searchString, string languageGuid, string defaultLanguageGuid, string externalLanguageGuid, CartHeader header, string sort, string direction, int startIndex, int endIndex);
    }

    public class ProductSearchService : BaseDataService, IProductSearchService
    {
        private readonly IConfigurationObject _configuration;
        private readonly IProductService _productService;

        public ProductSearchService(IExpanditDbFactory dbFactory, IProductService productService, IConfigurationObject configuration)
            : base(dbFactory)
        {
            _productService = productService;
            _configuration = configuration;
        }

        public ProductInfoNodeContainer GetResults(string searchstring, string languageGuid, string defaultLanguageGuid, string externalLanguageGuid, CartHeader header, string sort, string direction, int startIndex, int endIndex)
        {
            searchstring = searchstring.Trim();

            if (!string.IsNullOrEmpty(searchstring))
            {
                string sql = SearchSql(searchstring, languageGuid, defaultLanguageGuid, externalLanguageGuid);
                List<string> results = GetResults<string>(sql).ToList();
                List<ProductInfoNode> list = _productService.GetProducts(header, results, sort, direction, startIndex, endIndex).ProductInfoNodes;

                return new ProductInfoNodeContainer { ProductInfoNodes = list, TotalCountInDb = results.Count(), Header = header};
            }

            return null;
        }

        private string SearchSql(string searchstring, string languageGuid, string defaultLanguageGuid, string externalLanguageGuid)
        {
            string pt;
            string sql;
            const bool includeProductguid = true;
            const bool includeProductname = true;
            const bool includeProductdescription = true;
            const int dbmaxresult = 100;
            string dbsearchstring = PrepareSearchString(searchstring);
            string collation = _configuration.Read("SEARCH_COLLATION");

            if (string.IsNullOrEmpty(collation))
            {
                collation = "Latin1_General_CI_AI";
            }

            if (ExpanditLib2.CBoolEx(_configuration.Read("REQUIRE_CATALOG_ENTRY")))
            {
                pt = "(SELECT ProductTable.ProductGuid, ProductTable.ProductName" +
                     " FROM ProductTable INNER JOIN (SELECT DISTINCT GroupProduct.ProductGuid FROM GroupProduct) AS GP" +
                     " ON ProductTable.ProductGuid=GP.ProductGuid) AS PT";
            }
            else
            {
                pt = "ProductTable AS PT";
            }

            externalLanguageGuid = GetExternalLanguageGuid(languageGuid);
            string ptt = "SELECT * FROM ProductTranslation WHERE CAST(LanguageGuid AS VarChar(38))";

            if (!string.IsNullOrWhiteSpace(externalLanguageGuid))
            {
                ptt += " = " + ExpanditLib2.SafeString(externalLanguageGuid);
            }
            else
            {
                ptt += " IS NULL";
            }
            
            if (ExpanditLib2.CBoolEx(_configuration.Read("USE_PRODUCTVARIANTTRANSLATION")))
            {
                ptt = ptt + " AND (VariantCode IS NULL OR VariantCode='')";
            }

            // Search for ProductGuid and ProductName in ProductTable and ProductTranslation

            if (includeProductguid || includeProductname)
            {
                // Build search SQL based on the use of the ProductTranslation table
                if (ExpanditLib2.CBoolEx(_configuration.Read("USE_PRODUCTTRANSLATION")))
                {
                    sql = "SELECT TOP " + dbmaxresult + " PT.ProductGuid FROM " + pt + " LEFT JOIN (" + ptt + ") AS PTT" + " ON PT.ProductGuid=PTT.ProductGuid" + " WHERE (";
                    if (includeProductguid)
                    {
                        sql = sql + " (PT.ProductGuid COLLATE " + collation + " LIKE " + ExpanditLib2.SafeString(dbsearchstring) + ")";
                    }
                    if (includeProductname)
                    {
                        if (includeProductguid)
                        {
                            sql = sql + " OR ";
                        }
                        sql = sql + " (PT.ProductName COLLATE " + collation + " LIKE " +
                              ExpanditLib2.SafeString(dbsearchstring) + " AND PTT.ProductGuid IS NULL) OR" + " (PTT.ProductName COLLATE " + collation + " LIKE " +
                              ExpanditLib2.SafeString(dbsearchstring) + ")";
                    }
                    sql = sql + ")";
                }
                else
                {
                    sql = "SELECT TOP " + dbmaxresult + " PT.ProductGuid FROM " + pt + " WHERE";
                    if (includeProductguid)
                    {
                        sql = sql + " (PT.ProductGuid COLLATE " + collation + " LIKE " +
                              ExpanditLib2.SafeString(dbsearchstring) + ")";
                    }
                    if (includeProductname)
                    {
                        if (includeProductguid)
                        {
                            sql = sql + " OR ";
                        }
                        sql = sql + " (PT.ProductName COLLATE " + collation + " LIKE " +
                              ExpanditLib2.SafeString(dbsearchstring) + ")";
                    }
                }
            }

            // Search for DESCRIPTION PropTransText in table PropTrans
            if (includeProductdescription)
            {
                string propertysql = PropertySearchSql("DESCRIPTION", "PRD", dbmaxresult, dbsearchstring, languageGuid, defaultLanguageGuid);
                if (!string.IsNullOrEmpty(propertysql))
                {
                    propertysql = "SELECT ProductTable.ProductGuid FROM ProductTable INNER JOIN (" + propertysql +
                                  ") AS PropertyHits ON ProductTable.ProductGuid=PropertyHits.PropOwnerRefGuid";

                    if (!string.IsNullOrEmpty(sql))
                    {
                        sql += " UNION ";
                    }
                    sql += propertysql;
                }
            }
            return sql;
        }

        private string PrepareSearchString(string searchstring)
        {
            // Patch wildcard chars in searchstring
            searchstring = "%" + searchstring + "%";
            searchstring = searchstring.Replace("*", "%");
            searchstring = searchstring.Replace("?", "_");

            // Improvement needed to handle [ and ]

            // Remove double LIKE_PERCENT
            searchstring = searchstring.Replace("%%", "%");
            return searchstring.ToLower();
        }

        private string PropertySearchSql(string propertyguid, string ownertypeguid, int dbmaxresult, string dbsearchstring, string languageguid, object defaultlanguageguid)
        {
            string collation = _configuration.Read("SEARCH_COLLATION");
            if (string.IsNullOrEmpty(collation))
            {
                collation = "Latin1_General_CI_AI";
            }

            string sql = "SELECT TOP " + dbmaxresult + " PropOwnerRefGuid FROM " + "(SELECT V1.PropOwnerRefGuid, V1.PropOwnerTypeGuid, Specific, Multi, Fallback FROM ";
            sql = sql +
                  "(SELECT PropVal.PropOwnerRefGuid, PropVal.PropOwnerTypeGuid, PT1.PropTransText AS Specific FROM PropVal LEFT JOIN (SELECT PropValGuid, PropTransText FROM PropTrans WHERE PropLangGuid=" +
                  ExpanditLib2.SafeString(languageguid) +
                  ") AS PT1 ON PropVal.PropValGuid=PT1.PropValGuid WHERE PropVal.PropGuid=" +
                  ExpanditLib2.SafeString(propertyguid) +
                  ") AS V1 " + ", " +
                  "(SELECT PropVal.PropOwnerRefGuid, PT1.PropTransText AS Multi FROM PropVal LEFT JOIN (SELECT PropValGuid, PropTransText FROM PropTrans WHERE PropLangGuid IS NULL) AS PT1 ON PropVal.PropValGuid=PT1.PropValGuid WHERE PropVal.PropGuid=" +
                  ExpanditLib2.SafeString(propertyguid) + ") AS V2 " + ", " +
                  "(SELECT PropVal.PropOwnerRefGuid, PT1.PropTransText AS Fallback FROM PropVal LEFT JOIN (SELECT PropValGuid, PropTransText FROM PropTrans WHERE PropLangGuid=" +
                  ExpanditLib2.SafeString(defaultlanguageguid) + ") AS PT1 ON PropVal.PropValGuid=PT1.PropValGuid WHERE PropVal.PropGuid=" +
                  ExpanditLib2.SafeString(propertyguid) + ") AS V3 " + "WHERE V1.PropOwnerRefGuid=V2.PropOwnerRefGuid AND V1.PropOwnerRefGuid=V3.PropOwnerRefGuid " +
                  "AND V1.PropOwnerTypeGuid=" +
                  ExpanditLib2.SafeString(ownertypeguid) + " AND " + "(Specific COLLATE " + collation + " LIKE " +
                  ExpanditLib2.SafeString(dbsearchstring) + " OR (Specific IS NULL AND Multi COLLATE " + collation + " LIKE " +
                  ExpanditLib2.SafeString(dbsearchstring) + ") OR (Specific IS NULL AND Multi IS NULL AND Fallback COLLATE " + collation + " LIKE " +
                  ExpanditLib2.SafeString(dbsearchstring) + "))) AS T ";
            if (ownertypeguid == "PRD" & ExpanditLib2.CBoolEx(_configuration.Read("REQUIRE_CATALOG_ENTRY")))
            {
                sql = sql + "INNER JOIN (SELECT DISTINCT ProductGuid FROM GroupProduct) AS GP ON T.PropOwnerRefGuid=GP.ProductGuid ";
            }

            return sql;
        }

        private string GetExternalLanguageGuid(string languageGuid)
        {
            string sql = "SELECT ExternalLanguageGuid FROM LanguageTable WHERE LanguageGuid = @languageGuid";
            return GetSingle<string>(sql, new {languageGuid});
        }
    }
}