﻿using System;
using System.Configuration;
using System.Linq;
using CmsPublic.DataProviders.Logging;
using CmsPublic.DataRepository;
using EISCS.ExpandITFramework.Util;
using EISCS.Shop.DO.Dto;
using EISCS.Shop.DO.Dto.Cart;
using EISCS.Shop.DO.Interface;
using Google.Authenticator;

namespace EISCS.Shop.DO.Service
{
    public class LoginService : BaseDataService, ILoginService
    {
        private readonly ICartService _cartService;
        private readonly IExpanditUserService _expanditUserService;
        private readonly IKeyPerformanceIndicatorsService _kpiService;
        private readonly IUserItemRepository _userItemRepository;

        public LoginService(IExpanditDbFactory dbFactory, IExpanditUserService expanditUserService, ICartService cartService, IKeyPerformanceIndicatorsService kpiService, IUserItemRepository userItemRepository)
            : base(dbFactory)
        {
            _expanditUserService = expanditUserService;
            _cartService = cartService;
            _kpiService = kpiService;
            _userItemRepository = userItemRepository;
        }

        public UserTable Impersonate(string userGuid)
        {
            UserTable userTable = _expanditUserService.GetUser(userGuid);

            if (userTable == null)
            {
                return null;
            }

            return Login(userTable.UserGuid, null, userTable.CustomerGuid) == ExpanditLoginStatus.LoggedIn ? userTable : null;
        }

        public ExpanditLoginStatus SecondFactorLogin(string userName, string password, string pin){
            string loggedInUserGuid;
            string loggedOutUserGuid = _expanditUserService.UserGuid;
            string loggedInCustomerGuid;
            bool isAuthorized = Authorize(userName, password, out loggedInUserGuid, out loggedInCustomerGuid);

            if (!isAuthorized)
            {
                return ExpanditLoginStatus.Unauthorized;
            }

            var user = _expanditUserService.GetUser(loggedInUserGuid);
            var secret = user.Secret;
            // Authorize PIN
            var tfa = new TwoFactorAuthenticator();
            isAuthorized = tfa.ValidateTwoFactorPIN(secret, pin);

            if (!isAuthorized)
            {
                return ExpanditLoginStatus.Unauthorized;
            }

            return Login(loggedInUserGuid, loggedOutUserGuid, loggedInCustomerGuid);
        }

        public ExpanditLoginStatus Login(string userName, string password)
        {
            string loggedInUserGuid;
            return Login(userName, password, out loggedInUserGuid);
        }

        public ExpanditLoginStatus Login(string userName, string password, out string loggedInUserGuid)
        {
            string loggedOutUserGuid = _expanditUserService.UserGuid;
            string loggedInCustomerGuid;
            bool isAuthorized = Authorize(userName, password, out loggedInUserGuid, out loggedInCustomerGuid);

            if (!isAuthorized)
            {
                return ExpanditLoginStatus.Unauthorized;
            }

            var currentUser = _expanditUserService.GetUser(loggedInUserGuid);

            // TODO: Find a way to determine what to do if a user is not approved and the account was created before this setting was used?
            // TODO: Currently checking LastLoginDate, but it might not be solid proof?
            if (!currentUser.IsApproved && currentUser.LastLoginDate == DateTime.MinValue)
            {
                return ExpanditLoginStatus.NeedApproval;
            }

            if (currentUser.MultiFactorAuthEnabled)
            {
                return ExpanditLoginStatus.AuthorizedNeedPin;
            }

            return Login(loggedInUserGuid, loggedOutUserGuid, loggedInCustomerGuid);
        }

        private ExpanditLoginStatus Login(string loggedInUserGuid, string loggedOutUserGuid, string loggedInCustomerGuid)
        {
            // Get and set currency guid to user preference
            UserTable userTable = _expanditUserService.GetUser(loggedInUserGuid);

            _expanditUserService.CurrencyGuid = !String.IsNullOrEmpty(userTable.CurrencyGuid) ? userTable.CurrencyGuid : _expanditUserService.GetDefaultCurrency;

            if (!String.IsNullOrEmpty(userTable.LanguageGuid))
            {
                _expanditUserService.LanguageGuid = userTable.LanguageGuid;
            }

            // Load Cart and Update Minicart
            Result result = _cartService.MergeUserCarts(loggedInUserGuid, loggedOutUserGuid);
            if (!String.IsNullOrEmpty(result.Error))
            {
                FileLogger.Log(result.Error);
            }

            CartHeader cartHeader = _cartService.GetCartById(loggedInUserGuid);
            if (cartHeader != null)
            {
                _cartService.Calculate(cartHeader);
                _expanditUserService.MiniCart = _cartService.UpdateMiniCart(cartHeader);
            }

            _expanditUserService.UserGuid = loggedInUserGuid;
            _expanditUserService.CustomerGuid = loggedInCustomerGuid;
            _kpiService.NewLogin(loggedInUserGuid);
            return ExpanditLoginStatus.LoggedIn;
        }

        public void Logout()
        {
            _expanditUserService.UserGuid = "";
            _expanditUserService.MiniCart = _cartService.UpdateMiniCart(null);
        }

        private bool Authorize(string userName, string password, out string userGuid, out string customerGuid)
        {
            userName = userName.Trim();
            password = password.Trim();
            userGuid = null;
            customerGuid = null;
            var chosenEncryptionType = ConfigurationManager.AppSettings["ENCRYPTION_TYPE"];
            var defaultBackendEncryptionType = ConfigurationManager.AppSettings["DEFAULT_BACKEND_ENCRYPTION_TYPE"];

            var param = new { userName };
            
            // Get value of EncryptionType and EncryptionSalt from UserTableB2B if they exist, else return null values for those fields
            string sql = @"
                ;WITH utb2b AS 
                (
                SELECT CustomerGuid, UserGuid, UserPassword, PasswordVersion FROM UserTableB2B WHERE EnableLogin<>0 AND ltrim(rtrim(LOWER(UserLogin)))=@userName
                )
                SELECT TOP 1 CustomerGuid, UserGuid, UserPassword, PasswordVersion, EncryptionSalt = 
                (SELECT 
	                (SELECT EncryptionSalt FROM utb2b AS u2
	                WHERE utb2b.UserGuid = u2.UserGuid
	                ) AS encColumn 
                FROM (SELECT NULL AS EncryptionSalt) AS dummy
                ), EncryptionType =
                (SELECT 
	                (SELECT EncryptionType FROM utb2b AS u2
	                WHERE utb2b.UserGuid = u2.UserGuid
	                ) AS encColumn 
                FROM (SELECT NULL AS EncryptionType) AS dummy
                )
                FROM utb2b
            ";

            UserTable userTableB2B = GetResults<UserTable>(sql, param).FirstOrDefault();

            if (userTableB2B != null)
            {
                var backendEncryptionType = userTableB2B.EncryptionType ?? defaultBackendEncryptionType;
                var backendEncryptionSalt = userTableB2B.EncryptionSalt ?? string.Empty;
                // Check that the corresponding customer exists in the CustomerTable
                sql = "SELECT CustomerGuid FROM CustomerTable WHERE EnableLogin<>0 AND ltrim(rtrim(CustomerGuid))=@customerGuid";

                var customerParam = new { customerGuid = userTableB2B.CustomerGuid };

                customerGuid = GetSingle<string>(sql, customerParam);

                if (String.IsNullOrEmpty(customerGuid))
                {
                    return false;
                }

                userGuid = userTableB2B.UserGuid;

                string sqlB2C = @"SELECT UserPassword, PasswordVersion, EncryptionType, EncryptionSalt FROM UserTable WHERE UserGuid = @userGuid";
                var additionalUserData = GetResults<UserTable>(sqlB2C, new { userGuid }).FirstOrDefault();
                
                if (additionalUserData != null)
                {
                    if (userTableB2B.PasswordVersion == additionalUserData.PasswordVersion)
                    {
                        return ValidatePasswordAndEncryptionType(userGuid, password, chosenEncryptionType, additionalUserData.UserPassword, additionalUserData.EncryptionType, additionalUserData.EncryptionSalt);
                    }
                    
                    // Must update UserTable with the new password and password version from UserTableB2B if password versions are different
                    // NOTE!: The encryption type for UserTableB2B is configurable!
                    bool success = Encryption.Encrypt(password, backendEncryptionSalt, backendEncryptionType) == userTableB2B.UserPassword;

                    if (success)
                    {
                        // Update UserTable with new password encrypted to the shop choosen encryption type
                       success = ValidatePasswordAndEncryptionType(userGuid, password, chosenEncryptionType, userTableB2B.UserPassword, backendEncryptionType, backendEncryptionSalt,
                            userTableB2B.PasswordVersion);
                        if (success)
                        {
                            // If user is promoted to B2B - update fields in UserTable
                            string updateSql = @"UPDATE UserTable SET CustomerGuid = @customerGuid, IsB2B = 1, PasswordVersion = @passwordVersion WHERE UserGuid = @userGuid";
                            Execute(updateSql, new { userGuid, customerGuid = userTableB2B.CustomerGuid, passwordVersion = userTableB2B.PasswordVersion});
                        }
                    }

                    return success;
                }

                // First time login for current B2B user
                // NOTE!: The encryption type for UserTableB2B is configurable!
                if (Encryption.Encrypt(password, backendEncryptionSalt, backendEncryptionType) == userTableB2B.UserPassword)
                {
                    string languageGuid = _expanditUserService.LanguageGuid;
                    // Copy b2b user with additional fields from CustomerTable to the UserTable
                    string insertSql =
                        @"
                        INSERT INTO UserTable (IsB2B, CompanyName, CurrencyGuid, CountryGuid, Address1, CityName, PhoneNo, ZipCode, CountyName, UserLogin, UserGuid, CustomerGuid, ContactName, EmailAddress, UserPassword, PasswordVersion, EncryptionType, EncryptionSalt, LanguageGuid, RoleId, IsApproved) 
                        SELECT 1 AS IsB2B, CompanyName, CurrencyGuid, CountryGuid, Address1, CityName, PhoneNo, ZipCode, CountyName, UserLogin, UserGuid, b2b.CustomerGuid, b2b.ContactName, b2b.EmailAddress, UserPassword, PasswordVersion, @backendEncryptionType, @backendEncryptionSalt, @languageGuid, 'B2B', b2b.EnableLogin
                        FROM UserTableB2B b2b INNER JOIN CustomerTable cust 
                        ON b2b.CustomerGuid = (cust.CustomerGuid COLLATE Finnish_Swedish_CI_AS) 
                        WHERE b2b.UserGuid = @userGuid 
                        AND b2b.CustomerGuid = @customerGuid;
                        IF NOT EXISTS(SELECT UserGuid FROM UserRole WHERE UserGuid = @userGuid)
                            INSERT INTO UserRole VALUES(@userGuid, 'B2B')";
                    bool success = Execute(insertSql, new { userGuid, customerGuid = userTableB2B.CustomerGuid, languageGuid, backendEncryptionType, backendEncryptionSalt });

                    if (success)
                    {
                        // Update UserTable with new password encrypted to the shop choosen encryption type
                        ValidatePasswordAndEncryptionType(userGuid, password, chosenEncryptionType, userTableB2B.UserPassword, backendEncryptionType, backendEncryptionSalt,
                            userTableB2B.PasswordVersion);

                        if (string.IsNullOrEmpty(userTableB2B.CurrencyGuid))
                        {
                            string defaultCurrencyGuid = _expanditUserService.GetDefaultCurrency;
                            Execute("UPDATE UserTable SET CurrencyGuid = @currencyGuid WHERE UserGuid = @userGuid", new { currencyGuid = defaultCurrencyGuid, userGuid, backendEncryptionType, backendEncryptionSalt });
                        }
                    }

                    return true;
                }

                return false;
            }

            sql = "SELECT TOP 1 CustomerGuid, UserGuid, UserPassword, EncryptionType, EncryptionSalt, IsApproved FROM UserTable WHERE ltrim(rtrim(LOWER(UserLogin)))=@userName";

            UserTable userTable = GetResults<UserTable>(sql, param).FirstOrDefault();
            
            if (userTable != null)
            {
                userGuid = userTable.UserGuid;

                return ValidatePasswordAndEncryptionType( userGuid, password, chosenEncryptionType, userTable.UserPassword, userTable.EncryptionType, userTable.EncryptionSalt);
            }

            return false;
        }

        public bool ValidatePasswordAndEncryptionType(string userGuid, string password, string chosenEncryptionType, string passwordInTable, string encryptionTypeInTable, string encryptionSaltInTable, string passwordVersion = null)
        {
            bool isPasswordValid;
            bool isBCryptInTable = !string.IsNullOrWhiteSpace(encryptionTypeInTable) && encryptionTypeInTable.Equals(Encryption.EncryptionTypes.BCrypt.ToString());
            bool isBCryptChosen = !string.IsNullOrWhiteSpace(chosenEncryptionType) && chosenEncryptionType.Equals(Encryption.EncryptionTypes.BCrypt.ToString());

            // if this is true, it means the encryption type in the table is outdated.
            if (encryptionTypeInTable != chosenEncryptionType)
            {
                // If EncryptionType is null - our best guess is that it's old data using XOR
                encryptionTypeInTable = string.IsNullOrWhiteSpace(encryptionTypeInTable) ? "XOR" : encryptionTypeInTable;

                bool useSalt = Encryption.UseEncryptionSalt(chosenEncryptionType);

                // See if the chosen encryption type is valid
                bool isEncryptionTypeValid = Encryption.ValidateEncryptionType(chosenEncryptionType);

                // See if the password is valid using the old encryption type in the table.
                // BCrypt uses its own password validation
                if (isBCryptInTable)
                {
                    isPasswordValid = BCrypt.CheckPassword(password, passwordInTable);
                }
                else
                {
                    isPasswordValid = Encryption.Encrypt(password, encryptionSaltInTable, encryptionTypeInTable) == passwordInTable;    
                }

                // If the password and encryption type is valid, update the password, encryption type and encryption salt with the new encryption, encryptiontype and encryption salt.
                if (isPasswordValid && isEncryptionTypeValid)
                {
                    string newEncryptionSalt = Encryption.GenerateSaltValue();
                    var encryptedPassword = Encryption.Encrypt(password, newEncryptionSalt, chosenEncryptionType);

                    if (passwordVersion == null)
                    {
                        _userItemRepository.UpdatePassword(userGuid, encryptedPassword, chosenEncryptionType, useSalt ? newEncryptionSalt : "NULL");    
                    }
                    else
                    {
                        _userItemRepository.UpdatePasswordWithVersion(userGuid, encryptedPassword, chosenEncryptionType, useSalt ? newEncryptionSalt : "NULL", passwordVersion);    
                    }
                }
            }
            else
            {
                // BCrypt uses its own password validation
                if (isBCryptChosen)
                {
                    isPasswordValid = BCrypt.CheckPassword(password, passwordInTable);
                }
                else
                {
                    isPasswordValid = Encryption.Encrypt(password, encryptionSaltInTable, chosenEncryptionType) == passwordInTable;    
                }
            }

            return isPasswordValid;
        }
    }
}