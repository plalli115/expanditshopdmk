﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using CmsPublic.DataRepository;
using Dapper;
using EISCS.Shop.DO.BAS.Dto;
using EISCS.Shop.DO.Dto;
using EISCS.Shop.DO.Interface;

namespace EISCS.Shop.DO.Service
{
    public class UserManagerStorageService : BaseUserStorageService, IUserManagerStorageService
    {
        public UserManagerStorageService(IExpanditDbFactory dbFactory)
            : base(dbFactory)
        {
        }

        // Manage Roles

        public bool AddRole(string roleId, string roleDescription, bool readOnly)
        {
            var roleParam = new { roleId, roleDescription, readOnly };

            const string sql = @"
                IF NOT EXISTS(SELECT RoleDescription FROM RoleTable WHERE RoleId = @roleId)
                BEGIN
	                INSERT INTO RoleTable VALUES(@roleId, @roleDescription, @readOnly)
                END
                ";
            return Execute(sql, roleParam);
        }

        public bool AddRoleAccess(string roleId, string accessClass)
        {
            var roleAccessParam = new { roleId, accessClass };

            const string sql = @"
                IF NOT EXISTS(SELECT AccessClass FROM AccessRoles WHERE RoleId = @roleId AND AccessClass = @accessClass)
                BEGIN
	                INSERT INTO AccessRoles VALUES(@roleId, @accessClass)	
                END
                ";
            return Execute(sql, roleAccessParam);
        }

        public bool RemoveRoleAccess(string roleId, string accessClass)
        {
            var roleAccessParam = new { roleId, accessClass };

            const string sql = @"
                DELETE FROM AccessRoles WHERE RoleId = @roleId AND AccessClass = @accessClass
                ";
            return Execute(sql, roleAccessParam);
        }

        public bool DeleteRole(string roleId)
        {
            const string sql = @"
                BEGIN
	            DELETE FROM AccessRoles WHERE RoleId = @roleId
	            DELETE FROM RoleTable WHERE  RoleId = @roleId                
                END
                ";
            return Execute(sql, new { roleId });
        }

        public int RoleCount()
        {
            return RoleCount(null);
        }

        public int RoleCount(string searchParameter)
        {
            string sql =
                @"SELECT COUNT(*) FROM RoleTable WHERE
                RoleId LIKE('%" + searchParameter + "%')" +
                "OR RoleDescription LIKE('%" + searchParameter + "%')";

            return GetResults<int>(sql).FirstOrDefault();
        }

        public IEnumerable<RoleTable> Roles(int start, int end, string column, string direction, string searchParameter)
        {
            var filter = String.Empty;
            if (searchParameter != null)
            {
                filter = @"WHERE (RoleId LIKE('%" + searchParameter + "%') " +
                         "OR RoleDescription LIKE('%" + searchParameter + "%'))";
                filter = filter.Replace("*", "%");
            }
            var query = String.Format(@"SELECT * FROM
            (SELECT ROW_NUMBER() OVER (ORDER BY (CASE WHEN [ReadOnly] IS NULL THEN 0 ELSE 1 END), [ReadOnly], {0} {1}, RoleId) as RowID, {2}.*           
            FROM {2}  {3}) temp
            WHERE RowID BETWEEN @start AND @end ORDER BY RowID", column, direction, "RoleTable", filter);
            var param = new
            {
                column,
                start,
                end
            };
            return GetResults<RoleTable>(query, param);
        }

        public RoleTable GetRole(string roleId)
        {
            const string sql = @"SELECT * FROM RoleTable WHERE RoleId = @roleId";
            return GetResults<RoleTable>(sql, new { roleId }).FirstOrDefault();
        }

        // End Manage Roles

        // Manage User Roles

        public bool SetUserRole(string userGuid, string roleId)
        {
            var userRoleParam = new { userGuid, roleId };
            const string sql = @"
            IF EXISTS(SELECT RoleDescription FROM RoleTable WHERE RoleId = @roleId)
            BEGIN
	            UPDATE UserRole
	            SET RoleId = @roleId
	            WHERE UserGuid = @userGuid
            END
            ";
            return Execute(sql, userRoleParam);
        }

        public bool AddUser(UserTable user)
        {
            throw new NotImplementedException();
        }

        public bool DeleteUser(UserTable user)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<AccessTable> GetAllAccesses()
        {
            const string sql = @"
                SELECT * FROM AccessTable
                ORDER BY AccessType, AccessClass
                ";
            return GetResults<AccessTable>(sql);
        }

        public IEnumerable<RoleTable> GetAllRoles()
        {
            const string sql = @"
                SELECT * FROM RoleTable                
                ORDER BY RoleId
                ";
            return GetResults<RoleTable>(sql);
        }

        public IEnumerable<AccessRoles> GetAllAccessRoles()
        {
            const string sql = @"
                SELECT * FROM AccessRoles
                ORDER BY RoleId, AccessClass
                ";
            return GetResults<AccessRoles>(sql);
        }

        public bool UpdateAccess(Dictionary<string, List<string>> keyValues)
        {
            const string deleteStatement = @"DELETE FROM AccessRoles WHERE RoleId = @roleId";
            const string insertStatement = @"INSERT INTO AccessRoles VALUES(@roleId, @accessClass)";
            bool isSuccess = false;
            foreach (var key in keyValues.Keys)
            {
                var param = new { roleId = key };
                isSuccess = Execute(deleteStatement, param);
                foreach (var keyValue in keyValues[key])
                {
                    var insertParam = new { roleId = key, accessClass = keyValue };
                    isSuccess = Execute(insertStatement, insertParam);
                }
            }
            return isSuccess;
        }

        public bool UpdateUserRole(string userGuid, string roleId)
        {
            const string sql =
                @"
                IF EXISTS (SELECT * FROM UserRole WHERE UserGuid = @userGuid)
                BEGIN
                UPDATE UserRole
                SET RoleId = @roleId
                WHERE UserGuid = @userGuid
                END
                ELSE
                INSERT INTO UserRole VALUES(@userGuid, @roleId);
                UPDATE UserTable
                SET RoleId = @roleId
                WHERE UserGuid = @userGuid";
            return Execute(sql, new { userGuid, roleId });
        }

        public bool UpdateUserRoles(string roleId, string substituteRoleId)
        {
            const string sql =
                @"UPDATE UserRole
                SET RoleId = @substituteRoleId
                WHERE RoleId = @roleId;
                UPDATE UserTable
                SET RoleId = @substituteRoleId
                WHERE RoleId = @roleId;"
                ;
            return Execute(sql, new { substituteRoleId, roleId });
        }

        // End Manage User Roles

        // List Customers

        public int GetCustomersCount()
        {
            return GetCustomersCount(null);
        }

        public int GetCustomersCount(string searchValue)
        {
            var filter = String.Empty;
            if (searchValue != null)
            {
                filter = @"WHERE (LOWER(CustomerGuid) LIKE(LOWER('%" + searchValue + "%')) " +
                         "OR LOWER(CompanyName) LIKE(LOWER('%" + searchValue + "%')) " +
                         "OR LOWER(CreditLimitLCY) LIKE(LOWER('%" + searchValue + "%')) " +
                         "OR LOWER(CurrencyGuid) LIKE(LOWER('%" + searchValue + "%')) " +
                         "OR LOWER(CountryGuid) LIKE(LOWER('%" + searchValue + "%')) " +
                         "OR LOWER(Address1) LIKE(LOWER('%" + searchValue + "%')) " +
                         "OR LOWER(CityName) LIKE(LOWER('%" + searchValue + "%')) " +
                         "OR LOWER(PhoneNo) LIKE(LOWER('%" + searchValue + "%')) " +
                         "OR LOWER(ZipCode) LIKE(LOWER('%" + searchValue + "%')) " +
                         ")";
                filter = filter.Replace("*", "%");
            }
            var query = string.Format(@"SELECT COUNT(*) FROM CustomerTable {0}", filter);
            return GetResults<int>(query).FirstOrDefault();
        }

        public string GetCustomerCompanyName(string customerGuid)
        {
            string sql = "SELECT CompanyName FROM CustomerTable WHERE CustomerGuid = @customerGuid";
            return GetSingle<string>(sql, new { customerGuid });
        }

        public IEnumerable<CustomerTableBase> Customers(int start, int end, string column, string direction, string searchParameter)
        {
            var filter = String.Empty;
            if (searchParameter != null)
            {
                filter = @"WHERE (LOWER(CustomerGuid) LIKE(LOWER('%" + searchParameter + "%')) " +
                         "OR LOWER(CompanyName) LIKE(LOWER('%" + searchParameter + "%')) " +
                         "OR LOWER(CreditLimitLCY) LIKE(LOWER('%" + searchParameter + "%')) " +
                         "OR LOWER(CurrencyGuid) LIKE(LOWER('%" + searchParameter + "%')) " +
                         "OR LOWER(CountryGuid) LIKE(LOWER('%" + searchParameter + "%')) " +
                         "OR LOWER(Address1) LIKE(LOWER('%" + searchParameter + "%')) " +
                         "OR LOWER(CityName) LIKE(LOWER('%" + searchParameter + "%')) " +
                         "OR LOWER(PhoneNo) LIKE(LOWER('%" + searchParameter + "%')) " +
                         "OR LOWER(ZipCode) LIKE(LOWER('%" + searchParameter + "%')) " +
                         ")";
                filter = filter.Replace("*", "%");
            }
            var query = String.Format(@"SELECT * FROM
            (SELECT ROW_NUMBER() OVER (ORDER BY {0} {1}, CustomerGuid) as RowID, {2}.*           
            FROM {2}  {3}) temp
            WHERE RowID BETWEEN @start AND @end ORDER BY RowID", column, direction, "CustomerTable", filter);
            var param = new
            {
                column,
                start,
                end
            };
            return GetResults<CustomerTableBase>(query, param);
        }

        public int UsersCount(string searchValue)
        {
            var filter = String.Empty;
            if (searchValue != null)
            {
                filter = @"WHERE (LOWER(ContactName) LIKE(LOWER('%" + searchValue + "%')) " +
                         "OR LOWER(Address1) LIKE(LOWER('%" + searchValue + "%')) " +
                         "OR LOWER(Address2) LIKE(LOWER('%" + searchValue + "%')) " +
                         "OR LOWER(CityName) LIKE(LOWER('%" + searchValue + "%')) " +
                         "OR LOWER(ZipCode) LIKE(LOWER('%" + searchValue + "%')) " +
                         "OR LOWER(CountryGuid) LIKE(LOWER('%" + searchValue + "%')) " +
                         "OR LOWER(EmailAddress) LIKE(LOWER('%" + searchValue + "%')) " +
                         "OR LOWER(UserLogin) LIKE(LOWER('%" + searchValue + "%')) " +
                         ")";
                filter = filter.Replace("*", "%");
            }
            var query = string.Format(@"SELECT COUNT(*) FROM UserTable {0}", filter);

            return GetResults<int>(query).FirstOrDefault();
        }

        public IEnumerable<UserTable> Users(int startIndex, int endIndex, string columnSort, string direction, string searchValue)
        {
            var filter = String.Empty;
            if (searchValue != null)
            {
                filter = @"WHERE (LOWER(ContactName) LIKE(LOWER('%" + searchValue + "%')) " +
                         "OR LOWER(Address1) LIKE(LOWER('%" + searchValue + "%')) " +
                         "OR LOWER(Address2) LIKE(LOWER('%" + searchValue + "%')) " +
                         "OR LOWER(CityName) LIKE(LOWER('%" + searchValue + "%')) " +
                         "OR LOWER(ZipCode) LIKE(LOWER('%" + searchValue + "%')) " +
                         "OR LOWER(CountryGuid) LIKE(LOWER('%" + searchValue + "%')) " +
                         "OR LOWER(EmailAddress) LIKE(LOWER('%" + searchValue + "%')) " +
                         "OR LOWER(UserLogin) LIKE(LOWER('%" + searchValue + "%')) " +
                         ")";
                filter = filter.Replace("*", "%");
            }
            var query = String.Format(@"SELECT * FROM
            (SELECT ROW_NUMBER() OVER (ORDER BY {0} {1}, UserGuid) as RowID, *,
            (SELECT TOP 1 HeaderDate FROM ShopSalesHeader WHERE UserGuid LIKE(UserTable.UserGuid) ORDER BY HeaderDate desc) AS LastOrder
            FROM {2} {3}) temp
            WHERE RowID BETWEEN @startIndex AND @endIndex ORDER BY RowID", columnSort, direction, "UserTable", filter);
            var param = new
            {
                columnSort,
                startIndex,
                endIndex
            };
            return GetResults<UserTable>(query, param).ToList();
        }

        public bool UpdateUserCustomer(string userGuid, string customerGuid)
        {
            const string sql =
                @"UPDATE UserTable
                SET CustomerGuid = @customerGuid
                WHERE UserGuid = @userGuid";
            return Execute(sql, new { userGuid, customerGuid });
        }

        public CustomerTableBase GetCustomer(string id)
        {
            string query = @"SELECT * FROM CustomerTable WHERE CustomerGuid = @customerGuid";
            return GetResults<CustomerTableBase>(query, new { customerGuid = id }).FirstOrDefault();
        }

        public bool UpdateRole(RoleTable role)
        {
            string query = string.Format("UPDATE RoleTable SET {0} WHERE RoleId = @roleId", GenerateUpdateParameters(role));
            return Execute(query, new { @roleId = role.RoleId, role.RoleDescription, role.ReadOnly});
        }
    }
}
