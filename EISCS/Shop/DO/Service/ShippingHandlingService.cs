﻿using System.Collections.Generic;
using System.Linq;
using EISCS.Shop.DO.Dto;
using EISCS.Shop.DO.Interface;
using EISCS.Shop.DO.Repository;

namespace EISCS.Shop.DO.Service
{
	public class ShippingHandlingService : IShippingHandlingService
	{
		private readonly IShippingHandlingProviderRepository _shippingHandlingProviderRepository;
		private readonly IShippingHandlingPriceRepository _shippingHandlingPriceRepository;
		private readonly IPaymentService _paymentService;
		private readonly IExpanditUserService _userService;

		public ShippingHandlingService(IShippingHandlingProviderRepository shippingHandlingProviderRepository, IShippingHandlingPriceRepository shippingHandlingPriceRepository, IPaymentService paymentService, IExpanditUserService userService)
		{
			_shippingHandlingProviderRepository = shippingHandlingProviderRepository;
			_shippingHandlingPriceRepository = shippingHandlingPriceRepository;
			_paymentService = paymentService;
			_userService = userService;
		}

		public List<ShippingHandlingProvider> GetShippingHandlingProviders()
		{
		    var userType = _userService.UserType;
			var providersGuidList = _paymentService.GetProvidersByUser(userType.ToString());
			var shippingHandlingProviders = providersGuidList.Select(providerGuid => _shippingHandlingProviderRepository.GetShippingHandlingProvider(providerGuid)).ToList();

			foreach (var provider in shippingHandlingProviders)
			{
				provider.ShippingHandlingPrices = _shippingHandlingPriceRepository.GetShippingHandlingPrice(provider.ShippingHandlingProviderGuid);
			}

			return shippingHandlingProviders;
		}

		public ShippingHandlingProvider GetShippingHandlingProvider(string shippingProviderGuid)
		{
			var provider = _shippingHandlingProviderRepository.GetShippingHandlingProvider(shippingProviderGuid);
			provider.ShippingHandlingPrices = _shippingHandlingPriceRepository.GetShippingHandlingPrice(shippingProviderGuid);
			return provider;
		}
	}
}