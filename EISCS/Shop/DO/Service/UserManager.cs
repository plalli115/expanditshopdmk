﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using EISCS.ExpandITFramework.Util;
using EISCS.Shop.DO.Dto;
using EISCS.Shop.DO.Interface;
using EISCS.Shop.DO.PostData;

namespace EISCS.Shop.DO.Service
{
    public class UserManager : IUserManager
    {
        private readonly IUserManagerStorageService _userStorage;
        private readonly IExpanditUserService _expanditUserService;
        private readonly ICartService _cartService;
        private readonly IUserItemRepository _userItemRepository;

        public UserManager(IUserManagerStorageService userStorage, IExpanditUserService expanditUserService, ICartService cartService, IUserItemRepository userItemRepository)
        {
            _userStorage = userStorage;
            _expanditUserService = expanditUserService;
            _cartService = cartService;
            _userItemRepository = userItemRepository;
        }

        // Manage Users

        public UserTable GetUser(string userGuid)
        {
            return _expanditUserService.GetUser(userGuid);
        }

        public bool AddUser(UserTable user)
        {
            if ((_userItemRepository.GetUserByUserName(user.UserLogin) == null))
            {
                return _userItemRepository.Add(user) != null;
            }
            return false;
        }

        public bool DeleteUser(UserTable user)
        {
            return _userItemRepository.Remove(user.UserGuid);
        }

        public bool UpdateUser(UserTable user, bool noPasswordCheck){
            if (!noPasswordCheck)
            {
                return UpdateUser(user);
            }
            user.UserModified = DateTime.Now;
            if (user.UserCreated.Equals(DateTime.MinValue))
                user.UserCreated = DateTime.Now;
            user.PasswordResetRequestTime = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;
            return _userItemRepository.Update(user);
        }

        public bool UpdateUser(UserTable user)
        {
            var chosenEncryptionType = ConfigurationManager.AppSettings["ENCRYPTION_TYPE"];
            var encryptionSalt = Encryption.GenerateSaltValue();
            var userGuid = _expanditUserService.UserGuid;    

            var currentUser = _expanditUserService.GetUser(userGuid);
            var currentPassword = currentUser.UserPassword;
            var encryptedPassword = Encryption.Encrypt(user.UserPassword, encryptionSalt, chosenEncryptionType);
            bool useSalt = Encryption.UseEncryptionSalt(chosenEncryptionType);
            if (!encryptedPassword.Equals(currentPassword))
            {
                return false;
            }

            user.UserPassword = encryptedPassword;
            user.EncryptionType = chosenEncryptionType;
            user.EncryptionSalt = useSalt ? encryptionSalt : "NULL";
            user.UserGuid = userGuid;
            user.UserModified = DateTime.Now;
            user.UserCreated = currentUser.UserCreated;
            user.UserLogin = currentUser.UserLogin;
            if (user.UserCreated.Equals(DateTime.MinValue))
                user.UserCreated = DateTime.Now;
            user.PasswordResetRequestTime = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;
            
            SetCartData(user);

            return _userItemRepository.Update(user);
        }

        private void SetCartData(UserTable user)
        {
            var cartHeader = _cartService.GetCartById(user.UserGuid);

            if (cartHeader != null)
            {
                cartHeader.ContactName = user.ContactName;
                cartHeader.EmailAddress = user.EmailAddress;
                cartHeader.CompanyName = user.CompanyName;
                cartHeader.CountryGuid = user.CountryGuid;
                cartHeader.CurrencyGuid = user.CurrencyGuid;
                cartHeader.Address1 = user.Address1;
                cartHeader.Address2 = user.Address2;
                cartHeader.CityName = user.CityName;
                cartHeader.ZipCode = user.ZipCode;
                cartHeader.CountryName = _expanditUserService.GetCountryName(user.CountryGuid);
                cartHeader.ModifiedDate = DateTime.Now;
                _cartService.SaveCartHeader(cartHeader);
            }
        }

        public bool ManagerUpdateUser(UserTable user)
        {
            var currentUser = _expanditUserService.GetUser(user.UserGuid);
            user.UserCreated = currentUser.UserCreated;
            if (user.UserCreated.Equals(DateTime.MinValue))
                user.UserCreated = DateTime.Now;
            user.UserModified = DateTime.Now;
            user.UserPassword = currentUser.UserPassword;
            SetCartData(user);
            return _userItemRepository.Update(user);
        }

        public bool UpdatePassword(UserPasswordPostData postData)
        {
            var chosenEncryptionType = ConfigurationManager.AppSettings["ENCRYPTION_TYPE"];
            var newEncryptionSalt = Encryption.GenerateSaltValue();
            var userGuid = _expanditUserService.UserGuid;
            var currentUser = _expanditUserService.GetUser(userGuid);
            var currentPassword = currentUser.UserPassword;
            var currentEncryptionType = currentUser.EncryptionType;
            var currentEncryptionSalt = currentUser.EncryptionSalt;
            bool useSalt = Encryption.UseEncryptionSalt(chosenEncryptionType);
            bool isBCryptInTable = currentEncryptionType.Equals(Encryption.EncryptionTypes.BCrypt.ToString());
            bool isPasswordValid = false;

            // BCrypt uses a built-in password check
            if (isBCryptInTable && BCrypt.CheckPassword(postData.CurrentUserPassword, currentPassword))
            {
                _userItemRepository.UpdatePassword(userGuid, Encryption.Encrypt(postData.NewUserPassword, newEncryptionSalt, chosenEncryptionType), chosenEncryptionType, useSalt ? newEncryptionSalt : "NULL");
                isPasswordValid = true;
            }
            else if (Encryption.Encrypt(postData.CurrentUserPassword, currentEncryptionSalt, currentEncryptionType).Equals(currentPassword))
            {
                _userItemRepository.UpdatePassword(userGuid, Encryption.Encrypt(postData.NewUserPassword, newEncryptionSalt, chosenEncryptionType), chosenEncryptionType, useSalt ? newEncryptionSalt : "NULL");
                isPasswordValid = true;
            }

            return isPasswordValid;

        }

        public string UpdateLostPassword(string email, string userName, int length, out string contactName)
        {
            var chosenEncryptionType = ConfigurationManager.AppSettings["ENCRYPTION_TYPE"];
            Encryption.ValidateEncryptionType(chosenEncryptionType);
            string newEncryptionSalt = Encryption.GenerateSaltValue();
            bool useSalt = Encryption.UseEncryptionSalt(chosenEncryptionType);
            var userTable = _userItemRepository.GetUserByEmailAndUserLogin(email, userName);
            if (userTable != null)
            {
                // User is found
                string password = Utilities.GetRandomPassword(length);
                _userItemRepository.UpdatePassword(userTable.UserGuid, Encryption.Encrypt(password, newEncryptionSalt, chosenEncryptionType), chosenEncryptionType, useSalt ? newEncryptionSalt : "NULL");
                contactName = userTable.ContactName;
                return password;
            }
            contactName = null;
            return null;
        }

        // Manage Roles

        public bool AddRole(string roleId, string roleDescription, bool readOnly)
        {
            return _userStorage.AddRole(roleId, roleDescription, readOnly);
        }

        public bool AddRoleAccess(string roleId, string accessClass)
        {
            return _userStorage.AddRoleAccess(roleId, accessClass);
        }

        public bool RemoveRoleAccess(string roleId, string accessClass)
        {
            return _userStorage.RemoveRoleAccess(roleId, accessClass);
        }

        public bool DeleteRole(string roleId, string substituteRoleId)
        {
            bool success = _userStorage.DeleteRole(roleId);
            if (!string.IsNullOrWhiteSpace(substituteRoleId))
            {
                success = _userStorage.UpdateUserRoles(roleId, substituteRoleId);
            }
            return success;
        }
        
        public int RoleCount()
        {
            return _userStorage.RoleCount();
        }

        public int RoleCount(string searchParameter)
        {
            return _userStorage.RoleCount(searchParameter);
        }

        public IEnumerable<RoleTable> Roles(int start, int end, string column, string direction, string searchParameter)
        {
            return _userStorage.Roles(start, end, column, direction, searchParameter);
        }

        public RoleTable GetRole(string roleId)
        {
            return _userStorage.GetRole(roleId);
        }

        // End Manage Roles

        // Manage User Roles

        public bool SetUserRole(string userGuid, string roleId)
        {
            return _userStorage.SetUserRole(userGuid, roleId);
        }

        public IEnumerable<AccessTable> GetAllAccesses()
        {
            return _userStorage.GetAllAccesses();
        }

        public IEnumerable<RoleTable> GetAllRoles()
        {
            return _userStorage.GetAllRoles();
        }

        public IEnumerable<AccessRoles> GetAllAccessRoles()
        {
            return _userStorage.GetAllAccessRoles();
        }

        public bool UpdateAccess(Dictionary<string, List<string>> keyValues)
        {
            return _userStorage.UpdateAccess(keyValues);
        }

        public bool UpdateUserRole(string userGuid, string roleId)
        {
            if (string.IsNullOrWhiteSpace(userGuid) || string.IsNullOrWhiteSpace(roleId))
            {
                return false;
            }
            return _userStorage.UpdateUserRole(userGuid, roleId);
        }

        public int UserCount()
        {
            return UserCount(null);
        }

        public int UserCount(string searchParameter)
        {
            return _userStorage.UsersCount(searchParameter);
        }

        public IEnumerable<UserTable> Users(int start, int end, string column, string direction, string searchParameter)
        {
            return _userStorage.Users(start, end, column, direction, searchParameter);
        }

        // End Manage User Roles

        // List Customers

        public int CustomerCount()
        {
            return _userStorage.GetCustomersCount(null);
        }

        public int CustomerCount(string searchParameter)
        {
            return _userStorage.GetCustomersCount(searchParameter);
        }

        public IEnumerable<CustomerTableBase> Customers(int start, int end, string column, string direction, string searchParameter)
        {
            return _userStorage.Customers(start, end, column, direction, searchParameter);
        }

        public bool UpdateUserCustomer(string userGuid, string customerGuid)
        {
            return _userStorage.UpdateUserCustomer(userGuid, customerGuid);
        }

        public CustomerTableBase GetCustomer(string id)
        {
            return _userStorage.GetCustomer(id);
        }

        public bool UpdateRole(RoleTable role)
        {
            return _userStorage.UpdateRole(role);
        }

        public string CustomerCompanyName(string customerGuid)
        {
            return _userStorage.GetCustomerCompanyName(customerGuid);
        }
    }
}
