﻿using System.Collections.Generic;
using CmsPublic.DataRepository;
using EISCS.ExpandITFramework.Util;
using EISCS.Shop.DO.Dto;
using EISCS.Shop.DO.Dto.Product;
using EISCS.Shop.DO.Interface;

namespace EISCS.Shop.DO.Service
{
    public class ProductVariantService : BaseDataService, IProductVariantService
    {
        public ProductVariantService(IExpanditDbFactory dbFactory)
            : base(dbFactory)
        {
        }
        
        public virtual ProductVariantContainer GetVariants(IEnumerable<string> productGuids, string languageGuid)
        {
            const string query = "IF COL_LENGTH('ProductTranslation','VariantCode') IS NOT NULL BEGIN SELECT '1' END";
            bool hasVariantCode = !GetSingle<string>(query).IsNullOrEmpty();

            string sqlQuery = "SELECT * FROM ProductVariant WHERE ProductGuid IN @productGuids";

            if (hasVariantCode)
            {
                sqlQuery = @"
                SELECT DISTINCT ProductVariant.VariantCode, ProductVariant.ProductGuid, IsNull(CAST(ProductTranslation.ProductName AS NVARCHAR(50)), ProductVariant.VariantName) AS VariantName FROM
                ProductVariant LEFT JOIN ProductTranslation
                ON (ProductVariant.VariantCode = ProductTranslation.VariantCode AND ProductTranslation.LanguageGuid = (SELECT IsNull(ExternalLanguageGuid, @languageGuid) FROM LanguageTable WHERE LanguageGuid = @languageGuid))
                WHERE ProductVariant.ProductGuid IN @productGuids 
            ";
            }

            var param = new { productGuids, languageGuid };

            IEnumerable<ProductVariant> variants = GetResults<ProductVariant>(sqlQuery, param);

            var container = new ProductVariantContainer();
            container.SetVariants(variants);
            return container;
        }
    }
}