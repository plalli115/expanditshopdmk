﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Caching;
using System.Web.Security;
using AutoMapper;
using CmsPublic.DataProviders.Logging;
using EISCS.ExpandITFramework.Util;
using EISCS.Shop.BO.BAS;
using EISCS.Shop.DO.Dto;
using EISCS.Shop.DO.Interface;
using EISCS.Wrappers.Configuration;
using Newtonsoft.Json;

namespace EISCS.Shop.DO.Service
{
    public enum UserClass
    {
        Anonymous,
        B2C,
        B2B
    }

    public class ExpanditUserService : IExpanditUserService
    {
        private const string SHOP_COOKIE_NAME = "store";
        private const string USER_COOKIE_NAME = "user";
        private const string USER_GUID = "UserGuid";
        private const string USER_MAIL = "UserMail";
        private const string CUSTOMER_GUID = "CustomerGuid";
        private const string USER_LANGUAGE_NAME = "LanguageGuid";
        private const string USER_CURRENCY_NAME = "CurrencyGuid";
        private const string SHOP_DEFAULT_LANGUAGE = "LANGUAGE_DEFAULT";
        private const string SHOP_DEFAULT_CURRENCY = "MULTICURRENCY_SITE_CURRENCY";
        private const string MINI_CART_COOKIE_NAME = "NewMiniCart";
        private const string MINI_CART_COOKIE_KEY = "NewMarshallMiniCartInfo";
        private readonly IConfigurationObject _configurationObject;
        private readonly IUserStorageService _userStorage;
        private readonly HttpContextBase _httpContext;
        private string _currencyGuid;
        private MiniCartItem _miniCart;
        private readonly ICurrencyRepository _currencyRepository;
        private Dictionary<string, object> UserInformationStore { get; set; }

        //TEST
        public string UniqueIdentity { get; private set; }

        // Portal Related
        private UserTable ReturnPortalUser(UserTable user)
        {
            if (user == null || _configurationObject.Read("ENABLE_SERVICE_PORTAL") != "1")
            {
                return null;
            }
            
            // Set users CustomerGroup - default to BillToCustomer
            if (user.CustomerGuid != null)
            {
                user.CustomerGroupId = _userStorage.GetCustomerGroup(user.CustomerGuid);
            }

            return user;
        }

        public UserTable GetPortalUser()
        {
            return ReturnPortalUser(GetUser());
        }

        public UserTable GetPortalUser(string id)
        {
            return ReturnPortalUser(GetUser(id));
        }

        public IEnumerable<UserTable> GetAllB2BUsers()
        {
            return _userStorage.GetAllB2BUsers();
        }

        public IEnumerable<UserTable> GetAllUsers()
        {
            return _userStorage.GetAllUsers();
        }

        public int GetAllUsersCount()
        {
            return _userStorage.GetAllUsersCount();
        }

        public bool NewLogin(string userGuid)
        {
            return _userStorage.NewLogin(userGuid);
        }

        // End Portal Related

        public ExpanditUserService(IUserStorageService userStorage,
            HttpContextBase httpContext,
            IConfigurationObject config,
            ICurrencyRepository currencyRepository)
        {
            _userStorage = userStorage;
            _httpContext = httpContext;
            _configurationObject = config;
            _currencyRepository = currencyRepository;
            UserInformationStore = new Dictionary<string, object>();
            // TEST
            UniqueIdentity = Guid.NewGuid().ToString();
        }

        public UserClass UserType
        {
            get
            {
                object result = GetFromUserInfoStore(UserGuid);
                if (result == null)
                {
                    GetUser();
                    result = GetFromUserInfoStore(UserGuid);
                }
                return result == null ? UserClass.Anonymous : ((UserInfoNode)result).UserTypeClass;
            }
        }

        public UserTable GetUser()
        {
            return GetUser(UserGuid);
        }

        public UserTable GetUser(string userGuid)
        {
            object result = GetFromUserInfoStore(userGuid);
            if (result != null)
            {
                var node = (UserInfoNode)result;
                return node.UserTableItem;
            }

            var customerGuid = _userStorage.FindCustomerGuidByUser(userGuid);

            var isB2B = customerGuid != null;

            UserTable userTable;
            UserInfoNode userInfoNode;
            if (isB2B)
            {
                userTable = _userStorage.GetUserByCustomerAndUserGuid(customerGuid, userGuid);
                if (userTable != null)
                {
                    var additionalUserData = _userStorage.GetUserByUserGuid(userGuid);

                    if (additionalUserData != null)
                    {
                        if (userTable.PasswordVersion != additionalUserData.PasswordVersion)
                        {
                            UserGuid = "";
                            _httpContext.Response.Redirect("~/");
                            return null;
                        }
                        if (!string.IsNullOrWhiteSpace(additionalUserData.RoleId))
                        {
                            userTable.RoleId = additionalUserData.RoleId;
                        }

                        userTable.UserCreated = additionalUserData.UserCreated;
                        userTable.UserModified = additionalUserData.UserModified;
                        userTable.LastOrder = additionalUserData.LastOrder;
                        userTable.LastLoginDate = additionalUserData.LastLoginDate;
                        userTable.IsApproved = additionalUserData.IsApproved;
                        userTable.EncryptionType = additionalUserData.EncryptionType;
                        userTable.EncryptionSalt = additionalUserData.EncryptionSalt;
                        userTable.MultiFactorAuthEnabled = additionalUserData.MultiFactorAuthEnabled;
                    }

                    userInfoNode = new UserInfoNode { UserTableItem = userTable, UserTypeClass = UserClass.B2B };
                    AddToUserInfoStore(userInfoNode, userGuid);

                    userTable.UserStorage = _userStorage;
                    return userTable;
                }
            }

            userTable = _userStorage.GetUserByUserGuid(userGuid);

            if (userTable != null)
            {
                userInfoNode = new UserInfoNode { UserTableItem = userTable, UserTypeClass = UserClass.B2C };
                AddToUserInfoStore(userInfoNode, userGuid);
                userTable.UserStorage = _userStorage;
                return userTable;
            }

            userInfoNode = new UserInfoNode { UserTableItem = null, UserTypeClass = UserClass.Anonymous };
            AddToUserInfoStore(userInfoNode, userGuid);

            return null;
        }

        private void AddToUserInfoStore(object obj, string id)
        {
            if (UserInformationStore == null)
            {
                UserInformationStore = new Dictionary<string, object>();
            }

            if (UserInformationStore.ContainsKey(id))
            {
                UserInformationStore[id] = obj;
            }
            else
            {
                UserInformationStore.Add(id, obj);
            }
        }

        private object GetFromUserInfoStore(string id)
        {
            if (UserInformationStore == null || !UserInformationStore.ContainsKey(id))
            {
                return null;
            }
            return UserInformationStore[id];
        }

        public string UserGuid
        {
            get
            {
                string userGuid = GetValueFromCookie(USER_GUID, SHOP_COOKIE_NAME);
                userGuid = string.IsNullOrEmpty(userGuid) ? GetValueFromSession(USER_GUID) : userGuid;
                return string.IsNullOrEmpty(userGuid) ? UserGuid = Guid.NewGuid().ToString() : userGuid;
            }
            set
            {
                SetCookieValue(SHOP_COOKIE_NAME, USER_GUID, value);
                SetSessionValue(USER_GUID, value);
            }
        }

        public string CustomerGuid
        {
            get
            {
                string customerGuid = GetValueFromCookie(CUSTOMER_GUID, SHOP_COOKIE_NAME);
                customerGuid = string.IsNullOrEmpty(customerGuid) ? GetValueFromSession(CUSTOMER_GUID) : customerGuid;
                return string.IsNullOrEmpty(customerGuid) ? GetValueFromConfiguration("ANONYMOUS_CUSTOMERGUID") : customerGuid;
            }
            set
            {
                SetCookieValue(SHOP_COOKIE_NAME, CUSTOMER_GUID, value);
                SetSessionValue(CUSTOMER_GUID, value);
            }
        }

        public string LanguageGuid
        {
            get
            {
                string languageGuid = GetValueFromCookie(USER_LANGUAGE_NAME, USER_COOKIE_NAME);

                if (string.IsNullOrEmpty(languageGuid))
                {
                    languageGuid = GetValueFromSession(USER_LANGUAGE_NAME);
                }

                return string.IsNullOrEmpty(languageGuid) ? GetValueFromConfiguration(SHOP_DEFAULT_LANGUAGE) : languageGuid;
            }
            set
            {
                SetCookieValue(USER_COOKIE_NAME, USER_LANGUAGE_NAME, value);
                SetSessionValue(USER_LANGUAGE_NAME, value);
            }
        }

        public string CurrencyGuid
        {
            get
            {
                if (!string.IsNullOrEmpty(_currencyGuid))
                {
                    return _currencyGuid;
                }

                if (UserType == UserClass.B2B || UserType == UserClass.B2C)
                {
                    var userTable = GetUser();
                    if (!string.IsNullOrEmpty(userTable.CurrencyGuid))
                    {
                        var currencyItems = _currencyRepository.GetValid().ToDictionary(c => c.Id, c => c.Name);

                        if (!currencyItems.ContainsKey(userTable.CurrencyGuid))
                            userTable.CurrencyGuid = GetValueFromConfiguration(SHOP_DEFAULT_CURRENCY);
                        return userTable.CurrencyGuid;
                    }
                }

                var currencyGuid = GetValueFromCookie(USER_CURRENCY_NAME, USER_COOKIE_NAME);

                if (string.IsNullOrEmpty(currencyGuid))
                {
                    currencyGuid = GetValueFromSession(USER_CURRENCY_NAME);
                }

                return string.IsNullOrEmpty(currencyGuid) ? GetDefaultCurrency : currencyGuid;
            }
            set
            {
                _currencyGuid = value;
                SetCookieValue(USER_COOKIE_NAME, USER_CURRENCY_NAME, value);
                SetSessionValue(USER_CURRENCY_NAME, value);
            }
        }

        public string UserEmail
        {
            get
            {
                string userGuid = UserGuid;

                string userEmail = GetValueFromCookie(USER_MAIL, USER_COOKIE_NAME);

                if (string.IsNullOrEmpty(userEmail))
                {
                    var user = GetUser(userGuid);
                    userEmail = user.EmailAddress;
                }

                return userEmail;
            }
            set
            {
                SetCookieValue(USER_COOKIE_NAME, USER_MAIL, value);
            }
        }

        public MiniCartItem MiniCart
        {
            get
            {
                string miniCartString = GetValueFromCookie(MINI_CART_COOKIE_KEY, MINI_CART_COOKIE_NAME);

                if (!string.IsNullOrEmpty(miniCartString))
                {
                    _miniCart = JsonConvert.DeserializeObject<MiniCartItem>(miniCartString);
                }

                return _miniCart;
            }
            set
            {
                string miniCartString = JsonConvert.SerializeObject(value);
                SetCookieValue(MINI_CART_COOKIE_NAME, "NewMiniCartVersionNo", "EIS44");
                SetCookieValue(MINI_CART_COOKIE_NAME, MINI_CART_COOKIE_KEY, miniCartString);
            }
        }

        public string GetDefaultCurrency
        {
            get { return GetValueFromConfiguration(SHOP_DEFAULT_CURRENCY); }
        }

        public string GetCountryName(string countryGuid)
        {
            return _userStorage.GetCountryName(countryGuid);
        }

        // Checks if User has sufficient Access rights
        public bool UserAccess(string accessClass)
        {
            var user = GetUser();

            string userRole;

            if (user == null)
            {
                userRole = "Anonymous";
            }
            else if (string.IsNullOrWhiteSpace(user.RoleId))
            {
                userRole = "B2C";
            }
            else
            {
                userRole = user.RoleId;
            }

            // Determine if User has rights to the requested accessClass
            var data = _userStorage.GetUserAccessByRole(accessClass, userRole);
            return data.Any();
        }

        private string GetValueFromConfiguration(string key)
        {
            return _configurationObject.Read(key);
        }

        private string GetValueFromCookie(string key, string cookieName)
        {
            if (_httpContext.Request == null || _httpContext.Request.Cookies == null)
            {
                return string.Empty;
            }

            var userCookie = _httpContext.Request.Cookies[cookieName];
            if (userCookie == null)
                return string.Empty;

            var value = userCookie.Values[key];

            if (string.IsNullOrWhiteSpace(value))
                return ReturnHtmldDecodedValue(value);

            if (value.Length % 2 != 0)
                return ReturnHtmldDecodedValue(value);

            try
            {
                var decodedBytes = MachineKey.Decode(value, MachineKeyProtection.All);
                if (decodedBytes != null)
                    value = Encoding.UTF8.GetString(decodedBytes);
            }
            catch (Exception ex)
            {
                FileLogger.Log(ex.Message + ": " + ex.StackTrace + " value = " + value);
            }

            return ReturnHtmldDecodedValue(value);
        }

        private string ReturnHtmldDecodedValue(string value)
        {
            return string.IsNullOrEmpty(value) ? string.Empty : _httpContext.Server.HtmlDecode(value);
        }

        private string GetValueFromSession(string key)
        {
            if (_httpContext.Session == null)
            {
                return string.Empty;
            }
            var value = _httpContext.Session[key] as string;

            return string.IsNullOrEmpty(value) ? string.Empty : value;
        }

        private void SetCookieValue(string cookieName, string cookieKey, string cookieValue)
        {
            if (cookieValue == null)
            {
                cookieValue = "";
            }
            byte[] unprotectedBytes = Encoding.UTF8.GetBytes(cookieValue);
            string encodedCookieValue = MachineKey.Encode(unprotectedBytes, MachineKeyProtection.All);
            _httpContext.Response.Cookies[cookieName][cookieKey] = encodedCookieValue; //cookieValue;
            _httpContext.Response.Cookies[cookieName].Path = _httpContext.Server.UrlPathEncode(AppUtil.GetVirtualRoot()) + "/";
        }

        private void SetSessionValue(string key, string value)
        {
            if (_httpContext.Session != null)
            {
                _httpContext.Session[key] = value;
            }
        }
    }

    public class MiniCartItem
    {
        public double Total { get; set; }
        public double TotalInclTax { get; set; }
        public double NumberOfItems { get; set; }
    }

    class UserInfoNode
    {
        public UserTable UserTableItem { get; set; }
        public UserClass UserTypeClass { get; set; }
    }

}