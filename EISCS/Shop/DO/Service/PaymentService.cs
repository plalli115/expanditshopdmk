﻿using System.Collections.Generic;
using System.Linq;
using CmsPublic.DataRepository;
using EISCS.Shop.DO.Dto;
using EISCS.Shop.DO.Dto.Cart;
using EISCS.Shop.DO.Interface;
using ExpandIT;

namespace EISCS.Shop.DO.Service {
	public class PaymentService : BaseDataService, IPaymentService
	{
		public PaymentService(IExpanditDbFactory dbFactory) : base(dbFactory)
		{
		}

		public List<string> GetProvidersByUser(string userType)
		{
			var sql =
				string.Format(
					"SELECT DISTINCT prov.ShippingProviderGuid " +
					"FROM PaymentType_ShippingHandlingProvider AS prov " +
					"INNER JOIN PaymentType AS pay " +
					"ON pay.PaymentType = prov.PaymentType " +
					"WHERE (pay.UserType = @userType)");

			var param = new {userType};

			return GetResults<string>(sql, param).ToList();
		}

		public List<PaymentTypeTable> GetAllPaymentTypes()
		{
			var sql = string.Format("SELECT DISTINCT PaymentType.PaymentType, PaymentType.PaymentName " +
			                        "FROM PaymentType INNER JOIN " +
			                        "PaymentType_ShippingHandlingProvider ON PaymentType.PaymentType = PaymentType_ShippingHandlingProvider.PaymentType");

			var results = GetResults<PaymentTypeTable>(sql).ToList();

			foreach (var item in results)
			{
				var name = item.PaymentName;
				item.PaymentName = Resource.GetLabel(name);
			}

			return results;
		}

		public List<PaymentTypeTable> GetPaymentTypeByProvider(string providerGuid, string userType)
		{
			var sql = string.Format("SELECT DISTINCT PaymentType.PaymentType, PaymentType.PaymentName " +
			                        "FROM PaymentType INNER JOIN " +
			                        "PaymentType_ShippingHandlingProvider ON PaymentType.PaymentType = PaymentType_ShippingHandlingProvider.PaymentType " +
			                        "WHERE (PaymentType_ShippingHandlingProvider.ShippingProviderGuid = @providerGuid) AND (PaymentType.UserType = @userType)");
			var param = new {providerGuid, userType};

			var results = GetResults<PaymentTypeTable>(sql, param).ToList();

			foreach (var description in results)
			{
				var name = description.PaymentName;
				description.PaymentName = Resource.GetLabel(name);
			}

			return results;
		}

		public string GetPaymentMethodName(string paymentMethod)
		{
			if (paymentMethod == null)
				paymentMethod = "";

			var sql = string.Format("SELECT DISTINCT PaymentName " +
									"FROM PaymentType " +
									"WHERE (PaymentType = @paymentMethod)");

			var param = new {paymentMethod};

		    string lblName = GetResults<string>(sql, param).SingleOrDefault();
		    if (!string.IsNullOrEmpty(lblName))
		    {
                return Resource.GetLabel(lblName);    
		    }
		    return string.Empty;
		}
	}
}
