namespace EISCS.Shop.DO.Service
{
    public class Result
    {
        public Result()
        {
            Success = true;
            Error = string.Empty;
        }

        public bool Success { get; set; }
        public string Error { get; set; }
    }
}