﻿using System.Collections.Generic;
using EISCS.ExpandITFramework.Infrastructure;
using EISCS.Shop.DO.Dto.Cart;
using EISCS.Shop.DO.Dto.Country;
using EISCS.Shop.DO.Interface;

namespace EISCS.Shop.DO.Service
{
    public class OrderService : IOrderService
    {
        private readonly IShopSalesHeaderRepository _headerRepository;
        private readonly IShopSalesLineRepository _lineRepository;
        private readonly ICountryRepository _countryRepository;
        private readonly ICartService _cartService;
        private readonly IExpanditUserService _userService;

        public OrderService(IShopSalesHeaderRepository headerRepository, IShopSalesLineRepository lineRepository, ICountryRepository countryRepository, ICartService cartService, IExpanditUserService expanditUserService)
        {
            _headerRepository = headerRepository;
            _lineRepository = lineRepository;
            _countryRepository = countryRepository;
            _cartService = cartService;
            _userService = expanditUserService;
        }

        public List<ShopSalesHeader> GetOrdersByUser(string userGuid)
        {
            return _headerRepository.GetShopSalesHeadersByUser(userGuid);
        }

        public ShopSalesHeader GetOrder(string headerGuid)
        {
            ShopSalesHeader sh = _headerRepository.GetById(headerGuid);
            if (sh == null)
            {
                return null;
            }
            if (!string.IsNullOrEmpty(sh.ShipToCountryGuid))
            {
                CountryItem shipToCountry = _countryRepository.GetById(sh.ShipToCountryGuid);
                sh.ShipToCountryName = shipToCountry != null ? shipToCountry.CountryName : "";
            }
            if (!string.IsNullOrEmpty(sh.CountryGuid))
            {
                if (!string.IsNullOrEmpty(sh.ShipToCountryName) && sh.CountryGuid == sh.ShipToCountryGuid)
                {
                    sh.CountryName = sh.ShipToCountryName;
                }
                else
                {
                    CountryItem billToCountry = _countryRepository.GetById(sh.CountryGuid);
                    sh.CountryName = billToCountry != null ? billToCountry.CountryName : "";
                }
            }            
            sh.Lines = _lineRepository.GetShopSalesLinesByHeaderGuid(headerGuid);            
            return sh;
        }

        public List<ShopSalesHeader> SearchByCustRef(string userGuid, string custRef)
        {
            return _headerRepository.GetShopSalesHeadersByCustRef(userGuid, custRef);
        }

        public void Reorder(string headerGuid, PageMessages messages)
        {
            ShopSalesHeader sh = GetOrder(headerGuid);
            if (sh != null && sh.Lines != null && sh.Lines.Count > 0)
            {
                foreach (ShopSalesLine sl in sh.Lines)
                {
                    _cartService.Add(_userService.UserGuid, sl.ProductGuid, sl.ProductName, sl.Quantity, sl.LineComment, sl.VariantCode, false, messages);
                }
            }
        }
    }
}