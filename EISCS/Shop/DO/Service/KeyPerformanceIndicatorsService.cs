﻿using System;
using System.Collections.Generic;
using CmsPublic.DataRepository;
using EISCS.Shop.DO.Dto;
using EISCS.Shop.DO.Dto.Cart;
using EISCS.Shop.DO.Interface;

namespace EISCS.Shop.DO.Service
{
    public interface IKeyPerformanceIndicatorsService
    {
        void GenerateReport();
        List<KPI> GetLastMonthReport();
        bool NewLogin(string loggedInUserGuid);
        bool NewSale(ShopSalesHeader shopSalesHeader);
        int DaysWithoutReport();
        bool NewUser();
        List<KPI> GetReport(DateTime startDate, DateTime endDate, DateTime periodStartDate, DateTime periodEndDate);
    }

    public class KeyPerformanceIndicatorsService : BaseDataService, IKeyPerformanceIndicatorsService
    {
        private readonly IKeyPerformanceIndicatorsRepository _kpiRepository;
        private readonly IShopSalesHeaderRepository _salesRepository;
        private readonly IExpanditUserService _expanditUserService;

        public KeyPerformanceIndicatorsService(IExpanditDbFactory dbFactory, IKeyPerformanceIndicatorsRepository kpiRepository, IShopSalesHeaderRepository salesRepository, IExpanditUserService expanditUserService)
            : base(dbFactory)
        {
            _kpiRepository = kpiRepository;
            _salesRepository = salesRepository;
            _expanditUserService = expanditUserService;
        }

        public void GenerateReport()
        {
            var allSales = _salesRepository.All();
            foreach (var sale in allSales)
            {
                NewSale(sale);
            }
        }

        public List<KPI> GetLastMonthReport()
        {
            return _kpiRepository.GetLastMonthReport();
        }

        public bool NewLogin(string userGuid)
        {
            return _expanditUserService.NewLogin(userGuid) && _kpiRepository.NewLogin();
        }

        public bool NewSale(ShopSalesHeader shopSalesHeader)
        {
            return _kpiRepository.NewSale(shopSalesHeader);
        }

        public int DaysWithoutReport()
        {
            var today = DateTime.Today;
            var lastReport = _kpiRepository.GetLastDayReport();
            return lastReport != null ? (today - lastReport.EntryDate).Days : 0;
        }

        public bool NewUser()
        {
            return _kpiRepository.NewUser();
        }

        public List<KPI> GetReport(DateTime startDate, DateTime endDate, DateTime periodStartDate, DateTime periodEndDate)
        {
            var list = new List<KPI>();
            var firstPeriod = GetReportBetweenDates(startDate, endDate);
            var secondPeriod = GetReportBetweenDates(periodStartDate, periodEndDate);

            list.Add(_kpiRepository.GetTotals(firstPeriod, startDate, endDate));
            list.Add(_kpiRepository.GetTotals(secondPeriod, periodStartDate, periodEndDate));

            return list;
        }

        public List<KPI> GetReportBetweenDates(DateTime initDate, DateTime endDate)
        {
            return _kpiRepository.GetReportBetweenDates(initDate, endDate);
        }
    }
}