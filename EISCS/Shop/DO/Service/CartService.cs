﻿using System;
using System.Collections.Generic;
using System.Linq;
using CmsPublic.DataRepository;
using EISCS.ExpandITFramework.Infrastructure;
using EISCS.ExpandITFramework.Util;
using EISCS.Shop.BO.BusinessLogic;
using EISCS.Shop.DO.Dto;
using EISCS.Shop.DO.Dto.Cart;
using EISCS.Shop.DO.Dto.Product;
using EISCS.Shop.DO.Interface;
using EISCS.Wrappers.Configuration;
using EISCS.Shop.DO.Dto.Promotions;

namespace EISCS.Shop.DO.Service
{
    public interface ICartDataService
    {
        List<string> GetCartLinesToRemove(string headerGuid);
        string GenerateCustomerReference();
        string ShippingHandlingName(CartHeader cartHeader);
        IEnumerable<CartHeader> GetStaleUserCarts(int maxDays);
        string GetCurrencyIso4217(string currencyGuid);
    }

    public class CartDataService : BaseDataService, ICartDataService
    {
        public CartDataService(IExpanditDbFactory dbFactory)
            : base(dbFactory)
        {

        }

        public List<string> GetCartLinesToRemove(string headerGuid)
        {
            string query =
                @"SELECT LineGuid FROM CartLine WHERE HeaderGuid = @headerGuid
                EXCEPT
                SELECT cl.LineGuid FROM CartLine cl INNER JOIN ProductTable pt ON cl.ProductGuid = pt.ProductGuid WHERE cl.HeaderGuid = @headerGuid";
            return GetResults<string>(query, new { headerGuid }).ToList();
        }

        public string GenerateCustomerReference()
        {
            var generator = new RandomSequenceGenerator();

            for (var i = 0; i < 10; i++)
            {
                var reference = generator.GenerateRandomSequence(10);
                var sqlCartHeader = String.Format("SELECT TOP 1 CustomerReference FROM CartHeader WHERE CustomerReference = @customerReference");
                var sqlShopSalesHeader = String.Format("SELECT CustomerReference FROM ShopSalesHeader WHERE CustomerReference = @customerReference");

                var param = new { customerReference = reference };

                var referenceIsValid = !(GetResults<string>(sqlCartHeader, param).Any() && GetResults<string>(sqlShopSalesHeader, param).Any());
                if (referenceIsValid)
                {
                    return reference;
                }
            }

            throw new Exception("Error generating a new Customer Reference");
        }

        public string ShippingHandlingName(CartHeader cartHeader)
        {
            string query = "SELECT ProviderName FROM ShippingHandlingProvider WHERE ShippingHandlingProviderGuid = @providerGuid";
            var param = new { providerGuid = cartHeader.ShippingHandlingProviderGuid };
            return GetSingle<string>(query, param);
        }

        public IEnumerable<CartHeader> GetStaleUserCarts(int maxDays)
        {
            return GetResults<CartHeader>(
                @"SELECT ch.* FROM CartHeader ch INNER JOIN UserTable ut ON ch.UserGuid = ut.UserGuid WHERE DATEDIFF(day, ModifiedDate, @today) > @maxDays",
                new { @today = DateTime.Now.Date.ToShortDateString(), maxDays });
        }

        public string GetCurrencyIso4217(string currencyGuid)
        {
            string query = @"SELECT Iso4217 FROM CurrencyIso4217 WHERE CurrencyGuid = @currencyGuid";
            return GetSingle<string>(query, new { currencyGuid });
        }
    }

    public class CartService : ICartService
    {
        private readonly IBuslogic _busLogic;
        private readonly ICartHeaderRepository _cartHeaderRepository;
        private readonly ICartLineRepository _cartLineRepository;
        private readonly IExpanditUserService _expanditUserService;
        private readonly IProductService _productService;
        private readonly IProductVariantService _productVariantService;
        private readonly IShippingHandlingService _shippingHandlingProvider;
        private readonly ICurrencyConverter _currencyConverter;
        private readonly IConfigurationObject _configuration;
        private readonly ICartDataService _cartDataService;
        private readonly PromotionsServices _promotionsServices;

        public CartService(ICartDataService cartDataService, IBuslogic buslogic, IProductService productService, ICartHeaderRepository cartHeaderRepository,
            ICartLineRepository cartLineRepository,
            IExpanditUserService expanditUserService, IProductVariantService productVariantService, IShippingHandlingService shippingHandlingProvider, PromotionsServices promotionsServices, ICurrencyConverter currencyConverter, IConfigurationObject configuration)
        {
            _cartHeaderRepository = cartHeaderRepository;
            _cartLineRepository = cartLineRepository;
            _busLogic = buslogic;
            _productService = productService;
            _expanditUserService = expanditUserService;
            _productVariantService = productVariantService;
            _shippingHandlingProvider = shippingHandlingProvider;
            _currencyConverter = currencyConverter;
            _configuration = configuration;
            _cartDataService = cartDataService;
            _promotionsServices = promotionsServices;
        }

        public List<ProductInfoNode> GetProducts(CartHeader cartHeader)
        {
            List<string> guids = cartHeader.Lines.Select(line => line.ProductGuid).ToList();
            return _productService.GetProducts(cartHeader, guids, oneProductPerVariant: true).ProductInfoNodes;
        }

        public Result MergeUserCarts(string loggedInUserGuid, string offlineUserGuid)
        {
            var result = new Result();

            CartHeader offlineCart = GetCartById(offlineUserGuid);

            if (offlineCart.Lines == null)
            {
                return result;
            }
            try
            {
                //using (var scope = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                //{
                foreach (CartLine offlineCartline in offlineCart.Lines)
                {
                    Add(loggedInUserGuid, offlineCartline.ProductGuid, offlineCartline.ProductName, offlineCartline.Quantity, offlineCartline.LineComment,
                        offlineCartline.VariantCode, false);
                    _cartLineRepository.Remove(offlineCartline.LineGuid);
                }
                _cartHeaderRepository.Remove(offlineCart.HeaderGuid);
                //scope.Complete();
                //}
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Error = ex.Message;
            }

            return result;
        }

        public CartHeader GetCartById(string userGuid)
        {
            // If no active cart exists then create it.
            CartHeader cartHeader = _cartHeaderRepository.GetCartHeaderByUser(userGuid) ?? CreateCart(userGuid);
            string headerGuid = cartHeader.HeaderGuid;
            // Must check for absent products and remove them from the cart
            var lineGuids = _cartDataService.GetCartLinesToRemove(headerGuid);
            if (!lineGuids.IsNullOrEmpty())
            {
                _cartLineRepository.Delete(lineGuids);
            }
            cartHeader.Lines = _cartLineRepository.GetCartLinesById(headerGuid);
            Calculate(cartHeader);
            return cartHeader;
        }

        public void ClearCartLines(CartHeader cartHeader)
        {
            foreach (var line in cartHeader.Lines)
            {
                _cartLineRepository.Remove(line.LineGuid);
            }
        }

        public CartHeader Add(string userGuid, string productGuid, string productName, double quantity, string comment, string variantCode, bool updateQuantity, bool isGift = false)
        {
            PageMessages messages = new PageMessages();
            return Add(userGuid, productGuid, productName, quantity, comment, variantCode, updateQuantity, messages, isGift);
        }

        public CartHeader Add(string userGuid, string productGuid, string productName, double quantity, string comment, string variantCode, bool updateQuantity, PageMessages messages, bool isGift = false)
        {
            CartHeader cartHeader = GetCartById(userGuid);

            if (cartHeader == null)
            {
                return null;
            }

            // Check if product exists - if not return
            var nodeContainer = _productService.GetProduct(productGuid, variantCode);

            if (nodeContainer == null)
            {
                messages.Warnings.Add(productGuid);
                return cartHeader;
            }

            ProductVariantContainer productVariantContainer = _productVariantService.GetVariants(new[] { productGuid }, _expanditUserService.LanguageGuid);
            ProductVariant productVariant = productVariantContainer.GetVariantForProduct(productGuid, variantCode);

            string variantName = productVariant != null ? productVariant.GetDisplayName() : null;

            if (cartHeader.Lines != null)
            {
                if (variantCode == null) variantCode = "";

                CartLine line = cartHeader.Lines.FirstOrDefault(x => x.ProductGuid == productGuid && x.VariantCode == variantCode && x.PromotionalGift == isGift);

                if (line != null)
                {
                    if (quantity <= 0)
                    {
                        _cartLineRepository.Delete(new List<string> { line.LineGuid });
                        Calculate(cartHeader);
                        SaveCartHeader(cartHeader);
                        return cartHeader;
                    }

                    if (updateQuantity)
                    {
                        line.Quantity = quantity;
                    }
                    else
                    {
                        line.Quantity += quantity;
                    }

                    line.VariantCode = variantCode;
                    line.VariantName = variantName;
                    line.IsCalculated = false;
                    line.VersionGuid = Guid.NewGuid().ToString();
                    if (isGift)
                    {
                        line.IsCalculated = true;
                        line.ListPriceInclTax = line.ListPrice;
                        line.LineDiscountAmount = line.ListPrice;
                        line.LineTotal = 0;
                        line.TotalInclTax = 0;
                        line.PromotionalGift = true;
                        cartHeader.IsCalculated = true;
                    }
                    else
                    {
                        cartHeader.IsCalculated = false;
                        if (_configuration.Read("USE_USPROMOS") == "TRUE")
                        {
                            cartHeader = PromoGiftDiscounts(cartHeader);
                        }
                        Calculate(cartHeader);
                    }
                    _cartLineRepository.Update(line);
                    SaveCartHeader(cartHeader);
                    return cartHeader;
                }
            }

            if (cartHeader.Lines == null)
            {
                cartHeader.Lines = new List<CartLine>();
            }

            int lineNumber = 1;
            if (cartHeader.Lines.Any())
            {
                lineNumber = cartHeader.Lines.Max(x => x.LineNumber) + 1;
            }

            var newline = new CartLine
            {
                HeaderGuid = cartHeader.HeaderGuid,
                ProductGuid = productGuid,
                ProductName = productName,
                Quantity = quantity,
                LineComment = comment,
                VariantCode = variantCode,
                VariantName = variantName,
                LineGuid = Guid.NewGuid().ToString(),
                LineNumber = lineNumber,
                IsCalculated = false,
                VersionGuid = Guid.NewGuid().ToString()
            };
            if (isGift)
            {
                newline.IsCalculated = true;
                newline.ListPriceInclTax = newline.ListPrice;
                newline.LineDiscountAmount = newline.ListPrice;
                newline.LineTotal = 0;
                newline.TotalInclTax = 0;
                newline.PromotionalGift = true;
                cartHeader.IsCalculated = true;
            }

            cartHeader.Lines.Add(newline);

            if (cartHeader.CustomerReference.IsNullOrEmpty())
            {
                cartHeader.CustomerReference = _cartDataService.GenerateCustomerReference();
            }

            Calculate(cartHeader);
            _cartLineRepository.Add(newline);
            if (!isGift)
            {
                if (_configuration.Read("USE_USPROMOS") == "TRUE")
                {
                    cartHeader = PromoGiftDiscounts(cartHeader);
                }
            }
            SaveCartHeader(cartHeader);
            return cartHeader;
        }

        public CartHeader Update(string userGuid, List<CartUpdateInfo> cartInfo)
        {
            // Get user cart.
            // Look in collection and compare. 
            // Remove items from cart that does not exist in the collection.
            // Adjust quantities.
            // Calculate the cart.
            // Save changes.
            CartHeader cartHeader = GetCartById(userGuid);

            // In cart but not in collection
            List<string> toRemove = (from line in cartHeader.Lines
                                     where !cartInfo.Exists(x => x.LineGuid == line.LineGuid)
                                     select line.LineGuid).ToList();

            toRemove.AddRange(from info in cartInfo where info.Quantity <= 0 select info.LineGuid);

            // Remove all PromotionalGifts (they'll be readded later)
            toRemove.AddRange((from line in cartHeader.Lines where line.PromotionalGift select line.LineGuid).ToList());

            foreach (string s in toRemove)
            {
                cartInfo.RemoveAll(x => x.LineGuid == s);
            }

            _cartLineRepository.Delete(toRemove);

            cartHeader = GetCartById(userGuid);

            foreach (CartUpdateInfo info in cartInfo)
            {
                cartHeader = Add(userGuid, info.ProductGuid, info.ProductName, info.Quantity, info.Comment,
                    info.VariantCode, true);
            }

            if (_configuration.Read("USE_USPROMOS") == "TRUE")
            {
                cartHeader = PromoGiftDiscounts(cartHeader);
            }

            cartHeader = GetShippingHandlingPrice(_expanditUserService.UserGuid);

            return cartHeader;
        }

        public PageMessages Calculate(CartHeader cartHeader)
        {
            string currencyGuid = _expanditUserService.CurrencyGuid;
            string defaultCurrencyGuid = _expanditUserService.GetDefaultCurrency;
            return Calculate(cartHeader, currencyGuid, defaultCurrencyGuid);
        }

        public PageMessages Calculate(CartHeader cartHeader, string currency, string defaultCurrency)
        {
            cartHeader.IsCalculated = false;
            var messages = _busLogic.CalculateOrder(cartHeader, currency, defaultCurrency);
            cartHeader.SubTotal = cartHeader.Total;
            cartHeader.SubTotalInclTax = cartHeader.TotalInclTax;
            SaveCartHeader(cartHeader);
            return messages;
        }

        public MiniCartItem UpdateMiniCart(CartHeader header)
        {
            if (header == null)
            {
                return new MiniCartItem();
            }

            double counter = header.Lines.Sum(line => line.Quantity);

            var miniCart = new MiniCartItem
                {
                    Total = header.Total,
                    TotalInclTax = header.TotalInclTax,
                    NumberOfItems = counter
                };

            return miniCart;
        }

        public string ShippingHandlingName(CartHeader cartHeader)
        {
            return _cartDataService.ShippingHandlingName(cartHeader);
        }

        public CartHeader CreateCart(string userGuid)
        {
            var header = new CartHeader
                {
                    HeaderGuid = Guid.NewGuid().ToString(),
                    CreatedDate = DateTime.Now,
                    ModifiedDate = DateTime.Now,
                    UserGuid = userGuid,
                    MultiCartStatus = "ACTIVE",
                    MultiCartDescription = "Created"
                };
            return header;
        }

        public CartHeader Delete(string userGuid, string productGuid, string variantCode, bool isGift = false)
        {
            var cartHeader = GetCartById(userGuid);

            if (cartHeader == null)
            {
                return null;
            }

            var line = cartHeader.Lines.FirstOrDefault(x => x.ProductGuid == productGuid && (x.VariantCode == variantCode || variantCode == null && x.VariantCode == "") && x.PromotionalGift == isGift);
            if (line != null)
            {
                _cartLineRepository.Delete(new List<string> { line.LineGuid });
                cartHeader.Lines.Remove(line);
                if (_configuration.Read("USE_USPROMOS") == "TRUE")
                {
                    //Don't check for Promo Gifts if the line we're removing is itself a Promo Gift
                    if (!line.PromotionalGift)
                    {
                        cartHeader = PromoGiftDiscounts(cartHeader);
                    }
                }
                Calculate(cartHeader);
                SaveCartHeader(cartHeader);

            }

            return cartHeader;
        }

        public bool SaveCartHeader(CartHeader cartHeader)
        {
            if (cartHeader.MultiCartDescription == "Created")
            {
                cartHeader.MultiCartDescription = "";
                return _cartHeaderRepository.Add(cartHeader) != null;
            }
            return _cartHeaderRepository.Update(cartHeader);
        }

        // To use when there is a change in the Shipping Provider
        public CartHeader GetShippingHandlingPrice(string userGuid, string shippingProviderGuid)
        {
            var cartHeader = GetCartById(userGuid);
            cartHeader.ShippingHandlingProviderGuid = shippingProviderGuid;

            var shippingProvider = _shippingHandlingProvider.GetShippingHandlingProvider(shippingProviderGuid);

            if (shippingProvider.ShippingHandlingProviderGuid == "NONE")
            {
                cartHeader.ShippingAmount = 0.0;
                cartHeader.HandlingAmount = 0.0;
            }
            else
            {
                var prices = ConvertPrices(shippingProvider.ShippingHandlingPrices, _expanditUserService.GetDefaultCurrency, _expanditUserService.CurrencyGuid);
                var i = 0;
                while (i < prices.Count && (cartHeader.SubTotal - cartHeader.InvoiceDiscount) > prices[i].CalculatedValue)
                    i++;

                if (i == prices.Count)
                    i -= 1;

                cartHeader.HandlingAmount = Math.Round(prices[i].HandlingAmount, 2);
                cartHeader.ShippingAmount = Math.Round(prices[i].ShippingAmount, 2);
                if (_configuration.Read("USE_USPROMOS") == "TRUE")
                {
                    _busLogic.PromoShippingDiscounts(cartHeader, _expanditUserService.CurrencyGuid);
                }
                cartHeader.ShippingAmountInclTax = Math.Round(cartHeader.ShippingAmount * (1 + (ExpanditLib2.ConvertToDbl(_configuration.Read("SHIPPING_TAX_PCT"))) / 100), 2);
                cartHeader.HandlingAmountInclTax = Math.Round(cartHeader.HandlingAmount * (1 + (ExpanditLib2.ConvertToDbl(_configuration.Read("TAX_PCT"))) / 100), 2);
            }

            return cartHeader;
        }

        private List<ShippingHandlingPrice> ConvertPrices(List<ShippingHandlingPrice> shippingHandlingPrices, string getDefaultCurrency, string currencyGuid)
        {
            var convertedPrices = shippingHandlingPrices;
            foreach (var shippingHandlingPrice in convertedPrices)
            {
                shippingHandlingPrice.ShippingAmount = (float)_currencyConverter.ConvertCurrency(shippingHandlingPrice.ShippingAmount, getDefaultCurrency, currencyGuid);
                shippingHandlingPrice.HandlingAmount = (float)_currencyConverter.ConvertCurrency(shippingHandlingPrice.HandlingAmount, getDefaultCurrency, currencyGuid);
                shippingHandlingPrice.CalculatedValue = (float)_currencyConverter.ConvertCurrency(shippingHandlingPrice.CalculatedValue, getDefaultCurrency, currencyGuid);
            }
            return convertedPrices;
        }

        // To use when there is no change in the Shipping Provider
        public CartHeader GetShippingHandlingPrice(string userGuid)
        {
            var cartHeader = GetCartById(userGuid);

            return cartHeader.ShippingHandlingProviderGuid.IsNullOrEmpty() ? cartHeader : GetShippingHandlingPrice(userGuid, cartHeader.ShippingHandlingProviderGuid);
        }

        public List<CartHeader> GetStaleUserCarts(int maxAge)
        {
            var carts = _cartDataService.GetStaleUserCarts(maxAge).ToList();
            foreach (var header in carts)
            {
                header.Lines = _cartLineRepository.GetCartLinesById(header.HeaderGuid);
            }
            return carts.FindAll(c => c.Lines.Count > 0);
        }

        public List<CartHeader> ClearStaleUserCarts(int maxAge)
        {
            var carts = GetStaleUserCarts(maxAge);

            foreach (var cartHeader in carts)
            {
                ClearCartLines(cartHeader);
                _cartHeaderRepository.Remove(cartHeader.HeaderGuid);
            }
            return carts;
        }

        public string GetCurrencyIso4217(string currencyGuid)
        {
            return _cartDataService.GetCurrencyIso4217(currencyGuid);
        }

        public CartHeader PromoGiftDiscounts(CartHeader order)
        {

            // First remove any promotional gifts, to prevent duplications
            foreach (CartLine Line in order.Lines)
            {
                if (Line.PromotionalGift)
                {
                    order = Delete(order.UserGuid, Line.ProductGuid, Line.VariantCode, true);
                }
            }
            if (String.IsNullOrEmpty(order.PromotionCode))
            {
                return order;
            }
            Promotion p = _promotionsServices.getPromoByCode(order.PromotionCode);
            List<int> GiftTypes = new List<int> { 9, 10, 11, 12 };
            foreach (PromotionEntry promoEntry in p.PromotionEntries)
            {
                if (!GiftTypes.Contains(promoEntry.PromotionType))
                {
                    continue;
                }
                else if (_promotionsServices.PromotionEntryMet(promoEntry, order))
                {
                    order = Add(_expanditUserService.UserGuid, promoEntry.SubCode, _productService.GetProduct(promoEntry.SubCode, "").ProductInfoNodes[0].Prod.ProductName, 1, "Gift (" + p.PromotionCode + ")", "", false, true);
                }
            }
            return order;
        }

    }

}