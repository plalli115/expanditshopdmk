﻿using System;
using System.Collections.Generic;
using EISCS.Shop.BO.BusinessLogic;
using EISCS.Shop.DO.Dto.Ledger;
using EISCS.Shop.DO.Interface;

namespace EISCS.Shop.DO.Service
{
    public class LedgerService : ILedgerService
    {
        private readonly BuslogicBaseDataService _backendDataService;
        private readonly IExpanditUserService _userService;

        public LedgerService(BuslogicBaseDataService backendDataService, IExpanditUserService userService)
        {
            _backendDataService = backendDataService;
            _userService = userService;
        }

        public LedgerEntryContainer GetLedgerEntriesByUser()
        {
            return _backendDataService.GetCustomerLedgerEntries(_userService.CustomerGuid);
        }
    }
}