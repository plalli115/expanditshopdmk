﻿using System.Collections.Generic;
using System.Linq;
using CmsPublic.DataRepository;
using CmsPublic.ModelLayer.Models;
using EISCS.ExpandITFramework.Infrastructure;
using EISCS.Shop.BO.BusinessLogic;
using EISCS.Shop.DO.Dto;
using EISCS.Shop.DO.Dto.Cart;
using EISCS.Shop.DO.Dto.Product;
using EISCS.Shop.DO.Interface;

namespace EISCS.Shop.DO.Service
{
    public class ProductService : BaseDataService, IProductService
    {
        private readonly IBuslogic _buslogic;
        private readonly IExpanditUserService _expanditUserService;
        private readonly IFavoritesRepository _favoriteRepository;
        private readonly IProductRepository _productRepository;
        private readonly IProductVariantService _productVariantService;

        public ProductService(IExpanditDbFactory dbFactory, IBuslogic buslogic, IProductRepository productRepository, IExpanditUserService expanditUserService, IProductVariantService variantService,
            IFavoritesRepository favoritesRepository)
            : base(dbFactory)
        {
            _buslogic = buslogic;
            _productRepository = productRepository;
            _expanditUserService = expanditUserService;
            _productVariantService = variantService;
            _favoriteRepository = favoritesRepository;
        }

        public ProductInfoNodeContainer GetProduct(string productGuid, string variantCode)
        {
            CartHeader info = ProductServiceHelper.CreateCartHeader(_expanditUserService);
            Product product = _productRepository.SingleProduct(productGuid, _expanditUserService.LanguageGuid, true);

            if (product == null)
            {
                return null;
            }

            ProductVariantContainer productVariantContainer = _productVariantService.GetVariants(new[] { productGuid }, _expanditUserService.LanguageGuid);

            var variants = productVariantContainer.GetVariantsForProduct(productGuid);
            if (variants != null && string.IsNullOrEmpty(variantCode))
            {
                var randomVariant = variants.First();
                variantCode = randomVariant.Key;
            }
            ProductVariant productVariant = productVariantContainer.GetVariantForProduct(productGuid, variantCode);


            info.Lines = new List<CartLine>();
            var line = new CartLine
                {
                    ProductGuid = product.ProductGuid,
                    VariantCode = productVariant != null ? productVariant.VariantCode : string.Empty,
                    Quantity = 1.0
                };
            info.Lines.Add(line);

            PageMessages messages = _buslogic.CalculateOrder(info, _expanditUserService.CurrencyGuid, _expanditUserService.GetDefaultCurrency);

            var node = new ProductInfoNode
                {
                    Prod = product,
                    Line = line,
                    VariantDict = productVariantContainer.GetVariantsForProduct(productGuid),
                    Messages = messages
                };
            return new ProductInfoNodeContainer { ProductInfoNodes = new List<ProductInfoNode> { node }, Header = info };
        }

        public ProductInfoNodeContainer GetRelatedProducts(string productGuid)
        {
            string sql = @"
                SELECT a.* FROM
                (SELECT * FROM ProductRelation WHERE ProductGuid = @productGuid ) AS a
                INNER JOIN ProductTable ON a.RelatedToProductGuid = ProductTable.ProductGuid  ORDER BY SortIndex";

            var relatedItems = GetResults<ProductRelation>(sql, new { productGuid }).ToList();

            List<string> guids = relatedItems.Select(r => r.RelatedToProductGuid).ToList();
            List<Product> products = _productRepository.GetProducts(guids, _expanditUserService.LanguageGuid);

	        var sortedProducts = from g in guids join p in products on g equals p.ProductGuid select p;

			products = sortedProducts.ToList();

            CartHeader info = ProductServiceHelper.CreateCartHeader(_expanditUserService);
            info.Lines = new List<CartLine>();
            foreach (Product product in products)
            {
                var line = new CartLine { ProductGuid = product.ProductGuid, Quantity = 1 };
                info.Lines.Add(line);
            }

            PageMessages messages = _buslogic.CalculateOrder(info, _expanditUserService.CurrencyGuid, _expanditUserService.GetDefaultCurrency);

            ProductVariantContainer productVariantContainer = _productVariantService.GetVariants(guids, _expanditUserService.LanguageGuid);

            var container = new ProductInfoNodeContainer();
            container.Combine(info, products, productVariantContainer, messages, false, null, relatedItems);

            return container;
        }

        public ProductInfoNodeContainer GetProducts(CartHeader info, ICollection<string> guids, string sort = "", string direction = "", int startIndex = -1, int endIndex = -1,
            bool cartCombine = false)
        {
            //, sort, direction, startIndex, endIndex
            int startIndexDB = startIndex;
            int endIndexDB = endIndex;
            //prices can't be sorted in the db, because the price in the product table is a listprice and not a customer specific price
            if (sort == ProductServiceConstants.PRICE_SORT)
            {
                startIndexDB = -1;
                endIndexDB = -1;
            }
            List<Product> gp = _productRepository.GetProductsRange(guids, _expanditUserService.LanguageGuid, "", sort, direction, startIndexDB, endIndexDB);

            if (info.Lines == null)
            {
                info.Lines = new List<CartLine>();
                foreach (Product product in gp)
                {
                    var line = new CartLine { ProductGuid = product.ProductGuid, Quantity = 1};
                    info.Lines.Add(line);
                }
            }

            ProductInfoNodeContainer result = CalculateAndBuildResult(startIndex, endIndex, sort, direction, info, gp, guids.Count, cartCombine);

            return result;
        }

        public ProductInfoNodeContainer GetProducts(int groupGuid, int startIndex, int endIndex, string sort = null, string direction = null)
        {
            CartHeader info = ProductServiceHelper.CreateCartHeader(_expanditUserService);

            int startIndexDB = startIndex;
            int endIndexDB = endIndex;
            //prices can't be sorted in the db, because the price in the product table is a listprice and not a customer specific price
            if (sort == ProductServiceConstants.PRICE_SORT)
            {
                startIndexDB = -1;
                endIndexDB = -1;
            }

            List<Product> gp = _productRepository.GetGroupProductsRange(startIndexDB, endIndexDB, true, _expanditUserService.LanguageGuid, null, groupGuid, sort, direction);
            int totalCount = GetGroupProductsCount(groupGuid);

            ProductInfoNodeContainer result = CalculateAndBuildResult(startIndex, endIndex, sort, direction, info, gp, totalCount, false);


            return result;
        }

        public ProductInfoNodeContainer GetMostPopularProducts(int pageSize)
        {
            CartHeader info = ProductServiceHelper.CreateCartHeader(_expanditUserService);
            info.Lines = new List<CartLine>();


            string sql =
                @"
                ;WITH temp_2(ProductGuid, TotalQuantity) AS 
                ( 
                    SELECT  ProductGuid, SUM(Quantity) AS TotalQuantity 
                    FROM    ShopSalesLine 
                    GROUP BY ProductGuid 
                ) 
                SELECT  * 
                FROM    (SELECT ROW_NUMBER() OVER(ORDER BY TotalQuantity DESC) AS Row,   ProductGuid, TotalQuantity FROM temp_2) AS Data1 
                WHERE   Row BETWEEN 0 AND " + pageSize;

            List<ProductGuids> productGuids = GetResults<ProductGuids>(sql).ToList();

            List<string> guids = productGuids.Select(p => p.ProductGuid).ToList();

            List<Product> products = _productRepository.GetProducts(guids, _expanditUserService.LanguageGuid);

            ProductVariantContainer productVariantContainer = _productVariantService.GetVariants(products.Select(x => x.ProductGuid), _expanditUserService.LanguageGuid);

            IEnumerable<Product> orderedOptions = from option in products
                                                  join type in productGuids
                                                      on option.ProductGuid equals type.ProductGuid
                                                  orderby type.TotalQuantity
                                                  select option;

            List<Product> gp = orderedOptions.Reverse().ToList();

            foreach (Product product in gp)
            {
                var line = new CartLine { ProductGuid = product.ProductGuid, Quantity = 1 };
                info.Lines.Add(line);
            }

            PageMessages messages = _buslogic.CalculateOrder(info, _expanditUserService.CurrencyGuid, _expanditUserService.GetDefaultCurrency);

            var container = new ProductInfoNodeContainer();
            container.Combine(info, gp, productVariantContainer, messages);

            return container;
        }

        public int GetGroupProductsCount(int groupGuid)
        {
            return _productRepository.GetGroupProductsCount(groupGuid);
        }

        public ProductInfoNodeContainer GetFavoriteProducts(int startIndex, int endIndex, string sort = null, string direction = null)
        {
            CartHeader info = ProductServiceHelper.CreateCartHeader(_expanditUserService);

            int startIndexDB = startIndex;
            int endIndexDB = endIndex;
            //prices can't be sorted in the db, because the price in the product table is a listprice and not a customer specific price
            if (sort == ProductServiceConstants.PRICE_SORT)
            {
                startIndexDB = -1;
                endIndexDB = -1;
            }

            IEnumerable<FavoriteItem> favorites = _favoriteRepository.GetFavoritesForUser(_expanditUserService.UserGuid);
            List<string> guids = favorites.Select(x => x.ProductGuid).ToList();
            List<Product> gp = _productRepository.GetProductsRange(guids, _expanditUserService.LanguageGuid, "", sort, direction, startIndexDB, endIndexDB);
            ProductInfoNodeContainer result = CalculateAndBuildResult(startIndex, endIndex, sort, direction, info, gp, guids.Count, false);

            return result;
        }

        private ProductInfoNodeContainer CalculateAndBuildResult(int startIndex, int endIndex, string sort, string direction, CartHeader info, List<Product> gp, int totalCountInDb,
            bool oneProductPerVariant)
        {
            List<string> productGuids = gp.Select(x => x.ProductGuid).ToList();

            if (info.Lines == null)
            {
                info.Lines = new List<CartLine>();
                foreach (Product product in gp)
                {
                    var line = new CartLine { ProductGuid = product.ProductGuid, Quantity = 1 };
                    info.Lines.Add(line);
                }
            }

            PageMessages messages = _buslogic.CalculateOrder(info, _expanditUserService.CurrencyGuid, _expanditUserService.GetDefaultCurrency);
            IEnumerable<FavoriteItem> favorites = _favoriteRepository.GetFavoritesForUser(_expanditUserService.UserGuid, productGuids);
            ProductVariantContainer productVariantContainer = _productVariantService.GetVariants(productGuids, _expanditUserService.LanguageGuid);

            var container = new ProductInfoNodeContainer();
            container.Combine(info, gp, productVariantContainer, messages, oneProductPerVariant, favorites.ToList());
            container.PriceSortingAndPaging(sort, direction, startIndex, endIndex);
            container.TotalCountInDb = totalCountInDb;
            container.Header = info;

            return container;
        }
    }
}