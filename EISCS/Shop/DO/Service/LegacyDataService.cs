﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using CmsPublic.DataRepository;
using EISCS.ExpandITFramework.Util;

namespace EISCS.Shop.DO.Service
{

    public interface ILegacyDataService
    {
        DataTable SQL2DataTable(string sql);
        bool ExcecuteNonQueryDb(string sql);
        T GetSingleValueDb<T>(string sql);
        bool IsLocalMode { get; }
    }

    public class LegacyDataService : BaseDataService, ILegacyDataService
    {
        private readonly IHttpRuntimeWrapper _httpRuntimeWrapper;

        public LegacyDataService(IExpanditDbFactory dbFactory, IHttpRuntimeWrapper httpRuntimeWrapper)
            : base(dbFactory)
        {
            _httpRuntimeWrapper = httpRuntimeWrapper;
        }

        public DataTable SQL2DataTable(string sql)
        {
            IDbConnection conn = GetConnection(DbFactory);

            try
            {
                IDbCommand cmd = conn.CreateCommand();
                IDbDataAdapter da = GetDataAdapter(conn);

                //Don't time out
                cmd.CommandTimeout = 0;
                cmd.CommandText = sql;

                DataSet dt = new DataSet();

                da.SelectCommand = cmd;
                da.SelectCommand.Connection = conn;
                da.Fill(dt);

                return dt.Tables[0];
            }
            finally
            {
                DbFactory.Finalize(conn);
            }
        }

        public bool ExcecuteNonQueryDb(string sql)
        {
            return Execute(sql);
        }

        public T GetSingleValueDb<T>(string sql)
        {
            return GetSingle<T>(sql);
        }

        public bool IsLocalMode { get { return !IsRemoteMode(); } }

        private bool IsRemoteMode()
        {
            try
            {
                string str = _httpRuntimeWrapper.AppDomainAppPath + "expandITRemote.xml";
                return System.IO.File.Exists(str);
            }
            catch
            {
                return false;
            }
        }
    }
}
