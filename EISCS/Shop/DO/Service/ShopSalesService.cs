﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using CmsPublic.DataRepository;
using EISCS.ExpandIT;
using EISCS.Shop.BO.BusinessLogic;
using EISCS.Shop.DO.Dto.Cart;
using EISCS.Shop.DO.Interface;
using EISCS.Wrappers.Configuration;

namespace EISCS.Shop.DO.Service {
	public class ShopSalesService : BaseDataService, IShopSalesService
	{
		private readonly IShopSalesHeaderRepository _shopSalesHeaderRepository;
		private readonly IShopSalesLineRepository _shopSalesLineRepository;
		private readonly ICartHeaderRepository _cartHeaderRepository;
	    private readonly ICartLineRepository _cartLineRepository;
	    private readonly IConfigurationObject _config;
	    private readonly ICurrencyConverter _currencyConverter;
	    private readonly IKeyPerformanceIndicatorsService _kpiService;

	    public ShopSalesService(IExpanditDbFactory dbFactory, IShopSalesLineRepository shopSalesLineRepository, IShopSalesHeaderRepository shopSalesHeaderRepository, ICartHeaderRepository cartHeaderRepository, ICartLineRepository cartLineRepository, IConfigurationObject config, ICurrencyConverter currencyConverter, IKeyPerformanceIndicatorsService kpiService) : base(dbFactory)
		{
			_shopSalesLineRepository = shopSalesLineRepository;
			_shopSalesHeaderRepository = shopSalesHeaderRepository;
			_cartHeaderRepository = cartHeaderRepository;
	        _cartLineRepository = cartLineRepository;
	        _config = config;
	        _currencyConverter = currencyConverter;
	        _kpiService = kpiService;
	        _cartLineRepository = cartLineRepository;
		}

	    public ShopSalesHeader GetShopSalesHeader(string customerReference)
	    {
	        var headers = _shopSalesHeaderRepository.GetShopSalesHeadersByCustRef(customerReference);
	        return headers != null && headers.Count > 0 ? headers[0] : null;
	    }

	    public List<ShopSalesHeader> GetShopSalesHeaderByUser(string userGuid)
	    {
	        return _shopSalesHeaderRepository.GetShopSalesHeadersByUser(userGuid);
	    } 

		public ShopSalesHeader GetLatestShopSalesHeaderByUser(string userGuid)
		{
			return _shopSalesHeaderRepository.GetShopSalesHeadersByUser(userGuid).FirstOrDefault();
		}

	    public ShopSalesHeader CartHeader2ShopSalesHeader(CartHeader cartHeader, string userGuid)
		{
	        if (cartHeader.Total.Equals(0.0) || _cartLineRepository.GetCartLinesById(cartHeader.HeaderGuid).Count == 0)
	        {
	            return null;
	        }
			var shopSalesHeader = Mapper.Map<ShopSalesHeader>(cartHeader);
			shopSalesHeader.HeaderDate = DateTime.Now;
			SaveShopSalesHeader(shopSalesHeader);
			DeleteCartHeader(cartHeader);
	        _kpiService.NewSale(shopSalesHeader);
			return shopSalesHeader;
		}

		private void DeleteCartHeader(CartHeader cartHeader)
		{
			foreach (var line in cartHeader.Lines)
			{
				_cartHeaderRepository.Remove(line.LineGuid);
			}

			_cartHeaderRepository.Remove(cartHeader.HeaderGuid);
		}

		private void SaveShopSalesHeader(ShopSalesHeader shopSalesHeader)
		{
			foreach (var line in shopSalesHeader.Lines)
			{
				_shopSalesLineRepository.Add(line);
			}

			_shopSalesHeaderRepository.Add(shopSalesHeader);
		}

	    public void MailUpdate(ShopSalesHeader shopSalesHeader)
	    {
	        _shopSalesHeaderRepository.MailUpdate(shopSalesHeader);
	    }

	    public List<ShopSalesHeader> GetLastShopSales()
	    {
	        var salesHeaders = _shopSalesHeaderRepository.GetLastShopSales();
	        foreach (var saleHeader in salesHeaders)
	        {
	            saleHeader.Lines = _shopSalesLineRepository.GetShopSalesLinesByHeaderGuid(saleHeader.HeaderGuid);
	        }
	        return salesHeaders;
	    }

	    public List<ShopSalesHeader> GetLatestShopSalesInXDays(int days)
	    {
	        var salesHeaders = _shopSalesHeaderRepository.GetLatestShopSalesInXDays(days);

	        foreach (var sale in salesHeaders.Where(sale => sale.CurrencyGuid != _config.Read("MULTICURRENCY_SITE_CURRENCY")))
	        {
	            sale.Total = _currencyConverter.ConvertCurrency(sale.Total, sale.CurrencyGuid, _config.Read("MULTICURRENCY_SITE_CURRENCY"));
	            sale.SubTotal = _currencyConverter.ConvertCurrency(sale.SubTotal, sale.CurrencyGuid, _config.Read("MULTICURRENCY_SITE_CURRENCY"));
	            sale.TotalInclTax = _currencyConverter.ConvertCurrency(sale.TotalInclTax, sale.CurrencyGuid, _config.Read("MULTICURRENCY_SITE_CURRENCY"));
	            sale.SubTotalInclTax = _currencyConverter.ConvertCurrency(sale.SubTotalInclTax, sale.CurrencyGuid, _config.Read("MULTICURRENCY_SITE_CURRENCY"));
	            sale.CurrencyGuid = _config.Read("MULTICURRENCY_SITE_CURRENCY");
	        }

	        return salesHeaders;
	    }

	    public string GetRevenueBetweenDates(DateTime initDate, DateTime endDate)
	    {
	        var salesHeaders = _shopSalesHeaderRepository.GetSalesHeadersBetweenDates(initDate, endDate);
	        var total = 0.0;
            foreach (var sale in salesHeaders)
	        {
	            if (sale.CurrencyGuid != _config.Read("MULTICURRENCY_SITE_CURRENCY"))
	            {
                    sale.TotalInclTax = _currencyConverter.ConvertCurrency(sale.TotalInclTax, sale.CurrencyGuid, _config.Read("MULTICURRENCY_SITE_CURRENCY"));
                    sale.CurrencyGuid = _config.Read("MULTICURRENCY_SITE_CURRENCY");
	            }
	            total += sale.TotalInclTax;
	        }
	        return CurrencyFormatter.FormatCurrency(total, _config.Read("MULTICURRENCY_SITE_CURRENCY"));
	    }

        public List<ShopSalesHeader> GetShopSalesRange(string userGuid, int startIndex, int endIndex, string searchValue, string sort = null, string direction = null)
	    {
            var salesHeaders = _shopSalesHeaderRepository.GetShopSalesRange(userGuid, startIndex, endIndex, searchValue, sort, direction);
            foreach (var saleHeader in salesHeaders)
            {
                saleHeader.Lines = _shopSalesLineRepository.GetShopSalesLinesByHeaderGuid(saleHeader.HeaderGuid);
            }
            return salesHeaders;
	    }
	}
}
