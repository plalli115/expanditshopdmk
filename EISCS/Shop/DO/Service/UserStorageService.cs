using System;
using System.Collections.Generic;
using System.Linq;
using CmsPublic.DataRepository;
using EISCS.Shop.DO.Dto;
using EISCS.Shop.DO.Interface;
using EISCS.Wrappers.Configuration;

namespace EISCS.Shop.DO.Service
{
    public class UserStorageService : BaseUserStorageService, IUserStorageService
    {
        private readonly IConfigurationObject _configurationObject;


        public UserStorageService(IExpanditDbFactory dbFactory, IConfigurationObject config)
            : base(dbFactory)
        {
            _configurationObject = config;
        }

        public IEnumerable<UserTable> GetAllB2BUsers()
        {
            return GetResults<UserTable>("SELECT * FROM UserTableB2B");
        }

        public string FindCustomerGuidByUser(string userGuid)
        {
            const string sqlB2B = "SELECT TOP 1 CustomerGuid FROM UserTableB2B WHERE EnableLogin<>0 AND UserGuid = @userGuid";
            return GetSingle<string>(sqlB2B, new { userGuid });
        }

        // TODO: Might be replaced with new logic where UserTableB2B is gone. Hard coded B2B is probably completly wrong!
        public UserTable GetUserByCustomerAndUserGuid(string customerGuid, string userGuid)
        {
            var customerParam = new { customerGuid, userGuid };
            string sqlB2B = @"
                    SELECT IsB2B = 1, ut.EmailAddress, ut.UserGuid, ut.UserLogin, ut.UserPassword, ut.PasswordVersion, ut.CustomerGuid, ut.ContactName, RoleId = 'B2B', ct.CompanyName, ct.CountryGuid, ct.Address1, ct.CityName, ct.EnableLogin, ct.ZipCode, ct.CurrencyGuid, ct.BillToCustomerGuid AS CustomerGroupId
                    FROM CustomerTable ct INNER JOIN UserTableB2B ut ON ct.CustomerGuid = ut.CustomerGuid
                    WHERE ut.UserGuid = @userGuid
                    AND ct.EnableLogin <> 0 AND ut.CustomerGuid = @customerGuid
                    AND ut.UserGuid = @userGuid";
            return GetResults<UserTable>(sqlB2B, customerParam).FirstOrDefault();
        }

        public UserTable GetUserByUserGuid(string userGuid)
        {

            #region Check and update RoleId

            // Query used to handle changes to user access permissions on the local and remote site. UserTable is a download table, UserRole is an upload table.
            string query = @"
                ;WITH Roles (userGuid, utRoleId, urRoleId) AS
                (
                SELECT ut.UserGuid, ut.RoleId, ur.RoleId AS urRoleId FROM UserTable ut LEFT JOIN UserRole ur ON ut.UserGuid = ur.UserGuid WHERE ut.UserGuid = @userGuid
                ),
                commonUserRole AS
                (
                SELECT userGuid, roleId = 
	                CASE
		                WHEN utRoleId <> urRoleId AND urRoleId IS NOT NULL
		                THEN 
			                urRoleId
		                ELSE
			                utRoleId
	                END
	                , isToUpdate = 
	                CASE
		                WHEN utRoleId <> urRoleId AND urRoleId IS NOT NULL
		                THEN 
			                1
		                ELSE
			                0
	                END
                FROM Roles
                )
                SELECT * FROM commonUserRole";
            var dynamicObject = GetResults(query, new { userGuid }).FirstOrDefault();
            if (dynamicObject != null)
            {
                if (dynamicObject.isToUpdate > 0)
                {
                    string theRoleId = dynamicObject.roleId;
                    Execute(@"UPDATE UserTable SET RoleId = @roleId WHERE UserGuid = @userGuid", new { userGuid, roleId = theRoleId });
                }
            }
    
            #endregion

            // Always select from UserTable
            const string sql = @"SELECT * FROM UserTable WHERE UserGuid = @userGuid";
            return GetResults<UserTable>(sql, new { userGuid }).FirstOrDefault();
        }

        public string GetCountryName(string countryGuid)
        {
            string query = string.Format(@"SELECT CountryName FROM CountryTable WHERE CountryGuid = @countryGuid");

            var param = new { countryGuid };

            return GetResults<string>(query, param).FirstOrDefault();
        }

        public IEnumerable<AccessRoles> GetUserAccessByRole(string accessClass, string userRole)
        {
            var userAccessParam = new { accessClass, userRole };

            const string sql = @"
                SELECT * FROM AccessRoles WHERE RoleId = @userRole AND AccessClass = @accessClass;
                ";
            return GetResults<AccessRoles>(sql, userAccessParam);
        }

        public string GetCustomerGroup(string customerGuid)
        {
            string strBillToCustomerGuid = "";

            string query = @"SELECT BillToCustomerGuid  
                             FROM CustomerTable 
                             WHERE CustomerGuid = @customerGuid";

            strBillToCustomerGuid = GetSingle<string>(query, new { customerGuid });
            if (strBillToCustomerGuid == "")
                strBillToCustomerGuid = customerGuid;

            return strBillToCustomerGuid;
        }

        public IEnumerable<UserTable> GetAllUsers()
        {
            return GetAllFrom<UserTable>("UserTable");
        }

        public int GetAllUsersCount()
        {
            const string query = @"SELECT COUNT(*) FROM UserTable";
            return GetSingle<int>(query);
        }

        public bool NewLogin(string userGuid)
        {
            var now = DateTime.Now;
            const string query = @"UPDATE UserTable SET LastLoginDate = @now WHERE UserGuid = @userGuid";
            var param = new { now, userGuid };
            return Execute(query, param);
        }
    }
}