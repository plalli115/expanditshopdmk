﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using EISCS.Shop.DO.Dto.Cart;
using EISCS.Shop.DO.Dto.Product;
using EISCS.Shop.DO.Interface;
using EISCS.Shop.DO.Repository;

namespace EISCS.Shop.DO.Service
{
    public class ProductServiceHelper
    {



        public static CartHeader CreateCartHeader( IExpanditUserService userService)
        {
            var info = new CartHeader
            {
                UserGuid = userService.UserGuid,
                TaxPct = ConfigurationManager.AppSettings["TAX_PCT"],
                CurrencyGuid = userService.CurrencyGuid
            };
            return info;
        }


    }
}
