﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using CmsPublic.DataProviders.Logging;
using CmsPublic.DataRepository;
using CmsPublic.ModelLayer.Interfaces;
using CmsPublic.ModelLayer.Models;
using CmsPublic.Utilities;
using Dapper;
using EISCS.ExpandITFramework.Util;
using EISCS.Shop.DO.Dto;
using EISCS.Shop.DO.Interface;

namespace EISCS.Shop.DO.Service
{
    public class PropertiesDataService : BaseDataService, IPropertiesDataService
    {
        private const string TemplateTable = "TemplateTable";
        private const string TemplatePropertyTable = "TemplateProperty";
        public PropertiesDataService(IExpanditDbFactory dbFactory)
            : base(dbFactory)
        {
        }


        public Dictionary<string, List<PropValPropTransQuerySet>> LoadProperties(string propOwner, string languageGuid)
        {
            string sql = @" WITH temp(PropOwnerRefGuid, PropValGuid, PropGuid, PropOwnerTypeGuid) AS 
                            ( 
                                SELECT PropOwnerRefGuid, PropValGuid, PropGuid, PropOwnerTypeGuid 
                                FROM PropVal 
                                WHERE EXISTS(SELECT * FROM GroupProduct WHERE GroupProduct.ProductGuid = PropOwnerRefGuid) 
                            ) 
                            SELECT  PropTransText, PropOwnerRefGuid, PropGuid, PropTrans.PropLangGuid 
                            FROM    PropTrans 
                                    INNER JOIN temp ON PropTrans.PropValGuid = temp.PropValGuid 
                            WHERE   PropOwnerTypeGuid=@propOwnerTypeGuid 
                            AND     (PropLangGuid IS NULL OR PropLangGuid= @languageGuid) 
                            OPTION(HASH JOIN) ";

            var results = GetResults<PropValPropTransQuerySet>(sql, new { languageGuid, propOwnerTypeGuid = propOwner }).ToList();
            return Utilities.ConvertToDictionary(results, x => x.PropOwnerRefGuid);
        }

        public Dictionary<string, List<PropValPropTransQuerySet>> LoadProperties2(string propOwner, string languageGuid, string productGuids)
        {
            string sql =

            @"WITH temp(PropOwnerRefGuid, PropValGuid, PropGuid, PropOwnerTypeGuid) AS 
            ( 
                SELECT  PropOwnerRefGuid, PropValGuid, PropGuid, PropOwnerTypeGuid 
                FROM    PropVal 
                WHERE   PropOwnerRefGuid IN(" + productGuids + @") 
            ) 
            SELECT  PropTransText, PropOwnerRefGuid, PropGuid, PropTrans.PropLangGuid 
            FROM    PropTrans 
                    INNER JOIN temp ON PropTrans.PropValGuid = temp.PropValGuid 
            WHERE   PropOwnerTypeGuid=@propOwnerTypeGuid 
            AND     (PropLangGuid IS NULL OR PropLangGuid= @languageGuid) 
            OPTION(HASH JOIN) ";

            var results = GetResults<PropValPropTransQuerySet>(sql, new { languageGuid, propOwnerTypeGuid = propOwner }).ToList();
            return Utilities.ConvertToDictionary(results, x => x.PropOwnerRefGuid);
        }


        public Dictionary<string, List<PropValPropTransQuerySet>> LoadSubGroupProductProperties(int groupGuid, string propOwner, string languageGuid)
        {
            string sql = @"WITH temp_GroupTable(GroupGuid, iteration) AS 
            ( 
                SELECT GroupGuid, 0 
                FROM GroupTable WHERE ParentGuid = @groupGuid 
                UNION ALL 
                SELECT b.GroupGuid, a.iteration + 1 
                FROM temp_GroupTable AS a, GroupTable AS b 
                WHERE a.GroupGuid = b.ParentGuid 
            ),
            temp_GroupProduct(ProductGuid) AS 
            ( 
                SELECT  ProductGuid 
                FROM    GroupProduct 
                WHERE   EXISTS(SELECT ProductGuid FROM temp_GroupTable WHERE GroupProduct.GroupGuid = temp_GroupTable.GroupGuid) 
            ) 
            ,temp(PropOwnerRefGuid, PropValGuid, PropGuid, PropOwnerTypeGuid) AS 
            ( 
                SELECT  PropOwnerRefGuid, PropValGuid, PropGuid, PropOwnerTypeGuid 
                FROM    PropVal 
                WHERE   EXISTS(SELECT * FROM temp_GroupProduct WHERE temp_GroupProduct.ProductGuid = PropOwnerRefGuid) 
            ) 
            SELECT PropTransText, PropOwnerRefGuid, PropGuid, PropTrans.PropLangGuid 
            FROM PropTrans INNER JOIN temp 
            ON PropTrans.PropValGuid = temp.PropValGuid 
            WHERE PropOwnerTypeGuid=@propOwnerTypeGuid 
            AND (PropLangGuid IS NULL OR PropLangGuid= @languageGuid) 
            OPTION(HASH JOIN)";

            var results = GetResults<PropValPropTransQuerySet>(sql, new { languageGuid, propOwnerTypeGuid = propOwner, groupGuid }).ToList();
            return Utilities.ConvertToDictionary(results, x => x.PropOwnerRefGuid);
        }

        public string GetSingleProperty(string propertyOwner, string propertyOwnerType, string propName, string languageGuid)
        {
            string sql =
                @"SELECT    PropTransText 
                    FROM    PropVal 
                            INNER JOIN PropTrans ON PropVal.PropValGuid=PropTrans.PropValGuid 
                    WHERE   PropOwnerTypeGuid=@propertyOwnerType 
                    AND     (PropLangGuid = @languageGuid) 
                    AND     PropVal.PropGuid = @propName 
                    AND     PropOwnerRefGuid = @propertyOwner 
                    ORDER BY PropOwnerRefGuid, PropVal.PropGuid 
                    OPTION(HASH JOIN); ";

            return GetSingle<string>(sql, new { propertyOwnerType, propName, languageGuid, propertyOwner });
        }

        public void LoadGpProperties(ICollection<GroupProduct> propOwner, string languageGuid, string groupGuid)
        {
            List<PropValPropTransQuerySet> d = LoadGpPropQuerySet(propOwner, languageGuid, groupGuid);

            foreach (GroupProduct prod in propOwner)
            {
                List<PropValPropTransQuerySet> temp =
                    d.FindAll(desc => string.Equals(desc.PropOwnerRefGuid, (prod as IPropertyOwner).propertyIdentifier));

                var prop = Activator.CreateInstance<PrdPropValSet>();

                (prod as IPropertyOwner).IProperties = prop;
                (prop as IPropValSet).parentGuid = (prod as IPropertyOwner).propertyIdentifier;

                // Load properties
                var propTypeDict = new Dictionary<string, string>();
                string propGrpTypeGuid = GetPropGrpTypeGuid(propOwner.First());
                prop.PropDict = LoadPropData(temp, propTypeDict, propGrpTypeGuid);
                // Include property types
                prop.PropTypeDict = propTypeDict;
            }
        }

        private List<string> ReadData2String(string propGrpGuid)
        {
            string sql = @" SELECT  PropGuid 
                            FROM    PropGrpRel 
                            WHERE   PropGrpGuid = @propGrpGuid";

            return GetResults<string>(sql, new { propGrpGuid }).ToList();

            
        }

        private Dictionary<string, string> LoadPropData(List<PropValPropTransQuerySet> d,
                                                Dictionary<string, string> propTypeDict, string propGrpGuid)
        {
            var myClonedDict = new Dictionary<string, string>();

            List<string> propertyList = ReadData2String(propGrpGuid);

            foreach (string s in propertyList)
            {
                myClonedDict.Add(s, null);
                myClonedDict.Add("_" + s, null);
                propTypeDict.Add(s, null);
            }
            if (d != null && d.Count > 0)
            {
                foreach (string key in propertyList)
                {
                    try
                    {
                        PropValPropTransQuerySet found = (d.Find(desc => string.Equals(desc.PropGuid, key)));
                        if (found != null)
                        {
                            myClonedDict[key] = found.PropTransText;
                            //(d.Find(desc => string.Equals(desc.PropGuid, key))).PropTransText;
                            propTypeDict[key] = found.PropTypeGuid;
                            // TEST
                            myClonedDict["_" + key] = found.PropLangGuid;
                        }
                    }
                    catch
                    {
                    }
                }
            }
            return myClonedDict;
        }


        public void LoadProperties(ICollection<IPropertyOwner> propOwner, string languageGuid)
        {
            
            Dictionary<string, List<PropValPropTransQuerySet>> d = LoadPropQuerySet(propOwner, languageGuid);

            foreach (IPropertyOwner prod in propOwner)
            {
                List<PropValPropTransQuerySet> temp = d.ContainsKey(prod.propertyIdentifier) ? d[prod.propertyIdentifier] : new List<PropValPropTransQuerySet>();

                string propGrpTypeGuid = GetPropGrpTypeGuid(prod);


                if (propGrpTypeGuid == "PRODUCT")
                {
                    var prop = Activator.CreateInstance<PrdPropValSet>();

                    prod.IProperties = prop;
                }
                else if (propGrpTypeGuid == "GROUP")
                {
                    var prop = Activator.CreateInstance<GrpPropValSet>();

                    prod.IProperties = prop;

                }
                else if (propGrpTypeGuid == "MAILMESSAGE")
                {
                    var prop = Activator.CreateInstance<MailMessagePropValSet>();

                    prod.IProperties = prop;

                }

                prod.IProperties.parentGuid = prod.propertyIdentifier;


                var prop2 = prod.IProperties;
                // Load properties
                var propTypeDict = new Dictionary<string, string>();
                
                prop2.PropDict = LoadPropData(temp, propTypeDict, propGrpTypeGuid);
                // Include property types
                prop2.PropTypeDict = propTypeDict;
                (prod as IPropertyOwner).PropCollection = temp;
            }
            
        }

        public string GetExternalLanguageGuid(string languageGuid)
        {
            var sql = @"SELECT ExternalLanguageGuid FROM LanguageTable WHERE LanguageGuid = @languageGuid";
            return GetSingle<string>(sql, new { languageGuid });
        }


        private Dictionary<string, List<PropValPropTransQuerySet>> LoadPropQuerySet(ICollection<IPropertyOwner> propOwnerIdentifier, string languageGuid)
        {
            string propertyOwnerType = (propOwnerIdentifier.ElementAt(0) as IPropertyOwner).propType;

            
            var table = " GroupTable ";
            var tableClause = " WHERE GroupTable.GroupGuid ";

            if (propertyOwnerType == "PRD")
            {
                table = " ProductTable ";
                tableClause = " WHERE ProductTable.ProductGuid ";
            }

            string specificItems = " WHERE EXISTS(SELECT * FROM " + table + tableClause + " = PropOwnerRefGuid )";
            if (propOwnerIdentifier.Count <= 100)
            {
                string propOwnerRefGuidInClause = Utilities.ToInClauseString(propOwnerIdentifier.Select(x => x.propertyIdentifier).ToList(), "PropOwnerRefGuid");
                specificItems = " WHERE " + propOwnerRefGuidInClause + "";
            }

            string sql =
                @"
                    ;WITH temp(PropOwnerRefGuid, PropValGuid, PropGuid, PropOwnerTypeGuid, PropMultilanguage, PropName, PropTypeGuid) AS 
                     (
                        SELECT  PropOwnerRefGuid, PropValGuid, PropVal.PropGuid, PropOwnerTypeGuid, PropMultilanguage, PropName, PropTypeGuid 
                        FROM    PropVal  
                                LEFT JOIN PropTable p ON p.PropGuid = PropVal.PropGuid "
                        + specificItems + @"
                    ) 
                    SELECT  PropOwnerRefGuid, PropGuid, PropMultilanguage, PropTrans.PropLangGuid, PropTransText, PropName, PropTypeGuid 
                    FROM    PropTrans 
                            INNER JOIN temp ON PropTrans.PropValGuid = temp.PropValGuid 
                    WHERE   PropOwnerTypeGuid=@propertyOwnerType 
                    AND     (PropLangGuid IS NULL OR PropLangGuid= @languageGuid) 
                    ORDER BY PropOwnerRefGuid, PropGuid 
                ";

            var resultList = GetResults<PropValPropTransQuerySet>(sql, new { propertyOwnerType, languageGuid }).ToList();
            return Utilities.ConvertToDictionary(resultList, x => x.PropOwnerRefGuid);
        }

        public List<PropValPropTransQuerySet> LoadGpPropQuerySet(IEnumerable<IPropertyOwner> propOwnerIdentifier,
                                                         string languageGuid, string groupGuid)
        {

            string propOwnerTypeGuid = (propOwnerIdentifier.ElementAt(0) as IPropertyOwner).propType;

            string sql = @" WITH temp(PropOwnerRefGuid, PropValGuid, PropGuid, PropOwnerTypeGuid) AS 
                             ( 
                                SELECT  PropOwnerRefGuid, PropValGuid, PropGuid, PropOwnerTypeGuid 
                                FROM    PropVal
                                WHERE   EXISTS(SELECT * FROM GroupProduct WHERE GroupProduct.ProductGuid = PropOwnerRefGuid AND GroupGuid = @groupGuid) 
                            ) 
                            SELECT  PropTransText, PropOwnerRefGuid, PropGuid, PropTrans.PropLangGuid 
                            FROM    PropTrans 
                                    INNER JOIN temp ON PropTrans.PropValGuid = temp.PropValGuid 
                            WHERE   PropOwnerTypeGuid=@propOwnerTypeGuid 
                            AND     (PropLangGuid IS NULL OR PropLangGuid= @languageGuid) 
                            OPTION(HASH JOIN) ";

            return GetResults<PropValPropTransQuerySet>(sql, new { languageGuid, groupGuid, propOwnerTypeGuid }).ToList();
        }

        public IEnumerable<PropertyNode> SetPropertyNodeList(IPropValSet propValSet)
        {

            List<PropertyNode> propertyNodeList = new List<PropertyNode>();

            Dictionary<string, string> MyClonedDict = new Dictionary<string, string>();
            foreach (string s in propValSet.PropDict.Keys)
            {
                MyClonedDict.Add(s, propValSet.PropDict[s]);
            }

            foreach (string s in propValSet.PropDict.Keys)
            {
                if (!s.StartsWith("_"))
                {
                    string mval = MyClonedDict["_" + s];
                    bool isChecked = false;
                    if (mval != null)
                    {
                        try
                        {
                            if (mval.Contains("true"))
                            {
                                isChecked = true;
                            }
                        }
                        catch (Exception ex)
                        {
                            FileLogger.Log("IN FILTER PROPERTIES2: " + ex.Message);
                        }
                    }
                    propertyNodeList.Add(new PropertyNode(s, MyClonedDict[s], isChecked));
                }
            }
            return propertyNodeList;
        }


        public void SaveProperties(IPropValSet propValSet, string languageGuid)
        {
            IEnumerable<PropertyNode> propertyNodeList = SetPropertyNodeList(propValSet);

            IDbConnection conn = null;
            try
            {

                conn = GetConnection();

                //PropertyInfo[] propInfoArray = propValSet.GetType().GetProperties();

                // Multilanguage loop            
                foreach (PropertyNode pin in propertyNodeList)
                {

                    string pinValue = null;
                    try
                    {
                        pinValue = pin.Value;
                    }
                    catch (Exception e)
                    {
                        FileLogger.Log("ERROR SETTING valueParam " + e.Message);
                    }

                    var sqlLanguageGuid = languageGuid;
                    if (!pin.IsMulti)
                    {
                        sqlLanguageGuid = null;
                    }

                    Object param = new
                    {
                        languageGuid = sqlLanguageGuid,
                        propOwnerTypeGuidParam = propValSet.propOwnerTypeGuid,
                        propOwnerRefParam = propValSet.parentGuid,
                        newPropValGuidParam = Utilities.CreateGuid(),
                        newPropTransGuidParam = Utilities.CreateGuid(),
                        valueParam = pinValue,
                        propGuidParam = pin.Name
                    };
                    // Only create entries with real values - it would be a huge waste of system resources to add "white space entries" to the property tables
                    if (!string.IsNullOrEmpty(pinValue))
                    {




                        string langGuidString = sqlLanguageGuid != null ? "AND(PropLangGuid = @languageGuid) " : "AND PropLangGuid IS NULL ";

                        string sql =

                            // If the value isn't found by performing a regular join... 
                            @"IF NOT EXISTS( 
                                SELECT PropTransText 
                                FROM PropVal pv INNER JOIN PropTrans pt 
                                ON pv.PropValGuid = pt.PropValGuid 
                                WHERE pv.PropOwnerRefGuid = @propOwnerRefParam 
                                AND pv.PropGuid = @propGuidParam " +
                                langGuidString +
                            @") 

                            -- ...Then check for the existence of such a property in the PropTable.
                            BEGIN 
                                IF EXISTS( 
                                SELECT PropGuid FROM PropTable WHERE PropGuid = @propGuidParam) 

                                -- If the property exist in the PropTable then continue to the next step
                                BEGIN 

                                    -- First check if there is an entry in the PropVal Table already, if not then...
                                    IF NOT EXISTS( 
                                        SELECT PropOwnerTypeGuid, PropOwnerRefGuid, PropGuid 
                                        FROM PropVal 
                                        WHERE PropOwnerRefGuid = @propOwnerRefParam 
                                        AND PropGuid = @propGuidParam 
                                        AND PropOwnerTypeGuid = @propOwnerTypeGuidParam
                                    ) 

                                    -- ...Insert a new entry into both PropVal and PropTrans
                                    BEGIN 

                                        INSERT INTO PropVal (PropValGuid, PropOwnerTypeGuid, PropOwnerRefGuid, PropGuid, PropMultilanguage) 
                                        VALUES(
                                            @newPropValGuidParam , 
                                            @propOwnerTypeGuidParam, 
                                            @propOwnerRefParam,
                                            @propGuidParam, 
                                            1) 
                                        -- This is correct. A new value should be MultiLanguage by default. 
                                    
                                        -- This is redundant information which we don't really use but it's already in all the existing DB's.
                                        INSERT INTO PropTrans (PropTransGuid, PropValGuid, PropLangGuid, PropTransText) 
                                        VALUES( 
                                            @newPropTransGuidParam, 
                                            @newPropValGuidParam, 
                                            @languageGuid, 
                                            @valueParam) 

                                     END 
                                    ELSE

                                    -- An entry already exists in the PropTrans Table...
                                    BEGIN 

                                    DECLARE @pValGuid NVARCHAR(100)
                                
                                    SELECT  @pValGuid = PropValGuid 
                                    FROM    PropVal 
                                    WHERE   PropOwnerRefGuid = @propOwnerRefParam 
                                    AND     PropGuid = @propGuidParam 
                                    AND     PropOwnerTypeGuid = @propOwnerTypeGuidParam 

                                    -- ...So we add an entry to the PropTrans Table with the matching PropValGuid from the PropVal Table
                                    INSERT INTO PropTrans (PropTransGuid, PropValGuid, PropLangGuid, PropTransText) 
                                        VALUES( 
                                        @newPropTransGuidParam, 
                                        @pValGuid, 
                                        @languageGuid,
                                       @valueParam) 
                                    END 
                                END 
                              END 
                            ELSE 
                            BEGIN 
                                -- The value is found by performing a regular join, and we only need to perform an update
                                UPDATE PropTrans 
                                    SET PropTransText = @valueParam 
                                    FROM PropVal INNER JOIN PropTrans 
                                    ON PropVal.PropValGuid = PropTrans.PropValGuid 
                                    WHERE PropVal.PropOwnerRefGuid = @propOwnerRefParam 
                                    AND PropVal.PropGuid = @propGuidParam " +
                                    langGuidString +
                            "END ";
                        conn.Execute(sql, param);

                    }
                    else
                    { // If the incoming property text is empty we must still check for the property value in the DB. If it exists it should be deleted.                    


                        string sql = @" DECLARE @pVal NVARCHAR(100) 
                                         DECLARE @pValGuid NVARCHAR(100) 
                         
                            SELECT  @pValGuid = pv.PropValGuid 
                            FROM    PropVal pv 
                                    INNER JOIN PropTrans pt ON pv.PropValGuid = pt.PropValGuid 
                            WHERE   pv.PropOwnerRefGuid = @propOwnerRefParam 
                            AND     pv.PropGuid = @propGuidParam 
                            AND     (PropLangGuid = @languageGuid OR PropLangGuid IS NULL) 

                            SELECT  @pVal = COUNT(pv.PropValGuid) 
                            FROM    PropVal pv 
                                    INNER JOIN PropTrans pt ON pv.PropValGuid = pt.PropValGuid 
                            WHERE   pv.PropOwnerRefGuid = @propOwnerRefParam 
                            AND     pv.PropGuid = @propGuidParam 

                             IF @pVal <= 1 
                         	    BEGIN	 
                         	    DELETE FROM PropTrans 
                         	    WHERE PropValGuid = @pValGuid 
                         	    DELETE FROM PropVal 
                         	    WHERE PropValGuid = @pValGuid 
                         	    END 
                             ELSE 
                         	    BEGIN 
                         	    UPDATE PropTrans 
                         	    SET PropTransText = '' 
                         	    WHERE PropValGuid = @pValGuid 
                         	    AND(PropLangGuid = @languageGuid OR PropLangGuid IS NULL) 
                             END";

                        conn.Execute(sql, param);
                    }
                }

            }
            finally
            {
                if (conn != null)
                {
                    DbFactory.Finalize(conn);
                }
            }
        }

        public void LoadPropertiesForEdit(ICollection<IPropertyOwner> propOwner, string languageGuid)
        {
            string propGrpTypeGuid = GetPropGrpTypeGuid(propOwner.Single());
            List<PropValPropTransQuerySet> d = LoadPropQuerySetForEdit(propOwner, languageGuid, propGrpTypeGuid);
            foreach (var pOwner in propOwner)
            {
                List<PropValPropTransQuerySet> temp = d.FindAll(desc => string.Equals(desc.PropOwnerRefGuid,
                                                                                      (pOwner as IPropertyOwner).
                                                                                          propertyIdentifier) ||
                                                                        desc.PropOwnerRefGuid == null);
                (pOwner as IPropertyOwner).PropCollection = temp;
                LoadAdditionalEditorInfo((pOwner), languageGuid, propGrpTypeGuid);
            }
        }

        private string GetPropGrpTypeGuid(IPropertyOwner owner)
        {
            if (owner is Group )
            {
                return "GROUP";
            } 
            if (owner is Product)
            {
                return "PRODUCT";
            } 
            if (owner is ExpanditMailMessage)
            {
                return "MAILMESSAGE";
            }

            return "";
        }

        // For the editor output        
        private List<PropValPropTransQuerySet> LoadPropQuerySetForEdit(ICollection<IPropertyOwner> propOwnerIdentifier,
                                                                       string languageGuid, string propGrpGuid)
        {
            string propOwnerTypeGuid = (propOwnerIdentifier.ElementAt(0)).propType;
            string propOwnerRefGuidInClause = Utilities.ToInClauseString(propOwnerIdentifier.Select(x => x.propertyIdentifier).ToList(), "PropOwnerRefGuid");

            string sql =
                @"  SELECT  PropOwnerRefGuid, PropTable.PropGuid, PropMultilanguage, PropLangGuid, PropTransText, PropName, PropTypeGuid, PropGrpRel.SortIndex  
                    FROM    PropGrpRel 
                            JOIN PropTable ON PropGrpRel.PropGuid = PropTable.PropGuid 
                            LEFT JOIN PropVal ON PropTable.PropGuid = PropVal.PropGuid 
                            INNER JOIN PropTrans ON PropVal.PropValGuid=PropTrans.PropValGuid 
                    WHERE   PropGrpRel.PropGrpGuid = @propGrpGuid 
                    AND     (" + propOwnerRefGuidInClause + @" OR PropOwnerRefGuid IS NULL) 
                    AND     (PropOwnerTypeGuid= @propOwnerTypeGuid OR PropOwnerTypeGuid IS NULL) 
                    AND     (PropLangGuid IS NULL OR PropLangGuid= @languageGuid)
                    ORDER BY PropGrpRel.SortIndex, PropOwnerRefGuid, PropTable.PropGuid 
                    OPTION(HASH JOIN)";

            return GetResults<PropValPropTransQuerySet>(sql, new { propOwnerTypeGuid, propGrpGuid, languageGuid }).ToList();
        }

        private void LoadAdditionalEditorInfo(IPropertyOwner propOwner, string languageGuid, string propType)
        {

            string sql =
                @"  SELECT  @propOwnerRefGuid AS PropOwnerRefGuid, PropTable.PropGuid, PropMultilanguageDefault AS PropMultilanguage, NULL AS PropLangGuid, NULL AS PropTransText, PropName, PropTypeGuid, PropGrpRel.SortIndex 
                    FROM    PropGrpRel 
                            JOIN PropTable ON PropGrpRel.PropGuid = PropTable.PropGuid 
                    AND     PropGrpRel.PropGrpGuid = @propType 
                    ORDER BY PropGrpRel.SortIndex ";


            //propOwnerRefGuidParam.Value = propOwner.propertyIdentifier; // The ProductGuid/GroupGuid
            List<PropValPropTransQuerySet> pvptSet = GetResults<PropValPropTransQuerySet>(sql, new { propType, propOwnerRefGuid = propOwner.propertyIdentifier, languageGuid }).ToList();

            // Find what's missing in the PropTransPropValSet compared to the PropTable.            
            for (int i = 0; i < propOwner.PropCollection.Count; i++)
            {
                for (int j = 0; j < pvptSet.Count; j++)
                {
                    if (pvptSet.ElementAt(j).PropGuid.Equals(propOwner.PropCollection.ElementAt(i).PropGuid))
                    {
                        pvptSet.RemoveAt(j);
                    }
                }
            }
            // And add that information to the Set
            propOwner.PropCollection.AddRange(pvptSet);

            // Find the language specific properties
            List<PropValPropTransQuerySet> currentLanguageProps = null;

            try
            {
                currentLanguageProps =
                    propOwner.PropCollection.FindAll(x => x.PropLangGuid != null && x.PropLangGuid.Equals(languageGuid));
            }
            catch (ArgumentNullException e)
            {
                FileLogger.Log("LoadAdditionalEditorInfo error: " + e.Message);
            }

            // Remove duplicates from the set. If language specific properties exists, those are the ones we want to keep.
            if (currentLanguageProps != null)
            {
                for (int i = 0; i < currentLanguageProps.Count; i++)
                {
                    for (int j = 0; j < propOwner.PropCollection.Count; j++)
                    {
                        try
                        {
                            if (
                                propOwner.PropCollection.ElementAt(j).PropGuid.Equals(
                                    currentLanguageProps.ElementAt(i).PropGuid))
                            {
                                if (
                                    !StringUtil.IsStringEqual(propOwner.PropCollection.ElementAt(j).PropLangGuid,
                                                              currentLanguageProps.ElementAt(i).PropLangGuid))
                                {
                                    propOwner.PropCollection.RemoveAt(j);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            FileLogger.Log(ex.Message + "; " + ex.StackTrace);
                        }
                    }
                }
            }

            // Additional filetering to find out if property is MultiLanguage or not
            // The database value for PropMultiLanguage can not be relied on
            //IDIOTIC
            string sql2 = @"    IF EXISTS(  SELECT  * 
                                            FROM    PropVal pv 
                                                    INNER JOIN PropTrans pt  ON pv.PropValGuid = pt.PropValGuid 
                                            WHERE   pv.PropOwnerRefGuid = @propOwnerRefGuid2 
                                            AND     pv.PropGuid = @propGuid2 
                                            AND     pt.PropLangGuid IS NOT NULL
                                        )
                                    SELECT 1 
                                ELSE
                                    SELECT 0";

            foreach (PropValPropTransQuerySet pv in propOwner.PropCollection)
            {
                pv.PropMultilanguage = GetSingle<int>(sql2, new { propOwnerRefGuid2 = pv.PropOwnerRefGuid, propGuid2 = pv.PropGuid });
            }

            // Sort the list by SortIndex. The comparer is implemented in PropValPropTransQuerySet
            propOwner.PropCollection.Sort();
        }


        // MUST SEND IN BOTH Session languageGuid and possible NULL value for languageGuid!!
        // Remove all values except for the current language and convert the languageGuid to null
        public void ML2NULL(string propOwnerRefGuid, string propGuid, string languageGuid)
        {
            // MUST HANDLE NULL LanguageGuid value!
            string sql =
                @"  BEGIN 
                    DECLARE @pVal NVARCHAR(100) 
                    
                    SELECT  @pVal = pv.PropValGuid 
                    FROM    PropVal pv 
                            INNER JOIN PropTrans pt ON pv.PropValGuid = pt.PropValGuid 
                    WHERE   pv.PropOwnerRefGuid = @propOwnerRefGuid
                    AND     pv.PropGuid = @propGuid 
                    AND     (PropLangGuid = @languageGuid) 


                    IF LEN(@pVal) > 0 
                    BEGIN 
                        DELETE FROM PropTrans 
                        WHERE   PropValGuid = @pVal 
                        AND     PropLangGuid <> @languageGuid; 

                        DELETE FROM PropTrans 
                        WHERE   PropValGuid = @pVal 
                        AND     PropLangGuid IS NULL;

                        UPDATE  PropTrans 
                        SET     PropLangGuid = NULL 
                        WHERE   PropValGuid = @pVal; 

                        UPDATE  PropVal 
                        SET     PropMultilanguage = 0 
                        WHERE   PropValGuid = @pVal; 
                     END 
                 END ";

            Execute(sql, new { propGuid, propOwnerRefGuid, languageGuid });
        }

        // Copy current value to all other languages and remove the null value
        public void NULL2ML(string propOwnerRefGuid, string propGuid, string languageGuid, List<LanguageTable> allLanguages)
        {

            List<PropValPropTransQuerySet> propValueList = CheckCurrentValues(propOwnerRefGuid, propGuid, languageGuid);
            List<PropValPropTransQuerySet> nullList = propValueList.FindAll(finder => finder.PropLangGuid == null);
            List<PropValPropTransQuerySet> langList = null;

            try
            {
                langList = propValueList.FindAll(finder => finder.PropLangGuid != null);
            }
            catch { }


            if (nullList != null && nullList.Count > 0)
            {
                // First delete the null entry from PropTrans
                // Then set PropTrans with the same values but with a new languageGuid
                

                string sqlDelete =
                    @"  DELETE FROM PropTrans 
                        WHERE   PropTransGuid = @propTransGuid";
                Execute(sqlDelete, new { propTransGuid = nullList[0].PropTransGuid });

                string sqlInsert =
                    @"  INSERT INTO PropTrans (
                                PropTransGuid, 
                                PropValGuid, 
                                PropLangGuid, 
                                PropTransText) 
                        VALUES( 
                                @propTransGuid, 
                                @propValGuid, 
                                @languageGuid,
                                @propTransText );

                        UPDATE  PropVal 
                        SET     PropMultilanguage = 1 
                        WHERE   PropValGuid = @propValGuid; ";

                Execute(sqlInsert, new { propTransText = nullList[0].PropTransText, languageGuid, propValGuid = nullList[0].PropValGuid, nullList[0].PropTransGuid });

                if (langList != null && langList.Count > 0)
                {
                    Execute(sqlDelete, new { propTransGuid = langList[0].PropTransGuid});
                }
            }
            else if (langList != null && langList.Count > 0)
            {
                string sqlUpdate =
                    @"  UPDATE  PropTrans 
                        SET     PropLangGuid = @languageGuid 
                        WHERE   PropValGuid = @propValGuid; 

                        UPDATE  PropVal 
                        SET     PropMultilanguage = 1 
                        WHERE   PropValGuid = @propValGuid; ";

                Execute(sqlUpdate, new { languageGuid, propValGuid = langList[0].PropValGuid });
            }

            if (allLanguages != null)
            {

                foreach (LanguageTable lang in allLanguages)
                {
                    if (lang != null)
                    {
                        if (!languageGuid.Equals(lang.LanguageGuid))
                        {


                            string sql =

                            @"  BEGIN 
                                
                                DECLARE @valueParam NVARCHAR(MAX) 
                                SELECT  @valueParam = PropTransText
                                FROM    PropVal pv 
                                        INNER JOIN PropTrans pt ON pv.PropValGuid = pt.PropValGuid 
                                WHERE   pv.PropOwnerRefGuid = @propOwnerRefGuid 
                                AND     pv.PropGuid = @propGuid 

                                DECLARE @pValGuid NVARCHAR(100) 
                                SELECT  @pValGuid = PropValGuid 
                                FROM    PropVal 
                                WHERE   PropOwnerRefGuid = @propOwnerRefGuid 
                                AND     PropGuid = @propGuid 

                            
                                INSERT INTO PropTrans (
                                                PropTransGuid, 
                                                PropValGuid, 
                                                PropLangGuid, 
                                                PropTransText) 
                                         VALUES ( 
                                                @newPropTransGuid,
                                                @pValGuid, 
                                                @languageGuid, 
                                                @valueParam ) 
                                END ";

                            Execute(sql, new { languageGuid  = lang.LanguageGuid, newPropTransGuid = "{" + Guid.NewGuid() + "}", propGuid, propOwnerRefGuid });
                        }
                    }
                }
            }
        }



        // Work around possible null values
        private List<PropValPropTransQuerySet> CheckCurrentValues(string propOwnerRefGuid, string propGuid, string languageGuid)
        {
            string sql =
                @"  SELECT  * 
                    FROM    PropVal pv 
                            INNER JOIN PropTrans pt ON pv.PropValGuid = pt.PropValGuid 
                    WHERE   pv.PropOwnerRefGuid = @propOwnerRefGuid 
                    AND     pv.PropGuid = @propGuid 
                    AND     (PropLangGuid IS NULL OR PropLangGuid = @languageGuid) ";

            List<PropValPropTransQuerySet> propList = GetResults<PropValPropTransQuerySet>(sql, new { propOwnerRefGuid, propGuid, languageGuid }).ToList();
            return propList;
        }


        public void DeleteAllProperties(IPropValSet propValSet)
        {
            // will need to run as many times as there are Properties in the dictionary to delete all Property values from the DB            

            foreach (string s in propValSet.PropDict.Keys)
            {

                string sqlDelete =

                    @"  BEGIN 
                            DECLARE @pVal NVARCHAR(100) 
                            SELECT  @pVal = pv.PropValGuid 
                            FROM    PropVal pv 
                                    INNER JOIN PropTrans pt ON pv.PropValGuid = pt.PropValGuid 
                            WHERE   pv.PropOwnerRefGuid = @propOwnerRefParam 

                            IF LEN(@pVal) > 0 
                                DELETE FROM PropTrans 
                                WHERE   PropValGuid = @pVal 
                                DELETE FROM PropVal 
                                WHERE   PropValGuid = @pVal 

                        END ";
                Execute(sqlDelete, new { propOwnerRefParam = propValSet.parentGuid });
            }
        }

        public string GetTemplateGuid(string templateName)
        {
            string sql = "SELECT * FROM TemplateTable WHERE TemplateName = @templateName";
            var template = GetSingle<TemplateQuerySet>(sql, new { templateName });
            return template.TemplateGuid;
        }

        public void InitTemplateTables()
        {


            string sql = @"SET ARITHABORT ON 
                            IF EXISTS (SELECT name FROM sysobjects WHERE name = '" + TemplatePropertyTable + @"' AND type = 'U') 
                                DROP TABLE " + TemplatePropertyTable + @"
                            IF EXISTS (SELECT name FROM sysobjects WHERE name = '" + TemplateTable + @"' AND type = 'U') 
                                DROP TABLE " + TemplateTable + @"
                            IF NOT EXISTS (SELECT name FROM sysobjects WITH (NOLOCK) WHERE name = '" + TemplatePropertyTable + @"' AND type = 'U') 
                                IF NOT EXISTS (SELECT name FROM sysobjects WITH (TABLOCKX) WHERE name = '" + TemplatePropertyTable + @"' AND type = 'U') 
                                    CREATE TABLE " + TemplatePropertyTable + @" ( 
                                                    TemplateGuid NVARCHAR(50) NOT NULL, 
                                                    PropGuid NVARCHAR(50) NOT NULL, 
                                                    PropGroupGuid NVARCHAR(50), 
                                                    PropHeader NVARCHAR(150), 
                                                    PRIMARY KEY(TemplateGuid, PropGuid)); 
                            DELETE FROM " + TemplatePropertyTable + @"; 
                            IF NOT EXISTS (SELECT name FROM sysobjects WITH (NOLOCK) WHERE name = '" + TemplateTable + @"' AND type = 'U') 
                                IF NOT EXISTS (SELECT name FROM sysobjects WITH (TABLOCKX) WHERE name = '" + TemplateTable + @"' AND type = 'U') 
                                    CREATE TABLE " + TemplateTable + @" ( 
                                                    TemplateGuid NVARCHAR(50) NOT NULL, 
                                                    TemplateName NVARCHAR(100), 
                                                    TemplateFileName NVARCHAR(250), 
                                                    TemplateDescription NVARCHAR(250), 
                                                    TemplateController NVARCHAR(250), 
                                                    IsGroupTemplate BIT NOT NULL, 
                                                    IsProductTemplate BIT NOT NULL, 
                                                    PRIMARY KEY(TemplateGuid)); 
                            DELETE FROM " + TemplateTable + "; ";

            Execute(sql);
        }

        public TemplateQuerySet GetTemplateInfo(string id)
        {
            string sql = "SELECT * FROM TemplateTable WHERE TemplateGuid = @id";
            return GetSingle<TemplateQuerySet>(sql, new {id});
        }

        public List<TemplateQuerySet> GetAllTemplateInfo()
        {
            return GetResults<TemplateQuerySet>("SELECT * From TemplateTable").ToList();
        }

        //should be done with repositories
        public void InsertTemplates(List<TemplateQuerySet> propData)
        {
            string sql = null;
            foreach (TemplateQuerySet propertyData in propData)
            {
                sql += @" INSERT INTO " + TemplateTable + @"
                                VALUES(" +
                                        Utilities.SafeString(propertyData.TemplateGuid) + ", " +
                                        Utilities.SafeString(propertyData.TemplateName) + ", " +
                                        Utilities.SafeString(propertyData.TemplateFileName) + ", " +
                                        Utilities.SafeString(propertyData.Templatedescription) + ", " +
                                        Utilities.SafeString(propertyData.TemplateController) + ", " +
                                        Utilities.SafeString(propertyData.IsGroupTemplate.ToString()) + ", " +
                                        Utilities.SafeString(propertyData.IsProductTemplate.ToString()) + ");";

                if (propertyData.TemplatePropertyEntries != null)
                {
                    foreach (TemplatePropertyEntry data in propertyData.TemplatePropertyEntries)
                    {
                        sql += @"   INSERT INTO " + TemplatePropertyTable + @" (TemplateGuid, PropGuid, PropGroupGuid, PropHeader) 
                                    VALUES ( " +
                                            Utilities.SafeString(data.TemplateGuid) + ", " +
                                            Utilities.SafeString(data.PropGuid) + "," +
                                            Utilities.SafeString(data.PropGroupGuid) + ", " +
                                            Utilities.SafeString(data.Propheader) + ");";
                    }
                }
            }
            if (!string.IsNullOrWhiteSpace(sql))
            {
                Execute(sql); 
            }
        }

        public bool UpdateTemplates(List<TemplateQuerySet> oldData, List<TemplateQuerySet> newData)
        {
            Dictionary<string, string> guidMatrix = new Dictionary<string, string>();
            foreach (TemplateQuerySet data in newData)
            {
                TemplateQuerySet match = oldData.Find(p => p.TemplateController == data.TemplateController && p.TemplateFileName == data.TemplateFileName);
                if (match != null && match.TemplateGuid != data.TemplateGuid)
                {
                    if (!guidMatrix.ContainsKey(match.TemplateGuid))
                    {
                        guidMatrix.Add(match.TemplateGuid, data.TemplateGuid);
                    }
                }
            }

            // Now we have to look for the PropTransGuids of the changed Templates and collect them in an object
            List<string> sqlUpdateQueries = new List<string>();
            foreach (var kv in guidMatrix)
            {

                string sql = @" SELECT  pv.*, PropTransGuid, PropTransText 
                                FROM    PropVal pv 
                                        INNER JOIN PropTrans pt ON (pv.PropValGuid = pt.PropValGuid AND PropGuid = 'TEMPLATE' AND pt.PropTransText = '" + kv.Key + "')";
                var propTrans = GetResults<PropValPropTransQuerySet>(sql);

                if (propTrans == null || !propTrans.Any())
                {
                    continue;
                }

                // Prepare the PropTransGuid parameters
                var propTransGuidParameterBuilder = new StringBuilder();

                foreach (var propTran in propTrans)
                {
                    propTransGuidParameterBuilder.Append("('" + propTran.PropTransGuid + "'),");
                }
                if (propTransGuidParameterBuilder.Length > 0)
                {
                    propTransGuidParameterBuilder.Length = propTransGuidParameterBuilder.Length - 1;
                }

                string query = @"   UPDATE  PropTrans 
                                    SET     PropTransText = '" + kv.Value + "' " + @"
                                    FROM    PropTrans INNER JOIN
                                                                (
                                                                    SELECT  p.PropTransGuid 
                                                                    FROM    PropTrans 
                                                                            JOIN( VALUES " + propTransGuidParameterBuilder +
                               @" ) p (PropTransGuid) ON p.PropTransGuid = PropTrans.PropTransGuid
                                                                ) px ON px.PropTransGuid = PropTrans.PropTransGuid";

                sqlUpdateQueries.Add(query);
            }

            // Update PropTrans if there are any changes
            foreach (var sqlUpdateQuery in sqlUpdateQueries)
            {
                Execute(sqlUpdateQuery);
            }
            if (guidMatrix.Count > 0)
            {
                return true;
            }

            return false;
        }
    }
}