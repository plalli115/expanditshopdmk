﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.UI;
using CmsPublic.ModelLayer.Interfaces;
using CmsPublic.ModelLayer.Models;
using EISCS.CMS.Build;
using EISCS.CMS.Services.ResourceHandling;

namespace EISCS.CMS.CentralManagement.Helpers
{
    public static class UrlResourceHelper
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static string GetVirtualRoot()
        {
            if (HttpRuntime.AppDomainAppVirtualPath != null)
                return HttpRuntime.AppDomainAppVirtualPath.Equals("/") ? "" : HttpRuntime.AppDomainAppVirtualPath;
            return "";
        }
    }

    ///<summary>
    ///</summary>
    public static class CustomTreeRenderHtmlHelper
    {
        ///<summary>
        ///</summary>
        ///<param name="htmlHelper"></param>
        ///<param name="rootLocations"></param>
        ///<param name="locationRenderer"></param>
        ///<param name="counter"></param>
        ///<param name="name"></param>
        ///<typeparam name="T"></typeparam>
        ///<returns></returns>
        public static string RenderTree<T>(
            this HtmlHelper htmlHelper,
            IEnumerable<T> rootLocations,
            Func<T, Group> locationRenderer, int counter, string name)
            where T : IComposite<T>
        {
            return new TreeRenderer<T>(rootLocations, locationRenderer, htmlHelper, counter, name).Render();
        }
    }

    ///<summary>
    ///</summary>
    ///<typeparam name="T"></typeparam>
    public class TreeRenderer<T> where T : IComposite<T>
    {
        private readonly HtmlHelper _helper;
        private readonly Func<T, Group> _locationRenderer;
        private readonly IEnumerable<T> _rootLocations;

        private string[] _contractedItems;
        private int _counter;
        private const string ImageUrl = "EISCMS/Content/images/directory.png";
        private HtmlTextWriter _writer;
        private readonly string _name;

        ///<summary>
        ///</summary>
        ///<param name="rootLocations"></param>
        ///<param name="locationRenderer"></param>
        ///<param name="htmlHelper"></param>
        ///<param name="counter"></param>
        ///<param name="name"></param>
        public TreeRenderer(
            IEnumerable<T> rootLocations,
            Func<T, Group> locationRenderer, HtmlHelper htmlHelper, int counter, string name)
        {
            _rootLocations = rootLocations;
            _locationRenderer = locationRenderer;
            _helper = htmlHelper;
            _counter = counter;
            _name = name;
        }

        ///<summary>
        ///</summary>
        ///<returns></returns>
        public string Render()
        {
            HttpCookieCollection cookies = HttpContext.Current.Request.Cookies;
            if (cookies != null && cookies.Count > 0)
            {
                HttpCookie cookie = cookies["contractedItems"];
                if (cookie != null)
                {
                    string value = HttpContext.Current.Server.UrlDecode(cookie.Value);
                    if (value != null)
                    {
                        _contractedItems = value.Split(',');
                    }
                }
            }
            _writer = new HtmlTextWriter(new StringWriter());
            RenderLocations(_rootLocations);
            return _writer.InnerWriter.ToString();
        }

        /// <summary>
        /// Recursively walks the location tree outputting it as hierarchical UL/LI elements
        /// </summary>
        /// <param name="locations"></param>
        private void RenderLocations(IEnumerable<T> locations)
        {
            if (locations == null) return;
            if (!locations.Any()) return;

            InUl(() => locations.ForEach(location => InLi(location as Group, () =>
            {
                _writer.Write(
                    RenderTags(
                        _locationRenderer(location)));
                RenderLocations(location.Children);
            })));
        }

        private void InUl(Action action)
        {
            _writer.WriteLine();
            if (_counter++ == 0)
            {
                _writer.AddAttribute(HtmlTextWriterAttribute.Id, "sitemap_" + _name);
                _writer.AddAttribute(HtmlTextWriterAttribute.Class, "class1");
            }
            _writer.RenderBeginTag(HtmlTextWriterTag.Ul);
            action();
            _writer.RenderEndTag();
            _writer.WriteLine();
        }

        private void InLi(Group locations, Action action)
        {
            string masterGuid = locations.GetPropertyValue("MASTERGUID");
            _writer.AddAttribute(HtmlTextWriterAttribute.Id, "id_" + locations.GroupGuid + "_" + _name + "_" + masterGuid);
            // TEST   
            if (_contractedItems != null)
            {
                if (_contractedItems.Contains("id_" + locations.GroupGuid))
                {
                    if (locations.HasChildren() || locations.HadChildren)
                    {
                        _writer.AddAttribute(HtmlTextWriterAttribute.Class, "contracted");
                    }
                }
            }

            // END TEST
            _writer.RenderBeginTag(HtmlTextWriterTag.Li);
            // TEST            
            _writer.Write("<div class=\"sortablezone\"><img src=\"" + UrlResourceHelper.GetVirtualRoot() + "/" +
                          ImageUrl + "\" alt=\"move\" height=\"1\" width=\"1\"/></div>");
            if (locations.HasChildren())
            {
                _writer.Write("<span class=\"expand\"></span>");
            }
            if (locations.HadChildren)
            {
                _writer.Write("<span class=\"expand contract isparent\"></span>");
            }
            _writer.Write("<div class=\"dropzone\"></div>");
            // END TEST
            _writer.Write("<dl class=\"ui-droppable\">");
            _writer.Write("<dt>");
            action();
            _writer.RenderEndTag();
            _writer.WriteLine();
        }

        private string RenderTags(Group t)
        {
            var urlHelper = new UrlHelper(_helper.ViewContext.RequestContext, _helper.RouteCollection);
            var anchortagBuilder = new TagBuilder("a");
            anchortagBuilder.AddCssClass("grouptreelink");
            string nameString = null;
            try
            {
                if (t.Properties.PropDict.ContainsKey("NAME"))
                {
                    nameString = t.Properties.PropDict["NAME"];
                }
            }
            catch
            {
            }

            string name;
            if (string.IsNullOrEmpty(nameString))
            {
                string testVal = null;
                try
                {
                    var langGuid = (string)HttpContext.Current.Session["CurrentLanguage"];
                    testVal = ResourceManager.GetTranslatedValue("CATMAN_LABEL_UNKNOWN_PROPERTY", langGuid);
                }
                catch
                {
                }
                name = string.IsNullOrEmpty(testVal) ? CustomResourceHandler.CATMAN_LABEL_UNKNOWN_PROPERTY : testVal;
            }
            else
            {
                name = nameString;
            }

            anchortagBuilder.SetInnerText(name); // Sets the visible link text

            anchortagBuilder.MergeAttributes(new RouteValueDictionary());
            object routeValues = new { id = t.GroupGuid };

            //anchortagBuilder.MergeAttribute("href", urlHelper.Action("Index", "CMS", routeValues));
            anchortagBuilder.MergeAttribute("href", "#");

            var sb = new StringBuilder();
            sb.AppendLine(anchortagBuilder.ToString(TagRenderMode.Normal));
            sb.AppendLine("</dt>");
            sb.AppendLine("<dd>");
            sb.AppendLine("</dd>");
            sb.AppendLine("</dl>");
            return sb.ToString();
        }
    }

    ///<summary>
    ///</summary>
    ///<param name="value"></param>
    ///<typeparam name="T"></typeparam>
    public delegate void Func<T>(T value);

    ///<summary>
    ///</summary>
    public static class Extension
    {
        ///<summary>
        ///</summary>
        ///<param name="values"></param>
        ///<param name="function"></param>
        ///<typeparam name="T"></typeparam>
        public static void ForEach<T>(this IEnumerable<T> values, Func<T> function)
        {
            foreach (T value in values)
            {
                function(value);
            }
        }
    }
}