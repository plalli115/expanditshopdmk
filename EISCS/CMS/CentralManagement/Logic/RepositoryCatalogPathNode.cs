﻿using EISCS.Shop.DO.Interface;

namespace EISCS.CMS.CentralManagement.Logic
{
    public class RepositoryCatalogPathNode
    {
        public IGroupRepository Repository { get; set; }
        public string CatalogPath { get; set; }
        public string Description { get; set; }
        public string BaseUrl { get; set; }
    }
}