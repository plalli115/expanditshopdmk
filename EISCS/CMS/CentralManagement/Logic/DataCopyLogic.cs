﻿using System;
using System.Collections.Generic;
using System.IO;
using CmsPublic.ModelLayer.Interfaces;
using CmsPublic.ModelLayer.Models;
using EISCS.Catalog;
using EISCS.Shop.DO.Interface;

namespace EISCS.CMS.CentralManagement.Logic
{
    public class DataCopyLogic
    {
        private readonly List<RepositoryCatalogPathNode> _clients;
        private readonly RepositoryCatalogPathNode _master;

        public DataCopyLogic(RepositoryCatalogPathNode master, List<RepositoryCatalogPathNode> clients)
        {
            _master = master;
            _clients = clients;
        }

        #region From Controller

        public string CopyGroup(string groupGuid, string parentGuid, string siblingGuid, string from, string to,
                                string languageGuid)
        {
            int iGroupGuid = ParseInt(groupGuid);
            int iParentGuid = ParseInt(parentGuid);
            int iFromIndex = ParseInt(from);
            int iToIndex = ParseInt(to);
            int iSiblingGuid = ParseInt(siblingGuid);

            if (iFromIndex > 0 && iToIndex <= 0)
            {
                return "";
            }

            string fromPath = _master.CatalogPath;
            string toPath = "";

            IGroupRepository clientRepository;

            if (iToIndex > 0)
            {
                clientRepository = _clients[iToIndex - 1].Repository;
                toPath = _clients[iToIndex - 1].CatalogPath;
            }
            else
            {
                clientRepository = _master.Repository;
            }

            if (iFromIndex == iToIndex)
            {
                // This is a move action inside the same tree
                clientRepository.RearrangeGroupOrder(iGroupGuid, iParentGuid, iSiblingGuid);

                return "Same DB";
            }

            // This is a copy between master and client
            // Principally just insert values into GroupTable and PropVal/PropTrans

            //Group toBeCopied = _masterGroupRepository.GetGroup(iGroupGuid, languageGuid);
            Group toBeCopied = _master.Repository.GetGroup(iGroupGuid, languageGuid);

            // Scan client for MasterGuid
            // If MasterGuid is found check if Master GroupGuid = Client GroupGuid
            // If they are the same just use the Master Group as it is
            Group clientGroups = new CatalogTreeUtility(clientRepository).GetFilteredGroupTree(languageGuid, null, false,
                                                                                               0);

            string masterGuidValue = toBeCopied.GetPropertyValue("MASTERGUID");
            // If no MasterGuid then create it... must have it
            if (string.IsNullOrEmpty(masterGuidValue))
            {
                GrpPropValSet propVal = toBeCopied.Properties;

                if (propVal.PropDict != null)
                {
                    masterGuidValue = "Master_" + Guid.NewGuid();
                    propVal.PropDict["MASTERGUID"] = masterGuidValue;
                    propVal.PropDict["_MASTERGUID"] = masterGuidValue;
                }
                _master.Repository.SaveProperties(toBeCopied, languageGuid);
            }

            Group clientGroup = FindByProperty(clientGroups, "MASTERGUID", masterGuidValue);

            if (clientGroup != null && toBeCopied.ID != clientGroup.ID)
            {
                GrpPropValSet propVal = toBeCopied.Properties;
                IPropValSet iValset = toBeCopied.IProperties;

                clientGroup.Properties = propVal;
                clientGroup.IProperties = iValset;

                toBeCopied = clientGroup;
                clientRepository.SaveProperties(toBeCopied, languageGuid);

                CopyFolder(fromPath, toPath);

                return "MasterGuid exist but groupGuid is different";
            }

            if (clientGroup != null && toBeCopied.ID == clientGroup.ID)
            {
                clientRepository.SaveProperties(toBeCopied, languageGuid);
                CopyFolder(fromPath, toPath);
                return "MasterGuid exist and groupGuids are the same";
            }

            // MasterGuid was not found - Must still check for presence of Guid
            Group idGroup = clientGroups.Find(clientGroups, toBeCopied.ID);
            if (idGroup == null)
            {
                // Must find parent and sibling guid
                clientRepository.InsertGroup(iGroupGuid, iParentGuid, iSiblingGuid);
                clientRepository.SaveProperties(toBeCopied, languageGuid);
                CopyFolder(fromPath, toPath);
                return "New group inserted with original groupGuid";
            }

            // MasterGuid was not found but GroupGuid is already used in client
            // Get highest GroupGuid and add one to that number
            int hValue = GetHighestValue(clientGroups);
            int newGroupGuid = hValue + 1;
            toBeCopied.GroupGuid = newGroupGuid;
            // Have to set new PropOwnerRefGuid
            GrpPropValSet propValSet = toBeCopied.Properties;
            propValSet.parentGuid = newGroupGuid.ToString();

            clientRepository.InsertGroup(newGroupGuid, iParentGuid, iSiblingGuid);
            clientRepository.SaveProperties(toBeCopied, languageGuid);
            CopyFolder(fromPath, toPath);
            return "NewGuid," + newGroupGuid;
        }

        public int ParseInt(string value)
        {
            int iValue;
            int.TryParse(value, out iValue);
            return iValue;
        }

        #endregion

        // Internal Moving
        public void MoveDataInsideGroup(IGroupRepository groupRepository)
        {
            groupRepository.RearrangeGroupOrder(0, 0, 0);
        }

        public void MoveDataOutsideGroup(IGroupRepository groupRepository)
        {
            groupRepository.RegroupGroupTable(0, 0);
        }

        public Group GetGroupTree(IGroupRepository groupRepository, string languageGuid, int rootGuid)
        {
            var catalogTreeUtility = new CatalogTreeUtility(null);
            return catalogTreeUtility.GetFilteredGroupTree(languageGuid, null, false, rootGuid);
        }

        // Find the group wich has a specific value of a specific property
        public Group FindByProperty(Group group, string propertyName, string propertyText)
        {
            Group match = null;
            FindByProperty(group, propertyName, propertyText, ref match);
            return match;
        }

        private bool FindByProperty(Group group, string propertyName, string propertyText, ref Group match)
        {
            bool found = false;
            foreach (Group child in group.Children)
            {
                string text = child.GetPropertyValue(propertyName);
                if (text == propertyText)
                {
                    match = child;
                    found = true;
                }
                if (child.HasChildren() && match == null)
                {
                    found = FindByProperty(child, propertyName, propertyText, ref match);
                    if (found)
                    {
                        return true;
                    }
                }
            }
            return found;
        }

        public int GetHighestValue(Group group)
        {
            var ints = new Stack<int>();
            ints.Push(0);
            GetHighestValue(group, ints);
            return ints.Peek();
        }

        public void GetHighestValue(Group group, Stack<int> groups)
        {
            foreach (Group child in group.Children)
            {
                if (child.ID > groups.Peek())
                {
                    groups.Push(child.ID);
                }
                if (child.HasChildren())
                {
                    GetHighestValue(child, groups);
                }
            }
        }

        // Copy folder to another folder recursively
        public void CopyFolder(string sourceFolder, string destFolder)
        {
            if (string.IsNullOrEmpty(destFolder))
            {
                return;
            }
            if (!Directory.Exists(destFolder))
            {
                try
                {
                    Directory.CreateDirectory(destFolder);
                }
                catch (UnauthorizedAccessException)
                {
                    return;
                }
            }
            string[] files = Directory.GetFiles(sourceFolder);
            foreach (string file in files)
            {
                string name = Path.GetFileName(file);
                if (name == null) continue;
                string dest = Path.Combine(destFolder, name);
                File.Copy(file, dest, true);
            }
            string[] folders = Directory.GetDirectories(sourceFolder);
            foreach (string folder in folders)
            {
                string name = Path.GetFileName(folder);
                if (name == null) continue;
                string dest = Path.Combine(destFolder, name);
                CopyFolder(folder, dest);
            }
        }
    }
}