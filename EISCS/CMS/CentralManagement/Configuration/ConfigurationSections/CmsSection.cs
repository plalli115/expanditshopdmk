﻿using System.Configuration;

namespace EISCS.CMS.CentralManagement.Configuration.ConfigurationSections
{
    public class CmsSection : ConfigurationSection
    {
        [ConfigurationProperty("IsMasterCms", DefaultValue = false, IsRequired = false)]
        public bool IsMasterCms
        {
            get { return (bool) this["IsMasterCms"]; }
            set { this["IsMasterCms"] = value; }
        }
    }
}