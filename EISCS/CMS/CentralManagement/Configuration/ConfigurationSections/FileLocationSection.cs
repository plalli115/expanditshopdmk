﻿using System.Configuration;

namespace EISCS.CMS.CentralManagement.Configuration.ConfigurationSections
{
    public class FileLocationSection : ConfigurationSection
    {
        [ConfigurationProperty("UseSharedFolder", DefaultValue = false, IsRequired = false)]
        public bool UseSharedFolder
        {
            get { return (bool) this["UseSharedFolder"]; }
            set { this["UseSharedFolder"] = value; }
        }

        [ConfigurationProperty("CatalogFolder", DefaultValue = "catalog/images", IsRequired = false)]
        public string CatalogFolder
        {
            get { return (string) this["CatalogFolder"]; }
            set { this["CatalogFolder"] = value; }
        }

        [ConfigurationProperty("VirtualDirectory", DefaultValue = "", IsRequired = false)]
        public string VirtualDirectory
        {
            get { return (string) this["VirtualDirectory"]; }
            set { this["VirtualDirectory"] = value; }
        }
    }
}