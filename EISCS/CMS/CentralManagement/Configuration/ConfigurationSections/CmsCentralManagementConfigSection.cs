using System.Configuration;

namespace EISCS.CMS.CentralManagement.Configuration.ConfigurationSections
{
    internal class CmsCentralManagementConfigSection : ConfigurationSection
    {
        [ConfigurationProperty("CmsCatalogPaths", IsDefaultCollection = false),
         ConfigurationCollection(typeof (CmsCatalogPathCollection), AddItemName = "addCatalogPath",
             ClearItemsName = "clearCatalogPath", RemoveItemName = "removeCatalogPath")]
        public CmsCatalogPathCollection CatalogPaths
        {
            get { return this["CmsCatalogPaths"] as CmsCatalogPathCollection; }
        }
    }
}