﻿using System.Configuration;

namespace EISCS.CMS.CentralManagement.Configuration
{
    public class CmsCatalogPathCollection : ConfigurationElementCollection
    {
        public override ConfigurationElementCollectionType CollectionType
        {
            get { return ConfigurationElementCollectionType.AddRemoveClearMap; }
        }

        public CmsCatalogPathElement this[int index]
        {
            get { return (CmsCatalogPathElement) BaseGet(index); }
            set
            {
                if (BaseGet(index) != null)
                    BaseRemoveAt(index);
                BaseAdd(index, value);
            }
        }

        public void Add(CmsCatalogPathElement element)
        {
            BaseAdd(element);
        }

        public void Clear()
        {
            BaseClear();
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new CmsCatalogPathElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((CmsCatalogPathElement) element).ConnectionStringName;
        }

        public void Remove(CmsCatalogPathElement element)
        {
            BaseRemove(element.ConnectionStringName);
        }

        public void Remove(string name)
        {
            BaseRemove(name);
        }

        public void RemoveAt(int index)
        {
            BaseRemoveAt(index);
        }
    }

    public class CmsCatalogPathElement : ConfigurationElement
    {
        public CmsCatalogPathElement()
        {
        }

        public CmsCatalogPathElement(string title, string description)
        {
            ConnectionStringName = title;
            Description = description;
        }

        [ConfigurationProperty("Description", IsRequired = true, DefaultValue = "No description.")]
        public string Description
        {
            get { return (string) this["Description"]; }
            set { this["Description"] = value; }
        }

        [ConfigurationProperty("BaseUrl", IsRequired = true, DefaultValue = "")]
        public string BaseUrl
        {
            get { return (string)this["BaseUrl"]; }
            set { this["BaseUrl"] = value; }
        }

        [ConfigurationProperty("ConnectionStringName", IsRequired = true, DefaultValue = "Untitled")]
        public string ConnectionStringName
        {
            get { return (string) this["ConnectionStringName"]; }
            set { this["ConnectionStringName"] = value; }
        }

        [ConfigurationProperty("CatalogPath", IsRequired = false, DefaultValue = "")]
        public string CatalogPath
        {
            get { return (string) this["CatalogPath"]; }
            set { this["CatalogPath"] = value; }
        }
    }
}