﻿using System;
using System.Configuration;
using EISCS.CMS.CentralManagement.Configuration.ConfigurationSections;

namespace EISCS.CMS.CentralManagement.Configuration.Util
{
    public class ConfigUtil
    {
        public static string OptionalVirtualDirectory()
        {
            FileLocationSection config;
            try
            {
                config =
                    (FileLocationSection) ConfigurationManager.GetSection("FolderOptionsGroup/FileLocationSection");
            }
            catch (Exception)
            {
                return null;
            }

            if (config == null)
            {
                return null;
            }
            if (config.UseSharedFolder && !string.IsNullOrEmpty(config.VirtualDirectory))
            {
                return config.VirtualDirectory;
            }
            return null;
        }

        public static bool IsMasterCms()
        {
            CmsSection config;
            try
            {
                config =
                    (CmsSection) ConfigurationManager.GetSection("CmsOptionsGroup/CmsSection");
            }
            catch (Exception)
            {
                return false;
            }

            return config != null && config.IsMasterCms;
        }
    }
}