﻿using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using EISCS.CMS.CentralManagement.Configuration;
using EISCS.CMS.CentralManagement.Configuration.ConfigurationSections;

namespace EISCS.CMS.CentralManagement.Data
{
    public class ConfigSettings
    {
        private readonly string _sectionName;

        public ConfigSettings()
        {
            _sectionName = "CmsCentralManagement";
        }

        public ConfigSettings(string sectionName)
        {
            _sectionName = sectionName;
        }

        public CmsCatalogPathElement GetSectionElements(string connectionName)
        {
            CmsCentralManagementConfigSection section = GetSection(_sectionName);
            CmsCatalogPathElement[] collections = SectionToArray(section);
            if (collections == null)
            {
                return null;
            }
            IEnumerable<CmsCatalogPathElement> temp = collections.Where(x => x.ConnectionStringName == connectionName);
            return temp.FirstOrDefault();
        }

        public Dictionary<string, string> TestAlgoritm(string mainConnectionName, List<string> clientConnectionNames)
        {
            CmsCentralManagementConfigSection section = GetSection(_sectionName);
            CmsCatalogPathElement[] collections = SectionToArray(section);

            if (string.IsNullOrEmpty(mainConnectionName))
            {
                return null;
            }

            CmsCatalogPathElement element = GetElementByName(collections, mainConnectionName);

            if (element == null)
            {
                return null;
            }

            var pathConnectionDict = new Dictionary<string, string> { { mainConnectionName, element.CatalogPath } };

            foreach (string clientConnectionName in clientConnectionNames)
            {
                if (string.IsNullOrEmpty(clientConnectionName)) continue;
                CmsCatalogPathElement cmsElement = GetElementByName(collections, clientConnectionName);
                if (cmsElement == null) continue;
                pathConnectionDict.Add(clientConnectionName, cmsElement.CatalogPath);
            }

            return pathConnectionDict;
        }

        private CmsCentralManagementConfigSection GetSection(string sectionName)
        {
            return (CmsCentralManagementConfigSection)ConfigurationManager.GetSection(sectionName);
        }

        private CmsCatalogPathElement[] SectionToArray(CmsCentralManagementConfigSection section)
        {
            if (section == null)
            {
                return null;
            }
            int sections = section.CatalogPaths.Count;
            var collections = new CmsCatalogPathElement[sections];
            section.CatalogPaths.CopyTo(collections, 0);
            return collections;
        }

        private CmsCatalogPathElement GetElementByName(IEnumerable<CmsCatalogPathElement> collections, string title)
        {
            if (collections == null)
            {
                return null;
            }
            List<CmsCatalogPathElement> elements = collections.Where(x => x.ConnectionStringName == title).ToList();
            return elements.Any() ? elements[0] : null;
        }
    }
}