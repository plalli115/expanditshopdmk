﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace EISCS.CMS.CentralManagement.Data
{

    public class ConnectionStrings
    {

        public ConnectionStringSettings MainConnection { get; private set; }
        public List<ConnectionStringSettings> ClientConnections { get; private set; }
        public string MainConnectionstringName { get; private set; }
        public List<string> ClientConnectionstringNames { get; private set; }

        public ConnectionStrings(string mainConnectionStringName)
        {
            try
            {
                MainConnection = ConfigurationManager.ConnectionStrings.Cast<ConnectionStringSettings>().Where(
                        x => x.Name == mainConnectionStringName).ToList()[0];
            }
            catch (Exception)
            {
                throw new Exception("No Connectionstring with the name " + mainConnectionStringName + " was found");
            }
            try
            {
                ClientConnections = CollectDataConnections(mainConnectionStringName);
            }
            catch (Exception)
            {
                throw new Exception("Error reading ClientConnectionstrings");
            }

            MainConnectionstringName = MainConnection.Name;
            ClientConnectionstringNames = new List<string>();
            foreach (ConnectionStringSettings clientConnection in ClientConnections)
            {
                ClientConnectionstringNames.Add(clientConnection.Name);
            }
        }

        private static List<ConnectionStringSettings> CollectDataConnections(string mainConnectionStringName)
        {
            return ConfigurationManager.ConnectionStrings.Cast<ConnectionStringSettings>().Where(x => x.Name != mainConnectionStringName && x.Name != "LocalSqlServer" && x.Name.Contains("Client")).ToList();
        }
    }

    
    public class FieldMapping
    {
        public string MasterField { get; set; }
        public List<FieldMapping> ClientField { get; set; }
    }
}
