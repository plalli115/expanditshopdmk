﻿using CmsPublic.ModelLayer.Models;

namespace EISCS.CMS.CentralManagement.Models.ViewModels
{
    public class ManagementViewModel
    {
        public ManagementViewModel(Group compositeGroup, int index, string description)
        {
            CompositeGroup = compositeGroup;
            Index = index;
            Description = description;
        }

        public Group CompositeGroup { get; private set; }
        public int Index { get; private set; }
        public string Description { get; private set; }
        public string ErrorMessage { get; set; }
    }
}