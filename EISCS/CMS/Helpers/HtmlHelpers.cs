﻿using System;
using System.Text;
using System.Web.Mvc;
using EISCS.CMS.Services.AppSettings;
using EISCS.CMS.Services.PagingService;

namespace EISCS.CMS.Helpers
{
    ///<summary>
    ///</summary>
    public static class HtmlHelpers
    {
        ///<summary>
        ///</summary>
        ///<param name="helper"></param>
        ///<param name="modeKey"></param>
        ///<param name="defaultValue"></param>
        ///<returns></returns>
        public static string PollInterval(this HtmlHelper helper, string modeKey, string defaultValue)
        {
            return InternalPollInterval(modeKey, defaultValue);
        }

        ///<summary>
        ///</summary>
        ///<param name="modeKey"></param>
        ///<param name="defaultValue"></param>
        ///<returns></returns>
        public static string InternalPollInterval(string modeKey, string defaultValue)
        {
            string pollInterval = defaultValue;
            try
            {
                string appSettingsValue = AppSettingsReader.Read(modeKey);
                int intValue;
                if (int.TryParse(appSettingsValue, out intValue))
                {
                    pollInterval = intValue <= 0 ? defaultValue : intValue.ToString();
                }
            }
            catch (Exception)
            {
                pollInterval = defaultValue;
            }
            return pollInterval;
        }

        ///<summary>
        ///</summary>
        ///<param name="helper"></param>
        ///<param name="pager"></param>
        ///<param name="urlPrefix"></param>
        ///<param name="pagerClassName"></param>
        ///<param name="pagerCurrentClassName"></param>
        ///<returns></returns>
        public static string Pager(this HtmlHelper helper, Pager pager, string urlPrefix, string pagerClassName,
                                   string pagerCurrentClassName)
        {
            var sb = new StringBuilder();
            const int showIdeal = 5;
            const int showStart = (showIdeal - 1)/2;
            int x = showIdeal - pager.CurrentPage;
            int y = (pager.TotalPages - pager.CurrentPage) + (pager.CurrentPage - showIdeal);

            int start = pager.CurrentPage - showStart <= 0
                            ? 1
                            : (pager.TotalPages - showStart < pager.CurrentPage ? y + 1 : pager.CurrentPage - showStart);
            if (start <= 0)
            {
                start = 1;
            }
            int maxIterations = pager.TotalPages - showIdeal < 0
                                    ? pager.TotalPages
                                    : (pager.CurrentPage + showStart > pager.TotalPages
                                           ? pager.TotalPages
                                           : (pager.CurrentPage + showStart < showIdeal
                                                  ? pager.CurrentPage + x
                                                  : pager.CurrentPage + showStart));

            if (pager.TotalPages > 1)
            {
                sb.AppendLine(String.Format("<a class=\"{2}\" href=\"{0}{1}\">[...]</a>", urlPrefix, "1", pagerClassName));
                sb.AppendLine(pager.PreviousPage >= 1
                                  ? String.Format("<a class=\"{2}\" href=\"{0}{1}\">&#171;</a>", urlPrefix,
                                                  pager.PreviousPage, pagerClassName)
                                  : String.Format("<a class=\"{2}\" href=\"{0}{1}\">&#171;</a>", urlPrefix,
                                                  pager.CurrentPage, pagerClassName));
                while (start <= maxIterations)
                {
                    sb.AppendLine(start == pager.CurrentPage
                                      ? String.Format("<a class=\"{2}\" href=\"{0}{1}\">{1}</a>", urlPrefix,
                                                      pager.CurrentPage, pagerCurrentClassName)
                                      : String.Format("<a class=\"{2}\" href=\"{0}{1}\">{1}</a>", urlPrefix, start,
                                                      pagerClassName));
                    start++;
                }
                sb.AppendLine(!(pager.NextPage > pager.TotalPages)
                                  ? String.Format("<a class=\"{2}\" href=\"{0}{1}\">&#187;</a>", urlPrefix,
                                                  pager.NextPage, pagerClassName)
                                  : String.Format("<a class=\"{2}\" href=\"{0}{1}\">&#187;</a>", urlPrefix,
                                                  pager.CurrentPage, pagerClassName));
                sb.AppendLine(String.Format("<a class=\"{2}\" href=\"{0}{1}\">[...]</a>", urlPrefix, pager.TotalPages,
                                            pagerClassName));
            }
            else
            {
                sb.AppendLine(String.Format("<a class=\"{2}\" style=\"visibility:hidden\" href=\"{0}{1}\">{1}</a>",
                                            urlPrefix, pager.CurrentPage, pagerCurrentClassName));
            }

            return sb.ToString();
        }
    }
}