﻿namespace EISCS.CMS.Services.PagingService
{
    ///<summary>
    ///</summary>
    public class Pager
    {
        ///<summary>
        ///</summary>
        ///<param name="currentPage"></param>
        ///<param name="totalRecords"></param>
        ///<param name="pageRecords"></param>
        public Pager(int currentPage, int totalRecords, int pageRecords)
        {
            CurrentPage = currentPage;

            TotalPages = (totalRecords + pageRecords - 1)/pageRecords;

            NextPage = currentPage + 1;
            PreviousPage = currentPage - 1;
        }

        ///<summary>
        ///</summary>
        public int CurrentPage { get; private set; }

        ///<summary>
        ///</summary>
        public int NextPage { get; private set; }

        ///<summary>
        ///</summary>
        public int PreviousPage { get; private set; }

        ///<summary>
        ///</summary>
        public int TotalPages { get; private set; }
    }
}