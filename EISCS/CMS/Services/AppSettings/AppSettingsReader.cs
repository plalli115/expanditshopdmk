﻿using System.Web.Configuration;

namespace EISCS.CMS.Services.AppSettings
{
    /// <summary>
    /// AppSettings Reader
    /// </summary>
    public class AppSettingsReader
    {
        /// <summary>
        /// Read an AppSetting entry
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string Read(string key)
        {
            return WebConfigurationManager.AppSettings.Get(key);
        }
    }
}