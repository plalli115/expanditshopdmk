﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using CmsPublic.DataProviders.Logging;
using CmsPublic.ModelLayer.Models;
using EISCS.CMS.Services.FileService;
using EISCS.Routing.Providers;
using EISCS.Shop.DO.Interface;

namespace EISCS.CMS.Services.PropertyServices
{

 

    public class PropertyManager
    {
        // Steps:
        // 1. Scan Template Folder for templates
        // 2. Read each file, find the Properties definition.
        // 3. Put the Properties in a collection.
        // 4. Check the database, write Properties to DB

        private readonly IPropertiesDataService _propertiesDataService;

        private readonly string[] _propStrings =
            {
                "#NAME", "#DESCRIPTION", "#GROUPTEMPLATE", "#PRODUCTTEMPLATE",
                "#PROPERTYGROUP"
            };

        private List<TemplateQuerySet> _oldPropertyData;

        public PropertyManager()
        {
            _propertiesDataService = DependencyResolver.Current.GetService<IPropertiesDataService>();
        }

        public PropertyManager(IPropertiesDataService propertiesDataService)
        {
            _propertiesDataService = propertiesDataService;
        }

        public List<TemplateQuerySet> ScanFiles(string folderPath)
        {
            var propData = new List<TemplateQuerySet>();
            var fileHandler = new FileHandler();
            List<string> fileNames = fileHandler.ListFullFileNames(folderPath, "*.cs"); // Controllers/Templates

            const string viewPath = "Views";

            // For each controller - get the path to the corresponding view folder -- Views/Products etc.
            List<string> viewFolders = fileNames.Select(fileName => viewPath + "/" + fileName.Split('\\').LastOrDefault()).ToList();

            int iteration = 0;

            foreach (string str in viewFolders)
            {
                string folder = str.Replace("Controller.cs", "");
                string controller = folder.Replace(viewPath + "/", "");
                fileNames = fileHandler.ListFullFileNames(folder, "*.cshtml");


                int propGroupIteration = 0;
                foreach (string item in fileNames)
                {
                    iteration += 1;
                    var propertyData = new TemplateQuerySet();
                    IEnumerable<string> data = ReadFile(item, _propStrings, "-->");
                    if (!data.Any())
                    {
                        continue;
                    }
                    foreach (string s in data)
                    {
                        string[] parts = s.Split('=');
                        if (parts.Length > 1)
                        {
                            string value = parts[1].Trim();
                            if (s.Contains("#NAME"))
                            {
                                propertyData.TemplateName = value;
                            }
                            if (s.Contains("#DESCRIPTION"))
                            {
                                propertyData.Templatedescription = value;
                            }
                            if (s.Contains("#GROUPTEMPLATE"))
                            {
                                propertyData.IsGroupTemplate = (value == "TRUE");
                            }
                            if (s.Contains("#PRODUCTTEMPLATE"))
                            {
                                propertyData.IsProductTemplate = (value == "TRUE");
                            }
                            if (s.Contains("#PROPERTYGROUP"))
                            {
                                propGroupIteration += 1;
                                try
                                {
                                    string[] first = value.Split(new[] { '|' }, StringSplitOptions.None);
                                    string propString = first[0];
                                    string headerString = null;
                                    if (first.Length > 1)
                                    {
                                        headerString = first[1];
                                    }
                                    string[] props = propString.Split(',');
                                    foreach (string prop in props)
                                    {
                                        var templatePropertyEntry = new TemplatePropertyEntry();
                                        templatePropertyEntry.TemplateGuid = (iteration).ToString(); // Add TemplateGuid
                                        templatePropertyEntry.PropGuid = prop.Trim(); // Add PropGuid
                                        templatePropertyEntry.PropGroupGuid = propGroupIteration.ToString();
                                        // Add Property Grouping information
                                        if (!string.IsNullOrEmpty(headerString))
                                        {
                                            templatePropertyEntry.Propheader = headerString;
                                            // Add header information if availible
                                        }
                                        propertyData.TemplatePropertyEntries.Add(templatePropertyEntry);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    FileLogger.Log(ex.Message + " " + ex.StackTrace);
                                }
                            }
                        }
                    }
                    propertyData.TemplateGuid = (iteration).ToString();
                    string[] pathChunks = item.Split('\\');
                    propertyData.TemplateFileName = pathChunks[pathChunks.Length - 1].Replace(".cshtml", "");
                    propertyData.TemplateController = controller;
                    propData.Add(propertyData);
                }
            }
            return propData;
        }

        private IEnumerable<string> ReadFile(string fullPath, string[] conditions, string endCondition)
        {
            var contents = new List<string>();
            string content;
            try
            {
                using (var sr = new StreamReader(File.OpenRead(fullPath)))
                {
                    while ((content = sr.ReadLine()) != null)
                    {
                        foreach (string s in conditions)
                        {
                            if (content.Contains(s))
                            {
                                contents.Add(content);
                            }
                        }
                        if (content.Contains(endCondition))
                        {
                            break;
                        }
                    }
                }
            }
            catch (Exception)
            {
                content = "Error reading content, could not open file. path = " + fullPath;
                FileLogger.Log(content);
            }
            return contents;
        }

     
        /// <summary>
        /// </summary>
        public void Initialize()
        {
            _oldPropertyData = _propertiesDataService.GetAllTemplateInfo();
            _propertiesDataService.InitTemplateTables();
        }

        /// <summary>
        /// </summary>
        /// <param name="propData"></param>
        public void PersistData(List<TemplateQuerySet> propData)
        {
            // 1. Write to TemplateTable
            // 2. Write to TemplateProperty
            _propertiesDataService.InsertTemplates(propData);
            if (_oldPropertyData != null && _oldPropertyData.Count > 0)
            {
                List<TemplateQuerySet> newPropertyData = _propertiesDataService.GetAllTemplateInfo();
                var changed = _propertiesDataService.UpdateTemplates(_oldPropertyData, newPropertyData);
                if (changed)
                {
                    RouteManager.Route.PublicRouteDataProvider.RecreateAll();
                }
            }
        }


    }
}