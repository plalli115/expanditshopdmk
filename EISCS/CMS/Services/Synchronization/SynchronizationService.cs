﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Caching;
using System.Web.Hosting;
using System.Web.Mvc;
using EISCS.CMS.Configuration;
using EISCS.CMS.ControllerCommon;
using EISCS.CMS.Services.FileService;

namespace EISCS.CMS.Services.Synchronization
{
    public interface ISynchronizationService
    {
        string Poll();
    }

    public class SynchronizationService : ISynchronizationService
    {
        private readonly HttpContextBase _httpContext;
        private readonly SelectListStore _selectListStore = new SelectListStore(); // DI?
        private readonly SyncTask _syncTask; // DI?
        private readonly string _plainSyncFolderPath;
        private readonly string _escapedSyncFolderPath;

        public SynchronizationService(HttpContextBase httpContext)
        {
            _httpContext = httpContext;
            _syncTask = new SyncTask(httpContext); // DI?
            _plainSyncFolderPath = CmsSyncTaskConfigSection.Section.SyncFolder;
            _escapedSyncFolderPath = string.Format("/{0}/", _plainSyncFolderPath);
        }

        public string Poll()
        {
            var fh = new FileHandler();
            List<string> jobFiles = fh.ListAllFileNames("/" + _plainSyncFolderPath, "*.*");
            int files = jobFiles.Count;
            string jobFileName = "_semaphore_.txt";
            if (files > 0)
            {
                try
                {
                    jobFileName = jobFiles.First(f => !f.Contains(".txt"));
                }
                catch (Exception)
                {
                    if (!fh.IsFileExist(_escapedSyncFolderPath + "_semaphore_.txt"))
                    {
                        string errMess;
                        fh.CreateFile("/" + _plainSyncFolderPath, "_semaphore_.txt", "This file is used by the Sync Job", out errMess);
                    }
                }
            }
            // Add a "job" to the cache if it's empty
            if (_httpContext.Cache["CurrentJob"] == null)
            {
                _httpContext.Cache.Insert("CurrentJob", new SyncStatusNode(jobFileName, "-123", "-1"),
                                         new CacheDependency(HostingEnvironment.MapPath("~" + _escapedSyncFolderPath + jobFileName)));
            }
            else
            {
                var ssn = (SyncStatusNode)_httpContext.Cache["CurrentJob"];
                if (ssn.ActionFileName.Equals("_semaphore_.txt") && !jobFileName.Equals("_semaphore_.txt"))
                {
                    if (fh.IsFileExist(_escapedSyncFolderPath + "_semaphore_.txt"))
                    {
                        fh.DeleteFile(_escapedSyncFolderPath + "_semaphore_.txt");
                    }
                    _httpContext.Cache.Remove("CurrentJob");
                    _httpContext.Cache.Insert("CurrentJob", new SyncStatusNode(jobFileName, "-123", "-1"),
                                             new CacheDependency(HostingEnvironment.MapPath("~" + _escapedSyncFolderPath + jobFileName)));
                }
            }

            object cachedObject = null;
            string jobId = "-123";

            //TEST
            string jobCookieItem = null;
            HttpCookieCollection cookies = _httpContext.Request.Cookies;
            if (cookies != null && cookies.Count > 0)
            {
                HttpCookie cookie = cookies["jobId"];
                if (cookie != null)
                {
                    string value = _httpContext.Server.UrlDecode(cookie.Value);

                    // We have a cookie with jobId
                    jobCookieItem = value;

                    // check the cache for this jobId
                    if (jobCookieItem != null) cachedObject = _httpContext.Cache[jobCookieItem];
                    if (cachedObject != null)
                    {
                        jobId = ((SyncStatusNode)cachedObject).ActionGuid;
                    }
                }
            }

            // No cookie or no cache - either non starter or old information
            if (string.IsNullOrEmpty(jobCookieItem) || cachedObject == null)
            {
                // is there a job in the cache?
                jobId = ((SyncStatusNode)_httpContext.Cache["CurrentJob"]).ActionGuid;
                _httpContext.Response.Cookies.Add(new HttpCookie("jobId", jobId));
            }
            //END TEST           

            SelectList sl = _selectListStore.GetJobSelectList("");
            return _syncTask.Poll(sl, jobId);
        }
    }
}
