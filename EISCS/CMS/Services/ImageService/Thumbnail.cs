﻿namespace EISCMS.Services.ImageService
{
    ///<summary>
    ///</summary>
    public class Thumbnail
    {
        ///<summary>
        ///</summary>
        ///<param name="id"></param>
        ///<param name="data"></param>
        public Thumbnail(string id, byte[] data)
        {
            FileId = id;
            Data = data;
        }

        ///<summary>
        ///</summary>
        public string FileId { get; set; }

        ///<summary>
        ///</summary>
        public byte[] Data { get; set; }
    }
}