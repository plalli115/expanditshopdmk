﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;

namespace EISCS.CMS.Services.ImageService
{
    /// <summary>
    /// Handles Images
    /// </summary>
    public class ImageHandler
    {
        /// <summary>
        /// Get Image Stream from Resource file
        /// </summary>
        /// <param name="pName"></param>
        /// <returns></returns>
        private static Stream ImageData(string pName)
        {
            IEnumerable<string> resources =
                typeof (ImageHandler).Assembly.GetManifestResourceNames().Where(name => name.EndsWith(pName));
            string res = resources.First();
            Stream resStream = typeof (ImageHandler).Assembly.GetManifestResourceStream(res);
            return resStream;
        }

        /// <summary>
        /// Create a thumbnail from an image in a Resource file
        /// </summary>
        /// <param name="path"></param>
        /// <param name="maxHeight"></param>
        /// <param name="maxWidth"></param>
        /// <param name="extension"></param>
        /// <returns></returns>
        public static byte[] CreateThumbnail(string path, int maxHeight, int maxWidth, string extension)
        {
            //string temp = path.Replace("/", @"\");
            //path = AppDomain.CurrentDomain.BaseDirectory + temp;
            Image img;
            extension = extension.ToLower();
            try
            {
                img = Image.FromFile(path);
            }
            catch (FileNotFoundException)
            {
                img = Image.FromStream(ImageData("nopix50.png"));
            }
            byte[] buffer;
            try
            {
                int width = img.Size.Width;
                int height = img.Size.Height;

                bool doWidthResize = (maxWidth > 0 && width > maxWidth && width > maxHeight);
                bool doHeightResize = (maxHeight > 0 && height > maxHeight && height > maxWidth);

                //only resize if the image is bigger than the max
                if (doWidthResize || doHeightResize)
                {
                    int iStart;
                    Decimal divider;

                    if (doHeightResize)
                    {
                        iStart = height;
                        divider = Math.Abs(iStart/(Decimal) maxHeight);
                        height = maxHeight;
                        width = (int) Math.Round((width/divider));
                    }
                    else
                    {
                        iStart = width;
                        divider = Math.Abs(iStart/(Decimal) maxWidth);
                        width = maxWidth;
                        height = (int) Math.Round((height/divider));
                    }
                }
                // TEST
                img.RotateFlip(RotateFlipType.Rotate180FlipNone);
                img.RotateFlip(RotateFlipType.Rotate180FlipNone);
                //                
                Image newImg = img.GetThumbnailImage(width, height, null, new IntPtr());
                try
                {
                    using (var ms = new MemoryStream())
                    {
                        if (extension.IndexOf("jpg", StringComparison.Ordinal) > -1)
                        {
                            newImg.Save(ms, ImageFormat.Jpeg);
                        }
                        else if (extension.IndexOf("png", StringComparison.Ordinal) > -1)
                        {
                            newImg.Save(ms, ImageFormat.Png);
                        }
                        else if (extension.IndexOf("gif", StringComparison.Ordinal) > -1)
                        {
                            newImg.Save(ms, ImageFormat.Gif);
                        }
                        else // if (extension.IndexOf("bmp") > -1)
                        {
                            newImg.Save(ms, ImageFormat.Bmp);
                        }
                        buffer = ms.ToArray();
                    }
                }
                finally
                {
                    newImg.Dispose();
                }
            }
            catch
            {
                Image imNoimage = Image.FromFile("/EISCMS/Content/images/nopix50.png");
                try
                {
                    using (var ms = new MemoryStream())
                    {
                        imNoimage.Save(ms, ImageFormat.Png);
                        buffer = ms.ToArray();
                    }
                }
                finally
                {
                    imNoimage.Dispose();
                }
            }
            finally
            {
                img.Dispose();
            }
            return buffer;
        }
    }
}