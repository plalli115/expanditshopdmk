﻿using System.Globalization;
using EISCS.CMS.Build;

namespace EISCS.CMS.Services.ResourceHandling
{
    ///<summary>
    ///</summary>
    public class ResourceManager
    {
        ///<summary>
        ///</summary>
        ///<param name="key"></param>
        ///<param name="languageGuid"></param>
        ///<returns></returns>
        public static string GetTranslatedValue(string key, string languageGuid)
        {
            //
            string name = null;
            try
            {
                var ci = new CultureInfo(languageGuid);
                name = CustomResourceHandler.GetString(key, ci);
            }
            catch
            {
            }
            return name;
        }
    }
}