﻿using System;

namespace EISCS.CMS.Services.ResourceHandling
{
    ///<summary>
    ///</summary>
    [Flags]
    public enum TruncateOptions
    {
        ///<summary>
        ///</summary>
        None = 0x0,

        ///<summary>
        ///</summary>
        FinishWord = 0x1,

        ///<summary>
        ///</summary>
        AllowLastWordToGoOverMaxLength = 0x2,

        ///<summary>
        ///</summary>
        IncludeEllipsis = 0x4
    }

    /// <summary>
    /// ToolBox
    /// </summary>
    public class ToolBox
    {
        /// <summary>
        /// Truncate a string to the specified max length
        /// </summary>
        /// <param name="valueToTruncate"></param>
        /// <param name="maxLength"></param>
        /// <param name="options"></param>
        /// <returns></returns>
        public static string TruncateString
            (string valueToTruncate, int maxLength, TruncateOptions options)
        {
            if (valueToTruncate == null)
            {
                return "";
            }

            if (valueToTruncate.Length <= maxLength)
            {
                return valueToTruncate;
            }

            bool includeEllipsis = (options & TruncateOptions.IncludeEllipsis) ==
                                   TruncateOptions.IncludeEllipsis;
            bool finishWord = (options & TruncateOptions.FinishWord) ==
                              TruncateOptions.FinishWord;
            bool allowLastWordOverflow =
                (options & TruncateOptions.AllowLastWordToGoOverMaxLength) ==
                TruncateOptions.AllowLastWordToGoOverMaxLength;

            string retValue = valueToTruncate.Substring(0, maxLength - 1);

            if (includeEllipsis)
            {
                maxLength -= 1;
            }

            int lastSpaceIndex = retValue.LastIndexOf(" ", StringComparison.CurrentCultureIgnoreCase);

            if (!finishWord)
            {
                retValue = retValue.Remove(maxLength);
            }
            else if (allowLastWordOverflow)
            {
                int spaceIndex = retValue.IndexOf(" ",
                                                  maxLength, StringComparison.CurrentCultureIgnoreCase);
                if (spaceIndex != -1)
                {
                    retValue = retValue.Remove(spaceIndex);
                }
            }
            else if (lastSpaceIndex > -1)
            {
                retValue = retValue.Remove(lastSpaceIndex);
            }

            if (includeEllipsis && retValue.Length < valueToTruncate.Length)
            {
                retValue += "&hellip;";
            }
            return retValue;
        }
    }
}