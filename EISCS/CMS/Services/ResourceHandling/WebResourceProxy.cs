﻿using System;
using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EISCS.CMS.Services.ResourceHandling
{
    ///<summary>
    ///</summary>
    [DefaultProperty("Text")]
    [ToolboxData("<{0}:WebResourceProxy runat=server></{0}:WebResourceProxy>")]
    public class WebResourceProxy : WebControl
    {
        ///<summary>
        ///</summary>
        [Bindable(true)]
        [Category("Appearance")]
        [DefaultValue("")]
        [Localizable(true)]
        public string Text
        {
            get
            {
                var s = (String) ViewState["Text"];
                return (s ?? String.Empty);
            }

            set { ViewState["Text"] = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="output"></param>
        protected override void RenderContents(HtmlTextWriter output)
        {
            output.Write(Text);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            // NEW REFERENCE
            Page.ClientScript.RegisterClientScriptResource(GetType(), "EISCMS.Scripts.jquery-1.4.1.min.js");
            Page.ClientScript.RegisterClientScriptResource(GetType(), "EISCMS.Scripts.jquery-ui-1.8.6.custom.min.js");

            Page.ClientScript.RegisterClientScriptResource(GetType(), "EISCMS.Scripts.swfupload.swfupload.js");
            Page.ClientScript.RegisterClientScriptResource(GetType(), "EISCMS.Scripts.swfupload.handlers.js");
            Page.ClientScript.RegisterClientScriptResource(GetType(), "EISCMS.Scripts.uploadScript.js");
            Page.ClientScript.RegisterClientScriptResource(GetType(), "EISCMS.Scripts.filetree.filetree.js");
            // END NEW REFERENCE            
            Page.ClientScript.RegisterClientScriptResource(GetType(), "EISCMS.Scripts.helptext.js");
            Page.ClientScript.RegisterClientScriptResource(GetType(), "EISCMS.Scripts.helptext_da.js");
            Page.ClientScript.RegisterClientScriptResource(GetType(), "EISCMS.Scripts.helptext_de.js");
            Page.ClientScript.RegisterClientScriptResource(GetType(), "EISCMS.Scripts.helptext_es.js");
            Page.ClientScript.RegisterClientScriptResource(GetType(), "EISCMS.Scripts.helptext_fr.js");
            Page.ClientScript.RegisterClientScriptResource(GetType(), "EISCMS.Scripts.helptext_nl.js");
            Page.ClientScript.RegisterClientScriptResource(GetType(), "EISCMS.Scripts.helptext_pt.js");
            Page.ClientScript.RegisterClientScriptResource(GetType(), "EISCMS.Scripts.helptext_sv.js");
            Page.ClientScript.RegisterClientScriptResource(GetType(), "EISCMS.Scripts.cms.js");
            Page.ClientScript.RegisterClientScriptResource(GetType(), "EISCMS.Scripts.cm.js");
            Page.ClientScript.RegisterClientScriptResource(GetType(), "EISCMS.Content.Content.traditional.css");
            Page.ClientScript.RegisterClientScriptResource(GetType(), "EISCMS.Content.Content.cm.css");
            // NEW REFERENCE
            Page.ClientScript.RegisterClientScriptResource(GetType(),
                                                           "EISCMS.Content.Content.jquery-ui-1.8.6.custom.css");
            // END NEW REFERENCE           
            Page.ClientScript.RegisterClientScriptResource(GetType(), "EISCMS.Scripts.filetree.css.filemanager.css");

            Page.ClientScript.RegisterClientScriptResource(GetType(),
                                                           "EISCMS.Content.Images.ui-bg_flat_30_cccccc_40x100.png");
            Page.ClientScript.RegisterClientScriptResource(GetType(),
                                                           "EISCMS.Content.Images.ui-bg_flat_50_5c5c5c_40x100.png");

            Page.ClientScript.RegisterClientScriptResource(GetType(),
                                                           "EISCMS.Content.Images.ui-bg_glass_20_555555_1x400.png");
            Page.ClientScript.RegisterClientScriptResource(GetType(),
                                                           "EISCMS.Content.Images.ui-bg_glass_40_0078a3_1x400.png");
            Page.ClientScript.RegisterClientScriptResource(GetType(),
                                                           "EISCMS.Content.Images.ui-bg_glass_40_ffc73d_1x400.png");
            Page.ClientScript.RegisterClientScriptResource(GetType(),
                                                           "EISCMS.Content.Images.ui-bg_gloss-wave_25_333333_500x100.png");
            Page.ClientScript.RegisterClientScriptResource(GetType(),
                                                           "EISCMS.Content.Images.ui-bg_inset-soft_25_000000_1x100.png");
            Page.ClientScript.RegisterClientScriptResource(GetType(),
                                                           "EISCMS.Content.Images.ui-icons_ffffff_256x240.png");

            Page.ClientScript.RegisterClientScriptResource(GetType(), "EISCMS.Content.Images.arrow.png");
            Page.ClientScript.RegisterClientScriptResource(GetType(), "EISCMS.Content.Images.checkmark.gif");
            Page.ClientScript.RegisterClientScriptResource(GetType(), "EISCMS.Content.Images.directory.png");
            Page.ClientScript.RegisterClientScriptResource(GetType(), "EISCMS.Content.Images.file.png");
            Page.ClientScript.RegisterClientScriptResource(GetType(), "EISCMS.Content.Images.folder.gif");
            Page.ClientScript.RegisterClientScriptResource(GetType(), "EISCMS.Content.Images.minus1.png");
            Page.ClientScript.RegisterClientScriptResource(GetType(), "EISCMS.Content.Images.nav_btn.gif");
            Page.ClientScript.RegisterClientScriptResource(GetType(), "EISCMS.Content.Images.nopix50.png");
            Page.ClientScript.RegisterClientScriptResource(GetType(), "EISCMS.Content.Images.picture.png");
            Page.ClientScript.RegisterClientScriptResource(GetType(), "EISCMS.Content.Images.plus1.png");
            Page.ClientScript.RegisterClientScriptResource(GetType(), "EISCMS.Content.Images.delete.png");
            Page.ClientScript.RegisterClientScriptResource(GetType(), "EISCMS.Content.Images.newgroup.png");

            Page.ClientScript.RegisterClientScriptResource(GetType(), "EISCMS.Content.Images.spinner.gif");
            Page.ClientScript.RegisterClientScriptResource(GetType(), "EISCMS.Content.Images.ajax-loader2.gif");
            Page.ClientScript.RegisterClientScriptResource(GetType(), "EISCMS.Content.Images.error.png");
            Page.ClientScript.RegisterClientScriptResource(GetType(), "EISCMS.Content.Images.help2.png");
            Page.ClientScript.RegisterClientScriptResource(GetType(), "EISCMS.Content.Images.ok.png");
            Page.ClientScript.RegisterClientScriptResource(GetType(), "EISCMS.Content.Images.delete2.png");

            Page.ClientScript.RegisterClientScriptResource(GetType(), "EISCMS.Content.Images.binocular.png");
            Page.ClientScript.RegisterClientScriptResource(GetType(), "EISCMS.Content.Images.book_blue_open.png");
            Page.ClientScript.RegisterClientScriptResource(GetType(), "EISCMS.Content.Images.document_add.png");
            Page.ClientScript.RegisterClientScriptResource(GetType(), "EISCMS.Content.Images.document_delete.png");
            Page.ClientScript.RegisterClientScriptResource(GetType(), "EISCMS.Content.Images.document_view.png");
            Page.ClientScript.RegisterClientScriptResource(GetType(), "EISCMS.Content.Images.document_view_small.png");
            Page.ClientScript.RegisterClientScriptResource(GetType(), "EISCMS.Content.Images.edit_small.png");
            Page.ClientScript.RegisterClientScriptResource(GetType(), "EISCMS.Content.Images.floppy_disks.png");

            // New images
            Page.ClientScript.RegisterClientScriptResource(GetType(), "EISCMS.Content.Images.bb.png");
            Page.ClientScript.RegisterClientScriptResource(GetType(), "EISCMS.Content.Images.blc.png");
            Page.ClientScript.RegisterClientScriptResource(GetType(), "EISCMS.Content.Images.brc.png");
            Page.ClientScript.RegisterClientScriptResource(GetType(), "EISCMS.Content.Images.menu_blue_bar.png");
            Page.ClientScript.RegisterClientScriptResource(GetType(), "EISCMS.Content.Images.Questionmark_op.png");
            Page.ClientScript.RegisterClientScriptResource(GetType(), "EISCMS.Content.Images.tb.png");
            Page.ClientScript.RegisterClientScriptResource(GetType(), "EISCMS.Content.Images.tb_help2.png");
            Page.ClientScript.RegisterClientScriptResource(GetType(), "EISCMS.Content.Images.tlc.png");
            Page.ClientScript.RegisterClientScriptResource(GetType(), "EISCMS.Content.Images.tlc_help2.png");
            Page.ClientScript.RegisterClientScriptResource(GetType(), "EISCMS.Content.Images.trc.png");
            Page.ClientScript.RegisterClientScriptResource(GetType(), "EISCMS.Content.Images.trc_help2.png");

            Page.ClientScript.RegisterClientScriptResource(GetType(), "EISCMS.Content.Images.tb_top.png");
            Page.ClientScript.RegisterClientScriptResource(GetType(), "EISCMS.Content.Images.tlc_top.png");
            Page.ClientScript.RegisterClientScriptResource(GetType(), "EISCMS.Content.Images.trc_top.png");
            Page.ClientScript.RegisterClientScriptResource(GetType(), "EISCMS.Content.Images.shadow.png");

            Page.ClientScript.RegisterClientScriptResource(GetType(), "EISCMS.Scripts.swfupload.images.error.gif");
            Page.ClientScript.RegisterClientScriptResource(GetType(), "EISCMS.Scripts.swfupload.images.toobig.gif");
            Page.ClientScript.RegisterClientScriptResource(GetType(), "EISCMS.Scripts.swfupload.images.uploadlimit.gif");
            Page.ClientScript.RegisterClientScriptResource(GetType(),
                                                           "EISCMS.Scripts.swfupload.images.XPButtonNoText_160x22.png");
            Page.ClientScript.RegisterClientScriptResource(GetType(), "EISCMS.Scripts.swfupload.images.zerobyte.gif");

            // File Tree
            Page.ClientScript.RegisterClientScriptResource(GetType(),
                                                           "EISCMS.Scripts.filetree.css.images.application.png");
            Page.ClientScript.RegisterClientScriptResource(GetType(),
                                                           "EISCMS.Scripts.filetree.css.images.cancelbutton.gif");
            Page.ClientScript.RegisterClientScriptResource(GetType(), "EISCMS.Scripts.filetree.css.images.code.png");
            Page.ClientScript.RegisterClientScriptResource(GetType(), "EISCMS.Scripts.filetree.css.images.css.png");
            Page.ClientScript.RegisterClientScriptResource(GetType(), "EISCMS.Scripts.filetree.css.images.db.png");
            Page.ClientScript.RegisterClientScriptResource(GetType(), "EISCMS.Scripts.filetree.css.images.delete.png");
            Page.ClientScript.RegisterClientScriptResource(GetType(), "EISCMS.Scripts.filetree.css.images.directory.png");
            Page.ClientScript.RegisterClientScriptResource(GetType(), "EISCMS.Scripts.filetree.css.images.doc.png");
            Page.ClientScript.RegisterClientScriptResource(GetType(), "EISCMS.Scripts.filetree.css.images.error.gif");
            Page.ClientScript.RegisterClientScriptResource(GetType(), "EISCMS.Scripts.filetree.css.images.file.png");
            Page.ClientScript.RegisterClientScriptResource(GetType(), "EISCMS.Scripts.filetree.css.images.film.png");
            Page.ClientScript.RegisterClientScriptResource(GetType(), "EISCMS.Scripts.filetree.css.images.flash.png");
            Page.ClientScript.RegisterClientScriptResource(GetType(),
                                                           "EISCMS.Scripts.filetree.css.images.folder_open.png");
            Page.ClientScript.RegisterClientScriptResource(GetType(), "EISCMS.Scripts.filetree.css.images.header-bg.jpg");
            Page.ClientScript.RegisterClientScriptResource(GetType(), "EISCMS.Scripts.filetree.css.images.html.png");
            Page.ClientScript.RegisterClientScriptResource(GetType(), "EISCMS.Scripts.filetree.css.images.java.png");
            Page.ClientScript.RegisterClientScriptResource(GetType(), "EISCMS.Scripts.filetree.css.images.linux.png");
            Page.ClientScript.RegisterClientScriptResource(GetType(), "EISCMS.Scripts.filetree.css.images.music.png");
            Page.ClientScript.RegisterClientScriptResource(GetType(), "EISCMS.Scripts.filetree.css.images.newgroup.png");
            Page.ClientScript.RegisterClientScriptResource(GetType(),
                                                           "EISCMS.Scripts.filetree.css.images.page_white.png");
            Page.ClientScript.RegisterClientScriptResource(GetType(), "EISCMS.Scripts.filetree.css.images.pdf.png");
            Page.ClientScript.RegisterClientScriptResource(GetType(), "EISCMS.Scripts.filetree.css.images.php.png");
            Page.ClientScript.RegisterClientScriptResource(GetType(), "EISCMS.Scripts.filetree.css.images.picture.png");
            Page.ClientScript.RegisterClientScriptResource(GetType(), "EISCMS.Scripts.filetree.css.images.ppt.png");
            Page.ClientScript.RegisterClientScriptResource(GetType(), "EISCMS.Scripts.filetree.css.images.ruby.png");
            Page.ClientScript.RegisterClientScriptResource(GetType(), "EISCMS.Scripts.filetree.css.images.script.png");
            Page.ClientScript.RegisterClientScriptResource(GetType(), "EISCMS.Scripts.filetree.css.images.spinner.gif");
            Page.ClientScript.RegisterClientScriptResource(GetType(), "EISCMS.Scripts.filetree.css.images.toobig.gif");
            Page.ClientScript.RegisterClientScriptResource(GetType(), "EISCMS.Scripts.filetree.css.images.txt.png");
            Page.ClientScript.RegisterClientScriptResource(GetType(),
                                                           "EISCMS.Scripts.filetree.css.images.uploadlimit.gif");
            Page.ClientScript.RegisterClientScriptResource(GetType(), "EISCMS.Scripts.filetree.css.images.xls.png");
            Page.ClientScript.RegisterClientScriptResource(GetType(),
                                                           "EISCMS.Scripts.filetree.css.images.XPButtonNoText_160x22.png");
            Page.ClientScript.RegisterClientScriptResource(GetType(), "EISCMS.Scripts.filetree.css.images.zerobyte.gif");
            Page.ClientScript.RegisterClientScriptResource(GetType(), "EISCMS.Scripts.filetree.css.images.zip.png");
        }
    }
}