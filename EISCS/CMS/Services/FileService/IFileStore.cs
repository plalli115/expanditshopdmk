﻿using System.Collections.Generic;
using System.Web;

namespace EISCS.CMS.Services.FileService
{
    ///<summary>
    ///</summary>
    public interface IFileStore
    {
        ///<summary>
        ///</summary>
        ///<param name="fileBase"></param>
        ///<param name="relativePath"></param>
        string SaveUploadedFile(HttpPostedFileBase fileBase, string relativePath);

        ///<summary>
        ///</summary>
        ///<param name="parentPath"></param>
        ///<param name="folderName"></param>
        ///<returns></returns>
        string CreateFolder(string parentPath, string folderName);

        ///<summary>
        ///</summary>
        ///<param name="parentPath"></param>
        ///<returns></returns>
        bool DeleteFolder(string parentPath);

        ///<summary>
        ///</summary>
        ///<param name="oldPath"></param>
        ///<param name="newName"></param>
        ///<returns></returns>
        bool RenameFolder(string oldPath, string newName);

        ///<summary>
        ///</summary>
        ///<param name="oldName"></param>
        ///<param name="newName"></param>
        ///<returns></returns>
        bool RenameFile(string oldName, string newName);

        ///<summary>
        ///</summary>
        ///<param name="filePath"></param>
        ///<returns></returns>
        bool DeleteFile(string filePath);

        List<string> ListFiles(string extension);
        string FileTree(string validExtensionArray, string filter);
    }
}