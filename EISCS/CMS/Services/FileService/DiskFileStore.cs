﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Hosting;
using CmsPublic.DataProviders.Logging;
using EISCS.CMS.Build;
using EISCS.CMS.CentralManagement.Configuration.Util;

namespace EISCS.CMS.Services.FileService
{
    ///<summary>
    ///</summary>
    public class DiskFileStore : IFileStore
    {
        private string _uploadsFolder;

        #region IFileStore Members

        ///<summary>
        ///</summary>
        ///<param name="fileBase"></param>
        ///<param name="relativePath"></param>
        public string SaveUploadedFile(HttpPostedFileBase fileBase, string relativePath)
        {
            if (relativePath == null)
            {
                return "Path cant be null. Aborting.";
            }

            if (fileBase == null)
            {
                return "File is null. Aborting.";
            }

            _uploadsFolder = GetPath(relativePath);

            string strFileName = Path.GetFileName(fileBase.FileName);

            string responseText = "Success";

            try
            {
                fileBase.SaveAs(GetDiskLocation(strFileName));
            }
            catch (DirectoryNotFoundException dnfex)
            {
                string q = dnfex.Message;
                FileLogger.Log(q);
                responseText = "Directory does not exist. Failed";
            }
            catch (UnauthorizedAccessException ex)
            {
                string q = ex.Message;
                FileLogger.Log(q);
                responseText = CustomResourceHandler.CATMAN_LABEL_MISSING_WRITE_PERMISSION.Replace("\\", "");
            }
            catch (Exception ex)
            {
                string q = ex.Message;
                FileLogger.Log(q + "\r\n" + ex.StackTrace);
                responseText = "An error occurred while writing file to disk.";
            }
            return responseText;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parentPath"></param>
        /// <param name="folderName"></param>
        /// <returns></returns>
        public string CreateFolder(string parentPath, string folderName)
        {
            string physicalPath = GetPath(Path.Combine(parentPath, folderName));

            physicalPath = RenameUp(physicalPath);
            if (!Directory.Exists(physicalPath))
            {
                Directory.CreateDirectory(physicalPath);
            }
            return physicalPath;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="oldPath"></param>
        /// <param name="newPath"></param>
        /// <returns></returns>
        public bool RenameFolder(string oldPath, string newPath)
        {
            string fromPath = GetPath(oldPath);
            string toPath = GetPath(newPath);
            try
            {
                Directory.Move(fromPath, toPath);
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        ///<summary>
        ///</summary>
        ///<param name="oldPath"></param>
        ///<param name="newPath"></param>
        ///<returns></returns>
        public bool RenameFile(string oldPath, string newPath)
        {
            string fromPath = GetPath(oldPath);
            string toPath = GetPath(newPath);
            try
            {
                File.Move(fromPath, toPath);
            }
            catch (Exception e)
            {
                FileLogger.Log("Rename file failed. Error message: " + e.Message);
                return false;
            }
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parentPath"></param>
        /// <returns></returns>
        public bool DeleteFolder(string parentPath)
        {
            bool isSucess = true;
            string physicalPath = GetPath(parentPath);
            physicalPath = physicalPath.Substring(0, physicalPath.Length - 1);
            if (Directory.Exists(physicalPath))
            {
                try
                {
                    Directory.Delete(physicalPath, true);
                }
                catch (Exception exception)
                {
                    isSucess = false;
                    FileLogger.Log(exception.Message);
                }
            }
            return isSucess;
        }

        ///<summary>
        ///</summary>
        ///<param name="filePath"></param>
        ///<returns></returns>
        public bool DeleteFile(string filePath)
        {
            bool isSucess = true;
            string physicalPath = GetPath(filePath);

            if (File.Exists(physicalPath))
            {
                try
                {
                    if (physicalPath != null) File.Delete(physicalPath);
                }
                catch (Exception exception)
                {
                    isSucess = false;
                    FileLogger.Log(exception.Message);
                }
            }
            return isSucess;
        }

        public List<string> ListFiles(string extension)
        {
            string dir;
            DirectoryInfo di = GetDirectoryInfo(out dir);
            var validExtensions = new[] { extension };
            var files = new List<string>();
            foreach (FileInfo fi in di.GetFiles())
            {
                string ext = "";
                if (fi.Extension.Length > 1)
                {
                    ext = fi.Extension.Substring(1).ToLower();
                }

                bool isValid = validExtensions.Any(t => string.Equals(ext, t));
                if (isValid)
                {
                    files.Add(fi.Name);
                }
            }
            return files;
        }

        public string FileTree(string validExtensionArray, string filter)
        {
            string dir;
            DirectoryInfo di = GetDirectoryInfo(out dir);

            // Return string
            var builder = new StringBuilder();

            // Test for valid types
            string[] validExtensions = null;
            if (!string.IsNullOrEmpty(validExtensionArray))
            {
                validExtensions = validExtensionArray.Split(',');
            }

            if ((validExtensions == null) || (validExtensions.Length == 0))
            {
                validExtensions = new[] { "bmp", "gif", "jpg", "png", "doc", "docx", "pdf", "wmv" };
            }

            builder.Append("<ul class=\"jqueryFileTree\" style=\"display: none;\">\n");
            foreach (DirectoryInfo diChild in di.GetDirectories())
            {
                builder.Append("\t<li class=\"directory collapsed\"><a href=\"#\" rel=\"" + dir + diChild.Name + "/\">" +
                               diChild.Name + "</a></li>\n");
            }

            FileInfo[] fileInfos = di.GetFiles();
            List<FileInfo> pureFileInfos =
                !string.IsNullOrEmpty(filter) ? fileInfos.ToList().FindAll(f => f.Name.ToUpper().StartsWith(filter.ToUpper())) : fileInfos.ToList();

            foreach (FileInfo fi in pureFileInfos)
            {
                string ext = "";
                if (fi.Extension.Length > 1)
                {
                    ext = fi.Extension.Substring(1).ToLower();
                }

                bool isValid = validExtensions.Any(t => string.Equals(ext, t));
                if (isValid)
                {
                    builder.Append("\t<li class=\"file ext_" + ext + "\"><a href=\"#\" rel=\"" + dir + fi.Name + "\">" +
                                   fi.Name + "</a></li>\n");
                }
            }
            builder.Append("</ul>");

            return builder.ToString();
        }

        #endregion

        protected DirectoryInfo GetDirectoryInfo(out string dir)
        {
            HttpRequest request = HttpContext.Current.Request;
            HttpServerUtility server = HttpContext.Current.Server;
            if (request.Form["dir"] == null || request.Form["dir"].Length <= 0)
            {
                dir = "/";
            }
            else
            {
                dir = server.UrlDecode(request.Form["dir"]);
            }

            string sPath = request.ServerVariables["APPL_PHYSICAL_PATH"];

            string virtualDirectory = ConfigUtil.OptionalVirtualDirectory();
            if (virtualDirectory != null)
            {
                sPath = HostingEnvironment.MapPath("/" + virtualDirectory) + "\\";
            }

            if (dir == null) dir = "/";

            string physicalPath = dir;
            physicalPath = sPath + physicalPath.Replace("/", "\\");
            physicalPath = physicalPath.Replace("\\\\", "\\");
            return new DirectoryInfo(physicalPath);
        }

        ///<summary>
        ///</summary>
        ///<param name="pathName"></param>
        ///<returns></returns>
        public string RenameUp(string pathName)
        {
            DirectoryInfo directoryInfo = Directory.GetParent(pathName);
            string[] dirs = Directory.GetDirectories(directoryInfo.FullName);
            List<string> allPaths = dirs.ToList();
            allPaths.Sort();
            for (int i = 0; i < allPaths.Count; i++)
            {
                if (pathName == allPaths[i])
                {
                    string workWith = Numeric(pathName);
                    string theActual = pathName;

                    int intValue = 0;
                    if (!string.IsNullOrEmpty(workWith))
                    {
                        theActual = pathName.Remove(pathName.Length - workWith.Length);
                        intValue = int.Parse(workWith);
                    }
                    intValue += 1;
                    pathName = String.Concat(theActual, intValue);
                }
            }
            return pathName;
        }

        ///<summary>
        ///</summary>
        ///<param name="toTest"></param>
        ///<returns></returns>
        public string Numeric(string toTest)
        {
            string toReturn = "";
            char[] chars = toTest.ToCharArray();
            var lookup = new[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
            var numericsChars = new List<char>();

            for (int i = chars.Length - 1; i >= 0; i--)
            {
                foreach (char t in lookup)
                {
                    if (chars[i] == t)
                    {
                        numericsChars.Add(chars[i]);
                    }
                }
            }

            if (numericsChars.Count > 0)
            {
                int result = 0;
                var ints = new List<int>();
                for (int i = 0; i < numericsChars.Count; i++)
                {
                    Console.WriteLine(numericsChars[i]);
                    int temp = int.Parse(numericsChars[i].ToString());

                    int part = MultiplyBy(10, i);
                    if (part == 0)
                    {
                        part = 1;
                    }
                    int semiPart = part * temp;
                    result += semiPart;
                }
                toReturn = result.ToString();
            }

            return toReturn;
        }

        ///<summary>
        ///</summary>
        ///<param name="number"></param>
        ///<param name="times"></param>
        ///<returns></returns>
        public int MultiplyBy(int number, int times)
        {
            int total = 1;
            for (int i = 0; i < times; i++)
            {
                total = total * number;
            }
            return total;
        }

        protected string GetPath(string relativePath)
        {
            string virtualDirectory = ConfigUtil.OptionalVirtualDirectory();
            string path;
            if (virtualDirectory != null)
            {
                path = HostingEnvironment.MapPath("/" + virtualDirectory) + relativePath.Replace("/", @"\");
                return path;
            }
            string temp = relativePath.Replace("/", @"\");
            path = AppDomain.CurrentDomain.BaseDirectory + temp;
            return path;
        }

        private string GetDiskLocation(string identifier)
        {
            return Path.Combine(_uploadsFolder, identifier);
        }
    }
}