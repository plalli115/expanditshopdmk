﻿using System.Collections.Generic;
using System.Web;
using EISCS.CMS.Services.AppSettings;
using EISCS.Shop.DO.Dto;
using EISCS.Shop.DO.Interface;

namespace EISCS.CMS.ControllerCommon
{
    public class LanguageLoader
    {
        public static void LoadLanguageSessionData(ILanguageRepository languageRepository, HttpRequestBase request,
                                                   HttpSessionStateBase session, SelectListStore selectListStore,
                                                   HttpServerUtilityBase server)
        {
            // Load property languages and set initial selected language
            List<LanguageTable> languages = languageRepository.LoadLanguages();
            session["Languages"] = languages;
            string initialLanguageGuid = AppSettingsReader.Read("LANGUAGE_DEFAULT");

            // Load page languages and set initial selected page language
            string theLanguage = null;
            HttpCookieCollection cookies = request.Cookies;
            if (cookies != null && cookies.Count > 0)
            {
                HttpCookie cookie = cookies["pageLanguage"];
                if (cookie != null)
                {
                    theLanguage = server.UrlDecode(cookie.Value);
                }
            }
            theLanguage = string.IsNullOrEmpty(theLanguage) ? initialLanguageGuid : theLanguage;
            session["PageLanguageGuid"] = theLanguage;
        }
    }
}