﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Web.Mvc;
using CmsPublic.DataProviders.Logging;
using CmsPublic.ModelLayer.Models;
using EISCS.CMS.Build;
using EISCS.CMS.Configuration;
using EISCS.CMS.Models.Logic;
using EISCS.CMS.Services.FileService;
using EISCS.Shop.DO.Dto;

namespace EISCS.CMS.ControllerCommon
{

    ///<summary>
    ///</summary>
    public class RouteManagerNode
    {
        ///<summary>
        ///</summary>
        public string ActionName { get; set; }
        ///<summary>
        ///</summary>
        public string MethodName { get; set; }
    }

    ///<summary>
    ///</summary>
    public class SelectListStore
    {

        // Holds the sync filenames
        private List<SyncModes> _syncModeList;

        ///<summary>
        ///</summary>
        ///<param name="selected"></param>
        ///<returns></returns>
        public SelectList GetLanguageSelectList(string selected)
        {

            var pageLanguages = new List<LanguageTable>
                                    {
                                        new LanguageTable {LanguageGuid = "da", LanguageName = "Dansk"},
                                        new LanguageTable {LanguageGuid = "de", LanguageName = "Deutsch"},
                                        new LanguageTable {LanguageGuid = "en", LanguageName = "English"},
                                        new LanguageTable {LanguageGuid = "es", LanguageName = "Español"},
                                        new LanguageTable {LanguageGuid = "fr", LanguageName = "Français"},
                                        new LanguageTable {LanguageGuid = "nl", LanguageName = "Nederlands"},
                                        new LanguageTable {LanguageGuid = "pt", LanguageName = "Português"},
                                        new LanguageTable {LanguageGuid = "sv", LanguageName = "Svenska"}
                                    };

            return new SelectList(pageLanguages, "LanguageGuid", "LanguageName", selected);
        }

        ///<summary>
        ///</summary>
        ///<param name="selected"></param>
        ///<returns></returns>
        public SelectList GetJobSelectList(string selected)
        {

            if (_syncModeList == null)
            {
                _syncModeList = new List<SyncModes>();

                try
                {
                    JobCollection jobData = new JobConfiguration().ConfigurationToObjects();

                    foreach (var job in jobData.Job)
                    {
                        _syncModeList.Add(new SyncModes { FileName = job.FileName, MethodName = job.MethodName });
                    }
                }
                catch (Exception ex)
                {
                    FileLogger.Log(ex.Message + ": " + ex.StackTrace);
                }
            }

            return new SelectList(_syncModeList, "FileName", "MethodName", selected);

        }

        ///<summary>
        ///</summary>
        ///<param name="selected"></param>
        ///<returns></returns>
        public SelectList GetRouteSelectList(string selected)
        {
            var routeManagerNodes = new List<RouteManagerNode>
                                                           {
                                                               new RouteManagerNode
                                                                   {
                                                                       ActionName = "CreateAll",
                                                                       MethodName = CustomResourceHandler.CATMAN_ACTION_CREATE_ROUTES
                                                                   }
                                                           };
            var selectList = new SelectList(routeManagerNodes, "ActionName", "MethodName", selected);
            return selectList;
        }

        ///<summary>
        ///</summary>
        ///<param name="selected"></param>
        ///<returns></returns>
        public SelectList GetThemeSelectList(string selected)
        {
            var cssNameList = new List<CatManThemes>
                                  {
                                      new CatManThemes {CssName = "HeavyMetal", ThemeName = "HeavyMetal"},
                                      new CatManThemes {CssName = "traditional", ThemeName = "Pastel"},
                                      new CatManThemes {CssName = "SoftMetal", ThemeName = "SoftMetal"}
                                  };

            var selectList = new SelectList(cssNameList, "CssName", "ThemeName", selected);
            return selectList;
        }

        // Simple Template select
        /// <summary>
        /// 
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="selectedItem"></param>
        /// <param name="groupProductManager"> </param>
        /// <returns></returns>
        public SelectList CreateTemplateSelect(string filter, string selectedItem, GroupProductManager groupProductManager)
        {
            var templateList = groupProductManager.TemplateTable(filter);
            if (filter == "IsProductTemplate" && templateList.All(x => x.TemplateGuid != selectedItem))
            {
                // IsProductTemplate but the selected value is not a Product Template
                var productTemplate = templateList.Find(x => x.TemplateFileName == "Product");
                if (productTemplate != null)
                {
                    selectedItem = productTemplate.TemplateGuid;
                }
            }
            var selectList = new SelectList(
                templateList, "TemplateGuid", "TemplateName", selectedItem);
            return selectList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public SelectList CreateUrlSelect(string languageGuid, GroupProductManager groupProductManager, int start = 0, int length = 0)
        {
            var urls = new List<string> { "----------" };
            List<string> realUrls = groupProductManager.GetInternalLinkUrls(languageGuid);
            IEnumerable<string> urlList;
            if (length > 0)
            {
                urlList = realUrls.Take(length);
            }
            else
            {
                urlList = realUrls;
            }
            urls.AddRange(urlList);
            var selectList = new SelectList(urls, urls[0]);

            return selectList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public SelectList CreateAspxSelect(IFileStore fileStore)
        {
            var files = new List<string> { "----------" };
            List<string> realFiles = fileStore.ListFiles("aspx");
            files.AddRange(realFiles);
            var selectList = new SelectList(files, files[0]);
            return selectList;
        }

        public SelectList CreateTargetSelect(string selectedTargetValue)
        {
            if (string.IsNullOrEmpty(selectedTargetValue))
            {
                selectedTargetValue = "_blank";
            }
            return new SelectList(new List<string> { "_blank", "_self", "_parent", "_top" },
                                                  selectedTargetValue);
        }

    }
}
