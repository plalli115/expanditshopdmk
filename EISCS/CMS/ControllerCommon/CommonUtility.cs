﻿using System;
using System.Collections.Generic;
using System.Resources;
using CmsPublic.DataProviders.Logging;
using EISCS.CMS.Build;
using ResourceManager = EISCS.CMS.Services.ResourceHandling.ResourceManager;

namespace EISCS.CMS.ControllerCommon
{
    public class CommonUtility
    {
        public static string DictSafeString(Dictionary<string, string> dict, string key, string langGuid, bool isLoggingEnabled)
        {
            string dictionaryValue = null;
            string name = null;

            try
            {
                if (dict.ContainsKey(key))
                {
                    dictionaryValue = dict[key];
                }
            }
            catch (Exception exception)
            {
                if (isLoggingEnabled)
                {
                    FileLogger.Log(exception.Message);
                }
            }

            if (string.IsNullOrEmpty(dictionaryValue))
            {
                try
                {
                    name = ResourceManager.GetTranslatedValue("CATMAN_LABEL_UNKNOWN_PROPERTY", langGuid);
                }
                catch (Exception ex)
                {
                    if (isLoggingEnabled)
                    {
                        FileLogger.Log(ex.Message);
                    }
                }
                return string.IsNullOrEmpty(name) ? CustomResourceHandler.CATMAN_LABEL_UNKNOWN_PROPERTY : name;
            }
            return dictionaryValue;
        }
    }
}
