﻿namespace EISCS.CMS.ControllerCommon
{
    ///<summary>
    ///</summary>
    public class SyncStatusNode
    {
        ///<summary>
        ///</summary>
        ///<param name="name"></param>
        ///<param name="actionGuid"></param>
        ///<param name="status"></param>
        public SyncStatusNode(string name, string actionGuid, string status)
        {
            ActionFileName = name;
            ActionGuid = actionGuid;
            Status = status;
        }

        ///<summary>
        ///</summary>
        public string ActionFileName { get; set; }

        ///<summary>
        ///</summary>
        public string ActionGuid { get; set; }

        ///<summary>
        ///</summary>
        public string Status { get; set; }
    }
}