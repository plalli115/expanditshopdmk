﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using CmsPublic.DataProviders.Logging;
using EISCS.CMS.Configuration;
using EISCS.CMS.Helpers;
using EISCS.CMS.Services.FileService;

namespace EISCS.CMS.ControllerCommon
{
    ///<summary>
    ///</summary>
    public class SyncTask
    {
        private readonly HttpContextBase _httpContext;
        private readonly string _syncFolder;
        private static string _fastPollInterval;
        private static string _slowPollInterval;
        private static int _longPollValue;
        // Additional sync files        
        private readonly string[] _statusFiles = { ".job", ".job.queued", ".job.running", ".job.error", ".job.done" };

        public SyncTask(HttpContextBase httpContext)
        {
            _httpContext = httpContext;
            _syncFolder = string.Format("/{0}/", CmsSyncTaskConfigSection.Section.SyncFolder);
        }

        ///<summary>
        ///</summary>
        public static int LongPollValue
        {
            get
            {
                if (_longPollValue == 0)
                {
                    int fast = int.Parse(FastPollInterval());
                    int slow = int.Parse(SlowPollInterval());
                    _longPollValue = fast >= slow ? fast : slow;
                }
                return _longPollValue;
            }
        }

        private static string FastPollInterval()
        {
            return string.IsNullOrEmpty(_fastPollInterval)
                       ? _fastPollInterval = HtmlHelpers.InternalPollInterval("FastPoll", "7")
                       : _fastPollInterval;
        }

        private static string SlowPollInterval()
        {
            return string.IsNullOrEmpty(_slowPollInterval)
                       ? _slowPollInterval = HtmlHelpers.InternalPollInterval("SlowPoll", "60")
                       : _slowPollInterval;
        }

        ///<summary>
        ///</summary>
        ///<param name="jobId"></param>
        ///<returns></returns>
        public string JobStatus(string jobId)
        {
            Poll(new SelectListStore().GetJobSelectList(""), jobId);
            return ((SyncStatusNode)_httpContext.Cache[jobId]).Status;
        }

        ///<summary>
        ///</summary>
        ///<param name="sl"></param>
        ///<param name="jobId"></param>
        ///<returns></returns>
        public string Poll(SelectList sl, string jobId)
        {
            bool deleteFiles = false;
            string returnVal = "-1";
            var fh = new FileHandler();
            foreach (SelectListItem li in sl)
            {
                for (int i = 0; i < _statusFiles.Length; i++)
                {
                    string temp = li.Value;
                    string replaced = _syncFolder + temp + _statusFiles[i];

                    if (i == 0)
                    {
                        if (fh.IsFileExist(replaced))
                        {
                            returnVal = "0";
                        }
                    }
                    if (i == 1)
                    {
                        if (fh.IsFileExist(replaced))
                        {
                            returnVal = "1";
                        }
                    }
                    if (i == 2)
                    {
                        if (fh.IsFileExist(replaced))
                        {
                            returnVal = "2";
                        }
                    }
                    if (i == 3)
                    {
                        if (fh.IsFileExist(replaced))
                        {
                            //_finishedErrorJobId = ((SyncStatusNode)_httpContext.Cache[jobId]).ActionGuid;
                            // Error file was detected, read it's content.
                            string errorContent = fh.ReadFileFromRelativePath(replaced);
                            if (errorContent != null && errorContent.Length > 2)
                            {
                                returnVal = errorContent;
                            }
                            else
                            {
                                returnVal = "3";        
                            }
                            return returnVal;
                        }
                    }

                    if (i == 4)
                    {
                        if (fh.IsFileExist(replaced))
                        {
                            returnVal = "4";
                            deleteFiles = true;
                        }
                    }
                }
            }

            try
            {
                if (_httpContext.Cache[jobId] != null)
                    ((SyncStatusNode)_httpContext.Cache[jobId]).Status = returnVal;
            }
            catch (Exception ex)
            {
                FileLogger.Log(ex.Message + " Cache[jobId] could not be set."); 
            }

            if (deleteFiles)
            {
                DeleteSyncFiles(sl);
            }
            return returnVal;
        }

        /// <summary>
        /// </summary>
        /// <param name="sl"></param>
        /// <param name="jobType"></param>
        /// <returns></returns>
        public bool FindFiles(SelectList sl, out string jobType)
        {
            var fh = new FileHandler();

            foreach (SelectListItem li in sl)
            {
                // TODO: This if might not be needed?
                if (fh.IsFileExist(_syncFolder + li.Value + ".txt"))
                {
                    jobType = li.Value;
                    return true;
                }
                for (int i = 0; i < _statusFiles.Length; i++)
                {
                    if (fh.IsFileExist(_syncFolder + li.Value + _statusFiles[i]))
                    {
                        jobType = _statusFiles[i];
                        return true;
                    }
                }
            }
            jobType = null;
            return false;
        }

        ///<summary>
        ///</summary>
        ///<param name="sl"></param>
        ///<returns></returns>
        public bool DeleteSyncFiles(SelectList sl)
        {
            IEnumerable<string> allStatusStrings = BuildAllStatusStringList(sl);
            var fh = new FileHandler();

            foreach (string path in allStatusStrings)
            {
                if (fh.IsFileExist(path))
                {
                    fh.DeleteFile(path);
                }
            }

            return true;
        }

        private IEnumerable<string> BuildAllStatusStringList(IEnumerable<SelectListItem> sl)
        {
            var allStatusStrings = new List<string>();
            foreach (SelectListItem li in sl)
            {
                for (int i = 0; i < _statusFiles.Length; i++)
                {
                    allStatusStrings.Add(_syncFolder + li.Value + _statusFiles[i]);
                }
            }
            return allStatusStrings;
        }
    }
}