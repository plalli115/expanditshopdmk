﻿using System.Collections.Generic;
using EISCS.Shop.DO.Dto;

namespace EISCS.CMS.Account
{
    public interface IBusinessCenterAccountService
    {
        BusinessCenterUser Get(string userId);
        IEnumerable<AccessRoles> GetUserAccessByRole(string accessClass, string userRole);
        BusinessCenterUser Login(string userName, string password);
        bool ChangePassword(string username, string currentUserPassword, string newUserPassword);
        IEnumerable<BusinessCenterUser> GetAllUsers();
        bool CreateUser(BusinessCenterUser user);
        void DeleteUser(string userName);
    }
}