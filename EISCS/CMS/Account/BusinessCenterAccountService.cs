﻿using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using CmsPublic.DataRepository;
using EISCS.ExpandITFramework.Util;
using EISCS.Shop.DO.Dto;
using EISCS.Shop.DO.Interface;

namespace EISCS.CMS.Account
{
    public class BusinessCenterAccountService : BaseDataService, IBusinessCenterAccountService
    {
        private readonly ILoginService _loginService;

        public BusinessCenterAccountService(IExpanditDbFactory dbFactory, ILoginService loginService)
            : base(dbFactory)
        {
            _loginService = loginService;
        }

        public BusinessCenterUser Get(string userId)
        {
            const string sql = "SELECT * FROM BusinessCenterUser WHERE UserGuid = @userGuid";
            return GetResults<BusinessCenterUser>(sql, new { userGuid = userId }).FirstOrDefault();
        }

        public IEnumerable<AccessRoles> GetUserAccessByRole(string accessClass, string userRole)
        {
            var userAccessParam = new { accessClass, userRole };

            const string sql = @"
                SELECT * FROM AccessRoles WHERE RoleId = @userRole AND AccessClass = @accessClass;
                ";
            return GetResults<AccessRoles>(sql, userAccessParam);
        }

        public BusinessCenterUser Login(string userName, string password)
        {
            var chosenEncryptionType = ConfigurationManager.AppSettings["ENCRYPTION_TYPE"];
            
            const string sql = "SELECT * FROM BusinessCenterUser WHERE UserLogin = @userName";
            var user = GetResults<BusinessCenterUser>(sql, new { userName, password }).FirstOrDefault();
            if (user == null)
            {
                return null;
            }
            var success = _loginService.ValidatePasswordAndEncryptionType(user.UserGuid, password, chosenEncryptionType, user.UserPassword, user.EncryptionType, null);
            return success ? user : null;
        }

        public bool ChangePassword(string username, string currentUserPassword, string newUserPassword)
        {
            var user = Login(username, currentUserPassword);
            if (user != null)
            {
                var chosenEncryptionType = ConfigurationManager.AppSettings["ENCRYPTION_TYPE"];
                Encryption.ValidateEncryptionType(chosenEncryptionType);
                var encryptionSalt = Encryption.GenerateSaltValue();
                newUserPassword = Encryption.Encrypt(newUserPassword, encryptionSalt, chosenEncryptionType);
                currentUserPassword = Encryption.Encrypt(currentUserPassword, encryptionSalt, chosenEncryptionType);
                encryptionSalt = Encryption.UseEncryptionSalt(chosenEncryptionType) ? encryptionSalt : "NULL";
                return Execute("UPDATE BusinessCenterUser SET UserPassword = @newUserPassword, EncryptionSalt = @encryptionSalt WHERE UserLogin = @username AND UserPassword = @currentUserPassword",
                    new { username, newUserPassword, currentUserPassword, encryptionSalt });
            }
            return false;
        }

        public IEnumerable<BusinessCenterUser> GetAllUsers()
        {
            const string sql = "SELECT * FROM BusinessCenterUser";
            return GetResults<BusinessCenterUser>(sql);
        }

        public bool CreateUser(BusinessCenterUser user)
        {
            string sql = string.Format("INSERT INTO BusinessCenterUser {0}", GenerateInsertParameters(user, false));
            return Execute(sql, user);
        }

        public void DeleteUser(string userName)
        {
            const string sql = "DELETE FROM BusinessCenterUser WHERE UserLogin = @userName";
            Execute(sql, new {userName});
        }
    }
}