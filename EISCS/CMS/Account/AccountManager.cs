﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using CmsPublic.DataProviders.Logging;
using EISCS.ExpandITFramework.Util;
using EISCS.Shop.DO.Dto;

namespace EISCS.CMS.Account
{
    public interface IBusinessCenterAccountManager
    {
        BusinessCenterUser Get();
        bool Login(string userId, string password);
        void LogOut();
        bool UserAccess(string accessClass);
        bool ChangePassword(string username, string currentUserPassword, string newUserPassword);
        List<BusinessCenterUser> GetAllUsers();
        BusinessCenterUser CreateUser(string userName, string password, string roleId);
        void UpdateUser(BusinessCenterUser newUser);
        void AddUserToRole(string userName, string role);
        string ResetPassword(string userGuid, string password, int length);
        void DeleteUser(string userName);
    }

    public class AccountManager : IBusinessCenterAccountManager
    {
        private readonly HttpContextBase _httpContext;
        private readonly IBusinessCenterAccountService _service;
        private const string CookieName = "bcuser";
        private const string CookieField = "bcuserid";

        public AccountManager(IBusinessCenterAccountService service, HttpContextBase httpContext)
        {
            _httpContext = httpContext;
            _service = service;
        }

        public BusinessCenterUser Get()
        {
            string userId = GetValueFromCookie(CookieField, CookieName);
            return _service.Get(userId);
        }

        public bool ChangePassword(string username, string currentUserPassword, string newUserPassword)
        {
            return _service.ChangePassword(username, currentUserPassword, newUserPassword);
        }

        public List<BusinessCenterUser> GetAllUsers()
        {
            return _service.GetAllUsers() as List<BusinessCenterUser>;
        }

        public BusinessCenterUser CreateUser(string userName, string password, string roleId)
        {
            var chosenEncryptionType = ConfigurationManager.AppSettings["ENCRYPTION_TYPE"];
            Encryption.ValidateEncryptionType(chosenEncryptionType);
            var encryptionSalt = Encryption.GenerateSaltValue();

            BusinessCenterUser user = new BusinessCenterUser
            {
                UserGuid = Guid.NewGuid().ToString(),
                UserLogin = userName,
                UserPassword = Encryption.Encrypt(password, encryptionSalt, chosenEncryptionType),
                RoleId = roleId,
                LastLoginDate = DateTime.Now,
                EncryptionType = chosenEncryptionType,
                EncryptionSalt = Encryption.UseEncryptionSalt(chosenEncryptionType) ? encryptionSalt : "NULL",
                IsSuperUser = false
            };

            // No users found - this is the first to be created - this will be the super user that cannot be deleted
            if (!GetAllUsers().Any())
            {
                user.RoleId = "Admin";
                user.IsSuperUser = true;
            }

            return _service.CreateUser(user) ? user : null;
        }

        public void UpdateUser(BusinessCenterUser newUser)
        {
            throw new NotImplementedException();
        }

        public void AddUserToRole(string userName, string role)
        {
            throw new NotImplementedException();
        }

        public string ResetPassword(string userGuid, string password, int length)
        {
            var user = _service.Get(userGuid);
            if (user == null)
            {
                return null;
            }
            if (string.IsNullOrWhiteSpace(password))
            {
                password = Utilities.GetRandomPassword(length);
            }
            return ChangePassword(user.UserLogin, user.UserPassword, password) ? password : null;
        }

        public void DeleteUser(string userName)
        {
            _service.DeleteUser(userName);
        }

        public bool Login(string userName, string password)
        {
            var user = _service.Login(userName, password);
            if (user == null)
            {
                return false;
            }
            SetCookieValue(CookieName, CookieField, user.UserGuid);
            return true;
        }

        public void LogOut()
        {
            SetCookieValue(CookieName, CookieField, "");
        }

        public bool UserAccess(string accessClass)
        {
            bool hasdata = _service.GetAllUsers().Any();
            if (!hasdata)
            {
                return true;
            }

            var user = Get();

            string userRole;

            if (user == null)
            {
                userRole = "Anonymous";
            }
            else if (string.IsNullOrWhiteSpace(user.RoleId))
            {
                userRole = "B2C";
            }
            else
            {
                // If super user - give access to everything
                if (user.IsSuperUser)
                {
                    return true;
                }
                // Else set current role for access check
                userRole = user.RoleId;
            }

            var data = _service.GetUserAccessByRole(accessClass, userRole);
            return data.Any();
        }

        private string GetValueFromCookie(string key, string cookieName)
        {
            if (_httpContext.Request == null || _httpContext.Request.Cookies == null)
            {
                return string.Empty;
            }

            var userCookie = _httpContext.Request.Cookies[cookieName];
            if (userCookie == null)
                return string.Empty;

            var value = userCookie.Values[key];

            if (string.IsNullOrWhiteSpace(value))
                return ReturnHtmldDecodedValue(value);

            if (value.Length % 2 != 0)
                return ReturnHtmldDecodedValue(value);

            try
            {
                var decodedBytes = MachineKey.Decode(value, MachineKeyProtection.All);
                if (decodedBytes != null)
                    value = Encoding.UTF8.GetString(decodedBytes);
            }
            catch (Exception ex)
            {
                FileLogger.Log(ex.Message + ": " + ex.StackTrace + " value = " + value);
            }

            return ReturnHtmldDecodedValue(value);
        }

        private string ReturnHtmldDecodedValue(string value)
        {
            return string.IsNullOrEmpty(value) ? string.Empty : _httpContext.Server.HtmlDecode(value);
        }

        private void SetCookieValue(string cookieName, string cookieKey, string cookieValue)
        {
            if (cookieValue == null)
            {
                cookieValue = "";
            }
            byte[] unprotectedBytes = Encoding.UTF8.GetBytes(cookieValue);
            string encodedCookieValue = MachineKey.Encode(unprotectedBytes, MachineKeyProtection.All);
            _httpContext.Response.Cookies[cookieName][cookieKey] = encodedCookieValue; //cookieValue;
            _httpContext.Response.Cookies[cookieName].Path = _httpContext.Server.UrlPathEncode(AppUtil.GetVirtualRoot()) + "/";
        }
    }
}
