﻿using System.Configuration;

namespace EISCS.CMS.Configuration
{
    public class CmsSyncTaskConfigSection : ConfigurationSection
    {
        [ConfigurationProperty("actions", IsDefaultCollection = false),
         ConfigurationCollection(typeof(ActionsCollection), AddItemName = "action",
             ClearItemsName = "clearActions", RemoveItemName = "removeAction")]
        public ActionsCollection SyncTaskActions
        {
            get { return this["actions"] as ActionsCollection; }
        }

        [ConfigurationProperty("NumberOfActions", IsRequired = true)]
        [IntegerValidator(MinValue = 0, MaxValue = 1000000)]
        public int NumberOfActions
        {
            get { return (int)this["NumberOfActions"]; }
            set { this["NumberOfActions"] = value; }
        }

        [ConfigurationProperty("SyncFolder", IsRequired = true)]
        public string SyncFolder
        {
            get { return (string)this["SyncFolder"]; }
            set { this["SyncFolder"] = value; }
        }

        private static CmsSyncTaskConfigSection _section = ConfigurationManager.GetSection("CmsSyncTaskSettings") as CmsSyncTaskConfigSection;

        public static CmsSyncTaskConfigSection Section
        {
            get
            {
                return _section;
            }
        }
    }

    public class ActionsCollection : ConfigurationElementCollection
    {
        public override ConfigurationElementCollectionType CollectionType
        {
            get { return ConfigurationElementCollectionType.AddRemoveClearMap; }
        }

        public CmsSyncTaskElement this[int index]
        {
            get { return (CmsSyncTaskElement)BaseGet(index); }
            set
            {
                if (BaseGet(index) != null)
                    BaseRemoveAt(index);
                BaseAdd(index, value);
            }
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new CmsSyncTaskElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((CmsSyncTaskElement)element).Id;
        }
    }

    public class CmsSyncTaskElement : ConfigurationElement
    {
        public CmsSyncTaskElement()
        {

        }

        [ConfigurationProperty("id", IsRequired = true, DefaultValue = "")]
        public string Id
        {
            get { return (string)this["id"]; }
            set { this["id"] = value; }
        }

        [ConfigurationProperty("label", IsRequired = true, DefaultValue = "")]
        public string Label
        {
            get { return (string)this["label"]; }
            set { this["label"] = value; }
        }

        [ConfigurationProperty("SemaphoreName", IsRequired = true, DefaultValue = "")]
        public string SemaphoreName
        {
            get { return (string)this["SemaphoreName"]; }
            set { this["SemaphoreName"] = value; }
        }

        [ConfigurationProperty("ExecutablePath", IsRequired = true, DefaultValue = "")]
        public string ExecutablePath
        {
            get { return (string)this["ExecutablePath"]; }
            set { this["ExecutablePath"] = value; }
        }
    }
}
