﻿using System;
using CmsPublic.DataProviders.Logging;
using EISCS.CMS.Build;

namespace EISCS.CMS.Configuration
{
    public interface IJobConfiguration
    {
        JobCollection ConfigurationToObjects();
    }

    // class reads xml file and convert to object
    // Return a collection of objects.
    public class JobConfiguration
    {
        public JobCollection ConfigurationToObjects()
        {
            int items = CmsSyncTaskConfigSection.Section.NumberOfActions;

            var collection = new JobCollection { Job = new Job[items] };

            for (int i = 0; i < items; i++)
            {
                CmsSyncTaskElement element = CmsSyncTaskConfigSection.Section.SyncTaskActions[i];
                var job = new Job { FileName = element.SemaphoreName, MethodName = CustomResourceHandler.GetValue(element.Label), ExecutablePath = element.ExecutablePath };
                collection.Job[i] = job;
            }

            return collection;
        }
    }
}