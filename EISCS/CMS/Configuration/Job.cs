﻿namespace EISCS.CMS.Configuration
{
    public class Job
    {
        public string MethodName { get; set; }
        public string FileName { get; set; }
        public string ExecutablePath { get; set; }
    }

    public class JobCollection
    {
        public Job[] Job { get; set; }
    }
}