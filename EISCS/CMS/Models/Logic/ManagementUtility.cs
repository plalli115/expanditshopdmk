﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using CmsPublic.ModelLayer.Models;
using EISCS.Catalog;
using EISCS.Routing.Concrete;
using EISCS.Routing.Concrete.RouteCreation;
using EISCS.Routing.Interfaces;
using EISCS.Shop.DO.Dto;
using EISCS.Shop.DO.Interface;

namespace EISCS.CMS.Models.Logic
{
    ///<summary>
    ///</summary>
    public class ManagementUtility
    {
        private readonly CatalogTreeUtility _catalogTreeUtility;
        private readonly IGroupRepository _groupRepository;
        private readonly ILanguageRepository _languageRepository;
        private readonly IProductRepository _productRepository;

        /// <summary>
        /// </summary>
        /// <param name="groupRepository"></param>
        /// <param name="productRepository"></param>
        public ManagementUtility(IGroupRepository groupRepository, IProductRepository productRepository)
        {
            _groupRepository = groupRepository;
            _productRepository = productRepository;
            _catalogTreeUtility = new CatalogTreeUtility(null);
            _languageRepository = DependencyResolver.Current.GetService<ILanguageRepository>();
            Languages = _languageRepository.LoadLanguages();
        }

        #region Common Route Management Methods


        public void UpdateProductBasedRouteData(IRouteDataProvider<RouteDataNode> routeDataProvider,
                                                IRouteCreator<RouteDataNode> routeCreator, string productGuid,
                                                string languageGuid, List<GroupProduct> groupProducts )
        {
            // Delete all occurrences with spec. guid
            var guidList = new List<string> {productGuid};
            routeDataProvider.RemoveByProductGuid(guidList, languageGuid);
            // Recreate all routes in Product and GroupProduct

            Product p = _productRepository.SingleProduct(productGuid, languageGuid, true);

            if (groupProducts == null) return;
            var routeDataNodes = new FlushableCollection<RouteDataNode>(routeDataProvider);
            foreach (GroupProduct node in groupProducts)
            {
                Group grp = _groupRepository.GetGroup(node.GroupGuid, languageGuid, true);

                if (grp == null) continue;

                PrepareForCreateNew(grp, languageGuid);
                routeCreator.CreateGroupProductRoutes(routeDataNodes, grp, p, languageGuid);
            }

            // Add the product routes as well
            routeCreator.CreateProductRoutes(routeDataNodes, p, languageGuid);

            if (routeDataNodes.Count > 0)
            {
                routeDataProvider.AddRange(routeDataNodes);
            }
            /*** DEBUG WRITING ***/
            //foreach (RouteDataNode routeDataNode in routeDataNodes) {
            //    FileLogger.Log(String.Format("Route: {0}", routeDataNode.RouteUrl));
            //    FileLogger.Log(String.Format("TypeCode: {0}", routeDataNode.TypeCode));
            //}
            /* END DEBUG WRITING */
        }

        ///<summary>
        /// Should be run in transaction scope
        ///</summary>
        ///<param name="routeDataProvider"></param>
        ///<param name="routeCreator"></param>
        ///<param name="groupGuid"></param>
        ///<param name="defLang"></param>
        public void UpdateGroupBasedRouteData(IRouteDataProvider<RouteDataNode> routeDataProvider,
                                              IRouteCreator<RouteDataNode> routeCreator, int groupGuid, string defLang)
        {
            //Prepare for deletion
            var toDelete = new List<int>();
            PrepareDeleteByGuid(groupGuid, defLang, toDelete);
            //Delete
            var flushList = new List<int>(2000);
            for (int i = 0; i < toDelete.Count; i++)
            {
                flushList.Add(toDelete[i]);
                if (flushList.Count == 2000)
                {
                    routeDataProvider.RemoveByGroupGuid(flushList);
                    flushList.Clear();
                }
            }
            if (flushList.Count > 0)
            {
                routeDataProvider.RemoveByGroupGuid(flushList);
            }

            //routeDataProvider.RemoveByGroupGuid(toDelete);
            //Create new
            var dataNodes = new FlushableCollection<RouteDataNode>(routeDataProvider);
            foreach (LanguageTable language in Languages)
            {
                Group topGroup = PrepareBranch(groupGuid, language.LanguageGuid);
                routeCreator.CreateGroupProductRoutes(dataNodes, topGroup, language.LanguageGuid);
            }
            //Add
            routeDataProvider.AddRange(dataNodes);

            /*** DEBUG WRITING ***/
            //foreach (RouteDataNode routeDataNode in dataNodes) {
            //    FileLogger.Log(String.Format("Route: {0}", routeDataNode.RouteUrl));
            //}
            /* END DEBUG WRITING */
        }

        #endregion

        ///<summary>
        ///</summary>
        public List<LanguageTable> Languages { get; private set; }

        // Primitive utility methods
        ///<summary>
        /// Makes sure grp has a "single parent trail" from itself up to the root level
        /// This method connects the (sublevel) group with a parent group, and continues 
        /// to connect that parent group to its parent group and so on up to the root level.
        /// Connects with double binding, parents add group to Children collection, children 
        /// add parent to their Parent reference.
        ///</summary>
        ///<param name="grp"></param>
        ///<param name="languageGuid"></param>
        ///<returns></returns>
        public Group PrepareForCreateNew(Group grp, string languageGuid)
        {
            var dummy = new Group();
            Group topGroup = _catalogTreeUtility.TrickleUp(grp, languageGuid, dummy);
            return topGroup.HasChildren() ? grp.Parent : topGroup;
        }

        ///<summary>
        ///</summary>
        ///<param name="groupGuid"></param>
        ///<param name="languageGuid"></param>
        ///<returns></returns>
        public Group PrepareBranch(int groupGuid, string languageGuid)
        {
            int parentGuid = -1;
            object oParentGuid = _groupRepository.GetById(groupGuid).ParentGuid;
            if (oParentGuid != DBNull.Value)
            {
                parentGuid = (int) oParentGuid;
            }

            Group theRealGroup = _groupRepository.GetGroup(groupGuid, languageGuid, true);
            Group group = _catalogTreeUtility.AllSubGroups(languageGuid, groupGuid);
            theRealGroup.Children.AddRange(group.Children);

            foreach (Group child in group.Children)
            {
                child.Parent = theRealGroup;
                child.ParentGuid = theRealGroup.ID;
            }

            if (parentGuid == 0)
            {
                var parentGroup = new Group
                                      {
                                          GroupGuid = 100,
                                          Parent = null,
                                          Properties = new GrpPropValSet
                                                           {
                                                               PropDict =
                                                                   new Dictionary<string, string>
                                                                       {{"NAME", "DUMMY"}, {"TEMPLATE", ""}}
                                                           }
                                      };
                parentGroup.Children.Add(theRealGroup);
                theRealGroup.Parent = null;
                theRealGroup.ParentGuid = 0;
                return parentGroup;
            }

            var dummy = new Group();
            Group topGroup;
            if (group.HasChildren())
            {
                topGroup = _catalogTreeUtility.TrickleUp(theRealGroup, languageGuid, dummy);
            }
            else
            {
                group = _groupRepository.GetGroup(groupGuid, languageGuid);
                topGroup = PrepareForCreateNew(group, languageGuid);
            }
            return topGroup;
        }

        ///<summary>
        ///</summary>
        ///<param name="groupGuid"></param>
        ///<param name="languageGuid"></param>
        ///<param name="ints"></param>
        ///<returns></returns>
        public Group PrepareDeleteByGuid(int groupGuid, string languageGuid, List<int> ints)
        {
            Group group = PrepareBranch(groupGuid, languageGuid);
            GroupTrail2Guid(group, ints);
            return group;
        }

        ///<summary>
        ///</summary>
        ///<param name="group"></param>
        ///<param name="guidList"></param>
        public void GroupTrail2Guid(Group group, List<int> guidList)
        {
            foreach (Group grp in group.Children)
            {
                guidList.Add(grp.GroupGuid);
                if (grp.HasChildren())
                {
                    GroupTrail2Guid(grp, guidList);
                }
            }
        }
    }
}