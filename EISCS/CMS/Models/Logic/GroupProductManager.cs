﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web.Mvc;
using CmsPublic.DataProviders.Logging;
using CmsPublic.ModelLayer.Models;
using CmsPublic.Utilities;
using EISCS.CMS.Services.AppSettings;
using EISCS.Routing.Concrete;
using EISCS.Routing.Interfaces;
using EISCS.Routing.Providers;
using EISCS.Shop.DO.Dto;
using EISCS.Shop.DO.Interface;


namespace EISCS.CMS.Models.Logic
{
    ///<summary>
    ///</summary>
    public class GroupProductManager
    {
        private readonly IGroupRepository _gp = DependencyResolver.Current.GetService<IGroupRepository>();
        private readonly IGroupProductRepository _gpp = DependencyResolver.Current.GetService<IGroupProductRepository>();
        private readonly ILanguageRepository _languageProvider = DependencyResolver.Current.GetService<ILanguageRepository>();
        private readonly ManagementUtility _managementutility;
        private readonly IProductRepository _pr = DependencyResolver.Current.GetService<IProductRepository>();
        private readonly IPropertiesDataService _propertiesDataService = DependencyResolver.Current.GetService<IPropertiesDataService>();    
        private readonly IRouteCreator<RouteDataNode> _routeCreator = RouteManager.Route.PublicRouteCreator;

        private readonly IRouteDataProvider<RouteDataNode> _routeDataProvider =
            RouteManager.Route.PublicRouteDataProvider;


        public GroupProductManager(IGroupRepository groupRepository, IProductRepository productRepository, IGroupProductRepository groupProductRepository) 
        {
            
            if (groupRepository != null )
            {
                _gp = groupRepository;
            }

            if (groupProductRepository!= null)
            {
                _gpp = groupProductRepository;
            }

            if( productRepository != null)
            {
                _pr = productRepository;
            }
            _managementutility = new ManagementUtility(_gp, _pr);
            DefaultLanguage = AppSettingsReader.Read("LANGUAGE_DEFAULT");
        }

        #region BaseRepository

        ///<summary>
        ///</summary>
        ///<param name="propOwnerRefGuid"></param>
        ///<param name="propGuid"></param>
        ///<param name="languageGuid"></param>
        public void ConvertFromMultiLanguage(string propOwnerRefGuid, string propGuid, string languageGuid)
        {
            /*
             * USING TRANSACTION SCOPE
             */

            try
            {
                using (var scope = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                {
                    //Do usual stuff
                    _gp.ConvertFromMultiLanguage(propOwnerRefGuid, propGuid, languageGuid);

                    //ROUTE MANAGEMENT
                    bool isGroup = false;
                    int groupGuid;
                    if (int.TryParse(propOwnerRefGuid.Trim(), out groupGuid))
                    {
                        isGroup = _gp.GetById(groupGuid) != null;
                    }
                    if (isGroup)
                    {
                        _managementutility.UpdateGroupBasedRouteData(_routeDataProvider, _routeCreator, groupGuid,
                                                                     DefaultLanguage);
                    }
                    else
                    {
                        // This is a product. Must update productLinks and GroupProductLinks
                        List<LanguageTable> languages = _languageProvider.LoadLanguages();
                        List<GroupProduct> groupProducts = _gpp.GetGroupProducts(propOwnerRefGuid);
                        foreach (LanguageTable language in languages)
                        {
                            _managementutility.UpdateProductBasedRouteData(_routeDataProvider, _routeCreator,
                                                                           propOwnerRefGuid,
                                                                           language.LanguageGuid, groupProducts);
                        }
                    }
                    scope.Complete();
                }
              
            }
            catch (Exception ex)
            {
                FileLogger.Log(ex.Message);
            }
        }

        ///<summary>
        ///</summary>
        ///<param name="propOwnerRefGuid"></param>
        ///<param name="propGuid"></param>
        ///<param name="languageGuid"></param>
        ///<param name="allLanguages"></param>
        public void ConvertToMultiLanguage(string propOwnerRefGuid, string propGuid, string languageGuid,
                                           List<LanguageTable> allLanguages)
        {
            /*
             * USING TRANSACTION SCOPE
             */

            try
            {
                using (var scope = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                {
                    //Do usual stuff
                    _gp.ConvertToMultiLanguage(propOwnerRefGuid, propGuid, languageGuid, allLanguages);

                    //ROUTE MANAGEMENT
                    bool isGroup = false;
                    int groupGuid;
                    if (int.TryParse(propOwnerRefGuid.Trim(), out groupGuid))
                    {
                        isGroup = _gp.GetById(groupGuid) != null;
                    }
                    if (isGroup)
                    {
                        _managementutility.UpdateGroupBasedRouteData(_routeDataProvider, _routeCreator, groupGuid,
                                                                     DefaultLanguage);
                    }
                    else
                    {
                        // This is a product. Must update productLinks and GroupProductLinks
                        List<LanguageTable> languages = _languageProvider.LoadLanguages();
                        List<GroupProduct> groupProducts = _gpp.GetGroupProducts(propOwnerRefGuid);
                        foreach (LanguageTable language in languages)
                        {
                            _managementutility.UpdateProductBasedRouteData(_routeDataProvider, _routeCreator,
                                                                           propOwnerRefGuid,
                                                                           language.LanguageGuid, groupProducts);
                        }
                    }
                    scope.Complete();
                }
               
            }
            catch (Exception ex)
            {
                FileLogger.Log(ex.Message);
            }
        }

        ///<summary>
        ///</summary>
        ///<param name="filter"></param>
        ///<returns></returns>
        public List<TemplateQuerySet> TemplateTable(string filter)
        {
            List<TemplateQuerySet> templates = _gp.TemplateTable(filter);
            return templates;
        }

        ///<summary>
        /// Retrieves all links from the RouteTable
        ///</summary>
        ///<param name="languageGuid"></param>
        ///<returns></returns>
        public List<string> GetInternalLinkUrls(string languageGuid)
        {
            return _routeDataProvider.GetInternalLinkUrls(languageGuid);
        }

        #endregion

        #region GroupRepository

        /// <summary>
        /// Get a list of Group Objects based on a collection of GroupGuid's
        /// </summary>
        /// <param name="groupGuids"></param>
        /// <param name="languageGuid"></param>
        /// <param name="isLoadProperties"></param>
        /// <returns></returns>
        public List<Group> GetGroupsFromGroupGuids(List<int> groupGuids, string languageGuid, bool isLoadProperties)
        {
            return _gp.GetGroupsFromGroupGuids(groupGuids, languageGuid, isLoadProperties);
        }

        /// <summary>
        /// Get a list of all Group Objects 
        /// </summary>
        /// <param name="languageGuid"></param>
        /// <param name="isLoadProperties"></param>
        /// <returns></returns>
        public List<Group> GetAllGroups(string languageGuid, bool isLoadProperties)
        {
            return _gp.GetAllGroups(languageGuid, isLoadProperties);
        }

        /// <summary>
        /// Get a list of Group Objects descending from a parent Group Object
        /// </summary>
        /// <param name="languageGuid"></param>
        /// <param name="groupGuid"></param>
        /// <param name="isLoadProperties"></param>
        /// <returns></returns>
        public List<Group> GetSubGroups(string languageGuid, int groupGuid, bool isLoadProperties)
        {
            return _gp.GetSubGroups(languageGuid, groupGuid, isLoadProperties);
        }

        /// <summary>
        /// Get a list of Group Objects at the top level
        /// </summary>
        /// <param name="languageGuid"></param>
        /// <param name="isLoadProperties"></param>
        /// <returns></returns>
        public List<Group> GetTopGroups(string languageGuid, bool isLoadProperties)
        {
            return _gp.GetTopGroups(languageGuid, isLoadProperties);
        }

 
        ///<summary>
        ///</summary>
        ///<param name="groupId"></param>
        ///<param name="languageGuid"></param>
        ///<returns></returns>
        public Group GetGroupForEditor(int groupId, string languageGuid)
        {
            return _gp.GetGroupForEditor(groupId, languageGuid);
        }

        ///<summary>
        ///</summary>
        ///<param name="guid"></param>
        ///<param name="languageGuid"></param>
        ///<returns></returns>
        public Group GetGroup(int guid, string languageGuid)
        {
            return _gp.GetGroup(guid, languageGuid, true);
        }

        /*
         * DONE 
         * 
         */

        ///<summary>
        ///</summary>
        ///<param name="productGuids"></param>
        ///<param name="groupGuid"></param>
        public void AddProductsToGroup(string[] productGuids, string groupGuid)
        {
            /*
             * USING TRANSACTION SCOPE
             * 
             */

            try
            {
                using (var scope = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                {
                    // do usual stuff
                    _gpp.AddProductsToGroup(productGuids, groupGuid);

                    // Route stuff
                    int igroupGuid = int.Parse(groupGuid);
                    foreach (LanguageTable language in _languageProvider.LoadLanguages())
                    {
                        string languageGuid = language.LanguageGuid;
                        Group grp = _gp.GetGroup(igroupGuid, languageGuid, true);
                        _managementutility.PrepareForCreateNew(grp, languageGuid);
                        var routeDataNodes = new List<RouteDataNode>();
                        List<Product> products = _pr.GetProducts(productGuids, languageGuid);
                        foreach (Product product in products)
                        {
                            _routeCreator.CreateGroupProductRoutes(routeDataNodes, grp, product, languageGuid);
                        }

                        _routeDataProvider.AddRange(routeDataNodes);
                    }
                    scope.Complete();
                }
              
            }
            catch (Exception ex)
            {
                FileLogger.Log(ex.Message);
            }
        }

        /*
         * DONE 
         * 
         */

        ///<summary>
        ///</summary>
        ///<param name="groupProductGuids"></param>
        ///<param name="groupGuid"></param>
        public void DeleteProductsFromGroup(string[] groupProductGuids, string groupGuid)
        {
            //FileLogger.Log("DeleteProductsFromGroup");
            /*
             * USING TRANSACTION SCOPE
             * 
             * Delete from GroupProduct where GroupProductGuid = @groupProductGuid
             * 
             */
            List<GroupProduct> routeDataNodes = null;
            
            var values = groupProductGuids.ToList();

            try
            {
                routeDataNodes = _gpp.GetGroupProducts(values);
            }
            catch (Exception ex)
            {
                FileLogger.Log(string.Format("Message: {0}, Trace{1}", ex.Message, ex.StackTrace));
            }

            try
            {
                using (var scope = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                {
                    // do usual stuff
                    int groupId = int.Parse(groupGuid);
                    _gpp.DeleteProductsFromGroup(groupProductGuids, groupId);

                    // delete from Routes
         
                    if (routeDataNodes != null)
                    {
                        _routeDataProvider.RemoveByGroupGuidProductGuid(routeDataNodes);
                    }

                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                FileLogger.Log(ex.Message);
            }
        }

        ///<summary>
        ///</summary>
        ///<param name="currentItem"></param>
        ///<param name="nextItem"></param>
        ///<param name="prevItem"></param>
        ///<param name="groupGuid"></param>
        public void ChangeGroupProductSortOrder(string currentItem, string nextItem, string prevItem, string groupGuid)
        {
            _gpp.ChangeGroupProductSortOrder(currentItem, nextItem, prevItem, groupGuid);
        }

        ///<summary>
        ///</summary>
        ///<returns></returns>
        public int GetHighestRootGroupGuid()
        {
            return _gp.GetHighestRootGroupGuid();
        }

        ///<summary>
        ///</summary>
        ///<param name="languageGuid"></param>
        ///<returns></returns>
        public int EnsureParentGroup(string languageGuid)
        {
            return _gp.EnsureParentGroup(languageGuid);
        }

        ///<summary>
        ///</summary>
        ///<param name="languageGuid"></param>
        ///<param name="isLoadProperties"></param>
        ///<returns></returns>
        public List<Group> GetRootGroupChildren(string languageGuid, bool isLoadProperties)
        {
            return _gp.GetRootGroupChildren(languageGuid, isLoadProperties);
        }

        ///<summary>
        ///</summary>
        ///<returns></returns>
        public object CatalogLevels()
        {
            return _gp.CatalogLevels();
        }

        #region COMPLETED CHANGES FOR ROUTE MANAGEMENT

        ///<summary>
        ///</summary>
        ///<param name="g"></param>
        ///<param name="languageGuid"></param>
        public void SaveProperties(Group g, string languageGuid)
        {
            /*
             * USING TRANSACTION SCOPE
             */

            try
            {
                using (var scope = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                {
                    // Do usual stuff
                    _gp.SaveProperties(g, languageGuid);

                
                // and take care of RouteTable management
                    _managementutility.UpdateGroupBasedRouteData(_routeDataProvider, _routeCreator, g.ID,
                                                                     DefaultLanguage);
                    scope.Complete();
                }
               
            }
            catch (Exception ex)
            {
                FileLogger.Log(ex.Message);
            }
        }

        /// <summary>
        /// Drop child group on parent group. Child becomes a new sub group of the parent.
        /// </summary>
        /// <param name="groupGuid"></param>
        /// <param name="parentGuid"></param>
        public int RegroupGroupTable(int groupGuid, int parentGuid)
        {
            /*
             * USING TRANSACTION SCOPE 
             */

            //FileLogger.Log("RegroupGroupTable");

            var s = new StopWatch();
            s.Start();

            string defLang = DefaultLanguage;
            int iAffected = 0;

            try
            {
                using (var scope = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                {
                    // Do usual stuff
                    _gp.RegroupGroupTable(groupGuid, parentGuid);
              
                    // Update Routes
                    _managementutility.UpdateGroupBasedRouteData(_routeDataProvider, _routeCreator, groupGuid,
                                                                    defLang);
                    scope.Complete();
                }

            }
            catch (Exception ex)
            {
                FileLogger.Log(ex.Message);
            }
            //FileLogger.Log("Moving group. Elapsed time in milliseconds: " + s.GetElapsedTime());
            s.Stop();
            return iAffected;
        }

        /// <summary>
        /// Give the Group a new ParentGuid and a new SortIndex
        /// </summary>
        /// <param name="groupGuid"></param>
        /// <param name="parentGuid"></param>
        /// <param name="siblingGuid"></param>
        public void RearrangeGroupOrder(int groupGuid, int parentGuid, int siblingGuid)
        {
            //FileLogger.Log("RearrangeGroupOrder");
            /* REFACTORING FOR ROUTE MANAGEMENT
             * 
             * COMMENTS
             * 
             * Call ends up at "MoveGroup"in GroupProvider.
             * 
             * MOVES an EXISTING Group inside the "tree" changes SORT INDEX. AND between subtrees as well!. Affects all sub "trees" including products.
             * Must also look upwards to the Root level to get the correct full path.
             * If ParentGuid stays the same then nothing needs to be done here.
             * 
             */

            /*
             * USING TRANSACTION SCOPE
             */

            string defLang = DefaultLanguage;
            
            try
            {
                int oldParentGuid;
                using (var scope = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                {
                    //Find "old" ParentGuid
                    oldParentGuid = _gp.GetById(groupGuid).ParentGuid;


                    // Do usual stuff
                    _gp.RearrangeGroupOrder(groupGuid, parentGuid, siblingGuid);
                    
                    //}
                    //FileLogger.Log(iAffected);
                    // If move was successful...

                    if (oldParentGuid != parentGuid)
                    {
                        // Update Routes
                        _managementutility.UpdateGroupBasedRouteData(_routeDataProvider, _routeCreator,
                            groupGuid, defLang);
                    }
                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                FileLogger.Log(ex.Message);
            }
        }


        /* REFACTORING FOR ROUTE MANAGEMENT
         * 
         * COMMENTS
         * 
         * Call ends up at CreateGroup in GroupProvider.
         *         
         * */

        ///<summary>
        /// Inserts a new Group in the GroupTable
        /// The new GroupGuid will be the current hihgest GroupGuid + 1
        ///</summary>
        ///<param name="parentGuid"></param>
        ///<param name="siblingGuid"></param>
        ///<returns>The GroupGuid of the newly inserted Group</returns>
        public int CreateNewGroup(int parentGuid, int siblingGuid)
        {
            /*
             * USING TRANSACTION SCOPE
             */

            //FileLogger.Log("CreateNewGroup");
            int newGroupGuid = -100;

            try
            {
                using (var scope = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                {
                    // Do usual stuff
                    newGroupGuid = _gp.CreateNewGroup(parentGuid, siblingGuid);
                    

                    // Do Route Management

                    // 1. Get parent trail with properties loaded - from root down to current parent
                    // 2. Add current group to parent
                    if (newGroupGuid > 0)
                    {
                        Group grp = _gp.GetGroup(newGroupGuid, DefaultLanguage, true);

                        // Let the grp variable contain parent(s) up to root level

                        List<LanguageTable> languages = _languageProvider.LoadLanguages();
                        var dataNodes = new List<RouteDataNode>();

                        foreach (LanguageTable language in languages)
                        {
                            Group topGroup = _managementutility.PrepareForCreateNew(grp, language.LanguageGuid);
                            _routeCreator.CreateGroupProductRoutes(dataNodes, topGroup, language.LanguageGuid);
                        }

                        _routeDataProvider.AddRange(dataNodes);
                    }

                    scope.Complete();
                }
                
            }
            catch (Exception ex)
            {
                FileLogger.Log(ex.Message);
            }
            return newGroupGuid;
        }

        // TODO Fix this to use TransactionScope
        public int InsertGroup(int groupGuid, int parentGuid, int siblingGroupGuid)
        {
            return _gp.InsertGroup(groupGuid, parentGuid, siblingGroupGuid);
        }

        ///<summary>
        /// Delete Group and Sub Groups
        ///</summary>
        ///<param name="groupGuid"></param>
        ///<param name="languageGuid"></param>
        ///<returns></returns>
        public int DeleteGroup(int groupGuid, string languageGuid)
        {
            /*
             * USING TRANSACTION SCOPE
             */

            int parentGuid = -100;
            try
            {
                using (var scope = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                {
                    //Prepare for deletion
                    var toDelete = new List<int>();

                    _managementutility.PrepareDeleteByGuid(groupGuid, DefaultLanguage, toDelete);

                    //Do usual stuff
                    parentGuid = _gp.DeleteGroup(groupGuid, languageGuid);

                    //Delete
                    _routeDataProvider.RemoveByGroupGuid(toDelete);

                   scope.Complete();
                }
            }
            catch (Exception ex)
            {
                FileLogger.Log(string.Format("Message: {0}\n{1}", ex.Message, ex.StackTrace));
            }
            return parentGuid;
        }

        #endregion

        #endregion

        #region ProductRepository

        /// <summary>
        /// Get all products within a defined range
        /// </summary>
        /// <param name="startIndex">Range start</param>
        /// <param name="endIndex">Range end</param>
        /// <param name="type"></param>
        /// <param name="filter"></param>
        /// <param name="languageGuid"></param>
        /// <returns></returns>
        public List<Product> GetProductRange(int startIndex, int endIndex, string type, string filter,
                                             string languageGuid)
        {
            return _pr.GetProductRange(startIndex, endIndex, false, filter, languageGuid, null, null);
        }

        public List<Product> GetProductRange(int startIndex, int endIndex, bool isLoadProperties, string type, string filter,
                                             string languageGuid)
        {
            return _pr.GetProductRange(startIndex, endIndex, isLoadProperties, filter, languageGuid, null, null);
        }

        public List<Product> GetGroupProductsRange(int startIndex, int endIndex, bool isLoadProperties, string languageGuid,
                                              string filter, int groupGuid)
        {
            return _pr.GetGroupProductsRange(startIndex, endIndex, isLoadProperties, languageGuid, filter,
                                             groupGuid);
        }

        /// <summary>
        /// Get the number of products by filter type
        /// </summary>
        /// <param name="type"></param>
        /// <param name="filter"></param>
        /// <returns></returns>
        public int GetProductsCount(string type, string filter)
        {
            return _pr.GetProductsCount(type, filter);
        }


        /// <summary>
        /// Load properties for a single product
        /// </summary>
        /// <param name="guid"></param>
        /// <param name="languageGuid"></param>
        /// <returns></returns>
        public Product SingleProduct(string guid, string languageGuid)
        {
            return _pr.SingleProduct(guid, languageGuid, true);
        }

        /// <summary>
        /// Load product with a collection of properties
        /// </summary>
        /// <param name="guid"></param>
        /// <param name="languageGuid"></param>
        /// <returns></returns>
        public Product SingleProductAnonymousCollection(string guid, string languageGuid)
        {
            return _pr.SingleProductAnonymousCollection(guid, languageGuid);
        }

        /// <summary>
        /// Get all Products from the specified Group
        /// </summary>
        /// <param name="groupGuid"></param>
        /// <param name="languageGuid"></param>
        /// <param name="isLoadProperties"></param>
        /// <returns></returns>
        public List<GroupProduct> GroupProducts(string groupGuid, string languageGuid, bool isLoadProperties)
        {
            return _pr.GroupProducts(groupGuid, languageGuid, isLoadProperties);
        }

        /*
         * Done 
         * 
         */

        /// <summary>
        /// Save product properties by language
        /// </summary>
        /// <param name="p"></param>
        /// <param name="languageGuid"></param>
        public void SaveProperties(Product p, string languageGuid)
        {
            /*
             * USING TRANSACTION SCOPE
             * 
             */
            try
            {
                using (var scope = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                {
                    // Do usual stuff
                    _propertiesDataService.SaveProperties(p.IProperties, languageGuid);
                    
                    
                    // Do route management

                    List<LanguageTable> languages = _languageProvider.LoadLanguages();
                    List<GroupProduct> groupProducts = _gpp.GetGroupProducts(p.ProductGuid);
                    foreach (LanguageTable language in languages)
                    {
                        _managementutility.UpdateProductBasedRouteData(_routeDataProvider, _routeCreator,
                                                                       p.ProductGuid, language.LanguageGuid, groupProducts);
                    }

                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                FileLogger.Log(ex.Message);
            }
        }

        /// <summary>
        /// Get all products
        /// </summary>
        /// <param name="languageGuid"></param>
        /// <param name="isLoadProperties"></param>
        /// <returns></returns>
        public List<Product> GetAllProducts(string languageGuid, bool isLoadProperties)
        {
            return _pr.GetAllProducts(languageGuid, true);
        }

        #endregion

        ///<summary>
        /// Set default language
        ///</summary>
        public string DefaultLanguage { get; set; }

        /* Intercept calls to repositories here */

        #region IGroupRepository Members

        /// <summary>
        /// 
        /// </summary>
        /// <param name="guid"></param>
        /// <param name="languageGuid"></param>
        /// <param name="isLoadProperties"></param>
        /// <returns></returns>
        public Group GetGroup(int guid, string languageGuid, bool isLoadProperties)
        {
            return _gp.GetGroup(guid, languageGuid, isLoadProperties);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="templateName"></param>
        /// <returns></returns>
        public List<string> GetPropertyGuidsIncludedInTemplate(string templateName)
        {
            return _gp.GetPropertyGuidsIncludedInTemplate(templateName);
        }

        ///<summary>
        ///</summary>
        ///<param name="templateFileName"></param>
        ///<returns></returns>
        public List<TemplateProperty> GetTemplateProperties(string templateFileName)
        {
            return _gp.GetTemplateProperties(templateFileName);
        }

        #endregion

        public int GetGroupProductsCount(int groupGuid)
        {
            return _pr.GetGroupProductsCount(groupGuid);
        }

        public Group GetGroupByPropGuidAndPropTransText(string propGuid, string propTransText, string languageGuid)
        {
            return _gp.GetGroupByPropGuidAndPropTransText(propGuid, propTransText, languageGuid);
        }
    }
}