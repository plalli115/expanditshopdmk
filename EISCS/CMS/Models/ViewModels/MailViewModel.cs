﻿using System.Collections.Generic;
using CmsPublic.ModelLayer.Models;

namespace EISCS.CMS.Models.ViewModels
{
    ///<summary>
    ///</summary>
    public class MailViewModel
    {
        /// <summary>
        /// </summary>
        /// <param name="mailmessage"></param>
        /// <param name="mailMessages"></param>
        /// <param name="mailProducts"></param>
        public MailViewModel(ExpanditMailMessage mailmessage, List<ExpanditMailMessage> mailMessages, List<Product> mailProducts)
        {
            MailMessage = mailmessage;
            MailMessages = mailMessages;
            MailProducts = mailProducts;
        }

        ///<summary>
        ///</summary>
        public ExpanditMailMessage MailMessage { get; private set; }

        ///<summary>
        ///</summary>
        public List<ExpanditMailMessage> MailMessages { get; private set; }

        public List<Product> MailProducts { get; private set; }
    }
}