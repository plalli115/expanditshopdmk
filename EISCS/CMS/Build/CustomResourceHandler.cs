﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;

namespace EISCS.CMS.Build
{
    ///<summary>
    ///</summary>
    public class CustomResourceHandler
    {
        private const string Fallback = "en";
        private static readonly string[] SupportedLanguages = {"da", "de", "en", "es", "fr", "nl", "pt", "sv"};
        private static Dictionary<string, List<LanguageStringNode>> _languageDict;

        static CustomResourceHandler()
        {
            _languageDict = CreateLanguageLists();
        }

        ///<summary>
        ///</summary>
        ///<param name="key"></param>
        ///<param name="ci"></param>
        ///<returns></returns>
        public static string GetString(string key, CultureInfo ci)
        {
            return GetValue(key, ci);
        }

        public static string GetValue(string key)
        {
            CultureInfo ci = Thread.CurrentThread.CurrentUICulture;
            return GetValue(key, ci);
        }

        private static string GetValue(string key, CultureInfo ci)
        {
            string returnValue = key; // Return key if nothing is found
            if (_languageDict == null)
            {
                _languageDict = CreateLanguageLists();
            }
            try
            {
                string languageGuid = ci.TwoLetterISOLanguageName;
                List<LanguageStringNode> list;
                if (!string.IsNullOrEmpty(languageGuid) && SupportedLanguages.Contains(languageGuid))
                {
                    list = _languageDict[languageGuid];
                }
                else
                {
                    list = _languageDict[Fallback];
                }
                LanguageStringNode node = list.Find(k => k.Key.Equals(key));
                returnValue = node.Val;
            }
            catch
            {
                LogError();
            }
            return returnValue;
        }

        private static void LogError()
        {
            
        }

        #region languageLists

        private static Dictionary<string, List<LanguageStringNode>> CreateLanguageLists()
        {
            var languageDict = new Dictionary<string, List<LanguageStringNode>>();

            var danishList = new List<LanguageStringNode>
                                 {
                                     new LanguageStringNode
                                         {
                                             Key = "CATMAN_CONFIRM_ML_QUESTION",
                                             Val =
                                                 "Du har ændret egenskaben '%REPLACE%' til flersproget. Ønsker du at kopiere den nuværende værdi til alle sprog?"
                                         },
                                     new LanguageStringNode
                                         {
                                             Key = "CATMAN_CONFIRM_ML_WARNING",
                                             Val =
                                                 "Du har valgt at ændre egenskaben '%REPLACE%' fra flersproget til enkeltsproget. Dette vil slætte alle de oversatte værdier for denne egenskap. Kun værdien fra de aktuelle sprog vil blive gemt.\n\nØnsker du att fortsætte?"
                                         },
                                     new LanguageStringNode
                                         {Key = "CATMAN_ACTION_ADD_SELECTED", Val = "Tilføj"},
                                     new LanguageStringNode
                                         {
                                             Key = "CATMAN_ACTION_BOTH_UPLOAD_AND_DOWNLOAD",
                                             Val = "Både hent og send data fra hjemmeside"
                                         },
                                     new LanguageStringNode
                                         {Key = "CATMAN_ACTION_DELETE", Val = "Slet"},
                                     new LanguageStringNode
                                         {
                                             Key = "CATMAN_ACTION_DOWNLOAD",
                                             Val = "Hent data fra hjemmeside"
                                         },
                                     new LanguageStringNode
                                         {
                                             Key = "CATMAN_ACTION_EXTRACT",
                                             Val = "Eksporter fra økonomisystemet"
                                         },
                                     new LanguageStringNode
                                         {
                                             Key = "CATMAN_ACTION_INJECT",
                                             Val = "Importer til økonomisystemet"
                                         },
                                     new LanguageStringNode {Key = "CATMAN_ACTION_NEW", Val = "Ny"},
                                     new LanguageStringNode
                                         {Key = "CATMAN_ACTION_REMOVE_SELECTED", Val = "Slet"},
                                     new LanguageStringNode
                                         {Key = "CATMAN_ACTION_SAVE", Val = "Gem"},
                                     new LanguageStringNode
                                         {Key = "CATMAN_ACTION_SELECT", Val = "Vælg"},
                                     new LanguageStringNode
                                         {
                                             Key = "CATMAN_ACTION_UPLOAD",
                                             Val = "Send data til hjemmeside"
                                         },
                                     new LanguageStringNode
                                         {
                                             Key = "CATMAN_FULL_SYNCHRONIZATION",
                                             Val = "Fuld synkronisering"
                                         },
                                     new LanguageStringNode
                                         {Key = "CATMAN_LABEL_CATALOG", Val = "Katalog"},
                                     new LanguageStringNode
                                         {
                                             Key = "CATMAN_LABEL_CATALOG_LANGUAGE",
                                             Val = "Katalogsprog"
                                         },
                                     new LanguageStringNode
                                         {Key = "CATMAN_LABEL_DRAGMODE", Val = "Sortering"},
                                     new LanguageStringNode {Key = "CATMAN_LABEL_ML", Val = "FS"},
                                     new LanguageStringNode
                                         {Key = "CATMAN_LABEL_PREVIEW", Val = "Vis"},
                                     new LanguageStringNode
                                         {Key = "CATMAN_LABEL_PRODUCT_LIST", Val = "Produkter"},
                                     new LanguageStringNode
                                         {Key = "CATMAN_LABEL_PROPERTY", Val = "Egenskap"},
                                     new LanguageStringNode
                                         {Key = "CATMAN_LABEL_SELECT_LANGUAGE", Val = "Vælg sprog"},
                                     new LanguageStringNode
                                         {Key = "CATMAN_LABEL_SYNCHRONIZE", Val = "Synkroniser"},
                                     new LanguageStringNode
                                         {
                                             Key = "CATMAN_LABEL_UNKNOWN_PROPERTY",
                                             Val = "Mangler egenskap"
                                         },
                                     new LanguageStringNode
                                         {Key = "CATMAN_LABEL_VALUE", Val = "Værdi"},
                                     new LanguageStringNode
                                         {Key = "CATMAN_LINK_HTML_EDITOR", Val = "HTML-Editor"},
                                     new LanguageStringNode
                                         {Key = "CATMAN_DROPDOWN_NAME", Val = "Navn"},
                                     new LanguageStringNode
                                         {Key = "CATMAN_DROPDOWN_NUMBER", Val = "Nummer"},
                                     new LanguageStringNode
                                         {Key = "CATMAN_SELECT_FILE", Val = "Vælg fil"},
                                     new LanguageStringNode
                                         {Key = "CATMAN_SELECT_IMAGE", Val = "Vælg billede"},
                                     new LanguageStringNode
                                         {Key = "CATMAN_LABEL_BEGINS_WITH", Val = "Begynder med:"},
                                     new LanguageStringNode
                                         {
                                             Key = "CATMAN_LABEL_HANDLE_IMAGES",
                                             Val = "Håndtere billeder"
                                         },
                                     new LanguageStringNode
                                         {
                                             Key = "CATMAN_LABEL_HANDLE_ATTACHMENTS",
                                             Val = "Håndtere vedhæftelser"
                                         },
                                     new LanguageStringNode
                                         {
                                             Key = "CATMAN_LABEL_SPECIFY_FILE_TO_UPLOAD",
                                             Val = "Vælg en fil på harddisken"
                                         },
                                     new LanguageStringNode
                                         {Key = "CATMAN_LABEL_HELP", Val = "Hjælp"},
                                     new LanguageStringNode
                                         {
                                             Key = "CATMAN_DOCUMENTUPLOAD_WARNING_MESSAGE",
                                             Val = "Kun doc, docx, pdf, txt and zip filer er tilladte"
                                         },
                                     new LanguageStringNode
                                         {
                                             Key = "CATMAN_IMAGEUPLOAD_WARNING_MESSAGE",
                                             Val = "Kun jpg, png, jpeg, bmp and gif filer er tilladte"
                                         },
                                     new LanguageStringNode
                                         {Key = "CATMAN_BUTTONTEXT_BROWSE", Val = "søg fil"},
                                     new LanguageStringNode
                                         {Key = "CATMAN_BUTTONTEXT_SEND", Val = "Send"},
                                     new LanguageStringNode
                                         {Key = "CATMAN_BUTTONTEXT_CANCEL", Val = "Fortryd"},
                                     new LanguageStringNode
                                         {Key = "CATMAN_LABEL_SIZE", Val = "Størrelse"},
                                     new LanguageStringNode
                                         {Key = "CATMAN_LABEL_DIMENSIONS", Val = "Dimension"},
                                     new LanguageStringNode
                                         {Key = "CATMAN_LABEL_ALLSAVED", Val = "Alt gemt"},
                                     new LanguageStringNode
                                         {
                                             Key = "CATMAN_LABEL_ERROR_STARTING_JOB",
                                             Val =
                                                 "Fejl! Skrivrettigheder før katalogen App_Data er påkrævet."
                                         },
                                     new LanguageStringNode
                                         {
                                             Key = "CATMAN_LABEL_MISSING_WRITE_PERMISSION",
                                             Val =
                                                 "Brugeren \"IUSER\" mangler skrivrettigheder før destinationskatalogen"
                                         },
                                     new LanguageStringNode
                                         {
                                             Key = "CATMAN_LABEL_ANOTHER_JOB_RUNNING",
                                             Val = "Ett anded job er allerede igang"
                                         },
                                     new LanguageStringNode
                                         {Key = "CATMAN_LABEL_UPLOADING", Val = "Sender"},
                                     new LanguageStringNode
                                         {
                                             Key = "CATMAN_LABEL_FILE_UPLOADED",
                                             Val = "er blevet sendt"
                                         },
                                     new LanguageStringNode
                                         {
                                             Key = "CATMAN_LABEL_AN_ERROR_OCCURRED",
                                             Val = "Der er sket en fejl"
                                         },
                                     new LanguageStringNode
                                         {
                                             Key = "CATMAN_LABEL_SYNC_ERROR_RESET",
                                             Val = "Reset sync files?"
                                         },
                                     new LanguageStringNode
                                         {
                                             Key = "CATMAN_LABEL_SYNC_ERROR_NONSTARTER_RESET",
                                             Val =
                                                 "You did not start this job. Reset the sync files anyway?"
                                         },
                                     new LanguageStringNode
                                         {Key = "CATMAN_ACTION_RESET", Val = "Reset"},
                                     new LanguageStringNode
                                         {
                                             Key = "CATMAN_CONFIRM_UNSAVED_DATA_MESSAGE",
                                             Val =
                                                 "Dine ændringer er ikke gemt. Hvis du fortsætter vil dine ændringer blive tabt. Ønsker du at fortsætte?"
                                         },
                                     new LanguageStringNode
                                         {
                                             Key = "CATMAN_LABEL_UNSAVED_DATA",
                                             Val = "Ændringer er ikke gemt"
                                         },
                                     new LanguageStringNode
                                         {
                                             Key = "CATMAN_SYNC_FIRST_RUN_WARNING",
                                             Val =
                                                 "Første kørsel af FTPTransport kan ikke udføres fra Catalog Manager.\nI stedet skal du starte FTPTransport fra Start menuen på ShopLocal maskinen, og klikke på Send All knappen."
                                         },
                                     new LanguageStringNode
                                         {
                                             Key = "CATMAN_ACTION_CREATE_ROUTES",
                                             Val = "Genopret ruter"
                                         },
                                     new LanguageStringNode
                                         {
                                             Key = "CATMAN_LABEL_CREATING_ROUTES",
                                             Val = "Opretter ruter"
                                         },
                                     new LanguageStringNode
                                         {Key = "CATMAN_ACTION_DONE", Val = "Klar"},
                                     new LanguageStringNode
                                         {Key = "CATMAN_TOPMENU_ADMIN", Val = "Konto"},
                                     new LanguageStringNode
                                         {Key = "CATMAN_MENU_ADMIN", Val = "User Administration"},
                                     new LanguageStringNode
                                         {
                                             Key = "CATMAN_MENU_CHANGE_PASSWORD",
                                             Val = "Change Password"
                                         },
                                     new LanguageStringNode
                                         {Key = "CATMAN_MENU_LOGOUT", Val = "Log af"},
                                     new LanguageStringNode
                                         {Key = "CATMAN_CONFIRM_DELETE", Val = "Er du sikker?"},
                                     new LanguageStringNode
                                         {Key = "CATMAN_USED_IN_CATALOG", Val = "I katalogen"},
                                     new LanguageStringNode {Key = "CATMAN_YES", Val = "Ja"}
                                 };

            languageDict.Add("da", danishList);

            var germanList = new List<LanguageStringNode>
                                 {
                                     new LanguageStringNode
                                         {
                                             Key = "CATMAN_CONFIRM_ML_QUESTION",
                                             Val =
                                                 "Dieser Vorgang ändert die Eigenschaften von '%REPLACE%' in Mehrsprachigkeit. Möchten Sie den aktuellen Wert in alle Katalogsprachen kopieren?"
                                         },
                                     new LanguageStringNode
                                         {
                                             Key = "CATMAN_CONFIRM_ML_WARNING",
                                             Val =
                                                 "Sie sind dabei, die Mehrsprachigkeit für die Eigenschaft '%REPLACE%' zu deaktivieren. Dieser Vorgang wird alle übersetzten Werte für diese Eigenschaft dauerhaft löschen. Nur der Wert der aktuellen Sprache wird erhalten bleiben.\n\nMöchten Sie fortfahren?"
                                         },
                                     new LanguageStringNode {Key = "CATMAN_ACTION_ADD_SELECTED", Val = "Hinzufügen"},
                                     new LanguageStringNode
                                         {
                                             Key = "CATMAN_ACTION_BOTH_UPLOAD_AND_DOWNLOAD",
                                             Val = "Beides Upload und Download von der Web-Seite"
                                         },
                                     new LanguageStringNode {Key = "CATMAN_ACTION_DELETE", Val = "Löschen"},
                                     new LanguageStringNode
                                         {Key = "CATMAN_ACTION_DOWNLOAD", Val = "Download von der Web-Seite"},
                                     new LanguageStringNode
                                         {Key = "CATMAN_ACTION_EXTRACT", Val = "Extract vom Backend-System"},
                                     new LanguageStringNode
                                         {Key = "CATMAN_ACTION_INJECT", Val = "Inject in das Backend-System"},
                                     new LanguageStringNode {Key = "CATMAN_ACTION_NEW", Val = "Neu"},
                                     new LanguageStringNode {Key = "CATMAN_ACTION_REMOVE_SELECTED", Val = "Entfernen"},
                                     new LanguageStringNode {Key = "CATMAN_ACTION_SAVE", Val = "Aktualisieren"},
                                     new LanguageStringNode {Key = "CATMAN_ACTION_SELECT", Val = "Auswählen"},
                                     new LanguageStringNode {Key = "CATMAN_ACTION_UPLOAD", Val = "Upload zur Web-Seite"},
                                     new LanguageStringNode
                                         {Key = "CATMAN_FULL_SYNCHRONIZATION", Val = "Volle Synchronisation"},
                                     new LanguageStringNode {Key = "CATMAN_LABEL_CATALOG", Val = "Katalog"},
                                     new LanguageStringNode
                                         {Key = "CATMAN_LABEL_CATALOG_LANGUAGE", Val = "Katalogsprache"},
                                     new LanguageStringNode {Key = "CATMAN_LABEL_DRAGMODE", Val = "Sortierung"},
                                     new LanguageStringNode {Key = "CATMAN_LABEL_ML", Val = "ML"},
                                     new LanguageStringNode {Key = "CATMAN_LABEL_PREVIEW", Val = "Vorschau"},
                                     new LanguageStringNode {Key = "CATMAN_LABEL_PRODUCT_LIST", Val = "Artikelliste"},
                                     new LanguageStringNode {Key = "CATMAN_LABEL_PROPERTY", Val = "Eigenschaften"},
                                     new LanguageStringNode
                                         {Key = "CATMAN_LABEL_SELECT_LANGUAGE", Val = "Sprache wechseln"},
                                     new LanguageStringNode {Key = "CATMAN_LABEL_SYNCHRONIZE", Val = "Synchronisation"},
                                     new LanguageStringNode
                                         {Key = "CATMAN_LABEL_UNKNOWN_PROPERTY", Val = "Fehlende Eigenschaften"},
                                     new LanguageStringNode {Key = "CATMAN_LABEL_VALUE", Val = "Wert"},
                                     new LanguageStringNode {Key = "CATMAN_LINK_HTML_EDITOR", Val = "HTML-Editor"},
                                     new LanguageStringNode {Key = "CATMAN_DROPDOWN_NAME", Val = "Name"},
                                     new LanguageStringNode {Key = "CATMAN_DROPDOWN_NUMBER", Val = "Nummer"},
                                     new LanguageStringNode {Key = "CATMAN_SELECT_FILE", Val = "Wählen Sie eine Akte"},
                                     new LanguageStringNode {Key = "CATMAN_SELECT_IMAGE", Val = "Wählen Sie ein Bild"},
                                     new LanguageStringNode
                                         {Key = "CATMAN_LABEL_BEGINS_WITH", Val = "Fängt mit {..} an:"},
                                     new LanguageStringNode {Key = "CATMAN_LABEL_HANDLE_IMAGES", Val = "Bilder"},
                                     new LanguageStringNode
                                         {Key = "CATMAN_LABEL_HANDLE_ATTACHMENTS", Val = "Dateianhang"},
                                     new LanguageStringNode
                                         {
                                             Key = "CATMAN_LABEL_SPECIFY_FILE_TO_UPLOAD",
                                             Val = "Bitte wählen Sie eine Datei aus"
                                         },
                                     new LanguageStringNode {Key = "CATMAN_LABEL_HELP", Val = "Hilfe"},
                                     new LanguageStringNode
                                         {
                                             Key = "CATMAN_DOCUMENTUPLOAD_WARNING_MESSAGE",
                                             Val = "Es sind nur doc, docx, pdf, txt und zip Dateien erlaubt"
                                         },
                                     new LanguageStringNode
                                         {
                                             Key = "CATMAN_IMAGEUPLOAD_WARNING_MESSAGE",
                                             Val = "Es sind nur jpg, png, jpeg, bmp und gif Dateien erlaubt"
                                         },
                                     new LanguageStringNode {Key = "CATMAN_BUTTONTEXT_BROWSE", Val = "Durchsuchen"},
                                     new LanguageStringNode {Key = "CATMAN_BUTTONTEXT_SEND", Val = "Senden"},
                                     new LanguageStringNode {Key = "CATMAN_BUTTONTEXT_CANCEL", Val = "Abbrechen"},
                                     new LanguageStringNode {Key = "CATMAN_LABEL_SIZE", Val = "Größe"},
                                     new LanguageStringNode {Key = "CATMAN_LABEL_DIMENSIONS", Val = "Maße"},
                                     new LanguageStringNode {Key = "CATMAN_LABEL_ALLSAVED", Val = "Alles gespeichert"},
                                     new LanguageStringNode
                                         {
                                             Key = "CATMAN_LABEL_ERROR_STARTING_JOB",
                                             Val = "Fehler! Der ordner App_Data benötigt Schreibrechte"
                                         },
                                     new LanguageStringNode
                                         {
                                             Key = "CATMAN_LABEL_MISSING_WRITE_PERMISSION",
                                             Val =
                                                 "Dem Konto \"IUSER\" fehlen Schreibrechte für das angeforderte Verzeichnis. Upload fehlgeschlagen"
                                         },
                                     new LanguageStringNode
                                         {
                                             Key = "CATMAN_LABEL_ANOTHER_JOB_RUNNING",
                                             Val = "Eine andere Aufgabe wird ausgeführt"
                                         },
                                     new LanguageStringNode {Key = "CATMAN_LABEL_UPLOADING", Val = "Übertragung von"},
                                     new LanguageStringNode
                                         {Key = "CATMAN_LABEL_FILE_UPLOADED", Val = "wurde erfolgreich übertragen"},
                                     new LanguageStringNode
                                         {Key = "CATMAN_LABEL_AN_ERROR_OCCURRED", Val = "Es ist ein Fehler aufgetreten"},
                                     new LanguageStringNode
                                         {
                                             Key = "CATMAN_LABEL_SYNC_ERROR_RESET",
                                             Val = "Synchronisationsdateien zurücksetzen?"
                                         },
                                     new LanguageStringNode
                                         {
                                             Key = "CATMAN_LABEL_SYNC_ERROR_NONSTARTER_RESET",
                                             Val =
                                                 "Die Aufgabe wurde nicht von Ihnen gestartet. Möchten Sie die Synchronisationsdateien zurücksetzen?"
                                         },
                                     new LanguageStringNode {Key = "CATMAN_ACTION_RESET", Val = "Zurücksetzen"},
                                     new LanguageStringNode
                                         {
                                             Key = "CATMAN_CONFIRM_UNSAVED_DATA_MESSAGE",
                                             Val =
                                                 "Es sind nicht gespeicherte Daten vorhanden. Wenn Sie fortfahren gehen die Änderungen verloren. Wollen Sie wirklich fortfahren?"
                                         },
                                     new LanguageStringNode
                                         {Key = "CATMAN_LABEL_UNSAVED_DATA", Val = "Nicht gespeicherte Änderungen"},
                                     new LanguageStringNode
                                         {
                                             Key = "CATMAN_SYNC_FIRST_RUN_WARNING",
                                             Val =
                                                 "Der erste FTPTransport Start kann nicht mit dem Katalog Manager ausgeführt werden.\nStarten Sie stattdessen FTPTransport über das Startmenü auf dem Computer, auf welchem der lokale Shop installiert ist, und drücken Sie „Send All“."
                                         },
                                     new LanguageStringNode
                                         {Key = "CATMAN_ACTION_CREATE_ROUTES", Val = "URL's erzeugen"},
                                     new LanguageStringNode
                                         {Key = "CATMAN_LABEL_CREATING_ROUTES", Val = "URL's erzeugen"},
                                     new LanguageStringNode {Key = "CATMAN_ACTION_DONE", Val = "Fertig"},
                                     new LanguageStringNode {Key = "CATMAN_TOPMENU_ADMIN", Val = "Benutzerkonto"},
                                     new LanguageStringNode {Key = "CATMAN_MENU_ADMIN", Val = "Benutzerverwaltung"},
                                     new LanguageStringNode
                                         {Key = "CATMAN_MENU_CHANGE_PASSWORD", Val = "Passwort ändern"},
                                     new LanguageStringNode {Key = "CATMAN_MENU_LOGOUT", Val = "Abmelden"},
                                     new LanguageStringNode {Key = "CATMAN_CONFIRM_DELETE", Val = "Sind Sie sicher?"},
                                     new LanguageStringNode {Key = "CATMAN_USED_IN_CATALOG", Val = "In the catalog"},
                                     new LanguageStringNode {Key = "CATMAN_YES", Val = "Ja"}
                                 };
            

            languageDict.Add("de", germanList);

            var spanishList = new List<LanguageStringNode>
                                  {
                                      new LanguageStringNode
                                          {
                                              Key = "CATMAN_CONFIRM_ML_QUESTION",
                                              Val =
                                                  "¿Ha cambiado la propriedad '%REPLACE%' a multi-idioma. Desea copiar el valor actual a los demás idiomas?"
                                          },
                                      new LanguageStringNode
                                          {
                                              Key = "CATMAN_CONFIRM_ML_WARNING",
                                              Val =
                                                  "Está a punto de deshabilitar la configuración multi-idioma para la propriedad '%REPLACE%'. Esto borrará definitivamente todos los valores traducidos de esta propriedad. Sólo el valor del idioma actual será preservado.\n\n¿Desea continuar?"
                                          },
                                      new LanguageStringNode {Key = "CATMAN_ACTION_ADD_SELECTED", Val = "Añadir"},
                                      new LanguageStringNode
                                          {
                                              Key = "CATMAN_ACTION_BOTH_UPLOAD_AND_DOWNLOAD",
                                              Val = "Subir y bajar de sitio web"
                                          },
                                      new LanguageStringNode {Key = "CATMAN_ACTION_DELETE", Val = "Borrar"},
                                      new LanguageStringNode
                                          {Key = "CATMAN_ACTION_DOWNLOAD", Val = "Bajar de sitio web"},
                                      new LanguageStringNode
                                          {Key = "CATMAN_ACTION_EXTRACT", Val = "Extraer desde Back-end"},
                                      new LanguageStringNode
                                          {Key = "CATMAN_ACTION_INJECT", Val = "Inyectar en Back-end"},
                                      new LanguageStringNode {Key = "CATMAN_ACTION_NEW", Val = "Nuevo"},
                                      new LanguageStringNode {Key = "CATMAN_ACTION_REMOVE_SELECTED", Val = "Eliminar"},
                                      new LanguageStringNode {Key = "CATMAN_ACTION_SAVE", Val = "Conservar"},
                                      new LanguageStringNode {Key = "CATMAN_ACTION_SELECT", Val = "Seleccionar"},
                                      new LanguageStringNode {Key = "CATMAN_ACTION_UPLOAD", Val = "Subir a sitio web"},
                                      new LanguageStringNode
                                          {Key = "CATMAN_FULL_SYNCHRONIZATION", Val = "Sincronizacíon total"},
                                      new LanguageStringNode {Key = "CATMAN_LABEL_CATALOG", Val = "Catálogo"},
                                      new LanguageStringNode
                                          {Key = "CATMAN_LABEL_CATALOG_LANGUAGE", Val = "Idioma de catálogo"},
                                      new LanguageStringNode {Key = "CATMAN_LABEL_DRAGMODE", Val = "Clasificación"},
                                      new LanguageStringNode {Key = "CATMAN_LABEL_ML", Val = "EM"},
                                      new LanguageStringNode {Key = "CATMAN_LABEL_PREVIEW", Val = "Previsualizar"},
                                      new LanguageStringNode
                                          {Key = "CATMAN_LABEL_PRODUCT_LIST", Val = "Lista de productos"},
                                      new LanguageStringNode {Key = "CATMAN_LABEL_PROPERTY", Val = "Propiedad"},
                                      new LanguageStringNode
                                          {Key = "CATMAN_LABEL_SELECT_LANGUAGE", Val = "Cambiar idioma"},
                                      new LanguageStringNode {Key = "CATMAN_LABEL_SYNCHRONIZE", Val = "Sincronizar"},
                                      new LanguageStringNode
                                          {Key = "CATMAN_LABEL_UNKNOWN_PROPERTY", Val = "Propiedad perdida"},
                                      new LanguageStringNode {Key = "CATMAN_LABEL_VALUE", Val = "Valor"},
                                      new LanguageStringNode {Key = "CATMAN_LINK_HTML_EDITOR", Val = "HTML-Editor"},
                                      new LanguageStringNode {Key = "CATMAN_DROPDOWN_NAME", Val = "Nombre"},
                                      new LanguageStringNode {Key = "CATMAN_DROPDOWN_NUMBER", Val = "Número"},
                                      new LanguageStringNode {Key = "CATMAN_SELECT_FILE", Val = "Elija un archivo"},
                                      new LanguageStringNode {Key = "CATMAN_SELECT_IMAGE", Val = "Elija una imagen"},
                                      new LanguageStringNode {Key = "CATMAN_LABEL_BEGINS_WITH", Val = "comienza con:"},
                                      new LanguageStringNode
                                          {Key = "CATMAN_LABEL_HANDLE_IMAGES", Val = "Maneje las imágenes"},
                                      new LanguageStringNode
                                          {Key = "CATMAN_LABEL_HANDLE_ATTACHMENTS", Val = "Maneje las attachments"},
                                      new LanguageStringNode
                                          {
                                              Key = "CATMAN_LABEL_SPECIFY_FILE_TO_UPLOAD",
                                              Val = "Por favor, especifique el fichero a subir"
                                          },
                                      new LanguageStringNode {Key = "CATMAN_LABEL_HELP", Val = "Ayuda"},
                                      new LanguageStringNode
                                          {
                                              Key = "CATMAN_DOCUMENTUPLOAD_WARNING_MESSAGE",
                                              Val = "Sólo se permiten ficheros doc, docx, pdf, txt y zip"
                                          },
                                      new LanguageStringNode
                                          {
                                              Key = "CATMAN_IMAGEUPLOAD_WARNING_MESSAGE",
                                              Val = "Sólo se permiten ficheros jpg, png, jpeg, bmp y gif"
                                          },
                                      new LanguageStringNode {Key = "CATMAN_BUTTONTEXT_BROWSE", Val = "Navegar"},
                                      new LanguageStringNode {Key = "CATMAN_BUTTONTEXT_SEND", Val = "Enviar"},
                                      new LanguageStringNode {Key = "CATMAN_BUTTONTEXT_CANCEL", Val = "Cancelar"},
                                      new LanguageStringNode {Key = "CATMAN_LABEL_SIZE", Val = "Tamaño"},
                                      new LanguageStringNode {Key = "CATMAN_LABEL_DIMENSIONS", Val = "Dimensiones"},
                                      new LanguageStringNode {Key = "CATMAN_LABEL_ALLSAVED", Val = "Todos guardados"},
                                      new LanguageStringNode
                                          {
                                              Key = "CATMAN_LABEL_ERROR_STARTING_JOB",
                                              Val =
                                                  "Error iniciando trabajo. Se necesitan permisos de escritura sobre el directorio App_Data"
                                          },
                                      new LanguageStringNode
                                          {
                                              Key = "CATMAN_LABEL_MISSING_WRITE_PERMISSION",
                                              Val =
                                                  "El usuario \"IUSER\" no tiene permisos de escritura sobre el directorio especificado. Error al cargar"
                                          },
                                      new LanguageStringNode
                                          {
                                              Key = "CATMAN_LABEL_ANOTHER_JOB_RUNNING",
                                              Val = "Se está ejecutando otra tarea"
                                          },
                                      new LanguageStringNode {Key = "CATMAN_LABEL_UPLOADING", Val = "Cargando"},
                                      new LanguageStringNode
                                          {Key = "CATMAN_LABEL_FILE_UPLOADED", Val = "cargado correctamente"},
                                      new LanguageStringNode
                                          {Key = "CATMAN_LABEL_AN_ERROR_OCCURRED", Val = "Ha ocurrido un error"},
                                      new LanguageStringNode
                                          {Key = "CATMAN_LABEL_SYNC_ERROR_RESET", Val = "¿Reiniciar sincronización?"},
                                      new LanguageStringNode
                                          {
                                              Key = "CATMAN_LABEL_SYNC_ERROR_NONSTARTER_RESET",
                                              Val =
                                                  "No ha iniciado esta tarea. ¿Reiniciar la sincronización de todas maneras?"
                                          },
                                      new LanguageStringNode {Key = "CATMAN_ACTION_RESET", Val = "Reiniciar"},
                                      new LanguageStringNode
                                          {
                                              Key = "CATMAN_CONFIRM_UNSAVED_DATA_MESSAGE",
                                              Val =
                                                  "Tiene cambios sin guardar. Si continúa estos cambios se perderán. ¿Desea continuar?"
                                          },
                                      new LanguageStringNode
                                          {Key = "CATMAN_LABEL_UNSAVED_DATA", Val = "Cambios sin guardar"},
                                      new LanguageStringNode
                                          {
                                              Key = "CATMAN_SYNC_FIRST_RUN_WARNING",
                                              Val =
                                                  "La primera ejecución de FTP Transport no puede realizarse desde el Catalog Manager.\nEn su lugar, debe ejecutarlo desde el menú Inicio en el equipo donde esté instalada ShopLocal y pulsar en el botón \"Send All\"."
                                          },
                                      new LanguageStringNode
                                          {Key = "CATMAN_ACTION_CREATE_ROUTES", Val = "Create Routes"},
                                      new LanguageStringNode
                                          {Key = "CATMAN_LABEL_CREATING_ROUTES", Val = "Creating Routes"},
                                      new LanguageStringNode {Key = "CATMAN_ACTION_DONE", Val = "Done"},
                                      new LanguageStringNode {Key = "CATMAN_TOPMENU_ADMIN", Val = "Account"},
                                      new LanguageStringNode {Key = "CATMAN_MENU_ADMIN", Val = "User Administration"},
                                      new LanguageStringNode
                                          {Key = "CATMAN_MENU_CHANGE_PASSWORD", Val = "Change Password"},
                                      new LanguageStringNode {Key = "CATMAN_MENU_LOGOUT", Val = "Log Out"},
                                      new LanguageStringNode {Key = "CATMAN_CONFIRM_DELETE", Val = "Are you sure?"},
                                      new LanguageStringNode {Key = "CATMAN_USED_IN_CATALOG", Val = "En catálogo"},
                                      new LanguageStringNode {Key = "CATMAN_YES", Val = "Si"}
                                  };
            

            languageDict.Add("es", spanishList);

            var frenchList = new List<LanguageStringNode>
                                 {
                                     new LanguageStringNode
                                         {
                                             Key = "CATMAN_CONFIRM_ML_QUESTION",
                                             Val =
                                                 "Vous avez modifié les propriétés '%REPLACE%' dans le multi-langue. Copier les éléments actuels vers autres langues?"
                                         },
                                     new LanguageStringNode
                                         {
                                             Key = "CATMAN_CONFIRM_ML_WARNING",
                                             Val =
                                                 "Voulez-vous déactiver la fonction de plurilinguisme pour la propriété '%REPLACE%'? Cela aurait pour conséquence l'effacement de tous les éléments traduits pour cette propriété. Les éléments actuels de la langue sont conservées. Continuer?"
                                         },
                                     new LanguageStringNode {Key = "CATMAN_ACTION_ADD_SELECTED", Val = "Ajouter"},
                                     new LanguageStringNode
                                         {
                                             Key = "CATMAN_ACTION_BOTH_UPLOAD_AND_DOWNLOAD",
                                             Val = "Téléchargement vers le serveur et téléchargement"
                                         },
                                     new LanguageStringNode {Key = "CATMAN_ACTION_DELETE", Val = "Supprimer"},
                                     new LanguageStringNode
                                         {
                                             Key = "CATMAN_ACTION_DOWNLOAD",
                                             Val = "Téléchargement depuis le site Internet"
                                         },
                                     new LanguageStringNode
                                         {Key = "CATMAN_ACTION_EXTRACT", Val = "Extrait du système terminal"},
                                     new LanguageStringNode
                                         {Key = "CATMAN_ACTION_INJECT", Val = "Insertion dans système terminal"},
                                     new LanguageStringNode {Key = "CATMAN_ACTION_NEW", Val = "Nouveau"},
                                     new LanguageStringNode {Key = "CATMAN_ACTION_REMOVE_SELECTED", Val = "Supprimer"},
                                     new LanguageStringNode {Key = "CATMAN_ACTION_SAVE", Val = "Conserver"},
                                     new LanguageStringNode {Key = "CATMAN_ACTION_SELECT", Val = "Sélectionner"},
                                     new LanguageStringNode
                                         {Key = "CATMAN_ACTION_UPLOAD", Val = "Chargement vers le site Internet"},
                                     new LanguageStringNode
                                         {Key = "CATMAN_FULL_SYNCHRONIZATION", Val = "Synchronisation complète"},
                                     new LanguageStringNode {Key = "CATMAN_LABEL_CATALOG", Val = "Catalogue"},
                                     new LanguageStringNode
                                         {Key = "CATMAN_LABEL_CATALOG_LANGUAGE", Val = "Langue du catalogue"},
                                     new LanguageStringNode {Key = "CATMAN_LABEL_DRAGMODE", Val = "Assort"},
                                     new LanguageStringNode {Key = "CATMAN_LABEL_ML", Val = "ML"},
                                     new LanguageStringNode {Key = "CATMAN_LABEL_PREVIEW", Val = "Afficher"},
                                     new LanguageStringNode
                                         {Key = "CATMAN_LABEL_PRODUCT_LIST", Val = "Liste d'articles"},
                                     new LanguageStringNode {Key = "CATMAN_LABEL_PROPERTY", Val = "Propriétés"},
                                     new LanguageStringNode
                                         {Key = "CATMAN_LABEL_SELECT_LANGUAGE", Val = "Changer la langue"},
                                     new LanguageStringNode {Key = "CATMAN_LABEL_SYNCHRONIZE", Val = "Synchronisation"},
                                     new LanguageStringNode
                                         {Key = "CATMAN_LABEL_UNKNOWN_PROPERTY", Val = "Propriété manquante"},
                                     new LanguageStringNode {Key = "CATMAN_LABEL_VALUE", Val = "Valeur"},
                                     new LanguageStringNode {Key = "CATMAN_LINK_HTML_EDITOR", Val = "HTML-Editor"},
                                     new LanguageStringNode {Key = "CATMAN_DROPDOWN_NAME", Val = "Nom"},
                                     new LanguageStringNode {Key = "CATMAN_DROPDOWN_NUMBER", Val = "Nombre"},
                                     new LanguageStringNode {Key = "CATMAN_SELECT_FILE", Val = "Choisissez un dossier"},
                                     new LanguageStringNode {Key = "CATMAN_SELECT_IMAGE", Val = "Choisissez une image"},
                                     new LanguageStringNode {Key = "CATMAN_LABEL_BEGINS_WITH", Val = "commence par:"},
                                     new LanguageStringNode
                                         {Key = "CATMAN_LABEL_HANDLE_IMAGES", Val = "Contrôlez les images"},
                                     new LanguageStringNode
                                         {Key = "CATMAN_LABEL_HANDLE_ATTACHMENTS", Val = "Contrôlez les attachements"},
                                     new LanguageStringNode
                                         {
                                             Key = "CATMAN_LABEL_SPECIFY_FILE_TO_UPLOAD",
                                             Val = "Veuillez spécifier un dossier pour télécharger"
                                         },
                                     new LanguageStringNode {Key = "CATMAN_LABEL_HELP", Val = "Aide"},
                                     new LanguageStringNode
                                         {
                                             Key = "CATMAN_DOCUMENTUPLOAD_WARNING_MESSAGE",
                                             Val =
                                                 "Seuls les fichiers avec les extensions doc, docx, pdf, txt et zip sont autorisées"
                                         },
                                     new LanguageStringNode
                                         {
                                             Key = "CATMAN_IMAGEUPLOAD_WARNING_MESSAGE",
                                             Val =
                                                 "Seuls les fichiers avec les extensions jpg, png, jpeg, bmp et gif sont autorisées"
                                         },
                                     new LanguageStringNode {Key = "CATMAN_BUTTONTEXT_BROWSE", Val = "Parcourir"},
                                     new LanguageStringNode {Key = "CATMAN_BUTTONTEXT_SEND", Val = "Envoyer"},
                                     new LanguageStringNode {Key = "CATMAN_BUTTONTEXT_CANCEL", Val = "Annuler"},
                                     new LanguageStringNode {Key = "CATMAN_LABEL_SIZE", Val = "Taille"},
                                     new LanguageStringNode {Key = "CATMAN_LABEL_DIMENSIONS", Val = "Dimensions"},
                                     new LanguageStringNode
                                         {
                                             Key = "CATMAN_LABEL_ALLSAVED",
                                             Val = "Toutes les informations ont été enregistrées"
                                         },
                                     new LanguageStringNode
                                         {
                                             Key = "CATMAN_LABEL_ERROR_STARTING_JOB",
                                             Val =
                                                 "Erreur lors du démarrage de la tâche. L\\'autorisation Ecriture doit être mise sur le répertoire App_Data."
                                         },
                                     new LanguageStringNode
                                         {
                                             Key = "CATMAN_LABEL_MISSING_WRITE_PERMISSION",
                                             Val =
                                                 "L\\'utilisateur \"IUSER\" n\\'a pas l\\'autorisation d\\'écrire dans le répertoire demandé. Echec du chargement."
                                         },
                                     new LanguageStringNode
                                         {
                                             Key = "CATMAN_LABEL_ANOTHER_JOB_RUNNING",
                                             Val = "Une autre tâche est déjà en cours d\\'exécution"
                                         },
                                     new LanguageStringNode {Key = "CATMAN_LABEL_UPLOADING", Val = "Chargement de"},
                                     new LanguageStringNode
                                         {Key = "CATMAN_LABEL_FILE_UPLOADED", Val = "a été correctement chargé."},
                                     new LanguageStringNode
                                         {Key = "CATMAN_LABEL_AN_ERROR_OCCURRED", Val = "Une erreur s\\'est produite"},
                                     new LanguageStringNode
                                         {Key = "CATMAN_LABEL_SYNC_ERROR_RESET", Val = "Reset sync files?"},
                                     new LanguageStringNode
                                         {
                                             Key = "CATMAN_LABEL_SYNC_ERROR_NONSTARTER_RESET",
                                             Val = "You did not start this job. Reset the sync files anyway?"
                                         },
                                     new LanguageStringNode {Key = "CATMAN_ACTION_RESET", Val = "Reset"},
                                     new LanguageStringNode
                                         {
                                             Key = "CATMAN_CONFIRM_UNSAVED_DATA_MESSAGE",
                                             Val =
                                                 "Vous avez des données non enregistrées. Si vous continuez, les modifications seront perdues. Voulez-vous continuer?"
                                         },
                                     new LanguageStringNode
                                         {Key = "CATMAN_LABEL_UNSAVED_DATA", Val = "Modifications non enregistrées"},
                                     new LanguageStringNode
                                         {
                                             Key = "CATMAN_SYNC_FIRST_RUN_WARNING",
                                             Val =
                                                 "La première exécution du Transport FTP ne peut pas être effectuée depuis le Gestionnaire de Catalogue.\nVous devez exécuter le programme « FTPTransport » depuis le menu démarré de l\'ordinateur, où la boutique locale est installée, et cliquer sur le bouton « Send All »."
                                         },
                                     new LanguageStringNode
                                         {Key = "CATMAN_ACTION_CREATE_ROUTES", Val = "Crée des Routes"},
                                     new LanguageStringNode
                                         {Key = "CATMAN_LABEL_CREATING_ROUTES", Val = "Créer des Routes"},
                                     new LanguageStringNode {Key = "CATMAN_ACTION_DONE", Val = "Tout à fait"},
                                     new LanguageStringNode {Key = "CATMAN_TOPMENU_ADMIN", Val = "Account"},
                                     new LanguageStringNode {Key = "CATMAN_MENU_ADMIN", Val = "User Administration"},
                                     new LanguageStringNode
                                         {Key = "CATMAN_MENU_CHANGE_PASSWORD", Val = "Change Password"},
                                     new LanguageStringNode {Key = "CATMAN_MENU_LOGOUT", Val = "Log Out"},
                                     new LanguageStringNode {Key = "CATMAN_CONFIRM_DELETE", Val = "Confirmez-vous?"},
                                     new LanguageStringNode {Key = "CATMAN_USED_IN_CATALOG", Val = "In the catalog"},
                                     new LanguageStringNode {Key = "CATMAN_YES", Val = "Oui"}
                                 };
            

            languageDict.Add("fr", frenchList);

            var dutchList = new List<LanguageStringNode>
                                {
                                    new LanguageStringNode
                                        {
                                            Key = "CATMAN_CONFIRM_ML_QUESTION",
                                            Val =
                                                "U heft de eigenschap '%REPLACE%' gewijzigd naar Meertalig. Wilt u de huidige waarde naar alle andere talen kopiëren?"
                                        },
                                    new LanguageStringNode
                                        {
                                            Key = "CATMAN_CONFIRM_ML_WARNING",
                                            Val =
                                                "U wilt de meertaligheid voor de eigenschap '%REPLACE%' uitschakelen. Bijgevolg zullen alle vertalingen voor deze eigenschap verwijderd worden. Enkel de waarde voor de actieve taal wordt behouden.\n\nWilt u doorgaan?"
                                        },
                                    new LanguageStringNode {Key = "CATMAN_ACTION_ADD_SELECTED", Val = "Toevoegen"},
                                    new LanguageStringNode
                                        {Key = "CATMAN_ACTION_BOTH_UPLOAD_AND_DOWNLOAD", Val = "Uploaden en downloaden"},
                                    new LanguageStringNode {Key = "CATMAN_ACTION_DELETE", Val = "Schrappen"},
                                    new LanguageStringNode
                                        {Key = "CATMAN_ACTION_DOWNLOAD", Val = "Downloaden van website"},
                                    new LanguageStringNode {Key = "CATMAN_ACTION_EXTRACT", Val = "Uit database ophalen"},
                                    new LanguageStringNode {Key = "CATMAN_ACTION_INJECT", Val = "In database invoegen"},
                                    new LanguageStringNode {Key = "CATMAN_ACTION_NEW", Val = "Nieuw"},
                                    new LanguageStringNode {Key = "CATMAN_ACTION_REMOVE_SELECTED", Val = "Schrappen"},
                                    new LanguageStringNode {Key = "CATMAN_ACTION_SAVE", Val = "Spaar"},
                                    new LanguageStringNode {Key = "CATMAN_ACTION_SELECT", Val = "Selecteren"},
                                    new LanguageStringNode {Key = "CATMAN_ACTION_UPLOAD", Val = "Uploaden naar website"},
                                    new LanguageStringNode
                                        {Key = "CATMAN_FULL_SYNCHRONIZATION", Val = "Volledige synchronisatie"},
                                    new LanguageStringNode {Key = "CATMAN_LABEL_CATALOG", Val = "Catalogus"},
                                    new LanguageStringNode
                                        {Key = "CATMAN_LABEL_CATALOG_LANGUAGE", Val = "Catalogustaal"},
                                    new LanguageStringNode {Key = "CATMAN_LABEL_DRAGMODE", Val = "Sorteer"},
                                    new LanguageStringNode {Key = "CATMAN_LABEL_ML", Val = "Me"},
                                    new LanguageStringNode {Key = "CATMAN_LABEL_PREVIEW", Val = "Weergeven"},
                                    new LanguageStringNode {Key = "CATMAN_LABEL_PRODUCT_LIST", Val = "Productoverzicht"},
                                    new LanguageStringNode {Key = "CATMAN_LABEL_PROPERTY", Val = "Eigenschap"},
                                    new LanguageStringNode
                                        {Key = "CATMAN_LABEL_SELECT_LANGUAGE", Val = "Taal selecteren"},
                                    new LanguageStringNode {Key = "CATMAN_LABEL_SYNCHRONIZE", Val = "Synchroniseren"},
                                    new LanguageStringNode
                                        {Key = "CATMAN_LABEL_UNKNOWN_PROPERTY", Val = "Ontbrekende eigenschapp"},
                                    new LanguageStringNode {Key = "CATMAN_LABEL_VALUE", Val = "Waarde"},
                                    new LanguageStringNode {Key = "CATMAN_LINK_HTML_EDITOR", Val = "HTML-Editor"},
                                    new LanguageStringNode {Key = "CATMAN_DROPDOWN_NAME", Val = "Naam"},
                                    new LanguageStringNode {Key = "CATMAN_DROPDOWN_NUMBER", Val = "aantal"},
                                    new LanguageStringNode {Key = "CATMAN_SELECT_FILE", Val = "Kies een dossier"},
                                    new LanguageStringNode {Key = "CATMAN_SELECT_IMAGE", Val = "Kies een beld"},
                                    new LanguageStringNode {Key = "CATMAN_LABEL_BEGINS_WITH", Val = "begint met:"},
                                    new LanguageStringNode {Key = "CATMAN_LABEL_HANDLE_IMAGES", Val = "Beheer beelden"},
                                    new LanguageStringNode
                                        {Key = "CATMAN_LABEL_HANDLE_ATTACHMENTS", Val = "Beheer attachments"},
                                    new LanguageStringNode
                                        {
                                            Key = "CATMAN_LABEL_SPECIFY_FILE_TO_UPLOAD",
                                            Val = "Voer de naam van het te uploaden bestand in"
                                        },
                                    new LanguageStringNode {Key = "CATMAN_LABEL_HELP", Val = "Hulp"},
                                    new LanguageStringNode
                                        {
                                            Key = "CATMAN_DOCUMENTUPLOAD_WARNING_MESSAGE",
                                            Val = "Enkel doc-, docx-, pdf-, txt- en zip-bestanden zijn toegestaan"
                                        },
                                    new LanguageStringNode
                                        {
                                            Key = "CATMAN_IMAGEUPLOAD_WARNING_MESSAGE",
                                            Val = "Enkel jpg-, png-, bmp- en gif-bestanden zijn toegestaan"
                                        },
                                    new LanguageStringNode {Key = "CATMAN_BUTTONTEXT_BROWSE", Val = "Bladeren"},
                                    new LanguageStringNode {Key = "CATMAN_BUTTONTEXT_SEND", Val = "Verzenden"},
                                    new LanguageStringNode {Key = "CATMAN_BUTTONTEXT_CANCEL", Val = "Annuleren"},
                                    new LanguageStringNode {Key = "CATMAN_LABEL_SIZE", Val = "Grootte"},
                                    new LanguageStringNode {Key = "CATMAN_LABEL_DIMENSIONS", Val = "Dimensions"},
                                    new LanguageStringNode {Key = "CATMAN_LABEL_ALLSAVED", Val = "Alles gespeichert"},
                                    new LanguageStringNode
                                        {
                                            Key = "CATMAN_LABEL_ERROR_STARTING_JOB",
                                            Val =
                                                "Fout bij starten van de taak! Onvoldoende schrijfmachtigingen voor de map App_Data."
                                        },
                                    new LanguageStringNode
                                        {
                                            Key = "CATMAN_LABEL_MISSING_WRITE_PERMISSION",
                                            Val =
                                                "De \"IUSER\" heeft geen schrijfmachtigingen voor de opgegeven map. Niet uploaden"
                                        },
                                    new LanguageStringNode
                                        {Key = "CATMAN_LABEL_ANOTHER_JOB_RUNNING", Val = "Er is een andere taak actief"},
                                    new LanguageStringNode
                                        {Key = "CATMAN_LABEL_UPLOADING", Val = "Bezig met uploaden van"},
                                    new LanguageStringNode
                                        {Key = "CATMAN_LABEL_FILE_UPLOADED", Val = "met success geüpload"},
                                    new LanguageStringNode
                                        {Key = "CATMAN_LABEL_AN_ERROR_OCCURRED", Val = "Er is een fout opgetreden"},
                                    new LanguageStringNode
                                        {
                                            Key = "CATMAN_LABEL_SYNC_ERROR_RESET",
                                            Val = "Synchronisationsdateien zurücksetzen?"
                                        },
                                    new LanguageStringNode
                                        {
                                            Key = "CATMAN_LABEL_SYNC_ERROR_NONSTARTER_RESET",
                                            Val =
                                                "Die Aufgabe wurde nicht von Ihnen gestartet. Möchten Sie die Synchronisationsdateien zurücksetzen?"
                                        },
                                    new LanguageStringNode {Key = "CATMAN_ACTION_RESET", Val = "Reset"},
                                    new LanguageStringNode
                                        {
                                            Key = "CATMAN_CONFIRM_UNSAVED_DATA_MESSAGE",
                                            Val =
                                                "Er zijn niet-opgeslagen wijzigingen. Indien u doorgaat, gaan deze wijzigingen verloren. Wilt u doorgaan?"
                                        },
                                    new LanguageStringNode
                                        {Key = "CATMAN_LABEL_UNSAVED_DATA", Val = "Niet-opgeslagen wijzigingen"},
                                    new LanguageStringNode
                                        {
                                            Key = "CATMAN_SYNC_FIRST_RUN_WARNING",
                                            Val =
                                                "FTPTransport kan de eerste maal niet uitgevoerd worden vanuit de Catalog Manager.\nU dient FTPTransport te starten via het menu Start, Send All op de ShopLocal computer."
                                        },
                                    new LanguageStringNode {Key = "CATMAN_ACTION_CREATE_ROUTES", Val = "Create Routes"},
                                    new LanguageStringNode
                                        {Key = "CATMAN_LABEL_CREATING_ROUTES", Val = "Creating Routes"},
                                    new LanguageStringNode {Key = "CATMAN_ACTION_DONE", Val = "Done"},
                                    new LanguageStringNode {Key = "CATMAN_TOPMENU_ADMIN", Val = "Account"},
                                    new LanguageStringNode {Key = "CATMAN_MENU_ADMIN", Val = "User Administration"},
                                    new LanguageStringNode
                                        {Key = "CATMAN_MENU_CHANGE_PASSWORD", Val = "Change Password"},
                                    new LanguageStringNode {Key = "CATMAN_MENU_LOGOUT", Val = "Log Out"},
                                    new LanguageStringNode {Key = "CATMAN_CONFIRM_DELETE", Val = "Are you sure?"},
                                    new LanguageStringNode {Key = "CATMAN_USED_IN_CATALOG", Val = "In the catalog"},
                                    new LanguageStringNode {Key = "CATMAN_YES", Val = "Yes"}
                                };
            

            languageDict.Add("nl", dutchList);

            var portugueseList = new List<LanguageStringNode>
                                     {
                                         new LanguageStringNode
                                             {
                                                 Key = "CATMAN_CONFIRM_ML_QUESTION",
                                                 Val =
                                                     "Alterou propriedade '%REPLACE%' para multi linguagem. Deseja copiar o valor corrente para as outras linguagens?"
                                             },
                                         new LanguageStringNode
                                             {
                                                 Key = "CATMAN_CONFIRM_ML_WARNING",
                                                 Val =
                                                     "Vai desactivar o parâmetro de multi linguagem para a propriedade '%REPLACE%'. Esta operação vai apagar todos os valores traduzidos para esta propriedade. Só o valor para a linguagem corrente vai ser preservado.\n\nDeseja continuar?"
                                             },
                                         new LanguageStringNode {Key = "CATMAN_ACTION_ADD_SELECTED", Val = "Adicionar"},
                                         new LanguageStringNode
                                             {
                                                 Key = "CATMAN_ACTION_BOTH_UPLOAD_AND_DOWNLOAD",
                                                 Val = "Upload e Download do Web Site"
                                             },
                                         new LanguageStringNode {Key = "CATMAN_ACTION_DELETE", Val = "Apagar"},
                                         new LanguageStringNode
                                             {Key = "CATMAN_ACTION_DOWNLOAD", Val = "Download do Web Site"},
                                         new LanguageStringNode
                                             {Key = "CATMAN_ACTION_EXTRACT", Val = "Extrair do Sistema Back-end"},
                                         new LanguageStringNode
                                             {Key = "CATMAN_ACTION_INJECT", Val = "Injectar no Sistema Back-end"},
                                         new LanguageStringNode {Key = "CATMAN_ACTION_NEW", Val = "Novo"},
                                         new LanguageStringNode {Key = "CATMAN_ACTION_REMOVE_SELECTED", Val = "Apagar"},
                                         new LanguageStringNode {Key = "CATMAN_ACTION_SAVE", Val = "Conservar"},
                                         new LanguageStringNode {Key = "CATMAN_ACTION_SELECT", Val = "Seleccionar"},
                                         new LanguageStringNode
                                             {Key = "CATMAN_ACTION_UPLOAD", Val = "Upload para o Web Site"},
                                         new LanguageStringNode
                                             {Key = "CATMAN_FULL_SYNCHRONIZATION", Val = "Sincronização Completa"},
                                         new LanguageStringNode {Key = "CATMAN_LABEL_CATALOG", Val = "Catálogo"},
                                         new LanguageStringNode
                                             {Key = "CATMAN_LABEL_CATALOG_LANGUAGE", Val = "Linguagem do Catálogo"},
                                         new LanguageStringNode {Key = "CATMAN_LABEL_DRAGMODE", Val = "Classifique"},
                                         new LanguageStringNode {Key = "CATMAN_LABEL_ML", Val = "ML"},
                                         new LanguageStringNode {Key = "CATMAN_LABEL_PREVIEW", Val = "Ver"},
                                         new LanguageStringNode
                                             {Key = "CATMAN_LABEL_PRODUCT_LIST", Val = "Lista de Produtos"},
                                         new LanguageStringNode {Key = "CATMAN_LABEL_PROPERTY", Val = "Propriedade"},
                                         new LanguageStringNode
                                             {Key = "CATMAN_LABEL_SELECT_LANGUAGE", Val = "Seleccione Linguagem"},
                                         new LanguageStringNode {Key = "CATMAN_LABEL_SYNCHRONIZE", Val = "Sincronisar"},
                                         new LanguageStringNode
                                             {Key = "CATMAN_LABEL_UNKNOWN_PROPERTY", Val = "Falta propriedade"},
                                         new LanguageStringNode {Key = "CATMAN_LABEL_VALUE", Val = "Valor"},
                                         new LanguageStringNode {Key = "CATMAN_LINK_HTML_EDITOR", Val = "HTML-Editor"},
                                         new LanguageStringNode {Key = "CATMAN_DROPDOWN_NAME", Val = "Nome"},
                                         new LanguageStringNode {Key = "CATMAN_DROPDOWN_NUMBER", Val = "número"},
                                         new LanguageStringNode {Key = "CATMAN_SELECT_FILE", Val = "Escolha uma lima"},
                                         new LanguageStringNode
                                             {Key = "CATMAN_SELECT_IMAGE", Val = "Escolha uma imagem"},
                                         new LanguageStringNode {Key = "CATMAN_LABEL_BEGINS_WITH", Val = "Começa com:"},
                                         new LanguageStringNode
                                             {Key = "CATMAN_LABEL_HANDLE_IMAGES", Val = "Controle imagens"},
                                         new LanguageStringNode
                                             {Key = "CATMAN_LABEL_HANDLE_ATTACHMENTS", Val = "Controle attachments"},
                                         new LanguageStringNode
                                             {
                                                 Key = "CATMAN_LABEL_SPECIFY_FILE_TO_UPLOAD",
                                                 Val = "Por favor, especifique um arquivo para carregar"
                                             },
                                         new LanguageStringNode {Key = "CATMAN_LABEL_HELP", Val = "Ajuda"},
                                         new LanguageStringNode
                                             {
                                                 Key = "CATMAN_DOCUMENTUPLOAD_WARNING_MESSAGE",
                                                 Val = "Somente doc, docx, pdf, txt e zip são permitidas"
                                             },
                                         new LanguageStringNode
                                             {
                                                 Key = "CATMAN_IMAGEUPLOAD_WARNING_MESSAGE",
                                                 Val = "Somente jpg, png, jpeg, bmp e gif files são permitidas"
                                             },
                                         new LanguageStringNode {Key = "CATMAN_BUTTONTEXT_BROWSE", Val = "Procurar"},
                                         new LanguageStringNode {Key = "CATMAN_BUTTONTEXT_SEND", Val = "Enviar"},
                                         new LanguageStringNode {Key = "CATMAN_BUTTONTEXT_CANCEL", Val = "Cancelar"},
                                         new LanguageStringNode {Key = "CATMAN_LABEL_SIZE", Val = "Tamanho"},
                                         new LanguageStringNode {Key = "CATMAN_LABEL_DIMENSIONS", Val = "Dimensões"},
                                         new LanguageStringNode {Key = "CATMAN_LABEL_ALLSAVED", Val = "Todos guardados"},
                                         new LanguageStringNode
                                             {
                                                 Key = "CATMAN_LABEL_ERROR_STARTING_JOB",
                                                 Val =
                                                     "Erro começando trabalho. Permissão de escrita é exigida na pasta App_Data"
                                             },
                                         new LanguageStringNode
                                             {
                                                 Key = "CATMAN_LABEL_MISSING_WRITE_PERMISSION",
                                                 Val =
                                                     "O IUSER falta permissão de escrita para o diretório solicitado. Falha upload."
                                             },
                                         new LanguageStringNode
                                             {
                                                 Key = "CATMAN_LABEL_ANOTHER_JOB_RUNNING",
                                                 Val = "Outro trabalho é executado"
                                             },
                                         new LanguageStringNode {Key = "CATMAN_LABEL_UPLOADING", Val = "Carregando"},
                                         new LanguageStringNode
                                             {Key = "CATMAN_LABEL_FILE_UPLOADED", Val = "carregado com sucesso"},
                                         new LanguageStringNode
                                             {Key = "CATMAN_LABEL_AN_ERROR_OCCURRED", Val = "Ocorreu um erro"},
                                         new LanguageStringNode
                                             {Key = "CATMAN_LABEL_SYNC_ERROR_RESET", Val = "Reiniciar sincronización?"},
                                         new LanguageStringNode
                                             {
                                                 Key = "CATMAN_LABEL_SYNC_ERROR_NONSTARTER_RESET",
                                                 Val =
                                                     "No ha iniciado esta tarea. Reiniciar la sincronización de todas maneras?"
                                             },
                                         new LanguageStringNode {Key = "CATMAN_ACTION_RESET", Val = "Reset"},
                                         new LanguageStringNode
                                             {
                                                 Key = "CATMAN_CONFIRM_UNSAVED_DATA_MESSAGE",
                                                 Val =
                                                     "Tem dados não gravados. Se continuar, esses dados serão perdidos. Tem a certeza que deseja continuar?"
                                             },
                                         new LanguageStringNode
                                             {Key = "CATMAN_LABEL_UNSAVED_DATA", Val = "Modificações não gravadas"},
                                         new LanguageStringNode
                                             {
                                                 Key = "CATMAN_SYNC_FIRST_RUN_WARNING",
                                                 Val =
                                                     "Na primeira execução, o FTPTransport não pode ser executado a partir do Gestor de Catálogo.\nEm vez disso, deve executar o FTPTransport a partir do menu Iniciar, no computador do ShopLocal, e carregar no botão Send All."
                                             },
                                         new LanguageStringNode
                                             {Key = "CATMAN_ACTION_CREATE_ROUTES", Val = "Create Routes"},
                                         new LanguageStringNode
                                             {Key = "CATMAN_LABEL_CREATING_ROUTES", Val = "Creating Routes"},
                                         new LanguageStringNode {Key = "CATMAN_ACTION_DONE", Val = "Done"},
                                         new LanguageStringNode {Key = "CATMAN_TOPMENU_ADMIN", Val = "Account"},
                                         new LanguageStringNode {Key = "CATMAN_MENU_ADMIN", Val = "User Administration"},
                                         new LanguageStringNode
                                             {Key = "CATMAN_MENU_CHANGE_PASSWORD", Val = "Change Password"},
                                         new LanguageStringNode {Key = "CATMAN_MENU_LOGOUT", Val = "Log Out"},
                                         new LanguageStringNode {Key = "CATMAN_CONFIRM_DELETE", Val = "Are you sure?"},
                                         new LanguageStringNode {Key = "CATMAN_USED_IN_CATALOG", Val = "In the catalog"},
                                         new LanguageStringNode {Key = "CATMAN_YES", Val = "Si"}
                                     };


            languageDict.Add("pt", portugueseList);

            var englishList = new List<LanguageStringNode>
                                  {
                                      new LanguageStringNode
                                          {
                                              Key = "CATMAN_CONFIRM_ML_QUESTION",
                                              Val =
                                                  "You have changed the property '%REPLACE%' to multi language. Do you want to copy the current value to all other languages?"
                                          },
                                      new LanguageStringNode
                                          {
                                              Key = "CATMAN_CONFIRM_ML_WARNING",
                                              Val =
                                                  "You are about to disable the multi language setting for the property '%REPLACE%'. This will permanently delete all translated values of this property. Only the value for the current language will be preserved.\n\n Do you want to continue?"
                                          },
                                      new LanguageStringNode {Key = "CATMAN_ACTION_ADD_SELECTED", Val = "Add Selected"},
                                      new LanguageStringNode
                                          {
                                              Key = "CATMAN_ACTION_BOTH_UPLOAD_AND_DOWNLOAD",
                                              Val = "Both Upload and Download from Web Site"
                                          },
                                      new LanguageStringNode {Key = "CATMAN_ACTION_DELETE", Val = "Delete"},
                                      new LanguageStringNode
                                          {Key = "CATMAN_ACTION_DOWNLOAD", Val = "Download from Web Site"},
                                      new LanguageStringNode
                                          {Key = "CATMAN_ACTION_EXTRACT", Val = "Extract From Back-end System"},
                                      new LanguageStringNode
                                          {Key = "CATMAN_ACTION_INJECT", Val = "Inject In Back-end System"},
                                      new LanguageStringNode {Key = "CATMAN_ACTION_NEW", Val = "New"},
                                      new LanguageStringNode
                                          {Key = "CATMAN_ACTION_REMOVE_SELECTED", Val = "Remove Selected"},
                                      new LanguageStringNode {Key = "CATMAN_ACTION_SAVE", Val = "Save"},
                                      new LanguageStringNode {Key = "CATMAN_ACTION_SELECT", Val = "Select"},
                                      new LanguageStringNode {Key = "CATMAN_ACTION_UPLOAD", Val = "Upload to Web Site"},
                                      new LanguageStringNode
                                          {Key = "CATMAN_FULL_SYNCHRONIZATION", Val = "Full Synchronization"},
                                      new LanguageStringNode {Key = "CATMAN_LABEL_CATALOG", Val = "Catalog"},
                                      new LanguageStringNode
                                          {Key = "CATMAN_LABEL_CATALOG_LANGUAGE", Val = "Catalog Language:"},
                                      new LanguageStringNode {Key = "CATMAN_LABEL_DRAGMODE", Val = "Sort"},
                                      new LanguageStringNode {Key = "CATMAN_LABEL_ML", Val = "ML"},
                                      new LanguageStringNode {Key = "CATMAN_LABEL_PREVIEW", Val = "Preview"},
                                      new LanguageStringNode {Key = "CATMAN_LABEL_PRODUCT_LIST", Val = "Product List"},
                                      new LanguageStringNode {Key = "CATMAN_LABEL_PROPERTY", Val = "Property"},
                                      new LanguageStringNode
                                          {Key = "CATMAN_LABEL_SELECT_LANGUAGE", Val = "Change Language"},
                                      new LanguageStringNode {Key = "CATMAN_LABEL_SYNCHRONIZE", Val = "Synchronize"},
                                      new LanguageStringNode
                                          {Key = "CATMAN_LABEL_UNKNOWN_PROPERTY", Val = "Missing Property"},
                                      new LanguageStringNode {Key = "CATMAN_LABEL_VALUE", Val = "Value"},
                                      new LanguageStringNode {Key = "CATMAN_LINK_HTML_EDITOR", Val = "HTML-Editor"},
                                      new LanguageStringNode {Key = "CATMAN_DROPDOWN_NAME", Val = "Name"},
                                      new LanguageStringNode {Key = "CATMAN_DROPDOWN_NUMBER", Val = "number"},
                                      new LanguageStringNode {Key = "CATMAN_SELECT_FILE", Val = "Select a File"},
                                      new LanguageStringNode {Key = "CATMAN_SELECT_IMAGE", Val = "Select Image"},
                                      new LanguageStringNode {Key = "CATMAN_LABEL_BEGINS_WITH", Val = "Begins with:"},
                                      new LanguageStringNode {Key = "CATMAN_LABEL_HANDLE_IMAGES", Val = "Handle Images"},
                                      new LanguageStringNode
                                          {Key = "CATMAN_LABEL_HANDLE_ATTACHMENTS", Val = "Manage attachments"},
                                      new LanguageStringNode
                                          {
                                              Key = "CATMAN_LABEL_SPECIFY_FILE_TO_UPLOAD",
                                              Val = "Please specify a file to upload"
                                          },
                                      new LanguageStringNode {Key = "CATMAN_LABEL_HELP", Val = "Help"},
                                      new LanguageStringNode
                                          {
                                              Key = "CATMAN_DOCUMENTUPLOAD_WARNING_MESSAGE",
                                              Val =
                                                  "Only files with the extensions doc, docx, pdf, txt and zip are allowed"
                                          },
                                      new LanguageStringNode
                                          {
                                              Key = "CATMAN_IMAGEUPLOAD_WARNING_MESSAGE",
                                              Val = "Only jpg, png, jpeg, bmp and gif files are allowed"
                                          },
                                      new LanguageStringNode {Key = "CATMAN_BUTTONTEXT_BROWSE", Val = "Browse"},
                                      new LanguageStringNode {Key = "CATMAN_BUTTONTEXT_SEND", Val = "Send"},
                                      new LanguageStringNode {Key = "CATMAN_BUTTONTEXT_CANCEL", Val = "Cancel"},
                                      new LanguageStringNode {Key = "CATMAN_LABEL_SIZE", Val = "Size"},
                                      new LanguageStringNode {Key = "CATMAN_LABEL_DIMENSIONS", Val = "Dimensions"},
                                      new LanguageStringNode {Key = "CATMAN_LABEL_ALLSAVED", Val = "All Saved"},
                                      new LanguageStringNode
                                          {
                                              Key = "CATMAN_LABEL_ERROR_STARTING_JOB",
                                              Val =
                                                  "Error starting job! The App_Data folder requires write permissions."
                                          },
                                      new LanguageStringNode
                                          {
                                              Key = "CATMAN_LABEL_MISSING_WRITE_PERMISSION",
                                              Val =
                                                  "The \"IUSER\" account needs write permission on the the requested directory. Failed to upload"
                                          },
                                      new LanguageStringNode
                                          {
                                              Key = "CATMAN_LABEL_ANOTHER_JOB_RUNNING",
                                              Val = "An other job is already running"
                                          },
                                      new LanguageStringNode {Key = "CATMAN_LABEL_UPLOADING", Val = "Uploading"},
                                      new LanguageStringNode
                                          {Key = "CATMAN_LABEL_FILE_UPLOADED", Val = "successfully uploaded"},
                                      new LanguageStringNode
                                          {Key = "CATMAN_LABEL_AN_ERROR_OCCURRED", Val = "An error occurred"},
                                      new LanguageStringNode
                                          {Key = "CATMAN_LABEL_SYNC_ERROR_RESET", Val = "Reset sync files?"},
                                      new LanguageStringNode
                                          {
                                              Key = "CATMAN_LABEL_SYNC_ERROR_NONSTARTER_RESET",
                                              Val = "You did not start this job. Reset the sync files anyway?"
                                          },
                                      new LanguageStringNode {Key = "CATMAN_ACTION_RESET", Val = "Reset"},
                                      new LanguageStringNode
                                          {
                                              Key = "CATMAN_CONFIRM_UNSAVED_DATA_MESSAGE",
                                              Val =
                                                  "You have unsaved data. If you continue the changes will be lost. Do you wish to continue?"
                                          },
                                      new LanguageStringNode
                                          {Key = "CATMAN_LABEL_UNSAVED_DATA", Val = "Unsaved changes"},
                                      new LanguageStringNode
                                          {
                                              Key = "CATMAN_SYNC_FIRST_RUN_WARNING",
                                              Val =
                                                  "First run of FTPTransport can not be performed from the Catalog Manager.\nInstead you need to run the FTPTransport from the Start Menu on the ShopLocal computer and click the Send All button."
                                          },
                                      new LanguageStringNode
                                          {Key = "CATMAN_ACTION_CREATE_ROUTES", Val = "Create Routes"},
                                      new LanguageStringNode
                                          {Key = "CATMAN_LABEL_CREATING_ROUTES", Val = "Creating Routes"},
                                      new LanguageStringNode {Key = "CATMAN_ACTION_DONE", Val = "Done"},
                                      new LanguageStringNode {Key = "CATMAN_TOPMENU_ADMIN", Val = "Account"},
                                      new LanguageStringNode {Key = "CATMAN_MENU_ADMIN", Val = "User Administration"},
                                      new LanguageStringNode
                                          {Key = "CATMAN_MENU_CHANGE_PASSWORD", Val = "Change Password"},
                                      new LanguageStringNode {Key = "CATMAN_MENU_LOGOUT", Val = "Log Out"},
                                      new LanguageStringNode {Key = "CATMAN_CONFIRM_DELETE", Val = "Are you sure?"},
                                      new LanguageStringNode {Key = "CATMAN_USED_IN_CATALOG", Val = "In the catalog"},
                                      new LanguageStringNode {Key = "CATMAN_YES", Val = "Yes"}
                                  };
            

            languageDict.Add("en", englishList);


            var swedishList = new List<LanguageStringNode>
                                  {
                                      new LanguageStringNode
                                          {
                                              Key = "CATMAN_CONFIRM_ML_QUESTION",
                                              Val =
                                                  "Du har ändrat egenskapen '%REPLACE%' till flerspråkig. Vill du kopiera värdet för det nu aktuella språket till alla andra språk?"
                                          },
                                      new LanguageStringNode
                                          {
                                              Key = "CATMAN_CONFIRM_ML_WARNING",
                                              Val =
                                                  "Du kommer nu att ändra den flerspråkiga egenskapen '%REPLACE%' till enkelspråkig. Detta kommer oåterkalleligen att ta bort samtliga översättningar för den här egenskapen. Endast värdet för det nu aktuella språket kommer att bevaras.\n\nVill du fortsätta ändå?"
                                          },
                                      new LanguageStringNode {Key = "CATMAN_ACTION_ADD_SELECTED", Val = "Lägg till"},
                                      new LanguageStringNode
                                          {
                                              Key = "CATMAN_ACTION_BOTH_UPLOAD_AND_DOWNLOAD",
                                              Val = "Publicera och hämta från Web"
                                          },
                                      new LanguageStringNode {Key = "CATMAN_ACTION_DELETE", Val = "Tag bort"},
                                      new LanguageStringNode
                                          {Key = "CATMAN_ACTION_DOWNLOAD", Val = "Hämta från Webbplatsen"},
                                      new LanguageStringNode
                                          {Key = "CATMAN_ACTION_EXTRACT", Val = "Exportera från ekonomisystemet"},
                                      new LanguageStringNode
                                          {Key = "CATMAN_ACTION_INJECT", Val = "Importera till ekonomisystemet"},
                                      new LanguageStringNode {Key = "CATMAN_ACTION_NEW", Val = "Ny"},
                                      new LanguageStringNode {Key = "CATMAN_ACTION_REMOVE_SELECTED", Val = "Tag bort"},
                                      new LanguageStringNode {Key = "CATMAN_ACTION_SAVE", Val = "Spara"},
                                      new LanguageStringNode {Key = "CATMAN_ACTION_SELECT", Val = "Välj"},
                                      new LanguageStringNode
                                          {Key = "CATMAN_ACTION_UPLOAD", Val = "Ladda upp till Webbplatsen"},
                                      new LanguageStringNode
                                          {Key = "CATMAN_FULL_SYNCHRONIZATION", Val = "Full synkronisering"},
                                      new LanguageStringNode {Key = "CATMAN_LABEL_CATALOG", Val = "Katalog"},
                                      new LanguageStringNode
                                          {Key = "CATMAN_LABEL_CATALOG_LANGUAGE", Val = "Katalogspråk"},
                                      new LanguageStringNode {Key = "CATMAN_LABEL_DRAGMODE", Val = "Sortering"},
                                      new LanguageStringNode {Key = "CATMAN_LABEL_ML", Val = "FS"},
                                      new LanguageStringNode {Key = "CATMAN_LABEL_PREVIEW", Val = "Förhandsvisa"},
                                      new LanguageStringNode {Key = "CATMAN_LABEL_PRODUCT_LIST", Val = "Produktlista"},
                                      new LanguageStringNode {Key = "CATMAN_LABEL_PROPERTY", Val = "Egenskap"},
                                      new LanguageStringNode {Key = "CATMAN_LABEL_SELECT_LANGUAGE", Val = "Välj språk"},
                                      new LanguageStringNode {Key = "CATMAN_LABEL_SYNCHRONIZE", Val = "Synkronisera"},
                                      new LanguageStringNode
                                          {Key = "CATMAN_LABEL_UNKNOWN_PROPERTY", Val = "Egenskap saknas"},
                                      new LanguageStringNode {Key = "CATMAN_LABEL_VALUE", Val = "Värde"},
                                      new LanguageStringNode {Key = "CATMAN_LINK_HTML_EDITOR", Val = "HTML-Editor"},
                                      new LanguageStringNode {Key = "CATMAN_DROPDOWN_NAME", Val = "Namn"},
                                      new LanguageStringNode {Key = "CATMAN_DROPDOWN_NUMBER", Val = "nummer"},
                                      new LanguageStringNode {Key = "CATMAN_SELECT_FILE", Val = "Välj en fil"},
                                      new LanguageStringNode {Key = "CATMAN_SELECT_IMAGE", Val = "Välj en bild"},
                                      new LanguageStringNode {Key = "CATMAN_LABEL_BEGINS_WITH", Val = "Börjar på:"},
                                      new LanguageStringNode
                                          {Key = "CATMAN_LABEL_HANDLE_IMAGES", Val = "Hantera bilder"},
                                      new LanguageStringNode
                                          {Key = "CATMAN_LABEL_HANDLE_ATTACHMENTS", Val = "Hantera bilagor"},
                                      new LanguageStringNode
                                          {
                                              Key = "CATMAN_LABEL_SPECIFY_FILE_TO_UPLOAD",
                                              Val = "Välj fil för uppladdning till webben"
                                          },
                                      new LanguageStringNode {Key = "CATMAN_LABEL_HELP", Val = "Hjälp"},
                                      new LanguageStringNode
                                          {
                                              Key = "CATMAN_DOCUMENTUPLOAD_WARNING_MESSAGE",
                                              Val =
                                                  "Only files with the extensions doc, docx, pdf, txt and zip are allowed"
                                          },
                                      new LanguageStringNode
                                          {
                                              Key = "CATMAN_IMAGEUPLOAD_WARNING_MESSAGE",
                                              Val = "Only jpg, png, jpeg, bmp and gif files are allowed"
                                          },
                                      new LanguageStringNode {Key = "CATMAN_BUTTONTEXT_BROWSE", Val = "Sök"},
                                      new LanguageStringNode {Key = "CATMAN_BUTTONTEXT_SEND", Val = "Skicka"},
                                      new LanguageStringNode {Key = "CATMAN_BUTTONTEXT_CANCEL", Val = "Avbryt"},
                                      new LanguageStringNode {Key = "CATMAN_LABEL_SIZE", Val = "Storlek"},
                                      new LanguageStringNode {Key = "CATMAN_LABEL_DIMENSIONS", Val = "Dimension"},
                                      new LanguageStringNode {Key = "CATMAN_LABEL_ALLSAVED", Val = "Allt sparat"},
                                      new LanguageStringNode
                                          {
                                              Key = "CATMAN_LABEL_ERROR_STARTING_JOB",
                                              Val = "Fel! Foldern \"App_Data\" kräver skrivrättigheter"
                                          },
                                      new LanguageStringNode
                                          {
                                              Key = "CATMAN_LABEL_MISSING_WRITE_PERMISSION",
                                              Val =
                                                  "Användaren \"IUSER\" saknar skrivrättigheter för den angivna foldern. Uppladdningen misslyckades."
                                          },
                                      new LanguageStringNode
                                          {
                                              Key = "CATMAN_LABEL_ANOTHER_JOB_RUNNING",
                                              Val = "Ett annat jobb har redan startats"
                                          },
                                      new LanguageStringNode {Key = "CATMAN_LABEL_UPLOADING", Val = "Laddar upp."},
                                      new LanguageStringNode
                                          {Key = "CATMAN_LABEL_FILE_UPLOADED", Val = "har laddats upp."},
                                      new LanguageStringNode
                                          {Key = "CATMAN_LABEL_AN_ERROR_OCCURRED", Val = "Ett fel har inträffat"},
                                      new LanguageStringNode {Key = "CATMAN_LABEL_SYNC_ERROR_RESET", Val = "Återställ?"},
                                      new LanguageStringNode
                                          {
                                              Key = "CATMAN_LABEL_SYNC_ERROR_NONSTARTER_RESET",
                                              Val = "Du startade inte det här jobbet. Återställ ändå?"
                                          },
                                      new LanguageStringNode {Key = "CATMAN_ACTION_RESET", Val = "Återställ"},
                                      new LanguageStringNode
                                          {
                                              Key = "CATMAN_CONFIRM_UNSAVED_DATA_MESSAGE",
                                              Val =
                                                  "Det finns data som inte har sparats. Om du fortsätter så kommer alla ändringar att gå förlorade. Vill du fortsätta ändå?"
                                          },
                                      new LanguageStringNode
                                          {Key = "CATMAN_LABEL_UNSAVED_DATA", Val = "Ej sparade ändringar"},
                                      new LanguageStringNode
                                          {
                                              Key = "CATMAN_SYNC_FIRST_RUN_WARNING",
                                              Val =
                                                  "Första körningen av FTPTransport kan inte utföras från Catalog Manager.\nI stället skall du köra FTPTransport från Startmenyn på ShopLocal maskinen, och klicka på Send All knappen."
                                          },
                                      new LanguageStringNode {Key = "CATMAN_ACTION_CREATE_ROUTES", Val = "Skapa Routes"},
                                      new LanguageStringNode
                                          {Key = "CATMAN_LABEL_CREATING_ROUTES", Val = "Skapar Routes"},
                                      new LanguageStringNode {Key = "CATMAN_ACTION_DONE", Val = "Klart"},
                                      new LanguageStringNode {Key = "CATMAN_TOPMENU_ADMIN", Val = "Konto"},
                                      new LanguageStringNode {Key = "CATMAN_MENU_ADMIN", Val = "Administrera användare"},
                                      new LanguageStringNode
                                          {Key = "CATMAN_MENU_CHANGE_PASSWORD", Val = "Ändra lösenord"},
                                      new LanguageStringNode {Key = "CATMAN_MENU_LOGOUT", Val = "Logga ut"},
                                      new LanguageStringNode {Key = "CATMAN_CONFIRM_DELETE", Val = "Är du säker?"},
                                      new LanguageStringNode {Key = "CATMAN_USED_IN_CATALOG", Val = "Finns i katalogen"},
                                      new LanguageStringNode {Key = "CATMAN_YES", Val = "Ja"}
                                  };
            

            languageDict.Add("sv", swedishList);

            return languageDict;
        }

        #endregion

        #region Properties

        ///<summary>
        ///</summary>
        public static string CATMAN_CONFIRM_ML_WARNING
        {
            get { return GetValue("CATMAN_CONFIRM_ML_WARNING"); }
        }

        ///<summary>
        ///</summary>
        public static string CATMAN_CONFIRM_ML_QUESTION
        {
            get { return GetValue("CATMAN_CONFIRM_ML_QUESTION"); }
        }

        ///<summary>
        ///</summary>
        public static string CATMAN_ACTION_ADD_SELECTED
        {
            get { return GetValue("CATMAN_ACTION_ADD_SELECTED"); }
        }

        ///<summary>
        ///</summary>
        public static string CATMAN_ACTION_BOTH_UPLOAD_AND_DOWNLOAD
        {
            get { return GetValue("CATMAN_ACTION_BOTH_UPLOAD_AND_DOWNLOAD"); }
        }

        ///<summary>
        ///</summary>
        public static string CATMAN_ACTION_DELETE
        {
            get { return GetValue("CATMAN_ACTION_DELETE"); }
        }

        ///<summary>
        ///</summary>
        public static string CATMAN_ACTION_DOWNLOAD
        {
            get { return GetValue("CATMAN_ACTION_DOWNLOAD"); }
        }

        ///<summary>
        ///</summary>
        public static string CATMAN_ACTION_EXTRACT
        {
            get { return GetValue("CATMAN_ACTION_EXTRACT"); }
        }

        ///<summary>
        ///</summary>
        public static string CATMAN_ACTION_INJECT
        {
            get { return GetValue("CATMAN_ACTION_INJECT"); }
        }

        ///<summary>
        ///</summary>
        public static string CATMAN_ACTION_NEW
        {
            get { return GetValue("CATMAN_ACTION_NEW"); }
        }

        ///<summary>
        ///</summary>
        public static string CATMAN_ACTION_REMOVE_SELECTED
        {
            get { return GetValue("CATMAN_ACTION_REMOVE_SELECTED"); }
        }

        ///<summary>
        ///</summary>
        public static string CATMAN_ACTION_SAVE
        {
            get { return GetValue("CATMAN_ACTION_SAVE"); }
        }

        ///<summary>
        ///</summary>
        public static string CATMAN_ACTION_SELECT
        {
            get { return GetValue("CATMAN_ACTION_SELECT"); }
        }

        ///<summary>
        ///</summary>
        public static string CATMAN_ACTION_UPLOAD
        {
            get { return GetValue("CATMAN_ACTION_UPLOAD"); }
        }

        ///<summary>
        ///</summary>
        public static string CATMAN_FULL_SYNCHRONIZATION
        {
            get { return GetValue("CATMAN_FULL_SYNCHRONIZATION"); }
        }

        ///<summary>
        ///</summary>
        public static string CATMAN_LABEL_CATALOG
        {
            get { return GetValue("CATMAN_LABEL_CATALOG"); }
        }

        ///<summary>
        ///</summary>
        public static string CATMAN_LABEL_CATALOG_LANGUAGE
        {
            get { return GetValue("CATMAN_LABEL_CATALOG_LANGUAGE"); }
        }

        ///<summary>
        ///</summary>
        public static string CATMAN_LABEL_DRAGMODE
        {
            get { return GetValue("CATMAN_LABEL_DRAGMODE"); }
        }

        ///<summary>
        ///</summary>
        public static string CATMAN_LABEL_ML
        {
            get { return GetValue("CATMAN_LABEL_ML"); }
        }

        ///<summary>
        ///</summary>
        public static string CATMAN_LABEL_PREVIEW
        {
            get { return GetValue("CATMAN_LABEL_PREVIEW"); }
        }

        ///<summary>
        ///</summary>
        public static string CATMAN_LABEL_PRODUCT_LIST
        {
            get { return GetValue("CATMAN_LABEL_PRODUCT_LIST"); }
        }

        ///<summary>
        ///</summary>
        public static string CATMAN_LABEL_PROPERTY
        {
            get { return GetValue("CATMAN_LABEL_PROPERTY"); }
        }

        ///<summary>
        ///</summary>
        public static string CATMAN_LABEL_SELECT_LANGUAGE
        {
            get { return GetValue("CATMAN_LABEL_SELECT_LANGUAGE"); }
        }

        ///<summary>
        ///</summary>
        public static string CATMAN_LABEL_SYNCHRONIZE
        {
            get { return GetValue("CATMAN_LABEL_SYNCHRONIZE"); }
        }

        ///<summary>
        ///</summary>
        public static string CATMAN_LABEL_UNKNOWN_PROPERTY
        {
            get { return GetValue("CATMAN_LABEL_UNKNOWN_PROPERTY"); }
        }

        ///<summary>
        ///</summary>
        public static string CATMAN_LABEL_VALUE
        {
            get { return GetValue("CATMAN_LABEL_VALUE"); }
        }

        ///<summary>
        ///</summary>
        public static string CATMAN_LINK_HTML_EDITOR
        {
            get { return GetValue("CATMAN_LINK_HTML_EDITOR"); }
        }

        ///<summary>
        ///</summary>
        public static string CATMAN_DROPDOWN_NAME
        {
            get { return GetValue("CATMAN_DROPDOWN_NAME"); }
        }

        ///<summary>
        ///</summary>
        public static string CATMAN_DROPDOWN_NUMBER
        {
            get { return GetValue("CATMAN_DROPDOWN_NUMBER"); }
        }

        ///<summary>
        ///</summary>
        public static string CATMAN_SELECT_FILE
        {
            get { return GetValue("CATMAN_SELECT_FILE"); }
        }

        ///<summary>
        ///</summary>
        public static string CATMAN_SELECT_IMAGE
        {
            get { return GetValue("CATMAN_SELECT_IMAGE"); }
        }

        ///<summary>
        ///</summary>
        public static string CATMAN_LABEL_BEGINS_WITH
        {
            get { return GetValue("CATMAN_LABEL_BEGINS_WITH"); }
        }

        ///<summary>
        ///</summary>
        public static string CATMAN_LABEL_HANDLE_IMAGES
        {
            get { return GetValue("CATMAN_LABEL_HANDLE_IMAGES"); }
        }

        ///<summary>
        ///</summary>
        public static string CATMAN_LABEL_HANDLE_ATTACHMENTS
        {
            get { return GetValue("CATMAN_LABEL_HANDLE_ATTACHMENTS"); }
        }

        ///<summary>
        ///</summary>
        public static string CATMAN_LABEL_SPECIFY_FILE_TO_UPLOAD
        {
            get { return GetValue("CATMAN_LABEL_SPECIFY_FILE_TO_UPLOAD"); }
        }

        ///<summary>
        ///</summary>
        public static string CATMAN_LABEL_HELP
        {
            get { return GetValue("CATMAN_LABEL_HELP"); }
        }

        ///<summary>
        ///</summary>
        public static string CATMAN_DOCUMENTUPLOAD_WARNING_MESSAGE
        {
            get { return GetValue("CATMAN_DOCUMENTUPLOAD_WARNING_MESSAGE"); }
        }

        ///<summary>
        ///</summary>
        public static string CATMAN_IMAGEUPLOAD_WARNING_MESSAGE
        {
            get { return GetValue("CATMAN_IMAGEUPLOAD_WARNING_MESSAGE"); }
        }

        ///<summary>
        ///</summary>
        public static string CATMAN_BUTTONTEXT_BROWSE
        {
            get { return GetValue("CATMAN_BUTTONTEXT_BROWSE"); }
        }

        ///<summary>
        ///</summary>
        public static string CATMAN_BUTTONTEXT_SEND
        {
            get { return GetValue("CATMAN_BUTTONTEXT_SEND"); }
        }

        ///<summary>
        ///</summary>
        public static string CATMAN_BUTTONTEXT_CANCEL
        {
            get { return GetValue("CATMAN_BUTTONTEXT_CANCEL"); }
        }

        ///<summary>
        ///</summary>
        public static string CATMAN_LABEL_SIZE
        {
            get { return GetValue("CATMAN_LABEL_SIZE"); }
        }

        ///<summary>
        ///</summary>
        public static string CATMAN_LABEL_DIMENSIONS
        {
            get { return GetValue("CATMAN_LABEL_DIMENSIONS"); }
        }

        ///<summary>
        ///</summary>
        public static string CATMAN_LABEL_ALLSAVED
        {
            get { return GetValue("CATMAN_LABEL_ALLSAVED"); }
        }

        // NEW LABELS
        ///<summary>
        ///</summary>
        public static string CATMAN_LABEL_ERROR_STARTING_JOB
        {
            get { return GetValue("CATMAN_LABEL_ERROR_STARTING_JOB"); }
        }

        ///<summary>
        ///</summary>
        public static string CATMAN_LABEL_MISSING_WRITE_PERMISSION
        {
            get { return GetValue("CATMAN_LABEL_MISSING_WRITE_PERMISSION"); }
        }

        ///<summary>
        ///</summary>
        public static string CATMAN_LABEL_ANOTHER_JOB_RUNNING
        {
            get { return GetValue("CATMAN_LABEL_ANOTHER_JOB_RUNNING"); }
        }

        ///<summary>
        ///</summary>
        public static string CATMAN_LABEL_UPLOADING
        {
            get { return GetValue("CATMAN_LABEL_UPLOADING"); }
        }

        ///<summary>
        ///</summary>
        public static string CATMAN_LABEL_FILE_UPLOADED
        {
            get { return GetValue("CATMAN_LABEL_FILE_UPLOADED"); }
        }

        ///<summary>
        ///</summary>
        public static string CATMAN_LABEL_AN_ERROR_OCCURRED
        {
            get { return GetValue("CATMAN_LABEL_AN_ERROR_OCCURRED"); }
        }

        ///<summary>
        ///</summary>
        public static string CATMAN_LABEL_SYNC_ERROR_RESET
        {
            get { return GetValue("CATMAN_LABEL_SYNC_ERROR_RESET"); }
        }

        ///<summary>
        ///</summary>
        public static string CATMAN_LABEL_SYNC_ERROR_NONSTARTER_RESET
        {
            get { return GetValue("CATMAN_LABEL_SYNC_ERROR_NONSTARTER_RESET"); }
        }

        ///<summary>
        ///</summary>
        public static string CATMAN_ACTION_RESET
        {
            get { return GetValue("CATMAN_ACTION_RESET"); }
        }

        ///<summary>
        ///</summary>
        public static string CATMAN_CONFIRM_UNSAVED_DATA_MESSAGE
        {
            get { return GetValue("CATMAN_CONFIRM_UNSAVED_DATA_MESSAGE"); }
        }

        ///<summary>
        ///</summary>
        public static string CATMAN_LABEL_UNSAVED_DATA
        {
            get { return GetValue("CATMAN_LABEL_UNSAVED_DATA"); }
        }

        ///<summary>
        ///</summary>
        public static string CATMAN_SYNC_FIRST_RUN_WARNING
        {
            get { return GetValue("CATMAN_SYNC_FIRST_RUN_WARNING"); }
        }

        ///<summary>
        ///</summary>
        public static string CATMAN_ACTION_CREATE_ROUTES
        {
            get { return GetValue("CATMAN_ACTION_CREATE_ROUTES"); }
        }

        ///<summary>
        ///</summary>
        public static string CATMAN_LABEL_CREATING_ROUTES
        {
            get { return GetValue("CATMAN_LABEL_CREATING_ROUTES"); }
        }

        ///<summary>
        ///</summary>
        public static string CATMAN_ACTION_DONE
        {
            get { return GetValue("CATMAN_ACTION_DONE"); }
        }

        /// <summary>
        ///  
        /// </summary>
        public static string CATMAN_TOPMENU_ADMIN
        {
            get { return GetValue("CATMAN_TOPMENU_ADMIN"); }
        }

        /// <summary>
        /// 
        /// </summary>
        public static string CATMAN_MENU_ADMIN
        {
            get { return GetValue("CATMAN_MENU_ADMIN"); }
        }

        /// <summary>
        /// 
        /// </summary>
        public static string CATMAN_MENU_CHANGE_PASSWORD
        {
            get { return GetValue("CATMAN_MENU_CHANGE_PASSWORD"); }
        }

        /// <summary>
        /// 
        /// </summary>
        public static string CATMAN_MENU_LOGOUT
        {
            get { return GetValue("CATMAN_MENU_LOGOUT"); }
        }

        /// <summary>
        /// 
        /// </summary>
        public static string CATMAN_CONFIRM_DELETE
        {
            get { return GetValue("CATMAN_CONFIRM_DELETE"); }
        }

        /// <summary>
        /// 
        /// </summary>
        public static string CATMAN_USED_IN_CATALOG
        {
            get { return GetValue("CATMAN_USED_IN_CATALOG"); }
        }

        /// <summary>
        /// 
        /// </summary>
        public static string CATMAN_YES
        {
            get { return GetValue("CATMAN_YES"); }
        }

        #endregion
    }
}