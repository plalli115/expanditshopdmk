﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EISCS.CMS.Services.FileService;

namespace EISCS.CMS.Build
{
    ///<summary>
    ///</summary>
    public class EmbeddedResourceViewEngine : WebFormViewEngine
    {
        ///<summary>
        ///</summary>
        public EmbeddedResourceViewEngine()
        {
            MasterLocationFormats = new[]
                                        {
                                            "~/Views/{1}/{0}.master",
                                            "~/Views/Shared/{0}.master",
                                            "~/tmp/Views/{0}.master"
                                        };
            ViewLocationFormats = new[]
                                      {
                                          "~/Views/{1}/{0}.aspx",
                                          "~/Views/{1}/{0}.ascx",
                                          "~/Views/Shared/{0}.aspx",
                                          "~/Views/Shared/{0}.ascx",
                                          "~/tmp/Views/{0}.aspx",
                                          "~/tmp/Views/{0}.ascx"
                                      };
            PartialViewLocationFormats = ViewLocationFormats;
            // Commented out for the test of vpp
            //DumpOutViews();
        }

        public static void DumpOutViews()
        {
            IEnumerable<string> resources =
                typeof (EmbeddedResourceViewEngine).Assembly.GetManifestResourceNames().Where(
                    name => name.EndsWith(".Master") ||
                            name.EndsWith(".aspx") || name.EndsWith(".ascx") || name.EndsWith("Web.config"));
            foreach (string res in resources)
            {
                DumpOutView(res);
            }
        }

        private static void DumpOutView(string res)
        {
            string rootPath = HttpContext.Current.Server.MapPath("~/tmp/Views/");
            if (!Directory.Exists(rootPath))
            {
                Directory.CreateDirectory(rootPath);
            }
            Stream resStream = typeof (EmbeddedResourceViewEngine).Assembly.GetManifestResourceStream(res);
            int lastSeparatorIdx = res.LastIndexOf('.');
            string extension = res.Substring(lastSeparatorIdx + 1);
            res = res.Substring(0, lastSeparatorIdx);
            lastSeparatorIdx = res.LastIndexOf('.');
            string fileName = res.Substring(lastSeparatorIdx + 1);
            var fh = new FileHandler();
            fh.WriteFileContent(rootPath + fileName + "." + extension, resStream);
        }
    }
}