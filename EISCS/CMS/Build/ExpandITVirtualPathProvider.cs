﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Hosting;

namespace EISCS.CMS.Build
{
    /// <summary>
    /// ExpandIT Virtual Path Provider
    /// </summary>
    public class ExpanditVirtualPathProvider : VirtualPathProvider
    {
        private readonly List<VirtualFile> _virtualFiles = new List<VirtualFile>();

        /// <summary>
        /// Default Constructor
        /// </summary>
        public ExpanditVirtualPathProvider()
        {
            IEnumerable<string> resources =
                typeof (EmbeddedResourceViewEngine).Assembly.GetManifestResourceNames().Where(
                    name => name.EndsWith(".Master") ||
                            name.EndsWith(".aspx") || name.EndsWith(".ascx") || name.EndsWith(".cshtml") || name.EndsWith("Web.config"));
            foreach (string res in resources)
            {
                string[] undotted = res.Split(new[] {'.'});
                string fileName = undotted[undotted.Length - 2] + "." + undotted[undotted.Length - 1];
                _virtualFiles.Add(new ExpanditVirtualFile("~/EISCMS/Views/" + fileName, res));
            }
        }

        /// <summary>
        /// Check if virtual file exists
        /// </summary>
        /// <param name="virtualPath"></param>
        /// <returns></returns>
        public override bool FileExists(string virtualPath)
        {
            List<VirtualFile> files =
                (from f in _virtualFiles where f.VirtualPath.Equals(virtualPath) select f).ToList();
            return files.Count > 0 || base.FileExists(virtualPath);
        }

        /// <summary>
        /// Get virtual file by path
        /// </summary>
        /// <param name="virtualPath"></param>
        /// <returns></returns>
        public override VirtualFile GetFile(string virtualPath)
        {
            List<VirtualFile> files =
                (from f in _virtualFiles where f.VirtualPath.Equals(virtualPath) select f).ToList();
            return files.Count > 0 ? files[0] : base.GetFile(virtualPath);
        }

        #region Nested type: ExpanditVirtualFile

        /// <summary>
        /// Private class inherits from VirtualFile
        /// </summary>
        private class ExpanditVirtualFile : VirtualFile
        {
            private readonly string _resourceName;

            public ExpanditVirtualFile(string filename, string resourceName)
                : base(filename)
            {
                _resourceName = resourceName;
            }

            public override Stream Open()
            {
                Stream resStream = typeof (EmbeddedResourceViewEngine).Assembly.GetManifestResourceStream(_resourceName);

                return resStream ?? Stream.Null;
            }
        }

        #endregion
    }
}