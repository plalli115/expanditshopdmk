﻿using System.IO;
using System.Xml.Serialization;
using System.Collections;
using System.Collections.Generic;

namespace EISCS.ExpandIT.Serialization
{
    public class ExpXmlSerializer
    {


        public static void Serialize(MemoryStream writer, IDictionary dictionary)
        {
            if (dictionary == null)
            {
                return;
            }
            List<Entry> entries = new List<Entry>(dictionary.Count);

            foreach (object key in dictionary.Keys)
            {
                entries.Add(new Entry(key, dictionary[key]));
            }

            XmlSerializer serializer = new XmlSerializer(typeof(List<Entry>));
            serializer.Serialize(writer, entries);

        }

        public static void Deserialize(MemoryStream reader, IDictionary dictionary)
        {
            dictionary.Clear();
            XmlSerializer serializer = new XmlSerializer(typeof(List<Entry>));
            List<Entry> list = (List<Entry>)serializer.Deserialize(reader);
            foreach (Entry entryObj in list)
            {
                dictionary[entryObj.Key] = entryObj.Value;
            }
        }

        public class Entry
        {

            public object Key;

            public object Value;

            public Entry()
            {
            }


            public Entry(object key, object value)
            {
                Key = key;
                Value = value;
            }
        }
    }
}
