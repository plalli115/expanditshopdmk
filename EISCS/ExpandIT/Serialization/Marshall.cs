﻿using System;
using System.Collections;
using System.IO;
using System.Linq;
using System.Text;
using CmsPublic.DataProviders.Logging;
using ExpandIT;

namespace EISCS.ExpandIT.Serialization
{
    public class Marshall
    {
        public static string MarshallDictionary(IDictionary d)
        {
            try
            {
                MemoryStream memStream = new MemoryStream();
                ExpXmlSerializer.Serialize(memStream, d);
                byte[] b = memStream.ToArray();
                byte[] bc = Compress(b);
                string str = Convert.ToBase64String(bc);
                return str;

            }
            catch (Exception ex)
            {
                FileLogger.Log(ex.Message + " : " + ex.StackTrace);
            }
            return string.Empty;
        }

        /// <summary>
        /// Compresses data
        /// </summary>
        /// <param name="data">Byte() with to compress</param>
        /// <returns>Byte() with compressed data</returns>
        /// <remarks></remarks>
        public static byte[] Compress(byte[] data)
        {
            MemoryStream ms = new MemoryStream();
            System.IO.Compression.GZipStream stream = new System.IO.Compression.GZipStream(ms, System.IO.Compression.CompressionMode.Compress);
            stream.Write(data, 0, data.Length);
            stream.Close();
            return ms.ToArray();
        }

        /// <summary>
        /// Decompresses data
        /// </summary>
        /// <param name="data">Byte() with data to decompress</param>
        /// <returns>Byte() with decompressed data</returns>
        /// <remarks></remarks>
        public static byte[] Decompress(byte[] data)
        {
            MemoryStream ms = new MemoryStream();
            ms.Write(data, 0, data.Length);
            ms.Position = 0;
            System.IO.Compression.GZipStream stream = new System.IO.Compression.GZipStream(ms, System.IO.Compression.CompressionMode.Decompress);
            MemoryStream temp = new MemoryStream();
            byte[] buffer = new byte[1025];
            while ((true))
            {
                int read = stream.Read(buffer, 0, buffer.Length);
                if ((read <= 0))
                {
                    break;
                }
                temp.Write(buffer, 0, buffer.Length);
            }
            stream.Close();
            return temp.ToArray();
        }

        /// <summary>
        /// Unmarshalls a scripting dictionary from a string.
        /// </summary>
        /// <param name="s">The string to be unmashalled</param>
        /// <returns>Dictionary</returns>
        /// <remarks>A very usefull function for unmarshalling the dictionary object.</remarks>
        public static object UnmarshallDictionary(string s)
        {
            ExpDictionary retval = new ExpDictionary();
            try
            {
                byte[] dc = Decompress(Convert.FromBase64String(s));
                MemoryStream mdcStream = new MemoryStream(dc) { Position = 0 };
                ExpXmlSerializer.Deserialize(mdcStream, retval);
            }
            catch (Exception ex)
            {
                FileLogger.Log(ex.Message + " : " + ex.StackTrace);
            }
            return retval;
        }

    }
}
