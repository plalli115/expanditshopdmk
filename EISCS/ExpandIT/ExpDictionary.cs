﻿using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ExpandIT
{
    [Serializable]
    public class ExpDictionaryX : Dictionary<object, object>
    {

        public enum FilterMethods
        {
            Top = 1,
            Bottom = 2,
            Random = 3
        }

        public ExpDictionaryX(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }


        public ExpDictionaryX()
        {
        }

        public new object this[object key]
        {
            get
            {
                return ContainsKey(key) ? base[key] : null;
            }

            set
            {
                if (ContainsKey(key))
                {
                    base[key] = value;
                }
                else
                {
                    Add(key, value);
                }
            }
        }

        public bool Exists(object key)
        {
            return ContainsKey(key);
        }

        public object[] ClonedKeyArray
        {
            get {
                object[] retv = new object[Count];
				if (Count == 0) {
					return retv;
				}
                Keys.CopyTo(retv, 0);
                return retv;
            }
        }

        public object[] DictItems
        {
            get
            {
                object[] itemCol = new object[Values.Count];
                Values.CopyTo(itemCol, 0);
                return itemCol;
            }
        }

        public object[] DictKeys
        {
            get
            {
                object[] itemCol = new object[Keys.Count];
                Keys.CopyTo(itemCol, 0);
                return itemCol;
            }
        }

        public static ExpDictionaryX FilterDict(ExpDictionaryX dict, int maxitems, FilterMethods filtermethod)
        {
            ExpDictionaryX retv = new ExpDictionaryX();
            int i;

            ArrayList keys = new ArrayList(dict.ClonedKeyArray);
            switch (filtermethod)
            {
                case FilterMethods.Top:
                    while (retv.Count < maxitems & keys.Count > 0)
                    {
                        i = 0;
                        retv.Add(keys[i], dict[keys[i]]);
                        keys.RemoveAt(i);
                    }
                    break;
                case FilterMethods.Bottom:
                    while (retv.Count < maxitems & keys.Count > 0)
                    {
                        i = keys.Count - 1;
                        retv.Add(keys[i], dict[keys[i]]);
                        keys.RemoveAt(i);
                    }
                    break;
                case FilterMethods.Random:
                    while (retv.Count < maxitems & keys.Count > 0)
                    {
                        i = (int) Math.Floor(VBMath.Rnd() * keys.Count);
                        retv.Add(keys[i], dict[keys[i]]);
                        keys.RemoveAt(i);
                    }
                    break;
            }
            return retv;
        }

    }

}
