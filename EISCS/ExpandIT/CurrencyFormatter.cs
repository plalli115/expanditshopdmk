﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CmsPublic.DataProviders.Logging;
using EISCS.ExpandITFramework.Infrastructure;
using EISCS.Shop.DO.Service;
using ExpandIT;

namespace EISCS.ExpandIT
{

    public class CurrencyFormatter
    {

        private static Dictionary<string, CultureInfo> _currencyCultureInfo;
        private static ILegacyDataService _legacyDataService;

        private static bool _isCacheUsed;
        static CurrencyFormatter()
        {
            // Check if caching is enabled on CultureTable
            if (CacheManager.CacheEnabled("CultureTable"))
            {
                _isCacheUsed = true;
            }
            Init();
        }


        private static void Init()
        {
            // Antipattern but needed here
            _legacyDataService = DependencyResolver.Current.GetService<ILegacyDataService>();

            _currencyCultureInfo = new Dictionary<string, CultureInfo>();

            // get the list of cultures. We are not interested in neutral cultures, since
            // currency and RegionInfo is only applicable to specific cultures
            CultureInfo[] specificCultures = CultureInfo.GetCultures(CultureTypes.SpecificCultures);
            CultureInfo[] customCultures = CultureInfo.GetCultures(CultureTypes.UserCustomCulture);

            var cultures = specificCultures.Except(customCultures);

            // get the user defined ISO/Culture settings from DB
            DataTable dt = _legacyDataService.SQL2DataTable("SELECT ISO, Culture, CurrencyDecimalDigits, CurrencyDecimalSeparator, " + "CurrencyGroupSeparator, CurrencyNegativePattern, CurrencySymbol FROM CultureTable");

            if (dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    // Adapt Culture with data from DB
                    CultureInfo ci = AdaptCulture(row);
                    // Add ISO code and culture to dictionary
                    if (ci != null)
                    {
                        _currencyCultureInfo.Add(Convert.ToString(row["ISO"]), ci);
                    }
                }
            }

            foreach (CultureInfo ci in cultures)
            {
                try
                    {
                    // Create a RegionInfo from culture id. 
                    // RegionInfo holds the currency ISO code
                    RegionInfo ri = new RegionInfo(ci.LCID);
                    // multiple cultures can have the same currency code
                    if (!_currencyCultureInfo.ContainsKey(ri.ISOCurrencySymbol))
                    {
                        _currencyCultureInfo.Add(ri.ISOCurrencySymbol, ci);
                    }
                }catch (CultureNotFoundException ex) { 
                    // Do nothing but write to the log - this exception 'should' not ever happen...
                    FileLogger.Log(ex.Message);
                }
            }

            // If caching is enabled then use it to force update in case of changes to the DB-Table
            if (_isCacheUsed)
            {
                // Try to insert cultureInfo into cache
                _isCacheUsed = CacheManager.InsertIntoCache("CultureTable", _currencyCultureInfo);
            }
        }

        /// <summary>
        /// Lookup CultureInfo by currency ISO code
        /// </summary>
        /// <param name="isoCode"></param>
        /// <returns></returns>
        private static CultureInfo CultureInfoFromCurrencyIso(string isoCode)
        {
            return (_currencyCultureInfo.ContainsKey(isoCode)) ? _currencyCultureInfo[isoCode] : null;
        }

        /// <summary>
        /// Adapt Culture with info from DB
        /// </summary>
        /// <param name="dr"></param>
        /// <returns></returns>
        private static CultureInfo AdaptCulture(DataRow dr)
        {
            try
            {
                CultureInfo c = new CultureInfo(Convert.ToString(dr["Culture"]), true);
                if ((!dr["CurrencyDecimalDigits"].Equals(DBNull.Value)))
                {
                    c.NumberFormat.CurrencyDecimalDigits = Convert.ToInt32(dr["CurrencyDecimalDigits"]);
                }
                if (!dr["CurrencyDecimalSeparator"].Equals(DBNull.Value))
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(dr["CurrencyDecimalSeparator"])))
                    {
                        c.NumberFormat.CurrencyDecimalSeparator = Convert.ToString(dr["CurrencyDecimalSeparator"]);
                    }
                }
                if ((!dr["CurrencyGroupSeparator"].Equals(DBNull.Value)))
                {
                    c.NumberFormat.CurrencyGroupSeparator = Convert.ToString(dr["CurrencyGroupSeparator"]);
                }
                if ((!dr["CurrencySymbol"].Equals(DBNull.Value)))
                {
                    c.NumberFormat.CurrencySymbol = Convert.ToString(dr["CurrencySymbol"]);
                }
                if ((!dr["CurrencyNegativePattern"].Equals(DBNull.Value)))
                {
                    c.NumberFormat.CurrencyNegativePattern = Convert.ToInt32(dr["CurrencyNegativePattern"]);
                }
                return c;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static CultureInfo CurrencyIso2CultureInfo(string currencyIso)
        {
            return CultureInfoFromCurrencyIso(currencyIso);
        }

        /// <summary>
        /// Convert currency to a string using the specified currency format
        /// </summary>
        /// <param name="amount"></param>
        /// <param name="currencyIso"></param>
        /// <returns></returns>
        public static string FormatCurrency(decimal amount, string currencyIso)
        {
            // check if caching is enabled
            if (_isCacheUsed)
            {
                // if so, and db has changed, cache = nothing
                if (HttpContext.Current.Cache["CultureTable"] == null)
                {
                    Init();
                }
            }

            CultureInfo c = CultureInfoFromCurrencyIso(currencyIso);
            if (c == null)
            {
                // if currency ISO code doesn't match any culture
                // create a new culture without currency symbol
                // and use the ISO code as a prefix (e.g. YEN 123,123.00)
                c = CultureInfo.CreateSpecificCulture("en-US");
                c.NumberFormat.CurrencySymbol = "";
                c.NumberFormat.CurrencyDecimalDigits = 2;
                c.NumberFormat.CurrencyDecimalSeparator = ".";
                c.NumberFormat.CurrencyGroupSeparator = ",";

                return string.Format("{0} {1}", currencyIso, amount.ToString("C", c.NumberFormat));
            }
            string tstr = ExpanditLib2.RoundEx(amount, 2).ToString();
            return ExpanditLib2.CDblEx(tstr).ToString("C", c.NumberFormat);
        }

        /// <summary>
        /// Overloaded FormatCurrency
        /// </summary>
        /// <param name="amount"></param>
        /// <param name="currencyIso"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        public static string FormatCurrency(string amount, string currencyIso)
        {
            if (string.IsNullOrEmpty(amount))
            {
                return FormatCurrency(0.0, currencyIso);
            }
            decimal d = 0;
            try
            {
                d = decimal.Parse(amount.Replace(" ", ""));
            }
            catch (FormatException)
            {
            }
            return FormatCurrency(d, currencyIso);
        }

        /// <summary>
        /// Overloaded FormatCurrency
        /// </summary>
        /// <param name="amount"></param>
        /// <param name="currencyIso"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        public static string FormatCurrency(DBNull amount, string currencyIso)
        {
            return FormatCurrency(0.0, currencyIso);
        }

        /// <summary>
        /// Overloaded FormatCurrency
        /// </summary>
        /// <param name="amount"></param>
        /// <param name="currencyIso"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        public static string FormatCurrency(DBNull amount, DBNull currencyIso)
        {
            return FormatCurrency(0.0, "NOC");
        }

        /// <summary>
        /// Overloaded FormatCurrency
        /// </summary>
        /// <param name="amount"></param>
        /// <param name="currencyIso"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        public static string FormatCurrency(double amount, string currencyIso)
        {
            string str = amount.ToString("R");
            decimal dcml = decimal.Parse(str);
            return FormatCurrency(dcml, currencyIso);
        }

        /// <summary>
        /// Overloaded FormatCurrency
        /// </summary>
        /// <param name="amount"></param>
        /// <param name="currencyIso"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        public static string FormatCurrency(double amount, DBNull currencyIso)
        {
            string str = amount.ToString("R");
            decimal dcml = decimal.Parse(str);
            return FormatCurrency(dcml, "NOC");
        }

        public static string FormatAmount(string amount, string inputCurrency)
        {
            return FormatCurrency(amount, inputCurrency);
        }

        public static string FormatAmount(DBNull amount, string inputCurrency)
        {
            return FormatCurrency(amount, inputCurrency);
        }

        public static string FormatAmount(DBNull amount, DBNull inputCurrency)
        {
            return FormatCurrency(amount, inputCurrency);
        }

    }
}