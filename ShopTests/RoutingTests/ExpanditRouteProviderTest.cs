﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Security.Principal;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using CmsPublic.DataRepository;
using EISCS.ExpandITFramework.Util;
using EISCS.Routing.Concrete;
using EISCS.Routing.Providers;
using EISCS.Shop.DO.Interface;
using EISCS.Shop.DO.Repository;
using EISCS.Shop.DO.Service;
using NUnit.Framework;
using Moq;

namespace ShopTests.RoutingTests
{
    /// <summary>
    ///This is a test class for ExpanditRouteProviderTest and is intended
    ///to contain all ExpanditRouteProviderTest Unit Tests
    ///</summary>
    [TestFixture]
    public class ExpanditRouteProviderTest
    {
        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext { get; set; }

        #region Additional test attributes

        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //

        #endregion

        public HttpContextBase FakeHttpContext2(string value)
        {
            var context = new Mock<HttpContextBase>();
            var request = new Mock<HttpRequestBase>();
            var response = new Mock<HttpResponseBase>();
            var session = new Mock<HttpSessionStateBase>();
            var server = new Mock<HttpServerUtilityBase>();
            var user = new Mock<IPrincipal>();
            var identity = new Mock<IIdentity>();
            var requestContext = new Mock<RequestContext>();
            var routeData = new RouteData();

            request.Setup(req => req.ApplicationPath).Returns("~/");
            request.Setup(req => req.AppRelativeCurrentExecutionFilePath).Returns("~/english-movies-usa");
            request.Setup(req => req.PathInfo).Returns("");

            response.Setup(res => res.ApplyAppPathModifier(It.IsAny<string>()))
                .Returns((string virtualPath) => virtualPath);
            user.Setup(usr => usr.Identity).Returns(identity.Object);
            identity.SetupGet(ident => ident.IsAuthenticated).Returns(true);

            context.Setup(ctx => ctx.Request).Returns(request.Object);
            context.Setup(ctx => ctx.Response).Returns(response.Object);
            context.Setup(ctx => ctx.Session).Returns(session.Object);
            context.Setup(ctx => ctx.Server).Returns(server.Object);
            context.Setup(ctx => ctx.User).Returns(user.Object);

            routeData.Values.Add("target", value);
            routeData.Values.Add("RouteUrl", value);

            requestContext.Setup(x => x.HttpContext).Returns(context.Object);
            requestContext.Setup(x => x.RouteData).Returns(routeData);

            context.Setup(ctx => ctx.Request.RequestContext).Returns(requestContext.Object);

            return context.Object;
        }

        /// <summary>
        ///A test for GetVirtualGroupProductPath
        ///</summary>
        [Test]
        public void GetVirtualGroupProductPathTest()
        {
            const string expectedValue = "/myExpectedPath";
            HttpContextBase httpContext = FakeHttpContext2(expectedValue);
            var httpruntime = new Mock<HttpRuntimeWrapper>();
            httpruntime.Setup(htr => htr.AppDomainAppVirtualPath).Returns("/eis42_dev");
            var target = new ExpanditRouteProvider();
            string actual = target.GetVirtualGroupProductPath(httpContext.Request.RequestContext);
            Assert.AreEqual(expectedValue, actual);
        }

        public void SetupThreadTest(){
            string route = "/en/start";
            HttpContextBase httpContext = FakeHttpContext2(route);

            ConnectionStringSettings expanditShopConnection = ConfigurationManager.ConnectionStrings["ExpandITConnectionString"];
            IExpanditDbFactory factory = new ExpanditDbFactory(expanditShopConnection.ConnectionString, expanditShopConnection.ProviderName);
            IPropertiesDataService dataService = new PropertiesDataService(factory);
            IGroupRepository groupRepository = new GroupRepository(factory, dataService);

            ILanguageRepository languageRepository = new LanguageRepository(factory);
            IProductRepository productRepository = new ProductRepository(factory, dataService);


            var kernel = new Dictionary<Type, object>
            {
                { typeof(IExpanditDbFactory), factory },
                { typeof(IPropertiesDataService), dataService},
                { typeof(ILanguageRepository), languageRepository },
                { typeof(IGroupRepository), groupRepository},
                { typeof(IProductRepository), productRepository}
            };
            DependencyResolver.SetResolver(new DependencyResolverMock(kernel));
        }

        [Test]
        public void RouteDataProviderThreadTestSingleAddsAndMultipleReads()
        {
            SetupThreadTest();

            Assert.IsNotNull(RouteManager.Route.PublicRouteDataProvider);

            Console.WriteLine("Before tasks: Count = " + RouteManager.Route.PublicRouteDataProvider.Count());

            List<Task> tasks = new List<Task>();
            for (int i = 0; i < 10; i++)
            {
                tasks.Add(i % 2 == 1 ? Task.Factory.StartNew(() => T1Runner()) : Task.Factory.StartNew(() => T2Runner()));
            }
            Task.WaitAll(tasks.ToArray());

            int numberOfItemsInList = RouteManager.Route.PublicRouteDataProvider.RecreateAll();
            Console.WriteLine(numberOfItemsInList);
        }

        [Test]
        public void RouteDataProviderThreadTestRecreateAndMultipleReads()
        {
            SetupThreadTest();

            Assert.IsNotNull(RouteManager.Route.PublicRouteDataProvider);

            Console.WriteLine("Before tasks: Count = " + RouteManager.Route.PublicRouteDataProvider.Count());

            List<Task> tasks = new List<Task>();
            for (int i = 0; i < 10; i++)
            {
                tasks.Add(i % 2 == 1 ? Task.Factory.StartNew(() => T1Runner()) : Task.Factory.StartNew(() => RecreationRunner()));
            }
            Task.WaitAll(tasks.ToArray());

            int numberOfItemsInList = RouteManager.Route.PublicRouteDataProvider.RecreateAll();
            Console.WriteLine(numberOfItemsInList);
        }

        public void T1Runner()
        {
            string route = "award-winners";
            HttpContextBase httpContext = FakeHttpContext2(route);
            for (int i = 0; i < 3; i++)
            {
                var node = RouteManager.Route.PublicRouteDataProvider.FillByGroupGuid(11012, "en");
                var node2 = RouteManager.Route.PublicRouteDataProvider.FillByRoute(route, httpContext);
                var links = RouteManager.Route.PublicRouteDataProvider.GetInternalLinkUrls("en");

                if (links != null)
                {
                    Console.WriteLine("links found");
                    if (links.Count > 0)
                    {
                        Console.WriteLine(links[0]);
                    }
                }

                if (node != null)
                {
                    Console.WriteLine("Route Found");
                }

                if (node != null && node.RouteUrl != null)
                {
                    Console.WriteLine(node.RouteUrl);
                }
                Console.WriteLine("T1Runner: Count = " + RouteManager.Route.PublicRouteDataProvider.Count());
                //Thread.Sleep(1000);
            }
        }

        public void T2Runner()
        {
            var routedataNode = new RouteDataNode { Controller = "Group", GroupGuid = 1, LanguageGuid = "en", RouteUrl = "/en/start " };
            RouteManager.Route.PublicRouteDataProvider.Add(routedataNode);
        }

        public void RecreationRunner(){
            Console.WriteLine("Before Recreating");
            int numberOfItemsInList = RouteManager.Route.PublicRouteDataProvider.RecreateAll();
            Console.WriteLine("After Recreating: Items in list = " + numberOfItemsInList);
        }
    }

    public class DependencyResolverMock : IDependencyResolver
    {
        private readonly IDictionary<Type, object> _kernel;
        public DependencyResolverMock(IDictionary<Type, object> kernel)
        {
            _kernel = kernel;
        }

        public object GetService(Type serviceType)
        {
            return _kernel[serviceType];
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            throw new NotImplementedException();
        }
    }
}