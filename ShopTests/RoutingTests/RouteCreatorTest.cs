﻿using System;
using System.Collections.Generic;
using System.Configuration;
using CmsPublic.DataRepository;
using CmsPublic.ModelLayer.Models;
using CmsPublic.Utilities;
using EISCS.Catalog.Logic;
using EISCS.ExpandITFramework.Util;
using EISCS.Routing.Concrete;
using EISCS.Routing.Concrete.RouteCreation;
using EISCS.Routing.Concrete.RoutePatternFormatting;
using EISCS.Routing.Interfaces;
using EISCS.Routing.RouteDataProviders;
using EISCS.Shop.DO;
using EISCS.Shop.DO.Interface;
using EISCS.Shop.DO.Repository;
using EISCS.Shop.DO.Service;
using NUnit.Framework;
using Moq;
using ShopTests.SqlCacheDependencyCachingTest.Stubs;

namespace ShopTests.RoutingTests
{
    /// <summary>
    ///This is a test class for RouteCreatorTest and is intended
    ///to contain all RouteCreatorTest Unit Tests
    ///</summary>
    [TestFixture]
    public class RouteCreatorTest
    {
        private GroupRepository _groupRepository;

        #region Additional test attributes

        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //

        #endregion

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext { get; set; }

        /// <summary>
        ///A test for Create
        ///</summary>
        public RouteCreator<T> CreateTestHelper<T>(ConfigValues configValues)
            where T : class, IRouteDataNode, new()
        {
            var httpruntime = new Mock<HttpRuntimeWrapper>();
            httpruntime.Setup(htr => htr.AppDomainAppVirtualPath).Returns("/eis42_dev");
            HttpRuntimeWrapper httpRuntime = httpruntime.Object;

            string extension = string.Empty;
            const char replacement = '-';

            const string defaultGrpTemplate = "group.aspx";
            const string defaultPrdTemplate = "product.aspx";

            const string strGroupFormat = "{language}/{grouppath}";
            const string strGroupProductFormat = "{grouppath}/{productname}/{language}";
            const string productFormat = "{productname}/{language}";

            const char groupSeparator = '/';

            var routeUrlPatternsInfo = new RouteUrlPatternsInfo
            {
                GroupPattern = strGroupFormat,
                GroupProductPattern = strGroupProductFormat,
                ProductPattern = productFormat
            };

            var charReplacements = new CharReplacements(true);

            string neglist = configValues.AppSettings("CATALOG_HIDE_NEGLIST");
            string poslist = configValues.AppSettings("CATALOG_SHOW_POSLIST");

            string ExpanditShopConnectionString = ConfigurationManager.ConnectionStrings["ExpandITConnectionString"].ConnectionString;
            string ExpanditConnectionStringProviderName = ConfigurationManager.ConnectionStrings["ExpandITConnectionString"].ProviderName;
            ExpanditDbFactory db = new ExpanditDbFactory(ExpanditShopConnectionString, ExpanditConnectionStringProviderName);
            IPropertiesDataService propertiesDataService = new PropertiesDataService(db);
            IProductRepository productRepository = new ProductRepository(db, propertiesDataService);
            IGroupRepository groupRepository = new GroupRepository(db, propertiesDataService);

            IHideTopGroupInfoContainer hideTopGroupInfoContainer = new HideTopGroupInfoInfoContainer(poslist, neglist,
                                                                                                     "en",
                                                                                                     groupRepository);
            Dictionary<string, Dictionary<int, T>> fastGroupLookup = new Dictionary<string, Dictionary<int, T>>();
            

            var target = new RouteCreator<T>(extension, replacement, defaultGrpTemplate, defaultPrdTemplate, routeUrlPatternsInfo, charReplacements, groupSeparator,
                                             false, hideTopGroupInfoContainer, productRepository, fastGroupLookup, propertiesDataService);

            return target;
        }

        private static ConfigValues GetConfiguration()
        {
            var configValues = new ConfigValues();
            var node = new ConfigurationStringNode { Key = "CATALOG_HIDE_NEGLIST", Val = "0,-1" };
            configValues.AppSettingsList.Add(node);
            node = new ConfigurationStringNode { Key = "CATALOG_SHOW_POSLIST", Val = "" };
            configValues.AppSettingsList.Add(node);
            return configValues;
        }

        [Test]
        public void CreateGroupRoutesTest()
        {
            ConfigValues configValues = GetConfiguration();
            RouteCreator<RouteDataNode> routeCreator = CreateTestHelper<RouteDataNode>(configValues);
            const string languageGuid = "EN";
            var catLogic = new CatalogLogic(configValues, _groupRepository);
            Group grp = catLogic.GetCatalogGroupTree(languageGuid);
            var routeDataNodes = new List<RouteDataNode>();
            routeCreator.CreateGroupProductRoutes(routeDataNodes, grp, languageGuid);

            Assert.IsTrue(routeDataNodes.Count > 0);

            foreach (RouteDataNode routeDataNode in routeDataNodes)
            {
                Console.WriteLine(
                    string.Format(
                        "Route: {0,55}\t\tGroupGuid: {1,15}\tProductGuid: {2,15}\tTarget: {3,50}\tLanguage: {4,3}\tTypeCode:{5,10}",
                        routeDataNode.RouteUrl, routeDataNode.GroupGuid, routeDataNode.ProductGuid,
                        routeDataNode.TargetUrl, routeDataNode.LanguageGuid, routeDataNode.TypeCode));
            }
        }

        [Test]
        [Ignore("Need working Unity bootstrapper to resolve ILanguageRepository reference.")]
        // See: http://stackoverflow.com/questions/21681247/unity-mvc-getservice-in-background-thread-throws-nullreferenceexception
        public void CreateAllTest()
        {
            ConfigValues configValues = GetConfiguration();
            RouteCreator<RouteDataNode> routeCreator = CreateTestHelper<RouteDataNode>(configValues);
            IRouteCreationModule<RouteDataNode> creationModule = new RouteCreationModule<RouteDataNode>(configValues,
                                                                                                        routeCreator);
            var s = new StopWatch();
            s.Start();
            Console.WriteLine("Start test");
            // Make lookup
            INodeListLookup<RouteDataNode> nodeListLookup = new NodeListLookup<RouteDataNode>("da");

            var routeDataNodes = new List<RouteDataNode>();
            Dictionary<string, Dictionary<int, RouteDataNode>> fastGroupLookup = new Dictionary<string, Dictionary<int, RouteDataNode>>();
            var routeDataProvider =
                new ListRouteDataProvider<RouteDataNode>(routeDataNodes, nodeListLookup, creationModule, fastGroupLookup);

            IList<RouteDataNode> routeNodeList = creationModule.PrepareCreation(routeDataProvider);

            routeDataProvider.AddRange(routeNodeList);

            int counter = 0;
            foreach (var routeDataNode in routeNodeList)
            {
                counter++;
                Console.WriteLine(string.Format("Route: {0,55}\t\tGroupGuid: {1,15}\tProductGuid: {2,15}\tTarget: {3,40}\tLanguage: {4,3}\tTypeCode:{5,10}",
                    routeDataNode.RouteUrl, routeDataNode.GroupGuid, routeDataNode.ProductGuid, routeDataNode.TargetUrl, routeDataNode.LanguageGuid, routeDataNode.TypeCode));
            }

            Assert.IsTrue(routeNodeList.Count > 0);
            s.Stop();
            Console.WriteLine("Elapsed time: " + s.GetElapsedTimeSecs());
            Console.WriteLine("Number of items in list: " + counter);
        }

        [Test]
        public void CreateProductRoutesTest()
        {
            ConfigValues configValues = GetConfiguration();
            RouteCreator<RouteDataNode> routeCreator = CreateTestHelper<RouteDataNode>(configValues);
            var routeDataNodes = new List<RouteDataNode>();
            var s = new StopWatch();
            s.Start();
            routeCreator.CreateProductRoutes(routeDataNodes, "en");
            Console.WriteLine("Product Routes Created. Elapsed time in milliseconds: " + s.GetElapsedTime());
            s.Stop();
            Assert.IsTrue(routeDataNodes.Count > 0);
            Console.WriteLine(routeDataNodes.Count);
            Console.WriteLine(routeDataNodes[1].RouteUrl);

        }

        [SetUp]
        public void setup()
        {
            string ExpanditShopConnectionString = ConfigurationManager.ConnectionStrings["ExpandITConnectionString"].ConnectionString;
            string ExpanditConnectionStringProviderName = ConfigurationManager.ConnectionStrings["ExpandITConnectionString"].ProviderName;
            ExpanditDbFactory db = new ExpanditDbFactory(ExpanditShopConnectionString, ExpanditConnectionStringProviderName);
            IPropertiesDataService propertiesDataService = new PropertiesDataService(db);
            _groupRepository = new GroupRepository(db, propertiesDataService);
        }
    }
}