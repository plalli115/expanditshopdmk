﻿using System;
using System.Configuration;
using CmsPublic.DataRepository;
using CmsPublic.ModelLayer.Models;
using EISCS.Catalog.Logic;
using EISCS.Shop.DO;
using EISCS.Shop.DO.Interface;
using EISCS.Shop.DO.Repository;
using EISCS.Shop.DO.Service;
using NUnit.Framework;
using ShopTests.SqlCacheDependencyCachingTest.Stubs;

namespace ShopTests.RoutingTests
{
    /// <summary>
    ///This is a test class for CatalogLogicTest and is intended
    ///to contain all CatalogLogicTest Unit Tests
    ///</summary>
    [TestFixture]
    public class CatalogLogicTest
    {
        private IGroupRepository _groupRepository;
        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext { get; set; }

        #region Additional test attributes

        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //

        #endregion

        private static ConfigValues GetConfVal()
        {
            //Add items to "AppSettings"
            var confVal = new ConfigValues();
            confVal.AppSettingsList.Add(new ConfigurationStringNode
                                            {
                                                Key = "CATALOG_HIDE_NEGLIST",
                                                Val = "0,-1"
                                            });
            confVal.AppSettingsList.Add(new ConfigurationStringNode
                                            {
                                                Key = "CATALOG_SHOW_POSLIST",
                                                Val = ""
                                            });

            return confVal;
        }

        /// <summary>
        ///A test for GetCatalogGroupTree
        ///</summary>
        [Test]
        public void GetCatalogGroupTreeTest()
        {
            ConfigValues confVal = GetConfVal();
            var target = new CatalogLogic(confVal, _groupRepository);
            const string languageGuid = "en";
            Group actual = target.GetCatalogGroupTree(languageGuid);
            Console.WriteLine(string.Format("Root guid = {0}", actual.ParentGuid));
            Console.WriteLine(string.Format("Root has = {0} children", actual.Children.Count));
        }

        /// <summary>
        ///A test for GetMenuGroupTree
        ///</summary>
        [Test]
        public void GetMenuGroupTreeTest()
        {
            ConfigValues confVal = GetConfVal();
            var target = new CatalogLogic(confVal, _groupRepository);
            const int index = 0;
            const string languageGuid = "en";
            Group actual = target.GetMenuGroupTree(index, languageGuid);
            Console.WriteLine(string.Format("Root guid = {0}", actual.ParentGuid));
            Console.WriteLine(string.Format("Root has = {0} children", actual.Children.Count));
        }

        [SetUp]
        public void setup()
        {
            string ExpanditShopConnectionString = ConfigurationManager.ConnectionStrings["ExpandITConnectionString"].ConnectionString;
            string ExpanditConnectionStringProviderName = ConfigurationManager.ConnectionStrings["ExpandITConnectionString"].ProviderName;
            ExpanditDbFactory db = new ExpanditDbFactory(ExpanditShopConnectionString, ExpanditConnectionStringProviderName);
            IPropertiesDataService propertiesDataService = new PropertiesDataService(db);
            _groupRepository = new GroupRepository(db, propertiesDataService);
        }
    }
}