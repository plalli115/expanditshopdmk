﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using NUnit.Framework;

namespace ShopTests.RoutingTests
{
    /// <summary>
    ///This is a test class for RegularExpressionBuilderTest and is intended
    ///to contain all RegularExpressionBuilderTest Unit Tests
    ///</summary>
    [TestFixture]
    public class RegularExpressionBuilderTest
    {
        private readonly string[] _groupFormat = {"{grouppath}", "{language}"};
        private readonly string[] _groupProductFormat = {"{grouppath}", "{productname}", "{language}"};
        private readonly string[] _productFormat = {"{productname}", "{language}"};

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext { get; set; }

        #region Additional test attributes

        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //

        #endregion

        /// <summary>
        ///A test for Build
        ///</summary>
        [Test]
        public void BuildTest()
        {
            //string str = "1/{12{{abc}/34{grouppath}/56{language}78{jkl}";
            string str = "1/12{abc}/34{grouppath}/56{language}78";

            const string expression = @"\{([^}]+)\}";
            var matches = new Regex(expression, RegexOptions.IgnoreCase);
            MatchCollection mc = matches.Matches(str);

            var strings = new List<string>();

            for (int i = 0; i < mc.Count; i++)
            {
                Console.WriteLine(mc[i].Value);
                strings.Add(mc[i].Value);
            }

            Console.WriteLine("--------------------");

            List<string> gfList = _groupFormat.ToList();

            //IEnumerable<string> notWanted = strings.Where(x => x != _groupFormat[0] && x != _groupFormat[1]);

            //foreach (string s in strings)
            //{
            //    string st = s;
            //    if(!gfList.Exists(x => x == st))
            //    {
            //        str = str.Replace(s, "");
            //    }
            //}

            str = (from s in strings let st = s where !gfList.Exists(x => x == st) select s).Aggregate(str,
                                                                                                       (current, s) =>
                                                                                                       current.Replace(
                                                                                                           s, ""));

            //str = notWanted.Aggregate(str, (current, s) => current.Replace(s, ""));

            Console.WriteLine(str);
        }
    }
}