﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using CmsPublic.DataRepository;
using EISCS.ExpandITFramework.Infrastructure.Configuration;
using EISCS.ExpandITFramework.Infrastructure.Configuration.Interfaces;
using EISCS.ExpandITFramework.Util;
using EISCS.Routing.Concrete;
using EISCS.Routing.Concrete.RouteCreation;
using EISCS.Routing.Concrete.RoutePatternFormatting;
using EISCS.Routing.Interfaces;
using EISCS.Routing.RouteDataContainers;
using EISCS.Shop.DO;
using EISCS.Shop.DO.Interface;
using EISCS.Shop.DO.Repository;
using EISCS.Shop.DO.Service;
using NUnit.Framework;
using Moq;

namespace ShopTests.RoutingTests
{
    /// <summary>
    ///This is a test class for LinkCreatorTest and is intended
    ///to contain all LinkCreatorTest Unit Tests
    ///</summary>
    [TestFixture]
    public class LinkCreatorTest
    {
        private readonly string[] _groupFormat = {"{groupnames}"};
        private readonly string[] _groupProductFormat = {"{groupnames}", "{productname}"};
        private readonly string[] _productFormat = {"{productname}"};
        private readonly Dictionary<string, string> config = new Dictionary<string, string>();
        private string _defaultGrpTemplate;

        private string _defaultLanguageGuid;
        private string _defaultPrdTemplate;
        private HttpRuntimeWrapper _httpRuntimeWrapper;
        private LinkCreator _linkCreator;
        private IRouteDataProvider<RouteDataNode> _routeDataProvider;
        public IRouteCreator<RouteDataNode> DefaultRouteCreator { get; private set; }

        public RouteUrlPatternsInfo RouteUrlPatternInfo { get; private set; }
        public CharReplacements CharReplacement { get; private set; }

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext { get; set; }

        #region Additional test attributes

        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //

        #endregion

        public HttpContextBase FakeHttpContext2()
        {
            var context = new Mock<HttpContextBase>();
            var request = new Mock<HttpRequestBase>();
            var response = new Mock<HttpResponseBase>();
            var session = new Mock<HttpSessionStateBase>();
            var server = new Mock<HttpServerUtilityBase>();
            var user = new Mock<IPrincipal>();
            var identity = new Mock<IIdentity>();

            request.Setup(req => req.ApplicationPath).Returns("~/");
            request.Setup(req => req.AppRelativeCurrentExecutionFilePath).Returns(
                "~/G/english-movies/unrated-123as/P/the-best-bicyle.aspx");
            request.Setup(req => req.PathInfo).Returns("");

            response.Setup(res => res.ApplyAppPathModifier(It.IsAny<string>()))
                .Returns((string virtualPath) => virtualPath);
            user.Setup(usr => usr.Identity).Returns(identity.Object);
            identity.SetupGet(ident => ident.IsAuthenticated).Returns(true);

            context.Setup(ctx => ctx.Request).Returns(request.Object);
            context.Setup(ctx => ctx.Response).Returns(response.Object);
            context.Setup(ctx => ctx.Session).Returns(session.Object);
            context.Setup(ctx => ctx.Server).Returns(server.Object);
            context.Setup(ctx => ctx.User).Returns(user.Object);

            return context.Object;
        }

        private void PrepareConfig()
        {
            config.Add("enabled", "true");
            config.Add("extensionless", "true");
            config.Add("enablefallback", "true");

            config.Add("GroupFormatString", "G/{groupnames}");
            config.Add("GroupProductFormatString", "G/{groupnames}/P/{productname}");
            config.Add("ProductFormatString", "P/{productname}");

            config.Add("extension", ".aspx");
            config.Add("replacementchar", "-");
            config.Add("groupseparator", "/");

            config.Add("DataProvider", "EISCS.Routing.RouteDataContainers.ListRouteDataContainer");
        }

        private static bool String2Bool(string strBool)
        {
            bool bBool;
            bool.TryParse(strBool, out bBool);
            return bBool;
        }

        public void MimicInit()
        {
            bool isRoutingActive = String2Bool(config["enabled"]);
            bool isExtensionless = String2Bool(config["extensionless"]);
            bool isFallbackEnabled = String2Bool(config["enablefallback"]);

            string extension = isExtensionless ? string.Empty : config["extension"];
            char replacement = config["replacementchar"].ToCharArray()[0];
            char groupSeparator = config["groupseparator"].ToCharArray()[0];

            string configGroupFormatString = config["GroupFormatString"];
            string configGroupProductFormatString = config["GroupProductFormatString"];
            string configProductFormatString = config["ProductFormatString"];

            var stringParser = new StringParser();

            string parsedGroupFormatString = stringParser.ReplaceByPlaceholders(configGroupFormatString,
                                                                                _groupFormat, null);
            string parsedGroupProductFormatString = stringParser.ReplaceByPlaceholders(configGroupProductFormatString,
                                                                                       _groupProductFormat, null);
            string parsedProductFormatString = stringParser.ReplaceByPlaceholders(configProductFormatString,
                                                                                  _productFormat, null);

            var routeUrlPatternInfo = new RouteUrlPatternsInfo
                                          {
                                              GroupPattern = parsedGroupFormatString,
                                              GroupProductPattern = parsedGroupProductFormatString,
                                              ProductPattern = parsedProductFormatString
                                          };
            string ExpanditShopConnectionString = ConfigurationManager.ConnectionStrings["ExpandITConnectionString"].ConnectionString;
            string ExpanditConnectionStringProviderName = ConfigurationManager.ConnectionStrings["ExpandITConnectionString"].ProviderName;
            ExpanditDbFactory db = new ExpanditDbFactory(ExpanditShopConnectionString, ExpanditConnectionStringProviderName);
            IPropertiesDataService propertiesDataService = new PropertiesDataService(db);
            IGroupRepository groupRepository = new GroupRepository(db, propertiesDataService);

            CharReplacement = new CharReplacements();

            var httpruntime = new Mock<HttpRuntimeWrapper>();
            httpruntime.Setup(htr => htr.AppDomainAppVirtualPath).Returns("/shop");
            _httpRuntimeWrapper = httpruntime.Object;

            _defaultGrpTemplate = "group.aspx";
            _defaultPrdTemplate = "product.aspx";
            _defaultLanguageGuid = "en";

            // Get  DataProvider
            string dataProviderType = config["DataProvider"];
            bool isDemoMode = String2Bool(config["DataProvider"]);

            NodeListLookup<RouteDataNode> nodeListLookup = new NodeListLookup<RouteDataNode>(_defaultLanguageGuid);

            CharReplacements charReplacement = new CharReplacements(true);
            // Make HideTopGroupInfoInfoContainer - used by the RouteCreator
            IHideTopGroupInfoContainer hideTopGroupInfoContainer = new HideTopGroupInfoInfoContainer("", "", _defaultLanguageGuid,
                                                                                     groupRepository);

            

            IProductRepository productRepository = new ProductRepository(db, propertiesDataService);
            Dictionary<string, Dictionary<int, RouteDataNode>> fastGroupLookup = new Dictionary<string, Dictionary<int, RouteDataNode>>();

            IRouteCreator<RouteDataNode> routeCreator =
                new RouteCreator<RouteDataNode>(extension, replacement, _defaultGrpTemplate, _defaultPrdTemplate, 
                    routeUrlPatternInfo, charReplacement, groupSeparator, false, hideTopGroupInfoContainer, productRepository, fastGroupLookup, propertiesDataService);

            IConfigurationValue confVal = new ConfigurationValue();

            RouteCreationModule<RouteDataNode> creationModule = new RouteCreationModule<RouteDataNode>(confVal, routeCreator);

                _routeDataProvider = RouteDataContainer.GetContainer(dataProviderType).GetProvider(creationModule, nodeListLookup, fastGroupLookup);
            _linkCreator = new LinkCreator(_routeDataProvider, _httpRuntimeWrapper);
        }

        /// <summary>
        ///A test for CreateTemplateProductLink
        ///</summary>
        /// 
        [Test]
        public void CreateTemplateProductLinkTest()
        {
            //PREPARE CONFIG FOR TEST
            PrepareConfig();

            ConnectionStringSettings expanditShopConnection = ConfigurationManager.ConnectionStrings["ExpandITConnectionString"];
            IExpanditDbFactory factory = new ExpanditDbFactory(expanditShopConnection.ConnectionString, expanditShopConnection.ProviderName);
            IPropertiesDataService dataService = new PropertiesDataService(factory);
            IGroupRepository groupRepository = new GroupRepository(factory, dataService);

            ILanguageRepository languageRepository = new LanguageRepository(factory);
            IProductRepository productRepository = new ProductRepository(factory, dataService);

            var kernel = new Dictionary<Type, object>
            {
                { typeof(IExpanditDbFactory), factory },
                { typeof(IPropertiesDataService), dataService},
                { typeof(ILanguageRepository), languageRepository },
                { typeof(IGroupRepository), groupRepository},
                { typeof(IProductRepository), productRepository}
            };
            DependencyResolver.SetResolver(new DependencyResolverMock(kernel));

            //Mimic init method
            MimicInit();

            string link = _linkCreator.CreateTemplateProductLink("8102-05", _defaultLanguageGuid);
            Assert.IsNotNull(link);
        }
    }
}