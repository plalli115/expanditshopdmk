﻿using System;
using System.Collections.Generic;
using CmsPublic.ModelLayer.Models;
using EISCS.Shop.DO.Dto;
using EISCS.Shop.DO.Interface;

namespace ShopTests.RoutingTests.Stubs
{
    public class GroupRepositoryBaseStub : IGroupRepository
    {
        public string GetTableName()
        {
            throw new NotImplementedException();
        }

        public string AllColumns()
        {
            throw new NotImplementedException();
        }

        public string GetIdColumnName()
        {
            throw new NotImplementedException();
        }

        public string GetDefaultSql()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Group> All()
        {
            throw new NotImplementedException();
        }

        public Group GetById(int id)
        {
            throw new NotImplementedException();
        }

        public Group GetById(string id)
        {
            throw new NotImplementedException();
        }

        public Group Add(Group item, bool useOutputClause = true)
        {
            throw new NotImplementedException();
        }


        public bool Update(Group item)
        {
            throw new NotImplementedException();
        }

        public bool Remove(int id)
        {
            throw new NotImplementedException();
        }

        public bool Remove(string id)
        {
            throw new NotImplementedException();
        }

        public List<Group> GetGroupsFromGroupGuids(List<int> groupGuids, string languageGuid, bool isLoadProperties)
        {
            throw new NotImplementedException();
        }

        public List<Group> GetAllGroups(string languageGuid, bool isLoadProperties)
        {
            throw new NotImplementedException();
        }

        public Dictionary<int, Group> GetAllGroupsDictionary(string languageGuid, bool isLoadProperties)
        {
            throw new NotImplementedException();
        }

        public List<Group> GetSubGroups(string languageGuid, int groupGuid, bool isLoadProperties)
        {
            throw new NotImplementedException();
        }

        public Dictionary<int, Group> GetSubGroupsDictionary(string languageGuid, int groupGuid, bool isLoadProperties)
        {
            throw new NotImplementedException();
        }

        public virtual List<Group> GetTopGroups(string languageGuid, bool isLoadProperties)
        {
            throw new NotImplementedException();
        }

        public Dictionary<int, Group> GetTopGroupsDictionary(string languageGuid, bool isLoadProperties)
        {
            throw new NotImplementedException();
        }

        public Group GetGroupForEditor(int groupId, string languageGuid)
        {
            throw new NotImplementedException();
        }

        public Group GetGroup(int guid, string languageGuid, bool isLoadProperties = true)
        {
            throw new NotImplementedException();
        }

        public int InsertGroup(int groupGuid, int parentGuid, int siblingGroupGuid)
        {
            throw new NotImplementedException();
        }

        public int DeleteGroup(int groupGuid, string languageGuid)
        {
            throw new NotImplementedException();
        }

        public int GetHighestRootGroupGuid()
        {
            throw new NotImplementedException();
        }

        public int EnsureParentGroup(string languageGuid)
        {
            throw new NotImplementedException();
        }

        public List<Group> GetRootGroupChildren(string languageGuid, bool isLoadProperties)
        {
            throw new NotImplementedException();
        }

        public Dictionary<int, Group> GetSubGroupsWithExclude(string languageGuid, int groupGuid, bool isLoadProperties, string excludeParams)
        {
            throw new NotImplementedException();
        }

        public Dictionary<int, Group> GetSubGroupsWithInclude(string languageGuid, int groupGuid, bool isLoadProperties, string includeParams)
        {
            throw new NotImplementedException();
        }

        public object CatalogLevels()
        {
            throw new NotImplementedException();
        }

        public List<string> GetPropertyGuidsIncludedInTemplate(string templateName)
        {
            throw new NotImplementedException();
        }

        public List<TemplateProperty> GetTemplateProperties(string templateFileName)
        {
            throw new NotImplementedException();
        }

        public void ConvertFromMultiLanguage(string propOwnerRefGuid, string propGuid, string languageGuid)
        {
            throw new NotImplementedException();
        }

        public void ConvertToMultiLanguage(string propOwnerRefGuid, string propGuid, string languageGuid, List<LanguageTable> allLanguages)
        {
            throw new NotImplementedException();
        }

        public List<TemplateQuerySet> TemplateTable(string filter)
        {
            throw new NotImplementedException();
        }

        public Group GetGroupByPropGuidAndPropTransText(string propGuid, string propTransText, string languageGuid)
        {
            throw new NotImplementedException();
        }

        public void AddProductsToGroup(string[] productGuids, string groupGuid)
        {
            throw new NotImplementedException();
        }

        public void DeleteProductsFromGroup(string[] groupProductGuids, string groupGuid)
        {
            throw new NotImplementedException();
        }

        public void ChangeGroupProductSortOrder(string currentItem, string nextItem, string prevItem, string groupGuid)
        {
            throw new NotImplementedException();
        }

        public void SaveProperties(Group @group, string languageGuid)
        {
            throw new NotImplementedException();
        }

        public void RegroupGroupTable(int groupGuid, int parentGuid)
        {
            throw new NotImplementedException();
        }

        public void RearrangeGroupOrder(int groupGuid, int parentGuid, int siblingGuid)
        {
            throw new NotImplementedException();
        }

        public int CreateNewGroup(int parentGuid, int siblingGuid)
        {
            throw new NotImplementedException();
        }
    }
}