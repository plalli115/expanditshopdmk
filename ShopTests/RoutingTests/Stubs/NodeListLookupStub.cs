﻿using System.Collections.Generic;
using System.Linq;
using System.Web;
using EISCS.Routing.Interfaces;

namespace ShopTests.RoutingTests.Stubs
{
    public class NodeListLookupStub<T> : INodeListLookup<T> where T : class, IRouteDataNode
    {
        /* General Search Rules */
        // Find a GroupGuid | GroupGuid & ProductGuid | ProductGuid | Route that matches. 
        // If more than one node matches the criteria select first or the first one where TypeCode != "$Auto"

        //GET node for Group Link

        #region INodeListLookup<T> Members

        public T LookUpGroup(List<T> nodeList, string groupGuid, string languageGuid)
        {
            var matchList = new List<T>();
            if (nodeList != null)
                foreach (T routeDataNode in nodeList.Where(
                    routeDataNode =>
                    routeDataNode.GroupGuid.ToString() == groupGuid && string.IsNullOrEmpty(routeDataNode.ProductGuid)).
                    Where(
                        routeDataNode => Compare(routeDataNode, ref matchList)))
                {
                    break;
                }
            return matchList.Count > 0 ? matchList[0] : null;
        }

        //GET node for GroupProduct Link
        public T LookUpGroupProduct(List<T> nodeList, string groupGuid, string productGuid, string languageGuid)
        {
            var matchList = new List<T>();
            if (nodeList != null)
                foreach (T routeDataNode in nodeList.Where(
                    routeDataNode =>
                    routeDataNode.GroupGuid.ToString() == groupGuid && routeDataNode.ProductGuid == productGuid).Where(
                        routeDataNode => Compare(routeDataNode, ref matchList)))
                {
                    break;
                }
            return matchList.Count > 0 ? matchList[0] : null;
        }

        //GET node for Product Link
        public T LookUpProduct(List<T> nodeList, int rootGuid, string productGuid, string languageGuid)
        {
            var matchList = new List<T>();
            if (nodeList != null)
                foreach (T routeDataNode in
                    nodeList.Where(
                        routeDataNode => routeDataNode.GroupGuid == rootGuid && routeDataNode.ProductGuid == productGuid)
                        .Where(
                            routeDataNode => Compare(routeDataNode, ref matchList)))
                {
                    break;
                }
            return matchList.Count > 0 ? matchList[0] : LookUpProduct(nodeList, productGuid, languageGuid);
        }

        //GET node for Product Link
        public T LookUpProduct(List<T> nodeList, string productGuid, string languageGuid)
        {
            var matchList = new List<T>();
            if (nodeList != null)
                foreach (T routeDataNode in
                    nodeList.Where(routeDataNode => routeDataNode.ProductGuid == productGuid).Where(
                        routeDataNode => Compare(routeDataNode, ref matchList)))
                {
                    break;
                }
            return matchList.Count > 0 ? matchList[0] : null;
        }

        // Get node from path
        public T LookUpRoute(List<T> nodeList, string path, HttpContextBase context)
        {
            var matchList = new List<T>();
            if (nodeList != null)
            {
                foreach (
                    T routeDataNode in
                        nodeList.Where(rnode => rnode.RouteUrl.ToLower() == path.ToLower()).Where(
                            rnode => Compare(rnode, ref matchList)))
                {
                    break;
                }
            }
            return matchList.Count > 0 ? matchList[0] : null;
        }

        #endregion

        private static bool Compare(T routeDataNode, ref List<T> matchList)
        {
            if (matchList.Count == 0)
            {
                matchList.Add(routeDataNode);
                if (!string.IsNullOrEmpty(routeDataNode.TypeCode) && routeDataNode.TypeCode != "$Auto")
                {
                    return true;
                }
            }
            else if (!string.IsNullOrEmpty(routeDataNode.TypeCode) && routeDataNode.TypeCode != "$Auto")
            {
                matchList[0] = routeDataNode;
                return true;
            }
            return false;
        }
    }
}