﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CmsPublic.ModelLayer.Models;
using EISCS.Routing.Interfaces;

namespace ShopTests.RoutingTests.Stubs
{
    internal class ListRouteDataProviderStub<T> : IRouteDataProvider<T> where T : class, IRouteDataNode
    {
        private readonly List<T> _myList;

        public ListRouteDataProviderStub(List<T> nodeList, INodeListLookup<T> nodeListLookup,
                                         IRouteCreator<T> routeCreator)
        {
            _myList = nodeList;
            LookUp = nodeListLookup;
            DefaultRouteCreator = routeCreator;
        }

        public INodeListLookup<T> LookUp { get; private set; }
        public IRouteCreator<T> DefaultRouteCreator { get; private set; }

        public IRouteCreationModule<T> RouteRecreator
        {
            get { throw new NotImplementedException(); }
        }

        #region IRouteDataProvider<T> Members

        public T FillByGroupGuid(int groupGuid, string languageGuid)
        {
            IEnumerable<T> result = _myList.Where(x => x.GroupGuid == groupGuid);
            List<T> retList = Enumerable2List(result);
            return LookUp.LookUpGroup(retList, groupGuid.ToString(), languageGuid);
        }

        public T FillByGroupProductGuid(int groupGuid, string productGuid, string languageGuid)
        {
            List<T> retList =
                Enumerable2List(_myList.Where(x => x.GroupGuid == groupGuid && x.ProductGuid == productGuid));
            return LookUp.LookUpGroupProduct(retList, groupGuid.ToString(), productGuid, languageGuid);
        }

        public T FillByProductGuid(string productGuid, string languageGuid)
        {
            return null;
        }

        public T FillByRoute(string route, HttpContextBase context)
        {
            IEnumerable<T> myList = _myList.Where(x => x.RouteUrl.ToLower() == route.ToLower());
            List<T> retList = Enumerable2List(myList);
            return LookUp.LookUpRoute(retList, route, context);
        }

        public T FillByControllerName(string controllerName)
        {
            throw new NotImplementedException();
        }

        public int GetGroupGuidByProductGuid(string productGuid, string languageGuid)
        {
            throw new NotImplementedException();
        }

        public void Add(T t)
        {
            throw new NotImplementedException();
        }

        public int AddRange(ICollection<T> t)
        {
            throw new NotImplementedException();
        }

        public int AddRangeFlushable(ICollection<T> t){
            throw new NotImplementedException();
        }

        public void RemoveByProductGuid(ICollection<string> guids, string languageGuid)
        {
            throw new NotImplementedException();
        }


        public void RemoveByGroupGuidProductGuid(ICollection<GroupProduct> t)
        {
            throw new NotImplementedException();
        }

        public int RecreateAll()
        {
            throw new NotImplementedException();
        }

        public List<string> GetInternalLinkUrls(string languageGuid)
        {
            throw new NotImplementedException();
        }

        public int Count(){
            throw new NotImplementedException();
        }

        public void RemoveByGroupGuid(ICollection<int> guids)
        {
            foreach (int guid in guids)
            {
                Remove(guid);
            }
        }

        public void RemoveByProductGuid(ICollection<string> guids)
        {
            foreach (string guid in guids)
            {
                Remove(guid);
            }
        }

        #endregion

        private static List<T> Enumerable2List(IEnumerable<T> enumerable)
        {
            var retList = new List<T>();
            retList.AddRange(enumerable);
            return retList;
        }

        public void Remove(T t)
        {
            throw new NotImplementedException();
        }

        private void Remove(int guid)
        {
            T my = _myList.Find(x => x.GroupGuid == guid);
            if (my != null)
            {
                _myList.Remove(my);
            }
        }

        private void Remove(string guid)
        {
            T my = _myList.Find(x => x.ProductGuid == guid);
            if (my != null)
            {
                _myList.Remove(my);
            }
        }
    }
}