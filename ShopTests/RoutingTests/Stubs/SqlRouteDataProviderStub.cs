﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CmsPublic.DataRepository;
using CmsPublic.ModelLayer.Models;
using EISCS.Routing.Interfaces;
using EISCS.Shop.DO.Interface;
using EISCS.Shop.DO.Repository;

namespace ShopTests.RoutingTests.Stubs
{
    public class SqlRouteDataProviderStub<T> : BaseRepository<T>,  IRouteDataProvider<T> where T : class, IRouteDataNode
    {
        
        public SqlRouteDataProviderStub(INodeListLookup<T> nodeListLookup, IRouteCreator<T> routeCreator, IExpanditDbFactory expanditDbFactory) : base(expanditDbFactory)
        {
            LookUp = nodeListLookup;
            DefaultRouteCreator = routeCreator;
        }

        public INodeListLookup<T> LookUp { get; private set; }
        public IRouteCreator<T> DefaultRouteCreator { get; private set; }

        public IRouteCreationModule<T> RouteRecreator
        {
            get { throw new NotImplementedException(); }
        }

        #region IRouteDataProvider<T> Members

        public T FillByGroupGuid(int groupGuid, string languageGuid)
        {
            return null;
        }

        public T FillByGroupProductGuid(int groupGuid, string productGuid, string languageGuid)
        {
            return null;
        }

        public T FillByProductGuid(string productGuid, string languageGuid)
        {
            return null;
        }

        public T FillByRoute(string route, HttpContextBase context)
        {
            List<T> myList =
                GetResults<T>("SELECT * FROM RouteDataTable WHERE Route = '" + route.ToLower() + "'").ToList();
            return LookUp.LookUpRoute(myList, route, context);
        }

        public T FillByControllerName(string controllerName)
        {
            throw new NotImplementedException();
        }

        public int GetGroupGuidByProductGuid(string productGuid, string languageGuid)
        {
            throw new NotImplementedException();
        }

        public void Add(T t)
        {
            throw new NotImplementedException();
        }

        public int AddRange(ICollection<T> t)
        {
            throw new NotImplementedException();
        }

        public int AddRangeFlushable(ICollection<T> t){
            throw new NotImplementedException();
        }

        public void RemoveByProductGuid(ICollection<string> guids, string languageGuid)
        {
            throw new NotImplementedException();
        }

        public void RemoveByGroupGuidProductGuid(ICollection<GroupProduct> t)
        {
            throw new NotImplementedException();
        }


        public int RecreateAll()
        {
            throw new NotImplementedException();
        }

        public List<string> GetInternalLinkUrls(string languageGuid)
        {
            throw new NotImplementedException();
        }

        public int Count(){
            throw new NotImplementedException();
        }

        public void RemoveByGroupGuid(ICollection<int> guids)
        {
            throw new NotImplementedException();
        }

        public void RemoveByProductGuid(ICollection<string> guids)
        {
            throw new NotImplementedException();
        }

        #endregion

        public int AddRange(ICollection t)
        {
            throw new NotImplementedException();
        }

        public void Remove(T t)
        {
            throw new NotImplementedException();
        }
    }
}