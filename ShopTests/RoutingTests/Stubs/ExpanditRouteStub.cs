﻿using System.Web;
using System.Web.Routing;
using EISCS.ExpandITFramework.Util;
using EISCS.Routing.Concrete;
using EISCS.Routing.Interfaces;

namespace ShopTests.RoutingTests.Stubs
{
    public class ExpanditRouteStub : Route
    {
        private readonly HttpRuntimeWrapper _httpRuntime;
        private readonly IRouteDataProvider<RouteDataNode> _reverseLookup;

        #region ctor

        public ExpanditRouteStub(string url, IRouteHandler routeHandler, IRouteDataProvider<RouteDataNode> reverseLookup,
                                 HttpRuntimeWrapper httpRuntime)
            : base(url, routeHandler)
        {
            _reverseLookup = reverseLookup;
            _httpRuntime = httpRuntime;
        }

        public ExpanditRouteStub(string url, IRouteHandler routeHandler)
            : base(url, routeHandler)
        {
        }

        public ExpanditRouteStub(string url, RouteValueDictionary defaults, IRouteHandler routeHandler)
            : base(url, defaults, routeHandler)
        {
        }

        public ExpanditRouteStub(string url, RouteValueDictionary defaults, RouteValueDictionary constraints,
                                 IRouteHandler routeHandler)
            : base(url, defaults, constraints, routeHandler)
        {
        }

        public ExpanditRouteStub(string url, RouteValueDictionary defaults, RouteValueDictionary constraints,
                                 RouteValueDictionary dataTokens, IRouteHandler routeHandler)
            : base(url, defaults, constraints, dataTokens, routeHandler)
        {
        }

        #endregion

        public override RouteData GetRouteData(HttpContextBase httpContext)
        {
            //RouteManager.Route.GetLanguage(httpContext);
            string virtualPath =
                string.Concat(httpContext.Request.AppRelativeCurrentExecutionFilePath.Substring(2),
                              httpContext.Request.PathInfo);

            var data =
                new RouteData(this, RouteHandler);

            string lookupVal = "";


            string virtualRoot = _httpRuntime.AppDomainAppVirtualPath.Equals("/")
                                     ? ""
                                     : _httpRuntime.AppDomainAppVirtualPath;
            lookupVal =
                virtualRoot + "/" + virtualPath;

            IRouteDataNode pNode = _reverseLookup.FillByRoute(lookupVal, null);

            if (pNode == null)
            {
                return null;
            }
            if (!string.IsNullOrEmpty(pNode.ProductGuid))
            {
                if (data.Values.ContainsKey("productId"))
                {
                    data.Values["productId"] = pNode.ProductGuid;
                }
                else
                {
                    data.Values.Add("productId", pNode.ProductGuid);
                }
            }
            if (data.Values.ContainsKey("gid"))
            {
                data.Values["gid"] =
                    pNode.GroupGuid.ToString();
            }
            else
            {
                data.Values.Add("gid", pNode.GroupGuid.ToString());
            }


            DataTokens =
                new RouteValueDictionary(data.Values);

            return data;
        }

        public override VirtualPathData GetVirtualPath(RequestContext requestContext, RouteValueDictionary values)
        {
            return base.GetVirtualPath(requestContext, values);
        }
    }
}