﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Security.Principal;
using System.Web;
using System.Web.Routing;
using CmsPublic.DataRepository;
using CmsPublic.ModelLayer.Models;
using EISCS.Catalog.Logic;
using EISCS.ExpandITFramework.Infrastructure.Configuration;
using EISCS.ExpandITFramework.Infrastructure.Configuration.Interfaces;
using EISCS.ExpandITFramework.Util;
using EISCS.Routing;
using EISCS.Routing.Concrete;
using EISCS.Routing.Concrete.RouteCreation;
using EISCS.Routing.Concrete.RoutePatternFormatting;
using EISCS.Routing.Interfaces;
using EISCS.Routing.Providers;
using EISCS.Routing.RouteDataProviders;
using EISCS.Shop.DO;
using EISCS.Shop.DO.Interface;
using EISCS.Shop.DO.Repository;
using EISCS.Shop.DO.Service;
using NUnit.Framework;
using Moq;
using ShopTests.RoutingTests.Stubs;
using ShopTests.SqlCacheDependencyCachingTest.Stubs;

namespace ShopTests.RoutingTests
{
    /// <summary>
    ///This is a test class for RouteDataProviderTest and is intended
    ///to contain all RouteDataProviderTest Unit Tests
    ///</summary>
    [TestFixture]
    public class RouteDataProviderTest
    {
        private ConfigValues _confVal;
        private GroupRepository _groupRepository;

        #region Additional test attributes

        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        [SetUp]
        public void MyTestInitialize()
        {
            //Add items to "AppSettings"
            _confVal = new ConfigValues();
            _confVal.AppSettingsList.Add(new ConfigurationStringNode
                                             {
                                                 Key = "CATALOG_HIDE_NEGLIST",
                                                 Val = "0,-1"
                                             });
            _confVal.AppSettingsList.Add(new ConfigurationStringNode
                                             {
                                                 Key = "CATALOG_SHOW_POSLIST",
                                                 Val = ""
                                             });


            string expanditShopConnectionString = ConfigurationManager.ConnectionStrings["ExpandITConnectionString"].ConnectionString;
            string expanditConnectionStringProviderName = ConfigurationManager.ConnectionStrings["ExpandITConnectionString"].ProviderName;
            ExpanditDbFactory db = new ExpanditDbFactory(expanditShopConnectionString, expanditConnectionStringProviderName);
            IPropertiesDataService propertiesDataService = new PropertiesDataService(db);
            _groupRepository = new GroupRepository(db, propertiesDataService);
        }

        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //

        #endregion

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext { get; set; }

        [Test]
        public void LookupTestIsNotNull()
        {
            var node1 = new RouteDataNode
                            {
                                TypeCode = "$Auto",
                                GroupGuid = 1,
                                LanguageGuid = "sv",
                                ProductGuid = "",
                                RouteUrl = "Home",
                                TargetUrl = "~/templates/group.aspx"
                            };
            var node2 = new RouteDataNode
                            {
                                TypeCode = "$Auto",
                                GroupGuid = 1,
                                LanguageGuid = "sv",
                                ProductGuid = "",
                                RouteUrl = "Home",
                                TargetUrl = "~/templates/group.aspx"
                            };
            var node3 = new RouteDataNode
                            {
                                TypeCode = "$Auto",
                                GroupGuid = 1,
                                LanguageGuid = "sv",
                                ProductGuid = "",
                                RouteUrl = "Home",
                                TargetUrl = "~/templates/group.aspx"
                            };
            var myTestList = new List<RouteDataNode> {node1, node2, node3};

            var nodeListLookup = new NodeListLookupStub<RouteDataNode>();

            IRouteDataNode retNode = nodeListLookup.LookUpRoute(myTestList, "Home", FakeHttpContext());
            Assert.AreEqual("~/templates/group.aspx", retNode.TargetUrl);
            //Assert.IsNotNull(retNode);
        }

        public HttpContextBase FakeHttpContext()
        {
            var context = new Mock<HttpContextBase>();
            var request = new Mock<HttpRequestBase>();
            var response = new Mock<HttpResponseBase>();
            var session = new Mock<HttpSessionStateBase>();
            var server = new Mock<HttpServerUtilityBase>();
            var user = new Mock<IPrincipal>();
            var identity = new Mock<IIdentity>();

            request.Setup(req => req.ApplicationPath).Returns("~/");
            request.Setup(req => req.AppRelativeCurrentExecutionFilePath).Returns("~/g/Home");
            request.Setup(req => req.PathInfo).Returns("");

            response.Setup(res => res.ApplyAppPathModifier(It.IsAny<string>()))
                .Returns((string virtualPath) => virtualPath);
            user.Setup(usr => usr.Identity).Returns(identity.Object);
            identity.SetupGet(ident => ident.IsAuthenticated).Returns(true);

            context.Setup(ctx => ctx.Request).Returns(request.Object);
            context.Setup(ctx => ctx.Response).Returns(response.Object);
            context.Setup(ctx => ctx.Session).Returns(session.Object);
            context.Setup(ctx => ctx.Server).Returns(server.Object);
            context.Setup(ctx => ctx.User).Returns(user.Object);

            return context.Object;
        }


        public HttpContextBase FakeHttpContext2()
        {
            var context = new Mock<HttpContextBase>();
            var request = new Mock<HttpRequestBase>();
            var response = new Mock<HttpResponseBase>();
            var session = new Mock<HttpSessionStateBase>();
            var server = new Mock<HttpServerUtilityBase>();
            var user = new Mock<IPrincipal>();
            var identity = new Mock<IIdentity>();

            request.Setup(req => req.ApplicationPath).Returns("~/");
            request.Setup(req => req.AppRelativeCurrentExecutionFilePath).Returns(
                "~/G/english-movies/unrated-123as/P/the-best-bicyle.aspx");
            request.Setup(req => req.PathInfo).Returns("");

            response.Setup(res => res.ApplyAppPathModifier(It.IsAny<string>()))
                .Returns((string virtualPath) => virtualPath);
            user.Setup(usr => usr.Identity).Returns(identity.Object);
            identity.SetupGet(ident => ident.IsAuthenticated).Returns(true);

            context.Setup(ctx => ctx.Request).Returns(request.Object);
            context.Setup(ctx => ctx.Response).Returns(response.Object);
            context.Setup(ctx => ctx.Session).Returns(session.Object);
            context.Setup(ctx => ctx.Server).Returns(server.Object);
            context.Setup(ctx => ctx.User).Returns(user.Object);

            return context.Object;
        }

        [Test]
        public void TestExpanditRouteStubWithAnyPattern()
        {
            var node1 = new RouteDataNode
                            {
                                TypeCode = "$Auto",
                                GroupGuid = 1,
                                LanguageGuid = "sv",
                                ProductGuid = "",
                                RouteUrl = "/eis42_dev/Home",
                                TargetUrl = "~/templates/group.aspx"
                            };
            var node2 = new RouteDataNode
                            {
                                TypeCode = "Bob",
                                GroupGuid = 10,
                                LanguageGuid = "sv",
                                ProductGuid = "",
                                RouteUrl = "/eis42_dev/Home/Again",
                                TargetUrl = "~/templates/group.aspx"
                            };
            var node3 = new RouteDataNode
                            {
                                TypeCode = "$Auto",
                                GroupGuid = 100,
                                LanguageGuid = "sv",
                                ProductGuid = "",
                                RouteUrl = "/eis42_dev/Away",
                                TargetUrl = "~/templates/group.aspx"
                            };
            var node4 = new RouteDataNode
                            {
                                TypeCode = "$Alias",
                                GroupGuid = 1020,
                                LanguageGuid = "sv",
                                ProductGuid = "8102-01",
                                RouteUrl = "/eis42_dev/Film-Usa-Framling",
                                TargetUrl = "~/templates/product.aspx"
                            };
            var myTestList = new List<RouteDataNode> {node1, node2, node3, node4};

            var routeUrlPatternsInfo = new RouteUrlPatternsInfo
            {
                GroupPattern = "{0}",
                ProductPattern = "{0}",
                GroupProductPattern = "{0}-{1}"
            };
            var charReplacements = new CharReplacements(true);

            string expanditShopConnectionString = ConfigurationManager.ConnectionStrings["ExpandITConnectionString"].ConnectionString;
            string expanditConnectionStringProviderName = ConfigurationManager.ConnectionStrings["ExpandITConnectionString"].ProviderName;
            ExpanditDbFactory db = new ExpanditDbFactory(expanditShopConnectionString, expanditConnectionStringProviderName);
            IPropertiesDataService propertiesDataService = new PropertiesDataService(db);
            IGroupRepository groupRepository = new GroupRepository(db, propertiesDataService);

            INodeListLookup<RouteDataNode> nodeListLookup = new NodeListLookupStub<RouteDataNode>();


            IHideTopGroupInfoContainer hideTopGroupInfoContainer = new HideTopGroupInfoInfoContainer("", "0,-1", "en",
                                                                                                     groupRepository);
            Dictionary<string, Dictionary<int, RouteDataNode>> fastGroupLookup = new Dictionary<string, Dictionary<int, RouteDataNode>>();

            
            IRouteCreator<RouteDataNode> routeCreator = new RouteCreator<RouteDataNode>("", '-', "group.aspx",
                                                                                       "product.aspx",
                                                                                       routeUrlPatternsInfo,
                                                                                       charReplacements, '/', false,
                                                                                       hideTopGroupInfoContainer, new ProductRepository(db, propertiesDataService), fastGroupLookup, propertiesDataService);


            IRouteDataProvider<RouteDataNode> provider = new ListRouteDataProviderStub<RouteDataNode>(myTestList,
                                                                                                      nodeListLookup,
                                                                                                      routeCreator);

            const string expanditGroupProductUrlPattern = ""; //"{gid}";
            var httpruntime = new Mock<HttpRuntimeWrapper>();
            httpruntime.Setup(htr => htr.AppDomainAppVirtualPath).Returns("/eis42_dev");
            var route = new ExpanditRouteStub(expanditGroupProductUrlPattern, new ExpanditRouteHandler(), provider,
                                              httpruntime.Object);
            HttpContextBase myRequest = FakeHttpContext2();
            RouteData data = route.GetRouteData(myRequest);
            string groupGuid = null;
            string productGuid = null;
            if (data != null)
            {
                groupGuid = (string) data.Values["gid"];
                productGuid = (string) data.Values["productId"];
            }
            if (groupGuid != null)
            {
                Debug.WriteLine(groupGuid);
            }
            Assert.AreEqual(null, groupGuid);
            Assert.AreEqual(null, productGuid);
        }

        //[Test]
        //public void TestExpanditRouteHttpRequest() {
        //    RouteDataNode node1 = new RouteDataNode {
        //        TypeCode = "$Auto",
        //        GroupGuid = 1,
        //        Language = "sv",
        //        ProductGuid = "",
        //        Route = "/eis42_dev/g/Home",
        //        Target = "~/templates/group.aspx"
        //    };
        //    RouteDataNode node2 = new RouteDataNode {
        //        TypeCode = "Bob",
        //        GroupGuid = 10,
        //        Language = "sv",
        //        ProductGuid = "",
        //        Route = "/eis42_dev/g/Home",
        //        Target = "~/templates/group.aspx"
        //    };
        //    RouteDataNode node3 = new RouteDataNode {
        //        TypeCode = "$Auto",
        //        GroupGuid = 100,
        //        Language = "sv",
        //        ProductGuid = "",
        //        Route = "/eis42_dev/g/Home",
        //        Target = "~/templates/group.aspx"
        //    };
        //    List<RouteDataNode> myTestList = new List<RouteDataNode> { node1, node2, node3 };

        //    INodeListLookup<RouteDataNode> nodeListLookup = new NodeListLookupStub<RouteDataNode>();

        //    IRouteDataProvider<RouteDataNode> provider = new ListRouteDataProviderStub<RouteDataNode>(myTestList, nodeListLookup);

        //    const string expanditGroupProductUrlPattern = "{marker}/{gid}";
        //    var httpruntime = new Mock<HttpRuntimeWrapper>();
        //    httpruntime.Setup(htr => htr.AppDomainAppVirtualPath).Returns("/eis42_dev");
        //    ExpanditRoute route = new ExpanditRoute(expanditGroupProductUrlPattern, new ExpanditRouteHandler(), provider, httpruntime.Object);
        //    var myRequest = FakeHttpContext();
        //    RouteData data = route.GetRouteData(myRequest);
        //    string groupGuid = null;
        //    if (data != null) {
        //        groupGuid = (string)data.Values["gid"];
        //    }
        //    if (groupGuid != null) {
        //        Debug.WriteLine(groupGuid);
        //    }
        //    Assert.AreEqual("10", groupGuid);
        //}

        [Test]
        public void CreateRoutesTest()
        {
            string expanditShopConnectionString = ConfigurationManager.ConnectionStrings["ExpandITConnectionString"].ConnectionString;
            string expanditConnectionStringProviderName = ConfigurationManager.ConnectionStrings["ExpandITConnectionString"].ProviderName;
            ExpanditDbFactory db = new ExpanditDbFactory(expanditShopConnectionString, expanditConnectionStringProviderName);
            IPropertiesDataService propertiesDataService = new PropertiesDataService(db);
            IGroupRepository groupRepository = new GroupRepository(db, propertiesDataService);

            
            var routeUrlPatternsInfo = new RouteUrlPatternsInfo
                                           {
                                               GroupPattern = "{0}",
                                               ProductPattern = "{0}",
                                               GroupProductPattern = "{0}-{1}"
                                           };
            var charReplacements = new CharReplacements(true);
            const string languageGuid = "en";
            var httpRuntime = new Mock<HttpRuntimeWrapper>();
            httpRuntime.Setup(htr => htr.AppDomainAppVirtualPath).Returns("/eis42_dev");

            IHideTopGroupInfoContainer hideTopGroupInfoContainer = new HideTopGroupInfoInfoContainer("", "0,-1", "en",
                                                                                                     groupRepository);
            

            Dictionary<string, Dictionary<int, RouteDataNode>> fastGroupLookup = new Dictionary<string, Dictionary<int, RouteDataNode>>();
            IRouteCreator<RouteDataNode> routeCreator = new RouteCreator<RouteDataNode>("", '-', "group.aspx",
                                                                                        "product.aspx",
                                                                                        routeUrlPatternsInfo,
                                                                                        charReplacements, '/', false,
                                                                                        hideTopGroupInfoContainer, new ProductRepository(db, propertiesDataService), fastGroupLookup, propertiesDataService);
            var routeDataNodes = new List<RouteDataNode>();
            var catLogic = new CatalogLogic(_confVal, _groupRepository);
            Group group = catLogic.GetCatalogGroupTree(languageGuid);
            Assert.IsNotNull(group);
            //routeCreator.CreateAll(routeDataNodes, group, languageGuid);

            group = catLogic.GetMenuGroupTree(0, languageGuid);
            routeCreator.CreateGroupProductRoutes(routeDataNodes, group, languageGuid);

            foreach (RouteDataNode routeDataNode in routeDataNodes)
            {
                Console.WriteLine(
                    string.Format(
                        "Route: {0,55}\t\tGroupGuid: {1,15}\tProductGuid: {2,15}\tTarget: {3,50}\tLanguage: {4,3}\tTypeCode:{5,10}",
                        routeDataNode.RouteUrl, routeDataNode.GroupGuid, routeDataNode.ProductGuid,
                        routeDataNode.TargetUrl, routeDataNode.LanguageGuid, routeDataNode.TypeCode));
            }
            Assert.IsTrue(routeDataNodes.Count > 0);
            //PublicCrudProvider provider = new PublicCrudProvider();
            //int ret = provider.ClearAllEntries("RouteDataTable");
            //Console.WriteLine("Delete Returned: " + ret);
            //foreach (var routeDataNode in routeDataNodes) {
            //    provider.InsertIntoTable(routeDataNode, "RouteDataTable");
            //}
        }

        [Test]
        public void TestGroupLinkCreationLookup()
        {
            var routeUrlPatternsInfo = new RouteUrlPatternsInfo
            {
                GroupPattern = "{0}",
                ProductPattern = "{0}",
                GroupProductPattern = "{0}-{1}"
            };

            string expanditShopConnectionString = ConfigurationManager.ConnectionStrings["ExpandITConnectionString"].ConnectionString;
            string expanditConnectionStringProviderName = ConfigurationManager.ConnectionStrings["ExpandITConnectionString"].ProviderName;
            ExpanditDbFactory db = new ExpanditDbFactory(expanditShopConnectionString, expanditConnectionStringProviderName);
            IPropertiesDataService propertiesDataService = new PropertiesDataService(db);
            IGroupRepository groupRepository = new GroupRepository(db, propertiesDataService);

            var charReplacements = new CharReplacements(true);
            const string languageGuid = "en";
            var httpRuntime = new Mock<HttpRuntimeWrapper>();
            httpRuntime.Setup(htr => htr.AppDomainAppVirtualPath).Returns("/eis42_dev");
            IHideTopGroupInfoContainer hideTopGroupInfoContainer = new HideTopGroupInfoInfoContainer("", "0,-1", "en",
                                                                                                     groupRepository);
            Dictionary<string, Dictionary<int, RouteDataNode>> fastGroupLookup = new Dictionary<string, Dictionary<int, RouteDataNode>>();

            

            IRouteCreator<RouteDataNode> routeCreator =
                new RouteCreator<RouteDataNode>("", '-', "group.aspx", "product.aspx", routeUrlPatternsInfo,
                                                charReplacements, '/', false, hideTopGroupInfoContainer, new ProductRepository(db, propertiesDataService), fastGroupLookup, propertiesDataService);
            var routeDataNodes = new List<RouteDataNode>();
            Group group = new CatalogLogic(_confVal, _groupRepository).GetCatalogGroupTree(languageGuid);
            Assert.IsNotNull(group, "group is null");
            routeCreator.CreateAll(routeDataNodes, group, languageGuid);
            Assert.IsTrue(routeDataNodes.Count > 0, "nodes = 0");

            INodeListLookup<RouteDataNode> creatorLookup = new NodeListLookupStub<RouteDataNode>();
            IRouteDataNode retNode = creatorLookup.LookUpGroup(routeDataNodes, "11075", languageGuid);

            var nodeListLookupStub = new NodeListLookupStub<RouteDataNode>();
            RouteDataNode node1 = nodeListLookupStub.LookUpGroupProduct(routeDataNodes, "11075", "1000", languageGuid);
            Assert.IsNull(node1);
            
        }
    }
}