﻿using System;
using System.Collections.Generic;
using EISCS.Routing.Concrete.RoutePatternFormatting;
using NUnit.Framework;

namespace ShopTests.RoutingTests.Concrete
{
    /// <summary>
    ///This is a test class for StringParserTest and is intended
    ///to contain all StringParserTest Unit Tests
    ///</summary>
    [TestFixture]
    public class StringParserTest
    {
        private readonly string[] _groupFormat = {"{grouppath}", "{language}"};
        private readonly string[] _groupProductFormat = {"{grouppath}", "{productname}", "{language}"};
        private readonly string[] _productFormat = {"{productname}", "{language}"};

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext { get; set; }

        #region Additional test attributes

        #endregion

        /// <summary>
        ///A test for ReplaceByPlaceholders
        ///</summary>
        [Test]
        public void ReplaceByPlaceholdersTestShouldFindMissingValue()
        {
            var missingNodes = new List<MissingNode>();
            var target = new StringParser();
            const string toSearch = "{notvalid}{GroupGuid}/{ProductGuid}/{Name}/{ProductGuid}";
            string[] placeholders = {"{ProductGuid}", "{Name}", "{GroupGuid}", "{Language}"};
            const string expected = "{2}/{0}/{1}/{0}{3}";
            string actual = target.ReplaceByPlaceholders(toSearch, placeholders, missingNodes);
            bool exists = missingNodes.Exists(x => x.FormatItem == placeholders[3]);
            Assert.IsTrue(exists);
            Assert.AreEqual(expected, actual);
            Console.WriteLine(string.Format("Expected: {0}", expected));
            Console.WriteLine(string.Format("Actual: {0}", actual));
        }

        [Test]
        public void ReplaceByPlaceholdersTestShouldRemoveNotValidFormat()
        {
            var missingNodes = new List<MissingNode>();
            var target = new StringParser();
            const string toSearch = "{notvalid}{grouppath}/{language}/{notvalid}/{productname}";
            string actual = target.ReplaceByPlaceholders(toSearch, _groupProductFormat, missingNodes);
            const string expected = "{0}/{2}//{1}";
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void ReplaceByPlaceholdersTestShouldAddMissingValueLast()
        {
            var missingNodes = new List<MissingNode>();
            var target = new StringParser();
            const string toSearch = "";
            string actual = target.ReplaceByPlaceholders(toSearch, _groupFormat, missingNodes);
            const string expected = "{0}{1}";
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void ReplaceByPlaceHoldersTestShouldFindMissingAndPresentFormats()
        {
            var target = new StringParser();
            const string toSearch = "{productname}";

            var missingNodes = new List<MissingNode>();
            var beginnings = new List<IndexNode>();

            target.ReplaceByPlaceholders(toSearch, _productFormat, missingNodes, beginnings);

            foreach (MissingNode missingNode in missingNodes)
            {
                Console.WriteLine("Missing: " + missingNode.FormatItem);
            }

            foreach (IndexNode indexNode in beginnings)
            {
                Console.WriteLine("Found: " + indexNode.PlaceHolder);
            }

            Assert.IsTrue(missingNodes.Exists(x => x.FormatItem == "{language}"));
            Assert.IsTrue(beginnings.Exists(x => x.PlaceHolder == "{productname}"));
        }
    }
}