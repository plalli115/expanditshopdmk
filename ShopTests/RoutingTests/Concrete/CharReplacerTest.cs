﻿using System.Collections.Generic;
using EISCS.Routing.Concrete.RouteCreation;
using NUnit.Framework;

namespace ShopTests.RoutingTests.Concrete
{
    /// <summary>
    ///This is a test class for CharReplacerTest and is intended
    ///to contain all CharReplacerTest Unit Tests
    ///</summary>
    [TestFixture]
    public class CharReplacerTest
    {
        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext { get; set; }

        #region Additional test attributes

        #endregion

        /// <summary>
        ///A test for ReplaceIllegal
        ///</summary>
        [Test]
        public void ReplaceIllegalTest()
        {
            const string url = "My&Illegal\\Str-- ,./i n.g-";
            const char repl = '-';
            const string expected = "my-illegal-str---i-n-g";
            string actual = CharReplacer.ReplaceIllegal(url, repl);
            Assert.AreEqual(expected, actual);

            string illegalstring = "";
            actual = CharReplacer.ReplaceIllegal(illegalstring, repl);
            Assert.AreEqual(repl.ToString(), actual);
        }

        /// <summary>
        ///A test for ReplaceUnwanted
        ///</summary>
        [Test]
        public void ReplaceUnwantedAndIllegalTest()
        {
            const string url = "My&Illegal\\Str/--  ,.i n.gåä-Ø-Ú-çéÎðñŸþß";

            var charReplacements = new CharReplacements(true);

            const string expected = "my-illegal-str----i-n-gaa-o-u-ceidnyps";
            string actual = CharReplacer.ReplaceUnwanted(url, charReplacements);
            actual = CharReplacer.ReplaceIllegal(actual, '-');
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void ReplaceUnwantedTestCustom()
        {
            const string url = "àÀáÁâÂãÃåÅäÄæÆÇç";
            var node1 = new CharReplacementNode
                            {
                                Replacement = 'a',
                                ToReplace =
                                    new[] {'à', 'À', 'á', 'Á', 'â', 'Â', 'ã', 'Ã', 'å', 'Å', 'ä', 'Ä', 'æ', 'Æ'}
                            };
            var node2 = new CharReplacementNode
                            {
                                Replacement = 'c',
                                ToReplace =
                                    new[] {'Ç', 'ç'}
                            };

            var charReplacementNodes = new List<CharReplacementNode>
                                           {
                                               node1,
                                               node2
                                           };

            var customReplacer = new CharReplacements(false) {ReplacementList = charReplacementNodes};

            const string expected = "aaaaaaaaaaaaaacc";
            string actual = CharReplacer.ReplaceUnwanted(url, customReplacer);
            Assert.AreEqual(expected, actual);
        }
    }
}