﻿using System.Collections.Generic;
using System.Security.Principal;
using System.Web;
using EISCS.Routing.Concrete;
using EISCS.Routing.Interfaces;
using NUnit.Framework;
using Moq;

namespace ShopTests.RoutingTests.Concrete
{
    /// <summary>
    ///This is a test class for NodeListLookupTest and is intended
    ///to contain all NodeListLookupTest Unit Tests
    ///</summary>
    [TestFixture]
    public class NodeListLookupTest
    {
        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext { get; set; }

        #region Additional test attributes

        #endregion

        public HttpContextBase FakeHttpContext2()
        {
            var context = new Mock<HttpContextBase>();
            var request = new Mock<HttpRequestBase>();
            var response = new Mock<HttpResponseBase>();
            var session = new Mock<HttpSessionStateBase>();
            var server = new Mock<HttpServerUtilityBase>();
            var user = new Mock<IPrincipal>();
            var identity = new Mock<IIdentity>();

            request.Setup(req => req.ApplicationPath).Returns("~/");
            request.Setup(req => req.AppRelativeCurrentExecutionFilePath).Returns(
                "~/G/english-movies/unrated-123as/P/the-best-bicyle.aspx");
            request.Setup(req => req.PathInfo).Returns("");

            response.Setup(res => res.ApplyAppPathModifier(It.IsAny<string>()))
                .Returns((string virtualPath) => virtualPath);
            user.Setup(usr => usr.Identity).Returns(identity.Object);
            identity.SetupGet(ident => ident.IsAuthenticated).Returns(true);

            context.Setup(ctx => ctx.Request).Returns(request.Object);

            context.Setup(ctx => ctx.Request.Cookies).Returns(request.Object.Cookies);

            context.Setup(ctx => ctx.Response).Returns(response.Object);
            context.Setup(ctx => ctx.Session).Returns(session.Object);
            context.Setup(ctx => ctx.Server).Returns(server.Object);
            context.Setup(ctx => ctx.User).Returns(user.Object);

            return context.Object;
        }

        /// <summary>
        ///A test for LookUpGroupProduct
        ///</summary>
        public List<T> GetNodeList<T>()
            where T : class, IRouteDataNode, new()
        {
            var node = new T
                           {
                               GroupGuid = 11075,
                               ProductGuid = "1000",
                               TargetUrl = "~/templates/group.aspx",
                               LanguageGuid = "en",
                               RouteUrl = "/G/english-movies/unrated-123as/P/bicyle",
                               TypeCode = "$Auto"
                           };
            var node1 = new T
                            {
                                GroupGuid = 11075,
                                ProductGuid = "1000",
                                TargetUrl = "~/templates/group.aspx",
                                LanguageGuid = "en",
                                RouteUrl = "/G/english-movies/unrated-123as/P/the-best-bicyle",
                                TypeCode = "$Alias"
                            };
            var node2 = new T
                            {
                                GroupGuid = 11075,
                                ProductGuid = "1000",
                                TargetUrl = "~/templates/group.aspx",
                                LanguageGuid = "en",
                                RouteUrl = "/G/english-movies/unrated-123as/P/the-worst-bicyle",
                                TypeCode = "0"
                            };
            var node3 = new T
                            {
                                GroupGuid = 11075,
                                ProductGuid = "1000",
                                TargetUrl = "~/templates/group.aspx",
                                LanguageGuid = "da",
                                RouteUrl = "/G/english-movies/unrated-123as/P/bicyle",
                                TypeCode = "$Auto"
                            };
            var node4 = new T
                            {
                                GroupGuid = 11075,
                                ProductGuid = "1000",
                                TargetUrl = "~/templates/group.aspx",
                                LanguageGuid = "da",
                                RouteUrl = "/G/english-movies/unrated-123as/P/the-best-bicyle",
                                TypeCode = "$Alias"
                            };
            var node5 = new T
                            {
                                GroupGuid = 11075,
                                ProductGuid = "1000",
                                TargetUrl = "~/templates/group.aspx",
                                LanguageGuid = "da",
                                RouteUrl = "/G/english-movies/unrated-123as/P/the-worst-bicyle",
                                TypeCode = "0"
                            };
            var node6 = new T
                            {
                                GroupGuid = 11075,
                                ProductGuid = "1000",
                                TargetUrl = "~/templates/group.aspx",
                                LanguageGuid = null,
                                RouteUrl = "/G/english-movies/unrated-123as/P/bicyle",
                                TypeCode = "$Auto"
                            };
            var node7 = new T
                            {
                                GroupGuid = 11075,
                                ProductGuid = "1000",
                                TargetUrl = "~/templates/group.aspx",
                                LanguageGuid = null,
                                RouteUrl = "/G/english-movies/unrated-123as/P/the-best-bicyle",
                                TypeCode = "$Alias"
                            };
            var node8 = new T
                            {
                                GroupGuid = 11075,
                                ProductGuid = "1000",
                                TargetUrl = "~/templates/group.aspx",
                                LanguageGuid = null,
                                RouteUrl = "/G/english-movies/unrated-123as/P/the-worst-bicyle",
                                TypeCode = "0"
                            };
            var nodeList = new List<T> {node, node1, node2, node3, node4, node5, node6, node7, node8};
            return nodeList;
        }

        [Test]
        public void LookUpGroupProductTest()
        {
            List<RouteDataNode> routeDataNodes = GetNodeList<RouteDataNode>();
            var lookUp = new NodeListLookup<RouteDataNode>("da");
            RouteDataNode node = lookUp.LookUpProduct(routeDataNodes, "1000", "en");
            const string expected = "/G/english-movies/unrated-123as/P/the-best-bicyle";
            Assert.IsNotNull(node);
            string actual = node.RouteUrl;
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void LookUpRouteTest()
        {
            HttpContextBase context = FakeHttpContext2();
            List<RouteDataNode> routeDataNodes = GetNodeList<RouteDataNode>();
            var lookUp = new NodeListLookup<RouteDataNode>("da");
            const string path = "/G/english-movies/unrated-123as/P/the-best-bicyle";
            RouteDataNode node = lookUp.LookUpRoute(routeDataNodes, path, context);
            const string expected = "da";
            string actual = node.LanguageGuid;
            Assert.AreEqual(expected, actual);
        }
    }
}