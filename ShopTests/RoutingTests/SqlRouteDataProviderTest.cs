﻿using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using CmsPublic.DataRepository;
using Dapper;
using EISCS.Routing.Concrete;
using EISCS.Shop.DO;
using EISCS.Shop.DO.Interface;
using NUnit.Framework;

namespace ShopTests.RoutingTests
{
    /// <summary>
    ///This is a test class for SqlRouteDataProviderTest and is intended
    ///to contain all SqlRouteDataProviderTest Unit Tests
    ///</summary>
    [TestFixture]
    public class SqlRouteDataProviderTest
    {
        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext { get; set; }

        
        [Test]
        public void FillByRouteTest()
        {
            string ExpanditShopConnectionString = ConfigurationManager.ConnectionStrings["ExpandITConnectionString"].ConnectionString;
            string ExpanditConnectionStringProviderName = ConfigurationManager.ConnectionStrings["ExpandITConnectionString"].ProviderName;
            IExpanditDbFactory db = new ExpanditDbFactory(ExpanditShopConnectionString, ExpanditConnectionStringProviderName);

            const string tableName = "RouteTable";
            const string route = "%start%";
            
            string sql =
                string.Format("SELECT * FROM {0} WHERE RouteUrl LIKE('{1}')", tableName, route.ToLower());
            List<RouteDataNode> retList = db.GetConnection().Query<RouteDataNode>(sql, new { }).ToList();
            Assert.IsNotNull(retList);
        }
    }
}