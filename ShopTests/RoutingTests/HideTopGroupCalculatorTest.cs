﻿using System.Collections.Generic;
using CmsPublic.ModelLayer.Models;
using EISCS.Routing.Concrete.RouteCreation;
using EISCS.Shop.DO.Interface;
using NUnit.Framework;
using ShopTests.RoutingTests.Stubs;

namespace ShopTests.RoutingTests
{
    /// <summary>
    ///This is a test class for HideTopGroupCalculatorTest and is intended
    ///to contain all HideTopGroupCalculatorTest Unit Tests
    ///</summary>
    [TestFixture]
    public class HideTopGroupCalculatorTest
    {
        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext { get; set; }

        #region Additional test attributes

        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //

        #endregion

        /// <summary>
        ///A test for IsToHideRoot
        ///</summary>
        [Test]
        public void IsToHideRootTestShouldBeTrue()
        {
            const string poslist = "1";
            const string neglist = "0,-1";
            const string defaultLanguage = "en";

            IGroupRepository gr = new MyGroupRepositoryOneCatalog();
            var target = new HideTopGroupInfo(poslist, neglist, defaultLanguage, gr);

            var root = new Group {GroupGuid = 1, ParentGuid = 0};
            var parent = new Group {GroupGuid = 2, ParentGuid = 1, Parent = root};
            var group = new Group {GroupGuid = 3, ParentGuid = 2, Parent = parent};

            const bool expected = true;
            bool actual = target.IsToHideRoot(group);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for IsToHideRoot
        ///</summary>
        [Test]
        public void IsToHideRootTestShouldBeFalse()
        {
            const string poslist = "1,2,3,4,5,6,7";
            const string neglist = "10,-11";
            const string defaultLanguage = "en";

            IGroupRepository gr = new MyGroupRepositoryOneCatalog();
            var target = new HideTopGroupInfo(poslist, neglist, defaultLanguage, gr);

            var root = new Group {GroupGuid = 1, ParentGuid = 0};
            var parent = new Group {GroupGuid = 2, ParentGuid = 1, Parent = root};
            var group = new Group {GroupGuid = 3, ParentGuid = 2, Parent = parent};

            const bool expected = false;
            bool actual = target.IsToHideRoot(group);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for GetRoot
        ///</summary>
        [Test]
        public void GetRootTest()
        {
            const string poslist = "0,-1";
            string neglist = string.Empty;
            const string defaultLanguage = "en";
            IGroupRepository gr = new MyGroupRepositoryOneCatalog();
            var target = new HideTopGroupInfo(poslist, neglist, defaultLanguage, gr);

            var root = new Group {GroupGuid = 1, ParentGuid = 0};
            var parent = new Group {GroupGuid = 2, ParentGuid = 1, Parent = root};
            var group = new Group {GroupGuid = 3, ParentGuid = 2, Parent = parent};

            var dummy = new Group {GroupGuid = 123};

            const int expected = 1;
            int actual = target.GetRoot(group, dummy).ID;
            Assert.AreEqual(expected, actual);
        }

        #region Nested type: MyGroupRepositoryOneCatalog

        public class MyGroupRepositoryOneCatalog : GroupRepositoryBaseStub
        {
            public override List<Group> GetTopGroups(string languageGuid, bool isLoadProperties)
            {
                var grp = new Group {GroupGuid = 10}; // Menu
                var grp2 = new Group {GroupGuid = 1}; // Catalog
                var grp4 = new Group {GroupGuid = 12}; // Menu
                var topGroups = new List<Group> {grp, grp2, grp4};
                return topGroups;
            }
        }

        #endregion

        #region Nested type: MyGroupRepositoryTwoCatalogs

        public class MyGroupRepositoryTwoCatalogs : GroupRepositoryBaseStub
        {
            public override List<Group> GetTopGroups(string languageGuid, bool isLoadProperties)
            {
                var grp = new Group {GroupGuid = 10}; // Menu
                var grp2 = new Group {GroupGuid = 1}; // Catalog
                var grp3 = new Group {GroupGuid = 11}; // Catalog
                var grp4 = new Group {GroupGuid = 12}; // Menu
                var topGroups = new List<Group> {grp, grp2, grp3, grp4};
                return topGroups;
            }
        }

        #endregion
    }
}