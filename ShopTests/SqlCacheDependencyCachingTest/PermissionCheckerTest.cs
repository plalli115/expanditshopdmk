﻿using EISCS.ExpandITFramework.Infrastructure.SqlDependencyCaching;
using NUnit.Framework;

namespace ShopTests.SqlCacheDependencyCachingTest
{
    /// <summary>
    ///This is a test class for PermissionCheckerTest and is intended
    ///to contain all PermissionCheckerTest Unit Tests
    ///</summary>
    [Category("Integration")]
    [TestFixture]
    public class PermissionCheckerTest
    {
        private readonly SqlCacheDependencyPermissionChecker _target = new SqlCacheDependencyPermissionChecker();

        #region Additional test attributes

        #endregion

        /// <summary>
        ///A test for CheckSqlCacheDependencyPermissions
        ///</summary>
        [Test]
        public void CheckSqlCacheDependencyPermissionsTest()
        {
            const bool expected = true;
            bool actual = _target.CheckSqlCacheDependencyPermissions();
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for CheckRoles
        ///</summary>
        [Test]
        public void CheckRolesTest()
        {
            const bool expected = true;
            bool actual = _target.CheckRoles();
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for CheckDDLTriggerPermission
        ///</summary>
        [Test]
        public void CheckDdlTriggerPermissionTest()
        {
            var target = new SqlCacheDependencyPermissionChecker();
            const bool expected = true;
            bool actual = target.CheckDdlTriggerPermission();
            Assert.AreEqual(expected, actual);
        }
    }
}