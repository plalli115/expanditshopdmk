﻿using System;
using System.Configuration;
using System.Linq;
using System.Web.Caching;
using CmsPublic.DataRepository;
using EISCS.ExpandITFramework.Infrastructure.SqlDependencyCaching;
using EISCS.ExpandITFramework.Infrastructure.SqlDependencyCaching.Interfaces;
using EISCS.ExpandITFramework.Util;
using EISCS.Shop.DO.Service;
using NUnit.Framework;
using Moq;
using ShopTests.SqlCacheDependencyCachingTest.Stubs;

namespace ShopTests.SqlCacheDependencyCachingTest
{
    /// <summary>
    ///This is a test class for SqlCacheDependencyManagerTest and is intended
    ///to contain all SqlCacheDependencyManagerTest Unit Tests
    ///</summary>
    [Category("Integration")]
    [TestFixture]
    public class SqlCacheDependencyManagerTest
    {
        public const bool HasRoleValue = false;
        public const bool HasTriggerPermission = false;
        public const bool HasSpPermissions = false;
        private readonly string[] _disabledTables = {"NotValid1", "NotValid2"};

        // Common Test Instances
        private Mock<ISqlCacheDependencyConfigSettingsReader> _configReader;
        private ISqlCacheDependencyContext _context;
        private string[] _enabledTables;
        private PermissionManagerStub _permChecker;
        private Mock<ISqlCacheDependencyDatabaseInfrastructureManager> _sqlCacheDependencyInfraManager;

        // Target
        private SqlCacheDependencyManager _target;

        // TEST VALUES

        public bool Quack
        {
            get { return HasRoleValue; }
        }

        #region Additional test attributes

        [SetUp]
        public void MyTestInitialize()
        {
            _context = new HttpTestContext(); // Real object

            // Check Roles
            const bool hasDdlPermission = true;
            const bool hasRoles = true;
            const bool hasCachePermission = true;

            _permChecker = new PermissionManagerStub(hasDdlPermission, hasRoles, hasCachePermission); // Real object

            _sqlCacheDependencyInfraManager =
                new Mock<ISqlCacheDependencyDatabaseInfrastructureManager>();

            _sqlCacheDependencyInfraManager.Setup(x => x.CreateDdlTrigger()).Returns("");
            _sqlCacheDependencyInfraManager.Setup(x => x.CreateSpRestoreCache()).Returns(true);
            _sqlCacheDependencyInfraManager.Setup(x => x.CreateSqlCacheDependencyStructure()).Returns(true);
            _sqlCacheDependencyInfraManager.Setup(x => x.DeleteSqlCacheDependencyStructure()).Returns("");

            _configReader =
                new Mock<ISqlCacheDependencyConfigSettingsReader>();

            // "Read Config" and get database name
            _configReader.Setup(x => x.ReadConfigCacheSettings()).Returns("EIS43_DEV");

            //Add items to "AppSettings"
            var confVal = new ConfigValues();
            confVal.AppSettingsList.Add(new ConfigurationStringNode
                                            {
                                                Key = "GroupProductRelatedTablesForCaching",
                                                Val =
                                                    "GroupProduct|GroupTable|ProductTable|ProductTranslation|ProductVariant|ProductRelation|PropGrpRel|PropTrans|PropVal"
                                            });
            confVal.AppSettingsList.Add(new ConfigurationStringNode
                                            {
                                                Key = "TablesForCaching",
                                                Val =
                                                    "LanguageTable|CultureTable|CurrencyTable|CurrencyExchange|CompanyTable|CountryTable"
                                            });
            confVal.AppSettingsList.Add(new ConfigurationStringNode
                                            {
                                                Key = "LOCALMODE_USE_CACHE",
                                                Val = "1"
                                            });

            string[] temp1 = confVal.AppSettingsList[0].Val.Split(new[] {'|'});
            string[] temp2 = confVal.AppSettingsList[1].Val.Split(new[] {'|'});
            _enabledTables = temp1.Concat(temp2).ToArray();
            

            ConnectionStringSettings expanditShopConnection = ConfigurationManager.ConnectionStrings["ExpandITConnectionString"];
            IExpanditDbFactory dbFactory = new ExpanditDbFactory(expanditShopConnection.ConnectionString, expanditShopConnection.ProviderName);
            ILegacyDataService legacyDataService = new LegacyDataService(dbFactory, new HttpRuntimeWrapper());

            // Create the target
            _target = new SqlCacheDependencyManager(_context, _configReader.Object,
                                                    _sqlCacheDependencyInfraManager.Object, _permChecker, legacyDataService,
                                                    confVal);
        }

        #endregion


        

        /// <summary>
        ///A test for EnableCaching
        ///</summary>
        [Test]
        public void B1EnableCachingTestShouldBeTrue()
        {
            string tableName = _enabledTables[1];
            const bool expected = true;
            bool actual = _target.EnableCaching(tableName);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for EnableCaching
        ///</summary>
        [Test]
        public void B2EnableCachingOnNonExistingTableTestShouldBFalse()
        {
            const string tableName = "NotExisitngNotEnabled";
            const bool expected = false;
            bool actual = _target.EnableCaching(tableName);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for EnableCaching
        ///</summary>
        [Test]
        public void B3EnableCachingTest1ShouldBeTrue()
        {
            string[] tableNames = _enabledTables;
            bool actual = _target.EnableCaching(tableNames);
            Assert.AreEqual(true, actual);
        }

        /// <summary>
        ///A test for EnableCaching
        ///</summary>
        [Test]
        public void B4EnableCachingOnNonExistingTablesTest1ShouldBeFalse2()
        {
            string[] tableNames = { "NotValid123", "NotValid1234" };
            bool actual = _target.EnableCaching(tableNames);
            Assert.AreEqual(false, actual);
        }

        /// <summary>
        ///A test for InitializeCachedTables
        ///</summary>
        [Test]
        public void B5InitializeCachedTablesTest()
        {
            const bool expected = true;
            bool actual = _target.InitializeCachedTables();
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for EnableCaching
        ///</summary>
        [Test]
        public void B6EnableCachingTest1ShouldBeTrue2()
        {
            bool actual = _target.EnableCaching(_enabledTables);
            Assert.AreEqual(true, actual);
        }
        
        /// <summary>
        /// A test for CacheEnabled
        /// </summary>
        [Test]
        public void CacheEnabledTestShouldBeTrue()
        {
            string tableName = _enabledTables[0];
            const bool expected = true;
            bool actual = _target.CacheEnabled(tableName);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        /// A test for CacheEnabled
        /// </summary>
        [Test]
        public void CacheEnabledTestShouldBeFalse()
        {
            string tableName = _disabledTables[0];
            const bool expected = false;
            bool actual = _target.CacheEnabled(tableName);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for CacheSet
        ///</summary>
        [Test]
        public void CacheSetTestShouldAdd()
        {
            _target.EnableCaching(_enabledTables);
            const string key = "test01";
            object v = "MyValue";
            string tableName = _enabledTables[0];
            int nrInCacheBefore = ((HttpTestContext) _context).Count();
            _target.CacheSet(key, v, tableName);
            int nrInCacheAfter = ((HttpTestContext) _context).Count();
            Assert.IsTrue(nrInCacheAfter > nrInCacheBefore);
        }

        /// <summary>
        ///A test for CacheSet
        ///</summary>
        [Test]
        public void CacheSetTestShouldNotAdd()
        {
            const string key = "test02";
            object v = "MyValue";
            string tableName = _disabledTables[0];
            int nrInCacheBefore = ((HttpTestContext) _context).Count();
            _target.CacheSet(key, v, tableName);
            int nrInCacheAfter = ((HttpTestContext) _context).Count();
            Assert.AreEqual(nrInCacheBefore, nrInCacheAfter);
        }

        /// <summary>
        ///A test for CacheSetAggregated
        ///</summary>
        [Test]
        public void CacheSetAggregatedTestShouldAdd()
        {
            _target.EnableCaching(_enabledTables);
            const string key = "test11";
            object v = "MyValue";
            string[] tableNames = _enabledTables;
            int nrInCacheBefore = ((HttpTestContext) _context).Count();
            bool actual = _target.CacheSetAggregated(key, v, tableNames);
            Assert.IsTrue(actual);
            int nrInCacheAfter = ((HttpTestContext) _context).Count();
            Assert.IsTrue(nrInCacheAfter > nrInCacheBefore);
        }

        /// <summary>
        ///A test for CacheSetAggregated
        ///</summary>
        [Test]
        public void CacheSetAggregatedTestShouldNotAdd()
        {
            const string key = "test12";
            object v = "MyValue";
            string[] tableNames = {"GroupTable", "GroupProduct", "ProductTable", "NotEnabledForCacheDependency"};
            int nrInCacheBefore = ((HttpTestContext) _context).Count();
            _target.CacheSetAggregated(key, v, tableNames);
            int nrInCacheAfter = ((HttpTestContext) _context).Count();
            Assert.AreEqual(nrInCacheBefore, nrInCacheAfter);
        }

        /// <summary>
        ///A test for CacheSetAggregated
        ///</summary>
        [Test]
        public void CacheSetAggregatedTest1ShouldAdd()
        {
            _target.EnableCaching(_enabledTables);
            const string key = "test21";
            object v = "MyValue";
            string[] tableNames = _enabledTables;
            int nrInCacheBefore = ((HttpTestContext) _context).Count();
            bool isAdded = _target.CacheSetAggregated(key, v, tableNames, null);
            Assert.IsTrue(isAdded);
            int nrInCacheAfter = ((HttpTestContext) _context).Count();
            Assert.IsTrue(nrInCacheAfter > nrInCacheBefore);
        }

        /// <summary>
        ///A test for CacheSetAggregated
        ///</summary>
        [Test]
        public void CacheSetAggregatedTest1ShouldNotAdd()
        {
            _target.EnableCaching(_enabledTables);
            const string key = "test22";
            object v = "MyValue";
            string[] tableNames = _disabledTables;
            int nrInCacheBefore = ((HttpTestContext) _context).Count();
            _target.CacheSetAggregated(key, v, tableNames, null);
            int nrInCacheAfter = ((HttpTestContext) _context).Count();
            Assert.AreEqual(nrInCacheBefore, nrInCacheAfter);
        }

        /// <summary>
        ///A test for InsertIntoCache
        ///</summary>
        [Test]
        public void InsertIntoCacheTestShouldAdd()
        {
            _target.EnableCaching(_enabledTables);
            const string key = "TestKey-0";
            object cacheObject = "A string";
            CacheDependency dependency = null;
            var d = new DateTime();
            var t = new TimeSpan();
            const CacheItemPriority cip = new CacheItemPriority();
            CacheItemRemovedCallback circ = null;
            int nrInCacheBefore = ((HttpTestContext) _context).Count();
            _target.InsertIntoCache(key, cacheObject, dependency, d, t, cip, circ);
            int nrInCacheAfter = ((HttpTestContext) _context).Count();
            Assert.IsTrue(nrInCacheAfter > nrInCacheBefore);
        }

        /// <summary>
        ///A test for InsertIntoCache
        ///</summary>
        [Test]
        public void InsertIntoCacheTest1ShouldAdd()
        {
            _target.EnableCaching(_enabledTables);
            string tableName = _enabledTables[1];
            object cacheObject = "A string";
            const string key = "testKey-1";
            AggregateCacheDependency aggDependency = null;
            int nrInCacheBefore = ((HttpTestContext) _context).Count();
            _target.InsertIntoCache(key, cacheObject, aggDependency);
            int nrInCacheAfter = ((HttpTestContext) _context).Count();
            Assert.IsTrue(nrInCacheAfter > nrInCacheBefore);
        }

        /// <summary>
        ///A test for InsertIntoCache
        ///</summary>
        [Test]
        public void InsertIntoCacheTest2ShouldAdd()
        {
            _target.EnableCaching(_enabledTables);
            string tableName = _enabledTables[2];
            object cacheObject = "A string";
            int nrInCacheBefore = ((HttpTestContext) _context).Count();
            _target.InsertIntoCache(tableName, cacheObject);
            int nrInCacheAfter = ((HttpTestContext) _context).Count();
            Assert.IsTrue(nrInCacheAfter > nrInCacheBefore);
        }

        /// <summary>
        ///A test for InsertIntoCache
        ///</summary>
        [Test]
        public void InsertIntoCacheTest3ShouldAdd()
        {
            _target.EnableCaching(_enabledTables);
            const string key = "testKey-3";
            string tableName = _enabledTables[3];
            object cacheObject = "A string";
            int nrInCacheBefore = ((HttpTestContext) _context).Count();
            _target.InsertIntoCache(key, tableName, cacheObject);
            int nrInCacheAfter = ((HttpTestContext) _context).Count();
            Assert.IsTrue(nrInCacheAfter > nrInCacheBefore);
        }

        /// <summary>
        ///A test for InsertIntoCache
        ///</summary>
        [Test]
        public void InsertIntoCacheTest3ShouldNotAdd()
        {
            _target.EnableCaching(_enabledTables);
            const string key = "testKey-3";
            string tableName = _disabledTables[0];
            object cacheObject = "A string";
            int nrInCacheBefore = ((HttpTestContext) _context).Count();
            _target.InsertIntoCache(key, tableName, cacheObject);
            int nrInCacheAfter = ((HttpTestContext) _context).Count();
            Assert.AreEqual(nrInCacheAfter, nrInCacheBefore);
        }

        /// <summary>
        ///A test for DataBaseName
        ///</summary>
        [Test]
        public void DataBaseNameTest()
        {
            const string expected = "EIS43_DEV";
            string actual = _target.DataBaseName;
            Assert.AreEqual(expected, actual);
        }
    }
}