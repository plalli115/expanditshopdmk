﻿using System.ComponentModel;
using EISCS.ExpandITFramework.Infrastructure.SqlDependencyCaching;
using NUnit.Framework;

namespace ShopTests.SqlCacheDependencyCachingTest
{
    /// <summary>
    ///This is a test class for SqlCacheDependencyInfrastructureManagerTest and is intended
    ///to contain all SqlCacheDependencyInfrastructureManagerTest Unit Tests
    ///</summary>
    [NUnit.Framework.Category("Integration")]
    [TestFixture]
    public class SqlCacheDependencyInfrastructureManagerTest
    {
        private readonly SqlCacheDependencyDatabaseInfrastructureManager _target =
            new SqlCacheDependencyDatabaseInfrastructureManager();

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext { get; set; }

        #region Additional test attributes

        #endregion

        [Test]
        public void A1DeleteSqlChacheDependencyStructureTest()
        {
            const bool expected = true;
            string actual = _target.DeleteSqlCacheDependencyStructure();
            Assert.AreEqual(expected, string.IsNullOrEmpty(actual));
        }

        /// <summary>
        ///A test for CreateSqlCacheDependencyStructure
        ///</summary>
        [Test]
        public void B1CreateSqlCacheDependencyStructureTest()
        {
            const bool expected = true;
            bool actual = _target.CreateSqlCacheDependencyStructure();
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for CreateDDLTrigger
        ///</summary>
        [Test]
        public void C1CreateDdlTriggerTest()
        {
            const string expected = "";
            string actual = _target.CreateDdlTrigger();
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for CreateSPTriggers
        ///</summary>
        [Test]
        public void D1CreateSpTriggersTest()
        {
            const bool expected = true;
            bool actual = _target.CreateSpRestoreCache();
            Assert.AreEqual(expected, actual);
        }
    }
}