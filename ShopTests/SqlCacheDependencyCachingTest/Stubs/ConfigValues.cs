﻿using System.Collections.Generic;
using EISCS.ExpandITFramework.Infrastructure.Configuration.Interfaces;

namespace ShopTests.SqlCacheDependencyCachingTest.Stubs
{
    public class ConfigurationStringNode
    {
        public string Key { get; set; }
        public string Val { get; set; }
    }

    public class ConfigValues : IConfigurationValue
    {
        //EXAMPLE USAGE
        //AppSettingsList.Add(new ConfigurationStringNode() { Key = "CATMAN_ACTION_ADD_SELECTED", Val = "Tilføj" });

        public ConfigValues()
        {
            AppSettingsList = new List<ConfigurationStringNode>();
        }

        public List<ConfigurationStringNode> AppSettingsList { get; set; }

        #region IConfigurationValue Members

        public string AppSettings(string key)
        {
            ConfigurationStringNode node = AppSettingsList.Find(k => k.Key.Equals(key));
            return node != null ? node.Val : null;
        }

        #endregion
    }
}