﻿using System.Collections.Generic;

namespace ShopTests.SqlCacheDependencyCachingTest.Stubs
{
    public class AspNetCacheTableStub
    {
        public AspNetCacheTableStub()
        {
            EnabledTables = new List<string>();
        }

        public List<string> EnabledTables { get; private set; }

        public bool Add(string name)
        {
            if (!EnabledTables.Contains(name))
            {
                //System.Diagnostics.Debug.WriteLine(name);
                EnabledTables.Add(name);
            }
            return true;
        }

        public bool Get(string name)
        {
            return EnabledTables.Contains(name);
        }

        public bool Remove(string name)
        {
            if (EnabledTables.Contains(name))
            {
                EnabledTables.Remove(name);
                return true;
            }
            return false;
        }
    }
}