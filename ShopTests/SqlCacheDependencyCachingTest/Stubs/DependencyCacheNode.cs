﻿using System.Web.Caching;

namespace ShopTests.SqlCacheDependencyCachingTest.Stubs
{
    public class DependencyCacheNode
    {
        public DependencyCacheNode(object value, CacheDependency dependency)
        {
            Value = value;
            Dependency = dependency;
        }

        public object Value { get; set; }
        public CacheDependency Dependency { get; set; }
    }
}