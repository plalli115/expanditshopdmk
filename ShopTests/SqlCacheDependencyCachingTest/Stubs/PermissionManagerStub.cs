﻿using EISCS.ExpandITFramework.Infrastructure.SqlDependencyCaching.Interfaces;

namespace ShopTests.SqlCacheDependencyCachingTest.Stubs
{
    public class PermissionManagerStub : ISqlCacheDependencyPermissionChecker
    {
        private readonly bool _hasCachePermission;
        private readonly bool _hasDdlPermission;
        private readonly bool _hasRoles;

        public PermissionManagerStub(bool hasDdlPermission, bool hasRoles, bool hasCachePermission)
        {
            _hasDdlPermission = hasDdlPermission;
            _hasRoles = hasRoles;
            _hasCachePermission = hasCachePermission;
        }

        #region ISqlCacheDependencyPermissionChecker Members

        public bool CheckDdlTriggerPermission()
        {
            return _hasDdlPermission;
        }

        public bool CheckRoles()
        {
            return _hasRoles;
        }

        public bool CheckSqlCacheDependencyPermissions()
        {
            return _hasCachePermission;
        }

        #endregion
    }
}