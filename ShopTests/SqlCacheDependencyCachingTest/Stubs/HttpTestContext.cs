﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web.Caching;
using EISCS.ExpandITFramework.Infrastructure.SqlDependencyCaching.Interfaces;

namespace ShopTests.SqlCacheDependencyCachingTest.Stubs
{
    internal class HttpTestContext : ISqlCacheDependencyContext
    {
        #region ISqlCacheDependencyContext Members

        public string MapPath(string path)
        {
            return Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "ShopTests.dll.config");
        }

        public void CacheInsert(string key, object value, CacheDependency dependencies)
        {
            Cache.Add(key, value, dependencies);
        }

        public void CacheInsert(string key, object value, CacheDependency dependencies,
                                DateTime absoluteExpiration, TimeSpan slidingExpiration)
        {
            Cache.Add(key, value, dependencies);
        }

        public void CacheInsert(string key, object value, CacheDependency dependencies,
                                DateTime absoluteExpiration, TimeSpan slidingExpiration,
                                CacheItemUpdateCallback onUpdateCallback)
        {
            Cache.Add(key, value, dependencies);
        }

        public void CacheInsert(string key, object value, CacheDependency dependencies,
                                DateTime absoluteExpiration, TimeSpan slidingExpiration, CacheItemPriority priority,
                                CacheItemRemovedCallback onRemoveCallback)
        {
            Cache.Add(key, value, dependencies);
        }

        public object CacheGet(string key)
        {
            throw new NotImplementedException();
        }

        #endregion

        public int Count()
        {
            return Cache.Count();
        }

        public object Value(string key)
        {
            return Cache.Get(key);
        }

        public Dictionary<string, DependencyCacheNode> Dict()
        {
            return Cache.Dict();
        }

        #region Nested type: Cache

        private static class Cache
        {
            private static readonly Dictionary<string, DependencyCacheNode> MyCache =
                new Dictionary<string, DependencyCacheNode>(10);

            public static void Add(string key, object value, CacheDependency dependency)
            {
                if (MyCache.ContainsKey(key)) return;
                var node = new DependencyCacheNode(value, dependency);
                MyCache.Add(key, node);
            }

            public static object Get(string key)
            {
                return MyCache.ContainsKey(key) ? MyCache[key].Value : null;
            }

            public static int Count()
            {
                return MyCache.Count;
            }

            public static Dictionary<string, DependencyCacheNode> Dict()
            {
                return MyCache;
            }
        }

        #endregion
    }
}