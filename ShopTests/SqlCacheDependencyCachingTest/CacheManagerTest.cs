﻿using System;
using System.Configuration;
using System.Threading;
using System.Web.Caching;
using CmsPublic.DataRepository;
using EISCS.ExpandITFramework.Infrastructure;
using EISCS.ExpandITFramework.Util;
using EISCS.Shop.DO.Service;
using NUnit.Framework;
using ShopTests.SqlCacheDependencyCachingTest.Stubs;

namespace ShopTests.SqlCacheDependencyCachingTest
{
    /// <summary>
    ///This is a test class for CacheManagerTest and is intended
    ///to contain all CacheManagerTest Unit Tests
    ///</summary>
    [Category("Integration")]
    [TestFixture]
    public class CacheManagerTest
    {
        private HttpTestContext _context;

        #region Additional test attributes

        [SetUp]
        public void MyTestInitialize()
        {
            //SqlCacheDependencyDatabaseInfrastructureManager m = new SqlCacheDependencyDatabaseInfrastructureManager();
            //m.DeleteSqlCacheDependencyStructure();
            ConnectionStringSettings expanditShopConnection = ConfigurationManager.ConnectionStrings["ExpandITConnectionString"];
            IExpanditDbFactory dbFactory = new ExpanditDbFactory(expanditShopConnection.ConnectionString, expanditShopConnection.ProviderName);
            ILegacyDataService legacyDataService = new LegacyDataService(dbFactory, new HttpRuntimeWrapper());
            _context = new HttpTestContext();
            CacheManager.Initialize(_context, legacyDataService);
        }

        #endregion

        /// <summary>
        ///A test for CacheEnabled
        ///</summary>
        [Test]
        public void CacheEnabledTest()
        {
            const string tableName = "ProductTable";
            bool actual = CacheManager.CacheEnabled(tableName);
            Assert.AreEqual(true, actual);
        }
        
        /// <summary>
        ///A test for InitializeCachedTables
        ///</summary>
        [Test]
        public void A5InitializeCachedTablesTest()
        {
            bool actual = CacheManager.InitializeCachedTables();
            Assert.AreEqual(true, actual);
        }

        /// <summary>
        ///A test for disableCaching
        ///</summary>
        [Test]
        public void A1DisableCachingTest()
        {
            string[] tableNames = {"GroupProduct", "ProductTable"};
            bool actual = CacheManager.DisableCaching(tableNames);
            Assert.AreEqual(true, actual);
        }

        /// <summary>
        ///A test for disableCaching
        ///</summary>
        [Test]
        public void A2DisableCachingTest1()
        {
            const string tableName = "ProductTable";
            bool actual = CacheManager.DisableCaching(tableName);
            Assert.AreEqual(true, actual);
        }

        /// <summary>
        ///A test for enableCaching
        ///</summary>
        [Test]
        public void A3EnableCachingTest()
        {
            const string tableName = "ProductTable";
            bool actual = CacheManager.EnableCaching(tableName);
            Assert.AreEqual(true, actual);
        }

        /// <summary>
        ///A test for enableCaching
        ///</summary>
        [Test]
        public void A4EnableCachingTest1()
        {
            string[] tableNames = {"GroupProduct", "ProductTable"};
            bool actual = CacheManager.EnableCaching(tableNames);
            Assert.AreEqual(true, actual);
        }
    }
}