﻿using EISCS.ExpandITFramework.Infrastructure.SqlDependencyCaching;
using EISCS.ExpandITFramework.Infrastructure.SqlDependencyCaching.Interfaces;
using NUnit.Framework;
using ShopTests.SqlCacheDependencyCachingTest.Stubs;

namespace ShopTests.SqlCacheDependencyCachingTest
{
    /// <summary>
    ///This is a test class for ConfigCacheSettingsReaderTest and is intended
    ///to contain all ConfigCacheSettingsReaderTest Unit Tests
    ///</summary>
    [Category("Integration")]
    [TestFixture]
    public class ConfigCacheSettingsReaderTest
    {
       

        #region Additional test attributes

        #endregion

        /// <summary>
        ///A test for ReadConfigCacheSettings
        ///</summary>
        [Test]
        public void ReadConfigCacheSettingsTest()
        {
            ISqlCacheDependencyContext context = new HttpTestContext();
            const string connectionStringName = "ExpandITConnectionString";
            var target = new SqlCacheDependencyConfigSettingsReader(context, connectionStringName);
            const string expected = "EIS43_DEV";
            string actual = target.ReadConfigCacheSettings();
            Assert.AreEqual(expected, actual);
        }
    }
}