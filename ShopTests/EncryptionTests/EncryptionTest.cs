﻿using System;
using System.Configuration;
using EISCS.ExpandITFramework.Util;
using EISCS.Shop.DO.Interface;
using EISCS.Shop.DO.Repository;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;

namespace ShopTests.EncryptionTests
{
    [TestFixture]
    public class EncryptionTest
    {
        [Test]
        public void TestDifferentSaltSamePassword_HMAC_MD5()
        {
            string textToEncrypt1 = "1234";
            string encryptionType1 = "HMAC_MD5";
            string salt1 = Encryption.GenerateSaltValue();
            string textToEncrypt2 = "1234";
            string encryptionType2 = "HMAC_MD5";
            string salt2 = Encryption.GenerateSaltValue();

            var actual1 = Encryption.Encrypt(textToEncrypt1, salt1, encryptionType1);
            var actual2 = Encryption.Encrypt(textToEncrypt2, salt2, encryptionType2);
            Assert.AreNotEqual(actual1, actual2);
            Assert.IsNotNull(actual1);
            Assert.IsNotNull(actual2);
        }

        [Test]
        public void TestDifferentSaltSamePassword_HMAC_SHA1()
        {
            string textToEncrypt1 = "1234";
            string encryptionType1 = "HMAC_SHA1";
            string salt1 = Encryption.GenerateSaltValue();
            string textToEncrypt2 = "1234";
            string encryptionType2 = "HMAC_SHA1";
            string salt2 = Encryption.GenerateSaltValue();

            var actual1 = Encryption.Encrypt(textToEncrypt1, salt1, encryptionType1);
            var actual2 = Encryption.Encrypt(textToEncrypt2, salt2, encryptionType2);
            Assert.AreNotEqual(actual1, actual2);
            Assert.IsNotNull(actual1);
            Assert.IsNotNull(actual2);
        }

        [Test]
        public void TestDifferentSaltSamePassword_HMAC_SHA256()
        {
            string textToEncrypt1 = "1234";
            string encryptionType1 = "HMAC_SHA256";
            string salt1 = Encryption.GenerateSaltValue();
            string textToEncrypt2 = "1234";
            string encryptionType2 = "HMAC_SHA256";
            string salt2 = Encryption.GenerateSaltValue();

            var actual1 = Encryption.Encrypt(textToEncrypt1, salt1, encryptionType1);
            var actual2 = Encryption.Encrypt(textToEncrypt2, salt2, encryptionType2);
            Assert.AreNotEqual(actual1, actual2);
            Assert.IsNotNull(actual1);
            Assert.IsNotNull(actual2);
        }

        [Test]
        public void TestDifferentSaltSamePassword_HMAC_SHA384()
        {
            string textToEncrypt1 = "1234";
            string encryptionType1 = "HMAC_SHA384";
            string salt1 = Encryption.GenerateSaltValue();
            string textToEncrypt2 = "1234";
            string encryptionType2 = "HMAC_SHA384";
            string salt2 = Encryption.GenerateSaltValue();

            var actual1 = Encryption.Encrypt(textToEncrypt1, salt1, encryptionType1);
            var actual2 = Encryption.Encrypt(textToEncrypt2, salt2, encryptionType2);
            Assert.AreNotEqual(actual1, actual2);
            Assert.IsNotNull(actual1);
            Assert.IsNotNull(actual2);
        }

        [Test]
        public void TestDifferentSaltSamePassword_HMAC_SHA512()
        {
            string textToEncrypt1 = "1234";
            string encryptionType1 = "HMAC_SHA512";
            string salt1 = Encryption.GenerateSaltValue();
            string textToEncrypt2 = "1234";
            string encryptionType2 = "HMAC_SHA512";
            string salt2 = Encryption.GenerateSaltValue();

            var actual1 = Encryption.Encrypt(textToEncrypt1, salt1, encryptionType1);
            var actual2 = Encryption.Encrypt(textToEncrypt2, salt2, encryptionType2);
            Assert.AreNotEqual(actual1, actual2);
            Assert.IsNotNull(actual1);
            Assert.IsNotNull(actual2);
        }

        [Test]
        public void TestDifferentSaltSamePassword_HMAC_RIPEMD160()
        {
            string textToEncrypt1 = "1234";
            string encryptionType1 = "HMAC_RIPEMD160";
            string salt1 = Encryption.GenerateSaltValue();
            string textToEncrypt2 = "1234";
            string encryptionType2 = "HMAC_RIPEMD160";
            string salt2 = Encryption.GenerateSaltValue();

            var actual1 = Encryption.Encrypt(textToEncrypt1, salt1, encryptionType1);
            var actual2 = Encryption.Encrypt(textToEncrypt2, salt2, encryptionType2);
            Assert.AreNotEqual(actual1, actual2);
            Assert.IsNotNull(actual1);
            Assert.IsNotNull(actual2);
        }

        [Test]
        public void TestDifferentSaltSamePassword_BCrypt()
        {
            string textToEncrypt1 = "1234";
            string encryptionType1 = "BCrypt";

            // BCrypt uses a specialized salt, so it gets it salt in the Encrypt method
            string salt1 = String.Empty;
            string textToEncrypt2 = "1234";
            string encryptionType2 = "BCrypt";


            // BCrypt uses a specialized salt, so it gets it salt in the Encrypt method
            string salt2 = String.Empty;

            var actual1 = Encryption.Encrypt(textToEncrypt1, salt1, encryptionType1);
            var actual2 = Encryption.Encrypt(textToEncrypt2, salt2, encryptionType2);
            Assert.AreNotEqual(actual1, actual2);
            Assert.IsNotNull(actual1);
            Assert.IsNotNull(actual2);
        }

        [Test]
        public void TestEncryptPassword_Valid_ClearText()
        {
            string textToEncrypt1 = "1234";
            string encryptionType1 = "ClearText";
            string salt1 = Encryption.GenerateSaltValue();
            string textToEncrypt2 = "1234";
            string encryptionType2 = "ClearText";

            var actual1 = Encryption.Encrypt(textToEncrypt1, salt1, encryptionType1);
            var actual2 = Encryption.Encrypt(textToEncrypt2, salt1, encryptionType2);
            Assert.AreEqual(actual1, actual2);
        }

        [Test]
        public void TestEncryptPassword_Valid_XOR()
        {
            string textToEncrypt1 = "1234";
            string encryptionType1 = "XOR";
            string salt1 = Encryption.GenerateSaltValue();
            string textToEncrypt2 = "1234";
            string encryptionType2 = "XOR";

            var actual1 = Encryption.Encrypt(textToEncrypt1, salt1, encryptionType1);
            var actual2 = Encryption.Encrypt(textToEncrypt2, salt1, encryptionType2);
            Assert.AreEqual(actual1, actual2);
        }

        [Test]
        public void TestEncryptPassword_Valid_HMAC_MD5()
        {
            string textToEncrypt1 = "1234";
            string encryptionType1 = "HMAC_MD5";
            string salt1 = Encryption.GenerateSaltValue();
            string textToEncrypt2 = "1234";
            string encryptionType2 = "HMAC_MD5";

            var actual1 = Encryption.Encrypt(textToEncrypt1, salt1, encryptionType1);
            var actual2 = Encryption.Encrypt(textToEncrypt2, salt1, encryptionType2);
            Assert.AreEqual(actual1, actual2);
        }

        [Test]
        public void TestEncryptPassword_Valid_HMAC_SHA1()
        {
            string textToEncrypt1 = "1234";
            string encryptionType1 = "HMAC_SHA1";
            string salt1 = Encryption.GenerateSaltValue();
            string textToEncrypt2 = "1234";
            string encryptionType2 = "HMAC_SHA1";

            var actual1 = Encryption.Encrypt(textToEncrypt1, salt1, encryptionType1);
            var actual2 = Encryption.Encrypt(textToEncrypt2, salt1, encryptionType2);
            Assert.AreEqual(actual1, actual2);
        }

        [Test]
        public void TestEncryptPassword_Valid_HMAC_SHA256()
        {
            string textToEncrypt1 = "1234";
            string encryptionType1 = "HMAC_SHA256";
            string salt1 = Encryption.GenerateSaltValue();
            string textToEncrypt2 = "1234";
            string encryptionType2 = "HMAC_SHA256";

            var actual1 = Encryption.Encrypt(textToEncrypt1, salt1, encryptionType1);
            var actual2 = Encryption.Encrypt(textToEncrypt2, salt1, encryptionType2);
            Assert.AreEqual(actual1, actual2);
        }

        [Test]
        public void TestEncryptPassword_Valid_HMAC_SHA384()
        {
            string textToEncrypt1 = "1234";
            string encryptionType1 = "HMAC_SHA384";
            string salt1 = Encryption.GenerateSaltValue();
            string textToEncrypt2 = "1234";
            string encryptionType2 = "HMAC_SHA384";

            var actual1 = Encryption.Encrypt(textToEncrypt1, salt1, encryptionType1);
            var actual2 = Encryption.Encrypt(textToEncrypt2, salt1, encryptionType2);
            Assert.AreEqual(actual1, actual2);
        }

        [Test]
        public void TestEncryptPassword_Valid_HMAC_SHA512()
        {
            string textToEncrypt1 = "1234";
            string encryptionType1 = "HMAC_SHA512";
            string salt1 = Encryption.GenerateSaltValue();
            string textToEncrypt2 = "1234";
            string encryptionType2 = "HMAC_SHA512";

            var actual1 = Encryption.Encrypt(textToEncrypt1, salt1, encryptionType1);
            var actual2 = Encryption.Encrypt(textToEncrypt2, salt1, encryptionType2);
            Assert.AreEqual(actual1, actual2);
        }

        [Test]
        public void TestEncryptPassword_Valid_HMAC_RIPEMD160()
        {
            string textToEncrypt1 = "1234";
            string encryptionType1 = "HMAC_RIPEMD160";
            string salt1 = Encryption.GenerateSaltValue();
            string textToEncrypt2 = "1234";
            string encryptionType2 = "HMAC_RIPEMD160";

            var actual1 = Encryption.Encrypt(textToEncrypt1, salt1, encryptionType1);
            var actual2 = Encryption.Encrypt(textToEncrypt2, salt1, encryptionType2);
            Assert.AreEqual(actual1, actual2);
        }

        [Test]
        public void TestEncryptPassword_Valid_BCrypt()
        {
            string textToEncrypt1 = "1234";
            string salt1 = BCrypt.GenerateSalt();
            string textToEncrypt2 = "1234";

            var actual1 = BCrypt.HashPassword(textToEncrypt1, salt1);
            var actual2 = BCrypt.HashPassword(textToEncrypt2, salt1);
            Assert.AreEqual(actual1, actual2);
        }

        [Test]
        public void TestEncryptPassword_NotValid_ClearText()
        {
            string textToEncrypt1 = "1234";
            string encryptionType1 = "ClearText";
            string salt1 = Encryption.GenerateSaltValue();
            string textToEncrypt2 = "12345";
            string encryptionType2 = "ClearText";

            var actual1 = Encryption.Encrypt(textToEncrypt1, salt1, encryptionType1);
            var actual2 = Encryption.Encrypt(textToEncrypt2, salt1, encryptionType2);
            Assert.AreNotEqual(actual1, actual2);
        }

        [Test]
        public void TestEncryptPassword_NotValid_XOR()
        {
            string textToEncrypt1 = "1234";
            string encryptionType1 = "XOR";
            string salt1 = Encryption.GenerateSaltValue();
            string textToEncrypt2 = "12345";
            string encryptionType2 = "XOR";

            var actual1 = Encryption.Encrypt(textToEncrypt1, salt1, encryptionType1);
            var actual2 = Encryption.Encrypt(textToEncrypt2, salt1, encryptionType2);
            Assert.AreNotEqual(actual1, actual2);
        }

        [Test]
        public void TestEncryptPassword_NotValid_HMAC_MD5()
        {
            string textToEncrypt1 = "1234";
            string encryptionType1 = "HMAC_MD5";
            string salt1 = Encryption.GenerateSaltValue();
            string textToEncrypt2 = "12345";
            string encryptionType2 = "HMAC_MD5";

            var actual1 = Encryption.Encrypt(textToEncrypt1, salt1, encryptionType1);
            var actual2 = Encryption.Encrypt(textToEncrypt2, salt1, encryptionType2);
            Assert.AreNotEqual(actual1, actual2);
        }

        [Test]
        public void TestEncryptPassword_NotValid_HMAC_SHA1()
        {
            string textToEncrypt1 = "1234";
            string encryptionType1 = "HMAC_SHA1";
            string salt1 = Encryption.GenerateSaltValue();
            string textToEncrypt2 = "12345";
            string encryptionType2 = "HMAC_SHA1";

            var actual1 = Encryption.Encrypt(textToEncrypt1, salt1, encryptionType1);
            var actual2 = Encryption.Encrypt(textToEncrypt2, salt1, encryptionType2);
            Assert.AreNotEqual(actual1, actual2);
        }

        [Test]
        public void TestEncryptPassword_NotValid_HMAC_SHA256()
        {
            string textToEncrypt1 = "1234";
            string encryptionType1 = "HMAC_SHA256";
            string salt1 = Encryption.GenerateSaltValue();
            string textToEncrypt2 = "12345";
            string encryptionType2 = "HMAC_SHA256";

            var actual1 = Encryption.Encrypt(textToEncrypt1, salt1, encryptionType1);
            var actual2 = Encryption.Encrypt(textToEncrypt2, salt1, encryptionType2);
            Assert.AreNotEqual(actual1, actual2);
        }

        [Test]
        public void TestEncryptPassword_NotValid_HMAC_SHA384()
        {
            string textToEncrypt1 = "1234";
            string encryptionType1 = "HMAC_SHA384";
            string salt1 = Encryption.GenerateSaltValue();
            string textToEncrypt2 = "12345";
            string encryptionType2 = "HMAC_SHA384";

            var actual1 = Encryption.Encrypt(textToEncrypt1, salt1, encryptionType1);
            var actual2 = Encryption.Encrypt(textToEncrypt2, salt1, encryptionType2);
            Assert.AreNotEqual(actual1, actual2);
        }

        [Test]
        public void TestEncryptPassword_NotValid_HMAC_SHA512()
        {
            string textToEncrypt1 = "1234";
            string encryptionType1 = "HMAC_SHA512";
            string salt1 = Encryption.GenerateSaltValue();
            string textToEncrypt2 = "12345";
            string encryptionType2 = "HMAC_SHA512";

            var actual1 = Encryption.Encrypt(textToEncrypt1, salt1, encryptionType1);
            var actual2 = Encryption.Encrypt(textToEncrypt2, salt1, encryptionType2);
            Assert.AreNotEqual(actual1, actual2);
        }

        [Test]
        public void TestEncryptPassword_NotValid_HMAC_RIPEMD160()
        {
            string textToEncrypt1 = "1234";
            string encryptionType1 = "HMAC_RIPEMD160";
            string salt1 = Encryption.GenerateSaltValue();
            string textToEncrypt2 = "12345";
            string encryptionType2 = "HMAC_RIPEMD160";

            var actual1 = Encryption.Encrypt(textToEncrypt1, salt1, encryptionType1);
            var actual2 = Encryption.Encrypt(textToEncrypt2, salt1, encryptionType2);
            Assert.AreNotEqual(actual1, actual2);
        }

        [Test]
        public void TestEncryptPassword_NotValid_BCrypt()
        {
            string textToEncrypt1 = "1234";
            string encryptionType1 = "BCrypt";
            string salt1 = Encryption.GenerateSaltValue();
            string textToEncrypt2 = "12345";
            string encryptionType2 = "BCrypt";

            var actual1 = Encryption.Encrypt(textToEncrypt1, salt1, encryptionType1);
            var actual2 = Encryption.Encrypt(textToEncrypt2, salt1, encryptionType2);
            Assert.AreNotEqual(actual1, actual2);
        }
    }
}
