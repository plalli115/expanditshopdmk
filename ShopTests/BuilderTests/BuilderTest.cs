﻿using System;
using System.Configuration;
using System.Linq;
using CmsPublic.DataRepository;
using Dapper;
using EISCS.Shop.DO;
using EISCS.Shop.DO.Interface;
using NUnit.Framework;

namespace ShopTests.BuilderTests
{
    [TestFixture]
    public class BuilderTest
    {
        [SetUp]
        public void Setup()
        {
            ExpanditDbFactory = new ExpanditDbFactory(ExpanditShopConnectionString, ExpanditConnectionStringProviderName);
        }

        public static IExpanditDbFactory ExpanditDbFactory;
        public static string ExpanditShopConnectionString =
            ConfigurationManager.ConnectionStrings["ExpandITConnectionString"].ConnectionString;

        public static string ExpanditConnectionStringProviderName =
            ConfigurationManager.ConnectionStrings["ExpandITConnectionString"].ProviderName;

        [Test]
        public void TestConversion()
        {
            var myTable = ExpanditDbFactory.GetConnection().Query<MyProductTable>("SELECT ListPrice, IsTax, FromDate, SomeDecimal FROM PriceTest WHERE ProductGuid = 1").First();
            Assert.IsNotNull(myTable);
            Console.WriteLine(myTable.ListPrice);
            Console.WriteLine(myTable.IsTax);
            Console.WriteLine(myTable.FromDate);
            Console.WriteLine(myTable.SomeDecimal);
        }
    }

    public class MyProductTable
    {
        public double ListPrice { get; set; }
        public bool IsTax { get; set; }
        public DateTime FromDate { get; set; }
        public float SomeDecimal { get; set; }
    }
}
