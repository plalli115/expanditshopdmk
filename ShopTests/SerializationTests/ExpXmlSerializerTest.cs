﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EISCS.ExpandIT.Serialization;
using ExpandIT;
using NUnit.Framework;

namespace ShopTests.SerializationTests
{
    [TestFixture]
    public class ExpXmlSerializerTest
    {

        private ExpDictionary _sut;
        private string _serialized;

        [Test]
        public void SerializeDictionary()
        {
            _sut = new ExpDictionary{{"Key_1", "Value_1"}};
            _serialized = Marshall.MarshallDictionary(_sut);
            Assert.IsNotNull(_serialized);
        }

        [Test]
        public void DeserializeDictionary()
        {
            SerializeDictionary();
            ExpDictionary result = Marshall.UnmarshallDictionary(_serialized) as ExpDictionary;
            Assert.IsNotNull(result);
            Assert.IsTrue((string) _sut["Key_1"] == "Value_1");
        }

        [Test]
        public void DeserializeString()
        {
            string toDeserialize =
                "H4sIAAAAAAAEAIWPsQrCMBRFd8F/CNlN2loVQ5ri4KTiIIhrbKMNpGlJUtP+vaGiiIvbe7xz3+HSvK8VeAhjZaMzGKMI5mw6oRtj+HC8bbUzAwiItqS3MoOVcy3B2HuP/Bw15o6TKIrx5bA/FZWo+Uxq67guBPykyv8pGJQA0NE2jmHZiSC2krihFRkMb0jZdFclIEsWFIfrGzxz1YlfVBSy5gqyFKXL9SoERuqlwW8Pxd812RNGrbGwDAEAAA==";
            ExpDictionary result = Marshall.UnmarshallDictionary(toDeserialize) as ExpDictionary;
            Assert.IsNotNull(result);
            foreach (KeyValuePair<object, object> pair in result)
            {
                Console.WriteLine(pair.Key + " " + pair.Value);
            }
        }
    }
}
