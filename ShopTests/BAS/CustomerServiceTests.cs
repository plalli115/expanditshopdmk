﻿//using System.Collections.Generic;
//using System.Linq;
//using EISCS.Shop.DO.BAS.Dto;
//using EISCS.Shop.DO.BAS.Interface;
//using EISCS.Shop.DO.BAS.Service;
//using Moq;
//using NUnit.Framework;

//namespace ShopTests.BAS
//{
//    [TestFixture]
//    public class CustomerServiceTests
//    {
//        [SetUp]
//        public void SetUp()
//        {
//            string basConnectionString = string.Empty;
//            _customerPropertiesRepositoryMock = new Mock<ICustomerPropertiesRepository>();
//            _customerPropertiesRepositoryMock.Setup(m => m.All())
//                                             .Returns(new List<CustomerPropertyItem>
//                                                 {
//                                                     new CustomerPropertyItem {CustomerId = "unknown"},
//                                                     new CustomerPropertyItem {CustomerId = DefaultTestCustomerId},
//                                                     new CustomerPropertyItem {CustomerId = DefaultTestCustomerId}
//                                                 });

//            _sut = new CustomerService(basConnectionString, 
//                                        new Mock<ICustomerRepository>().Object, 
//                                        _customerPropertiesRepositoryMock.Object,
//                                        new Mock<ICustomerGroupService>().Object,   
//                                        new Mock<IUserService>().Object);
//        }

//        private CustomerService _sut;
//        private const string DefaultTestCustomerId = "defaultTestCustomer";
//        private Mock<ICustomerPropertiesRepository> _customerPropertiesRepositoryMock;

//        [Test]
//        public void GetPropertiesByCustomerId_WhenCalled_FiltersOnCustomerId()
//        {
//            IEnumerable<CustomerPropertyItem> items = _sut.GetPropertiesByCustomerId(DefaultTestCustomerId);

//            Assert.That(items.Count(i => i.CustomerId == DefaultTestCustomerId) == 2, "Should contain two items");
//        }
//    }
//}