﻿//using System.Data;
//using EISCS.Shop;
//using EISCS.Shop.DO;
//using Moq;
//using NUnit.Framework;

//namespace ShopTests.BAS
//{
//    [TestFixture]
//    public class BaseDataServiceTests
//    {
//        private BaseDataService _sut;
//        private Mock<IDbCommand> _commandMock;

//        [SetUp]
//        public void Setup()
//        {
//            var connectionFactoryMock = new Mock<IDatabaseConnectionFactory>();
//            var connectionMock = new Mock<IDbConnection>();
//            _commandMock = new Mock<IDbCommand>();

//            connectionFactoryMock.Setup(m => m.GetConnection()).Returns(connectionMock.Object);
//            connectionMock.Setup(m => m.CreateCommand()).Returns(_commandMock.Object);

//            _sut = new BaseDataService(connectionFactoryMock.Object);
//        }


//        [Test]
//        public void Execute_WhenSuccess_ReturnsTrue()
//        {
//            const int oneRecordAffected = 1;
//            _commandMock.Setup(m => m.ExecuteNonQuery()).Returns(oneRecordAffected);

//            bool result = _sut.Execute("");

//            Assert.IsTrue(result);
//        }

//        [Test]
//        public void Execute_WhenFailed_ReturnsFalse()
//        {
//            const int noRecordsAffected = 0;
//            _commandMock.Setup(m => m.ExecuteNonQuery()).Returns(noRecordsAffected);

//            bool result = _sut.Execute("");

//            Assert.IsFalse(result);
//        }

//    }
//}