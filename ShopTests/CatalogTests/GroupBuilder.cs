﻿using System.Collections.Generic;
using CmsPublic.ModelLayer.Models;

namespace ShopTests.CatalogTests
{
    public class GroupBuilder
    {
        private readonly Group _grp;

        public GroupBuilder()
        {
            _grp = new Group {Properties = new GrpPropValSet {PropDict = new Dictionary<string, string>()}};
        }

        public Group Build()
        {
            return _grp;
        }

        public GroupBuilder WithValidDates()
        {
            //set valid dates on _grp;
            _grp.Properties.PropDict.Add("STARTDATE", "2012-08-06 00:00:00");
            _grp.Properties.PropDict.Add("ENDDATE", "2032-08-16 00:00:00");
            return this;
        }

        public GroupBuilder WithInvalidDates()
        {
            //set valid dates on _grp;
            _grp.Properties.PropDict.Add("STARTDATE", "2012-08-08 00:00:00");
            _grp.Properties.PropDict.Add("ENDDATE", "2012-08-16 00:00:00");
            return this;
        }

        public GroupBuilder WithEmptyDateProperties()
        {
            // set  STARTDATE and ENDDATE to ""
            _grp.Properties.PropDict.Add("STARTDATE", "");
            _grp.Properties.PropDict.Add("ENDDATE", "");
            return this;
        }

        public GroupBuilder WithValidNameProperty()
        {
            _grp.Properties.PropDict.Add("NAME", "GropupName1");
            return this;
        }

        public GroupBuilder WithChildren()
        {
            var group = new Group
                            {
                                Properties = new GrpPropValSet
                                                 {
                                                     PropDict =
                                                         new Dictionary<string, string> {{"NAME", "MyName"}}
                                                 }
                            };
            _grp.Children.Add(group);
            return this;
        }
    }
}