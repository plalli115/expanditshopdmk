﻿using CmsPublic.ModelLayer.Models;
using EISCS.Catalog.Render;
using NUnit.Framework;

namespace ShopTests.CatalogTests
{
    [TestFixture]
    public class Catalog2XmlTests
    {
        private GroupBuilder _builder;
        private Catalog2Xml _sut;

        [SetUp]
        public void Setup()
        {
            _sut = new Catalog2Xml("NAME", "REDIRECTURL", "da");
            _builder = new GroupBuilder();
        }

        [Test]
        public void IsStateVisible_HasValidValue_ShouldBeVisible()
        {
            Group grp = _builder.WithValidDates().Build();
            bool isStateVisible = _sut.IsStateVisible(grp);

            Assert.IsTrue(isStateVisible);
        }

        [Test]
        public void IsStateVisible_HasInvalidValue_ShouldNotBeVisible()
        {
            Group grp = _builder.WithInvalidDates().Build();
            bool isStateVisible = _sut.IsStateVisible(grp);

            Assert.IsFalse(isStateVisible);
        }

        [Test]
        public void IsStateVisible_HasEmptyValue_ShouldBeVisible()
        {
            Group grp = _builder.WithEmptyDateProperties().Build();
            bool isStateVisible = _sut.IsStateVisible(grp);

            Assert.IsTrue(isStateVisible);
        }
    }
}