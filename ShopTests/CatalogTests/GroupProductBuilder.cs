﻿using System;
using System.Collections.Generic;
using System.Linq;
using CmsPublic.ModelLayer.Models;
using EISCS.Shop.DO.Interface;

namespace ShopTests.CatalogTests
{
    internal class GroupProductBuilder
    {
        private IProductRepository _productRepository;

        public IProductRepository Build()
        {
            return _productRepository;
        }

        public GroupProductBuilder WithDefaultData()
        {
            var gp = new GroupProduct();
            //gp.Properties.PropDict.Add("NAME", "MyName");
            var groupProductList = new List<GroupProduct> {gp};
            _productRepository = new TestProductRepository(groupProductList);
            return this;
        }
    }

    public class TestProductRepository : IProductRepository
    {
        private readonly List<GroupProduct> _groupProductList;
        private readonly List<Product> _productList;

        public TestProductRepository(List<GroupProduct> groupProducts)
        {
            _groupProductList = groupProducts;
        }

        public TestProductRepository(List<Product> groupProducts)
        {
            _productList = groupProducts;
        }

        #region IProductRepository Members

        public int GetProductsCount(string type, string filter)
        {
            throw new NotImplementedException();
        }

        public int GetGroupProductsCount(int groupGuid)
        {
            throw new NotImplementedException();
        }

        public List<Product> GetProductRange(int startIndex, int endIndex, string filter, string languageGuid, string columnSort, string sortDir)
        {
            throw new NotImplementedException();
        }

        public List<Product> GetProductRange(int startIndex, int endIndex, bool isLoadProperties, string filter, string languageGuid, string columnSort, string sortDir)
        {
            throw new NotImplementedException();
        }

        public List<Product> GetProductsRange(ICollection<string> guids, string languageGuid, string filter, string sort, string direction, int startIndex, int endIndex)
        {
            throw new NotImplementedException();
        }

        public List<Product> GetProductRange(int startIndex, int endIndex, string type, string filter,
                                             string languageGuid)
        {
            throw new NotImplementedException();
        }

        public List<Product> GetProductRange(int startIndex, int endIndex, bool isLoadProperties, string type, string filter, string languageGuid)
        {
            throw new NotImplementedException();
        }

        public List<Product> GetProductsRange(ICollection<string> guids, string languageGuid, string type, string filter, string sort, string direction, int startIndex, int endIndex)
        {
            throw new NotImplementedException();
        }

        public List<Product> GetGroupProductsRange(int startIndex, int endIndex, bool isLoadProperties, string languageGuid, string filter, int groupGuid, string sort = null, string direction = null)
        {
            throw new NotImplementedException();
        }

        public List<Product> GetGroupProductsRange(int startIndex, int endIndex, bool isLoadProperties, string languageGuid, string type, string filter, int groupGuid, string sort = null,
                                          string direction = null)
        {
            throw new NotImplementedException();
        }

        public List<Product> GetGroupProductsRange(int startIndex, int endIndex, bool isLoadProperties, string languageGuid, string type, string filter, int groupGuid)
        {
            throw new NotImplementedException();
        }

        public List<GroupProduct> GroupProducts(string groupGuid, string languageGuid, bool isLoadProperties)
        {
            throw new NotImplementedException();
        }

        public Product SingleProduct(string guid, string languageGuid)
        {
            return _productList.FirstOrDefault(x => x.ProductGuid == guid);
        }

        public Product SingleProduct(string guid, string languageGuid, bool isLoadProperties)
        {
            return _productList.FirstOrDefault(x => x.ProductGuid == guid);
        }

        public Product SingleProductAnonymousCollection(string guid, string languageGuid)
        {
            throw new NotImplementedException();
        }

        public void SaveProperties(Product p, string languageGuid)
        {
            throw new NotImplementedException();
        }

        public List<Product> GetAllProducts(string languageGuid, bool isLoadProperties)
        {
            throw new NotImplementedException();
        }

        public List<GroupProduct> GetAllGroupProducts(string languageGuid, bool isLoadProperties)
        {
            return _groupProductList;
        }

        public List<GroupProduct> GetAllGroupProducts(int groupGuid, string languageGuid, bool isLoadProperties)
        {
            return _groupProductList;
        }

        public string GetSingleProperty(string propertyOwner, string propertyOwnerType, string propName,
                                        string languageGuid)
        {
            throw new NotImplementedException();
        }

        public int GetTotalRecords()
        {
            throw new NotImplementedException();
        }

        public int GetTotalRecordsFiltered(string filter)
        {
            throw new NotImplementedException();
        }

        public List<Product> FindProducts(string searchQuery)
        {
            throw new NotImplementedException();
        }

        public List<Product> GetProducts(ICollection<string> guids, string languageGuid)
        {
            throw new NotImplementedException();
        }

        #endregion

        public string GetTableName()
        {
            throw new NotImplementedException();
        }

        public string AllColumns()
        {
            throw new NotImplementedException();
        }

        public string GetIdColumnName()
        {
            throw new NotImplementedException();
        }

        public string GetDefaultSql()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Product> All()
        {
            throw new NotImplementedException();
        }

        public Product GetById(int id)
        {
            throw new NotImplementedException();
        }

        public Product GetById(string id)
        {
            throw new NotImplementedException();
        }

        public Product Add(Product item, bool useOutputClause = true)
        {
            throw new NotImplementedException();
        }


        public bool Update(Product item)
        {
            throw new NotImplementedException();
        }

        public bool Remove(int id)
        {
            throw new NotImplementedException();
        }

        public bool Remove(string id)
        {
            throw new NotImplementedException();
        }
    }
}