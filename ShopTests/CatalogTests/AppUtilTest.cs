﻿using System;
using System.Text.RegularExpressions;
using EISCS.ExpandITFramework.Util;
using NUnit.Framework;

namespace ShopTests.CatalogTests
{
    /// <summary>
    ///This is a test class for AppUtilTest and is intended
    ///to contain all AppUtilTest Unit Tests
    ///</summary>
    [TestFixture]
    public class AppUtilTest
    {
        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext { get; set; }

        #region Additional test attributes

        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //

        #endregion

        /// <summary>
        ///A test for NormalizeLinkString
        ///</summary>
        [Test]
        public void NormalizeLinkStringTest()
        {
            string urlValue = "www.test.com";
            string expected = "http://www.test.com";
            string actual = urlValue.NormalizeLinkString();
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void NormalizeHtmlContentTest()
        {
            string imageSrc = @"src=""/bc/test/image.jpg""";
            string expected = @"src='/bc/test/image.jpg'";
            string actual = imageSrc.NormalizeHtmlContent();
            Assert.AreEqual(expected, actual);
            Console.WriteLine(actual);
        }
    }
}