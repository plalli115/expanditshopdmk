﻿using System.Collections.Generic;
using DataFieldDefinition;
using EISCS.Shop.BO.BusinessLogic;
using EISCS.Shop.BO.BusinessLogic.None;
using EISCS.Shop.DO.Dto.Cart;
using NUnit.Framework;
using ShopTests.BusinessLogicTests.CartTests;
using ShopTests.BusinessLogicTests.Common;

namespace ShopTests.BusinessLogicTests.NoneTests
{
    [TestFixture]
    public class BusinessLogicNoneTest
    {
        const string ProductGuid = "1";

        private CartBuilder _builder;
        private ConfigReaderBuilder _configBuilder;
        private FakeDbFactory _fakeDbFactory;

        [SetUp]
        public void Setup()
        {
            _builder = new CartBuilder();
            _configBuilder = new ConfigReaderBuilder();
            _fakeDbFactory = new FakeDbFactory();
        }

        [Test]
        public void Ex20Times1PlusTaxShouldBe25()
        {
            const double listPrice = 20;
            ConfigReader configReader = _configBuilder.WithConfigurationValue("LISTPRICE_INCLUDE_TAX", "0");
            CartHeader cartHeader = _builder.WithHeaderGuid("1").WithProductGuid(ProductGuid).WithQuantity(1).WithListPrice(20).WithTaxPct("25").WithHeaderCurrencyGuid("GBP").WithLineCurrencyGuid("GBP");

            var dataService = new NoneTestDataService(ProductGuid, listPrice);
            BuslogicNone buslogic = new BuslogicNone(configReader, new CurrencyConverter(_fakeDbFactory, configReader), dataService);

            buslogic.CalculateOrder(cartHeader, "GBP", "GBP");

            Assert.AreEqual(25, cartHeader.TotalInclTax);
            Assert.AreEqual(20, cartHeader.Total);
            Assert.AreEqual(5, cartHeader.TaxAmount);
        }

        [Test]
        public void Ex20Times3PlusTaxShouldBe75()
        {
            const double listPrice = 20;
            ConfigReader configReader = _configBuilder.WithConfigurationValue("LISTPRICE_INCLUDE_TAX", "0").WithConfigurationValue("SECONDKEY", "1");
            CartHeader cartHeader = _builder.WithProductGuid(ProductGuid).WithQuantity(3).WithListPrice(20).WithTaxPct("25").WithHeaderCurrencyGuid("GBP").WithLineCurrencyGuid("GBP");

            var dataService = new NoneTestDataService(ProductGuid, listPrice);
            BuslogicNone buslogic = new BuslogicNone(configReader, new CurrencyConverter(_fakeDbFactory, configReader), dataService);

            buslogic.CalculateOrder(cartHeader, "GBP", "GBP");

            Assert.AreEqual(75, cartHeader.TotalInclTax);
            Assert.AreEqual(60, cartHeader.Total);
            Assert.AreEqual(15, cartHeader.TaxAmount);
        }

        [Test]
        public void Incl25Times3MinusTaxShouldBe60()
        {
            const double listPrice = 25;
            ConfigReader configReader = _configBuilder.WithConfigurationValue("LISTPRICE_INCLUDE_TAX", "1");
            CartHeader cartHeader = _builder.WithProductGuid(ProductGuid).WithQuantity(3).WithListPrice(25).WithTaxPct("25").WithHeaderCurrencyGuid("GBP").WithLineCurrencyGuid("GBP");

            var dataService = new NoneTestDataService(ProductGuid, listPrice);
            BuslogicNone buslogic = new BuslogicNone(configReader, new CurrencyConverter(_fakeDbFactory, configReader), dataService);

            buslogic.CalculateOrder(cartHeader, "GBP", "GBP");

            Assert.AreEqual(75, cartHeader.TotalInclTax);
            Assert.AreEqual(60, cartHeader.Total);
            Assert.AreEqual(15, cartHeader.TaxAmount);
        }

    }

    public class NoneTestDataService : INoneDataService
    {
        private string _productGuid;
        private double _listPrice;

        public NoneTestDataService(string productGuid, double listPrice)
        {
            _productGuid = productGuid;
            _listPrice = listPrice;
        }

        public List<ProductTableColumns> SelectListPrices(IEnumerable<string> inParameter)
        {
            ProductTableColumns columns = new ProductTableColumns
            {
                ListPrice = _listPrice,
                ProductGuid = _productGuid,
                ProductName = "Alien"
            };
            return new List<ProductTableColumns> { columns };
        }

        public LedgerEntryContainer GetCustomerLedgerEntries(string customerGuid)
        {
            throw new System.NotImplementedException();
        }
    }
}
