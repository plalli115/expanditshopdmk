using System.Collections.Generic;
using EISCS.Shop.DO.Dto.Cart;
using ShopTests.BusinessLogicTests.CartTests;

namespace ShopTests.CartTests
{
    public class CartLineRepositoryBuilder
    {
        private readonly TestCartLineRepository _cartLineRepository;

        public CartLineRepositoryBuilder()
        {
            _cartLineRepository = new TestCartLineRepository();
        }

        public TestCartLineRepository Build()
        {
            return _cartLineRepository;
        }

        public CartLineRepositoryBuilder WithCartHeader(CartHeader header)
        {
            if (_cartLineRepository.CartHeaders == null)
            {
                _cartLineRepository.CartHeaders = new List<CartHeader>();
            }
            _cartLineRepository.CartHeaders.Add(header);
            return this;
        }

        public static implicit operator TestCartLineRepository(CartLineRepositoryBuilder instance)
        {
            return instance.Build();
        }
    }
}