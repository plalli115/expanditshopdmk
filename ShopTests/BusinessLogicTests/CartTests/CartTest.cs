﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using CmsPublic.DataRepository;
using CmsPublic.ModelLayer.Models;
using EISCS.Shop.BO.BusinessLogic;
using EISCS.Shop.BO.BusinessLogic.None;
using EISCS.Shop.DO.Dto;
using EISCS.Shop.DO.Dto.Cart;
using EISCS.Shop.DO.Dto.Product;
using EISCS.Shop.DO.Interface;
using EISCS.Shop.DO.Service;
using NUnit.Framework;
using ShopTests.BusinessLogicTests.Common;
using ShopTests.BusinessLogicTests.NoneTests;
using ShopTests.CartTests;
using ShopTests.CatalogTests;

namespace ShopTests.BusinessLogicTests.CartTests
{
    [TestFixture]
    public class CartTest
    {
        [SetUp]
        public void Setup()
        {
            _builder1 = new CartBuilder();
            _builder2 = new CartBuilder();
            _configBuilder = new ConfigReaderBuilder();
            _cartheaderRepositoryBuilder = new CartheaderRepositoryBuilder();
            _cartLineRepositoryBuilder = new CartLineRepositoryBuilder();
        }

        private const string ProductGuid = "1";

        private CartBuilder _builder1;
        private CartBuilder _builder2;
        private ConfigReaderBuilder _configBuilder;
        private CartheaderRepositoryBuilder _cartheaderRepositoryBuilder;
        private CartLineRepositoryBuilder _cartLineRepositoryBuilder;

        [Test]
        public void TestMergeCarts_LinesShouldBe1_QuantityShouldBe2()
        {
            ConfigReader reader = _configBuilder.WithConfigurationValue("LISTPRICE_INCLUDE_TAX", "0");
            CartHeader cartHeader1 =
                _builder1.WithHeaderGuid("1")
                    .WithProductGuid(ProductGuid)
                    .WithQuantity(1)
                    .WithListPrice(20)
                    .WithTaxPct("25")
                    .WithHeaderCurrencyGuid("GBP")
                    .WithLineCurrencyGuid("GBP")
                    .WithUserGuid("1");
            CartHeader cartHeader2 =
                _builder2.WithHeaderGuid("2")
                    .WithProductGuid(ProductGuid)
                    .WithQuantity(1)
                    .WithListPrice(20)
                    .WithTaxPct("25")
                    .WithHeaderCurrencyGuid("GBP")
                    .WithLineCurrencyGuid("GBP")
                    .WithUserGuid("2");

            Assert.IsTrue(cartHeader1.HeaderGuid == "1");
            Assert.IsTrue(cartHeader2.HeaderGuid == "2");

            TestCartHeaderRepository cartHeaderRepository = _cartheaderRepositoryBuilder.WithCartHeader(cartHeader1).WithCartHeader(cartHeader2);
            TestCartLineRepository cartLineRepository = _cartLineRepositoryBuilder.WithCartHeader(cartHeader1).WithCartHeader(cartHeader2);

            var dataService = new NoneTestDataService(ProductGuid, 10);
            var userService = new SimpleExpanditUserService();

            var fakeDbFactory = new FakeDbFactory();
            var buslogic = new BuslogicNone(reader, new CurrencyConverter(fakeDbFactory, reader), dataService);
            
            var testProductVariantRepository = new TestProductVariantService(fakeDbFactory);
            var products = new List<Product>();
            products.Add(new Product() {ProductGuid = "1"});
            var productService = new ProductService(fakeDbFactory, buslogic, new TestProductRepository(products), userService, testProductVariantRepository, null);

            var currencyConverter = new TestCurrencyConverter();
            TestShippingHandlingService testShippingHandlingService = new TestShippingHandlingService();
            
            var fakeDataService = new FakeDataService();
            var cartService = new CartService(fakeDataService ,buslogic, productService, cartHeaderRepository, cartLineRepository, userService, testProductVariantRepository, testShippingHandlingService, null, currencyConverter,reader);

            Result result = cartService.MergeUserCarts("1", "2");

            Assert.IsTrue(result.Success);

            Assert.IsTrue(cartHeader1.Lines.Count == 1);

            const double expected = 2;
            Assert.IsTrue(cartHeader1.Lines[0].Quantity.Equals(expected));
        }
    }

    public class FakeDataService : ICartDataService
    {
        public List<string> GetCartLinesToRemove(string headerGuid)
        {
            return null;
        }

        public string GenerateCustomerReference()
        {
            return "12345";
        }

        public string ShippingHandlingName(CartHeader cartHeader)
        {
            return null;
        }

        public IEnumerable<CartHeader> GetStaleUserCarts(int maxDays)
        {
            throw new NotImplementedException();
        }

        public string GetCurrencyIso4217(string currencyGuid){
            return "208";
        }
    }

    public class FakeDbFactory : IExpanditDbFactory
    {
        public IDbConnection GetConnection()
        {
            return null;
        }

        public void Finalize(IDbConnection connection)
        {
            return;
        }

        public IDbDataAdapter GetDataAdapter(IDbConnection connection)
        {
            throw new NotImplementedException();
        }
    }

    public class TestProductVariantService : ProductVariantService
    {
        public TestProductVariantService(IExpanditDbFactory dbFactory) : base(dbFactory)
        {
        }

        public override ProductVariantContainer GetVariants(IEnumerable<string> inParameter, string languageGuid)
        {
            var container = new ProductVariantContainer();
            List<ProductVariant> variants = new List<ProductVariant>();
            container.SetVariants(variants);
            return container;
        }
    }

    public class TestCartLineRepository : ICartLineRepository
    {
        public List<CartHeader> CartHeaders { get; set; }

        public string GetTableName()
        {
            throw new NotImplementedException();
        }

        public string AllColumns()
        {
            throw new NotImplementedException();
        }

        public string GetIdColumnName()
        {
            throw new NotImplementedException();
        }

        public string GetDefaultSql()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<CartLine> All()
        {
            throw new NotImplementedException();
        }

        public CartLine GetById(int id)
        {
            throw new NotImplementedException();
        }

        public CartLine GetById(string id)
        {
            throw new NotImplementedException();
        }

        public CartLine Add(CartLine item, bool useOutputClause = true)
        {
            throw new NotImplementedException();
        }


        public bool Update(CartLine item)
        {
            CartHeader cartHeader = CartHeaders.FirstOrDefault(x => x.HeaderGuid == item.HeaderGuid);
            if (cartHeader != null)
            {
                CartLine line = cartHeader.Lines.FirstOrDefault(x => x.LineGuid == item.LineGuid);
                if (line != null)
                {
                    line.Quantity = item.Quantity;
                }
                return true;
            }
            return false;
        }

        public bool Remove(int id)
        {
            throw new NotImplementedException();
        }

        public bool Remove(string id)
        {
            return false;
        }

        public List<CartLine> GetCartLinesById(string headerGuid)
        {
            CartHeader cartHeader = CartHeaders.FirstOrDefault(x => x.HeaderGuid == headerGuid);
            return cartHeader != null ? cartHeader.Lines.Where(x => x.HeaderGuid == headerGuid).ToList() : null;
        }

        public bool Delete(IEnumerable<string> inParameter)
        {
            throw new NotImplementedException();
        }

        public bool Add(CartLine item)
        {
            CartHeader cartHeader = CartHeaders.FirstOrDefault(x => x.HeaderGuid == item.HeaderGuid);
            if (cartHeader != null)
            {
                cartHeader.Lines.Add(item);
                return true;
            }
            return false;
        }
    }

    public class TestCartHeaderRepository : ICartHeaderRepository
    {
        public List<CartHeader> CartHeaders { get; set; }

        public string GetTableName()
        {
            throw new NotImplementedException();
        }

        public string AllColumns()
        {
            throw new NotImplementedException();
        }

        public string GetIdColumnName()
        {
            throw new NotImplementedException();
        }

        public string GetDefaultSql()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<CartHeader> All()
        {
            throw new NotImplementedException();
        }

        public CartHeader GetById(int id)
        {
            throw new NotImplementedException();
        }

        public CartHeader GetById(string id)
        {
            throw new NotImplementedException();
        }

        public CartHeader Add(CartHeader item, bool useOutputClause = true)
        {
            throw new NotImplementedException();
        }

        public bool Update(CartHeader item)
        {
            return true;
        }

        public bool Remove(int id)
        {
            throw new NotImplementedException();
        }

        public bool Remove(string id)
        {
            CartHeader header = CartHeaders.FirstOrDefault(x => x.HeaderGuid == id);
            if (header != null)
            {
                CartHeaders.Remove(header);
                return true;
            }
            return false;
        }

        public CartHeader GetCartHeaderByUser(string userGuid)
        {
            return CartHeaders.FirstOrDefault(x => x.UserGuid == userGuid);
        }

        public CartHeader UpdateShippingAddress(ShippingAddress shippingAddress)
        {
            throw new NotImplementedException();
        }

        public CartHeader MapBillingAddress(UserTable user)
        {
            throw new NotImplementedException();
        }

        public bool Add(CartHeader item)
        {
            throw new NotImplementedException();
        }
    }
}