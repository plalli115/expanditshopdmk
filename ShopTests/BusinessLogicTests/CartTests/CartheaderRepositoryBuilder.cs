using System.Collections.Generic;
using EISCS.Shop.DO.Dto.Cart;
using ShopTests.BusinessLogicTests.CartTests;

namespace ShopTests.CartTests
{
    public class CartheaderRepositoryBuilder
    {
        private readonly TestCartHeaderRepository _cartHeaderRepository;

        public CartheaderRepositoryBuilder()
        {
            _cartHeaderRepository = new TestCartHeaderRepository();
        }

        public TestCartHeaderRepository Build()
        {
            return _cartHeaderRepository;
        }

        public CartheaderRepositoryBuilder WithCartHeader(CartHeader header)
        {
            if (_cartHeaderRepository.CartHeaders == null)
            {
                _cartHeaderRepository.CartHeaders = new List<CartHeader>();
            }
            _cartHeaderRepository.CartHeaders.Add(header);
            return this;
        }

        public static implicit operator TestCartHeaderRepository(CartheaderRepositoryBuilder instance)
        {
            return instance.Build();
        }
    }
}