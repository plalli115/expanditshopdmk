﻿using System.Configuration;
using CmsPublic.DataRepository;
using EISCS.Shop.BO.BusinessLogic;
using EISCS.Shop.BO.BusinessLogic.None;
using EISCS.Shop.DO;
using EISCS.Shop.DO.Dto.Cart;
using EISCS.Shop.DO.Interface;
using EISCS.Shop.DO.Repository;
using EISCS.Shop.DO.Service;
using NUnit.Framework;
using ShopTests.BusinessLogicTests.Common;
using ShopTests.BusinessLogicTests.NoneTests;

namespace ShopTests.BusinessLogicTests.CartTests
{
    [Category("Integration")]
    [TestFixture]
    public class CartIntegrationTest
    {
        [SetUp]
        public void Setup()
        {
            _builder = new CartBuilder();
            _configBuilder = new ConfigReaderBuilder();
            ExpanditDbFactory = new ExpanditDbFactory(ExpanditShopConnectionString, ExpanditConnectionStringProviderName);
        }

        public static string ExpanditShopConnectionString =
            ConfigurationManager.ConnectionStrings["ExpandITConnectionString"].ConnectionString;

        public static string ExpanditConnectionStringProviderName =
            ConfigurationManager.ConnectionStrings["ExpandITConnectionString"].ProviderName;

        public static IExpanditDbFactory ExpanditDbFactory;
        private const string ProductGuid = "1";

        private CartBuilder _builder;
        private ConfigReaderBuilder _configBuilder;

        [Test]
        public void InsertIntoCartLine()
        {
            ConfigReader configReader = _configBuilder.WithConfigurationValue("LISTPRICE_INCLUDE_TAX", "0");
            CartHeader cartHeader =
                _builder.WithHeaderGuid("1").WithProductGuid(ProductGuid).WithQuantity(1).WithListPrice(20).WithTaxPct("25").WithHeaderCurrencyGuid("GBP").WithLineCurrencyGuid("GBP");

            var dataService = new NoneTestDataService(ProductGuid, 10);
            var userService = new SimpleExpanditUserService();


            var buslogic = new BuslogicNone(configReader, new CurrencyConverter(ExpanditDbFactory, configReader), dataService);
            IPropertiesDataService propertiesDataService = new PropertiesDataService(ExpanditDbFactory);
            var productService = new ProductService(ExpanditDbFactory, buslogic, new ProductRepository(ExpanditDbFactory, propertiesDataService), userService, new ProductVariantService(ExpanditDbFactory), null);
	        
			var cartHeaderRepository = new CartHeaderRepository(ExpanditDbFactory, userService);
            var cartLineRepository = new CartLineRepository(ExpanditDbFactory);

            var currencyConverter = new TestCurrencyConverter();
            TestShippingHandlingService testShippingHandlingService = new TestShippingHandlingService();

            ICartDataService cartDataService = new CartDataService(ExpanditDbFactory);

            var cartService = new CartService(cartDataService, buslogic, productService, cartHeaderRepository, cartLineRepository, userService, new ProductVariantService(ExpanditDbFactory), testShippingHandlingService, null, currencyConverter,configReader);

            cartService.Add(userService.UserGuid, ProductGuid, "Test Product", 1, "My Test Insert", null, false);
        }
    }
}