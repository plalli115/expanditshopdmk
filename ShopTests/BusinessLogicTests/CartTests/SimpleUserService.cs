

using System.Collections.Generic;
using EISCS.Shop.DO.Dto;
using EISCS.Shop.DO.Interface;
using EISCS.Shop.DO.Service;

namespace ShopTests.BusinessLogicTests.CartTests
{
    public class SimpleExpanditUserService : IExpanditUserService
    {
        private string _userGuid;
        private string _languageGuid;
        private string _currencyGuid;

        public string UserGuid
        {
            get { return "e62c3969-004a-40be-9529-692d439c2f7e"; }
            set { _userGuid = value; }
        }

        public string CustomerGuid
        {
            get { return "e62c3969-004a-40be-9529-692d439c2f7e"; }
            set { _userGuid = value; }
        }

        public string LanguageGuid
        {
            get { return "en"; }
            set { _languageGuid = value; }
        }

        public string CurrencyGuid
        {
            get { return "DKK"; }
            set { _currencyGuid = value; }
        }

        public string UserEmail { get; set; }

        public MiniCartItem MiniCart { get; set; }
        public string GetDefaultCurrency { get; set; }
        public string GetCountryName(string countryGuid)
        {
            return "Denmark";
        }

        public UserClass UserType { get; private set; }
        public bool UserAccess(string accessClass)
        {
            return true;
        }

        public string UniqueIdentity { get; private set; }
        public UserTable GetPortalUser()
        {
            throw new System.NotImplementedException();
        }

        public UserTable GetPortalUser(string id)
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<UserTable> GetAllB2BUsers()
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<UserTable> GetAllUsers()
        {
            throw new System.NotImplementedException();
        }

        public int GetAllUsersCount()
        {
            throw new System.NotImplementedException();
        }

        public bool NewLogin(string userGuid)
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<UserTable> FindUsers(string query)
        {
            throw new System.NotImplementedException();
        }

        public void DeleteUser(string id)
        {
            throw new System.NotImplementedException();
        }

        public void SetUserAsAdmin(string userId)
        {
            throw new System.NotImplementedException();
        }

        public void UpdateUserAccess(string userId, int? customerGroupId, string customerId, IEnumerable<string> projectIds)
        {
            throw new System.NotImplementedException();
        }

        public void SetServiceOrderAccess(string userId, bool canCreateServiceOrders)
        {
            throw new System.NotImplementedException();
        }

        public bool AddRole(string roleId, string roleDescription)
        {
            throw new System.NotImplementedException();
        }

        public bool AddRoleAccess(string roleId, string accessClass)
        {
            throw new System.NotImplementedException();
        }

        public bool RemoveRoleAccess(string roleId, string accessClass)
        {
            throw new System.NotImplementedException();
        }

        public bool DeleteRole(string roleId)
        {
            throw new System.NotImplementedException();
        }

        public bool SetUserRole(string userGuid, string roleId)
        {
            throw new System.NotImplementedException();
        }

        public bool AddUser(UserTable user)
        {
            throw new System.NotImplementedException();
        }

        public bool DeleteUser(UserTable user)
        {
            throw new System.NotImplementedException();
        }

        public UserTable GetUser()
        {
            return null;
        }

        public UserTable GetUser(string userGuid)
        {
            return null;
        }
    }
}