﻿using System;
using System.Collections.Generic;
using System.Linq;
using EISCS.Shop.BO.BusinessLogic;
using EISCS.Shop.BO.BusinessLogic.Navision.Nav;
using EISCS.Shop.DO.Dto;
using EISCS.Shop.DO.Dto.Backend.Navision;
using EISCS.Shop.DO.Dto.Backend.Navision.Nav;
using EISCS.Shop.DO.Dto.Cart;
using EISCS.Shop.DO.Interface;
using NUnit.Framework;
using ShopTests.BusinessLogicTests.CartTests;
using ShopTests.BusinessLogicTests.Common;

namespace ShopTests.BusinessLogicTests.NavTests
{
    [TestFixture]
    public class BusinessLogicNavTest
    {
        const string ProductGuid = "1";

        private CartBuilder _builder;
        private ConfigReaderBuilder _configBuilder;
        private NavTestDataServiceBuilder _dataServiceBuilder;
        private FakeDbFactory _fakeDbFactory;

        [SetUp]
        public void Setup()
        {
            _builder = new CartBuilder();
            _configBuilder = new ConfigReaderBuilder();
            _dataServiceBuilder = new NavTestDataServiceBuilder();
            _fakeDbFactory = new FakeDbFactory();
        }

        [Test]
        public void InitOrderTestShouldAddParameters()
        {
            ConfigReader configuration = _configBuilder.WithConfigurationValue("ATTAIN_USE_CURRENCY_FROM_BILLTO_CUSTOMER", "0").WithConfigurationValue("NAV_COLLECT_QUANTITES_FOR_LINE_DISCOUNTS", "0");
            CartHeader cartHeader = _builder.WithHeaderGuid("1").WithProductGuid(ProductGuid).WithQuantity(1).WithListPrice(20).WithTaxPct("25").WithHeaderCurrencyGuid("GBP").WithLineCurrencyGuid("GBP");

            NavTestDataService service = _dataServiceBuilder.WithCustomerTable("INET", null, false, null, null, null, "");

            IExpanditUserService userService = new SimpleExpanditUserService();

            IShippingHandlingPriceRepository shippingHandlingPriceRepository = new FakeShippingHandlingPriceRepository();

            BuslogicNav buslogic = new BuslogicNav(configuration, new CurrencyConverter(_fakeDbFactory, configuration), service, shippingHandlingPriceRepository, userService, null);

            buslogic.InitOrder(cartHeader, "INET", "GBP");

            Assert.IsNotNull(cartHeader.HeaderParameter);
        }

        [Test]
        public void ListPriceEx20Times1PlusTaxShouldBe25()
        {
            ConfigReader configuration =
                _configBuilder.WithConfigurationValue("ATTAIN_USE_CURRENCY_FROM_BILLTO_CUSTOMER", "0")
                .WithConfigurationValue("NAV_COLLECT_QUANTITES_FOR_LINE_DISCOUNTS", "0")
                .WithConfigurationValue("LISTPRICE_INCLUDE_TAX", "0")
                .WithConfigurationValue("ATTAIN_SERVICECHARGE_PRODUCTPOSTINGGROUP", "VAT25")
                .WithConfigurationValue("ANONYMOUS_CUSTOMERGUID", "INET");

            CartHeader cartHeader =
                _builder.WithHeaderGuid("1")
                .WithProductGuid(ProductGuid)
                .WithQuantity(1)
                .WithListPrice(20)
                .WithTaxPct("25")
                .WithHeaderCurrencyGuid("GBP")
                .WithLineCurrencyGuid("GBP");

            NavTestDataService service =
                _dataServiceBuilder
                .WithUserTable("1", "INET")
                .WithCustomerTable("INET", null, false, null, "INET", "NATIONAL", "")
                .WithNavProductTable("1", 20, false, "VAT25", null)
                .WithAttainVatPostingSetup("NATIONAL", "VAT25", 25);

            IShippingHandlingPriceRepository shippingHandlingPriceRepository = new FakeShippingHandlingPriceRepository();
            IExpanditUserService userService = new SimpleExpanditUserService();
            BuslogicNav buslogic = new BuslogicNav(configuration, new CurrencyConverter(_fakeDbFactory, configuration), service, shippingHandlingPriceRepository, userService, null);

            buslogic.CalculateOrder(cartHeader, "GBP", "GBP");

            Assert.AreEqual(25, cartHeader.TotalInclTax);

        }

    }

    public class FakeShippingHandlingPriceRepository : IShippingHandlingPriceRepository
    {
        public List<ShippingHandlingPrice> GetShippingHandlingPrice(string providerGuid)
        {
            return new List<ShippingHandlingPrice>();
        }
    }

    public class NavTestDataServiceBuilder
    {
        private readonly NavTestDataService _navDataService;

        public NavTestDataServiceBuilder()
        {
            _navDataService = new NavTestDataService();
        }

        public NavTestDataService Build()
        {
            return _navDataService;
        }

        public NavTestDataServiceBuilder WithUserTable(string userGuid, string customerGuid)
        {
            var table = new UserTable
            {
                UserGuid = userGuid,
                CustomerGuid = customerGuid
            };
            _navDataService.UserTable.Add(table);

            return this;
        }

        public NavTestDataServiceBuilder WithCustomerTable(string customerGuid, string customerDiscountGroup, bool allowLineDisc, string priceGroupCode, string invoiceDiscountCode, string vatBusinessPostingGroup, string currencyGuid)
        {
            var table = new CustomerTableNav
                                                {
                                                    CustomerGuid = customerGuid,
                                                    CustomerDiscountGroup = customerDiscountGroup,
                                                    AllowLineDisc = allowLineDisc,
                                                    PriceGroupCode = priceGroupCode,
                                                    InvoiceDiscountCode = invoiceDiscountCode,
                                                    VATBusinessPostingGroup = vatBusinessPostingGroup,
                                                    CurrencyGuid = currencyGuid
                                                };

            _navDataService.CustomerTable.Add(table);

            _navDataService.AttainCustomerInvoiceDiscounts = new List<Attain_CustomerInvoiceDiscount>();

            return this;
        }

        public NavTestDataServiceBuilder WithNavProductTable(string productGuid, double listPrice, bool allowInvoiceDiscount, string vatProductPostingGroup, string itemCustomerDiscountGroup)
        {
            var table = new NavProductTable
            {
                ProductGuid = productGuid,
                ListPrice = listPrice,
                AllowInvoiceDiscount = allowInvoiceDiscount,
                VATProductPostingGroup = vatProductPostingGroup,
                ItemCustomerDiscountGroup = itemCustomerDiscountGroup
            };
            _navDataService.ProductTable.Add(table);
            return this;
        }

        public NavTestDataServiceBuilder WithAttainProductPrice(string productGuid, bool allowInvoiceDiscount, string attain_VATBusPostingGrPrice, int customerRelType, double minimumQuantity,
            DateTime endingDate, string customerRelGuid, string currencyGuid, DateTime startingDate, double unitPrice, string uomGuid, string variantCode, bool priceInclTax, bool allowLineDiscount)
        {
            var table = new Attain_ProductPrice
            {
                ProductGuid = productGuid,
                AllowInvoiceDiscount = allowInvoiceDiscount,
                Attain_VATBusPostingGrPrice = attain_VATBusPostingGrPrice,
                CustomerRelType = customerRelType,
                MinimumQuantity = minimumQuantity,
                EndingDate = endingDate,
                CustomerRelGuid = customerRelGuid,
                CurrencyGuid = currencyGuid,
                StartingDate = startingDate,
                UnitPrice = unitPrice,
                UOMGuid = uomGuid,
                VariantCode = variantCode,
                PriceInclTax = priceInclTax,
                AllowLineDiscount = allowLineDiscount
            };

            _navDataService.AttainProductPrice.Add(table);
            return this;
        }

        public NavTestDataServiceBuilder WithAttainSalesLineDiscount(string code, int salesType, double minimumQuantity,
            DateTime endingDate, string salesCode, int type, string currencyCode, DateTime startingDate, double lineDiscountPct, string variantCode)
        {

            var table = new Attain_SalesLineDiscount
            {
                Code = code,
                SalesType = salesType,
                MinimumQuantity = minimumQuantity,
                EndingDate = endingDate,
                SalesCode = salesCode,
                Type = type,
                CurrencyCode = currencyCode,
                StartingDate = startingDate,
                LineDiscountPct = lineDiscountPct,
                VariantCode = variantCode
            };
            _navDataService.AttainSalesLineDiscount.Add(table);
            return this;
        }


        public NavTestDataServiceBuilder WithInvoiceDiscounts(double discountPct, string currencyCode, double serviceCharge, double minimumAmount, string invoiceDiscountCode)
        {
            var table = new Attain_CustomerInvoiceDiscount
            {
                InvoiceDiscountCode = invoiceDiscountCode,
                MinimumAmount = minimumAmount,
                DiscountPct = discountPct,
                ServiceCharge = serviceCharge,
                CurrencyCode = currencyCode
            };
            _navDataService.AttainCustomerInvoiceDiscounts.Add(table);
            return this;
        }


        public NavTestDataServiceBuilder WithAttainVatPostingSetup(string businessPostingGroup, string productPostingGroup, double vatPct)
        {
            var table = new Attain_VATPostingSetup
            {
                BusinessPostingGroup = businessPostingGroup,
                ProductPostingGroup = productPostingGroup,
                VATPct = vatPct
            };
            _navDataService.AttainVatPostingSetup.Add(table);
            return this;
        }

        public static implicit operator NavTestDataService(NavTestDataServiceBuilder instance)
        {
            return instance.Build();
        }

    }


    public class NavTestDataService : INavDataService
    {
        public List<UserTable> UserTable;
        public List<CustomerTableNav> CustomerTable;
        public List<NavProductTable> ProductTable;
        public List<Attain_ProductPrice> AttainProductPrice;
        public List<Attain_SalesLineDiscount> AttainSalesLineDiscount;
        public List<Attain_VATPostingSetup> AttainVatPostingSetup;
        public List<Attain_CustomerInvoiceDiscount> AttainCustomerInvoiceDiscounts;

        public NavTestDataService()
        {
            UserTable = new List<UserTable>();
            CustomerTable = new List<CustomerTableNav>();
            ProductTable = new List<NavProductTable>();
            AttainProductPrice = new List<Attain_ProductPrice>();
            AttainSalesLineDiscount = new List<Attain_SalesLineDiscount>();
            AttainVatPostingSetup = new List<Attain_VATPostingSetup>();
            AttainCustomerInvoiceDiscounts = new List<Attain_CustomerInvoiceDiscount>();
        }

        public string GetCustomerGuid(string userGuid)
        {
            var firstOrDefault = UserTable.FirstOrDefault(x => x.UserGuid == userGuid);
            return firstOrDefault != null ? firstOrDefault.CustomerGuid : null;
        }

        public CustomerTableNav SelectFromCustomerTable(string customerGuid)
        {
            return CustomerTable.FirstOrDefault(c => c.CustomerGuid == customerGuid);
        }

        public List<Attain_CustomerInvoiceDiscount> SelectFromAttainCustomerInvoiceDiscount(string invoiceDiscountCode, string currencyGuid)
        {
            return null;
        }

        public Attain_VATPostingSetup GetVatPctFromAttainVatPostingSetup(string businessPostingGroup, string productPostingGroup)
        {
            return AttainVatPostingSetup.FirstOrDefault(
                x => x.BusinessPostingGroup == businessPostingGroup && x.ProductPostingGroup == productPostingGroup);
        }

        public List<NavProductTable> SelectStandardPrice(string productGuids)
        {
            var guids = productGuids.Replace("'", "").Split(Convert.ToChar(","));

            return guids.Select(guid => ProductTable.Find(x => x.ProductGuid == guid)).Where(product => product != null).ToList();
        }

        public List<Attain_ProductPrice> SelectListPrice(string productGuids, string customerGuid, string priceGroupCode, string customerCurrencyGuid, string defaultCurrency)
        {
            var guids = productGuids.Replace("'", "").Split(Convert.ToChar(","));
            List<Attain_ProductPrice> temp = guids.Select(guid => AttainProductPrice.Find(x => x.ProductGuid == guid)).Where(product => product != null).ToList();

            var query = from tmp in temp
                        where
                            (tmp.CustomerRelType == 0 && tmp.CustomerRelGuid == customerGuid) ||
                            (tmp.CustomerRelType == 1 && tmp.CustomerRelGuid == priceGroupCode) ||
                            (tmp.CustomerRelType == 2)
                            && (tmp.StartingDate <= DateTime.Now || tmp.StartingDate == default(DateTime)) &&
                            (tmp.EndingDate >= DateTime.Now || tmp.EndingDate == default(DateTime)) &&
                            (tmp.CurrencyGuid == customerCurrencyGuid || string.IsNullOrEmpty(tmp.CurrencyGuid) ||
                             tmp.CurrencyGuid == defaultCurrency)
                        select tmp;

            return query.ToList();
        }

        public List<Attain_SalesLineDiscount> SelectFromAttainSalesLineDiscount(string productInListParameter, string itemCustomerDiscountGrouplist, string customerGuid, string customerDiscountGroup, string orderCurrencyGuid, string defaultCurrency)
        {
            return null;
        }

        public List<VatPctInfo> SelectFromProductTableJoinAttainVatPostingSetup(string vatBusinessPostingGroup, string productGuids)
        {
            if (AttainVatPostingSetup == null)
            {
                return null;
            }

            if (AttainVatPostingSetup.Count == 0)
            {
                return null;
            }

            var guids = productGuids.Replace("'", "").Split(Convert.ToChar(","));

            List<NavProductTable> products = guids.Select(guid => ProductTable.FirstOrDefault(x => x.ProductGuid == guid)).ToList();

            var query = from p in products
                        join a in AttainVatPostingSetup.DefaultIfEmpty() on p.VATProductPostingGroup equals a.ProductPostingGroup
                        select new { p, a };

            return query.Select(item => new VatPctInfo { ProductGuid = item.p.ProductGuid, VATPct = item.a.VATPct }).ToList();
        }

        public LedgerEntryContainer GetCustomerLedgerEntries(string customerGuid)
        {
            throw new NotImplementedException();
        }
    }
}
