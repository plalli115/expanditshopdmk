﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EISCS.Shop.DO.Dto;
using EISCS.Shop.DO.Interface;

namespace ShopTests.BusinessLogicTests.Common
{
    public class TestShippingHandlingService : IShippingHandlingService
    {
        public List<ShippingHandlingProvider> GetShippingHandlingProviders()
        {
            var shippingHandlingPrice = new ShippingHandlingPrice {HandlingAmount = 0, ShippingAmount = 0, ShippingHandlingProviderGuid = "NONE"};
            var shippingHandlingProvider = new ShippingHandlingProvider {ShippingHandlingPrices = new List<ShippingHandlingPrice> {shippingHandlingPrice}, IsPickUp = true, ProviderDescription = "", ProviderName = "", ShippingHandlingProviderGuid = "NONE", SortIndex = 1};
            var providers = new List<ShippingHandlingProvider> {shippingHandlingProvider};

            return providers;
        }

        public ShippingHandlingProvider GetShippingHandlingProvider(string shippingProviderGuid)
        {
            var shippingHandlingPrice = new ShippingHandlingPrice { HandlingAmount = 0, ShippingAmount = 0, ShippingHandlingProviderGuid = "NONE" };
            var shippingHandlingProvider = new ShippingHandlingProvider { ShippingHandlingPrices = new List<ShippingHandlingPrice> { shippingHandlingPrice }, IsPickUp = true, ProviderDescription = "", ProviderName = "", ShippingHandlingProviderGuid = "NONE", SortIndex = 1 };
            return shippingHandlingProvider;
        }
    }
}
