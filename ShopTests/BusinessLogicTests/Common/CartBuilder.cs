using System.Collections.Generic;
using EISCS.Shop.DO.Dto.Cart;

namespace ShopTests.BusinessLogicTests.Common
{
    public class CartBuilder
    {
        private readonly CartHeader _cartHeader;

        public CartBuilder()
        {
            _cartHeader = new CartHeader
                              {
                                  Lines = new List<CartLine>(){
                                                                  new CartLine(){
                                                                                    CurrencyGuid = "", HeaderGuid = "1", IsCalculated = false, LineComment = "", LineDiscount = 0, LineDiscountAmount = 0, LineDiscountAmountInclTax = 0, LineGuid = "1", LineNumber = 1, Quantity = 1, VariantCode = ""
                                                                                }
                                                              },
                                  HeaderGuid = "1",
                                  UserGuid = "1",
                                  TaxPct = "25",
                                  PricesIncludingVat = false
                              };
        }

        public CartHeader Build()
        {
            return _cartHeader;
        }

        public CartBuilder WithHeaderCurrencyGuid(string currencyGuid)
        {
            _cartHeader.CurrencyGuid = currencyGuid;
            return this;
        }

        public CartBuilder WithLineCurrencyGuid(string currencyGuid)
        {
            _cartHeader.Lines[0].CurrencyGuid = currencyGuid;
            return this;
        }

        public CartBuilder WithTaxPct(string taxPct)
        {
            _cartHeader.TaxPct = taxPct;
            return this;
        }

        public CartBuilder WithProductGuid(string productGuid)
        {
            _cartHeader.Lines[0].ProductGuid = productGuid;
            return this;
        }

        public CartBuilder WithQuantity(double quantity)
        {
            _cartHeader.Lines[0].Quantity = quantity;
            return this;
        }

        public CartBuilder WithListPrice(double listPrice)
        {
            _cartHeader.Lines[0].ListPrice = listPrice;
            return this;
        }

        public CartBuilder WithHeaderGuid(string headerGuid)
        {
            _cartHeader.HeaderGuid = headerGuid;
            _cartHeader.Lines[0].HeaderGuid = headerGuid;
            return this;
        }

        public CartBuilder WithUserGuid(string userGuid)
        {
            _cartHeader.UserGuid = userGuid;
            return this;
        }

        public static implicit operator CartHeader(CartBuilder instance)
        {
            return instance.Build();
        }

    }
}