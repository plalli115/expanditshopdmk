﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EISCS.Shop.BO.BusinessLogic;

namespace ShopTests.BusinessLogicTests.Common
{
    public class TestCurrencyConverter : ICurrencyConverter
    {
        public double ConvertCurrency(double inputAmount, string inputCurrency, string outputCurrency)
        {
            return inputAmount;
        }

        public decimal GetExchangeFactor(string fromCurrency, string toCurrency)
        {
            return 1;
        }

        public string GetIso4217Code(string aCurrency)
        {
            return null;
        }
    }
}
