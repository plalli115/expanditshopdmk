using System.Collections.Generic;
using EISCS.Wrappers.Configuration;

namespace ShopTests.BusinessLogicTests.Common
{
    public class ConfigReader : IConfigurationObject
    {
        public Dictionary<string, string> ConfigValues { get; private set; }

        public ConfigReader()
        {
            ConfigValues = new Dictionary<string, string>();
        }

        public string Read(string value)
        {
            return ConfigValues.ContainsKey(value) ? ConfigValues[value] : null;
        }
    }
}