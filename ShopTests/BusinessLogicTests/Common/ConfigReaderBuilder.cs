namespace ShopTests.BusinessLogicTests.Common
{
    public class ConfigReaderBuilder
    {
        private readonly ConfigReader _configReader;

        public ConfigReaderBuilder()
        {
            _configReader = new ConfigReader();
        }

        public ConfigReader Build()
        {
            return _configReader;
        }

        public ConfigReaderBuilder WithConfigurationValue(string config, string value)
        {
            if (!_configReader.ConfigValues.ContainsKey(config))
            {
                _configReader.ConfigValues.Add(config, value);
            }
            else
            {
                _configReader.ConfigValues[config] = value;
            }
            return this;
        }

        public static implicit operator ConfigReader(ConfigReaderBuilder instance)
        {
            return instance.Build();
        }

    }
}