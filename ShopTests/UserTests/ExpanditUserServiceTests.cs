﻿using System.Collections.Generic;
using System.Configuration;
using System.Security.Principal;
using System.Web;
using EISCS.Shop.DO.Dto;
using EISCS.Shop.DO.Interface;
using EISCS.Shop.DO.Service;
using Moq;
using NUnit.Framework;
using ShopTests.BusinessLogicTests.Common;

namespace ShopTests.UserTests
{
    [TestFixture]
    public class ExpanditUserServiceTests
    {
        private IUserStorageService _storageService;
        private HttpContextBase _httpContext;
        private ConfigReader _configurationObject;
        private ICurrencyRepository _currencyRepository;

        [SetUp]
        public void Setup()
        {
            _httpContext = FakeHttpContext("store", "user", null);
            _configurationObject = new ConfigReaderBuilder()
                .WithConfigurationValue("ANONYMOUS_CUSTOMERGUID", "INET");

            _currencyRepository = new TestCurrencyRepository();
        }

        public HttpContextBase FakeHttpContext(string cookieName, string cookieKey, string cookieValue)
        {
            var context = new Mock<HttpContextBase>();
            var request = new Mock<HttpRequestBase>();
            var response = new Mock<HttpResponseBase>();
            var session = new Mock<HttpSessionStateBase>();
            var server = new Mock<HttpServerUtilityBase>();
            var user = new Mock<IPrincipal>();
            var identity = new Mock<IIdentity>();

            var cookies = new HttpCookieCollection();
            HttpCookie cookie = new HttpCookie(cookieName);
            cookie[cookieKey] = cookieValue;
            cookies.Add(cookie);

            server.Setup(svr => svr.HtmlDecode(It.IsAny<string>())).Returns((string s) => s);
            server.Setup(svr => svr.HtmlEncode(It.IsAny<string>())).Returns((string s) => s);

            request.Setup(req => req.ApplicationPath).Returns("~/");
            request.Setup(req => req.AppRelativeCurrentExecutionFilePath).Returns(
                "~/a/path/to/some/product");
            request.Setup(req => req.PathInfo).Returns("");

            response.Setup(res => res.ApplyAppPathModifier(It.IsAny<string>()))
                .Returns((string virtualPath) => virtualPath);
            user.Setup(usr => usr.Identity).Returns(identity.Object);
            identity.SetupGet(ident => ident.IsAuthenticated).Returns(true);

            context.Setup(ctx => ctx.Request).Returns(request.Object);
            context.Setup(ctx => ctx.Response).Returns(response.Object);
            context.Setup(ctx => ctx.Session).Returns(session.Object);
            context.Setup(ctx => ctx.Server).Returns(server.Object);
            context.Setup(ctx => ctx.User).Returns(user.Object);

            context.Setup(ctx => ctx.Request.Cookies).Returns(cookies);
            context.Setup(ctx => ctx.Response.Cookies).Returns(cookies);

            return context.Object;
        }

        [Test]
        public void LoadB2CUserTest()
        {
            var users = new List<UserTable>() { new UserTable() { UserGuid = "User1", RoleId = "B2C" } };
            _storageService = new TestUserStorageService(users);
            var userService = new ExpanditUserService(_storageService, _httpContext, _configurationObject, _currencyRepository);
            UserTable user = userService.GetUser("User1");
            Assert.IsNotNull(user);
        }

        // COMMON SETTINGS
        public static string ExpanditShopConnectionString =
            ConfigurationManager.ConnectionStrings["ExpandITConnectionString"].ConnectionString;

        public static string ExpanditConnectionStringProviderName =
            ConfigurationManager.ConnectionStrings["ExpandITConnectionString"].ProviderName;

    }
}
