﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EISCS.Shop.DO.Dto.Currency;
using EISCS.Shop.DO.Interface;

namespace ShopTests.UserTests
{
    public class TestCurrencyRepository : ICurrencyRepository
    {
        public string GetTableName()
        {
            throw new NotImplementedException();
        }

        public string AllColumns()
        {
            throw new NotImplementedException();
        }

        public string GetIdColumnName()
        {
            throw new NotImplementedException();
        }

        public string GetDefaultSql()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<CurrencyItem> All()
        {
            throw new NotImplementedException();
        }

        public CurrencyItem GetById(int id)
        {
            throw new NotImplementedException();
        }

        public CurrencyItem GetById(string id)
        {
            throw new NotImplementedException();
        }

        public CurrencyItem Add(CurrencyItem item, bool useOutputClause = true)
        {
            throw new NotImplementedException();
        }

        public bool Update(CurrencyItem item)
        {
            throw new NotImplementedException();
        }

        public bool Remove(int id)
        {
            throw new NotImplementedException();
        }

        public bool Remove(string id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<CurrencyItem> GetValid()
        {
            throw new NotImplementedException();
        }
    }
}
