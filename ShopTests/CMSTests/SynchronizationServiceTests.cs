﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Web;
using System.Web.Caching;
using System.Web.Hosting;
using System.Web.Routing;
using EISCS.CMS.Configuration;
using EISCS.CMS.ControllerCommon;
using EISCS.CMS.Services.FileService;
using EISCS.CMS.Services.Synchronization;
using Moq;
using NUnit.Framework;

namespace ShopTests.CMSTests
{
    [TestFixture]
    public class SynchronizationServiceTests
    {
        private HttpContextBase _httpContext;
        private string _plainSyncFolderPath;
        private JobCollection _jobCollection;
        private string _escapedSyncFolderPath;

        public TestContext TestContext { get; set; }

        [SetUp]
        public void Setup()
        {
            _httpContext = CustomHttpContext();
            _plainSyncFolderPath = CmsSyncTaskConfigSection.Section.SyncFolder;
            _escapedSyncFolderPath = string.Format("/{0}/", _plainSyncFolderPath);
            _jobCollection = new JobConfiguration().ConfigurationToObjects();
        }

        private HttpContextBase CustomHttpContext()
        {
            var context = new Mock<HttpContextBase>();
            var request = new Mock<HttpRequestBase>();
            var response = new Mock<HttpResponseBase>();
            var session = new Mock<HttpSessionStateBase>();
            var server = new Mock<HttpServerUtilityBase>();
            var user = new Mock<IPrincipal>();
            var identity = new Mock<IIdentity>();
            var requestContext = new Mock<RequestContext>();

            request.Setup(req => req.ApplicationPath).Returns("~/");
            request.Setup(req => req.AppRelativeCurrentExecutionFilePath).Returns("~/english-movies-usa");
            request.Setup(req => req.PathInfo).Returns("");

            response.Setup(res => res.ApplyAppPathModifier(It.IsAny<string>()))
                .Returns((string virtualPath) => virtualPath);
            user.Setup(usr => usr.Identity).Returns(identity.Object);
            identity.SetupGet(ident => ident.IsAuthenticated).Returns(true);

            context.Setup(ctx => ctx.Request).Returns(request.Object);
            context.Setup(ctx => ctx.Response).Returns(response.Object);
            context.Setup(ctx => ctx.Session).Returns(session.Object);
            context.Setup(ctx => ctx.Server).Returns(server.Object);
            context.Setup(ctx => ctx.User).Returns(user.Object);
            context.Setup(ctx => ctx.Cache).Returns(HttpRuntime.Cache);

            context.SetupGet(ctx => ctx.Response.Cookies).Returns(new HttpCookieCollection());
            context.SetupGet(ctx => ctx.Request.Cookies).Returns(new HttpCookieCollection());

            requestContext.Setup(x => x.HttpContext).Returns(context.Object);
            context.Setup(ctx => ctx.Request.RequestContext).Returns(requestContext.Object);

            return context.Object;
        }

        [Test]
        public void TestPollReturnsNegativeValue()
        {
            SynchronizationService service = new SynchronizationService(_httpContext);
            Assert.IsNotNull(service);
            var value = service.Poll();
            Assert.IsNotNull(value);
            string expected = "-1";
            Assert.AreEqual(expected, value);
        }

        [Test]
        public void TestPollReturnsPositiveOrZeroValueIfCached()
        {
            const string jobId = "1";
            const string actionFileName = "anything.job";
            var jobItem = _jobCollection.Job.First();
            string executablePath = "";
            if (jobItem != null)
            {
                executablePath = jobItem.ExecutablePath;
            }
            FileHandler fh = new FileHandler();
            string errMess;
            if (fh.CreateFile("/" + _plainSyncFolderPath, actionFileName, executablePath, out errMess))
            {
                // A job is started, keep the name in the cache   
                    _httpContext.Cache.Insert(jobId, new SyncStatusNode(actionFileName, jobId, "started"),
                                             new CacheDependency(
                                                 HostingEnvironment.MapPath("~" + _escapedSyncFolderPath + actionFileName)));
                    _httpContext.Cache.Insert("CurrentJob", new SyncStatusNode(actionFileName, jobId, "started"),
                                             new CacheDependency(
                                                 HostingEnvironment.MapPath("~" + _escapedSyncFolderPath + actionFileName)));
            }

            SynchronizationService service = new SynchronizationService(_httpContext);
            var value = service.Poll();
            Assert.IsNotNull(value);
            const string wrongValue = "-1";
            Assert.AreNotEqual(wrongValue, value);
            _httpContext.Cache.Remove("CurrentJob");
            fh.DeleteFile("/" + _plainSyncFolderPath + "/" + actionFileName);
        }

        [Test]
        public void TestPollReturnsOneIfQueued()
        {
            // .queued
            const string jobId = "1";
            const string actionFileName = "anything.job.queued";
            var jobItem = _jobCollection.Job.First();
            string executablePath = "";
            if (jobItem != null)
            {
                executablePath = jobItem.ExecutablePath;
            }
            FileHandler fh = new FileHandler();
            string errMess;
            if (fh.CreateFile("/" + _plainSyncFolderPath, actionFileName, executablePath, out errMess))
            {
                // A job is started, keep the name in the cache   
                _httpContext.Cache.Insert(jobId, new SyncStatusNode(actionFileName, jobId, "started"),
                                         new CacheDependency(
                                             HostingEnvironment.MapPath("~" + _escapedSyncFolderPath + actionFileName)));
                _httpContext.Cache.Insert("CurrentJob", new SyncStatusNode(actionFileName, jobId, "started"),
                                         new CacheDependency(
                                             HostingEnvironment.MapPath("~" + _escapedSyncFolderPath + actionFileName)));
            }

            SynchronizationService service = new SynchronizationService(_httpContext);
            var value = service.Poll();
            Assert.IsNotNull(value);
            const string expected = "1";
            Assert.AreEqual(expected, value);
            _httpContext.Cache.Remove("CurrentJob");
            fh.DeleteFile("/" + _plainSyncFolderPath + "/" + actionFileName);
        }
    }
}
