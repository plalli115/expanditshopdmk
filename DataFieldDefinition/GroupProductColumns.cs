﻿namespace DataFieldDefinition
{
    public class GroupProductColumns
    {
        public int GroupProductGuid { get; set; }
        public int GroupGuid { get; set; }
        public string ProductGuid { get; set; }
        public int SortIndex { get; set; }
    }
}