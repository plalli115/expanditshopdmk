﻿namespace DataFieldDefinition
{
    public class ProductTableColumns
    {
        public string ProductGuid { get; set; }
        public string ProductGrp { get; set; }
        public string ProductName { get; set; }
        public double ListPrice { get; set; }
    }
}