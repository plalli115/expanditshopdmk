﻿namespace DataFieldDefinition
{
    public class GroupTableColumns
    {
        public int GroupGuid { get; set; }
        public int ParentGuid { get; set; }
        public int SortIndex { get; set; }
    }
}